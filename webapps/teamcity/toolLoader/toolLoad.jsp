<%@include file="/include-internal.jsp" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="form" scope="request" type="jetbrains.buildServer.tools.web.ToolLoadForm"/>

<c:set var="ajaxUrl"><c:url value="${url}"/></c:set>
<c:set var="selectedTool" value="${form.selectedTool}"/>

<bs:linkScript>
  /js/bs/blocks.js
  /js/bs/blocksWithHeader.js
  /js/bs/forms.js
  /js/bs/multipart.js
</bs:linkScript>

<div>
  <form id="toolLoadForm" action="${ajaxUrl}" method="post" onsubmit="return BS.ToolLoader.ToolLoadForm.submit();"
        autocomplete="off">
    <table class="runnerFormTable">
      <tr>
        <td colspan="2">
          Here you can set up one of the predefined tools to be used by appropriate plugins
        </td>
      </tr>
      <tr>
        <th>
          <label for="toolType">Tool:</label>
        </th>
        <td>
          <select name="toolType" id="toolType" onchange="BS.ToolLoader.refreshToolSettings(this.value);">
            <forms:option value="" selected="${empty selectedTool}">--- Choose tool to load ---</forms:option>
            <c:forEach var="tool" items="${form.tools}">
              <forms:option value="${tool.type}" selected="${not empty selectedTool and tool.type eq selectedTool.type}">
                <c:out value="${tool.displayName}"/>
              </forms:option>
            </c:forEach>
          </select>
          <forms:saving id="toolSettingsSaving" className="progressRingInline"/>
        </td>
      </tr>
    </table>

    <bs:refreshable containerId="toolLoadSettings" pageUrl="${ajaxUrl}">
      <c:if test="${not empty selectedTool}">
        <table class="runnerFormTable">
          <tr>
            <td colspan="2">
              <c:if test="${not empty selectedTool.description}">
              <em>${selectedTool.description}</em>
              </c:if>
              <c:if test="${not empty selectedTool.teamCityHelpFile}">
                <bs:help file="${selectedTool.teamCityHelpFile}" anchor="${selectedTool.teamCityHelpAnchor}"/>
              </c:if>
            </td>
          </tr>
          <tr style="${selectedTool.supportDownload ? '' : 'display:none;'}">
            <th>
              <label for="loadType">Load ${selectedTool.displayName}:</label>
            </th>
            <td>
              <c:set var="onclick">
                if ($('upload').checked) {
                BS.Util.show('uploadSettingsContainer');
                BS.Util.hide('downloadSettingsContainer');
                } else {
                BS.Util.hide('uploadSettingsContainer');
                BS.Util.show('downloadSettingsContainer');
                }
              </c:set>
              <forms:radioButton name="loadType" id="upload"
                                 checked="${form.loadType == 'UPLOAD'}"
                                 onclick="${onclick}" value="UPLOAD"/>
              <label for="upload">Upload</label><br/>

              <forms:radioButton name="loadType" id="download"
                                 checked="${form.loadType == 'DOWNLOAD'}"
                                 onclick="${onclick}" value="DOWNLOAD"/>
              <label for="download">Download</label><br/>
            </td>
          </tr>

          <tr id="uploadSettingsContainer" class="noBorder" style="${form.loadType == 'UPLOAD' ? '' : 'display: none;'}">
            <th class="noBorder"><label for="file:toolFile">Path for uploading: <l:star/></label></th>
            <td class="noBorder">
              <c:set var="attrs">size="60"</c:set>
              <forms:file name="toolFile" attributes="${attrs}"/>
              <span class="error" id="errorToolFile"></span>
              <div class="smallNote" style="margin: 0;">
                Path for uploading
                <c:choose>
                  <c:when test="${empty selectedTool.toolSiteUrl}">
                    ${selectedTool.displayName}
                  </c:when>
                  <c:otherwise>
                    <a showdiscardchangesmessage="false"
                       target="_blank"
                       href="${selectedTool.toolSiteUrl}">${selectedTool.displayName}</a>.
                  </c:otherwise>
                </c:choose>
                <c:if test="${not empty selectedTool.toolLicenseUrl}">
                  <br/>
                  Note that by pressing "Continue" button you accept the
                  <a target="_blank" href="${selectedTool.toolLicenseUrl}"> license terms</a>.
                </c:if>
              </div>
            </td>
          </tr>

          <tr id="downloadSettingsContainer" class="noBorder" style="${form.loadType == 'DOWNLOAD' ? '' : 'display: none;'}">
            <th class="noBorder"><label for="url">URL for downloading: <l:star/></label></th>
            <td class="noBorder">
              <forms:textField name="url" id="url"  className="longField"
                               value="${not empty selectedTool.downloadUrl ? selectedTool.downloadUrl : ''}"/>
              <span class="error" id="errorUrl"></span>
              <div class="smallNote" style="margin: 0;">
                URL for downloading
                <c:choose>
                  <c:when test="${empty selectedTool.toolSiteUrl}">
                    ${selectedTool.displayName}
                  </c:when>
                  <c:otherwise>
                    <a showdiscardchangesmessage="false"
                       target="_blank"
                       href="${selectedTool.toolSiteUrl}">${selectedTool.displayName}</a>.
                  </c:otherwise>
                </c:choose>
                <c:if test="${not empty selectedTool.toolLicenseUrl}">
                  <br/>
                  Note that by pressing "Continue" button you accept the
                  <a target="_blank" href="${selectedTool.toolLicenseUrl}"> license terms</a>.
                </c:if>
              </div>
            </td>
          </tr>
        </table>

        <div class="saveButtonsBlock">
          <forms:submit
                 id="continueButton" name="continueButton" label="Continue"/>
          <forms:saving/>
          <input type="hidden" id="submit" name="submit"/>
        </div>
      </c:if>
    </bs:refreshable>
  </form>
</div>

<bs:refreshable containerId="toolLoadMessages" pageUrl="${ajaxUrl}">
  <c:forEach items="${form.messages}" var="message">
    ${message}<br/>
  </c:forEach>
  <script type="text/javascript">
    <c:if test="${form.running}">
    window.setTimeout(function() {
      $('toolLoadMessages').refresh()
    }, 200);
    </c:if>
  </script>
</bs:refreshable>

<script type="text/javascript">
  BS.ToolLoader = {
    refreshToolSettings: function(toolType) {
      $("toolLoadSettings").refresh("toolSettingsSaving", "toolType=" + toolType, null);
      $("toolLoadMessages").refresh();
    },

    ToolLoadForm : OO.extend(BS.AbstractWebForm, {
      formElement: function() {
        return $('toolLoadForm');
      },

      submit: function() {
        var that = this;

        var url = that.formElement().action;
        var listener = OO.extend(BS.ToolLoader.ToolLoadListener, {
          getForm: function() {
            return that;
          }
        });

        if ($('upload').checked) {
          BS.MultipartFormSaver.save(that, url, listener);
        } else {
          BS.FormSaver.save(that, url, listener);
        }

        return false;
      }
    }),

    ToolLoadListener : OO.extend(BS.ErrorsAwareListener, {
      onBeginSave: function(form) {
        BS.ErrorsAwareListener.onBeginSave(form);

        window.setTimeout(function() {
          $('toolLoadMessages').refresh();
        }, 200);
      },

      onWrongUrlError: function(elem) {
        $("errorUrl").innerHTML = elem.firstChild.nodeValue;
        this.getForm().highlightErrorField($("url"));
      },

      onWrongFileError: function(elem) {
        $("errorToolFile").innerHTML = elem.firstChild.nodeValue;
        this.getForm().highlightErrorField($("file:toolFile"));
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        $('toolLoadMessages').refresh();
        if (!err) {
          this.getForm().enable();
          $('continueButton').disable()
        }
      }
    })
  };
</script>