<%@ include file="include-internal.jsp" %>
<jsp:useBean id="queuedBuild" type="jetbrains.buildServer.serverSide.SQueuedBuild" scope="request"/>
<jsp:useBean id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary" scope="request"/>
<c:set var="buildType" value="${queuedBuild.buildType}"/>
<c:set var="numberOfAgents" value="${serverSummary.authorizedAgentsCount}"/>
<c:set var="queuedBuildTitle">#${queuedBuild.orderNumber} ${buildType.fullName}</c:set>
<c:set var="pageTitle" scope="request">Build Queue > ${queuedBuildTitle} > Compatible Agents</c:set>
<bs:page>
  <jsp:attribute name="head_include">
  <bs:linkCSS>
    /css/compatibilityList.css
  </bs:linkCSS>
  <bs:linkScript>
    /js/bs/blocks.js
    /js/bs/blocksWithHeader.js
    /js/bs/blockWithHandle.js
    /js/bs/collapseExpand.js
  </bs:linkScript>
  <script type="text/javascript">
    BS.Navigation.items = [
      {title: "Build Queue", url: '<c:url value="/queue.html"/>'},
      {title: "<bs:escapeForJs text="${queuedBuildTitle}" forHTMLAttribute="true"/>", url: "<c:url value='/queue.html?buildTypeId=${buildType.buildTypeId}#ref${buildType.buildTypeId}'/>"},
      {title: "Compatible Agents", selected: true}
    ];
  </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <p>
      This page shows compatible agents for queued build <queue:queueLink itemId="${buildType.buildTypeId}" noPopup="true"><c:out value="${queuedBuildTitle}"/></queue:queueLink>.
      If queued build has custom parameters this set of compatible agents can differ from <bs:buildTypeTabLink buildType="${buildType}" title="Click to view compatible agents of build configuration" tab="compatibilityList">compatible agents</bs:buildTypeTabLink> of <bs:buildTypeLinkFull buildType="${buildType}"/>.
    </p>

    <p>There <bs:are_is val="${numberOfAgents}"/> ${numberOfAgents} agent<bs:s val="${numberOfAgents}"/> authorized.</p>
    <bs:buildTypeCompatibility compatibleAgents="${queuedBuild}" project="${buildType.project}"/>
  </jsp:attribute>
</bs:page>

