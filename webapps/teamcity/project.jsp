<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %><%@
    include file="include-internal.jsp" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin"
%><jsp:useBean id="currentUser" type="jetbrains.buildServer.users.User" scope="request"
/><jsp:useBean id="project" type="jetbrains.buildServer.serverSide.SProject"  scope="request"
/><jsp:useBean id="branchBean" type="jetbrains.buildServer.controllers.BranchBean" scope="request"
/><jsp:useBean id="branchSelectorEnabled" type="java.lang.Boolean" scope="request"

/><ext:defineExtensionTab placeId="<%=PlaceId.PROJECT_TAB%>"/>
<%--@elvariable id="extensionTab" type="jetbrains.buildServer.web.openapi.CustomTab"--%>
<bs:page>
  <jsp:attribute name="page_title">Project: ${project.name}</jsp:attribute>
  <jsp:attribute name="quickLinks_include">
    <c:if test="${extensionTab.tabId == 'projectOverview'}">
      <div class="toolbarItem">
        <profile:booleanPropertyCheckbox propertyKey="overview.hideSuccessful"
                                         labelText="Hide Successful Configurations" afterComplete="BS.reload(true);"/>
      </div>
    </c:if>
    <authz:authorize projectId="${project.projectId}" allPermissions="EDIT_PROJECT">
      <admin:editProjectLink projectId="${project.projectId}"
                             title="Edit project settings"
                             addToUrl="&cameFromUrl=${cameFromUrl}"
        >Edit Project Settings</admin:editProjectLink>
    </authz:authorize>
  </jsp:attribute>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/settingsTable.css
      /css/progress.css
      /css/overviewTable.css
      /css/overview.css
      /css/filePopup.css
      /css/buildQueue.css
      /css/agentsInfoPopup.css
      /css/pager.css
      /css/project.css
      /css/visibleProjects.css
    </bs:linkCSS>

    <style type="text/css">
      /* Hiding "project header" */
      .build .projectHeader {
        display: none;
      }

      .projectContent {
        margin-top: 10px;
      }

      .projectContent .logTable {
        padding: 0;
      }
    </style>

    <bs:linkScript>
      /js/bs/blocks.js
      /js/bs/blockWithHandle.js

      /js/bs/overview.js
      /js/bs/runningBuilds.js
      /js/bs/testGroup.js

      /js/bs/systemProblemsMonitor.js
      /js/bs/collapseExpand.js
      /js/bs/visibleDialog.js
      /js/bs/queueLikeSorter.js
      /js/bs/buildQueue.js
      /js/bs/overflower.js
      /js/bs/buildType.js
      /js/bs/problemsSummary.js
    </bs:linkScript>
    <c:if test="${not empty project.description}">
      <c:set var="projectDescription"><bs:out value="${project.description}" /></c:set>
      <c:set var="projectDescription">(<bs:escapeForJs text="${projectDescription}" forHTMLAttribute="false"/>)</c:set>
    </c:if>

    <script type="text/javascript">
      <bs:trimWhitespace>
        BS.Navigation.items = [
          {
            title: "<bs:escapeForJs text="${project.name}" forHTMLAttribute="true"/> <small>${projectDescription}</small>",
            selected: true
          }
        ];

        $j(document).ready(function() {
          BS.RunningBuilds.startUpdates();
          BS.SystemProblems.setOptions({projectId:'${project.projectId}'});
          BS.SystemProblems.startUpdates();
          BS.Navigation.selectProjectsTab();
        });

        <authz:authorize allPermissions="CHANGE_OWN_PROFILE">
        $j(document).on("bs.navigationRendered", function() {
          BS.Projects.installShowHideLink(${isProjectPresentOnOverview ? 1 : 0}, "${project.projectId}");
        });
        </authz:authorize>

        // The following is used from projectBuildTypes.jsp
        BS.Projects.updateProjectView = function(projectId, firstProjectInList, force) {
          projectId = projectId || '${project.projectId}';
          force = !!force || false;
          BS.reload(force, function() {
            $('projectContainer').refresh(null, null, function() {
              BS.SystemProblems.updateNow();
            });
          });
        };

        <c:if test="${branchBean.hasBranches}">
          $j(document).on("bs.breadcrumbRendered", function(event) {
            BS.Branch.installDropdownToBreadcrumb("<bs:escapeForJs text="${branchBean.userBranch}"/>", ${branchSelectorEnabled},
                                                  <bs:_branchesListJs branches="${branchBean.activeBranches}"/>,
                                                  <bs:_branchesListJs branches="${branchBean.otherBranches}"/>,
                                                  {projectId: "${project.projectId}", tab: "${extensionTab.tabId}"});
          });
        </c:if>
      </bs:trimWhitespace>
    </script>
  </jsp:attribute>

  <jsp:attribute name="body_include">
    <et:subscribeOnProjectEvents projectId="${project.projectId}">
      <jsp:attribute name="eventNames">
        PROJECT_RESTORED
        PROJECT_PERSISTED
      </jsp:attribute>
      <jsp:attribute name="eventHandler">
        BS.Projects.updateView();
      </jsp:attribute>
    </et:subscribeOnProjectEvents>
    <et:subscribeOnProjectEvents projectId="${project.projectId}">
      <jsp:attribute name="eventNames">
        PROJECT_REMOVED
        PROJECT_ARCHIVED
        PROJECT_DEARCHIVED
      </jsp:attribute>
      <jsp:attribute name="eventHandler">
        BS.reload();
      </jsp:attribute>
    </et:subscribeOnProjectEvents>

    <bs:refreshable containerId="projectContainer" pageUrl="${pageUrl}">
      <bs:projectArchived project="${project}"/>
      <div class="projectContent">
        <ext:showTabs placeId="<%=PlaceId.PROJECT_TAB%>" urlPrefix="/project.html?projectId=${project.projectId}"/>
      </div>
    </bs:refreshable>
  </jsp:attribute>
</bs:page>
