<%@ include file="include-internal.jsp" %>
<%@ taglib prefix="agent" tagdir="/WEB-INF/tags/agent"%>
<jsp:useBean id="agentGroups" scope="request" type="java.util.List<jetbrains.buildServer.controllers.agent.AgentGroup>"/>
<jsp:useBean id="agentsForm" scope="request" type="jetbrains.buildServer.controllers.agent.AgentListForm"/>

<c:set var="grouped" value="${agentsForm.actuallyGroupByPools}"/>

<table cellspacing="0" class="agents dark sortable borderBottom">
  <tr>
    <agent:poolTH/>
    <th class="buildAgentName sortable"><span id="SORT_BY_NAME">Agent</span></th>
    <th class="agentStatus sortable"><span id="SORT_BY_STATUS">Status</span></th>
    <th class="lastActivity sortable"><span id="SORT_BY_LAST_ACTIVITY_DATE">Last communication date</span></th>
    <th class="inactivityReason sortable"><span id="SORT_BY_INACTIVITY_REASON">Inactivity reason</span></th>
    <th class="lastCell sortable" colspan="2"><span id="SORT_BY_RUNNING_BUILD">Running build</span></th>
  </tr>

  <c:forEach var="agentGroup" items="${agentGroups}" varStatus="groupIndex">
    <c:if test="${grouped}">
      <tr class="no-border">
        <td class="no-border firstCell"><bs:agentPoolHandle agentPoolId="${agentGroup.poolId}"/></td>
        <td colspan="6" class="poolHeader"><bs:agentPoolLink agentPoolId="${agentGroup.poolId}" agentPoolName="${agentGroup.name}" groupHeader="${true}"/></td>
      </tr>
    </c:if>

    <c:forEach var="buildAgent" items="${agentGroup.agents}" varStatus="agentIndex">
      <c:set var="rowClass" value=""/>
      <c:if test="${not buildAgent.enabled}">
        <c:set var="rowClass" value="disabledAgent"/>
      </c:if>
      <tr class="${rowClass} agentRow-${agentGroup.poolId} <c:if test="${grouped and agentIndex.last}">no-border</c:if>" id="agentRow:${buildAgent.id}" style='<bs:agentRowVisibility agentPoolId="${agentGroup.poolId}"/>'>

        <c:if test="${grouped}">
          <td class="emptyCell">&nbsp;</td>
        </c:if>

        <td class="buildAgentName">
          <bs:agentDetailsFullLink agent="${buildAgent}" doNotShowOutdated="${true}" doNotShowPoolInfo="${grouped}" doNotShowUnavailableStatus="${true}"/>
        </td>

        <td class="agentStatus">
          <jsp:include page="/agentStatus.html?id=${buildAgent.id}&tableMode=1"/>
        </td>

        <td class="lastActivity">
          <bs:date value="${buildAgent.lastCommunicationTimestamp}"/>
        </td>

        <td class="inactivityReason">
          <c:out value="${buildAgent.unregistrationComment}"/>
        </td>
        <c:if test="${empty buildAgent.runningBuild}">
          <td colspan="2">&nbsp;</td>
        </c:if>
        <c:if test="${not empty buildAgent.runningBuild}">
          <authz:authorize allPermissions="VIEW_PROJECT" projectId="${buildAgent.runningBuild.projectId}">
            <jsp:attribute name="ifAccessGranted">
              <td class="buildName">
                <bs:buildTypeLinkFull buildType="${buildAgent.runningBuild.buildType}"/>
              </td>
              <td class="status">
                <bs:buildDataIcon buildData="${buildAgent.runningBuild}"
                                  imgId="build:${buildAgent.runningBuild.buildId}:img"/>
                <bs:resultsLink build="${buildAgent.runningBuild}"><span id="build:${buildAgent.runningBuild.buildId}:text">${buildAgent.runningBuild.statusDescriptor.text}</span></bs:resultsLink>
              </td>
            </jsp:attribute>
            <jsp:attribute name="ifAccessDenied">
              <td colspan="2"><img src="<c:url value='/img/buildStates/running_green_transparent.gif'/>" class="icon"/> Running a build. You do not have permissions to see the build details.</td>
            </jsp:attribute>
          </authz:authorize>
        </c:if>
      </tr>
    </c:forEach>
  </c:forEach>
</table>

<agent:restoreAgentBlockStates grouped="${grouped}"/>

<bs:changeAgentStatus agentActionCode="changeAgentStatus"/>