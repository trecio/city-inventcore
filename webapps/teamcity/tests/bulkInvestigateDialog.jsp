<%@ include file="/include-internal.jsp"%>
<%@ taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="resp" tagdir="/WEB-INF/tags/responsible" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>

<jsp:useBean id="investigateBean" type="jetbrains.buildServer.controllers.tests.BulkInvestigateBean" scope="request"/>
<jsp:useBean id="muteTestBean" type="jetbrains.buildServer.controllers.tests.MuteTestBean" scope="request"/>
<%--@elvariable id="currentUser" type="jetbrains.buildServer.users.SUser"--%>

<div class="investigation-dialog">
  <div class="list custom-scroll">
    <c:forEach items="${muteTestBean.tests}" var="entry">
      <c:set var="test" value="${entry.key}"/>
      <div class="testSpan">
        <bs:responsibleIcon responsibility="${test.responsibility}" test="${test}" noActions="true"/>
        <bs:currentMuteIcon test="${test}" noActions="true"/>
        <tt:testName testBean="${test}" showPackage="true"/>

        <c:set var="valueStr" value=""/>
        <c:forEach var="build" items="${entry.value}">
          <c:set var="valueStr" value="${valueStr},${build.buildId}"/>
        </c:forEach>
        <input type="hidden" name="${test.testId}" value="${valueStr}"/>
      </div>
    </c:forEach>
  </div>

  <authz:authorize projectId="${investigateBean.projectId}" allPermissions="ASSIGN_INVESTIGATION">
    <jsp:attribute name="ifAccessGranted">
      <h3 class="blockHeader">
        <a href="#" id="reset-investigate" class="reset">reset</a>
        <forms:checkbox name="do-investigate" checked="${investigateBean.known}"/>
        <label>Investigation options</label>
      </h3>
      <table class="investigate-params collapsible-section" id="investigate-section">
        <tr>
          <th>
            <forms:radioButton name="investigate" value="ASSIGN" id="assign-investigate"
                               checked="${investigateBean.known and investigateBean.assigned}"/>
            <label for="assign-investigate">Investigated by:</label>
          </th>
          <td>
            <forms:select name="investigator" className="wholeWidth" id="investigator" enableFilter="true">
              <resp:userOptions currentUser="${currentUser}" selectedUser="${investigateBean.investigator}"
                                potentialResponsibles="${investigateBean.users}" allowNoResponsible="false"/>
            </forms:select>
            <div id="test-investigation-warning"></div>
            <table class="inner">
              <tr>
                <th>
                  <label for="remove-investigation">Resolve:</label>
                </th>
                <td>
                  <forms:select name="remove-investigation">
                    <forms:option value="0" selected="${investigateBean.removeMethod.whenFixed}">When test passes successfully</forms:option>
                    <forms:option value="1" selected="${investigateBean.removeMethod.manually}">Manually</forms:option>
                  </forms:select>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <th>
            <forms:radioButton name="investigate" value="MARK_FIXED" id="fix-investigate"
                               checked="${investigateBean.known and investigateBean.fixed}"/>
            <label for="fix-investigate">Mark as fixed</label>
          </th>
          <td></td>
        </tr>
        <tr>
          <th>
            <forms:radioButton name="investigate" value="GIVE_UP" id="giveup-investigate"
                               checked="${investigateBean.known and investigateBean.givenUp}"/>
            <label for="giveup-investigate">No investigation</label>
          </th>
          <td></td>
        </tr>
      </table>
    </jsp:attribute>
  </authz:authorize>

  <authz:authorize projectId="${investigateBean.projectId}" allPermissions="MANAGE_BUILD_PROBLEMS">
    <jsp:attribute name="ifAccessGranted">
      <h3 class="blockHeader">
        <a href="#" id="reset-mute" class="reset">reset</a>
        <forms:checkbox name="do-mute" checked="${muteTestBean.known}"/>
        <label>Mute options</label>
      </h3>
      <div class="collapsible-section"  id="mute-section">
        <div>
          <table class="mute-params">
            <tr>
              <th>
                <forms:radioButton name="mute" value="MUTE" id="mute-mute" checked="${muteTestBean.known and muteTestBean.muted}"/>
                <label for="mute-mute">Mute in:</label>
              </th>
              <td>
                <forms:select name="scope" id="mute-scope">
                  <forms:option value="P">Project-wide</forms:option>
                  <forms:option value="C" selected="${muteTestBean.buildTypeScopeSelected}">Selected build configuration</forms:option>
                </forms:select>
                <div class="custom-scroll" id="mute-in-bt-list" <c:if test="${not muteTestBean.buildTypeScopeSelected}">style="display: none"</c:if>>
                  <div class="error-msg" id="bt-list-error"></div>
                  <tt:_checkedBuildTypes muteTestBean="${muteTestBean}"/>
                </div>

                <table class="inner">
                  <tr>
                    <th>
                      <label for="unmute">Unmute:</label>
                    </th>
                    <td>
                      <c:set var="unmuteOption" value="${muteTestBean.selectedUnmuteOption}"/>
                      <forms:select name="unmute">
                        <forms:option value="F" selected="${unmuteOption == 'F'}">When test passes successfully</forms:option>
                        <forms:option value="M" selected="${unmuteOption == 'M'}">Manually</forms:option>
                        <forms:option value="T" selected="${unmuteOption == 'T'}">On a specific date</forms:option>
                      </forms:select>
                    </td>
                  </tr>
                  <c:set var="style" value="${unmuteOption == 'T' ? '' : 'display: none'}"/>
                  <tr id="unmute-row" style="${style}">
                    <th>
                      <label for="unmute-time">Unmute on:</label>
                    </th>
                    <td>
                      <forms:textField name="unmute-time" value="${muteTestBean.selectedUnmuteTimeOption}"/>
                    </td>
                  </tr>
                  <tr><td colspan="2"><div class="error-msg" id="unmute-time-error"></div></td></tr>
                </table>
              </td>
            </tr>
            <tr>
              <th>
                <forms:radioButton name="mute" value="UNMUTE" id="unmute-mute" checked="${muteTestBean.known and not muteTestBean.muted}"/>
                <label for="unmute-mute">Not muted</label>
              </th>
              <td></td>
            </tr>
          </table>
        </div>
      </div>
    </jsp:attribute>
  </authz:authorize>

  <h3 class="blockHeader">Comment</h3>
  <table class="mute-params collapsible-section">
    <tr>
      <td colspan="2">
        <textarea name="comment" id="comment" rows="3" cols="46" class="commentTextArea">${investigateBean.comment}</textarea>
        <div class="error-msg" id="comment-error"></div>
      </td>
    </tr>
  </table>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.BulkInvestigateDialog.close()"/>
    <forms:submit onclick="return BS.BulkInvestigateDialog.submit();" id="submit" label="Save"/>
    <forms:saving id="investigate-saving"/>

    <input type="hidden" name="projectId" value="${investigateBean.projectId}"/>
  </div>
</div>

<script type="text/javascript">
  <c:set var="assignAutomatically" value="${investigateBean.givenUp and not muteTestBean.muted and not muteTestBean.bulk ? '1' : '0'}"/>
  <c:set var="contextBuildTypeId" value="${not empty muteTestBean.contextBuildType ? muteTestBean.contextBuildType.buildTypeId : ''}"/>
  BS.BulkInvestigateDialog.prepareOnShow(${assignAutomatically}, '${contextBuildTypeId}');
  BS.BulkInvestigateDialog.checkUserPermissionsOnChange('${investigateBean.projectId}');
</script>
