<%@ page
  %><%@include file="/include-internal.jsp"
  %><%@ taglib prefix="tt" tagdir="/WEB-INF/tags/tests"

%><jsp:useBean id="pagerUrlPattern" scope="request" type="java.lang.String"
/><jsp:useBean id="pager" type="jetbrains.buildServer.util.Pager" scope="request"
/><jsp:useBean id="tests" type="java.util.List<jetbrains.buildServer.serverSide.stat.TestFailureRate>" scope="request"
/><jsp:useBean id="thresholdHours" type="java.lang.Long" scope="request"
/><jsp:useBean id="buildTypes" type="java.util.List" scope="request"
/><jsp:useBean id="project" scope="request" type="jetbrains.buildServer.serverSide.SProject"

/><c:if test="${empty buildType}">
  <c:set var="buildType" value="${null}"/>
</c:if

><bs:refreshable containerId="problematicTests" pageUrl="">
  
<div class="actionBar clearfix">
  Show problematic tests<bs:help file="Viewing+Tests+and+Configuration+Problems" anchor="ViewingTestsFailedwithinLast120Hours"/> for:
  <forms:select name="btSelector" className="btSelector" onchange="selectBuildType(this.options[this.selectedIndex].value);" enableFilter="true">
    <option value="" <c:if test="${empty buildType}">selected</c:if>>&lt;All build configurations&gt;</option>
    <c:forEach var="bt" items="${buildTypes}">
      <option value="${bt.buildTypeId}" <c:if test="${bt == buildType}">selected</c:if>>${bt.name}</option>
    </c:forEach>
  </forms:select>
  <forms:saving className="progressRingInline"/>
</div>

<c:choose>
  <c:when test="${fn:length(tests) == 0}">
    <p>No tests found</p>
  </c:when>
  <c:otherwise>
    <table cellspacing="0" class="testList dark borderBottom">
      <tr>
        <th>Test</th>
        <th style="text-align:right;">Total Runs</th>
        <th style="text-align:right;">Failure Count</th>
        <th style="text-align:right;">Last Failure Time</th>
      </tr>
      <c:forEach items="${tests}" var="failureRate">
        <jsp:useBean id="failureRate" type="jetbrains.buildServer.serverSide.stat.TestFailureRate"/>
        <tr>
          <td>
            <tt:testNameWithLink showPackage="true" testBean="${failureRate.test}"/>
          </td>
          <td style="text-align:right;">
            ${failureRate.successCount + failureRate.failureCount}
          </td>
          <td style="text-align:right;">
            <%--<fmt:formatNumber value="${test.failureRate * 100}" minFractionDigits="1" maxFractionDigits="1"/> %--%>
            ${failureRate.failureCount}
          </td>
          <td style="text-align:right;">
            <bs:date value="${failureRate.lastFailureTime}" pattern="dd MMM yy HH:mm"/>
          </td>
        </tr>
      </c:forEach>
    </table>
  </c:otherwise>
</c:choose>
<bs:pager place="bottom" urlPattern="${pagerUrlPattern}" pager="${pager}"/>
</bs:refreshable>
<c:url var="pagerUrl" value="project.html?projectId=${project.projectId}&tab=tests"/>
<script type="text/javascript">
  function selectBuildType(btId) {
    BS.Refreshable.createRefreshFunction('problematicTests', '${pagerUrl}&buildTypeId=' + btId, '')('saving');
  }
</script>
