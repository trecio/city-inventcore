<%@ include file="include-internal.jsp" %>
<jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.BuildTypeEx" scope="request"/>
<jsp:useBean id="dependencyGraphs" type="jetbrains.buildServer.controllers.graph.DependencyGraphsBean" scope="request"/>

<c:set var="numGraphs" value="${dependencyGraphs.numberOfChains}"/>

<bs:buildChains dependencyGraphsBean="${dependencyGraphs}"/>

<script type="text/javascript">
  BS.Branch.baseUrl = "${pageUrl}";
</script>