<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %>
<%@include file="/include-internal.jsp"%>
<c:set var="pageTitle" value="License Agreement"/>
<bs:externalPage>
  <jsp:attribute name="page_title">${pageTitle}</jsp:attribute>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/forms.css
      /css/initialPages.css
    </bs:linkCSS>
    <style type="text/css">
      .licensesPage {
        width: 800px;
        margin-top: 3em;
      }

      .agreement {
        width: 750px;
        height: 550px;
        overflow: auto;
        margin: 2em auto;
      }

      .agreementForm {
        width: 750px;
        margin: auto;
      }

      .agreementForm .continueBlock {
        margin-bottom: 0;
      }
    </style>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <div class="licensesPage">
      <div class="agreement">
      <jsp:include page="/license/agreement.jsp"/>
      </div>
      <div class="agreementForm clearfix">
        <p>You must accept the license agreement to proceed.</p>
        <form action="${pageUrl}" method="post" onsubmit="if (!this.accept.checked) { alert('Please accept the license agreement'); return false; };">
          <p><forms:checkbox name="accept"/><label for="accept" class="rightLabel">Accept license agreement</label></p>
          <ext:includeExtensions placeId="<%=PlaceId.ACCEPT_LICENSE_SETTING%>"/>
          <p class="continueBlock">
            <input class="btn" type="submit" name="Continue" value="Continue &raquo;"/>
          </p>
        </form>
      </div>
    </div>
  </jsp:attribute>
</bs:externalPage>
