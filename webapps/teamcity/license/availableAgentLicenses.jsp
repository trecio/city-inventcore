<%@ include file="/include-internal.jsp"%>
<jsp:useBean id="agentLicenseData" type="jetbrains.buildServer.controllers.license.AgentLicenseData" scope="request"/>

<div>
  <c:set var="prefix">TeamCity ${serverTC.fullServerVersion}, effective release date <bs:formatDate value="${agentLicenseData.releaseDate}" pattern="yyyy-MMM-dd"/></c:set>

  <bs:refreshable containerId="availableLicenses" pageUrl="${pageUrl}">
    <c:choose>
      <c:when test="${agentLicenseData.enterpriseLicenseAvailable and not agentLicenseData.evaluationMode}">
        <div class="licenseMode">
          ${prefix}, currently running in <strong>enterprise</strong> mode. <bs:help file="Licensing+Policy"/>
        </div>
      </c:when>
      <c:when test="${not agentLicenseData.enterpriseLicenseAvailable}">
        <div class="licenseMode">
          ${prefix}, currently running in <strong>professional</strong> mode. <bs:help file="Licensing+Policy"/>
        </div>
      </c:when>
      <c:when test="${agentLicenseData.evaluationMode}">
        <div class="licenseMode">
          ${prefix}, currently running in <strong>evaluation</strong> mode. <bs:help file="Licensing+Policy"/>
        </div>
      </c:when>
    </c:choose>

    <c:set value="${agentLicenseData.licensedAgentCount - agentLicenseData.numberOfPredefinedAgents}" var="additionalAgents"/>
    <p>Maximum number of licensed agents: <c:choose>
      <c:when test="${agentLicenseData.unlimitedAgents}"><strong>unlimited</strong></c:when>
      <c:otherwise>
        <strong>${agentLicenseData.numberOfPredefinedAgents}<c:if test="${additionalAgents > 0}"> + ${additionalAgents}</c:if></strong>
        agent<bs:s val="${agentLicenseData.licensedAgentCount}"/>
      </c:otherwise>
    </c:choose>
    </p>

    <bs:messages key="licenseKeyRemoved" style="width: 76em; margin-left: 0;"/>
    <bs:messages key="licenseKeysAdded" style="width: 76em; margin-left: 0;"/>

    <c:if test="${agentLicenseData.licenseKeysExist}">
    <h3 class="title_underlined">Active license keys</h3>

    <table class="settings licensesTable">
      <tr>
        <th class="name licenseKey">License key</th>
        <th class="name">Type</th>
        <th class="name features" >Features</th>
        <th class="name numAgents" ># of agents</th>
        <th class="name generationDate">Generation date</th>
        <th class="name subscriptionDate">End of maintenance</th>
        <th class="name expirationDate" colspan="2">Expiration date</th>
      </tr>
      <c:forEach items="${agentLicenseData.licenses}" var="lic">
        <tr>
          <td class="licenseKey">
            <bs:trimWithTooltip maxlength="31">${lic.key}</bs:trimWithTooltip>
          </td>
          <td class="licenseType">
            <strong><c:out value="${lic.licenseName}"/></strong>
          </td>
          <td class="features"><c:out value='${lic.features}'/></td>
          <td class="numAgents"><c:out value='${lic.numberOfAgents}'/></td>
          <td class="generationDate"><bs:formatDate value="${lic.generationDate}" pattern="yyyy-MMM-dd"/></td>
          <td class="subscriptionDate">
            <bs:formatDate value="${lic.maintenanceDueDate}" pattern="yyyy-MMM-dd"/>
          </td>
          <td class="expirationDate">
            <c:choose>
              <c:when test="${not empty lic.expirationDate}"><bs:formatDate value="${lic.expirationDate}" pattern="yyyy-MMM-dd"/></c:when>
              <c:otherwise>N/A</c:otherwise>
            </c:choose>
            <c:if test="${lic.licenseExpired}"> (<span class="expired">expired</span>)</c:if>
          </td>
          <td class="edit">
            <a href="#" onclick="BS.LicensesForm.removeLicenseKey('${lic.key}'); return false">Remove</a>
          </td>
        </tr>
      </c:forEach>
    </table>
    </c:if>

    <c:if test="${not empty agentLicenseData.inactiveLicenses}">
      <div class="inactiveLicensesContainer">
        <l:blockStateCss blocksType="Block_inactiveLicenses" collapsedByDefault="false" id="inactiveLicensesTable"/>
        <p class="blockHeader expanded" id="inactiveLicenses">Inactive license keys: ${fn:length(agentLicenseData.inactiveLicenses)}</p>
        <table class="settings inactiveLicensesTable" id="inactiveLicensesTable">
          <tr>
            <th class="name licenseKey">License key</th>
            <th class="name" colspan="2">Description</th>
          </tr>
          <c:forEach items="${agentLicenseData.inactiveLicenses}" var="lic">
            <tr>
              <td><c:out value="${lic.key}"/></td>
              <td>
                <c:choose>
                  <c:when test="${not lic.recognized and lic.pre50Key}"><strong><c:out value="${lic.pre50KeyDescription}"/></strong> (<span class="expired">not valid starting from 5.0 version</span>)</c:when>
                  <c:when test="${not lic.recognized}">Invalid key</c:when>
                  <c:when test="${lic.recognized}">
                    <strong><c:out value="${lic.licenseName}"/></strong>,
                    <c:choose>
                      <c:when test="${not empty lic.expirationDate}">expires: <bs:formatDate value="${lic.expirationDate}" pattern="yyyy-MMM-dd"/></c:when>
                      <c:otherwise>end of maintenance: <bs:formatDate value="${lic.maintenanceDueDate}" pattern="yyyy-MMM-dd"/></c:otherwise>
                    </c:choose>

                    <c:if test="${lic.licenseExpired}"> (<span class="expired">expired</span>)</c:if>
                    <c:if test="${lic.licenseObsolete}"> (<span class="expired">obsolete</span>)</c:if>
                  </c:when>
                </c:choose>
              </td>
              <td class="edit">
                <a href="#" onclick="BS.LicensesForm.removeLicenseKey('<bs:escapeForJs text="${lic.key}" forHTMLAttribute="true"/>'); return false">Remove</a>
              </td>
            </tr>
          </c:forEach>
        </table>

        <script type="text/javascript">
          <l:blockState blocksType="Block_inactiveLicenses"/>
          new BS.BlocksWithHeader('inactiveLicenses');
        </script>

        <br/>
      </div>
    </c:if>
  </bs:refreshable>

</div>
