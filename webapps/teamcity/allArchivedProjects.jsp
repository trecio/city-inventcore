<%@ include file="include-internal.jsp" %>

<l:tableWithHighlighting highlightImmediately="true" id="archivedProjectsPopupTable" className="projectsPopupTable">
  <c:forEach items="${archivedProjects}" var="project">
  <%--@elvariable id="project" type="jetbrains.buildServer.serverSide.SProject"--%>
    <tr class="">
      <td class="projectName highlight" title="<c:out value='${project.description}'/>">
        <a href="<bs:projectUrl projectId="${project.projectId}"/>"
           class="projectLink"
           title="Open project page"
           showdiscardchangesmessage="false"><c:out value="${project.name}"/></a>
      </td>
    </tr>
  </c:forEach>
</l:tableWithHighlighting>
