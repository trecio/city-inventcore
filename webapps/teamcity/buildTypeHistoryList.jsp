<%@ include file="include-internal.jsp" %><%@
    taglib prefix="tags" tagdir="/WEB-INF/tags/tags"
%><jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.SBuildType" scope="request"
/><jsp:useBean id="historyPager" type="jetbrains.buildServer.util.Pager" scope="request"
/><jsp:useBean id="historyForm" type="jetbrains.buildServer.controllers.buildType.tabs.HistorySearchBean" scope="request"
/><c:url var="history_url" value="/viewType.html?buildTypeId=${buildType.buildTypeId}&tab=buildTypeHistoryList"/>

<style type="text/css">
  #buildTypeHistoryList {
    margin-top: 10px;
  }
</style>

<div id="buildTypeHistoryList">
  <bs:refreshable containerId="historyTable" pageUrl="${history_url}">
    <bs:messages key="recordNotFound"/>
    <div id="jumpToPopup" class="popupDiv quickLinksMenuPopup">
      <ul class="menuList">
        <l:li><a href="#" onclick="BS.JumpTo.jump('lastSuccessful'); return false">Last successful build</a></l:li>
        <l:li><a href="#" onclick="BS.JumpTo.jump('lastPinned'); return false">Last pinned build</a></l:li>
      </ul>
    </div>

    <c:set var="jumpedTo" value="${not empty jumpedTo ? jumpedTo : -1}"/>

    <form id="historyFilter" action="<c:url value='/viewType.html'/>" method="post" onsubmit="return BS.HistoryTable.doSearch()">

    <table class="historyFilterTable">
      <tr>
        <td colspan="3" style="width: 100%;">
          <c:if test="${not empty buildType.tags or not empty historyForm.tag}">
            <div style="padding-bottom: 5px;">
              Filter by tag: &nbsp;
              <tags:showTags tags="${buildType.tags}"
                          buildTypeId="${buildType.buildTypeId}"
                          selectedTag="${historyForm.tag}"
                onclick="$('historyFilter').elements['tag'].value = this.innerHTML; BS.HistoryTable.doSearch(); return false;"/>
              <c:if test="${not empty historyForm.tag}">
                &nbsp; <forms:resetFilter resetHandler="$('historyFilter').elements['tag'].value = ''; BS.HistoryTable.doSearch(); return false;"/>
              </c:if>
            </div>
          </c:if>
        </td>
      </tr>
      <tr>
         <td class="column1">
          <div>
            <label for="agentName">Filter by agent name:</label>
            <forms:textField name="agentName" style="width: 8em;" value="${historyForm.agentName}"/>
            <input class="btn btn_mini" type="submit" name="Search" value="Filter"/>
            <c:if test="${not empty historyForm.agentName}">
              &nbsp;&nbsp;<forms:resetFilter resetHandler="BS.HistoryTable.update('agentName=')"/>
            </c:if>
            <script type="text/javascript">
              $('agentName').focus();
            </script>
          </div>
         </td>
         <td class="column2">
          <div id="savingFilter">
            <forms:progressRing className="progressRingInline"/>
            Updating results...
          </div>
         </td>
        <td class="column3">
          <forms:checkbox name="showAllBuilds" checked="${historyForm.showAllBuilds}" onclick="BS.HistoryTable.doSearch()"/>
          <label for="showAllBuilds">Show canceled builds and builds that failed to start</label>
          <span class="separator">|</span><bs:clickPopup showPopupCommand="BS.JumpTo.showPopupNearElement(this)" controlId="jumpToControl">Jump to&hellip;</bs:clickPopup>
        </td>
      </tr>
    </table>
      <input type="hidden" name="tab" value="buildTypeHistoryList"/>
      <input type="hidden" name="buildTypeId" value="${buildType.buildTypeId}"/>
      <input type="hidden" name="tag" value="${historyForm.tag}"/>
    </form>

    <%--@elvariable id="branchBean" type="jetbrains.buildServer.controllers.BranchBean"--%>
    <bs:historyTable historyRecords="${historyRecords}"
                     highlightRecord="${jumpedTo}"
                     buildType="${buildType}"
                     hasBranches="${branchBean.hasBranches}"
                     showTrivialColumnNames="${true}"/>

    <c:set var="pagerUrlPattern" value="viewType.html?buildTypeId=${buildType.buildTypeId}&page=[page]&tab=buildTypeHistoryList"/>
    <c:if test="${not empty branchBean}">
      <c:set var="pagerUrlPattern" value="${pagerUrlPattern}&branch_${buildType.projectId}=${branchBean.userBranchEscaped}"/>
    </c:if>
    <bs:pager place="bottom" urlPattern="${pagerUrlPattern}" pager="${historyPager}"/>
  </bs:refreshable>
</div>
<script type="text/javascript">
  BS.Branch.baseUrl = "${history_url}";
</script>
