<%@ include file="include-internal.jsp" %>
<c:set var="title" value="Set up Administrator Account"/>
<jsp:useBean id="loginDescription" beanName="loginDescription" scope="request" type="java.lang.String"/>
<jsp:useBean id="publicKey" beanName="publicKey" scope="request" type="java.lang.String"/>
<bs:externalPage>
  <jsp:attribute name="page_title">${title}</jsp:attribute>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/forms.css
      /css/initialPages.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/crypt/rsa.js
      /js/crypt/jsbn.js
      /js/crypt/prng4.js
      /js/crypt/rng.js
      /js/bs/forms.js
      /js/bs/encrypt.js
      /js/bs/setupAdmin.js
    </bs:linkScript>
    <script type="text/javascript">
      $j(document).ready(function($) {
        $("#username1").focus();
      });
    </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <div id="setupAdminPage" class="initialPage">
      <span class="logo"><img src="img/logoLogin.gif" alt="TeamCity" width="61" height="102"/></span>

      <div id="pageContent">
        <h1>${title}</h1>
        <bs:version/>
        <form action="<c:url value='/setupAdminSubmit.html'/>" onsubmit="return BS.SetupAdminForm.submitSetupAdmin(this);" method="post">


          <div id="errorMessage"></div>
          <c:if test="${fn:length(loginDescription) > 0}">
            <div id="loginDescription">
              <c:out value="${loginDescription}"/>
            </div>
          </c:if>

          <table>
            <tr class="formField">
              <th><label for="username1">* Username:</label></th>
              <td><input class="text" id="username1" type="text" name="username1" style="width:15em;" maxlength="256"></td>
            </tr>
            <tr class="formField">
              <th><label for="password1">* Password:</label></th>
              <td><input class="text" id="password1" type="password" name="password1" style="width:15em;" maxlength="80"></td>
            </tr>
            <tr>
              <th><forms:saving style="padding-top: 0.6em"/></th>
              <td><input class="btn" type="submit" id="submitButton" value="Set as Administrator"/></td>
            </tr>
          </table>

          <input type="hidden" id="submitSetupAdmin" name="submitSetupAdmin" value=""/>
          <input type="hidden" id="publicKey" name="publicKey" value="${publicKey}"/>

        </form>
      </div>
    </div>
  </jsp:attribute>
</bs:externalPage>
