<%@ include file="/include-internal.jsp"
%><jsp:useBean id="serverTC" type="jetbrains.buildServer.serverSide.SBuildServer" scope="request"
/><c:set var="buildTypes" value="${overviewBean.projectsModel}"
/><c:set var="projectStatusDetails" value="${overviewBean.projectStatusDetails}"
/><c:set var="hiddenBuildTypes" value="${overviewBean.hiddenBuildTypes}"/>
<div id="buildTypes">
  <c:if test="${empty buildTypes}">
    <c:if test="${empty serverTC.projectManager.activeProjects}">
      <authz:authorize allPermissions="CREATE_CLONE_PROJECT">
        <a class="createProject" href="<c:url value='/admin/createProject.html?init=1'/>">Create a project</a>
        To start running builds, create projects and build configurations first.
      </authz:authorize>
      <authz:authorize anyPermission="CHANGE_SERVER_SETTINGS, MANAGE_SERVER_LICENSES, CREATE_USER">
      <p style="margin:2em 0 0 0;">You may also want to:</p>
      <ul class="startPage">
        <authz:authorize allPermissions="CHANGE_SERVER_SETTINGS">
        <li><a href="<c:url value='/admin/admin.html?item=email'/>">configure email and Jabber settings</a> to enable notifications,</li>
        </authz:authorize>
        <authz:authorize allPermissions="MANAGE_SERVER_LICENSES">
        <li><a href="<c:url value='/admin/admin.html?item=license'/>">manage licenses</a>, and</li>
        </authz:authorize>
        <authz:authorize allPermissions="CREATE_USER">
        <li><a href="<c:url value='/admin/admin.html?item=users'/>">add more users to TeamCity</a>.</li>
        </authz:authorize>
      </ul>
      </authz:authorize>
    </c:if>

    <authz:authorize anyPermission="CHANGE_SERVER_SETTINGS, MANAGE_SERVER_LICENSES, CREATE_USER, CREATE_CLONE_PROJECT">
      <jsp:attribute name="ifAccessGranted">
        <c:set var="noProjectsReason" value="${overviewBean.overviewIsEmptyReason}"/>
        <c:choose>
          <c:when test="${noProjectsReason == 'NO_BUILD_TYPES_IN_SYSTEM'}">
            <p>No build configurations have been created in your project. Please create build configurations in the <a href="<c:url value='/admin/admin.html'/>">Administration area</a> (also see link at the top navigation bar)</p>
          </c:when>
          <c:when test="${noProjectsReason == 'NO_VISIBLE_PROJECTS'}">
            <p>
              There are no projects to show.
              <authz:authorize allPermissions="CHANGE_OWN_PROFILE">
                Please <a href="#" onclick="BS.VisibleProjectsDialog.show(); return false">configure visible projects</a>.
              </authz:authorize>
            </p>
          </c:when>
          <c:when test="${noProjectsReason == 'SUCCESSFUL_HIDDEN'}">
              <%@ include file="_someSuccessfulHiddenNote.jspf" %>
          </c:when>
          <c:otherwise>
            <p style="margin:2em 0 0 0;">There are no projects to show. Possible reasons:</p>
            <ul class="startPage">
              <li>You should <a href="#" onclick="BS.VisibleProjectsDialog.show(); return false">configure visible projects</a></li>
              <li>There are no projects/build configurations yet. Please, <a href="<c:url value='/admin/admin.html'/>">create them</a>.</li>
            </ul>
          </c:otherwise>
        </c:choose>
      </jsp:attribute>
      <jsp:attribute name="ifAccessDenied">
        <c:set var="noProjectsReason" value="${overviewBean.overviewIsEmptyReason}"/>
        <c:choose>
          <c:when test="${noProjectsReason == 'NO_PROJECTS_IN_SYSTEM'}">
            <p>There are no projects in TeamCity. Please contact your system administrator.</p>
          </c:when>
          <c:when test="${noProjectsReason == 'NO_BUILD_TYPES_IN_SYSTEM'}">
            <p>There are no build configurations in TeamCity. Please contact your system administrator.</p>
          </c:when>
          <c:when test="${noProjectsReason == 'NO_ACCESSIBLE_PROJECTS'}">
            <p>There are no projects to show, because you don't have permissions to view projects. Please contact your system administrator.</p>
          </c:when>
          <c:when test="${noProjectsReason == 'NO_VISIBLE_PROJECTS'}">
            <p>
              There are no projects to show.
              <authz:authorize allPermissions="CHANGE_OWN_PROFILE">
                Please <a href="#" onclick="BS.VisibleProjectsDialog.show(); return false">configure visible projects</a>.
              </authz:authorize>
            </p>
          </c:when>
          <c:when test="${noProjectsReason == 'SUCCESSFUL_HIDDEN'}">
              <%@ include file="_someSuccessfulHiddenNote.jspf" %>
          </c:when>
            <c:otherwise>
            <p style="margin:2em 0 0 0;">There are no projects to show. Possible reasons:</p>
            <ul class="startPage">
              <authz:authorize allPermissions="CHANGE_OWN_PROFILE">
                <li>You should <a href="#" onclick="BS.VisibleProjectsDialog.show(); return false">configure visible projects</a></li>
              </authz:authorize>
              <li>You do not have enough permissions to view projects</li>
              <li>TeamCity server administrators have not configured any projects</li>
            </ul>
          </c:otherwise>
        </c:choose>
      </jsp:attribute>
    </authz:authorize>
  </c:if
  ><bs:messages key="buildNotFound"
 /><bs:messages key="buildTypeNotFound"
 /><bs:messages key="changeNotFound"
 /><bs:messages key="visibleProjectsSaved"
 /><div class="clr"></div>

<c:set var="runningBuilds" value="${overviewBean.runningBuildsModel}" scope="request"
/><c:set var="problemsSummary" value="${overviewBean.problemsSummary.countersMap}" scope="request"
/><c:if test="${not empty buildTypes}"
    ><c:url var="url" value="/overview.html"
   /><bs:refreshable containerId="overviewMain" pageUrl="${url}"
    ><c:forEach var="item" items="${buildTypes}" varStatus="status"
      ><c:set var="project" value="${item.key}" scope="request"
     /><c:set var="projectBuildTypes" value="${item.value}" scope="request"
     /><c:set var="projectStatusDetails" value="${projectStatusDetails[project]}" scope="request"
     /><c:set var="hiddenBuildTypes" value="${hiddenBuildTypes[project]}" scope="request"
     /><c:set var="firstProjectInList" value="${status.first}" scope="request"
     /><c:set var="branchBean" value="${overviewBean.branchBeans[project]}" scope="request"
     /><jsp:include page="/projectBuildTypes.jsp"
    /></c:forEach
  ></bs:refreshable
></c:if
></div>