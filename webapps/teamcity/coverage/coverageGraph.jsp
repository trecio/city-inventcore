<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="stats" tagdir="/WEB-INF/tags/graph" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<bs:forBuildTypeOrItems items="${coverageBuildTypes}">
  <stats:buildGraph id="CodeCoverage${buildTypeId}" valueType="CodeCoverage" height="110" hideFilters="averaged" defaultFilter="showFailed"/>
</bs:forBuildTypeOrItems>
