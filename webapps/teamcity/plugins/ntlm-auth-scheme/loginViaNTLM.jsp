<%@ page import="jetbrains.buildServer.controllers.login.NTLMLoginController" %>
<%@ include file="/include-internal.jsp"%>
<c:set var="ntlmPath"><%=NTLMLoginController.PATH%></c:set>
<div><a href="<c:url value='${ntlmPath}'/>">Login using NT domain account</a></div>