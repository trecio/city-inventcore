<%@ include file="/include-internal.jsp" %>
<c:if test="${not empty buildType}">
<jsp:useBean id="modification" scope="request" type="jetbrains.buildServer.vcs.SVcsModification"/>
<jsp:useBean id="buildType" scope="request" type="jetbrains.buildServer.serverSide.SBuildType"/>

<c:if test="${not empty buildType and not modification.personal and afn:permissionGrantedForBuildType(buildType, 'RUN_BUILD') and afn:permissionGrantedForBuildType(buildType, 'CUSTOMIZE_BUILD_REVISIONS')}">
  <c:set var="branch"><c:if test="${not empty branchName}">, branchName: '<bs:escapeForJs forHTMLAttribute="true" text="${branchName}"/>'</c:if></c:set>
  <a class="noUnderline" href="#" onclick="BS.RunBuild.runCustomBuild('${buildType.buildTypeId}', { modificationId: ${modification.id}${branch}, init: true, stateKey: 'history' }); return false" title="Run build with this change...">
    <img style="vertical-align:middle;" src="<c:url value='/img/runBuildChanges.png'/>">
  </a>
</c:if>
</c:if>