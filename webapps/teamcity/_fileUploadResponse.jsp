<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
  <c:choose>
    <c:when test="${not empty error}">
      parent.${jsBase}.error("${error}");
    </c:when>
    <c:otherwise>
      parent.${jsBase}.closeAndRefresh();
    </c:otherwise>
  </c:choose>
</script>
