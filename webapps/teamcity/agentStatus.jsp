<%@ include file="include-internal.jsp" %>
<jsp:useBean id="agentDetails" type="jetbrains.buildServer.controllers.agent.AgentDetailsForm" scope="request"/>
<c:set var="agent" value="${agentDetails.agent}"/>
<c:set var="agentType" value="${agentDetails.agentType}"/>
<c:set var="pattern" value="dd MMM HH:mm"/>
<bs:refreshable containerId="agentStatus:${agent.id}" pageUrl="${pageUrl}">
<c:choose>

  <c:when test="${'1' == param['tableMode']}"> <!-- Agent list: Enable/Disable link  -->

    <c:set var="action" value="" />
    <authz:authorize allPermissions="ENABLE_DISABLE_AGENT">
      <c:set var="action">
       class="actionLink" href="#"
       onclick="BS.Agent.showChangeStatusDialog(${!agent.enabled}, ${agent.id}, ${agent.registered}, 'changeAgentStatus'); return false"
      </c:set>
    </authz:authorize>
    <bs:agentStatusInfo trueText="Enabled" falseText="Disabled"
                        trueCssClass="enabled" falseCssClass="disabled"
                        comment="${agent.statusComment}" state="${agent.enabled}" action="${action}"/>

  </c:when>


  <c:when test="${'2' == param['tableMode']}"> <!-- Agent list: Unauthorize link  -->

    <c:set var="action"></c:set>
    <authz:authorize allPermissions="AUTHORIZE_AGENT">
      <c:if test="${agent.authorized or (not agent.authorized and agentDetails.canBeAuthorized)}">
        <c:set var="poolOptions" value="{poolId: ${agentType.agentPoolId}, cloud: ${agentType.details.cloudAgent ? 1 : 0}}"/>
        <c:set var="action">
          class="actionLink" href="#"
          onclick="BS.Agent.showChangeStatusDialog(${not agent.authorized}, ${agent.id}, ${agent.authorized}, 'changeAuthorizeStatus', ${poolOptions}); return false"
        </c:set>
      </c:if>
    </authz:authorize>
    <bs:agentStatusInfo trueText="Authorized" falseText="Unauthorized"
                        trueCssClass="authorized" falseCssClass="unauthorized"
                        comment="${agent.authorizeComment}" state="${agent.authorized}" action="${action}"/>
  </c:when>


  <c:otherwise> <!-- This block is used when shown on the agent page itself -->
    <div>
    <bs:agentStatusInfo2 trueText="Authorized" falseText="Unauthorized"
                         trueCssClass="authorized" falseCssClass="unauthorized"
                         comment="${agent.authorizeComment}" state="${agent.authorized}"/>
    <c:if test="${agent.authorized or (not agent.authorized and agentDetails.canBeAuthorized)}">
      &nbsp;&nbsp;&nbsp;

      <authz:authorize allPermissions="AUTHORIZE_AGENT">
        <c:set var="poolOptions" value="{cloud: ${agentType.details.cloudAgent ? 1 : 0}}"/>
        <a class="actionLink" href="#"
           onclick="BS.Agent.showChangeStatusDialog(${not agent.authorized}, ${agent.id}, ${agent.authorized}, 'changeAuthorizeStatus', ${poolOptions}); return false"
          >${(agent.authorized)?"Unauthorize":"Authorize"} agent</a>
      </authz:authorize>

    </c:if>
    </div>
    <div>
    <bs:agentStatusInfo2 trueText="Enabled" falseText="Disabled"
                         trueCssClass="enabled" falseCssClass="disabled"
                         comment="${agent.statusComment}" state="${agent.enabled}"/>
     &nbsp;&nbsp;&nbsp;

      <authz:authorize allPermissions="ENABLE_DISABLE_AGENT">
        <a class="actionLink" href="#" onclick="BS.Agent.showChangeStatusDialog(${not agent.enabled}, ${agent.id}, ${agent.registered}, 'changeAgentStatus'); return false"
          > ${(agent.enabled)?"Disable":"Enable"} agent</a>
      </authz:authorize>

      &nbsp;&nbsp;&nbsp;
      <c:if test="${agent.agentStatusToRestore != null}">
          (Will be automatically <c:choose><c:when test="${agent.agentStatusToRestore}">enabled</c:when><c:otherwise>disabled</c:otherwise></c:choose> at <strong><bs:date value="${agent.agentStatusRestoringTimestamp}"/></strong>)
      </c:if>

    </div>
  </c:otherwise>
</c:choose>
</bs:refreshable>