<%@ page
  %><%@include file="/include-internal.jsp"
  %>

<jsp:useBean id="project" scope="request" type="jetbrains.buildServer.serverSide.SProject"/>
<c:set var="pagerUrlPattern" scope="request" value="project.html?projectId=${project.projectId}&buildTypeId=${empty buildType ? '' : buildType.buildTypeId}&page=[page]&tab=tests"/>
<jsp:include page="/tests/recentlyFailedTests.jsp"/>
