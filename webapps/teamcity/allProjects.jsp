<%@ include file="include-internal.jsp" %>

<c:set var="linkTitleText" value="Click to open the build configuration page"/>
<c:set var="allProjectsVisible" value="${fn:length(allProjects) eq fn:length(visibleProjects)}"></c:set>

<div class="projectsContainer">
  <c:if test="${empty allProjects}">
    There are no projects or build configurations.
  </c:if>

  <c:if test="${not empty allProjects}">
    <bs:inplaceFilter containerId="allProjectsPopupTable" activate="true" afterApplyFunc="function(filterField) {BS.AllProjectsPopup.searchBuildTypes(filterField); BS.AllProjectsPopup.globalInstance.updatePopup()}" filterText="&lt;filter projects and build configurations>"/>
    <l:tableWithHighlighting highlightImmediately="true" id="allProjectsPopupTable" className="projectsPopupTable">
      <c:if test="${not empty visibleProjects}">
        <tbody>
        <c:forEach items="${visibleProjects}" var="project">
          <c:set var="projectId" value="${project.projectId}"/>
          <tr class="inplaceFiltered inplaceFiltered_${projectId}<c:if test="${!allProjectsVisible}"> visibleProject</c:if>" data="${projectId}" data-title="${project.name}">
            <td class="projectName highlight" title="<c:out value='${project.description}'/>">
              <a
                href="<bs:projectUrl projectId="${projectId}"/>"
                class="projectLink"
                title="Open project page"
                showdiscardchangesmessage="false"><c:out value="${project.name}"/></a>
            </td>
            <td class="highlight pin">
              <c:if test="${not empty project.buildTypes}">
                <authz:authorize allPermissions="CHANGE_OWN_PROFILE">
                  <a href="#" onclick="return BS.AllProjectsPopup.toggleOverview(this, '${projectId}');"><img class="pinLinkImage" src="<c:url value="/img/watched.png"/>" alt="" title="Project is shown on the overview page. Click to hide"/></a>
                </authz:authorize>
              </c:if>
            </td>
            <td class="highlight bt">
              <c:if test="${not empty project.buildTypes}">
                <bs:popupControl showPopupCommand="" hidePopupCommand="" stopHidingPopupCommand=""
                                 controlId="buildTypesPopup${projectId}"
                                 imageSrc="/img/popUpControl_right.png"><span class="btCount">${fn:length(project.buildTypes)}</span></bs:popupControl>
              </c:if>
            </td>
          </tr>
        </c:forEach>
        </tbody>
      </c:if>

      <c:if test="${not empty invisibleProjects}">
        <tbody>
        <c:forEach items="${invisibleProjects}" var="project">
          <c:set var="projectId" value="${project.projectId}"/>
          <tr class="inplaceFiltered inplaceFiltered_${projectId}" data="${projectId}" data-title="${project.name}">
            <td class="projectName highlight" title="<c:out value='${project.description}'/>">
              <a
                 href="<bs:projectUrl projectId="${project.projectId}"/>"
                 class="projectLink"
                 title="Open project page"
                 showdiscardchangesmessage="false"><c:out value="${project.name}"/></a>
            </td>
            <td class="highlight pin">
              <c:if test="${not empty project.buildTypes}">
                <authz:authorize allPermissions="CHANGE_OWN_PROFILE">
                  <a class="pinLink" href="#" onclick="return BS.AllProjectsPopup.toggleOverview(this, '${projectId}');"><img class="pinLinkImage pinLink" src="<c:url value="/img/watch.png"/>" title="Click to show project on the overview page"/></a>
                </authz:authorize>
              </c:if>
            </td>
            <td class="highlight bt">
              <c:if test="${not empty project.buildTypes}">
                <bs:popupControl showPopupCommand="" hidePopupCommand="" stopHidingPopupCommand=""
                                 controlId="buildTypesPopup${projectId}"
                                 imageSrc="/img/popUpControl_right.png"><span class="btCount">${fn:length(project.buildTypes)}</span></bs:popupControl>
              </c:if>
            </td>
          </tr>
        </c:forEach>
        </tbody>
      </c:if>
    </l:tableWithHighlighting>

    <script type="text/javascript">
      (function() {
        var tmp;
        <c:if test="${not empty visibleProjects}">
        <c:forEach items="${visibleProjects}" var="project">
        BS.projectMap['${project.projectId}'] = [];
        tmp = BS.projectMap['${project.projectId}'];
        <c:forEach items="${project.buildTypes}" var="buildType">
        tmp.push({id: '${buildType.buildTypeId}', projectName: '<bs:escapeForJs text="${project.name}" forHTMLAttribute="true"/>', name: '<bs:escapeForJs text="${buildType.name}" forHTMLAttribute="true"/>', description: '<bs:escapeForJs text="${buildType.description}" forHTMLAttribute="true"/>'});</c:forEach>
        </c:forEach>
        </c:if>

        <c:if test="${not empty invisibleProjects}">
        <c:forEach items="${invisibleProjects}" var="project">
        BS.projectMap['${project.projectId}'] = [];
        tmp = BS.projectMap['${project.projectId}'];
        <c:forEach items="${project.buildTypes}" var="buildType">tmp.push({id: '${buildType.buildTypeId}', projectName: '<bs:escapeForJs text="${project.name}" forHTMLAttribute="true"/>', name: '<bs:escapeForJs text="${buildType.name}" forHTMLAttribute="true"/>', description: '<bs:escapeForJs text="${buildType.description}" forHTMLAttribute="true"/>'});</c:forEach>
        </c:forEach>
        </c:if>

        BS.AllProjectsPopup.Navigation.init();
      })();
    </script>
  </c:if>

  <c:set var="controlId" value="archivedProjects"/>
  <c:url value="/allProjects.html" var="popupUrl">
    <c:param name="jsp" value="allArchivedProjects.jsp"/>
  </c:url>
  <c:set var="popup_options" value="shift: {x: 30, y: -5}, url: '${popupUrl}'"/>

  <c:if test="${not empty archivedProjects}">
    <%-- Unfortunately we can't use SimplePopup here, because the content DIV must be
         outside of 'allProjectsPopup' div. So we emulate it here. --%>
    <script type="text/javascript">
      BS.${controlId}_handle = new BS.Popup("${controlId}Content",{${popup_options}});
    </script>
    <div class="archivedPopup">
      <bs:popupControl showPopupCommand="BS.${controlId}_handle.showPopupNearElement(this)"
                       hidePopupCommand="BS.${controlId}_handle.hidePopup(300)"
                       stopHidingPopupCommand="BS.${controlId}_handle.stopHidingPopup()"
                       controlId="${controlId}">
        and ${fn:length(archivedProjects)}
        <c:if test="${afn:adminSpaceAvailable()}">
          <a title="View archived projects list"
             href="<c:url value="/admin/admin.html?tab=archived"/>">
            archived project<bs:s val="${fn:length(archivedProjects)}"/>
          </a>
        </c:if>
        <c:if test="${not afn:adminSpaceAvailable()}">
            archived project<bs:s val="${fn:length(archivedProjects)}"/>
        </c:if>

      </bs:popupControl>
    </div>
  </c:if>
</div>
