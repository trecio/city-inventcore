<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><c:if test="${showVcsTreeIcon}"
><script type="text/javascript">
  BS.VCS.registerTree('${vcsTreeId}', '${buildFormId}', '${callback}', '${fieldId}', 'vcsTreeControl_${vcsTreeId}');
</script>
<img id="vcsTreeControl_${vcsTreeId}" src="${serverPath}<c:url value="/img/tree/popup-artifacts-tree.png"/>" class="handle vcsTreeHandle" onclick="BS.VCS.showTree('${vcsTreeId}')" title="Choose file or directory in VCS"/>
</c:if>
