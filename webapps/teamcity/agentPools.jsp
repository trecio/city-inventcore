<%@ page import="jetbrains.buildServer.controllers.agent.AgentPoolController" %>
<%@ page import="jetbrains.buildServer.serverSide.agentPools.AgentPoolConstants" %>
<%@ include file="include-internal.jsp"%>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<jsp:useBean id="data" scope="request" type="jetbrains.buildServer.controllers.agent.PoolsData"/>

<!--[if IE 7]>
<style type="text/css">
  div.item-rect {
    display: inline;
    zoom: 1;
  }
</style>
<![endif]-->

<div class="pool-width">
  <bs:messages key="<%=AgentPoolController.AGENT_POOL_ACTION_MESSAGE%>"/>
</div>

<c:url value='/agentPools.html' var="actionUrl"/>

<div class="pool-width">


  <authz:authorize allPermissions="MANAGE_AGENT_POOLS">
    <div>
      <forms:addButton onclick="BS.AP.confirmCreateNewPool(); return false;">Create new pool</forms:addButton>
    </div>
  </authz:authorize>

  <div id="pool-boxes-container">
    <c:set var="defaultPoolAdmin" value="${data.defaultPoolAdmin}"/>

    <c:forEach items="${data.pools}" var="pool">

      <c:set var="poolId" value="${pool.agentPool.agentPoolId}"/>
      <c:set var="poolName"><c:out value='${pool.agentPool.name}'/></c:set>
      <c:set var="poolNameForJS"><bs:escapeForJs text="${pool.agentPool.name}" forHTMLAttribute="${true}"/></c:set>

      <a name="${poolId}"></a>
      <div id="pool-${poolId}" class="pool-box">

        <div class="head-row clearfix">
          <span class="pool-name-title"><span class="pool-name">${poolName}</span> pool</span>
          <c:set var="numProjects" value="${fn:length(pool.projects) + pool.hiddenProjectCount}"/>
          <span class="pool-numbers">${fn:length(pool.agents)} agent<bs:s val="${fn:length(pool.agents)}"/>, ${numProjects} project<bs:s val="${numProjects}"/></span>
          <authz:authorize allPermissions="MANAGE_AGENT_POOLS">
            <c:if test="${poolId ne 0}">
              <div class="pool-delete"><a href="#" onclick="BS.AP.confirmRemovePool(${poolId}); return false">Delete</a></div>
              <div class="pool-rename"><a href="#" onclick="BS.AP.confirmRenamePool(${poolId}); return false">Rename</a></div>
            </c:if>
          </authz:authorize>
        </div>

        <table class="pool-layout-table" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="67%" class="pool-layout-content agents">

              <c:choose>
                <c:when test="${pool.admin}">
                  <c:if test="${pool.canAddMoreAgents}">
                    <div class="head-row">
                      <forms:addButton onclick="BS.AP.clickOnAddAgentType(${poolId}, this); return false">Assign agents...</forms:addButton>
                    </div>
                  </c:if>
                </c:when>
                <c:otherwise>
                  <c:if test="${empty pool.agents}">
                    <div class="head-row">No agents</div>
                  </c:if>
                </c:otherwise>
              </c:choose>

              <table width="100%">

                <c:forEach items="${pool.agents}" var="agt">

                  <c:set var="agtId" value="${agt.id}"/>
                  <c:set var="crossId" value="pool_${poolId}_agt_${agtId}"/>
                  <c:set var="agtName"><c:out value='${agt.name}'/></c:set>
                  <c:set var="agtNameForJS"><bs:escapeForJs text="${agt.name}" forHTMLAttribute="${true}"/></c:set>

                  <tr class="agt-row">
                    <td width="1%" valign="center" style="padding-left: 7px;">
                      <bs:osIcon osName="${agt.OSName}"/>
                    </td>
                    <td class="agentNameRow" valign="center">
                      <bs:agentDetailsLink agentName="${agt.name}" agentTypeId="${agtId}"/>
                    </td>
                    <td width="5%" valign="center">
                      <c:if test="${agt.cloudAgent}">
                        <img src="<c:url value='/img/cloud.png'/>"
                             <%-- title="<c:out value='${agt.cloudNote}'/>" --%>
                             title="Cloud agent image"
                             alt="Cloud"/>
                      </c:if>
                    </td>
                    <td width="15%" valign="center">
                      <span class="agent-note">
                        <c:if test="${not agt.cloudAgent}">
                          <c:set var="instance1" value="${agt.firstInstance}"/>
                          <c:if test="${instance1 ne null}">
                            <c:if test="${instance1.authorized and instance1.registered and not instance1.enabled}">
                              <span class="agent-bad-status" title="${instance1.statusComment.comment}">Disabled</span>
                            </c:if>
                            <c:if test="${instance1.authorized and not instance1.registered}">
                              <span class="agent-bad-status" title="<bs:disconnectedStatusTitle agent='${instance1}'/>">Disconnected</span>
                            </c:if>
                            <c:if test="${not instance1.authorized}">
                              <span class="agent-bad-status" title="${instance1.authorizeComment.comment}">Not authorized</span>
                            </c:if>
                          </c:if>
                        </c:if>
                        <c:if test="${agt.cloudAgent}">
                          <c:if test="${not empty(agt.instances)}">
                            <c:set var="agtInstancesCount" value="${fn:length(agt.instances)}"/>
                            <b>${agtInstancesCount}</b> agent<bs:s val="${agtInstancesCount}"/>
                          </c:if>
                        </c:if>
                      </span>
                    </td>
                    <td width="23%" valign="center">
                      <span class="agent-note">
                        <c:if test="${agt.incompatibleWarning or agt.willRunNothingWarning}">
                          <c:if test="${agt.incompatibleWarning}">
                            <c:set var="warningShortcut" value="Incompatible"/>
                            <c:set var="warningNote" value="This build agent is not compatible with any of build configurations from projects assigned to this pool."/>
                          </c:if>
                          <c:if test="${agt.willRunNothingWarning and not agt.incompatibleWarning}">
                            <c:set var="warningShortcut" value="Incompatible"/>
                            <c:set var="warningNote" value="Run policy of this agent doesn't allow to run build configurations from projects associated with this pool."/>
                          </c:if>
                          <img src="<c:url value='/img/buildStates/redSign.gif'/>"
                               alt="!"
                               title="${warningNote}"
                               style="vertical-align: text-top;"/>
                          <a href="agentDetails.html?agentTypeId=${agtId}&tab=agentCompatibleConfigurations"
                             title="${warningNote}"><span title="${warningNote}">${warningShortcut}</span>
                          </a>
                        </c:if>
                      </span>
                    </td>
                    <td width="2%" valign="center" align="right">
                      <c:choose>
                        <c:when test="${pool.admin && !pool.defaultPool}">
                          <a href="#"
                             id="drop-agt-${agtId}-cross"
                             class="gray agt-remove-link"
                             onclick="{BS.AP.clickOnDropAgentCross(${agtId}, ${!defaultPoolAdmin}, '${agtNameForJS}'); return false;}"
                             title="Move ${agtName} to Default pool"
                            >X</a>
                          <forms:saving id="drop-agt-${agtId}-progress"/>
                        </c:when>
                        <c:otherwise>&nbsp;</c:otherwise>
                      </c:choose>
                    </td>
                  </tr>

                </c:forEach>

              </table>

            </td>
            <td width="3%" class="spacegap"></td>
            <td width="30%" class="pool-layout-content projects">

              <c:if test="${pool.admin}">
                <c:if test="${pool.canAddMoreProjects}">
                  <div class="head-row">
                    <forms:addButton onclick="BS.AP.clickOnAssociateProjects(${poolId}, this); return false">Assign projects...</forms:addButton>
                  </div>
                </c:if>
              </c:if>
              <c:if test="${not pool.admin}">
                <c:if test="${empty pool.projects and pool.hiddenProjectCount == 0}">
                  <div class="head-row">No projects</div>
                </c:if>
              </c:if>

              <c:if test="${not empty pool.projects}">
                <table width="100%">
                  <c:forEach items="${pool.projects}" var="poolProject">

                    <c:set var="project" value="${poolProject.project}"/>
                    <c:set var="projectId"><c:out value='${project.projectId}'/></c:set>
                    <c:set var="projectName"><c:out value='${project.name}'/></c:set>
                    <c:set var="projectNameForJS"><bs:escapeForJs text="${project.name}" forHTMLAttribute="${true}"/></c:set>
                    <c:set var="associatedPools" value="${poolProject.associatedPools}"/>
                    <c:set var="associatedPoolsCount" value="${fn:length(associatedPools)}"/>
                    <c:set var="crossId" value="drop-pro-${projectId}-from-pool-${poolId}-cross"/>

                    <tr class="project-row">
                      <td>
                        <a href="project.html?projectId=${projectId}" id="pool-${poolId}-project-${projectId}" class="project-name"><bs:trim maxlength="40">${project.name}</bs:trim></a><%-- do not use c:out inside bs:trim --%>
                        &nbsp;
                        <c:if test="${associatedPoolsCount == 2}">
                          <span class="another-pools-span">also in <strong><bs:_otherPoolsList pools="${associatedPools}" poolId="${poolId}"/></strong> pool</span>
                        </c:if>
                        <c:if test="${associatedPoolsCount > 2}">
                          <span class="another-pools-span">also in <bs:simplePopup controlId="other-pools-popup-${crossId}" linkOpensPopup="true" popup_options="delay: 0, hideDelay: 0, shift: {x: 0, y: 20}"
                            ><jsp:attribute name="content"><bs:_otherPoolsList pools="${associatedPools}" poolId="${poolId}"/></jsp:attribute
                            ><jsp:body>${associatedPoolsCount - 1} other pools</jsp:body
                          ></bs:simplePopup></span>
                        </c:if>
                      </td>
                      <td align="right">
                        <c:if test="${poolProject.canBeDissociated}">
                          <a href="#"
                             id="${crossId}"
                             class="gray project-remove-link"
                             onclick="{BS.AP.clickOnDropProjectCross(${poolId}, '${projectId}', '${crossId}', ${pool.showLastProjectWarning}, '${poolNameForJS}', '${projectNameForJS}'); return false;}"
                             title="Dissociate '${projectName}' from pool ${poolName}"
                            >X</a>
                          <forms:saving id="${crossId}-progress"/>
                        </c:if>
                        <c:if test="${not poolProject.canBeDissociated}">
                          &nbsp;
                        </c:if>
                      </td>
                    </tr>

                  </c:forEach>
                </table>
              </c:if>

              <c:if test="${pool.hiddenProjectCount != 0}">
                <c:if test="${not empty pool.projects}"><br/></c:if>
                <div class="attentionComment">There <bs:are_is val="${pool.hiddenProjectCount}"/> ${pool.hiddenProjectCount}<c:if test="${not empty pool.projects}"> more</c:if> project<bs:s val="${pool.hiddenProjectCount}"/> you do not have permissions to see.</div>
              </c:if>

            </td>
          </tr>
        </table>

      </div>

    </c:forEach>

  </div>

</div>

<c:set var="poolNameMaxLength"><%=AgentPoolConstants.MAX_POOL_NAME_LENGTH%></c:set>

<authz:authorize allPermissions="MANAGE_AGENT_POOLS">

  <script type="text/javascript">

    BS.AP.init();

  </script>

  <bs:dialog dialogId="PoolNameDialog"
             title=""
             closeCommand="BS.PoolNameDialog.close();"
             titleId="PoolNameDialogTitle">

    <div>
      <label style="margin-right: 1em"
             for="PoolNameDialogInputField">Pool name:</label>
      <input type="text"
             style="width: 98%"
             maxlength="${poolNameMaxLength}"
             id="PoolNameDialogInputField"
             value=""/>
    </div>

    <div class="popupSaveButtonsBlock">
      <forms:cancel onclick="BS.PoolNameDialog.close()" />
      <forms:submit
             id="PoolNameDialogSubmitButton"
             type="button"
             label="Save"
             onclick="BS.PoolNameDialog.doIt()"/>
      <forms:saving id="agentPoolNameProgress"/>
    </div>
  </bs:dialog>

  <bs:dialog dialogId="RemovePoolDialog"
             title="Delete Agent Pool"
             closeCommand="BS.RemovePoolDialog.close();"
             titleId="RemovePoolDialogTitle">

    <div>
      <p>
        You're about to delete pool <b><span id="remove-pool-name"> </span></b>.
      </p>
      <p id="remove-agents-message">
        This pool contains <b><span id="remove-agents-number">0</span></b> agent<span id="remove-agents-singular">. It will be moved to the Default pool.</span><span id="remove-agents-plural">s. All of them will be moved to the Default pool.</span>
      </p>
      <p id="remove-projects-message">
        <b><span id="remove-projects-number">0</span></b> project<span id="remove-projects-singular"> is associated with this pool. It will be dissociated.</span><span id="remove-projects-plural">s are associated with this pool. All of them will be dissociated.</span>
      </p>
    </div>

    <div class="popupSaveButtonsBlock">
      <forms:cancel onclick="BS.RemovePoolDialog.close()"/>
      <forms:submit
             id="RemovePoolDialogSubmitButton"
             type="button"
             label="Delete Pool"
             onclick="BS.RemovePoolDialog.doIt()"/>
      <forms:saving id="agentPoolRemoveProgress"/>
    </div>
  </bs:dialog>

</authz:authorize>

<bs:dialog dialogId="DissociateLastProjectDialog"
           title="Dissociate last project from the pool"
           closeCommand="BS.DissociateLastProjectDialog.close();"
           titleId="DissociateLastProjectDialogTitle">

  <div>
    <p>
      You're about to dissociate project <b><span id="dissociate-last-project-name"> </span></b> from pool <b><span id="dissociate-last-project-from-pool-name"> </span></b>.
    </p>
    <p>
      This is the only project that is associated with this pool. If you dissociate it you become unable to associate it back without system administrator. Are you sure you want to continue?
    </p>
  </div>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.DissociateLastProjectDialog.close()"/>
    <forms:submit
        type="button"
        label="Dissociate project"
        onclick="BS.DissociateLastProjectDialog.doIt()"/>
  </div>
</bs:dialog>

<bs:dialog dialogId="MoveAgentToDefaultPoolDialog"
           title="Move agent to Default pool"
           closeCommand="BS.MoveAgentToDefaultPoolDialog.close();"
           titleId="MoveAgentToDefaultPoolDialogTitle">

  <div>
    <p>
      You're about to move agent <b><span id="move-agent-to-default-pool-name"> </span></b> to <b><span>Default</span></b> pool.
    </p>
    <p>
      Please note that you will be unable to move it back without system administrator. Are you sure you want to continue?
    </p>
  </div>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.MoveAgentToDefaultPoolDialog.close()"/>
    <forms:submit
        type="button"
        label="Move agent"
        onclick="BS.MoveAgentToDefaultPoolDialog.doIt()"/>
  </div>
</bs:dialog>
