<%@ include file="include-internal.jsp" %>
<%@ taglib prefix="resp" tagdir="/WEB-INF/tags/responsible" %>
<%@ taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %>
<bs:messages key="buildNotFound"/>

<c:if test="${not empty buildResultsSummary}">
<jsp:useBean id="buildResultsSummary" type="jetbrains.buildServer.controllers.viewLog.BuildResultsSummary" scope="request"/>
<jsp:useBean id="dependenciesBean" type="jetbrains.buildServer.controllers.viewLog.DependenciesInfo" scope="request"/>

<c:set var="build" value="${buildResultsSummary.build}"/>
<c:set var="buildStatistics" value="${buildResultsSummary.buildStatistics}"/>

<table cellspacing="0" class="buildResultsSummaryTable">
  <td>

    <div class="header">Build shortcuts</div>

      <ul class="bsLinks">
        <li>
          <a href="<c:url value='/viewLog.html?tab=buildLog&buildTypeId=${build.buildTypeId}&buildId=${build.buildId}'/>"
                 title="View log messages">Build log</a>
          <span class="separator">|</span>
          <a href="<c:url value='downloadBuildLog.html?buildId=${build.buildId}&archived=true'/>" target="_blank"
             title="Download archived build log">.zip</a>
        </li>
        <li><a href="<c:url value='/viewLog.html?tab=buildParameters&buildTypeId=${build.buildTypeId}&buildId=${build.buildId}'/>"
               title="View build parameters">Parameters</a></li>

        <c:forEach items="${buildResultsSummary.extensions}" var="extension">
          <li><bs:_viewLog build="${build}" title="View ${extension.tabTitle}" tab="${extension.tabId}">${extension.tabTitle}</bs:_viewLog></li>
        </c:forEach>
      </ul>

  </td>

  <c:if test="${buildStatistics.failedTestCount > 0 or not empty dependenciesBean.failedDependencies}">
  <td style="padding: 0 0 5px 15px">
    <c:if test="${not empty dependenciesBean.failedDependencies}">
      <div class="header"><bs:icon icon="error_small.gif"/> Failed dependencies: ${fn:length(dependenciesBean.failedDependencies)}</div>
      <table class="depsTable">
      <c:forEach items="${dependenciesBean.failedDependencies}" var="depBuild">
        <tr>
          <td><bs:buildTypeLinkFull buildType="${depBuild.buildType}"/>
            <div class="res"><bs:resultsLink build="${depBuild}" >#${depBuild.buildNumber} &nbsp; ${depBuild.statusDescriptor.text}</bs:resultsLink></div>
          </td>
        </tr>
      </c:forEach>
      </table>
    </c:if>

    <c:if test="${buildStatistics.failedTestCount > 0}">
      <div class="header">
        ${buildStatistics.failedTestCount} tests failed (<bs:new count="${buildStatistics.newFailedCount}"/>)
      </div>

      <c:set var="linkToTestRequired" value="true" scope="request"/>
      <tt:testGroupWithActions groupedTestsBean="${buildResultsSummary.groupedTests}" defaultOption="package"
                               withoutActions="true" groupSelector="false" singleBuildTypeContext="true"
                               maxTests="12" maxTestsPerGroup="3" id="build_summary">
        <jsp:attribute name="viewAllUrl">
          <div style="margin-top: 10px">
            <bs:resultsLink build="${build}" noPopup="true">View all tests &raquo;</bs:resultsLink>
          </div>
        </jsp:attribute>
      </tt:testGroupWithActions>
    </c:if>
  </td>
  </c:if>

</table>
</c:if>


