<%@ page import="static jetbrains.buildServer.serverSide.BuildAgentManager.RunConfigurationPolicy.*" %><%@
    include file="/include-internal.jsp" %><%@
    taglib prefix="agent" tagdir="/WEB-INF/tags/agent"

%><jsp:useBean id="agentDetails" scope="request" type="jetbrains.buildServer.controllers.agent.AgentDetailsFormBase"
/><jsp:useBean id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary" scope="request"
/><c:set var="hasSeveralPools" value="${serverSummary.hasSeveralAgentPools}"
/><c:set var="agentType" value="${agentDetails.agentType}"
/><c:set var="url" value="agentDetails.html?${agentDetailInfo.urlParameters}&tab=agentCompatibleConfigurations"
/><c:set var="cssClass" value=""/>
<script type="text/javascript">
  BS.CompatibilityUtils.rewriteUrlIfAllHash();
</script>
<c:if test="${agentDetails.allConfigurationsCount == 0}">
  <p>There are no configurations found.</p>
</c:if>
<c:if test="${agentDetails.allConfigurationsCount > 0}">
  <table id="compatibleChooser">
    <tr>
      <td class="chooser">
        Current run configuration policy:
        <authz:authorize allPermissions="CHANGE_AGENT_RUN_CONFIGURATION_POLICY">
          <jsp:attribute name="ifAccessGranted">
            <select name="policy" onchange="BS.AgentRunPolicy.onPolicyChange(${agentType.agentTypeId}, this.options[this.selectedIndex].value)">
              <forms:option value="<%=ALL_COMPATIBLE_CONFIGURATIONS.name()%>"
                            selected="${agentDetails.allConfigurationsPolicy}">Run all compatible configurations</forms:option>
              <forms:option value="<%=SELECTED_COMPATIBLE_CONFIGURATIONS.name()%>"
                            selected="${agentDetails.selectedConfigurationsPolicy}">Run assigned configurations only</forms:option>
            </select>
          </jsp:attribute>
          <jsp:attribute name="ifAccessDenied">
            <strong><c:choose>
              <c:when test="${agentDetails.allConfigurationsPolicy}">Run all compatible configurations</c:when>
              <c:otherwise>Run assigned configurations only</c:otherwise>
            </c:choose></strong>
          </jsp:attribute>
        </authz:authorize>
      </td>
      <td class="actions">
        <div id="assignButtons">
          <authz:authorize allPermissions="CHANGE_AGENT_RUN_CONFIGURATION_POLICY">
            <c:if test="${agentDetails.selectedConfigurationsPolicy}">
              <forms:addButton onclick="return BS.AgentSelectConfigurationsDialog.show()" showdiscardchangesmessage="false">Assign configurations</forms:addButton>
              <c:set var="cssClass" value="selected"/>
            </c:if>
          </authz:authorize>
        </div>

        <div class="messagesHolder">
          <div id="savingData">Saving...</div>
          <div id="dataSaved">Your changes have been saved</div>
          <div id="errors" class="error" style="display: none; margin: auto;"></div>
        </div>
      </td>
    </tr>
  </table>

  <c:if test="${not empty param['showAll']}">
    <script type="text/javascript">
      BS.AgentRunPolicy.showAll = true;
    </script>
  </c:if>

  <c:set var="showActive" value="${empty param['showInactive']}"/>
  <c:set var="doNotShowInactive" value="${showActive and empty param['showAll']}"/>
  <c:set var="inactiveConfigurationsCount"
         value="${agentDetails.totalSelectedConfigurationsCount -
                        agentDetails.activeCompatibilities.numTotalCompatible -
                        agentDetails.activeCompatibilities.numTotalIncompatible}"/>
  <c:set var="displayNote" value="${hasSeveralPools and (inactiveConfigurationsCount > 0 or agentDetails.selectedConfigurationsPolicy)}"/>

  <bs:refreshable containerId="agentCompatibilityTableRefreshable" pageUrl="${url}">
    <form action="#" id="runConfigurationForm">
      <input type="hidden" id="agentTypeId" value="${agentType.agentTypeId}"/>
      <c:set var="showButton" value="${false}"/>
      <div class="compatibilityDetailsToolbar">
        <c:if test="${showActive}">
          <span class="expand-collapse">
            <bs:collapseExpand collapseAction="" expandAction=""/>
          </span>
        </c:if>
        <c:if test="${displayNote}">
          <span id="poolDetails">
            <c:set var="total" value="${agentDetails.totalSelectedConfigurationsCount}"/>
            This agent belongs to <bs:agentPoolLink agentPoolId="${agentDetails.agentType.agentPool.agentPoolId}"
                                                    agentPoolName="${agentDetails.agentType.agentPool.name}"/>.
            <span <c:if test="${not doNotShowInactive}">style="display:none"</c:if>>
              You're viewing build configurations only from that pool.
              <a href="#" onclick="return BS.CompatibilityUtils.showFromOtherPools(${showButton ? 1 : 0});">
                <c:if test="${total > 0}">View all ${total} configurations &raquo;</c:if>
                <c:if test="${total == 0}">Display build configurations from other pools &raquo;</c:if>
              </a>
            </span>
            <span <c:if test="${doNotShowInactive}">style="display:none"</c:if>>
              You're viewing <i>all</i> <c:if test="${total > 0}">${total}</c:if> build configurations from <i>all</i> pools.
            </span>
          </span>
        </c:if>
      </div>
      <c:if test="${showActive}">
        <agent:agentCompatibilityTable data="${agentDetails.activeCompatibilities}"
                                       selectedConfigurationsPolicy="${agentDetails.selectedConfigurationsPolicy}"
                                       cssClass="${cssClass}"
                                       active="${true}"/>
        <c:set var="showButton" value="${agentDetails.selectedConfigurationsPolicy &&
                                        (agentDetails.activeCompatibilities.numCompatible > 0 ||
                                         agentDetails.activeCompatibilities.numIncompatible > 0)}"/>
      </c:if>
      <c:if test="${displayNote}">
        <div id="poolDetailsBottom" <c:if test="${not doNotShowInactive}">style="display:none"</c:if>>
          <a href="#" onclick="return BS.CompatibilityUtils.showFromOtherPools(${showButton ? 1 : 0});">View build configurations from other pools &raquo;</a>
        </div>
      </c:if>
      <bs:refreshable containerId="inactiveConfigurationsTable" pageUrl="${pageUrl}">
        <div id="inactiveConfigurationsTableParent" style='<c:if test="${doNotShowInactive}">display: none;</c:if>'>
          <c:choose>
            <c:when test="${doNotShowInactive}">
              <forms:progressRing style="padding-top: 0.1em; float: left;"/>
              <span>&nbsp;Loading...</span>
            </c:when>
            <c:otherwise>
              <div id="compatibleDivider">
                Build configurations from other pools.
                <a href="#" onclick="return BS.CompatibilityUtils.hideFromOtherPools()">Hide</a>
              </div>
              <agent:agentCompatibilityTable data="${agentDetails.inactiveCompatibilities}"
                                             selectedConfigurationsPolicy="${agentDetails.selectedConfigurationsPolicy}"
                                             cssClass="${cssClass}"
                                             active="${false}"/>
              <c:set var="showButton"
                     value="${agentDetails.selectedConfigurationsPolicy && (showButton || agentDetails.inactiveCompatibilities.numCompatible > 0 || agentDetails.inactiveCompatibilities.numIncompatible > 0)}"/>
            </c:otherwise>
          </c:choose>
        </div>
      </bs:refreshable>
    </form>
    <script type="text/javascript">
      BS.CompatibilityUtils.initAgentCompatibility();
    </script>
  </bs:refreshable>

  <forms:modified modifiedText="Changes not yet applied" buttonCaption="Unassign selected"/>
  <script type="text/javascript">
    $j(document).ready(function() {
      BS.AgentRunPolicy.setupEventHandlers();
    });
  </script>
  <jsp:include page="/agent/agentSelectConfigurationsDialog.html?mode=empty&agentTypeId=${agentType.agentTypeId}"/>
</c:if>
