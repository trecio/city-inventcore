<%@ include file="/include-internal.jsp"
  %><jsp:useBean id="agentStatistics" scope="request" type="jetbrains.buildServer.controllers.agent.statistics.AgentStatisticsForm"
  /><jsp:useBean id="serverSummary" scope="request" type="jetbrains.buildServer.web.openapi.ServerSummary"
  /><c:set var="lines" value="${agentStatistics.lines}"
  />
<bs:linkCSS dynamic="${true}">
  /css/agentBlocks.css
</bs:linkCSS>

<div class="agentStatistics">
<c:url value="/agentsStatistics.html" var="controllerUrl"/>
<form action="${controllerUrl}" method="post" id="agentsStatsFilter">
  <input type="hidden" name="tab" value="agentStatistics"/>
  <input type="hidden" name="agentsStatistics" value="1"/>
  <table class="agentStatsFilter">
    <tr>
      <td class="dateFromLabel"><label for="dateFrom">From:</label></td>
      <td class="dateFromInput">
        <forms:textField name="dateFrom" value="${agentStatistics.range.dateFrom}" noAutoComplete="true"/>
        <div class="error" id="error_dateFrom" style="margin-left:0; white-space:nowrap;"></div>
      </td>
      <td class="dateToLabel"><label for="dateTo">To:</label></td>
      <td class="dateToInput">
        <forms:textField name="dateTo" value="${agentStatistics.range.dateTo}" noAutoComplete="true"/>
        <div class="error" id="error_dateTo" style="margin-left:0; white-space:nowrap;"></div>
      </td>
      <td><input class="btn btn_mini" type="submit" name="submitFilter" value="Update"/></td>
      <td style="width:50px;vertical-align:middle;"><forms:saving id="agentStatisticsProgress" className="progressRingInline"/></td>
      <td>&nbsp;</td>
      <c:if test="${serverSummary.hasSeveralAgentPools}">
        <td style="width: 300px; text-align: right;"><forms:checkbox name="groupByPools" checked="${agentStatistics.groupByPools}" style="vertical-align:middle;"/><label for="groupByPools">Group by agent pools</label></td>
      </c:if>
      <td style="width: 100px; text-align: right;"><label for="sortOrder" style="vertical-align:middle;">Sort by:</label></td>
      <td style="width: 50px">
        <select id="sortOrder" name="sortOrder">
          <c:set var="sortOrder">${agentStatistics.range.sortOrder}</c:set>
          <c:forEach items="${agentStatistics.sortOrders}" var="item"><forms:option value="${item.key}" selected="${item.key eq sortOrder}"><c:out
            value="${item.value}"/></forms:option></c:forEach>
        </select>
      </td>
    </tr>
  </table>
</form>


  <script type="text/javascript">
    BS.AgentsStatistics.decorateDateInputs();
    BS.AgentsStatistics.buildTypeToProject = {<bs:mapToJSON map="${lines.projectToName}"/>};
    BS.AgentsStatistics.buildTypeToProject[''] = 'Configuration is unavailable';
  </script>
  <table>
    <tr>
      <td>
        <table class="agentScale" style="width:${lines.scale.size}px;">
          <tr>
            <c:forEach items="${lines.scale.beans}" var="seg">
              <td style="width:${seg.length}px;">
                <div>
                    <c:out value="${seg.timeFirst}"/>
                  <div></div>
                  <div style="margin-top:-10px;"><c:out value="${seg.timeSecond}"/></div>
              </td>
            </c:forEach>
          </tr>
          <tr class="rulerUp">
            <c:forEach items="${lines.scale.beans}" var="seg" varStatus="pos">
              <td style="width:${seg.length}px;<c:if test="${pos.last}">border-bottom:none;</c:if>"></td>
            </c:forEach>
          </tr>
        </table>
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <div class="agents_scrollable custom-scroll" id="agents_scrollable">
    <c:if test="${fn:length(lines.lines) == 0}">
      <div class="noData" style="width:${lines.scale.size - 80}px;">No data found</div>
    </c:if>
    <table>
      <c:forEach items="${lines.lines}" var="poolLine">
        <c:set var="pool" value="${poolLine.first}"/>
        <c:if test="${pool != null}">
          <tr><td align="center"><bs:agentPoolLink agentPoolId="${pool.agentPoolId}" agentPoolName="${pool.name}" groupHeader="${true}"/></td><td>&nbsp;</td></tr>
        </c:if>
        <c:forEach items="${poolLine.second}" var="line" varStatus="pos">
          <tr class="agents">
            <td <c:if test="${pos.last}">style="border-bottom:none;"</c:if>>
              <table class="agentLines" style="width:${line.value.size+2}px;">
                <tr>
                  <c:forEach items="${line.value.beans}" var="seg">
                    <c:set var="kind" value="${seg.kind}"/>

                    <c:set var="borderClass">
                      <c:if test="${seg.drawLeftBorder or seg.drawRightBorder}">
                        <c:if test="${seg.drawLeftBorder}">left</c:if>
                        <c:if test="${seg.drawRightBorder}">right</c:if>
                      </c:if>
                    </c:set>

                    <c:set var="kindAttr">
                      <c:choose>
                        <c:when test="${kind.idle}">idle</c:when>
                        <c:when test="${kind.one}">one</c:when>
                        <c:when test="${kind.group}">group</c:when>
                      </c:choose>
                    </c:set>

                    <c:if test="${kind.one}">
                      <c:set var="val" value="${seg.agentBuildValue}"/>
                    </c:if>

                    <c:if test="${not empty val}">
                      <c:set var="buildTypeId" value="${val.buildTypeId}"/>
                    </c:if>

                    <c:if test="${not empty val}">
                      <c:set var="buildId" value="${val.inHistory ? val.buildId : 0}"/>
                    </c:if>

                    <c:if test="${kind.group}">
                      <c:set var="buildsCount" value="${seg.buildsCount}"/>
                    </c:if>

                    <c:if test="${not empty val and not(kind.one and val.inHistory)}">
                      <c:set var="segStartTime" value="${seg.startTime}"/>
                      <c:set var="segFinishTime" value="${seg.finishTime}"/>
                      <c:set var="segDuration" value="${seg.duration}"/>
                    </c:if>

                    <td class="${borderClass}"
                        data-kind="${kindAttr}"
                        data-build-type-id="${buildTypeId}"
                        data-build-id="${buildId}"
                        data-builds-count="${buildsCount}"
                        data-seg-start-time="${segStartTime}"
                        data-seg-finish-time="${segFinishTime}"
                        data-seg-duration="${segDuration}"
                        style="width:${seg.length}px;background-color:${seg.cssColor};">
                      <c:if test="${seg.drawable}"><span><c:out value="${seg.duration}"/></span></c:if>
                    </td>
                  </c:forEach>
                </tr>
              </table>
            </td>
            <td<c:if test="${pos.last}"> style="border-bottom:none;"</c:if>>
              <div>
                <c:set var="agentName" value="${line.key.agentName}"/>
                <c:set var="agent" value="${line.key.agent}"/>
                <c:set var="agentType" value="${line.key.agentType}"/>
                <c:choose>
                  <c:when test="${not empty agent or not empty agentType}">
                    <bs:agentDetailsFullLink agentType="${agentType}" agent="${agent}" doNotShowPoolInfo="${agentStatistics.actuallyGroupedByPools}"><c:out value="${agentName}"/></bs:agentDetailsFullLink>
                  </c:when>
                  <c:otherwise><c:out value="${agentName}"/></c:otherwise>
                </c:choose>
              </div>
              <div>Usage: <c:out value="${line.value.loadFactor}"/></div>
            </td>
          </tr>
        </c:forEach>
      </c:forEach>
    </table>
  </div>
  <table>
    <tr>
      <td>
        <table class="agentScale" style="width:${lines.scale.size}px;">
          <tr class="rulerDown">
            <c:forEach items="${lines.scale.beans}" var="seg" varStatus="pos">
              <td style="width:${seg.length}px;<c:if test="${pos.last}">border-top:none;</c:if>"></td>
            </c:forEach>
          </tr>
          <tr>
            <c:forEach items="${lines.scale.beans}" var="seg">
              <td style="width:${seg.length}px;">
                <div>
                    <c:out value="${seg.timeFirst}"/>
                  <div></div>
                  <div style="margin-top:-10px;"><c:out value="${seg.timeSecond}"/></div>
              </td>
            </c:forEach>
          </tr>
        </table>
  </table>

</div>
<script type="text/javascript">
  BS.AgentsStatistics.init();
</script>