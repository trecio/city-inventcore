<%@ include file="/include-internal.jsp"%>
<jsp:useBean id="runBuildBean" type="jetbrains.buildServer.controllers.RunBuildBean" scope="request"/>

<div id="comment-tab" style="display: none;" class="tabContent">
  <table class="runnerFormTable">
    <tr>
      <th>Build comment:</th>
      <td>
        <textarea name="buildComment" rows="5" cols="46" class="commentTextArea" onfocus="if (this.value == this.defaultValue) this.value = ''" onblur="if (this.value == '') this.value='&lt;your comment here&gt;'">&lt;your comment here&gt;</textarea>
        <c:if test="${not empty runBuildBean.buildComment}">
          <script type="text/javascript">BS.RunBuildDialog.updateComment('<bs:escapeForJs text="${runBuildBean.buildComment}"/>');</script>
        </c:if>
      </td>
    </tr>
  </table>
</div>