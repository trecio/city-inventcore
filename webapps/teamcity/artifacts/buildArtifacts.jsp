<%@ page contentType="text/html;charset=UTF-8" language="java" session="true" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="tags" tagdir="/WEB-INF/tags/tags" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags"

%><jsp:useBean id="build" type="jetbrains.buildServer.serverSide.SBuild" scope="request"
/><jsp:useBean id="showAll" type="java.lang.Boolean" scope="request"/>

<script type="text/javascript">
enablePeriodicalRefresh();
</script>

<div class="buildArtifacts">
  <%@ include file="/buildLog/userBuildDetails.jspf" %>
  <c:choose>
    <c:when test="${build.hasInternalArtifactsOnly and not showAll}">
      Build has hidden artifacts only.
      <bs:_viewLog build="${build}" title="View build artifacts" urlAddOn="&showAll=true" tab="artifacts">Show hidden</bs:_viewLog>
    </c:when>
    <c:otherwise>
      <jsp:include page="printArtifactsTree.jsp" />
    </c:otherwise>
  </c:choose>
</div>
