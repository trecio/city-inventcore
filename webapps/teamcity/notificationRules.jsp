<%@ include file="include-internal.jsp"%>
<%@ taglib prefix="profile" tagdir="/WEB-INF/tags/userProfile"%>
<jsp:useBean id="currentUser" type="jetbrains.buildServer.users.User" scope="request"/>
<jsp:useBean id="notificationRulesForm" type="jetbrains.buildServer.controllers.profile.notifications.NotificationRulesForm" scope="request"/>
<c:set var="inheritedRulesMap" value="${notificationRulesForm.inheritedRules}"/>
<c:set var="groups" value="<%=notificationRulesForm.getInheritedRules().keySet()%>"/>

<script type="text/javascript">
  BS.NotificationRuleForm.setNotificatorType('<c:url value="${notificationRulesForm.notificatorType}"/>');
  BS.NotificationRuleForm.setHolderId('${notificationRulesForm.editeeId}');
</script>

<c:if test="${not notificationRulesForm.canEditRules and not empty notificationRulesForm.notificationsRules}">
  <n:readOnlyRules rules="${notificationRulesForm.notificationsRules}"/>
</c:if>

<c:if test="${notificationRulesForm.canEditRules}">
<c:if test="${fn:length(notificationRulesForm.notificationsRules) > 1}">
  <div>Use drag-and-drop to reorder the rules. The first matching rule will apply.</div>
</c:if>

<div class="messagesHolder" style="width:80%;">
  <div id="savingData">Saving...</div>
  <div id="dataSaved">New notification rules order applied</div>
  <bs:messages key="ruleUpdated" style="margin: 0em; "/>
  <bs:messages key="ruleAdded" style="margin: 0em;"/>
  <bs:messages key="ruleDeleted" style="margin: 0em;"/>
</div>

<c:if test="${fn:startsWith(notificationRulesForm.editeeId, 'user:') and (fn:length(notificationRulesForm.notificationsRules) > 0 or fn:length(groups) > 0)}">
<div class="sidebarNote">
  <bs:help file="Subscribing+to+Notifications" style="float: right;"/>
  <p>To unsubscribe from group notifications you can add your own rule with same watched builds and different notification events.</p>
  <p>To unsubscribe from all events, add a rule with corresponding watched builds and no events selected.</p>
</div>
</c:if>

<c:if test="${not empty notificationRulesForm.notificationsRules}">
  <c:set var="idsIndex">1</c:set>

  <div class="rulesWrapper">
    <table class="dark rulesTable">
      <tr class="header">
        <th class="dragHandle"><img src="<c:url value='/img/dragAndDrop/dots.gif'/>" alt="" style="visibility:hidden;"/></th>
        <th class="moveTop"><img src="<c:url value='/img/dragAndDrop/moveTopInactive.gif'/>" alt="" style="visibility:hidden;"/></th>
        <th class="builds">Watching</th>
        <th class="condition">Send notification when</th>
        <th class="edit"><span style="visibility:hidden;">edit</span></th>
        <th class="remove"><span style="visibility:hidden;">delete</span></th>
      </tr>
    </table>

    <div id="notificationRulesRows">
    <c:set var="editing">${not empty notificationRulesForm.editingRule}</c:set>
    <c:forEach items="${notificationRulesForm.notificationsRules}" var="rule" varStatus="pos">
    <div <c:choose>
           <c:when test="${editing and notificationRulesForm.editingRule.id == rule.id}">class="draggableEditingRow"</c:when>
           <c:when test="${not editing and rule.addedOrEdited}">class="draggableChangedRow"</c:when>
           <c:otherwise>class="draggable"</c:otherwise>
         </c:choose> id="rule_${rule.id}"
    >
      <jsp:useBean id="rule" type="jetbrains.buildServer.controllers.profile.notifications.NotificationRulesForm.EditableNotificationRule"/>
      <table class="dark rulesTable">
        <tr>
          <td class="dragHandle"></td>
          <td class="moveTop">
            <img id="id_${idsIndex}"
                 src="<c:url value='/img/dragAndDrop/moveTopInactive.gif'/>"
                 class="moveTopIcon" width="16" height="16"
                 <c:if test="${pos.first}">style='visibility:hidden'</c:if>
                 onmouseover="this._oldSrc = this.src; this.src = '<c:url value='/img/dragAndDrop/moveTopActive.gif'/>';"
                 onmouseout="this.src = this._oldSrc;"
                 onclick="BS.NotificationRuleForm.moveToTop('id_${idsIndex}')"
                 alt="Move to top"
            />
            <c:set var="idsIndex">${idsIndex + 1}</c:set>
          </td>
      <td class="builds">
        <n:watchedBuilds rule="${rule}"/>
      </td>
      <td class="condition">
        <n:eventsList rule="${rule}"/>
      </td>
      <td class="edit">
        <c:set var="onclick">BS.NotificationRuleForm.editRule('<c:url value='/notificationRules.html'/>', 'id_${idsIndex}')</c:set>
        <a id="id_${idsIndex}" href="#" onclick="${onclick}; return false">edit</a>
        <c:set var="idsIndex">${idsIndex + 1}</c:set>
      </td>
      <td class="remove" title="Remove rule">
        <a id="id_${idsIndex}" href="#" onclick="BS.NotificationRuleForm.removeRule('<c:url value='/notificationRules.html'/>', 'id_${idsIndex}'); return false">delete</a>
        <c:set var="idsIndex">${idsIndex + 1}</c:set>
      </td>
        </tr>
        </table>
    </div>
    </c:forEach>
    </div>
  </div>

  <script type="text/javascript">
    Sortable.create('notificationRulesRows', {
      tag : "div",
      onUpdate: function(el) {
        BS.NotificationRuleForm.scheduleUpdate(el);
      }
    });
  </script>
</c:if>

<c:if test="${empty ruleBean or not ruleBean.newRule}">
<p>
  <c:url value='/notificationRules.html' var="editRuleUrl"/>
  <forms:addButton onclick="BS.NotificationRuleForm.editRule('${editRuleUrl}', null); return false">Add new rule</forms:addButton>
</p>
</c:if>

<c:if test="${not empty ruleBean}">
  <a name="ruleEditingForm"></a>
  <jsp:include page="/notificationRuleForm.jsp"/>
</c:if>

</c:if>

<br/>

<c:if test="${not empty inheritedRulesMap}">
<c:forEach items="${groups}" var="group">
  <c:set var="inheritedRules" value="${inheritedRulesMap[group]}"/>
  <c:if test="${not empty inheritedRules}">
  <div class="groupHeader">Rules inherited from the group <bs:editGroupLink group="${group}"><strong><c:out value="${group.name}"/></strong></bs:editGroupLink> <c:if test="${not empty group.description}">(<c:out value="${group.description}"/>)</c:if>:</div>

  <n:readOnlyRules rules="${inheritedRules}"/>
  </c:if>
</c:forEach>
</c:if>
