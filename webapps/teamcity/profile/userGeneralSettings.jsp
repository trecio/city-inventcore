<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="profile" tagdir="/WEB-INF/tags/userProfile"%>
<jsp:useBean id="currentUser" type="jetbrains.buildServer.users.User" scope="request"/>
<jsp:useBean id="profileForm" type="jetbrains.buildServer.controllers.profile.EditPersonalProfileForm" scope="request"/>

<div id="profilePage">
<script type="text/javascript">
$j(document).ready(function() {
  BS.UpdatePersonalProfileForm.setupEventHandlers();

  <c:if test="${profileForm.stateModified}">
  BS.UpdatePersonalProfileForm.setModified(true);
  </c:if>
});
</script>

<form id="profileForm" action="profile.html" onsubmit="return BS.UpdatePersonalProfileForm.submitPersonalProfile()" method="post" autocomplete="off">

<input type="hidden" id="submitUpdateUser" name="submitUpdateUser" value="storeInSession"/>
<input type="hidden" name="tab" value="${currentTab}"/>

<bs:messages key="userChanged"/>
  <l:t2x2>
    <jsp:attribute name="b1">
      <profile:general profileForm="${profileForm}"/>
      <div style="font-size: 0; height: 6px;"></div>
      <profile:vcsPlugins profileForm="${profileForm}" adminMode="false"/>
    </jsp:attribute>
    <jsp:attribute name="b2">
      <c:if test="${not isGuestUser}">
        <profile:notifications profileForm="${profileForm}" adminMode="false"/>
        <div style="font-size: 0; height: 6px;"></div>
      </c:if>
      <l:settingsBlock title="UI Settings">
        <div style="padding: 2px 0;">
          <forms:checkbox name="highlightMyChanges" checked="${profileForm.highlightMyChanges}"/> <label for="highlightMyChanges">Highlight my changes and investigations</label><br/>
        </div>
        <div style="padding: 2px 0;">
          <forms:checkbox name="autodetectTimeZone" checked="${profileForm.autodetectTimeZone}"/> <label for="autodetectTimeZone">Show date/time in my timezone</label>
        </div>
        <div style="padding: 2px 0;">
          <forms:checkbox name="showAllPersonalBuilds" checked="${profileForm.showAllPersonalBuilds}"/> <label for="showAllPersonalBuilds">Show all personal builds</label>
        </div>
      </l:settingsBlock>
    </jsp:attribute>
  </l:t2x2>

  <div class="center" style="width: 20em; padding-top:1em;">
    <forms:submit label="Save changes"/>
    <forms:saving id="saving1"/>
  </div>

</form>

<forms:modified/>
</div>
