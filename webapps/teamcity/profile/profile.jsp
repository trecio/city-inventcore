<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %>
<%@ include file="/include-internal.jsp" %>
<jsp:useBean id="currentUser" type="jetbrains.buildServer.users.User" scope="request"/>
<jsp:useBean id="profileForm" type="jetbrains.buildServer.controllers.profile.EditPersonalProfileForm" scope="request"/>
<c:set var="pageTitle" value="My Settings & Tools" scope="request"/>

<l:defineCurrentTab defaultTab="userGeneralSettings"/>
<bs:page>
<jsp:attribute name="head_include">
  <bs:linkCSS>
    /css/visibleProjects.css
    /css/settingsTable.css
    /css/userRoles.css
    /css/notificationRules.css
    /css/profilePage.css
    /css/settingsBlock.css
  </bs:linkCSS>
  <bs:linkScript>
    /js/bs/updateUser.js
    /js/bs/queueLikeSorter.js
    /js/bs/notificationRules.js
  </bs:linkScript>

  <script type="text/javascript">
    BS.Navigation.items = [
        {title: "<c:out value="${pageTitle}"/>", selected:true}
    ];

    document.observe("dom:loaded", function() {
      BS.Navigation.selectMySettingsTab();
    });
  </script>
</jsp:attribute>

<jsp:attribute name="body_include">

  <div id="sidebar">

    <c:if test="${currentTab == 'userGeneralSettings'}">
    <div class="blockAgents">
      <h3>TeamCity Tools</h3>

      <p class="toolTitle idea">IntelliJ Platform plugin
        <bs:help style="display:inline;" file="IntelliJ+Platform+Plugin"/>
      </p>
      <a showdiscardchangesmessage="false" title="Download plugin for IntelliJ Platform IDEs" href="<c:url value='/update/TeamCity-IDEAplugin.zip'/>">download</a>
      <div class="divider"></div>

      <p class="toolTitle eclipsePlugin">Eclipse Plugin
        <bs:help style="display:inline;" file="Eclipse+Plugin"/>
      </p>
      <a showdiscardchangesmessage="false" title="Copy the link location and use it as Eclipse update site" href="<c:url value='/update/eclipse/'/>" target="_blank">update site link</a>

      <ext:includeExtensions placeId="<%=PlaceId.MY_TOOLS_SECTION%>"/>

    </div>
    </c:if>
  </div>

  <ext:showTabs placeId="<%= PlaceId.MY_TOOLS_TABS %>" urlPrefix="/profile.html" tabContainerId="tabsContainer3"/>

  <div class="clr"></div>

</jsp:attribute>

</bs:page>

