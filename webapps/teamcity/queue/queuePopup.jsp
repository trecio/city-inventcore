<%@ include file="/include-internal.jsp" %>
<jsp:useBean id="buildQueue" type="jetbrains.buildServer.controllers.queue.BuildQueueForm" scope="request"/>
<queue:buildQueueList/>
<script type="text/javascript">
  BS.QueuedBuildsPopup._currentBuildTypeId = "${buildQueue.filteredByBuildType.buildTypeId}";
</script>
