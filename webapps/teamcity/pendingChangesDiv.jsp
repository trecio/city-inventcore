<%@ include file="include-internal.jsp"%>
<jsp:useBean id="changeLogBean" type="jetbrains.buildServer.controllers.buildType.tabs.ChangeLogBean" scope="request"/>
<jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.SBuildType" scope="request"/>
<bs:changesList changeLog="${changeLogBean}"
                url="viewType.html?buildTypeId=${buildType.buildTypeId}&tab=pendingChangesDiv"
                filterUpdateUrl="pendingChangeLogTab.html?buildTypeId=${buildType.buildTypeId}"
                projectId="${buildType.projectId}"
                hideBuildSelectors="true"
                hideShowBuilds="true"
                enableCollapsibleChanges="true"/>