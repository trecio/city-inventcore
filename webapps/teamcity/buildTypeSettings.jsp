<%@ page import="jetbrains.buildServer.serverSide.BuildFeature" %>
<%@ page import="jetbrains.buildServer.serverSide.BuildTypeOptions" %>
<%@ page import="jetbrains.buildServer.serverSide.dependency.DependencyOptions" %>
<%@include file="include-internal.jsp"%>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<div id="buildTypeSettings" class="buildTypeSettings">

    <jsp:useBean id="buildTypeSettings" type="jetbrains.buildServer.controllers.buildType.BuildTypeSettingsBean" scope="request"/>
    <c:set var="title" value="${buildTypeSettings.buildType.fullName} settings"/>

    <c:if test="${buildTypeSettings.buildType.templateBased}">
      <div class="smallNoteAttention">
        This build configuration settings are based on template:
        <authz:authorize projectId="${buildTypeSettings.buildType.template.parentProject.projectId}" allPermissions="EDIT_PROJECT"
              ><jsp:attribute name="ifAccessGranted"
              ><admin:editTemplateLink templateId="${buildTypeSettings.buildType.template.id}"><c:out value="${buildTypeSettings.buildType.template.fullName}"/> &raquo;</admin:editTemplateLink
              ></jsp:attribute
              ><jsp:attribute name="ifAccessDenied"><c:out value="${buildTypeSettings.buildType.template.fullName}"/></jsp:attribute
              ></authz:authorize>
      </div>
    </c:if>
      
    <div class="parameterGroup">
      <bs:buildTypeSettingsEditLink buildType="${buildTypeSettings.buildType}" num="0"/>

      <div class="parameter">
        Name: <strong><c:out value="${buildTypeSettings.buildType.name}"/></strong>
      </div>

      <div class="parameter">
        Description: <strong><c:out value="${empty buildTypeSettings.buildType.description ? 'none' : buildTypeSettings.buildType.description}"/></strong>
      </div>

      <div class="parameter">
        Build number format: <strong><c:out value="${buildTypeSettings.buildType.buildNumberPattern}"/></strong>, next build number: <strong>#<c:out value="${buildTypeSettings.buildType.buildNumbers.buildNumber}"/></strong>
      </div>

      <div class="parameter">
        Artifact paths:
        <div class="nestedParameter"><strong>
        <c:choose>
          <c:when test="${empty buildTypeSettings.buildType.artifactPaths}">none specifed</c:when>
          <c:otherwise><bs:out value="${buildTypeSettings.buildType.artifactPaths}"/></c:otherwise>
        </c:choose></strong>
        </div>
      </div>

      <c:set var="failIfExitCode" value="<%=buildTypeSettings.getOption(BuildTypeOptions.BT_FAIL_ON_EXIT_CODE)%>"/>
      <c:set var="failIfTestFailed" value="<%=buildTypeSettings.getOption(BuildTypeOptions.BT_FAIL_IF_TESTS_FAIL)%>"/>
      <c:set var="failIfErrMessage" value="<%=buildTypeSettings.getOption(BuildTypeOptions.BT_FAIL_ON_ANY_ERROR_MESSAGE)%>"/>
      <c:set var="failIfOOME" value="<%=buildTypeSettings.getOption(BuildTypeOptions.BT_FAIL_ON_OOME_OR_CRASH)%>"/>
      <c:set var="maxBuildsNum" value="<%=buildTypeSettings.getOption(BuildTypeOptions.BT_MAX_RUNNING_BUILDS)%>"/>
      <c:set var="hangingBuildsDetection" value="<%=buildTypeSettings.getOption(BuildTypeOptions.BT_HANGING_BUILDS_DETECTION_ENABLED)%>"/>

      <div class="parameter">
        Build options:

        <div class="nestedParameter">
        hanging builds detection: <strong>${hangingBuildsDetection ? 'ON' : 'OFF'}</strong>
        </div>

        <div class="nestedParameter">
        status widget: <strong>${buildTypeSettings.buildType.allowExternalStatus ? 'ON' : 'OFF'}</strong>
        </div>

        <div class="nestedParameter">
        maximum number of simultaneously running builds: <strong>${maxBuildsNum == 0 ? 'unlimited' : maxBuildsNum}</strong>
        </div>

      </div>
    </div>

    <div class="parameterGroup">
      <bs:buildTypeSettingsEditLink buildType="${buildTypeSettings.buildType}" num="1"/>

      <div class="parameter">
        VCS checkout mode: <strong><c:out value="${buildTypeSettings.vcsRootsBean.checkoutTypeDescription[buildTypeSettings.vcsRootsBean.checkoutType]}"/></strong>
      </div>

      <div class="parameter">
        Checkout directory:
        <strong>
          <c:choose>
            <c:when test="${not empty buildTypeSettings.vcsRootsBean.checkoutDir}">
              <c:out value="${buildTypeSettings.vcsRootsBean.checkoutDir}"/>
            </c:when>
            <c:otherwise>
              default
            </c:otherwise>
          </c:choose>
        </strong>
      </div>

      <div class="parameter">
        Clean all files before build: <strong>${buildTypeSettings.buildType.cleanBuild ? 'ON' : 'OFF'}</strong>
      </div>

      <div class="parameter">
        VCS labeling:
        <c:choose>
          <c:when test="${buildTypeSettings.vcsRootsBean.labelingTypeStr eq 'NONE'}"><strong>disabled</strong></c:when>
          <c:when test="${buildTypeSettings.vcsRootsBean.labelingTypeStr eq 'ALWAYS'}"><strong>enabled for all of the builds</strong></c:when>
          <c:when test="${buildTypeSettings.vcsRootsBean.labelingTypeStr eq 'SUCCESSFUL_ONLY'}"><strong>enabled for successful builds only</strong></c:when>
        </c:choose>
      </div>

      <c:if test="${buildTypeSettings.vcsRootsBean.labelingTypeStr ne 'NONE'}">
        <div class="parameter">
          VCS label pattern: <strong><c:out value="${buildTypeSettings.vcsRootsBean.labelingPattern}"/></strong>
        </div>
      </c:if>

      <div class="parameter">Attached VCS roots:
        <c:if test="${empty buildTypeSettings.vcsRootsBean.vcsRoots}">
          <div class="nestedParameter">There are no VCS roots attached to this build configuration</div>
        </c:if>
        <c:if test="${not empty buildTypeSettings.vcsRootsBean.vcsRoots}">
          <div class="nestedParameter">
          <table class="settings">
            <tr>
              <th class="name vcsRootName">Name</th>
              <th class="name">Checkout rules</th>
              <th class="name vcsRootLabel">Set label</th>
            </tr>
            <c:forEach items="${buildTypeSettings.vcsRootsBean.vcsRoots}" var="vcsRootEntry">
              <tr>
                <td class="vcsRootName"><c:out value="${vcsRootEntry.vcsRoot.name}"/></td>
                <td><c:set var="checkoutRules" value="${vcsRootEntry.checkoutRules.asString}"/><c:choose>
                  <c:when test="${not empty checkoutRules}"><bs:out value="${vcsRootEntry.checkoutRules.asString}"/></c:when>
                  <c:otherwise>not specified</c:otherwise>
                </c:choose></td>
                <td class="vcsRootLabel"><c:choose>
                  <c:when test="${buildTypeSettings.vcsRootsBean.labelingRoots[vcsRootEntry.vcsRoot.id]}"><strong>YES</strong></c:when>
                  <c:otherwise>NO</c:otherwise>
                </c:choose></td>
              </tr>
            </c:forEach>
          </table>
          </div>
        </c:if>
      </div>

      <div class="parameter">
        Show changes from snapshot dependencies: <strong>${buildTypeSettings.vcsRootsBean.showDependenciesChanges ? 'ON' : 'OFF'}</strong>
      </div>

    </div>

    <div class="parameterGroup">
      <bs:buildTypeSettingsEditLink buildType="${buildTypeSettings.buildType}" num="2"/>

      <c:forEach var="runnerBean" items="${buildTypeSettings.buildRunners}" varStatus="pos">
        <c:if test="${runnerBean.enabled}">
        <div class="parameter">

          Step ${pos.index+1}:
          <c:if test="${fn:length(runnerBean.buildStepName) > 0}"><strong><c:out value="${runnerBean.buildStepName}"/></strong></c:if>

          <div class="nestedParameter">
            <div class="parameter">Runner type: <strong><c:out value="${runnerBean.runType.displayName}"/></strong> (<c:out value="${runnerBean.runType.description}"/>)</div>
            <div class="parameter">Execute:<bs:help file="Configuring+Build+Steps"/> <strong><c:out value="${runnerBean.selectedExecutionPolicy.description}"/></strong></div>
            <admin:runnerSettings runnerBean="${runnerBean}"/>
          </div>

        </div>
        </c:if>
      </c:forEach>

      <c:set var="buildFeaturesBean" value="${buildTypeSettings.buildFeaturesBean}"/>
      <c:set var="place" value="<%= BuildFeature.PlaceToShow.GENERAL %>"/>
      <c:set var="features">
        <c:forEach items="${buildFeaturesBean.buildFeatureDescriptors}" var="featureDescriptor">
          <c:if test="${featureDescriptor.descriptor.buildFeature.placeToShow == place and featureDescriptor.enabled}">
            <tr>
              <td style="white-space: nowrap;">
                <admin:featureInfo feature="${featureDescriptor}" showDescription="false"/>
              </td>
              <td>
                <admin:featureInfo feature="${featureDescriptor}" showName="false"/>
              </td>
            </tr>
          </c:if>
        </c:forEach>
      </c:set>

      <c:if test="${not empty fn:trim(features)}">
        <div class="parameter">
          Build features:

          <div class="nestedParameter">
            <table class="settings">
              <tr>
                <th class="name" style="width: 30%;">Type</th>
                <th class="name" style="width: 70%;">Parameters Description</th>
              </tr>
              ${features}
            </table>
          </div>
        </div>
      </c:if>


    </div>

    <div class="parameterGroup">
      <bs:buildTypeSettingsEditLink buildType="${buildTypeSettings.buildType}" num="3"/>

      <div class="parameter">
        Fail build if:
        <div class="nestedParameter">
          process exit code is not zero: <strong>${failIfExitCode ? 'ON' : 'OFF'}</strong>
        </div>

        <div class="nestedParameter">
          at least one test failed: <strong>${failIfTestFailed ? 'ON' : 'OFF'}</strong>
        </div>

        <div class="nestedParameter">
          an error message is logged by build runner: <strong>${failIfErrMessage ? 'ON' : 'OFF'}</strong>
        </div>

        <div class="nestedParameter">
          it runs longer than:
          <strong>
          <c:choose>
            <c:when test="${buildTypeSettings.buildType.executionTimeoutMin <= 0}">no limit</c:when>
            <c:otherwise>${buildTypeSettings.buildType.executionTimeoutMin} minutes</c:otherwise>
          </c:choose>
          </strong>
        </div>

        <div class="nestedParameter">
          out of memory or crash is detected: <strong>${failIfOOME ? 'ON' : 'OFF'}</strong>
        </div>
      </div>

      <c:set var="buildFeaturesBean" value="${buildTypeSettings.buildFeaturesBean}"/>
      <c:set var="place" value="<%= BuildFeature.PlaceToShow.FAILURE_REASON %>"/>
      <c:set var="features">
        <c:forEach items="${buildFeaturesBean.buildFeatureDescriptors}" var="featureDescriptor">
          <c:if test="${featureDescriptor.descriptor.buildFeature.placeToShow == place}">
            <tr>
              <td style="white-space: nowrap;">
                <admin:featureInfo feature="${featureDescriptor}" showDescription="false"/>
              </td>
              <td>
                <admin:featureInfo feature="${featureDescriptor}" showName="false"/>
              </td>
            </tr>
          </c:if>
        </c:forEach>
      </c:set>

      <c:if test="${not empty fn:trim(features)}">
        <div class="parameter">
          Other build failure conditions:

          <div class="nestedParameter">
            <table class="settings">
              <tr>
                <th class="name" style="width: 30%;">Type</th>
                <th class="name" style="width: 70%;">Parameters Description</th>
              </tr>
              ${features}
            </table>
          </div>
        </div>
      </c:if>

    </div>

    <div class="parameterGroup">
      <bs:buildTypeSettingsEditLink buildType="${buildTypeSettings.buildType}" num="4"/>

      <c:set var="buildTriggersBean" value="${buildTypeSettings.buildTriggersBean}"/>

      <div class="parameter">
      <c:if test="${buildTypeSettings.buildType.paused}">
        Build configuration is paused (triggering disabled).
      </c:if>
      <c:if test="${not buildTypeSettings.buildType.paused}">
        Build configuration is active (triggering enabled).
      </c:if>
      </div>

      <div class="parameter">
        <c:set var="triggers">
          <c:forEach items="${buildTriggersBean.triggers}" var="trigger">
            <c:if test="${trigger.enabled}">
              <tr>
                <td style="white-space: nowrap;">
                  <admin:triggerInfo trigger="${trigger}" showDescription="false"/>
                </td>
                <td>
                  <admin:triggerInfo trigger="${trigger}" showName="false"/>
                </td>
              </tr>
            </c:if>
          </c:forEach>
        </c:set>

        <c:if test="${not empty fn:trim(triggers)}">
          <div class="nestedParameter">
            <table class="settings">
              <tr>
                <th class="name" style="width: 30%;">Build Trigger</th>
                <th class="name" style="width: 70%;">Parameters Description</th>
              </tr>
              ${triggers}
            </table>
          </div>
        </c:if>
      </div>

    </div>

    <div class="parameterGroup">
      <bs:buildTypeSettingsEditLink buildType="${buildTypeSettings.buildType}" num="5"/>

      <c:set var="sourceDependencies" value="${buildTypeSettings.sourceDependenciesBean.visibleDependencies}"/>
      <c:set var="takeStartedBuildOption"><%=DependencyOptions.TAKE_STARTED_BUILD_WITH_SAME_REVISIONS.getKey()%></c:set>
      <c:set var="takeSuccessfulBuildsOnlyOption"><%=DependencyOptions.TAKE_SUCCESSFUL_BUILDS_ONLY.getKey()%></c:set>
      <c:set var="runBuildIfDependencyFailedOption"><%=DependencyOptions.RUN_BUILD_IF_DEPENDENCY_FAILED.getKey()%></c:set>
      <c:set var="runBuildOnSameAgent"><%=DependencyOptions.RUN_BUILD_ON_THE_SAME_AGENT.getKey()%></c:set>

      <div class="parameter">
        Snapshot Dependencies:
        <div class="nestedParameter">
          <c:if test="${empty sourceDependencies}">
          There are no snapshot dependencies.
          </c:if>
          <c:if test="${not empty sourceDependencies}">
            <table class="settings">
              <tr>
                <th class="name">Depends on</th>
                <th class="name">Dependency options</th>
              </tr>
              <c:forEach items="${sourceDependencies}" var="dependency">
                <tr>
                  <td>
                    <c:choose>
                      <c:when test="${dependency.sourceBuildTypeAccessible}">
                        <bs:buildTypeLinkFull buildType="${dependency.sourceBuildType}"/>
                      </c:when>
                      <c:otherwise>
                        <em title="You do not have enough permissions for this build configuration">&laquo;inaccessible build configuration&raquo;</em>
                      </c:otherwise>
                    </c:choose>
                  </td>
                  <td>
                    <c:if test="${dependency.setOptions[takeStartedBuildOption]}"><div>Do not run new build if there is a suitable one</div></c:if>
                    <c:if test="${dependency.setOptions[takeSuccessfulBuildsOnlyOption]}"><div style="margin-left: 1.5em">Only use successful builds from suitable ones</div></c:if>
                    <c:if test="${dependency.setOptions[runBuildIfDependencyFailedOption]}"><div>Run build even if dependency has failed</div></c:if>
                    <c:if test="${dependency.setOptions[runBuildOnSameAgent]}"><div>Run build on the same agent</div></c:if>
                    <c:if test="${empty dependency.setOptions}"><em>Always run a new build</em></c:if>
                  </td>
                </tr>
              </c:forEach>
            </table>
          </c:if>
        </div>
      </div>

      <c:set var="dependencies" value="${buildTypeSettings.artifactDependenciesBean.dependencies}"/>

      <div class="parameter">
        Artifact dependencies:
        <div class="nestedParameter">
          <c:if test="${empty dependencies}">
          There are no artifact dependencies.
          </c:if>
          <c:if test="${not empty dependencies}">
            <table class="settings">
              <tr>
                <th class="name">Depends on</th>
                <th class="name">Artifacts paths</th>
              </tr>
              <c:forEach items="${dependencies}" var="dependency">
                <tr>
                  <td>
                    <c:choose>
                      <c:when test="${dependency.sourceBuildTypeAccessible}">
                        <bs:buildTypeLinkFull buildType="${dependency.sourceBuildType}"/>
                      </c:when>
                      <c:when test="${not dependency.sourceBuildTypeExists}">
                        <em title="Build configuration does not exist">&laquo;build configuration does not exist&raquo;</em>
                      </c:when>
                      <c:otherwise>
                        <em title="You do not have enough permissions for this build configuration">&laquo;inaccessible build configuration&raquo;</em>
                      </c:otherwise>
                    </c:choose>
                    <br/>
                    (<bs:_artifactDependencyLink dependency="${dependency}"/>)
                  </td>
                  <td>
                    <c:forEach items="${dependency.artifactsPaths}" var="path">
                      <bs:out  value="${path}" multilineOnly="true"/><br/>
                    </c:forEach>
                    <c:if test="${dependency.cleanDestination}"> Destinations will be cleaned before downloading artifacts</c:if>
                  </td>
                </tr>
              </c:forEach>
            </table>
          </c:if>
        </div>
      </div>
    </div>

    <div class="parameterGroup">
      <bs:buildTypeSettingsEditLink buildType="${buildTypeSettings.buildType}" num="6"/>
      <c:set var="buildPropertiesBean" value="${buildTypeSettings.buildPropertiesBean}"/>

      <bs:viewBuildParameters buildParameters="${buildPropertiesBean}" nameValueSeparator=":"/>

    </div>

    <div class="parameterGroup">
      <bs:buildTypeSettingsEditLink buildType="${buildTypeSettings.buildType}" num="7"/>
      <c:set var="requirementsBean" value="${buildTypeSettings.requirementsBean}"/>

      <c:if test="${empty requirementsBean.requirements}"><p>None defined</p></c:if>

      <c:if test="${not empty requirementsBean.requirements}">
      <div class="nestedParameter">
        <table class="settings">
          <tr>
            <th class="name" style="width: 30%;">Parameter Name</th>
            <th class="name" style="width: 70%;">Condition</th>
          </tr>
          <c:forEach items="${requirementsBean.requirements}" var="req">
            <tr>
              <td style="white-space: nowrap;"><c:out value="${req.parameterNameWithPrefix}"/></td>
              <td>
                <admin:requirementValue requirementType="${req.type}" parameterValue="${req.parameterValue}"/>
              </td>
            </tr>
          </c:forEach>
        </table>
      </div>
      </c:if>

    </div>

</div>
