<%@ page
  %><%@include file="/include-internal.jsp"
  %>

<jsp:useBean id="buildType" scope="request" type="jetbrains.buildServer.serverSide.SBuildType"/>
<c:set var="pagerUrlPattern" scope="request" value="viewType.html?buildTypeId=${buildType.buildTypeId}&page=[page]&tab=tests"/>

<jsp:include page="/tests/recentlyFailedTests.jsp"/>
