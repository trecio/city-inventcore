<%@ include file="/include-internal.jsp"%>
<%@ taglib prefix="profile" tagdir="/WEB-INF/tags/userProfile" %>
<c:set var="pageTitle" value="Version Control Username Settings" scope="request"/>
<jsp:useBean id="vcsSettingsForm" type="jetbrains.buildServer.controllers.profile.vcs.VcsSettingsForm" scope="request"/>

<bs:page>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/settingsTable.css
      /css/vcsSettings.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/bs/vcsSettings.js
    </bs:linkScript>
    <script type="text/javascript">
      BS.Navigation.items = [
      <c:choose>
         <c:when test="${vcsSettingsForm.adminMode}">
          {title: "Administration", url: '<c:url value="/admin/admin.html"/>'},
          {title: "Users", url: '<c:url value="/admin/admin.html?item=users"/>'},
          <c:set var="name"><c:out value="${vcsSettingsForm.owner.descriptiveName}"/></c:set>
          {title: "<bs:escapeForJs text="${name}"/>", url: '<c:url value="/admin/editUser.html?init=1&userId=${vcsSettingsForm.owner.id}"/>'},
         </c:when>
         <c:otherwise>
          {title: 'My Settings & Tools', url: '<c:url value="/profile.html"/>'},
         </c:otherwise>
      </c:choose>
          {title: '<c:out value="${pageTitle}"/>', selected:true}
      ];
    </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <bs:refreshable containerId="vcsUsernames" pageUrl="${pageUrl}">
      <c:if test="${fn:length(vcsSettingsForm.specifiedVcsUsernames) > 0}">
      <bs:messages key="settingsUpdated"/>
      <l:tableWithHighlighting id="vcsSettingsTable" className="settings" mouseovertitle="Click to edit VCS settings" highlightImmediately="true">
        <tr style="background-color: #f5f5f5;">
          <th class="vcsRoot">VCS root</th>
          <th colspan="3" class="username">Username</th>
        </tr>
        <c:forEach items="${vcsSettingsForm.specifiedVcsUsernames}" var="vcsUsername">
          <c:set var="onclick">BS.EditVcsUsername.showEditDialog('${vcsUsername.key}','<bs:escapeForJs text="${vcsUsername.username}" forHTMLAttribute="true"/>','<bs:forJs><profile:vcsDisplayName vcsUsername="${vcsUsername}"/></bs:forJs>')</c:set>
          <tr>
            <td class="highlight" onclick="${onclick}"><profile:vcsDisplayName vcsUsername="${vcsUsername}"/></td>
            <td class="highlight" onclick="${onclick}"><c:out value="${vcsUsername.username}"/></td>
            <td class="edit highlight" onclick="${onclick}"><a href="#" showdiscardchangesmessage="false" onclick="${onclick}; Event.stop(event)">edit</a>
            </td>
            <td class="edit"><a href="#" onclick="BS.EditVcsUsername.deleteUsername('${vcsUsername.key}'); return false">delete</a></td>
          </tr>
        </c:forEach>
      </l:tableWithHighlighting>
      </c:if>
      <c:if test="${fn:length(vcsSettingsForm.availableVcsUsernames) > 0}">
      <p>
        <c:set var="defaultValue"><bs:escapeForJs text="${vcsSettingsForm.owner.username}" forHTMLAttribute="true"/></c:set>
        <forms:addButton onclick="BS.EditVcsUsername.showAddDialog('${defaultValue}'); return false">Add new VCS username</forms:addButton>
      </p>
      </c:if>
      <profile:vcsSettingsForm form="${vcsSettingsForm}"/>
    </bs:refreshable>
  </jsp:attribute>
</bs:page>
