<%@ include file="include-internal.jsp" %><%@
    taglib prefix="agent" tagdir="/WEB-INF/tags/agent"

%><jsp:useBean id="agentGroups" scope="request" type="java.util.List<jetbrains.buildServer.controllers.agent.AgentGroup>"
/><jsp:useBean id="agentsForm" scope="request" type="jetbrains.buildServer.controllers.agent.AgentListForm"
/><c:set var="grouped" value="${agentsForm.actuallyGroupByPools}"
/><c:set var="tableClass">dark agents sortable borderBottom</c:set
><c:if test="${grouped}"><c:set var="tableClass">dark agents agents-grouped sortable borderBottom</c:set></c:if>

<table cellspacing="0" class="${tableClass}">
  <tr>
    <agent:poolTH/>
    <th class="buildAgentName sortable"><span id="SORT_BY_NAME">Agent</span></th>
    <c:set var="poolHeaderColSpan" value="2"/>
    <authz:authorize allPermissions="VIEW_AGENT_DETAILS">
      <c:set var="poolHeaderColSpan" value="3"/>
      <th class="cpuRank sortable" title="Shows estimated performance of the agent CPU">
        <span id="SORT_BY_CPU_RANK">CPU</span>
      </th>
    </authz:authorize>
    <th class="agentStatus sortable"><span id="SORT_BY_STATUS">Status</span></th>
    <th class="emptyCell">&nbsp;</th>
    <th class="buildName sortable"><span id="SORT_BY_RUNNING_BUILD">Running build</span></th>
    <th class="buildNumber"></th>
    <th class="status"></th>
    <th class="duration"></th>
    <th class="stopBuild"></th>
  </tr>

  <c:forEach var="agentGroup" items="${agentGroups}" varStatus="groupIndex">
    <c:set var="poolId" value="${agentGroup.poolId}"/>
    <c:if test="${grouped}">
      <tr>
        <td class="no-border firstCell"><bs:agentPoolHandle agentPoolId="${poolId}"/></td>
        <td colspan="${poolHeaderColSpan}" class="poolHeader"><bs:agentPoolLink agentPoolId="${poolId}"
                                                                                agentPoolName="${agentGroup.name}"
                                                                                groupHeader="${true}"/></td>
        <td class="emptyCell">&nbsp;</td>

        <c:set var="totalAgentCount" value="${agentGroup.totalCount}"/>
        <c:set var="buildCount" value="${agentGroup.buildCount}"/>
        <c:set var="idleAgentCount" value="${totalAgentCount - buildCount}"/>

        <td colspan="5" class="poolHeader">
          <span>
            <agent:agentGroupMug idleAgentCount="${idleAgentCount}"
                                 totalAgentCount="${totalAgentCount}"/><span class="commentText">
            <c:choose>
              <c:when test="${buildCount == 0}">All agents are idle</c:when>
              <c:when test="${buildCount == totalAgentCount}">All agents are busy</c:when>
              <c:otherwise>
                ${buildCount} build<bs:s val="${buildCount}"/> <bs:are_is val="${buildCount}"/> running,
                ${idleAgentCount} agent<bs:s val="${idleAgentCount}"/> <bs:are_is val="${idleAgentCount}"/> idle
              </c:otherwise>
            </c:choose></span>
          </span>
        </td>
      </tr>
    </c:if>

    <c:forEach var="buildAgent" items="${agentGroup.agents}" varStatus="agentIndex">
      <c:set var="rowClass" value=""/>
      <c:if test="${not buildAgent.enabled}">
        <c:set var="rowClass" value="disabledAgent"/>
      </c:if>
      <tr class="${rowClass} agentRow-${poolId} <c:if test="${grouped and agentIndex.last}">no-border</c:if>"
          id="agentRow:${buildAgent.id}" style='<bs:agentRowVisibility agentPoolId="${poolId}"/>'>
        <c:if test="${grouped}">
          <td class="emptyCell">&nbsp;</td>
        </c:if>
        <td class="buildAgentName">
          <bs:agentDetailsFullLink agent="${buildAgent}"
                                   doNotShowPoolInfo="${grouped}"
                                   doNotShowUnavailableStatus="${true}"/>
        </td>
        <authz:authorize allPermissions="VIEW_AGENT_DETAILS">
          <td class="cpuRank" title="Shows estimated performance of the agent CPU">
            <c:choose>
              <c:when test="${buildAgent.cpuBenchmarkIndex > 0}">${buildAgent.cpuBenchmarkIndex}</c:when>
              <c:otherwise>N/A</c:otherwise>
            </c:choose>
          </td>
        </authz:authorize>
        <td class="agentStatus">
          <jsp:include page="/agentStatus.html?id=${buildAgent.id}&tableMode=1"/>
        </td>
        <td class="emptyCell no-border">&nbsp;</td>
        <c:set var="agentBuild" value="${buildAgent.runningBuild}"/>
        <c:if test="${empty agentBuild}">
          <td colspan="5" class="idle">Idle</td>
        </c:if>
        <c:if test="${not empty agentBuild}">
          <authz:authorize allPermissions="VIEW_PROJECT" projectId="${agentBuild.projectId}">
            <jsp:attribute name="ifAccessGranted">
              <td class="buildName">
                <bs:buildTypeLinkFull buildType="${agentBuild.buildType}"/>
              </td>
              <bs:buildRow build="${agentBuild}" rowClass="${rowClass}"
                           showBranchName="false"
                           showBuildNumber="true"
                           showStatus="true"
                           showArtifacts="false"
                           showCompactArtifacts="false"
                           showChanges="false"
                           showProgress="true"
                           showStop="true"/>
            </jsp:attribute>
            <jsp:attribute name="ifAccessDenied">
              <td colspan="5">
                <img src="<c:url value='/img/buildStates/running_green_transparent.gif'/>" class="icon"/>
                Running a build. You do not have permissions to see the build details.
              </td>
            </jsp:attribute>
          </authz:authorize>
        </c:if>
      </tr>
    </c:forEach>
  </c:forEach>
</table>

<agent:restoreAgentBlockStates grouped="${grouped}"/>
<bs:changeAgentStatus agentActionCode="changeAgentStatus"/>
