/**
 * TextAreaExpander plugin for jQuery
 * v1.0
 * Expands or contracts a textarea height depending on the
 * quatity of content entered by the user in the box.
 *
 * By Craig Buckler, Optimalworks.net
 *
 * As featured on SitePoint.com:
 * http://www.sitepoint.com/blogs/2009/07/29/build-auto-expanding-textarea-1/
 *
 * Please use as you wish at your own risk.
 */

/**
 * Usage:
 *
 * From JavaScript, use:
 *     $(<node>).textAreaExpander(<minHeight>, <maxHeight>);
 *     where:
 *       <node> is the DOM node selector, e.g. "textarea"
 *       <minHeight> is the minimum textarea height in pixels (optional)
 *       <maxHeight> is the maximum textarea height in pixels (optional)
 *
 * Alternatively, in you HTML:
 *     Assign a class of "expand" to any <textarea> tag.
 *     e.g. <textarea name="textarea1" rows="3" cols="40" class="expand"></textarea>
 *
 *     Or assign a class of "expandMIN-MAX" to set the <textarea> minimum and maximum height.
 *     e.g. <textarea name="textarea1" rows="3" cols="40" class="expand50-200"></textarea>
 *     The textarea will use an appropriate height between 50 and 200 pixels.
 */

(function($) {

	// jQuery plugin definition
	$.fn.textAreaExpander = function(minHeight, maxHeight, source) {

		var hCheck = !($.browser.msie || $.browser.opera);

		// resize a textarea
		function resizeTextarea(e) {
			// event or initialize element?
			e = e.target || e;

			// find content length and box width
			var vlen = e.value.length, ewidth = e.offsetWidth;

			if (vlen != e.valLength || ewidth != e.boxWidth) {
				if (hCheck && (vlen < e.valLength || ewidth != e.boxWidth)) e.style.height = "0px";

				var h = Math.max(e.expandMin, Math.min(e.scrollHeight, e.expandMax));

                // When content is tall, allow the standard browser resizer in addition to auto expansion.
                // Addresses TW-19254
				if (e.scrollHeight > h) {
                  e.style.overflow = 'auto';
                  e.style.resize = 'both';
                }  else {
                  e.style.overflow = 'hidden';
                  e.style.resize = 'none';
                }

				e.style.height = Math.max(h, e.scrollHeight) + "px";

				e.valLength = vlen;
				e.boxWidth = ewidth;

                if (BS.MultilineProperties) {
                  if (!source || source != 'updateVisibility') {
                    BS.MultilineProperties.updateVisible();
                  }
                }
			}

			return true;
		};

		// initialize
		this.each(function() {
			// is a textarea?
			if (this.nodeName.toLowerCase() != "textarea") return;

			// set height restrictions
			var p = this.className.indexOf("expandable") != -1;
			this.expandMin = minHeight || (p ? parseInt('0'+p[1], 10) : 0);
			this.expandMax = maxHeight || (p ? parseInt('0'+p[2], 10) : 99999);

			// initial resize
			resizeTextarea(this);

			// zero vertical padding and add events
			if (!this.initialized) {
				this.initialized = true;
                var $el = $(this);
				$el.css("padding-top", 0).css("padding-bottom", 0);
                // Listen to mouseup to detect native resize
				$el.bind("keyup mouseup focus", resizeTextarea);
			}
		});

		return this;
	};

})(jQuery);
