BS.BuildChangesBlock = function(elementId) {
  BS.BlocksWithHeader.call(this, elementId, true);
};

_.extend(BS.BuildChangesBlock.prototype, BS.BlocksWithHeader.prototype);

_.extend(BS.BuildChangesBlock.prototype, {
  getBlockContentElement: function(id) {
    var el = $(id + "_changes");
    if (!el) return;

    return el;
  },

  onHideBlock: function(contentElement, id) {
    BS.BlocksWithHeader.prototype.onHideBlock.call(this, contentElement, id);
    BS.ChangesPopup.updatePopup();
  },

  onShowBlock: function(contentElement, id) {
    BS.BlocksWithHeader.prototype.onShowBlock.call(this, contentElement, id);
    BS.ChangesPopup.updatePopup();
  }
});
