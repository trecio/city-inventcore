BS.BuildLog = {
  $window: $j(window),
  $doc: $j(document),
  $content: $j('#bodyWrapper'),

  // Incremental update (all messages, tree view)
  startUpdates: function(buildId, initialCounter, isTree, pageName, additionalAttrs) {
    var counter = initialCounter;
    var that = this;
    var updater = new BS.PeriodicalUpdater(null, that.getUrl(buildId, counter, pageName, additionalAttrs), {
      frequency: 2,
      evalScripts: true,
      onSuccess: function(transport) {
        /* Add the response HTML piece, scroll to bottom if necessary. */
        var response = transport.responseText;
        var scrollAtBottom = that.isScrollAtBottom();

        $j('#buildLog').append(response);

        if (scrollAtBottom) {
          that.scrollToBottom();
        }
        /* Extract the number of messages to update the size. */
        var pos = response.lastIndexOf("<div class='hidden updateInfo'>");
        if (pos != -1) {
          var from = pos + 31;
          var to = response.indexOf('<', from);
          var newCounter = parseInt(response.substr(from, to - from));
          counter = isTree ? newCounter : counter + newCounter;
        }

        /* Check if build has finished. */
        var finished = response.indexOf('<div style="display: none;">Build finished</div>') != -1;
        if (finished) {
          updater.stop();
          that.$doc.off('mousewheel.buildLog keydown.buildLog');
          $j('#buildLogProgress').remove();
        }

        $j('.updateInfo').remove(); // Romove utility tags

        // Set a new counter now.
        updater.url = that.getUrl(buildId, counter, pageName, additionalAttrs + (isTree ? BS.BuildLogTree.getHrefParameter('state') : ''));
      }
    });

    this.$doc.on('mousewheel.buildLog', function() {
      that.isScrollPositionKnown = false;
    });

    this.$doc.on('keydown.buildLog', function(e) {
      // Up/Down/Left/Right/Home/End/PageUp/PageDown
      if (e.keyCode >= 33 && e.keyCode <= 40) {
        that.isScrollPositionKnown = false;
      }
    });
  },

  getVerticalScroll: function() {
    return this.$window.scrollTop();
  },

  getHorizontalScroll: function() {
    return this.$window.scrollLeft();
  },

  getContentHeight: function() {
    return this.$content.height();
  },

  getWindowHeight: function() {
    return this.$window.height();
  },

  isScrollPositionKnown: false,
  isScrollAtBottom: function() {
    if (!this.isScrollPositionKnown) {
      var height = this.getContentHeight();
      var scroll = this.getVerticalScroll();
      var innerHeight = this.getWindowHeight();
      this._isScrollAtBottom = Math.abs(innerHeight + scroll - height) < 20;
      this.isScrollPositionKnown = true;
    }

    return this._isScrollAtBottom;
  },

  scrollToBottom: function() {
    this.$window.scrollTop(this.getContentHeight());
  },

  getUrl: function(buildId, counter, pageName, additionalAttrs) {
    return window['base_uri'] + '/buildLog/' + pageName + '?' +
           'buildId=' + buildId +
           '&counter=' + counter +
           (additionalAttrs ? '&' + additionalAttrs : '');
  },

  initLineWrap: function() {
    var wrapToggle = $j('#wrapLines');

    wrapToggle.change(function() {
      var wrapEnabled = !!this.checked;
      $j('#buildResults').toggleClass('buildResultsWrap', wrapEnabled);

      if (wrapEnabled) {
        BS.Cookie.set('buildResultsWrap', '1', 365);
      } else {
        BS.Cookie.remove('buildResultsWrap');
      }
    });

    if (BS.Cookie.get('buildResultsWrap') == '1') {
      wrapToggle.click();
    }
  },

  // Full refresh (important messages, tail view)
  refreshRunning: false,
  enableRefresh: function() {
    var that = this;

    if (that.refreshRunning) {
      return;
    }
    that.refreshRunning = true;

    this.$doc.ready(function() {
      BS.PeriodicalRefresh.start(5, function() {
        var scrollAtBottom = that.isScrollAtBottom();
        $('buildResults').refresh(null, "runningBuildRefresh=1", function() {
          if (scrollAtBottom) {
            that.scrollToBottom();
          }
        });
      });
    });
  },

  // Updates build log size in the header
  minorUpdate: function(buildLogSize) {
    BS.BuildResults.minorUpdate();
    $j("#buildLogSizeEstimate").html(buildLogSize);
  }
};

$j(document).ready(function() {
  BS.BuildLog.initLineWrap();
});
