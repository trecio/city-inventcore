BS.ProblemsSummary = {
  _buildTypes: [],
  _lastUpdate: new Date().getTime(),
  _timeout: 0,

  requestBuildTypeUpdate: function(btId) {
    if (this._buildTypes.indexOf(btId) == -1) {
      this._buildTypes.push(btId);
    }

    this._scheduleUpdate();
  },

  requestUpdateAll: function() {
    var that = this;
    $j("#overviewMain").find("div.tableCaption").each(function() {
      var id = this.id;
      var btId = id.substr(0, id.length - 4);
      that.requestBuildTypeUpdate(btId);
    });
  },

  _scheduleUpdate: function() {
    if (!this._timeout) {
      var that = this;
      var timeDiff = new Date().getTime() - that._lastUpdate;
      var timeout = (timeDiff > 8000) ? 100 : Math.max(100, 8000 - timeDiff);
      this._timeout = setTimeout(this.update.bind(this), timeout);
    }
  },

  update: function() {
    var currentBuildTypes = this._buildTypes;
    this._buildTypes = [];
    this._lastUpdate = new Date().getTime();
    this._timeout = 0;

    if (!currentBuildTypes.length) {
      return;
    }

    var parameters = $j.extend({
      getProblemsSummary: currentBuildTypes.join(",")
    }, BS.Branch.getAllBranchesFromUrl());

    var that = this;
    BS.ajaxRequest(window['base_uri'] + "/ajax.html", {
      parameters: parameters,
      onSuccess: function(transport) {
        var root = BS.Util.documentRoot(transport);
        if (!root) {
          return;
        }

        var i;
        var buildTypeNodes = root.childNodes;
        for (i = 0; i < buildTypeNodes.length; ++i) {
          var node = buildTypeNodes[i];
          that._doUpdateInUI(node);

          // Remove from 'currentBuildTypes'.
          var btId = node.getAttribute("btId");
          currentBuildTypes.splice($j.inArray(btId, currentBuildTypes), 1);
        }

        // Hide others.
        for (i = 0; i < currentBuildTypes.length; ++i) {
          that.hideSummaryFor(currentBuildTypes[i]);
        }
      }
    });
  },

  _doUpdateInUI: function(node) {
    var btId = node.getAttribute("btId");
    var projectId = node.getAttribute("projectId");
    var failed = node.getAttribute("failed");
    var invest = node.getAttribute("invest");
    var fixed = node.getAttribute("fixed");
    var muted = node.getAttribute("muted");

    var caption = $j("#" + btId + "-div");
    var currentSummary = caption.find(".problemsSummary");

    currentSummary.html("");

    if (failed + invest + fixed + muted == 0) {
      return;
    }

    var currentSummaryExists = currentSummary.length;

    if (!currentSummaryExists) {
      currentSummary = $j('<span class="problemsSummary"/>').attr({
        title: "View problems summary for this build configuration"
      });
    }

    var link = $j('<a class="summaryLink"/>').attr({
      href: window['base_uri'] + "/project.html?projectId=" + projectId + "&tab=problems&buildTypeId=" + btId
    }).append("<span>Test<span class='problemDetails'> failure</span>s:</span> ");

    if (failed > 0) {
      this._getSpan("/img/buildStates/buildFailed.gif", failed, "not investigated", invest + fixed + muted > 0).appendTo(link);
    }
    if (invest > 0) {
      this._getSpan("/img/investigate.gif", invest, "under investigation", fixed + muted > 0).appendTo(link);
    }
    if (fixed > 0) {
      this._getSpan("/img/buildStates/fixedTestResp.gif", fixed, "marked as fixed", muted > 0).appendTo(link);
    }
    if (muted > 0) {
      this._getSpan("/img/muted-red.gif", muted, "muted", false).appendTo(link);
    }

    link.appendTo(currentSummary);

    if (!currentSummaryExists) {
      currentSummary.appendTo(caption)
    }
  },

  _getSpan: function(icon, num, details, comma) {
    var img = $j('<img class="problemIcon"/>').attr({
      src: window['base_uri'] + icon,
      alt: ""
    });
    var detailsSpan = $j('<span class="problemDetails"/>').html(details).append(comma ? ", " : "");
    return $j("<span/>").append(img).append(num).append(" ").append(detailsSpan);
  },

  hideSummaryFor: function(btId) {
    $j("#" + btId + "-div").find(".problemsSummary").html("");
  }
};
