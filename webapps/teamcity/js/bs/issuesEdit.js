BS.IssueProviderForm = OO.extend(BS.PluginPropertiesForm, OO.extend(BS.AbstractModalDialog, {
  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('newIssueProviderProgress');
    } else {
      BS.Util.hide('newIssueProviderProgress');
    }
  },

  getContainer: function() {
    return $('newIssueProviderFormDialog');
  },

  formElement: function() {
    return $('newIssueProviderForm');
  },

  showDialog: function() {
    BS.Util.reenableForm(this.formElement());
    this.showCentered();
    this.bindCtrlEnterHandler(this.submit.bind(this));
    $("noType").selected = 'selected';
    this.disableButton(true);
  },

  getSelectedType: function() {
    var idx = this.formElement().providerType.selectedIndex;
    return this.formElement().providerType.options[idx].value.toLowerCase();  // providerType
  },

  disableButton: function(value) {
    if (value) {
      $("createButton").disabled = 'disabled';
      $("connectionButton").disabled = 'disabled';
    } else {
      $("createButton").disabled = '';
      $("connectionButton").disabled = '';
    }
  },

  submit: function() {
    var providerType = this.getSelectedType();
    if (providerType == '') {
      return;
    }
    this.setSaving(true);
    var url = window['base_uri'] + "/admin/issueTracker/editIssueProvider.html?create=1&type=" + providerType;
    this.saveForm(url);

    return false;
  },

  saveForm: function(url) {
    var that = this;
    BS.PasswordFormSaver.save(this, url, OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function(form, responseXML, err) {
        that.setSaving(false);
        var wereErrors = BS.XMLResponse.processErrors(responseXML, {
          onProviderNotFoundError: function() {
            alert('Provider not found');
          }
        }, that.propertiesErrorsHandler);

        if (wereErrors) {
          BS.Util.reenableForm(that.formElement());
          return;
        }

        that.close();
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          BS.XMLResponse.processRedirect(responseXML);
        }
        $('providersTable').refresh();
        that.clearContent();
      }
    }));
  },

  clearContent: function() {
    $("newProviderDiv").innerHTML = "";
    $("editProviderDiv").innerHTML = "";
  },

  cancelDialog: function() {
    this.clearContent();
    this.close();
  },

  refreshDialog: function(pageUrl) {
    var that = this;
    var providerType = this.getSelectedType();
    if (providerType != '') {
      $('newProviderSaving').show();
      this.disableButton(true);
      var url = pageUrl + '&selectedType=' + providerType;
      BS.Refreshable.createRefreshFunction('newIssueProviderContainer', url, '')
          // TODO what is that?
          (false, false, function() {
             $('newProviderSaving').hide();
             that.disableButton(false);
             BS.VisibilityHandlers.updateVisibility("newIssueProviderContainer");
             BS.IssueProviderForm.recenterDialog();
          }
          );
    } else {
      $("newProviderDiv").innerHTML = "";
      this.disableButton(true);
    }
  },

  testConnection: function() {
    if (!$('host').value) {
      alert('Please, set the "host" property (and optionally credentials)');
      return;
    }

    var providerType = this.getSelectedType();
    if (providerType == '') {
      return;
    }
    var url = window['base_uri'] + "/admin/issueTracker/testConnection.html?type=" + providerType;
    BS.IssueProviderTestConnection.show(this, url);
  }
}));

BS.EditIssueProviderForm = OO.extend(BS.IssueProviderForm, {
  show: function(providerType, providerId) {
    this.providerType = providerType;
    this.providerId = providerId;

    $("editProviderDiv").innerHTML = BS.loadingIcon + " Loading settings... ";
    this.showDialog();

    var url = this.getUrl();
    BS.ajaxUpdater($("editProviderDiv"), url, {
      method: "get",
      evalScripts: true,
      onComplete: function() {
        BS.VisibilityHandlers.updateVisibility("editProviderDiv");
        BS.EditIssueProviderForm.recenterDialog();
      }
    });
  },

  getUrl: function() {
    return window['base_uri'] + "/admin/issueTracker/editIssueProvider.html?" +
           "type=" + this.providerType + "&providerId=" + this.providerId;
  },

  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('editIssueProviderProgress');
    } else {
      BS.Util.hide('editIssueProviderProgress');
    }
  },

  getContainer: function() {
    return $('editIssueProviderFormDialog');
  },

  formElement: function() {
    return $('editIssueProviderForm');
  },

  submit: function() {
    this.setSaving(true);
    var url = this.getUrl();
    this.saveForm(url);

    return false;
  },

  testConnection: function() {
    if (!$('host').value) {
      alert('Please, set the "host" property (and optionally credentials)');
      return;
    }

    var url = window['base_uri'] + "/admin/issueTracker/testConnection.html?type=" + this.providerType;
    BS.IssueProviderTestConnection.show(this, url);
  }
});

BS.IssueProviderTestConnection = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('testConnectionDialog');
  },

  formElement: function() {
    return $('testConnection');
  },

  show: function(form, url) {
    this.form = form;
    this.url = url;
    this.clearPreviousResults();
    this.showCentered();
    $("issueId").focus();
  },

  cancelDialog: function() {
    this.close();
  },

  clearPreviousResults: function() {
    $('issueId').value = "";
    $('issueDetails').innerHTML = "";
  },

  testConnection: function() {
    var issueId = $('issueId').value;
    if (!issueId) {
      alert('Please, set the Issue ID field');
      return;
    }

    this.form.setSaving(true);
    var params = this.form.serializeParameters() + "&issueId=" + encodeURIComponent(issueId);
    var resultElem = $('issueDetails');
    var that = this;
    BS.ajaxRequest(this.url, {
      method: "post",
      parameters: params,
      onComplete: function(transport) {
        that.form.setSaving(false);
        var errors = BS.XMLResponse.processErrors(transport.responseXML, {
          onFailureError: function(elem) {
            var message = elem.firstChild.nodeValue;
            resultElem.innerHTML = "<span class='err'>" + message + "</span>";

            // Some fancy UI stuff. TW-23282
            var width = $j(resultElem).children().width();
            if (width > $j(resultElem).width()) {
              $j("#testConnectionDialog").width(Math.min(width + 22, 700));
              that.showCentered();
            }
          },
          onPublicKeyExpiredError: function() {
            resultElem.innerHTML = "<span class='err'>Public key expired. " +
                                   "Please refresh the page.</span>";
          }
        });
        if (!errors) {
          resultElem.innerHTML = "<span style='color: green'>Success!</span>";
          var child = transport.responseXML.firstChild.firstChild;
          while (child) {
            resultElem.innerHTML += "<br><b>" + child.nodeName + "</b>: " + this.getElemContent(child);
            child = child.nextSibling; 
          }
        }
      },
      getElemContent: function(elem) {
        return (BS.Browser.msie) ? elem.firstChild.nodeValue : elem.textContent;
      }
    });
  },

  _makeLinks: function(text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
  }
}));

// A Jira specific feature: allows to prefill all available projects in a text field.
// Details: we send several values to the TeamCity to get the required data (server host, encrypted
// credentials and the public key). TeamCity performs the action and returns the list of projects
// in a XML file.
BS.getJiraProjects = function() {
  var host = $('host').value;
  if (!host) {
    alert('Please, set the "host" property (and optionally credentials)');
    return;
  }
  var user = $('username').value;

  var publicKey = $('publicKey').value;
  var pass = $('secure:password').value;
  var encyptedPass;
  if ($('prop:encrypted:secure:password').value != '') {
    encyptedPass = $('prop:encrypted:secure:password').value;
  } else {
    encyptedPass = BS.Encrypt.encryptData(pass, publicKey);
  }

  var prefixField = $('idPrefix');
  prefixField.value = '';
  BS.Util.show('getProjectsProgress');

  var url = window['base_uri'] + "/admin/jira/getProjects.html";
  BS.ajaxRequest(url, {
    method: "post",
    parameters: "user=" + user +
                "&pass=" + encyptedPass +
                "&publicKey=" + publicKey +
                "&host=" + encodeURIComponent(host),
    onComplete: function(transport) {
      var errors = BS.XMLResponse.processErrors(transport.responseXML, {
        onExceptionError: function(elem) {
          alert(elem.firstChild.nodeValue);
        },
        onRemoteAuthenticationError: function(elem) {
          alert(elem.firstChild.nodeValue);
        }
      });
      if (!errors) {
        var child = transport.responseXML.firstChild.firstChild;
        while (child) {
          if (prefixField.value.length > 0) {
            prefixField.value += ' ';
          }
          prefixField.value += child.attributes.getNamedItem('key').value;
          child = child.nextSibling;
        }
      }
      BS.Util.hide('getProjectsProgress');
    }
  });
};
