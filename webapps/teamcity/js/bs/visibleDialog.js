// Contains projects and build types visibility dialogs.

BS.AbstractVisibleDialog = OO.extend(BS.AbstractModalDialog, {
  alwaysReload: false,

  show: function(progressIcon) {
    this.changed = false;
    var that = this;
    if (progressIcon) progressIcon.css("visibility", "visible");
    BS.ajaxUpdater(this.getDialogContainer(), this.getRequestUrl(), {
      method: 'get',
      evalScripts: true,
      onComplete: function() {
        that.showCentered();
        that.bindCtrlEnterHandler(that.save.bind(that));
        that.updateButtons();
        if (progressIcon) progressIcon.css("visibility", "hidden");
      }
    });
  },

  save: function() {
    if (!this.canSave()) {
      return false;
    }

    BS.Util.show(this.getLoadingElemId());
    var form = this.getFormElement();
    Form.disable(form);

    this.getObjectsOrder().value = this.getVisibleProjectsOrder();
    var options = this.getVisibleObjects().options;
    for (var i = 0; i < options.length; i++) {
      options[i].selected = true;
    }
    var params = BS.Util.serializeForm(form);
    var that = this;
    BS.ajaxRequest(form.action, {
      parameters: params,
      onComplete: function() {
        Form.enable(form);
        BS.Util.hide(that.getLoadingElemId());
        that.close();
        if (that.alwaysReload || that.changed) {
          BS.reload(true);
        }
      }
    });

    return false;
  },

  canSave: function() {
    return true;
  },

  getVisibleProjectsOrder: function() {
    var order = "";
    var options = this.getVisibleObjects().options;
    for (var i = 0; i < options.length; i++) {
      if (order.length > 0) {
        order += ",";
      }
      order += options[i].getAttribute("value");
    }
    return order;
  },

  moveToVisible: function() {
    this.doMoveSelectedTo(this.getHiddenObjects(), this.getVisibleObjects(), this.visibleObjectsAreSorted());
  },

  moveToHidden: function() {
    var filterField = this.getHiddenObjectsFilter();
    var from = this.getVisibleObjects();
    var to = this.getHiddenObjects();
    if (filterField.value == filterField.defaultValue || filterField.value == "") {
      this.doMoveSelectedTo(from, to, true);
    } else {
      filterField.value = "";
      BS.InPlaceFilter.applyFilter(this.getHiddenObjectsId(), filterField, function() {
        this.doMoveSelectedTo(from, to, true);
        filterField.value = filterField.defaultValue;
      });
    }
  },

  moveUp: function() {
    this.moveSelected(this.getVisibleObjects(), -1);
  },

  moveDown: function() {
    this.moveSelected(this.getVisibleObjects(), 1);
  },

  doMoveSelectedTo: function(from, to, sort) {
    this.doMoveTo(from, to, sort, function(option) {
      return option.selected;
    });
  },

  doMoveTo: function(from, to, sort, condition) {
    this.changed = true;
    this.selectAll(to, false);
    var toRemove = [];
    var that = this;
    this.iterateOptions(from, function(option) {
      if (condition(option)) {
        var copy = that.createCopy(option);
        if (sort) {
          that.insertToSorted(copy, to);
        } else {
          to.appendChild(copy);
        }
        toRemove.push(option);
      }
    });
    for (var j = 0; j < toRemove.length; j++) {
      from.removeChild(toRemove[j]);
    }
    this.updateButtons();
    to.activate();
  },

  selectAll: function(select, selected) {
    this.iterateOptions(select, function(option) {
      option.selected = selected;
    });
  },

  iterateOptions: function(select, handler) {
    for (var k = 0; k < select.options.length; k++) {
      handler(select.options[k]);
    }
  },

  insertToSorted: function(option, select) {
    if (option.className.indexOf("inplaceFiltered") == -1) {
      option.className += " inplaceFiltered";
    }
    var options = select.options;
    for (var i = 0; i < options.length; i++) {
      if (option.innerHTML.toLowerCase() < options[i].innerHTML.toLowerCase()) {
        select.insertBefore(option, options[i]);
        return;
      }
    }
    select.appendChild(option);
  },

  createCopy: function(option) {
    var newOption = document.createElement("option");
    newOption.value = option.value;
    newOption.innerHTML = option.innerHTML;
    newOption.selected = option.selected;
    newOption.className = option.className;
    return newOption;
  },

  moveSelected: function(select, dir) {
    var options = select.options;
    var i = dir == 1 ? options.length-1 : 0;
    var canMove = false;
    for (var k = 0; k < options.length; k++) {
      var option1 = options[i];
      if (option1.selected) {
        if (canMove) {
          var option2 = options[i+dir];
          var copy1 = this.createCopy(option1);
          var copy2 = this.createCopy(option2);
          select.replaceChild(copy2, option1);
          select.replaceChild(copy1, option2);
          this.changed = true;
        }
      } else {
        canMove = true;
      }
      i -= dir;
    }
    this.updateButtons();
  },

  updateButtons: function() {
    this.getMoveToVisible().disabled = !this.hasSelection(this.getHiddenObjects());
    var visibleSelected = this.hasSelection(this.getVisibleObjects());
    this.getMoveToHidden().disabled = !visibleSelected;
    if (!this.visibleObjectsAreSorted()) {
      this.getMoveVisibleUp().disabled = !visibleSelected;
      this.getMoveVisibleDown().disabled = !visibleSelected;
    }
  },

  visibleObjectsAreSorted: function() {
    return false;
  },

  hasSelection: function(select) {
    var options = select.options;
    for (var i = 0; i < options.length; i++) {
      if (options[i].selected) {
        return true;
      }
    }
    return false;
  },

  getContainer: function() {
    return $(this.getPrefix() + 'visibleFormDialog');
  },

  getFormElement: function() {
    return $(this.getPrefix() + 'visibleForm');
  },

  getDialogContainer: function() {
    return $(this.getPrefix() + 'visibleDialogContainer');
  },

  getVisibleObjects: function() {
    return $(this.getPrefix() + 'visible');
  },

  getHiddenObjectsId: function() {
    return this.getPrefix() + 'hidden';
  },

  getHiddenObjects: function() {
    return $(this.getHiddenObjectsId());
  },

  getHiddenObjectsFilter: function() {
    return this.getHiddenObjectsId() + '_filter';
  },

  getObjectsOrder: function() {
    return $(this.getPrefix() + 'order');
  },

  getLoadingElemId: function() {
    return this.getPrefix() + 'savingVisible';
  },

  getMoveToVisible: function() {
    return $(this.getPrefix() + 'moveToVisible');
  },

  getMoveToHidden: function() {
    return $(this.getPrefix() + 'moveToHidden');
  },

  getMoveVisibleUp: function() {
    return $(this.getPrefix() + 'moveVisibleUp');
  },

  getMoveVisibleDown: function() {
    return $(this.getPrefix() + 'moveVisibleDown');
  },

  getPrefix: function() {
  },

  getRequestUrl: function() {
  }
});

BS.VisibleProjectsDialog = OO.extend(BS.AbstractVisibleDialog, {
  getPrefix: function() {
    return "projects_";
  },

  getRequestUrl: function() {
    return window['base_uri'] + '/visibleProjects.html?init=1';
  }
});

BS.VisibleBuildTypesDialog = OO.extend(BS.AbstractVisibleDialog, {
  projectId: '',

  showForProject: function(projectId) {
    this.projectId = projectId;
    var progressIcon = $j("#vp_" + projectId);
    this.show(progressIcon);
  },

  getPrefix: function() {
    return "bt_";
  },

  getRequestUrl: function() {
    return window['base_uri'] + '/visibleBuildTypes.html?projectId=' + this.projectId;
  },

  canSave: function() {
    var s = this.getVisibleProjectsOrder();
    var result = s && (s.length > 0);
    if (!result) {
      alert("Hiding all build configurations is not allowed");
    }
    return result;
  },

  resetAction: function() {
    if (!confirm("Your order settings will be reset to defaults. Are you sure?")) {
      return;
    }

    var that = this;
    BS.Util.show(this.getLoadingElemId());
    var form = this.getFormElement();
    Form.disable(form);

    BS.ajaxRequest(form.action, {
      parameters: 'reset=true',
      onComplete: function() {
        Form.enable(form);
        BS.Util.hide(that.getLoadingElemId());
        that.close();
        BS.reload(true);
      }
    });
  },

  showSingleBuildType: function(projectId, buildTypeId) {
    var url = window['base_uri'] + '/visibleBuildTypes.html';
    BS.ajaxRequest(url, {
      parameters: {
        projectId: projectId,
        bt_visible: buildTypeId,
        showOne: "true"
      },
      onComplete: function() {
        // For now just reload.
        BS.reload(true);
      }
    });
    return false;
  }
});

BS.hideBuildType = function(projectId, buildTypeId) {
  var url = window['base_uri'] + '/visibleBuildTypes.html';
  BS.ajaxRequest(url, {
    parameters: {
      projectId: projectId,
      bt_visible: buildTypeId,
      hideOne: "true"
    },
    onComplete: function() {
      jQuery(BS.Util.escapeId(buildTypeId + "-div"))
          .add(BS.Util.escapeId("btb" + buildTypeId))
          .fadeOut(1000);
      var hiddenBuildTypesCounter = jQuery(BS.Util.escapeId("p_" + projectId))
          .find("table td.hideProject span")
          .contents()
          .filter(function() { return this.nodeType == 3; });
      var text = hiddenBuildTypesCounter.text();
      var newText;
      if (text.indexOf("no") >= 0) {
        newText = "1 hidden";
      } else {
        var num = parseInt(text.split(" ")[0]);
        newText = (num + 1) + " hidden";
      }

      // Highlight.
      setTimeout(function() {
        var parent = hiddenBuildTypesCounter.parent();
        parent.prepend(newText);
        hiddenBuildTypesCounter.remove();

        new Effect.Highlight(parent[0]);
      }, 1100);
    }
  });
  return false;
};
