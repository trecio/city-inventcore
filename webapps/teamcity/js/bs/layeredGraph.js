BS.LayeredGraph = function() {
  this.layers = [];
  this.nodes = [];

  this.Layer = function() {
    this.nodes = [];
    this.width = 0;
  };

  this.Node = function(elem, layerIdx, fakeNode) {
    this.elem = $(elem);
    this.id = this.elem.id;
    this.layerIdx = layerIdx;
    this.dependencies = [];
    this.dependedOnMe = [];
    this.fakeNodeFlag = fakeNode;
    this.numDependedOnMe = 0;
    this.numDependencies = 0;
    this._numDifferentLines = 0;
    this._numIncomingLines = 0;
    this._outgoingLines = [];
    this._graph = null;

    this.fakeNode = function() {
      return this.fakeNodeFlag;
    };

    this.addDependency = function(id) {
      var node = this._graph.nodes[id];
      if (node == null) alert("Cannot find node for id: " + id);
      this.dependencies[id] = node;
      this.numDependencies++;
      node.dependedOnMe[this.id] = this;
      node.numDependedOnMe++;
    };

    this.getDependencies = function() {
      var res = [];
      for (var id in this.dependencies) {
        if (!this.dependencies.hasOwnProperty(id)) continue;
        res.push(this.dependencies[id]);
      }
      return res;
    };

    this.getDependedOnMe = function() {
      var res = [];
      for (var id in this.dependedOnMe) {
        if (!this.dependedOnMe.hasOwnProperty(id)) continue;
        res.push(this.dependedOnMe[id]);
      }
      return res;
    }
  };

  this.addNode = function (elem, layerIdx, fakeNode) {
    var layer = this.layers[layerIdx];
    if (layer == null) {
      layer = new this.Layer();
      this.layers[layerIdx] = layer;
    }
    var node = new this.Node($(elem), layerIdx, fakeNode);
    node._graph = this;
    layer.nodes.push(node);
    this.nodes[node.id] = node;
    return node;
  };

  this.getNodes = function() {
    var res = [];
    for (var id in this.nodes) {
      if (!this.nodes.hasOwnProperty(id)) continue;
      res.push(this.nodes[id]);
    }
    return res;
  }
};

BS.LayeredGraphDrawer = function(graph, options) {
  this.options = {
    canvasPadding: [2, 2], // left & top only
    horizontalSpacing: 60,
    verticalSpacing: 20,
    arrowDimensions: [5, 5]
  };

  _.extend(this.options, options);

  this.graph = graph;

  this.Point = function (x,y) {
    this.x = x;
    this.y = y;
  };

  this.paths = {
    absCubicBezier: function(x1, y1, x2, y2, end) {
      return "C" + x1 + " " + y1 + " " + x2 + " " + y2 + " " + end.x + " "+ end.y
    },

    absMove: function(point) {
      return "M" + point.x + " " + point.y;
    },

    absLine: function(point) {
      return "L" + point.x + " " + point.y;
    }
  };

  this.drawGraph = function(canvasElem, callback) {
    canvasElem = $(canvasElem);
    canvasElem.setStyle({'visibility': 'hidden'});
    BS.LoadStyleSheetDynamically(window['base_uri'] + '/css/layeredGraph.css', function() {
      this._repositionNodes();
      this._initCanvas($(canvasElem));
      this._drawEdges();
      this._attachEventHandlers();
      canvasElem.setStyle({'visibility': 'visible'});
      callback();
    }.bind(this));
  };

  this._repositionNodes = function() {
    var xPos = this.options.canvasPadding[0];
    for (var i=0; i<this.graph.layers.length; i++) {
      var layer = this.graph.layers[i];
      var maxWidth = 0;
      var yPos = this.options.canvasPadding[1];
      for (var j=0; j<layer.nodes.length; j++) {
        var ln = layer.nodes[j];
        maxWidth = Math.max(maxWidth, ln.elem.offsetWidth);
        BS.Util.place(ln.elem, xPos, yPos);

        yPos += ln.elem.offsetHeight + this.options.verticalSpacing;
      }

      layer.width = maxWidth + this.options.horizontalSpacing;
      layer.maxNodeWidth = maxWidth;
      xPos += layer.width;
    }
  },

  this._initCanvas = function (canvasEl) {
    canvasEl.innerHTML = "";
    this.rootEl = canvasEl.parentNode;
    var height = 0;
    var width = 0;
    for (var i=0; i<this.graph.layers.length; i++) {
      var layer = this.graph.layers[i];
      width += layer.width;
      for (var j=0; j<layer.nodes.length; j++) {
        height = Math.max(height, layer.nodes[j].elem.offsetTop + layer.nodes[j].elem.offsetHeight);
      }
    }

    //alert((width + 100) + ", " + (height + 100));
    this.paper = Raphael(canvasEl, width, height);
  };


  this._computeGroups = function(layer) {
    var groups = this._groupByParents(layer);
    var origFactor = this._groupsFactor(groups);
    while (true) {
      var groupsChanged = false;
      for (var i=0; i<groups.length; i++) {
        var gr1 = groups[i];
        if (gr1 == null) continue;

        for (var j=i+1; j<groups.length; j++) {
          var gr2 = groups[j];
          if (gr2 == null) continue;

          var chIntersection = _.intersection(gr1.children, gr2.children);
          if (chIntersection.length == 0) continue;

          var factor = this._groupsFactor([gr1, gr2]);

          var newGroups = [];
          newGroups.push({children: chIntersection, parents: gr1.parents.concat(gr2.parents)});
          var gr1Group = this._extractGroup(gr1, _.difference(gr1.children, chIntersection));
          if (gr1Group != null) newGroups.push(gr1Group);
          var gr2Group = this._extractGroup(gr2, _.difference(gr2.children, chIntersection));
          if (gr2Group != null) newGroups.push(gr2Group);

          var newFactor = this._groupsFactor(newGroups);
          if (newFactor < factor) {
            delete groups[i];
            delete groups[j];
            groups = groups.concat(newGroups);
            groupsChanged = true;
            break;
          }
        }

        if (groupsChanged) break;
      }

      if (!groupsChanged) break;
    }

    var result = [];
    for (var i=0; i<groups.length; i++) {
      if (groups[i] != null) result.push(groups[i]);
    }

    if (this._groupsFactor(result) < origFactor) {
      //alert(origFactor + ", " + this._groupsFactor(result));
    }

    return result;
  };

  this._extractGroup = function(origGroup, newChildren) {
    if (newChildren.length == 0) return null;
    var group = {children: newChildren, parents: []};
    for (var i=0; i<newChildren.length; i++) {
      var child = newChildren[i];
      var intersParents = _.intersection(origGroup.parents, child.getDependedOnMe());
      group.parents = group.parents.concat(intersParents);
    }
    return group;
  };

  this._groupByParents = function(layer) {
    var result = [];
    for (var i=0; i < layer.nodes.length; i++) {
      var parent = layer.nodes[i];
      var group = {parents: [parent], children: parent.getDependencies()};
      result.push(group);
    }

    return result;
  };

  this._groupsFactor = function(groups) {
    var factor = 0;
    for (var i=0; i<groups.length; i++) {
      factor += groups[i].parents.length + groups[i].children.length;
    }
    return factor;
  };

  this._maxYPos = function(nodes) {
    var max = 0;
    for (var n=0; n<nodes.length; n++) max = Math.max(max, nodes[n].elem.offsetTop);
    return max;
  };

  this._drawEdges = function() {
    for (var i=1; i<this.graph.layers.length; i++) {
      var prevLayer = this.graph.layers[i-1];
      var layer = this.graph.layers[i];

      var trunkDims = function(group) {
        var all = [].concat(group.children).concat(group.parents);
        var minY = 100000;
        var maxY = 0;
        for (var m=0; m<all.length; m++) {
          minY = Math.min(minY, all[m].elem.offsetTop);
          maxY = Math.max(maxY, all[m].elem.offsetTop);
        }
        return [minY, maxY];
      };

      var numIntersections = function(group1, dims1, group2, dims2) {
        var startY = Math.max(dims1[0], dims2[0]);
        var endY = Math.min(dims1[1], dims2[1]);

        var res = 0;
        for (var m=0; m<group1.parents.length; m++) {
          var y = group1.parents[m].elem.offsetTop;
          if (y >= startY && y <= endY) res++; // intersection found
        }

        for (var m=0; m<group2.children.length; m++) {
          var y = group2.children[m].elem.offsetTop;
          if (y >= startY && y <= endY) res++; // intersection found
        }

        return res;
      };

      var grouped = this._computeGroups(layer);
      grouped.sort(function(group1, group2) {
        var dims1 = trunkDims(group1);
        var dims2 = trunkDims(group2);
        if (dims1[0]+dims1[1] < dims2[0]) return -1; // do not intersect
        if (dims2[0]+dims2[1] < dims1[0]) return 1; // do not intersect

        // probably intersect
        var num1 = numIntersections(group1, dims1, group2, dims2);
        var num2 = numIntersections(group2, dims2, group1, dims1);

        if (num1 < num2) return -1;
        return 1;
      });

      var groupsMap = this._computeGroupsMap(grouped, layer.nodes, prevLayer.nodes);

      var maxTrunks = grouped.length;
      var trunkWidth = Math.round(this.options.horizontalSpacing / (maxTrunks+1));
      if (trunkWidth < this.options.arrowDimensions[0]) trunkWidth = this.options.arrowDimensions[0]+1;
      var trunkXMax = prevLayer.nodes[0].elem.offsetLeft + prevLayer.maxNodeWidth + this.options.horizontalSpacing - trunkWidth;;

      var trunkIdx = 1;
      for (var j=0; j<grouped.length; j++) {
        var group = grouped[j];

        var trunkX = prevLayer.nodes[0].elem.offsetLeft + prevLayer.maxNodeWidth + (trunkIdx++) * trunkWidth;
        if (trunkX > trunkXMax) trunkX = trunkXMax;

        var curGroup;
        for (var k=0; k<group.children.length; k++) {
          var dep = group.children[k];

          var edgesDiff = Math.round(dep.elem.offsetHeight / (groupsMap[dep.id].length+1));
          var lineIdx = groupsMap[dep.id].indexOf(group);

          var depYPos = dep.fakeNode() ? dep.elem.offsetTop : dep.elem.offsetTop + edgesDiff * (lineIdx + 1);
          //alert(depYPos + ", " + edgesDiff + ", " + dep.elem.offsetHeight);

          for (var m=0; m<group.parents.length; m++) {
            var parent = group.parents[m];
            var parEdgesDiff = Math.round(parent.elem.offsetHeight / (groupsMap[parent.id].length+1));

            if (curGroup != group) {
              parent._numIncomingLines++;
            }
            var parYPos = parent.fakeNode() ? parent.elem.offsetTop : parent.elem.offsetTop + parEdgesDiff * parent._numIncomingLines - this.options.arrowDimensions[1];
            parYPos = Math.max(parent.elem.offsetTop, parYPos);
            parYPos = Math.min(parent.elem.offsetTop+parent.elem.offsetHeight, parYPos);

            var depXPos = dep.fakeNode() ? dep.elem.offsetLeft : dep.elem.offsetLeft + dep.elem.offsetWidth;

            var points = [
              new this.Point(depXPos, depYPos),
              new this.Point(trunkX, depYPos),
              new this.Point(trunkX, parYPos),
              new this.Point(parent.elem.offsetLeft, parYPos)
            ];

            if (!parent.fakeNode()) {
              points = points.concat([
                new this.Point(parent.elem.offsetLeft - this.options.arrowDimensions[0], parYPos - this.options.arrowDimensions[1]),
                new this.Point(parent.elem.offsetLeft, parYPos),
                new this.Point(parent.elem.offsetLeft - this.options.arrowDimensions[0], parYPos + this.options.arrowDimensions[1])
              ]);
            }

            var line = this._drawLines(points);
            dep._outgoingLines[parent.id] = line;
            line._setSelected = function(selected) {
              if (selected) {
                this.attr({stroke: "#333"});
                this.data('bgLine').toFront();
                this.toFront();
              } else {
                this.attr({stroke: "#ccc"});
                this.toBack();
                this.data('bgLine').toBack();
              }
            }
          }

          dep._numDifferentLines++;
          curGroup = group;
        }
      }
    }
  };

  this._computeGroupsMap = function(groups, parents, children) {
    var result = [];
    for (var i=0; i<parents.length; i++) {
      var p = parents[i];
      result[p.id] = [];
      for (var j=0; j<groups.length; j++) {
        if (groups[j].parents.indexOf(p) != -1) result[p.id].push(groups[j]);
      }
    }

    for (var i=0; i<children.length; i++) {
      var c = children[i];
      result[c.id] = [];
      for (var j=0; j<groups.length; j++) {
        if (groups[j].children.indexOf(c) != -1) result[c.id].push(groups[j]);
      }
    }

    for (var j=0; j<children.length; j++) {
      var node = children[j];

      var nearestByYPos = function(node, neighbors) {
        var yPos = node.elem.offsetTop;
        var delta = 1000000;
        var curNearest;
        for (var z=0; z<neighbors.length; z++) {
          var nYPos = neighbors[z].elem.offsetTop;

          if (Math.abs(nYPos - yPos) < delta) {
            delta = Math.abs(nYPos - yPos);
            curNearest = neighbors[z];
          }
        }
        return curNearest;
      };

      var compareGroups = function(group1, group2) {
        var n1 = nearestByYPos(node, group1.parents);
        var n2 = nearestByYPos(node, group2.parents);

        return n1.elem.offsetTop - n2.elem.offsetTop;
      }.bind(this);

      result[node.id].sort(compareGroups);
    }

    return result;
  };

  this._attachEventHandlers = function() {
    var nodes = this.graph.getNodes();
    for (var i=0; i<nodes.length; i++) {
      nodes[i]._selectedNodes = [];
      nodes[i].setSelected = function(selected, curNode, processed, direction, prevNode) {
        if (processed[this.id]) return;
        processed[this.id] = true;

        var newSelected = [];
        var prevRemoved = false;
        for (var j=0; j<this._selectedNodes.length; j++) {
          if (!selected && this._selectedNodes[j] == prevNode.id && !prevRemoved) {
            prevRemoved = true;
            continue;
          }
          newSelected.push(this._selectedNodes[j]);
        }
        if (selected) {
          newSelected.push(prevNode.id);
        }

        this._selectedNodes = newSelected;

        var parents = this.getDependedOnMe();
        if (this == curNode) {
          for (var j=0; j<parents.length; j++) {
            parents[j].setSelected(selected, curNode, processed, 1, this);
          }

          var children = this.getDependencies();
          for (var j=0; j<children.length; j++) {
            children[j].setSelected(selected, curNode, processed, -1, this);
          }
        } else if (this.fakeNode()) {
          var n;
          if (direction > 0) {
            n = parents;
          } else {
            n = this.getDependencies();
          }

          for (var j=0; j<n.length; j++) {
            n[j].setSelected(selected, curNode, processed, direction, this);
          }
        }

        if (curNode == this) {
          if (selected) {
            this.elem.style.backgroundColor = "#ffffcc";
            this._explicitlySelected = true;
          } else {
            this._explicitlySelected = false;
          }
        }

        if (!this._explicitlySelected) {
          if (this._selectedNodes.length > 0) {
            this.elem.style.backgroundColor = "#eee";
          } else {
            this.elem.style.backgroundColor = "#fff";
          }
        }

        if (this.fakeNode() || this == curNode || direction < 0) {
          var from = [this];
          var to = direction < 0 ? [prevNode] : parents;

          for (var j=0; j<from.length; j++) {
            for (var k=0; k<to.length; k++) {
              var line = from[j]._outgoingLines[to[k].id];
              if (line == null) continue;

              if (selected || this._explicitlySelected) line._setSelected(true);
              else if (this._selectedNodes.length == 0 || (!this.fakeNode() && this._selectedNodes.indexOf(to[k].id) == -1)) line._setSelected(false);
            }
          }
        }
      };

      nodes[i].elem.onclick = function() {
        this.setSelected(!this._explicitlySelected, this, [], 0, this);
      }.bind(nodes[i]);

      nodes[i].setCurrent = function() {
        this.elem.style.border = "2px solid darkorange";
      }
    }
  };

  this._drawLines = function(points) {
    var pathString = this.paths.absMove(points[0]);
    for (var i=1; i<points.length; i++) {
      var p = points[i];
      if (p) pathString += this.paths.absLine(p);
    }

    try {
      // A background line fixes various kinds of rendering artefacts
      var bgLine = this.paper.path(pathString);
      bgLine.attr({stroke: '#FFF', 'stroke-width': 2});
      var line = this.paper.path(pathString);
      line.attr({stroke:"#ccc"});
      line.data('bgLine', bgLine);
      return line;
    } catch (e) {
      alert(pathString);
    }
  }
};
