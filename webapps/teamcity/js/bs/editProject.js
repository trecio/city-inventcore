
BS.SaveProjectListener = OO.extend(BS.ErrorsAwareListener, {

  onEmptyProjectNameError: function(elem) {
    $("errorName").innerHTML = elem.firstChild.nodeValue;
    this.getForm().highlightErrorField($("name"));
  },

  onInvalidProjectNameError: function(elem) {
    $("errorName").innerHTML = elem.firstChild.nodeValue;
    this.getForm().highlightErrorField($("name"));
  },

  onProjectRenameFailedError: function(elem) {
    $("errorName").innerHTML = elem.firstChild.nodeValue;
    this.getForm().highlightErrorField($("name"));
  },

  onSaveProjectErrorError: function(elem) {
    $("errorName").innerHTML = elem.firstChild.nodeValue;
  },

  onProjectNotFoundError: function(elem) {
    BS.reload(true);
  },

  onCompleteSave: function(form, responseXML, err) {
    BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
    if (!err) {
      BS.reload(true);
    }
  }
});

BS.CreateProjectForm = OO.extend(BS.AbstractWebForm, {
  submitCreateProject: function() {
    var that = this;

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SaveProjectListener, {
      getForm: function() {
        return that;
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        if (!err) {
          BS.XMLResponse.processRedirect(responseXML);
        }
      }
    }));

    return false;
  }
});


BS.EditProjectForm = OO.extend(BS.AbstractWebForm, {
  formElement: function() {
    return $('editProjectForm');
  },

  setupEventHandlers: function() {
    var that = this;

    this.setUpdateStateHandlers({
      updateState: function() {
        that.saveInSession();
      },
      saveState: function() {
        that.submitProject();
      }
    });
  },

  saveInSession: function() {
    var that = this;
    $("submitProject").value = 'storeInSession';

    BS.FormSaver.save(this, this.formElement().action, BS.StoreInSessionListener);
  },

  submitProject: function() {
    var that = this;
    $("submitProject").value = 'store';

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SaveProjectListener, {
      getForm: function() {
        return that;
      }
    }));

    return false;
  }
});

BS.VcsSettingsForm = OO.extend(BS.PluginPropertiesForm, {
  formElement: function() {
    return $('vcsSettingsForm');
  },

  setupEventHandlers: function() {
    var that = this;

    this.setUpdateStateHandlers({
      updateState: function() {
        that.saveInSession();
      },

      saveState: function() {
        that.submitVcsSettings(false);
      }
    });

  },

  saveInSession: function() {
    $("submitVcsRoot").value = 'storeInSession';

    BS.PasswordFormSaver.save(this, this.formElement().action, BS.StoreInSessionListener, false);
  },

  setSelectedVcs: function(vcsName) {
    if (vcsName == '') return;

    BS.ajaxRequest(this.formElement().action, {
      parameters: "&vcsName=" + vcsName + "&vcsRootId=" + $('vcsRootId').value + "&editingScope=" + $('editingScope').value,
      onComplete: function() {
        $('vcsRootProperties').refresh('chooseVcsTypeProgress');
      }
    });
  },

  submitVcsSettings: function(newSettings) {
    var vcsNameSelector = this.formElement().vcsName;
    if (newSettings && vcsNameSelector.selectedIndex == 0) {
      alert("Please choose a type of the VCS root.");
      vcsNameSelector.focus();
      return false;
    }

    var that = this;
    $("submitVcsRoot").value = 'store';

    var saveOption = this.formElement().saveOption;
    if (saveOption && saveOption.length > 0) {
      var saveOptionChecked = false;
      for (var i=0; i<saveOption.length; i++) {
        if (saveOption[i].checked) {
          saveOptionChecked = true;
          break;
        }
      }

      if (!saveOptionChecked) {
        alert("Please choose corresponding option for applying changes in this VCS root.");
        return false;
      }
    }

    this.removeUpdateStateHandlers();
    this.clearErrors();

    BS.PasswordFormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function(form, responseXML, err) {
        var wereErrors = BS.XMLResponse.processErrors(responseXML, {
          onProjectNotFoundError: function(elem) {
            alert(elem.firstChild.nodeValue);
            BS.XMLResponse.processRedirect(responseXML);
          },
          onDuplicateVcsRootError: function(elem) {
            $('errorVcsRootName').innerHTML = elem.firstChild.nodeValue;
            that.highlightErrorField($('vcsRootName'));
          },
          onInvalidModificationCheckIntervalError: function(elem) {
            $('invalidModificationCheckInterval').innerHTML = elem.firstChild.nodeValue;
            that.highlightErrorField($('modificationCheckInterval'));
          },
          onInvalidBranchSpecError: function(elem) {
            $('error_teamcity:branchSpec').innerHTML = elem.firstChild.nodeValue;
          },
          onVcsRootsSaveFailureError: function(elem) {
            alert(elem.firstChild.nodeValue);
          }
        }, that.propertiesErrorsHandler);

        if (wereErrors) {
          form.enable();
          BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
          if (!newSettings) {
            that.setupEventHandlers();
          }

          BS.MultilineProperties.updateVisible();
        } else {
          BS.XMLResponse.processRedirect(responseXML);
        }
      }
    }));

    return false;
  },

  submitTestConnection: function(newSettings) {
    var that = this;
    $("submitVcsRoot").value = 'testConnection';

    this.removeUpdateStateHandlers();
    this.clearErrors();

    BS.PasswordFormSaver.save(this, this.formElement().action, OO.extend(BS.SimpleListener, {
      onBeginSave: function(form) {
        form.setSaving(true);
        form.disable();
      },

      onFailedTestConnectionError: function(elem) {
        var text = "";
        if (elem.firstChild) {
          text = elem.firstChild.nodeValue;
        }

        BS.TestConnectionDialog.show(false, text, $('testConnectionButton'));
      },

      onCompleteSave: function(form, responseXML, err) {
        var wereErrors = BS.XMLResponse.processErrors(responseXML, {}, that.propertiesErrorsHandler);
        if (!wereErrors) {
          var additionalInfo = "";
          var testConnectionResultNodes = responseXML.documentElement.getElementsByTagName("testConnectionResult");
          if (testConnectionResultNodes && testConnectionResultNodes.length > 0) {
            var testConnectionResult = testConnectionResultNodes.item(0);
            if (testConnectionResult.firstChild) {
              additionalInfo = testConnectionResult.firstChild.nodeValue;
            }
          }

          BS.TestConnectionDialog.show(true, additionalInfo, $('testConnectionButton'));
        }

        form.setSaving(false);
        form.enable();
        if (!newSettings) {
          that.setupEventHandlers();
        }

        BS.MultilineProperties.updateVisible();
      }
    }));

    return false;
  },

  checkForModifications: function() {
    $("submitVcsRoot").value = 'schedule';
    var form = this.formElement();
    var submitUrl = this.formElement().action;
    var params = this.serializeParameters();

    BS.ajaxRequest(submitUrl, {
      method: "post",

      parameters: params
    });
    return false;
  }

});
