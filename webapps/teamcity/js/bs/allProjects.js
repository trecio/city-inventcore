BS.projectMap = {};
BS.typeVisibilityMap = {};

BS.BuildTypesPopup = function(projectId) {
  this.projectId = projectId;
  this.buildTypes = BS.projectMap[projectId];

  BS.Popup.call(this, projectId + "btPopup", {
    shift: {x: 30, y: -5},
    delay: 0,
    className: 'buildTypesPopup quickLinksMenuPopup custom-scroll',
    hideOnMouseOut: true,
    fitWithinWindowHeight: true,
    fitWithinWindowBottomMargin: 100,
    doScroll: false,

    textProvider: function(popup) {
      var visibilityMap = BS.typeVisibilityMap;
      var hasVisibleConfigurations = false;

      function isVisibleBuildType(typeId) {
        if (visibilityMap && visibilityMap[projectId]) {
          // If any types are visible, hide the types that don't match the current filter, otherwise - show all types
          for (var type in visibilityMap[projectId]) {
            if (visibilityMap[projectId][type] === 1) {
              hasVisibleConfigurations = true;
            }
          }

          if (hasVisibleConfigurations && visibilityMap[projectId][typeId] === 0) {
            return false;
          }
        }

        return true;
      }

      var menuList = '<ul class="menuList menuListFilterable">';
      for (var i = 0; i < popup.buildTypes.length; i ++) {
        var typeId = popup.buildTypes[i].id;
        var url = window["base_uri"] + '/viewType.html?buildTypeId=' + typeId;
        menuList += '<li class="menuItem btItem_' + typeId + '" title="' + popup.buildTypes[i].description + '" data-bt-id="' + typeId +'"';

        if (isVisibleBuildType(typeId) === false) {
          menuList+= 'style="display: none"';
        }

        menuList += '><a href="' + url + '" showdiscardchangesmessage="false" title="Open build configuration page">' + popup.buildTypes[i].name + '</a>';
        menuList += '</li>';
      }
      menuList += '</ul>';
      return menuList;
    }
  });

  this.install();
};

_.extend(BS.BuildTypesPopup.prototype, BS.Popup.prototype);
_.extend(BS.BuildTypesPopup.prototype, {
  install: function() {
    var el = $('buildTypesPopup' + this.projectId);
    if (!el) return;
    this.handle = el;

    var boundShow = function() {
      if (BS.BuildTypesPopup.current) {
        BS.BuildTypesPopup.current.hidePopup(0, true);
      }
      this.showPopupNearElement(this.handle);
      BS.BuildTypesPopup.current = this;
    }.bind(this);

    el.on('mouseover', boundShow);
  },

  uninstall: function() {
    if (this.handle) {
      Event.stopObserving(this.handle);
      this.handle = null;
    }
  }
});

BS.AllProjectsPopup = function(name, url) {
  BS.Popup.call(this, name, {
    url: url,
    shift: {x: -215, y: 21},
    delay: 0,
    hideOnMouseOut: false,

    afterShowFunc: function() {
      this.btPopups = [];
      if ($("allProjectsPopupTable_filter")) $("allProjectsPopupTable_filter").activate();

      setTimeout(function() {

        for(var projectId in BS.projectMap) {
          this.btPopups.push(new BS.BuildTypesPopup(projectId));
        }

        BS.AllProjectsPopup.populateVisibilityMapOnce();

      }.bind(this), 200);
    }.bind(this),

    afterHideFunc: function() {
      this.btPopups = [];
      for (var i = 0; i < this.btPopups.length; i ++) {
        var popup = this.btPopups[i];
        popup.uninstall();
      }
      this.btPopups = [];
      BS.projectMap = {};
      BS.typeVisibilityMap = {};

      // It is necessary to forcedly blur the input field, because otherwise focus remains in a hidden element and certain things
      // stop working (like the 'p' shortcut in WebKit browsers)
      jQuery('#allProjectsPopupTable_filter').blur();
    }.bind(this)

  });
};


_.extend(BS.AllProjectsPopup.prototype, BS.Popup.prototype);

BS.AllProjectsPopup.showAll = function(e) {
  if (BS.AllProjectsPopup.globalInstance) {
    var overviewTab = $('overview_Tab');
    var allPopupImg = $('allPopupImg');
    var dim = allPopupImg.getDimensions();
    var pos1 = allPopupImg.cumulativeOffset();
    var pos2 = overviewTab.cumulativeOffset();
    BS.AllProjectsPopup.globalInstance.showPopupNearElement(allPopupImg, {
      shift: {x: pos2[0] - pos1[0], y: dim.height},
      hideOnMouseOut: true,
      overrideHideIfActive: 'allProjectsPopupTable_filter',
      delay: e.type == 'mouseover' ? 300 : 0,
      fitWithinWindowHeight: true,
      fitWithinWindowBottomMargin: 100,
      doScroll: false
    });
    Event.stop(e);
  }
};

BS.AllProjectsPopup.install = function() {
  var projectsTab = BS.topNavPane.getTab("overview");
  // <img src='" + window['base_uri'] + "/img/popUpControl.gif' alt='View all projects' >
  projectsTab.setPostLinkContent("<div id='allPopupImg' title='View all projects (keyboard shortcut [p])'>&nbsp;</div>");
  projectsTab.setCaption("Projects");

  BS.AllProjectsPopup.globalInstance = new BS.AllProjectsPopup("allProjectsPopup", window['base_uri'] + "/allProjects.html");

  var img = $('allPopupImg');
  img.on('mouseover', function(e) {
    img.addClassName('hovered');
    if (BS.AllProjectsPopup.globalInstance && !BS.AllProjectsPopup.globalInstance.isShown()) {
      BS.AllProjectsPopup.showAll(e);
    }
    else {
      BS.AllProjectsPopup.globalInstance.stopHidingPopup();
    }
  }.bindAsEventListener());

  img.on('mouseout', function() {
    img.removeClassName('hovered');
    if (BS.AllProjectsPopup.globalInstance.options.hideOnMouseOut) {
      BS.AllProjectsPopup.globalInstance.hidePopup(1000);
    }
  });

  img.on('click', function(e) {
    BS.AllProjectsPopup.globalInstance.hidePopup(0);
    BS.AllProjectsPopup.showAll(e);
  }.bindAsEventListener());

  // Invoking "All projects" from the keyboard ('p' shortcut).
  jQuery(document).keydown(function(e) {
    if (e.keyCode == 80) {

      var element = jQuery(e.target);
      if (element.is("input") || element.is("textarea") || element.is("select")) {
        return;
      }
      BS.AllProjectsPopup.showAll(e);
    }
  });
};

BS.AllProjectsPopup.Navigation = {
  projects: [],
  selectedProjectIdx: -1,
  selectedProject: null,
  popup: null,
  buildTypes: null,
  selectedBuildTypeIdx: -1,
  selectedBuildType: null,

  init: function() {
    var that = this;
    jQuery("#allProjectsPopupTable_filter").on('keydown', function(e) {
      if (e.keyCode == 40) {
        that.activate();
        return false;
      }
    });

    jQuery("#allProjectsPopupTable tr").on('keydown', function(e) {
      if (that.isActive()) {
        switch (e.keyCode) {
          case Event.KEY_DOWN: that.nextProject();  break;
          case Event.KEY_UP:   that.prevProject();  break;
          case Event.KEY_RIGHT: that.toBuildTypes(); break;
          case Event.KEY_RETURN: return true;
          default: return false;
        }
        return false;
      }
    });
  },

  activate: function() {
    this.projects = jQuery("#allProjectsPopupTable tr").filter(":visible");
    if (this.projects.length == 0) {
      return;
    }

    jQuery("#allProjectsPopupTable_filter").blur();
    this.selectProjectRow(0);
  },

  deactivate: function() {
    if (this.selectedProject) {
      this.unselectProjectRow();
    }

    this.projects = [];
    this.selectedProjectIdx = -1;
    this.selectedProject = null;
  },

  isActive: function() {
    return this.projects.length > 0;
  },

  selectProjectRow: function(idx) {
    this.selectedProjectIdx = idx;
    this.selectedProject = jQuery(this.projects[idx]);
    this.selectedProject.addClass("selected");
    this.selectedProject.find("a.projectLink").focus();
  },

  unselectProjectRow: function() {
    this.selectedProject.removeClass("selected");
  },

  selectBuildTypeItem: function(idx) {
    this.selectedBuildTypeIdx = idx;
    this.selectedBuildType = jQuery(this.buildTypes[idx]);
    this.selectedBuildType.addClass("selected");
    this.selectedBuildType.find("a").focus();
  },

  unselectBuildTypeItem: function() {
    this.selectedBuildType.removeClass("selected");
  },

  nextProject: function() {
    if (this.selectedProjectIdx < this.projects.length - 1) {
      this.unselectProjectRow();
      this.selectProjectRow(this.selectedProjectIdx + 1);
    }
  },

  prevProject: function() {
    if (this.selectedProjectIdx > 0) {
      this.unselectProjectRow();
      this.selectProjectRow(this.selectedProjectIdx - 1);
    } else {
      this.deactivate();
      jQuery("#allProjectsPopupTable_filter").focus();
    }
  },

  toBuildTypes: function() {
    var projectId = this.selectedProject.attr("data");
    if (BS.projectMap[projectId].length == 0) {
      return;
    }

    this.popup = new BS.BuildTypesPopup(projectId);
    this.popup.showPopupNearElement(this.popup.handle);

    this.buildTypes = jQuery(BS.Util.escapeId(projectId + "btPopup")).find("li.menuItem").filter(":visible");
    this.unselectProjectRow();
    this.selectBuildTypeItem(0);

    var that = this;
    this.buildTypes.on('keydown', function(e) {
      switch (e.keyCode) {
        case Event.KEY_DOWN: that.nextBuildType();  break;
        case Event.KEY_UP:   that.prevBuildType();  break;
        case Event.KEY_LEFT: that.toProjects();     break;
        case Event.KEY_RETURN: return true;
        case Event.KEY_ESC:  that.toProjects(true); break;
        case Event.KEY_RIGHT:
        default: return false;
      }
      return false;
    });
  },

  toProjects: function(keep_popup) {
    this.unselectBuildTypeItem();
    if (!keep_popup) {
      this.popup.hidePopup(0, true);
    }
    this.selectProjectRow(this.selectedProjectIdx);
  },

  nextBuildType: function() {
    if (this.selectedBuildTypeIdx < this.buildTypes.length - 1) {
      this.unselectBuildTypeItem();
      this.selectBuildTypeItem(this.selectedBuildTypeIdx + 1);
    }
  },

  prevBuildType: function() {
    if (this.selectedBuildTypeIdx > 0) {
      this.unselectBuildTypeItem();
      this.selectBuildTypeItem(this.selectedBuildTypeIdx - 1);
    }
  }
};

// Performs a search across build types in addition to projects, highlights projects which contain matching build types
BS.AllProjectsPopup.populateVisibilityMap = function(keyword) {
  var visibilityMap = BS.typeVisibilityMap;
  var btPopups = BS.AllProjectsPopup.globalInstance.btPopups;

  function hasMatchingType(text, keyword, parentProject) {
    var projectName = parentProject['projectName'].toUpperCase();
    var hasMatch = 0;

    text = text.replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&amp;/g,'&').replace(/&#034;/g, '"').replace(/&#039;/g, '\'').toUpperCase();

    if (keyword == '' || (text.indexOf(keyword) > -1 && projectName.indexOf(keyword) == -1)) {
      hasMatch = 1;
    }

    return hasMatch;
  }

  for (var i=0; i<btPopups.length; i++) {
    var projectId = btPopups[i].projectId;
    var buildTypes = btPopups[i].buildTypes;
    var text, parentProject, matchingType;

    visibilityMap[projectId] = visibilityMap[projectId] || {};
    visibilityMap[projectId]['_domCache'] = visibilityMap[projectId]['_domCache'] || {};

    for (var j=0; j<buildTypes.length; j++) {
      text = buildTypes[j].name;
      parentProject = BS.projectMap[projectId][0];
      matchingType = hasMatchingType(text, keyword, parentProject);
      visibilityMap[projectId][buildTypes[j].id] = matchingType;
    }
  }

  return visibilityMap;
};

BS.AllProjectsPopup.populateVisibilityMapOnce = _.once(function() {
  BS.AllProjectsPopup.populateVisibilityMap('');
});

BS.AllProjectsPopup.searchBuildTypes = function(element) {
  var keyword = element.value.toUpperCase();
  var allProjectsPopupTable = $j('#allProjectsPopupTable');

  var visibilityMap = BS.AllProjectsPopup.populateVisibilityMap(keyword);

  function toggleVisibility() {
    for (var project in visibilityMap) {
      var visibleConfigurationsCount = 0;
      var markedForHiding = [];
      var projectDomCache = visibilityMap[project]['_domCache'];
      var previousProjectRow = '';

      var projectRow = projectDomCache['projectRowEl'] ||
                       (projectDomCache['projectRowEl'] = allProjectsPopupTable.find('.inplaceFiltered_' + project));

      var countEl = projectDomCache['countEl'] || (projectDomCache['countEl'] = projectRow.find('.btCount'));

      // Restore order
      if (projectRow.data('previousProjectRow')) {
        previousProjectRow = projectRow.data('previousProjectRow');

        if (previousProjectRow == 'first') {
          projectRow.prependTo(projectRow.parent());
        } else {
          projectRow.insertAfter(allProjectsPopupTable.find('.inplaceFiltered_' + previousProjectRow));
        }
      }

      // Reset highlighting
      if (countEl.data('originalCount')) {
        countEl.html(countEl.data('originalCount'));
      }
      countEl.removeClass('btCountHighlighted');

      for (var type in visibilityMap[project]) {
        if (type == '_domCache') continue;

        var typeDomCache = visibilityMap[project]['_domCache']['typeRowEl' + type];
        var typeRow = typeDomCache || (typeDomCache = $j('.' + project + 'btPopup').find('.btItem_' + type));

        // Reset filtering
        typeRow.show();

        if (keyword.length > 0) {
          // Apply highlighting
          if (visibilityMap[project][type] === 1) {
            visibleConfigurationsCount++;
          }

          // Apply filtering
          if (visibilityMap[project][type] === 0) {
            markedForHiding.push(typeRow);
          }
        }
      }

      if (visibleConfigurationsCount > 0) {
        $j.each(markedForHiding, function(i, typeRow) {
          typeRow.hide();
        });

        if (!countEl.data('originalCount')) {
          countEl.data('originalCount', countEl.html());
        }
        countEl
            .html(visibleConfigurationsCount)
            .addClass('btCountHighlighted');

        // Reorder - matching projects first, then projects with matching configurations
        if (!projectRow.data('previousProjectRow')) {
          var originalSibling = projectRow.prev();

          if (originalSibling.length > 0) {
            projectRow.data('previousProjectRow', originalSibling.attr('data'));
          } else {
            projectRow.data('previousProjectRow', 'first');
          }
        }

        projectRow
            .show()
            .appendTo(projectRow.parent());
      }
    }
  }

  toggleVisibility();
};

/* Toggles project's visibility on the overview page */
BS.AllProjectsPopup.toggleOverview = function(link, projectId, options) {
  options = $j.extend({
    loadingTop: 0,
    loadingLeft: -20,
    updateParent: true
  }, options || {});

  var pinLink = $j(link),
      pinLinkImage = pinLink.children("img"),
      imgSrc = pinLinkImage.attr("src"),
      isPinned = imgSrc.endsWith("watched.png"),
      action = isPinned ? "hideProject" : "addProject";

  var offset = pinLink.offset(),
      loading = $j(BS.loadingIcon);

  loading.css({
    position: "absolute",
    top: offset.top + options.loadingTop + 'px',
    left: offset.left + options.loadingLeft + 'px',
    zIndex: 1000
  }).appendTo(document.body);

  BS.ajaxRequest("ajax.html", {
    parameters: action + "=" + projectId,
    onComplete: function() {
      loading.remove();
    },
    onSuccess: function() {
      var newSrc, newTitle;

      if (isPinned) {
        newSrc = imgSrc.replace(/watched.png/, "watch.png");
        newTitle = "Click to show project on the overview page";
      } else {
        newSrc = imgSrc.replace(/watch.png/, "watched.png");
        newTitle = "Project is shown on the overview page. Click to hide";
      }
      pinLinkImage.attr("src", newSrc);
      pinLinkImage.attr("title", newTitle);

      if (options.updateParent) {
        pinLink.toggleClass("pinLink");
        pinLink.parents('tr.inplaceFiltered').toggleClass("visibleProject");
      }

      if (BS.AllProjectsPopup.isOverviewPage === true) {
        var overviewProjects = $j('#overview-projects');

        if (overviewProjects.length == 0) {
          overviewProjects = $j('<div id="overview-projects">' +
                                'Overview configuration has been changed, ' +
                                '<a href="#" onclick="BS.reload(true)">refresh the page</a> to see the changes.' +
                                '<a href="#" class="dismiss-link" onclick="BS.Util.hide(this.parentNode)">Dismiss</a>' +
                                '</div>');
          $j('#toolbar').append(overviewProjects);
        } else {
          overviewProjects.show();
        }
      }
    }
  });
  return false;
};
