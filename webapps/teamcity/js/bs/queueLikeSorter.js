
BS.QueueLikeSorter = {
  containerId: null,

  getActionUrl: function(node) {},

  afterOrderSaving: function() {},
  
  _timoutHandle: null,
  scheduleUpdate: function(node) {
    this.updateMoveTopIcons();
    if (this._timoutHandle) {
      clearTimeout(this._timoutHandle);
    }
    this._timoutHandle = setTimeout(this.saveOrder.bind(this, node.id), 1000);
  },

  updateMoveTopIcons: function() {
    var queue = $(this.containerId);
    var moveTopIcons = queue && queue.select('.moveTopIcon');
    if (!moveTopIcons) return;
    for (var i=0; i<moveTopIcons.length; i++) {
      if (moveTopIcons[i] == null) continue; // under Safari document.getElementsByClassName may return array with null element
      if (!moveTopIcons[i]._oldSrc) {
        moveTopIcons[i]._oldSrc = moveTopIcons[i].src;
      }
      if (i == 0) {
        Event.stopObserving(moveTopIcons[i]);
        moveTopIcons[i].style.visibility = 'hidden';
      } else {
        moveTopIcons[i].src = moveTopIcons[i]._oldSrc;
        moveTopIcons[i].style.visibility = '';
        $(moveTopIcons[i]).on("mouseover", function() {
          this._oldSrc = this.src;
          this.src = window['base_uri'] + '/img/dragAndDrop/moveTopActive.gif';
        }.bind(moveTopIcons[i]));
        $(moveTopIcons[i]).on("mouseout", function() {
          this.src = this._oldSrc;
        }.bind(moveTopIcons[i]));
      }
    }
  },

  saveOrder: function(nodeId) {
    var node = $(nodeId);
    if (!node) return;
    this.setOrderSaving(true);
    var that = this;
    BS.ajaxRequest(this.getActionUrl(node), {
      onComplete: function(transport, object) {
        that.setOrderSaving(false);
        that.afterOrderSaving();
      }
    });
  },

  setOrderSaving: function(saving) {
    if (saving) {
      BS.Util.hide("dataSaved");
      BS.Util.show('savingData');
    } else {
      window.setTimeout(function() {
        BS.Util.hide('savingData');
        BS.Util.show("dataSaved");
        window.setTimeout(function() {
          BS.Util.hide("dataSaved");
        }, 3000);
      }, 1000);
    }
  },

  moveToTop: function(node) {
    var elem = $(node);
    var parent = elem.parentNode;
    if (parent.firstChild.id == elem.id) return; // do not move first item
    parent.removeChild(elem);
    parent.insertBefore(elem, parent.firstChild);
    elem.className = elem.className.replace(/draggableHover/, '');
    this.scheduleUpdate(parent);
  },

  computeOrder: function(node, prefix) {
    if (node.childNodes && node.childNodes.length > 0) {
      $(node).cleanWhitespace();
      var children = node.childNodes;
      var order = "";
      for(var i = 0; i < children.length; i++) {
        var id = children[i].id;
        if (id.indexOf(prefix) != 0) continue;
        order += children[i].id.substring(prefix.length) + ";";
      }
      return order;
    }

    return "";
  }
};
