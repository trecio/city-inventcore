BS.BuildLogTree = {
  expand:'none',
  filter:'all',
  clickTimeout:null,
  hideBlocks:'false',

  SEPARATOR:',',

  // Init stuff that can be inited before the page has finished loading.
  // Be careful when putting code here - test in Internet Explorer. DOM modifications of the log itself
  // have to be done in initDeferred()
  init:function (expand, filter, hideBlocks) {
    this.expand = expand;
    this.filter = filter;
    this.hideBlocks = hideBlocks;

    var container = $j('#buildLog');
    var that = this;

    container.delegate('.msg', 'mouseover mouseout', function (e) {
      if (e.type == 'mouseover') {
        that.hover(this, 'hover');
      } else {
        that.unHoverAll('hover');
      }
    });

    container.delegate('.msg', 'click', function () {
      var id = this.id.replace(/msg_/, '');
      var oldFocus = that.getFocus();

      that.unFocus();
      if (id != oldFocus) {
        BS.LocationHash.setHashParameter('focus', id);
        that.focus(id, false);
      }
    });

    container.delegate('.msg .ts_in', 'mouseover mouseout', function(e) {
      if (e.type == 'mouseover') {
        that.showMessagePopup($j(this).parent('.ts').attr('id').replace('ts_', ''));
      } else {
        that.hideMessagePopup();
      }
    });

    container.delegate('.closed > .msg > .ts, .open > .msg > .ts', 'mousedown click dblclick', function (e) {
      if (e.type == 'mousedown') return false; // Prevent text selection

      var id = this.id.replace('ts_', '');
      e.type == 'click' && that.onNodeClick(id);
      e.type == 'dblclick' && that.onNodeDoubleClick(id);
    });

    container.delegate('.msgLink', 'click', function (e) {
      e.stopPropagation();
    });

    this.initCheckbox('hideBlockNames', 'hideBlocks', true, !this.hideBlocks);
    this.initCheckbox('highlightFlows', 'highlightFlows', false, window.location.search.include('highlightFlows=true'));

    //var checkbox = $j('#groupFlows');
    //checkbox.prop('checked', this.getHashParameter('groupFlows') == 'true');
    //checkbox.change(function () {
    //  if (this.checked) {
    //    that.setHashParameter('groupFlows', 'true');
    //    that.groupFlows();
    //  } else {
    //    that.setHashParameter('groupFlows', '');
    //    BS.reload(true);
    //  }
    //});
    //

    if (that.getHrefParameter('filter') == 'debug') {
      $j('.rawMessageFileOption').css('display', 'table-cell');
    }

    this.initDeferred();
  },

  initDeferred: function() {
    var container = $j('#buildLog');
    var that = this;

    $j(document).ready(function () {
      // Custom oncopy handler for IE - removes empty lines from the copied fragment. Empty lines appear because
      // subtrees are present in the HTML, but hidden
      if (window.attachEvent && window.clipboardData) {
        container.get(0).attachEvent('oncopy', function(e) {
          setTimeout(function() {
            var text = window.clipboardData.getData('Text');
            text = text.split('\n');
            text = _.reject(text, function(item) {
              return item.match(/^\s$/)
            });
            text = text.join('\r\n');
            window.clipboardData.setData('Text', text);
          }, 10);
        });
      }

      that.restoreState();
      that.focus(that.getFocus(), true);

      //var groupFlows = $j('#groupFlows');
      //groupFlows.removeAttr('disabled');
      //if (groupFlows.attr('checked')) {that.groupFlows();}
      //
    });
  },

  initCheckbox: function (id, paramName, inverse, initVal) {
    if (!paramName) paramName = id;

    var that = this;
    var checkbox = $j('#' + id);

    checkbox.prop('checked', initVal);
    checkbox.change(function () {
      window.location.replace(that.getHrefWithParameter(paramName, inverse ? !this.checked : this.checked));
    });
  },

  onNodeClick:function (id) {
    if (this.clickTimeout) return;

    var that = this;
    this.clickTimeout = window.setTimeout(function () {
      that.clickTimeout = null;
      that.toggleNodeAndState(id, false);
    }, 200);
  },

  onNodeDoubleClick:function (id) {
    if (this.clickTimeout) {
      clearTimeout(this.clickTimeout);
      this.clickTimeout = null;
    }
    this.toggleNodeAndState(id, true);
  },

  toggleNodeAndState:function (id, recursive) {
    this.updateState(id, this.toggleNode(id, recursive), recursive);
  },

  // returns true if element is opened after toggling
  toggleNode:function (id, recursive) {
    var elem = $j('#node_' + id);
    if (elem.length > 0) {
      var wasOpen = elem.hasClass('open');
      if (wasOpen) {
        this.closeNode(id);
      } else {
        this.openNode(id, recursive);
      }
      elem.toggleClass('open closed');
      if (!recursive && !wasOpen) {
        //this.smartExpand(id);
      }
      return !wasOpen;
    }
  },

  openNode:function(id, recursive) {
    var that = this;

    var node = $j('#node_' + id);
    var loading = $j("<i/>").css({
      marginLeft: "16px",
      display: "none"
    }).append(BS.loadingIcon).append(" Loading...").attr('id', 'load_' + id).insertAfter(node);

    setTimeout(function() {
      loading.show();
    }, 200);

    BS.ajaxRequest(window['base_uri'] + '/buildLog/buildLogTreeBlock.html', {
      parameters: {
        'id': id,
        'buildId': this.getHrefParameter('buildId'),
        'expand': recursive ? 'all' : that.expand,
        'filter': this.filter,
        'hideBlocks': that.hideBlocks,
        'baseLevel': node.data('level'),
        'state': that.getHrefParameter('state')
      },
      onComplete: function(transport) {
        if (node.hasClass('closed') || $j('.ch_' + id).length) {
          // already closed
          return;
        }

        var response = $j(transport.responseText);

        var baseCh = that.getChClasses(node);
        var showBlocks = $j('#hideBlockNames').prop('checked');

        var flowBlocks = new Object();
        flowBlocks[that.getFlowId(node)] = that.getBlockText(node);

        var children = response.filter('.leaf, .open, .closed');
        children.each(function() {
          var child = $j(this);          
          child.addClass(baseCh + 'ch_' + id);

          if (showBlocks && !child.children('.msg').children('.mark').children('.parent').length) {
            var flow = that.getFlowId(child);
            var flowBlock = flowBlocks[flow];
            if (!flowBlock) {
              flowBlock = that.getFlowBlock(flow, node, child);
              flowBlocks[flow] = flowBlock;
            }
            if (flowBlock && flowBlock != 'unknown') {
              child.children('.msg').children('.mark').prepend('<i class="parent">[' + flowBlock + '] </i>');
            }
          }
        });

        loading.remove();
        node.after(response);
      }
    });
  },

  getFlowId:function(elem) {
    var flowId = elem.get(0).className.match(/fl_(\w+)/);
    return flowId.length > 0 ? flowId[1] : '';
  },

  getFlowBlock:function(flowId, node, child) {
    var elem = child;
    while (true) {
      elem = elem.prevAll('#flow_' + flowId);
      if (elem.length) {
        flowId = this.getFlowId(elem);
      } else break;
    }

    if (node.filter('.fl_' + flowId).length) {
      return this.getBlockText(node);
    }

    elem = node;
    while (true) {
      elem = elem.prevAll('.open.fl_' + flowId + ', .closed.fl_' + flowId + ', #flow_' + flowId).filter(':first'); // JQuery prev doesn't work!
      if (elem.length) {
        if (elem.hasClass('.flow')) {
          flowId = this.getFlowId(elem);
        } else {
          return this.getBlockText(elem);
        }
      } else {
        return 'unknown';
      }
    }
  },

  getBlockText:function(block) {
    return block.children('.msg').children('.mark').contents().filter(function() {return this.nodeType == 3;}).text()
  },

  closeNode:function(id) {
    $j('#load_' + id).remove();

    //var log = document.getElementsByClassName('rTree')[0];
    //var children = document.getElementsByClassName('ch_' + id);
    //for (var i = 0; i < children.length; ++i) {
    //  log.removeChild(children[i]);
    //}
    $j('.ch_' + id).remove();
  },

  getChClasses:function(elem) {
    var classAttr = elem.attr('class');
    if (!classAttr) return '';

    var chClasses = '';
    var classes = classAttr.split(/\s+/);
    $j.each(classes, function (index, item) {
      if (item.indexOf('ch_') == 0) chClasses += item + ' ';
    });
    return chClasses;
  },

  updateState:function (id, open, recursive) {
    if (open) {
      this.expand == 'all' ? this.removeFromState(id) : this.addToState(id, recursive);
    } else {
      this.expand == 'all' ? this.addToState(id) : this.removeFromState(id);
    }
  },

  addToState:function (id, recursive) {
    this.removeFromState(id);
    if (recursive) id = this.wrapRecursiveId(id);
    var ids = this.getIdsFromState();
    var index = ids.indexOf(id);
    if (index == -1) {
      ids.push(id);
      this.setState(ids.join(this.SEPARATOR));
    }
  },

  removeFromState:function (id) {
    var ids = this.getIdsFromState();
    var index = ids.indexOf(id);
    if (index > -1) {
      ids.splice(index, 1);
      this.setState(ids.join(this.SEPARATOR));
    } else {
      index = ids.indexOf(this.wrapRecursiveId(id));
      if (index > -1) {
        ids.splice(index, 1);
        this.setState(ids.join(this.SEPARATOR));
      }
    }
  },

  wrapRecursiveId:function (id) {
    return id + '!';
  },

  isRecursiveId:function (id) {
    return /!/g.test(id);
  },

  getRecursiveId:function (id) {
    return id.split('!')[0];
  },

  getIdsFromState:function () {
    var state = this.getState();
    if (!state) return [];
    return state.split(this.SEPARATOR);
  },

  getState:function () {
    return BS.LocationHash.getHashParameter('state');
  },

  setState:function (state) {
    BS.LocationHash.setHashParameter('state', state);
  },

  hover:function (elem, className) {
    elem = $j(elem);
    if (!elem || elem.hasClass(className)) return false;
    elem.addClass(className);
    return true;
  },

  unHover:function (elem, className) {
    elem = $j(elem);
    if (!elem || !elem.hasClass(className)) return;

    elem.removeClass(className);
  },

  unHoverAll:function (className) {
    $j('.' + className).removeClass(className);
  },

  focus:function (id, scrollTo) {
    if (!id || id.blank()) return;

    var msg = $j('#msg_' + id);
    if (!msg.length || !this.hover(msg.get(0), 'focus') || !scrollTo) return;

    $j(window).scrollTop(msg.position().top - 30);
  },

  unFocus:function () {
    BS.LocationHash.setHashParameter('focus', '');
    this.unHoverAll('focus');
  },

  getFocus:function () {
    return BS.LocationHash.getHashParameter('focus');
  },

  restoreState: function() {
    var state = BS.LocationHash.getHashParameter('state');
    if (state || this.getHrefParameter('state')) {
      window.location.replace(this.getHrefWithParameter('state', state));
    }
  },

  updateFlows: function() {
  },

  smartExpand: function(id) {
    var elem = $j('#node_' + id);
    var hours = elem.children('#sub_' + id + ' > .closed > .msg > .time').filter(':contains("h:")');
    if (hours.length) {
      hours.parent().parent().toggleClass('open closed');
    } else {
      var minutes = elem.find('#sub_' + id + ' > .closed > .msg > .time').filter(':contains("m:")');
      if (minutes.length) {
        minutes.parent().parent().toggleClass('open closed');
      } else {
        var seconds = elem.find('#sub_' + id + ' > .closed > .msg > .time').filter(':contains("s")');
        if (seconds.length) {
          seconds.parent().parent().toggleClass('open closed');
        }
      }
    }
    elem.children('#sub_' + id + ' > .closed > .msg > .time').filter(':contains("started"), :contains("running")').toggleClass('open closed');
    elem.find('.sub > .closed:only-child').toggleClass('open closed');
  },

  reloadExpand:function (expand) {
    BS.LocationHash.setHashParameter('state', '');

    var newHref = this.replaceHrefParameter(this.getHrefWithParameter('expand', expand), 'state', '');
    if (newHref == window.location.href) {
      window.location.reload();
    } else {
      window.location.replace(newHref);
    }
  },

  reloadFilter:function (filter) {
    if (filter == this.filter) return;
    window.location.replace(this.getHrefWithParameter('filter', filter));
  },

  getHrefWithParameter:function (key, val) {
    return this.replaceHrefParameter(window.location.href, key, val);
  },

  replaceHrefParameter:function (href, key, val) {
    var search = window.location.search;
    var params = search.toQueryParams();
    params[key] = val;
    return href.replace(search, '?' + Object.toQueryString(params));
  },

  getHrefParameter:function (key) {
    var param = window.location.search.toQueryParams()[key];
    return param ? param : '';
  },

  showMessagePopup: function(id) {
    var elem = $j('#node_'+ id);
    var filter = this.filter;
    var fromServer = elem.children('.pC').html();

    var popupText = '';

    if (elem.hasClass('open') || elem.hasClass('closed')) {
      popupText += '<li class="mToggle">' +
                   '<a href="#" title="Collapse/Expand all inside this node" onclick="BS.BuildLogTree.toggleNodeAndState(\'' + id + '\', true); return false">Collapse/Expand Subtree</a>' +
                   '</li>';
    }

    if (filter && filter != 'all' && filter != 'debug') {
      popupText += '<li><a href="#" onclick="BS.BuildLogTree.navigateToTree(\'' + id + '\');return false;" title="Navigate to this message in All messages view">Show in All messages</a></li>';
    }

    if (fromServer) {
      popupText += fromServer;
    }

    if (popupText == '') return;

    if (!BS.MessagePopup) BS.MessagePopup = new BS.Popup("messagePopup", {
      hideDelay: 1,
      className: "messagePopup"
    });
    BS.MessagePopup.hidePopup(600, true);
    var that = this;
    BS.MessagePopup.showPopupNearElement($('node_' + id), {
      shift: {x: 36, y: 15},
      innerText: "<ul class='messages'>" + popupText + "</ul>",
      afterShowFunc:function (popup) {
        that.hover('#msg_' + id, "withPopup");
      },
      afterHideFunc:function () {
        that.unHover('#msg_' + id, "withPopup");
      }
    });
  },

  hideMessagePopup: function() {
    if (BS.MessagePopup) BS.MessagePopup.hidePopup(600);
  },

  markBlockEmpty: function(id) {
    var block = $j('#node_' + id);
    if (block.hasClass('open')) {
      block.toggleClass('open emptyOpen');
    } else {
      block.toggleClass('closed emptyClosed');
    }
  },

  updateBlockClass: function(id, hasChildren) {
    var block = $j('#node_' + id);
    if (hasChildren) {
      if (block.hasClass('emptyOpen')) {
        block.toggleClass('emptyOpen open');
      } else if (block.hasClass('emptyClosed')) {
        block.toggleClass('emptyClosed closed');
      }
    } else {
      if (block.hasClass('open')) {
        block.toggleClass('emptyOpen open');
      } else if (block.hasClass('closed')) {
        block.toggleClass('emptyClosed closed');
      }
    }
  },

  updateBlockDuration: function(id, duration) {
    var time = $j('#msg_' + id + ' > .time');
    if (duration.length > 0) {
      duration = ' (' + duration + ')';
      if (time.length > 0) {
        time.html(duration);
      } else {
        $j('#msg_' + id).append('<u class="time"> ' + duration + '</u>');
      }
    } else {
      time.remove();
    }
  },

  updateBlockColor: function(id, color) {
    var mark = $j('#msg_' + id + ' > .mark');
    if (mark.length > 0) {
      if (mark.css('color') != color) {
        mark.css('color', color);
      }
    } else {
      mark = $j('<i class="mark"/>').css('color', color);
      BS.Util.getTextChildren('msg_' + id).wrap(mark); // we get text and wrap it in <i class='mark'>
    }
  },

  navigateTo: function(id, filter) {
    this.unFocus();
    BS.LocationHash.setHashParameter('focus', id);
    this.focus(id, true);
  },

  navigateToTree: function(id, filter) {
    if (!filter) filter = 'all';
    if ($j('.rTree').length && $j('#node_' + id).length && (filter == this.filter)) {
      this.navigateTo(id, filter);
    } else {
      window.location.replace(window['base_uri'] + '/viewLog.html?tab=buildLog&logTab=tree&expand=all&buildId=' +
                              this.getHrefParameter('buildId') + '&filter=' + filter + '##focus=' + id);
    }
  },

  appendFlow: function(flowId) {
    var insertion = $j('#ins_' + flowId);
    $j('#fsub_' + flowId).append(insertion.html());
    insertion.remove();
  },

  withFlows:function (onFlow, onFinish) {
    $j('#groupFlows, #highlightFlows').attr('disabled', true);

    BS.BuildLogTree.ProgressRing.show();

    setTimeout(function () {
      var flows = $j('.rTree div.parallel');

      // afterToggle will actually be run only for the last flow, see http://documentcloud.github.com/underscore/#after
      var afterToggle = _.after(flows.length, function(){
        if (onFinish) onFinish();
        BS.BuildLogTree.ProgressRing.hide();
        $j('#groupFlows, #highlightFlows').removeAttr('disabled');
      });

      flows.each(function () {
        var that = this;
        setTimeout(
            function () {
              onFlow(that);
              afterToggle();
            }, 10);
      });
    }, 10);
  },

  firstHighlight : true,

  //highlightFlows: function () {
  //  if (!this.firstHighlight) {
  //    $j('.hl').removeClass('hiddenHl');
  //    return;
  //  }
  //
  //  this.firstHighlight = false;
  //
  //  if ($j('#groupFlows').attr('checked')) {
  //    this.withFlows(function(elem){
  //      var flow = $j(elem);
  //      flow.addClass('c' + flow.data('index')%24 + ' hl'); // 24 colors in buildLogColors.css
  //    });
  //  } else {
  //    this.withFlows(function(elem){
  //      var flow = $j(elem);
  //      var flowId = elem.id.replace('flow_', '');
  //      var msgs = $j('.rTree div:not(.fl_' + flowId + ') > .sub > ' + 'div.fl_' + flowId).add('.rTree > ' + 'div.fl_' + flowId);
  //      msgs.addClass('c' + flow.data('index')%24 + ' hl'); // 24 colors in buildLogColors.css
  //    });
  //  }
  //},

  groupFlows: function () {
    var that = this;
    var highlightFlows = $j('#highlightFlows').attr('checked');
    this.withFlows(function (elem) {
      var flow = $j(elem);
      var flowId = elem.id.replace('flow_', '');
      var msgs = $j('.rTree div:not(.fl_' + flowId + ') > .sub > ' + 'div.fl_' + flowId).add('.rTree > ' + 'div.fl_' + flowId);
      if (msgs.length) {
        if (highlightFlows) flow.addClass('c' + flow.data('index')%24 + ' hl');

        flow.append(msgs);
        flow.show();
        flow.parents('.flow').show();
      }
    }, function () {
      that.updateFlows();
    });
  },

  flowColors:['#FFE0E0', '#E0ECF8', '#F8ECE0', '#E0E0F8', '#F7F8E0',
              '#ECE0FF', '#ECF8E0', '#F8E0F7', '#E0F8E0', '#F8E0EC',
              '#E0F8EC', '#FAFAFA', '#E0F8F7', '#E0F2F7', '#FFE6E0',
              '#F7F2E0', '#E0E6F8', '#E6E0F8', '#F1F8E0', '#F2E0F7',
              '#E6F8E0', '#F8E0F1', '#E0F8E6', '#FFE0E6', '#E0F8F1'], // 25 colors

  //background: -webkit-gradient(linear, left top, right top, from(#XXXXXX), to(#FFF));
  //background: -webkit-linear-gradient(right, #FFF, #XXXXXX);
  //background: -moz-linear-gradient(right, #FFF, #XXXXXX);
  //background: -ms-linear-gradient(right, #FFF, #XXXXXX);
  //background: -o-linear-gradient(right, #FFF, #XXXXXX);
  //background: linear-gradient(right, #FFF, #XXXXXX);

  markFlowParallel: function(flowId, index) {
    var flow = $j('#flow_' + flowId);

    if (flow.hasClass('parallel')) return;

    flow.addClass('parallel');
    flow.data('index', index);

    var color = this.flowColors[index%25];

    $j('<style type="text/css">.fl_' + flowId + '{' +
       'background-color:' + color + ';' +
       'background: -webkit-gradient(linear, left top, right top, from(' + color + '), to(#FFF));' +
       'background: -webkit-linear-gradient(right, #FFF, ' + color + ');' +
       'background: -moz-linear-gradient(right, #FFF, ' + color + ');' +
       'background: -ms-linear-gradient(right, #FFF, ' + color + ');' +
       'background: -o-linear-gradient(right, #FFF, ' + color + ');' +
       'background: linear-gradient(right, #FFF, ' + color + ');' +
       '}' + '</style>').appendTo("head");
  }
};

BS.BuildLogTree.ProgressRing = {
  counter: 0,
  show: function() {
    if (this.counter == 0) {
      $j('#groupFlowsContainer').after('<span id="busyRing" style="margin: 0.5em;">' + BS.loadingIcon + '</span>');
    }
    ++this.counter;
  },
  hide: function() {
    --this.counter;
    if (this.counter == 0) {
      $j('#busyRing').remove();
    }
  }
};