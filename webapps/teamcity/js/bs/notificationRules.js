
BS.NotifierPropertiesForm = OO.extend(BS.AbstractWebForm, {
  formElement: function() {
    return $('notifierSettingsForm');
  },

  setOrderSaving: function(saving) {
    if (saving) {
      BS.Util.show('saving_settings');
    } else {
      BS.Util.hide('saving_settings');
    }
  },

  submitSettings: function() {
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        BS.reload(true);
      }
    }), false);

    return false;
  }
});


BS.NotificationRuleForm = OO.extend(BS.QueueLikeSorter, OO.extend(BS.AbstractWebForm, {
  notificatorType: "",
  holderId: "",

  setOrderSaving: function(saving) {
    this.hideSuccessMessages();
    BS.QueueLikeSorter.setOrderSaving(saving);
  },

  setNotificatorType: function(type) {
    this.notificatorType = type;
  },

  setHolderId: function(holderId) {
    this.holderId = holderId;
  },

  containerId: 'notificationRulesRows',

  afterOrderSaving: function() {
    BS.NotificationRuleForm.requestNewIds();
  },

  updateIdsFromData: function () {
    for(var oldId in BS.NotificationRuleIds._ids) {
      var newId = BS.NotificationRuleIds._ids[oldId];
      var element = $('rule_' + oldId);
      if (element && newId != null) {
        element.setAttribute('id', 'rule_' + newId);
      }
    }
  },

  _parameters: function() {
    return "&holderId=" + this.holderId + "&notificatorType=" + this.notificatorType;
  },

  requestNewIds: function() {
    BS.ajaxRequest(window['base_uri'] + "/notificationRules.html", {
      parameters: "idsRequest=1" + BS.NotificationRuleForm._parameters(),
      onSuccess: function(transport, object) {
        transport.responseText.evalScripts();
        BS.NotificationRuleForm.updateIdsFromData();
      },
      method: "get"
    });
  },

  getActionUrl: function(node) {
    return window['base_uri'] + "/notificationRules.html?newOrder=" + this.computeOrder(node, "rule_") + this._parameters();
  },

  _draggableTagName: "div",
  _rulePrefix: "rule_",

  getRuleIdFromNodeId: function(nodeId, tagNameInLowerCase, prefix) {
    var node = $(nodeId);
    while (node != null) {
      if (node.tagName.toLowerCase() == tagNameInLowerCase) {
        var id = node.getAttribute('id');
        if (id.startsWith(prefix)) {
          return id.substring(prefix.length);
        }
      }
      node = node.parentNode;
    }
    return null;
  },

  _getRuleIdFromNodeId: function(nodeId) {
    return BS.NotificationRuleForm.getRuleIdFromNodeId(nodeId, BS.NotificationRuleForm._draggableTagName, BS.NotificationRuleForm._rulePrefix);
  },

  editRule: function(url, nodeId) {
    var ruleId = nodeId == null ? -1 : BS.NotificationRuleForm._getRuleIdFromNodeId(nodeId);

    BS.ajaxRequest(url, {
      parameters: "editRule=true&ruleId=" + ruleId + BS.NotificationRuleForm._parameters(),
      onComplete: function(transport, object) {
        document.location.hash = 'ruleEditingForm';
        BS.reload(true);
      }
    });
  },

  moveToTop: function(nodeId) {
    var ruleId = BS.NotificationRuleForm._getRuleIdFromNodeId(nodeId);
    var node = BS.NotificationRuleForm._rulePrefix + ruleId;
    var elem = $(node);
    var parent = elem.parentNode;
    if (parent.firstChild.id == elem.id) return; // do not move first item
    parent.removeChild(elem);
    parent.insertBefore(elem, parent.firstChild);
    elem.className = elem.className.replace(/draggableHover/, '');
    this.scheduleUpdate(parent);
  },

  removeRule: function(url, nodeId) {
    var ruleId = BS.NotificationRuleForm._getRuleIdFromNodeId(nodeId);

    if (!confirm("Are you sure you want to remove this rule?")) return;

    BS.ajaxRequest(url, {
      parameters: "deleteRule=" + ruleId + BS.NotificationRuleForm._parameters(),
      onComplete: function(transport, object) {
        BS.reload(true);
      }
    });
  },

  cancelEditing: function() {
    BS.ajaxRequest(this.formElement().action, {
      parameters: "cancelEditing=true" + BS.NotificationRuleForm._parameters(),
      onComplete: function(transport, object) {
        var href = document.location.href;
        var pos = href.indexOf("#");
        if (pos != -1) {
          href = href.substring(0, pos);
        }
        document.location.replace(href);
      }
    });
  },

  formElement: function() {
    return $('notificationRuleForm');
  },

  submitRule: function(overwrite) {
    var that = this;

    if ($('watchTypeBuildConfigurations').checked) {
      if ($('configurations').selectedIndex < 0) {
        alert('Please select at least one build configuration.');
        return false;
      }
    }

    var url = this.formElement().action;
    if (overwrite) {
      url += "?overwriteRule=1";
    }

    BS.FormSaver.save(this, url, OO.extend(BS.ErrorsAwareListener, {
      onProjectNotFoundError: function(elem) {
        $("errorProject").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("projectBuildType"));
      },

      onBuildTypeNotFoundError: function(elem) {
        $("errorBuildType").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("projectBuildType"));
      },

      onDuplicateRuleError: function(elem) {
        var committerRule = $('watchTypeCommitter').checked;
        var projectsRule = $('watchTypeProject').checked;
        var message;
        if (committerRule) {
          message = "Rule for builds with your changes already exists.\n\n" +
                    "Click \"Ok\" to overwrite the existing rule.\n" +
                    "Click \"Cancel\" to continue editing.";
        }

        if (projectsRule) {
          message = "Rule for the specified project already exists.\n\n" +
                    "Click \"Ok\" to overwrite the existing rule.\n" +
                    "Click \"Cancel\" to continue editing.";
        }

        if (!committerRule && !projectsRule) {
          message = "One or more rules with selected build configuration(s) already exist.\n\n" +
                    "Click \"Ok\" to save this rule and remove conflicting configurations from the existing rules.\n" +
                    "Click \"Cancel\" to continue editing."
        }

        if (confirm(message)) {
          that.submitRule(true);
        }
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        if (!err) {
          BS.reload(true);
        }
      }
    }), false);

    return false;
  }
}));

_.extend(BS.NotificationRuleForm, {
  uncheckEvent: function(eventName) {
    $("editingEvents['" + eventName + "']").checked = false;
  },

  checkEvent: function(eventName) {
    $("editingEvents['" + eventName + "']").checked = true;
  },

  enableEvent: function(eventName) {
    $("editingEvents['" + eventName + "']").disabled = false;
  },

  disableEvent: function(eventName) {
    $("editingEvents['" + eventName + "']").disabled = true;
  },

  isSystemEventsShown: function() {
    return BS.Util.visible('system-wide-events');
  },

  uncheckNonSystemEvents: function() {
    this.uncheckEvent('BUILD_FINISHED_FAILURE');
    this.uncheckEvent('BUILD_FINISHED_NEW_FAILURE');
    this.uncheckEvent('FIRST_FAILURE_AFTER_SUCCESS');
    this.uncheckEvent('BUILD_FINISHED_SUCCESS');
    this.uncheckEvent('FIRST_SUCCESS_AFTER_FAILURE');
    this.uncheckEvent('BUILD_FAILING');
    this.uncheckEvent('BUILD_STARTED');
    this.uncheckEvent('BUILD_PROBABLY_HANGING');
    this.uncheckEvent('RESPONSIBILITY_CHANGES');
    this.uncheckEvent('MUTE_UPDATED');
  },

  uncheckSystemEvents: function() {
    this.uncheckEvent('RESPONSIBILITY_ASSIGNED');
  },

  switchRightPanel: function(showSystemEvents) {
    var currentState = this.isSystemEventsShown();
    if (currentState == showSystemEvents) {
      return;
    }

    if (currentState) {
      this.uncheckSystemEvents();
      BS.Util.hide('system-wide-events');
      BS.Util.show('non-system-events');
    } else {
      this.uncheckNonSystemEvents();
      BS.Util.show('system-wide-events');
      BS.Util.hide('non-system-events');
    }
  }
});

