/*
 * Copyright (c) 2006-2012, JetBrains, s.r.o. All Rights Reserved.
 */

// See TW-18828.
BS.ResponsibilityCommon = {
  checkUserPermissionsOnChange: function(warningContainer, investigatorDropdown, projectId) {
    investigatorDropdown.change(function() {
      var option = this.options[this.selectedIndex];
      var userId = option ? option.value : 0;

      warningContainer.html();
      var loading = $j(BS.loadingIcon).appendTo("body").css({
        zIndex: 100
      }).position({
        my: "left",
        at: "right",
        of: investigatorDropdown.parent(),
        offset: "2 0"
      });

      if (userId) {
        BS.ajaxRequest(window['base_uri'] + "/investigationWarning.html", {
          method: "get",
          parameters: {
            projectId: projectId,
            userId: userId
          },
          onComplete: function(transport) {
            warningContainer.html(transport.responseText);
            loading.remove();
          }
        })
      }
    });
  }
};

BS.ResponsibilityDialog = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  formElement: function() {
    return $('investigationForm');
  },

  getContainer: function() {
    return $('investigationFormDialog');
  },

  getUrl: function() {
    return window['base_uri'] + "/buildTypeInvestigationDialog.html";
  },

  showDialog: function(buildTypeId, buildTypeFullName, presetFix) {
    var title = "Investigate \"" + buildTypeFullName.escapeHTML() + "\"" + (buildTypeFullName.length > 20 ? "" : " build configuration");
    $j("#investigationFormTitle").html(title);

    var bodyElement = $j("#investigationForm .modalDialogBody");
    bodyElement.html(BS.loadingIcon + " Loading...");
    this.showCentered();
    this.bindCtrlEnterHandler(this.submit.bind(this));
    function highlight(radio) {
      radio = $j(radio);
      if (!radio.prop("checked")) {
        radio.click().trigger("change");
        new Effect.Highlight(radio.next("label")[0]);  // highligh animation
      }
    }

    function highlightStickyOptionIfNeeded() {
      if ($j("#currentBuildType img").attr("src").indexOf("success") != -1) {
        var dropdown = $j("#bt-remove-investigation");
        dropdown.prop("selectedIndex", 1).trigger("change");
        new Effect.Highlight(dropdown.parent().parent().find("label")[0]);  // highligh animation
      }
    }

    var that = this;
    BS.ajaxRequest(this.getUrl(), {
      method: "get",
      parameters : {buildTypeId: buildTypeId},
      onComplete: function(transport) {
        bodyElement.html(transport.responseText);
        that.showCentered();
        that._handleDefaults(jQuery);
        that.hideMoreLinkIfNeeded();

        // Preset "fixed" if needed.
        if (presetFix) {
          highlight("#bt-fix-investigate");
        } else if ($j("#bt-responsible option:selected").text() == "me" && $j("#bt-giveup-investigate").is(":checked")) {
          highlight("#bt-assign-investigate");
          highlightStickyOptionIfNeeded();
        }
      }
    });

    return false;
  },

  _handleDefaults: function($) {
    var investigateCheckbox = $("#do-bt-investigate"),
        investigateRadio = $("#bt-investigate-section input[name=investigate]"),
        assign = $("#bt-assign-investigate"),
        investigator = $("#bt-responsible"),
        removeInvestigation = $("#bt-remove-investigation");

    investigateRadio.change(function() {
      check(investigateCheckbox);
    });
    investigateCheckbox.change(function() {
      if (!$(this).is(":checked")) {
        uncheck(investigateRadio);
      } else {
        check(assign);
      }
    });
    investigator.add(removeInvestigation).change(function() {
      check(assign);
      check(investigateCheckbox);
    });

    function check(input) {
      return input.prop("checked", true);
    }
    function uncheck(input) {
      return input.prop("checked", false);
    }
    function enable(input) {
      input.prop("disabled", false);
    }
    function disable(input) {
      input.prop("disabled", true);
    }

    function enableOrDisable() {
      if (investigateCheckbox.is(":checked")) {
        enable($("#bt-submit"));
      } else {
        disable($("#bt-submit"));
      }
    }

    // Handle defaults

    var investigateDefault = {};
    var investigateSection = $("#bt-investigate-section");

    function saveDefault(map, section) {
      section.find("select, input[type=text]").each(function() {
        map[this.id] = $(this).val();
      });
      section.find("input[type=radio], input[type=checkbox]").each(function() {
        map[this.id] = $(this).is(":checked");
      });
    }
    function restoreDefault(map, section) {
      section.find("select, input[type=text]").each(function() {
        $(this).val(map[this.id]).trigger("change");
      });
      section.find("input[type=radio], input[type=checkbox]").each(function() {
        if (map[this.id]) {
          check($(this));
        } else {
          uncheck($(this));
        }
      });
    }

    saveDefault(investigateDefault, investigateSection);

    // Show / hide [reset]

    var investigationReset = $("#bt-reset-investigate");

    investigateRadio.add(investigator).add(removeInvestigation).change(function() {
      investigationReset.show()
                        .parent().addClass("modifiedParam");
    });
    investigationReset.click(function() {
      restoreDefault(investigateDefault, investigateSection);
      investigationReset.hide()
                        .parent().removeClass("modifiedParam");
      uncheck(investigateCheckbox);
      enableOrDisable();
      return false;
    });

    // Enable / disable

    investigateRadio.add(investigator).add(removeInvestigation)
                    .change(function() {
                      enableOrDisable();
                    });
  },

  moreConfigurations: function() {
    $j('#allBuildTypes').show();
    $j('#moreBuildTypes, #currentBuildType').hide();
    this._fixElementPlacement();
    return false;
  },

  hideMoreLinkIfNeeded: function() {
    var count = $j('#allBuildTypes input[type=checkbox]').length;
    if (count <= 1) {
      $j('#moreBuildTypes').hide();
    }
  },

  checkUserPermissionsOnChange: function(projectId) {
    BS.ResponsibilityCommon.checkUserPermissionsOnChange($j("#bt-investigation-warning"), $j("#bt-responsible"), projectId);
  },

  submit: function() {
    if (this.formElement().comment.value == this.formElement().comment.defaultValue) {
      this.formElement().comment.value = '';
    }

    //TODO: validation

    $j('#moreBuildTypes').hide();
    var icon = $j('#responsibilityProgressIcon').show();

    BS.FormSaver.save(this, this.getUrl(), OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function() {
        icon.hide();
        BS.reload(true); // TODO: smart reload?
      },

      onFailure: function() {
        icon.hide();
        alert("Problem accessing server");
      }
    }));

    return false;
  },

  setButtonsEnabling: function() {
    var hasCheckedBuildType = $j("#allBuildTypes input[type=checkbox]:checked").length > 0;
    $j("#bt-submit").prop("disabled", !hasCheckedBuildType);
  }
}));

BS.Responsibilities = OO.extend(BS.AbstractWebForm, {
  formElement: function() {
    return $('responsibilitiesFilter');
  },

  savingIndicator: function() {
    return $('respRefreshProgress');
  },

  submitFilter: function() {
    var that = this;
    this.setSaving(true);
    this.disable();
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SimpleListener, {
      onCompleteSave: function() {
        that.setSaving(false);
        that.enable();
        if ($('responsibilitiesTable')) {
          $('responsibilitiesTable').refresh('respRefreshProgress');
        }
      }
    }));

    return false;
  }
});

BS.BulkInvestigateDialog = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('muteTestsFormDialog');
  },

  formElement: function() {
    return $('muteTestsForm');
  },

  getInnerContainer: function() {
    return $('mute-dialog-container');
  },

  getLoadingElement: function() {
    return $('mute-dialog-progress');
  },

  show: function(testsData, fixMode) {
    var size = _.size(testsData);
    if (testsData["projectId"]) --size;
    var title = (fixMode ? "Fix " : "Investigate / Mute ") + size + " Test" + (size == 1 ? "" : "s");
    return this._doShow(title, testsData, fixMode);
  },

  showForTest: function(testId, buildId, projectId, fixMode) {
    var title = fixMode ? "Fix 1 Test" : "Investigate / Mute 1 Test";
    var params = {};
    params[testId] = buildId;
    params["projectId"] = projectId;
    this._lastTestId = testId;
    return this._doShow(title, params, fixMode);
  },

  prepareOnShow: function(assignAutomatically, contextBuildTypeId) {
    (function($) {
      // Utils

      function check(input) {
        return input.prop("checked", true);
      }
      function uncheck(input) {
        return input.prop("checked", false);
      }
      function enable(input) {
        input.prop("disabled", false);
      }
      function disable(input) {
        input.prop("disabled", true);
      }

      // Basic investigation

      var investigateCheckbox = $("#do-investigate"),
          investigateRadio = $("#muteTestsForm input[name=investigate]"),
          assign = $("#assign-investigate"),
          investigator = $("#investigator"),
          removeInvestigation = $("#remove-investigation");

      investigateRadio.change(function() {
        check(investigateCheckbox);
      });
      investigateCheckbox.change(function() {
        if (!$(this).is(":checked")) {
          uncheck(investigateRadio);
        } else {
          check(assign);
        }
      });
      investigator.add(removeInvestigation).change(function() {
        check(assign);
        check(investigateCheckbox);
      });

      // Basic mute

      var muteCheckbox = $("#do-mute"),
          muteRadio = $("#muteTestsForm input[name=mute]"),
          mute = $("#mute-mute"),
          muteOptions = $("#mute-scope, #unmute, #unmute-time, #mute-in-bt-list input");

      muteRadio.change(function() {
        check(muteCheckbox);
      });
      muteCheckbox.change(function() {
        if (!$(this).is(":checked")) {
          uncheck(muteRadio);
        } else {
          check(mute);
        }
      });
      muteOptions.change(function() {
        check(mute);
        check(muteCheckbox);
      });

      // Advanced mute

      $("#unmute-time").placeholder({text: "Click to select the date"});
      $("#unmute-time").focus(function() {
        $("#unmute-time").placeholder("hide");
      });

      $("#mute-scope").change(function() {
        if (this.selectedIndex == 1) {
          $("#mute-in-bt-list").show();
          if (contextBuildTypeId) {
            var buildTypeCheckbox = $("#mute-in-bt-" + contextBuildTypeId);
            if (!buildTypeCheckbox.is(":checked")) {
              check(buildTypeCheckbox);
              $("#mute-in-bt-list").animate({scrollTop: buildTypeCheckbox.position().top}, function() {
                new Effect.Highlight(buildTypeCheckbox.parent()[0]);
              });
            }
          }
        } else {
          $("#mute-in-bt-list").hide();
        }
        $("#unmute-time").placeholder("refresh");
      });

      $("#unmute").change(function() {
        if (this.selectedIndex == 2) {
          $("#unmute-row").show();
        } else {
          $("#unmute-row").hide();
        }
        $("#unmute-time").placeholder("refresh");
      });

      $("#unmute-time").datepicker({
        minDate: +1,
        dateFormat: "dd.mm.yy",
        defaultDate: +7
      }).css({marginTop: 4});

      // Handle defaults

      var investigateDefault = {},
          muteDefault = {};
      var investigateSection = $("#investigate-section"),
          muteSection = $("#mute-section");

      function saveDefault(map, section) {
        section.find("select, input[type=text]").each(function() {
          map[this.id] = $(this).val();
        });
        section.find("input[type=radio], input[type=checkbox]").each(function() {
          map[this.id] = $(this).is(":checked");
        });
      }
      function restoreDefault(map, section) {
        section.find("select, input[type=text]").each(function() {
          $(this).val(map[this.id]).trigger("change");
        });
        section.find("input[type=radio], input[type=checkbox]").each(function() {
          if (map[this.id]) {
            check($(this));
          } else {
            uncheck($(this));
          }
        });
      }

      saveDefault(investigateDefault, investigateSection);
      saveDefault(muteDefault, muteSection);

      // Show / hide [reset]

      var investigationReset = $("#reset-investigate"),
          muteReset = $("#reset-mute");

      investigateRadio.add(investigator).add(removeInvestigation).change(function() {
        investigationReset.show()
                          .parent().addClass("modifiedParam");
      });
      investigationReset.click(function() {
        restoreDefault(investigateDefault, investigateSection);
        investigationReset.hide()
                          .parent().removeClass("modifiedParam");
        uncheck(investigateCheckbox);
        enableOrDisable();
        return false;
      });

      muteRadio.add(muteOptions).change(function() {
        muteReset.show()
                 .parent().addClass("modifiedParam");
      });
      muteReset.click(function() {
        restoreDefault(muteDefault, muteSection);
        muteReset.hide()
                 .parent().removeClass("modifiedParam");
        uncheck(muteCheckbox);
        enableOrDisable();
        return false;
      });

      if (assignAutomatically) {
        check(assign);
        investigationReset.show()
                          .parent().addClass("modifiedParam");
      }

      // Enable / disable submit button

      function enableOrDisable() {
        if (investigateCheckbox.is(":checked") || muteCheckbox.is(":checked")) {
          enable($("#submit"));
        } else {
          disable($("#submit"));
        }
      }
      investigateRadio.add(investigator).add(removeInvestigation)
                      .add(muteRadio).add(muteOptions).change(function() {
        enableOrDisable();
      });
      enableOrDisable();
    })(jQuery);
  },

  _doShow: function(title, params, fixMode) {
    this._reenable(title);
    this._dialogClass(true, "wideDialog");

    this.showAtFixed($(this.getContainer()));
    this.bindCtrlEnterHandler(this.submit.bind(this));

    this._loadContent(window['base_uri'] + "/tests/bulkInvestigate.html", params, fixMode);
    return false;
  },

  submit: function() {
    if (this.validate()) {
      return this._submit(window['base_uri'] + "/tests/bulkInvestigate.html?post=true");
    }
    return false;
  },

  validate: function() {
    var scope = jQuery("#mute-scope").val(),
        unmute = jQuery("#unmute").val(),
        comment = jQuery("#comment").val();

    jQuery("#mute-dialog-container div.error-msg").html("");  // clear previous errors

    if (jQuery("#mute-mute").is(":checked")) {
      if (scope == "C") {
        if (jQuery("#mute-in-bt-list input:checked").length == 0) {
          return this.validationError("#bt-list-error", "Please choose the build type");
        }
      }
      if (unmute == "T") {
        var time = jQuery("#unmute-time").val();
        if (time.length == 0) {
          return this.validationError("#unmute-time-error", "Please specify the unmute date");
        } else if (!/^\d{1,2}\.\d{1,2}\.\d{4}$/.match(time)) {
          return this.validationError("#unmute-time-error", "Please specify the correct unmute date");
        }
      }
    }

    if (comment.length > 2000) {
      return this.validationError("#comment-error", "The comment is too long, only 2000 characters are possible.");
    }

    return true;
  },

  validationError: function(errorElement, message) {
    jQuery(errorElement).html(message);
    return false;
  },

  _reenable: function(title) {
    BS.Util.reenableForm(this.formElement());
    this.setTitle(title);
    this.getInnerContainer().innerHTML = "";
  },

  _loadContent: function(url, params, fixMode) {
    var that = this;
    BS.Util.show(this.getLoadingElement());
    BS.ajaxRequest(url, {
      method: "POST",
      parameters: params,
      onComplete: function(transport) {
        BS.Util.hide(that.getLoadingElement());
        $j(that.getInnerContainer()).html(transport.responseText);   // TW-22300
        that.showCentered();

        if (fixMode) {
          setTimeout(function() {
            that._updateToFixedState();
          }, 500);
        }
      }
    });
  },

  _updateToFixedState: function() {
    var selectFixOption = function(el_id) {
      var checked = $j(el_id).attr("checked");
      if (!checked) {
        $j(el_id).click();
        $j(el_id).trigger("change");
        new Effect.Highlight($j(el_id).next("label")[0]);  // highligh animation (same as "mute in build type")
      }
    };

    selectFixOption('#fix-investigate');
    selectFixOption('#unmute-mute');

    $('comment').value = "I've fixed this!";
    $('comment').activate();

    $("submit").enable();
  },

  _submit: function(url) {
    var that = this,
        loadingIcon = $j("#investigate-saving");

    loadingIcon.show();
    BS.FormSaver.save(this, url, OO.extend(BS.ErrorsAwareListener, {
      onSuccessfulSave: function() {
        loadingIcon.hide();
        that.close();
        that.smartReload();
      },
      onGeneralError: function(elem) {
        alert(elem.firstChild.nodeValue);
      }
    }));
    return false;
  },

  _dialogClass: function(add, className) {
    var dialog = jQuery(this.getContainer());
    if (add) {
      dialog.addClass(className);
    } else {
      dialog.removeClass(className);
    }
  },

  setTitle: function(title) {
    $j('#muteTestsFormTitle').html(title);
  },

  getQuery: function() {
    var query = window.location.search.substring(1),
        vars = query.split("&"),
        map = {};
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split("=");
      map[pair[0]] = pair[1];
    }
    return map;
  },

  // See TW-13653
  smartReload: function() {
    var url = window.location.href,
        uri = window['base_uri'];
    if (!url.startsWith(uri)) {
      // Should never happen. No idea what to do in this case.
      return this.fallbackReload();
    }

    url = url.substr(uri.length);
    var query = this.getQuery(),
        tab = query["tab"];

    if (url == "/" || url.startsWith("/overview.html") ||
        (url.startsWith("/project.html") && (!tab || tab == "projectOverview")) ||
        (url.startsWith("/viewType.html"))) {
      // Dashboard or project / build type overview page, build popup.
      var currentPopup = jQuery("div.summaryContainer").filter(":visible").parent();
      if (!currentPopup.length) {
        return this.fallbackReload();
      }

      var id = currentPopup.attr("id");
      var position = currentPopup.position();
      if (!id || !position) {
        return this.fallbackReload();
      }

      var popup = BS["_BuildResultsSummary" + id.substr("popupRes".length)];
      if (!popup) {
        return this.fallbackReload();
      }

      // Close the popup and open it again (exactly at position it was).
      popup.hidePopup();
      popup._showWithDelay(position.left, position.top);
      return;
    }

    if (url.startsWith("/viewLog.html")) {
      // Build results / build tests page.
      return this.reloadSubtree("buildResults");
    }

    if (url.startsWith("/project.html")) {
      // Some project page, except for overview.
      return this.reloadSubtree("projectContainer");
    }

    if (url.startsWith("/changes.html")) {
      // My changes page. Right now one can only investigate/mute just one test at a time.
      if (this._lastTestId) {
        var refreshable = $("testNameId" + this._lastTestId).up(".refreshable");
        return this.reloadSubtree(refreshable);
      }

      return this.fallbackReload();
    }

    // Some other page...
    return this.fallbackReload();
  },

  reloadSubtree: function(id) {
    var elem = $(id);

    // Append so that it will disappear after refresh.
    $j(BS.loadingIcon).appendTo(elem).position({
      my: "right top",
      at: "left top",
      of: elem
    });

    elem.refresh();

    // Close opened popups.
    jQuery("table.testActionsPopup, div.name-value").filter(":visible").parent("div").hide();
  },

  fallbackReload: function() {
    BS.reload(true);
  },

  checkUserPermissionsOnChange: function(projectId) {
    BS.ResponsibilityCommon.checkUserPermissionsOnChange($j("#test-investigation-warning"), $j("#investigator"), projectId);
  }
}));
