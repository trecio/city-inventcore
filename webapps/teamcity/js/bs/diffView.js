BS.DiffView = {
  //[*[0-lCount, 1-lStart, 2-rCount, 3-rStart, 4-addedEmptyLines, 5-lastLine]]
  changes:[],
  cc:0,

  RTL:false,
  LINE_HEIGHT:17,
  EXPANDER:("<br />&nbsp;"),

  pageUrl:"",
  loadingMessage:"",
  fileName:"",
  beforeLines:0,
  addedLines:0,
  finalLines:0,

  $window:$j(window),
  status:$j("#status"),
  panels:$j("#panels"),
  dbefore:$j("#dbefore"),
  tbefore:$j("#tbefore"),
  dafter:$j("#dafter"),
  tafter:$j("#tafter"),
  dmap:$j("#dmap"),
  mapWindow:$j("#mapWindow"),

  init:function () {
    this.status.html(this.loadingMessage);
    this.sourceDiff();
    this.initClipboard();
    this.$window.resize($j.proxy(this.doResize, this));
  },

  sourceDiff:function () {
    this.status.hide();
    this.doResize();
    this.panels.show();
    this.setupPanelsScrolling();

    // Highlight using HLJS (if the server-side highlighter didn't do anything)
    var highlightBlocks = $j('.needsHighlight');

    highlightBlocks.each(function() {
      var plainContent = $j('#' + this.id + 'Plain');
      var highlighted = hljs.highlightAuto(plainContent.val());
      var highlightedLines = highlighted.value.split('\n');

      BS.Log.info('Detected language: ' + highlighted.language);

      if (highlightedLines.length > 2500) {
        BS.Log.info('File size is too large, highlighting will not be performed');
        return;
      }

      $j(this).find('li').each(function(i) {
        this.innerHTML = highlightedLines[i] || '&nbsp;';
      });
    });

    if (this.changes.length > 0) {
      this.highlightChanges();
      this.buildChangeMiniMap();
      this.scrollToChange();
    } else {
      //no changes
      this.mapWindow.hide();
    }
  },

  highlightChanges:function () {
    var changes = this.changes;
    var j;

    for (var i = 0; i < changes.length; i++) {
      changes[i][4] = changes[i][0];

      for (j = 0; j < changes[i][0]; j++) {
        $j("#l_l_" + (changes[i][1] + j + 1)).addClass('changeRemoved');
      }

      for (j = 0; j < changes[i][2]; j++) {
        $j("#r_l_" + (changes[i][3] + j + 1)).addClass('changeAdded');
      }

      var d = changes[i][2] - changes[i][0];
      if (d > 0) {
        for (j = 0; j < d; j++) {
          $j("#l_l_" + (changes[i][1] + changes[i][0])).append(this.EXPANDER);
          this.addedLines++;
          changes[i][4]++;
        }
      } else if (d < 0) {
        for (j = 0; j < -d; j++) {
          $j("#r_l_" + (changes[i][3] + changes[i][2])).append(this.EXPANDER);
        }
      }
      changes[i][5] = changes[i][3] + this.addedLines;
    }
    this.finalLines = this.beforeLines + this.addedLines;
  },

  buildChangeMiniMap:function () {
    var changes = this.changes;
    var d;
    for (var i = 0; i < changes.length; i++) {
      var t = ($j("#l_l_" + changes[i][1]).get(0).offsetTop / ((this.finalLines - 1) * this.LINE_HEIGHT) * 100).toFixed(2);
      var h = (changes[i][4] / this.finalLines * 100 + .5).toFixed(2);

      t += "%";
      h += "%";

      if (changes[i][0] > 0) {
        d = $j('<div class="changeL"/>');
        d.css('top', t);
        if (parseInt(h, 10) > 0) {
          d.css('height', h);
        }
        d.attr('title', 'Change #' + (i + 1));
        d.attr('i', i);
        d.appendTo(this.dmap);
      }
      if (changes[i][2] > 0) {
        d = $j('<div class="changeR"/>');
        d.css('top', t);
        if (parseInt(h, 10) > 0) {
          d.css('height', h);
        }
        d.attr('title', 'Change #' + (i + 1));
        d.attr('i', i);
        d.appendTo(this.dmap);
      }
    }

    this.dmap.on('click', '.changeL, .changeR', function () {
      BS.DiffView.cc = parseInt(this.getAttribute("i"), 10);
      BS.DiffView.scrollToChange();
    });
  },

  scrollToChange:function () {
    if (this.changes.length > 0) {
      var scrollTop = $j("#l_l_" + this.changes[this.cc][1]).get(0).offsetTop - this.LINE_HEIGHT;
      this.dbefore.scrollTop(scrollTop);
      this.dafter.scrollTop(scrollTop);
    }
  },

  width:0,
  height:0,
  topOffset:$j('#diffView').outerHeight() + $j('#toolbar').outerHeight(),
  mapWidth:40,

  scrollPanels:function () {
    if (this.RTL) {
      if (this.dafter.get(0).scrollHeight > this.dafter.height()) {
        this.dbefore.scrollTop(this.dafter.scrollTop());
        this.dbefore.scrollLeft(this.dafter.scrollLeft());
      }
    } else {
      if (this.dbefore.get(0).scrollHeight > this.dbefore.height()) {
        this.dafter.scrollTop(this.dbefore.scrollTop());
        this.dafter.scrollLeft(this.dbefore.scrollLeft());
      }
    }

    var mapWindowHeight = (this.height * 100 / (this.finalLines * this.LINE_HEIGHT)).toFixed(2);
    var mapWindowTop = (this.dafter.scrollTop() * 99 / (this.finalLines * this.LINE_HEIGHT + .1)).toFixed(2);

    this.mapWindow.css('top', mapWindowTop + "%");
    if (mapWindowHeight > 0) {
      this.mapWindow.css('height', mapWindowHeight + "%");
    }
  },

  setupPanelsScrolling:function () {
    var that = this;

    this.dbefore.on('scroll', function() {
      BS.DiffView.RTL = false;
      that.scrollPanels();
    });
    this.dafter.on('scroll', function() {
      BS.DiffView.RTL = true;
      that.scrollPanels();
    });
  },

  //resize window
  doResize:function () {
    this.width = (this.$window.width() - this.mapWidth) / 2;
    this.height = this.$window.height() - this.topOffset - 1;

    if (this.width < 0 || this.height < 0) return;

    // Webkit browsers have custom-styled scrollbars, thus correction is needed
    var heightCorrection = BS.Browser.webkit ? 3 : 0;

    this.panels.css('height', this.height + 'px');
    this.dbefore.css('height', this.height - heightCorrection + 'px');
    this.dafter.css('height', this.height - heightCorrection + 'px');
    this.dmap.css('height', this.height + 'px');

    this.dbefore.css('left', 0);
    this.dbefore.css('width', this.width + 'px');

    this.tbefore.css('min-width', this.width + 'px');

    this.dafter.css('left', this.width + this.mapWidth + 'px');
    this.dafter.css('width', this.width + 'px');

    this.tafter.css('min-width', this.width + 'px');

    this.dmap.css('left', this.width + 'px');
    this.dmap.css('width', this.mapWidth + 'px');
  },

  ignoreSpaces:function (ignore) {
    var url = this.pageUrl;

    var search = '&ignoreSpaces=' + (ignore ? 'false' : 'true');
    var replace = '&ignoreSpaces=' + (ignore ? 'true' : 'false');

    if (url.indexOf(search) != -1) {
      url = url.replace(search, replace);
    } else {
      url += replace;
    }
    window.location = url;
  },

  initClipboard:function () {
    var clipboardButtons = $j('#toolbar .clipboard');

    var lineFeed = BS.Util.getLineFeed();

    var prepareContent = function (el) {
      var contentId = $j(el).data('for');
      var processedContent = '';

      $j(BS.Util.escapeId(contentId)).find('li').each(function () {
        // u00A0 is &nbsp;
        processedContent += $j(this).text().replace(/\u00A0/g, ' ') + lineFeed;
      });

      return processedContent;
    };

    if (window['clipboardData']) {
      clipboardButtons.click(function () {
        window.clipboardData.setData("Text", prepareContent(this));
        return false;
      });
    } else if (swfobject.getFlashPlayerVersion().major > 0) {
      clipboardButtons.zclip({
        path:window.base_uri + "/img/ZeroClipboard.swf",
        copy:function () {
          return prepareContent(this);
        },
        afterCopy:function () {
        }
      });
    } else {
      clipboardButtons.css('visibility', 'hidden');
    }
  }
};
