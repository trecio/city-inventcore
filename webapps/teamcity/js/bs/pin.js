BS.Pin = {
  noHover: false,
  _onBuildPage: false,

  pin: function(onBuildPage) {
    if (onBuildPage) {
      BS.Pin._onBuildPage = true;
    }
    BS.Pin.togglePin(true, 'Unpin');
  },

  unpin: function(onBuildPage) {
    if (onBuildPage) {
      BS.Pin._onBuildPage = true;
    }
    BS.Pin.togglePin(false, 'Pin');
  },

  togglePin: function(toPin, newLinkText) {
    var buildId = BS.PinBuildDialog.formElement().buildId.value;
    var link = $('pinLink' + buildId);    

    BS.Util.show("progressIcon" + buildId);
    BS.Util.hide(link);

    BS.FormSaver.save(BS.PinBuildDialog, BS.PinBuildDialog.formElement().action, OO.extend(BS.ErrorsAwareListener, {

      onCompleteSave: function(form, responseXML, err) {
        $(link.parentNode).cleanWhitespace();

        BS.Util.hide("progressIcon" + buildId);
        BS.Util.show(link);

        BS.reload(true);
      },

      onFailure: function() {
        BS.Util.show(link);
        BS.Util.hide("progressIcon" + buildId);
        alert("Problem accessing server");
      }
    }));
  },

  showSuccessMessage: function(buildId, pinned) {
    var link = $('pinLink' + buildId);
    var tr = link.parentNode.parentNode;
    $(tr).cleanWhitespace();
    for (var i = 0; i < tr.childNodes.length; i++) {
      var td = tr.childNodes[i];
      if (td.tagName == 'TD') {
        if (pinned) {
          td.style.backgroundColor = '#FFFFCC';
          BS.Util.show('successMessage');
          $('successMessage').innerHTML = "Pinned build won't be removed from the history list until you unpin it.";
        }
        else {
          td.style.backgroundColor = 'white';
          BS.Util.show('successMessage');
          $('successMessage').innerHTML = "The build has been unpinned.";
        }

        BS.Util.hideSuccessMessages();
        $('successMessage').style.backgroundColor = '#ffffcc';
      }
    }
  },

  installHover: function(td) {
    if (BS.Pin.noHover) return;

    $(td).cleanWhitespace();

    var buildId = td.id.substring("pinTd".length);
    var img = $('pinImg' + buildId);
    var link = $('pinLink' + buildId);
    BS.Util.hide(link);
    link.addClassName("unpinLink");

    td.on("mouseover", function() { BS.Util.show(link); });
    td.on("mouseout", function() { BS.Util.hide(link); });

    BS.Util.show(img);
  },

  uninstallHover: function(td) {
    var buildId = td.id.substring("pinTd".length);
    var img = $('pinImg' + buildId);
    var link = $('pinLink' + buildId);
    if (link.className) {
      link.removeClassName("unpinLink");
    }
    if (img.style) {
      BS.Util.hide(img);
    }
    td.stopObserving();
  }
};

BS.PinBuildDialog = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  formElement: function() {
    return $('pinBuildForm');
  },

  getContainer: function() {
    return $('pinBuildFormDialog');
  },

  onText: "Pin",
  offText: "Unpin",

  appendTag: function(tag) {
    BS.Util.addWordToTextArea(this.formElement().buildTagsInfo, tag);
  },

  showPinBuildDialog: function(buildId, pin, escapedTags, partOfChain, defaultMessage) {
    this.formElement().buildId.value = buildId;

    var pinText = pin ? this.onText : this.offText;

    this.formElement().pin.value = pin;
    this.formElement().PinSubmitButton.value = pinText;
    $('pinBuildFormTitle').innerHTML = pinText + ' build' ;

    this.formElement().pinComment.value = defaultMessage != null && defaultMessage.length > 0 ? defaultMessage : this.formElement().pinComment.defaultValue;

    this.formElement().buildTagsInfo.value = escapedTags;

    if (partOfChain) {
      BS.Util.show("applyToAllDiv");
    }

    this.showCentered();
    this.formElement().pinComment.focus();
    this.formElement().pinComment.select();

    this.bindCtrlEnterHandler(this.submit.bind(this));

    return false;
  },

  submit: function() {
    var onBuildPage = this.formElement().onBuildPage.value == 'true';

    if (this.formElement().pinComment.value == this.formElement().pinComment.defaultValue) {
      this.formElement().pinComment.value = "";
    }

    var pin = this.formElement().pin.value == 'true';

    if (pin) {
      BS.Pin.pin(onBuildPage);
    } else {
      BS.Pin.unpin(onBuildPage);
    }

    return false;
  }
}));
