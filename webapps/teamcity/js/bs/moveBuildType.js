/*
 * See also BS.CopyBuildTypeForm.
 */
BS.MoveForm = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  _projects: new Array(),
  _createMoveErrorsListener: function() {
    var that = this;
    return OO.extend(BS.ErrorsAwareListener, {
      onSaveProjectErrorError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onProjectNotFoundError: function(elem) {
        $("error_" + that.formElement().id + '_projectId').innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField(that.formElement().projectId);
      },

      onCannotMoveError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          BS.XMLResponse.processRedirect(responseXML);
        }
      }
    });
  },

  _fillInProjectsSelector: function(currentProjectId) {
    var selector = this.formElement().projectId;
    selector.options.length = 0;
    for (var i=0; i<this._projects.length; i++) {
      var p = this._projects[i];
      if (p.id == currentProjectId) continue;
      var opt = new Option(p.name, p.id);
      selector.options.add(opt);
    }
    BS.jQueryDropdown(BS.Util.escapeId(selector.id)).ufd("changeOptions");
  }
}));

BS.MoveBuildTypeForm = OO.extend(BS.MoveForm, {
  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('moveBuildTypeProgress');
    } else {
      BS.Util.hide('moveBuildTypeProgress');
    }
  },

  getContainer: function() {
    return $('moveBuildTypeFormDialog');
  },

  formElement: function() {
    return $('moveBuildTypeForm');
  },

  showDialog: function(sourceBuildTypeId, sourceProjectId) {
    BS.AdminActions.checkBuildTypeHasLocalVcsRoots(sourceBuildTypeId, function(hasLocalVcsRoots) {
      BS.MoveBuildTypeForm.formElement().setShowVcsRootsSection(hasLocalVcsRoots);
      BS.MoveBuildTypeForm.formElement().buildTypeId.value = sourceBuildTypeId;
      BS.MoveBuildTypeForm.formElement().sourceProjectId.value = sourceProjectId;
      BS.MoveBuildTypeForm._fillInProjectsSelector(sourceProjectId);

      BS.MoveBuildTypeForm.showCentered();
      BS.MoveBuildTypeForm.formElement().projectId.onchange();
      BS.MoveBuildTypeForm.formElement().projectId.focus();
      BS.MoveBuildTypeForm.bindCtrlEnterHandler(BS.MoveBuildTypeForm.submitMove.bind(BS.MoveBuildTypeForm));
    });
  },

  cancelDialog: function() {
    this.close();
  },

  afterClose: function() {
    this.clearErrors();
  },

  submitMove: function() {
    BS.FormSaver.save(this, this.formElement().action, OO.extend(this._createMoveErrorsListener(), {
      onMaxNumberOfBuildTypesReachedError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onBuildTypeNotFoundError: function(elem) {
        alert(elem.firstChild.nodeValue);
        BS.reload(true);
      }
    }));

    return false;
  }
});

BS.MoveTemplateForm = OO.extend(BS.MoveForm, {
  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('moveTemplateProgress');
    } else {
      BS.Util.hide('moveTemplateProgress');
    }
  },

  getContainer: function() {
    return $('moveTemplateFormDialog');
  },

  formElement: function() {
    return $('moveTemplateForm');
  },

  showDialog: function(templateId, sourceProjectId) {
    BS.AdminActions.checkTemplateHasLocalVcsRoots(templateId, function(hasLocalVcsRoots) {
      BS.MoveTemplateForm.formElement().templateId.value = templateId;
      BS.MoveTemplateForm.formElement().sourceProjectId.value = sourceProjectId;
      BS.MoveTemplateForm._fillInProjectsSelector(sourceProjectId);

      BS.MoveTemplateForm.showCentered();
      Form.Element.enable(BS.MoveTemplateForm.formElement().moveTemplate);
      BS.MoveTemplateForm.formElement().projectId.focus();
      BS.MoveTemplateForm.bindCtrlEnterHandler(BS.MoveTemplateForm.submitMove.bind(BS.MoveTemplateForm));

      var moveTemplate_vcsRootsSection = $("moveTemplate_vcsRootsSection");
      if (hasLocalVcsRoots) {
        moveTemplate_vcsRootsSection.show();
        moveTemplate_vcsRootsSection.onshow();
      } else {
        moveTemplate_vcsRootsSection.hide();
      }
    });

  },

  cancelDialog: function() {
    this.close();
  },

  afterClose: function() {
    this.clearErrors();
  },

  submitMove: function() {
    var that = this;

    BS.FormSaver.save(this, this.formElement().action, OO.extend(this._createMoveErrorsListener(), {
      onTemplateNotFoundError: function(elem) {
        alert(elem.firstChild.nodeValue);
        BS.reload(true);
      }}));

    return false;
  }
});
