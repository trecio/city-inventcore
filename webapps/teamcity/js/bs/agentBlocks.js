BS.AgentBlocks = OO.extend(BS.BaseMultiElementBlocks, {
  blocksType: "agentPool",
  collapsedByDefault: false,
  collapsedIcon: "/img/blockCollapsed.gif",
  expandedIcon: "/img/blockExpanded.gif",

  getBlockContentElement: function(id) {
    return $j(".agentRow-" + id);
  },

  iterateHandles: function(id, handler) {
    $j(".agentBlockHandle-" + id).each(function() {
      handler(this);
    });
  },

  iterateBlocks: function(handler) {
    $j(".agentBlockHandle").each(function() {
      handler(this.id.replace(/^agentBlockHandle:/, ''));
    });
  }
});
