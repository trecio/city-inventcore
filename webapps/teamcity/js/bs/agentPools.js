BS.AP = {


  agentKeywords: {},


  init: function () {

    document.onkeydown = function(event) {
      event = event || window.event;
      if (event.keyCode == 27)
        BS.AP.escape();
      if (event.keyCode == 13)
        return false;
    }

  },


  //// POOLS MANIPULATION \\\\



  //// AGENT TYPES MANIPULATION \\\\


  clickOnAddAgentType: function (poolId, nearestElement) {
    var urlParams = "pool="+poolId;
    BS.PopupDialog.show(nearestElement, "pool-agent-types-popup", urlParams, function() {
      BS.reload(true);
    });
  },

  getAgtItem: function(poolId, agtId) {
    var crossId = "pool_" + poolId + "_agt_" + agtId;
    return $j("#row_"+crossId);
  },

  clickOnDropAgentCross: function(agtId, showMoveAgentToDefaultPoolWarning, agentName) {
    if (showMoveAgentToDefaultPoolWarning) {
      BS.MoveAgentToDefaultPoolDialog.showDialog(agtId, agentName);
    }
    else {
      this.doDropAgent(agtId);
    }
  },

  doDropAgent: function(agtId) {
    var params = "action=MoveAgentsAction&agt=" + agtId + "&pool=0";
    BS.Util.hide("drop-agt-" + agtId + "-cross");
    this.ajax("drop-agt-" + agtId + "-progress", params);
  },



  //// PROJECTS MANIPULATION \\\\


  clickOnAssociateProjects: function (poolId, nearestElement) {
    var urlParams = "pool="+poolId;
    BS.PopupDialog.show(nearestElement, "pool-projects-popup", urlParams, function() {
      BS.reload(true);
    });
  },


  clickOnDropProjectCross: function (poolId, projectId, crossId, showLastProjectWarning, poolName, projectName) {
    if (showLastProjectWarning) {
      BS.DissociateLastProjectDialog.showDialog(poolId, projectId, crossId, poolName, projectName);
    }
    else {
      this.doDropProject(poolId, projectId, crossId);
    }
  },

  doDropProject: function(poolId, projectId, crossId) {
    var urlparams = "action=ConfigureProjectsAction&pool=" + poolId + "&dissociatedProject=" + projectId;
    BS.Util.hide(crossId);
    this.ajax(crossId + "-progress", urlparams);
  },


  //// UTILS \\\\


  getPoolBox: function(poolId) {
    return $j("#pool-" + poolId).first();
  },

  escape: function() {

  },

  showSingularOrPlural: function(spanIdPrefix, count) {
    var singularId = spanIdPrefix + "-singular";
    var pluralId = spanIdPrefix + "-plural";
    if (count == 1) {
      BS.Util.show(singularId);
      BS.Util.hide(pluralId);
    }
    else {
      BS.Util.hide(singularId);
      BS.Util.show(pluralId);
    }
  },

  ajax: function(progressId, params) {
    BS.Util.show(progressId);
    BS.ajaxRequest(window['base_uri'] + "/agentPools.html", {
      parameters: params,
      onComplete: function(transport) {
        var respXML = transport.responseXML;
        if (respXML) {
          var hasError = BS.XMLResponse.processErrors(respXML, {
            onAgentPoolActionError: function(elem) {
              BS.Util.hide(progressId);
              alert(elem.firstChild.nodeValue);
            }
          });
          if (!hasError) {
            var poolIdNode = respXML.documentElement.firstChild;
            if (poolIdNode && poolIdNode.nodeName == "pool-id") {
              var poolId = poolIdNode.textContent;
              document.location.href = document.location.pathname + document.location.search + "&" + poolId + "#" + poolId;
            }
            else {
              BS.reload(true);
            }
          }
        }
      }
    });
  },


  //// ACTIONS \\\\


  confirmCreateNewPool: function() {
    var gatherParameters = function(dialog, userText) {
      var poolName = encodeURIComponent(userText);
      return "action=CreateNewPoolAction&name=" + poolName;
    };

    BS.PoolNameDialog.showDialog("Create New Pool", "", gatherParameters, false);
  },

  confirmRenamePool: function(poolId) {
    var pool = this.getPoolBox(poolId);
    var oldName = pool.find(".pool-name").text();
    var title = "Rename Pool \"" + oldName + "\"";

    var gatherParameters = function(dialog, userText) {
      var poolName = encodeURIComponent(userText);
      return "action=RenamePoolAction&pool=" + poolId + "&name=" + poolName;
    };

    BS.PoolNameDialog.showDialog(title, oldName, gatherParameters, true);
  },

  confirmRemovePool: function(poolId) {
    var pool = this.getPoolBox(poolId);
    var poolName = pool.find(".pool-name").text();
    var agts = pool.find(".agt.item.existent");
    var projects = pool.find(".pro.item.existent");

    BS.RemovePoolDialog.showDialog(poolId, poolName, agts, projects)
  }
};


BS.PoolNameDialog = OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $("PoolNameDialog");
  },

  gatherParameters: function(dialog, userText) {},

  showDialog: function(title, defaultText, gatherParameters, rename) {
    this.gatherParameters = gatherParameters;
    $("PoolNameDialogTitle").innerHTML = title.escapeHTML();
    $("PoolNameDialogInputField").value = defaultText;
    $("PoolNameDialogSubmitButton").value = rename ? "Rename Pool" : "Create Pool";

    this.showCentered();
    $("PoolNameDialogInputField").focus();
    $("PoolNameDialogInputField").select();

    this.bindEnterHandler(this.doIt.bind(this));
  },

  doIt: function() {
    var dialog = $j(this);
    var userText = BS.Util.trimSpaces($("PoolNameDialogInputField").value);

    BS.AP.ajax("agentPoolNameProgress", BS.PoolNameDialog.gatherParameters(dialog, userText));
  }
});


BS.RemovePoolDialog = OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $("RemovePoolDialog");
  },

  poolIdToRemove: 0,

  showDialog: function(poolId, poolName, agts, projects) {
    this.poolIdToRemove = poolId;

    this.setupDialog(poolName, agts, projects);

    this.showCentered();
    this.bindEnterHandler(this.doIt.bind(this));
  },

  setupDialog: function(poolName, agts, projects) {
    $j("#remove-pool-name").text(poolName);
    $j("#remove-agents-number").text(agts.length);
    $j("#remove-projects-number").text(projects.length);
    BS.AP.showSingularOrPlural("remove-agents", agts.length);
    BS.AP.showSingularOrPlural("remove-projects", projects.length);

    if (agts.length) {
      $j("#remove-agents-message").show();
    }
    else {
      $j("#remove-agents-message").hide();
    }

    if (projects.length) {
      $j("#remove-projects-message").show();
    }
    else {
      $j("#remove-projects-message").hide();
    }
  },

  doIt: function() {
    BS.AP.ajax("agentPoolRemoveProgress", "action=RemovePoolAction&pool=" + BS.RemovePoolDialog.poolIdToRemove);
  }
});


BS.DissociateLastProjectDialog = OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $("DissociateLastProjectDialog");
  },

  _onConfirm: null,

  showDialog: function(poolId, projectId, crossId, poolName, projectName) {
    this._onConfirm = function() {
      BS.AP.doDropProject(poolId, projectId, crossId);
    };

    this.setupDialog(poolName, projectName);

    this.showCentered();
    this.bindEnterHandler(this.doIt.bind(this));
  },

  setupDialog: function(poolName, projectName) {
    $j("#dissociate-last-project-from-pool-name").text(poolName);
    $j("#dissociate-last-project-name").text(projectName);
  },

  doIt: function() {
    this.close();
    if (this._onConfirm) {
      this._onConfirm();
    }
  }
});


BS.MoveAgentToDefaultPoolDialog = OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $("MoveAgentToDefaultPoolDialog");
  },

  _onConfirm: null,

  showDialog: function(agtId, agentName) {
    this._onConfirm = function() {
      BS.AP.doDropAgent(agtId);
    };

    $j("#move-agent-to-default-pool-name").text(agentName);

    this.showCentered();
    this.bindEnterHandler(this.doIt.bind(this));
  },

  doIt: function() {
    this.close();
    if (this._onConfirm) {
      this._onConfirm();
    }
  }
});


BS.SelectAgentsDialog = OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $("SelectAgentsDialog");
  },

  destPoolId: 0,

  showDialog: function(destPoolId, destPoolName) {

    this.destPoolId = destPoolId;

    BS.Util.hide("SelectAgentsContainer");
    BS.Util.show("LoadingStub");
    $j("#SelectAgentsDialogSubmitButton").attr("disabled","disabled");
    $j("#SelectAgentsFilter").attr("value","");

    var url = window['base_uri'] + "/agentPools.html?query=agentsToMove&pool=" + destPoolId;

    var loadOptions = {
      method: "get",
      onComplete: function() {
        BS.Util.show("SelectAgentsContainer");
        BS.Util.hide("LoadingStub");
        BS.SelectAgentsDialog.showCentered();
        $j("#SelectAgentsDialogSubmitButton").removeAttr("disabled");
      }
    };

    BS.ajaxUpdater($("SelectAgentsContainer"), url, loadOptions);

    this.showCentered();
    this.bindEnterHandler(this.doIt.bind(this));
  },

  doIt: function() {
    var params = "action=MoveAgentsAction&pool=" + this.destPoolId;
    var checkedAgents =
      $j("#agents-to-move").find("input[type=checkbox]:checked");
    checkedAgents.each(function() {
      var id = $j(this).attr("name").replace(/^.*-/,'');
      params += '&agt=' + id;
    });

    BS.AP.ajax("movingAgentsProgress", params);
  }
});


BS.AgentPoolProjectsDialog = OO.extend(BS.AbstractVisibleDialog, {
  getPrefix: function() {
    return "agent_pool_";
  },

  _agentPoolId: -1,

  visibleObjectsAreSorted: function() {
    return true;
  },

  show: function(poolId) {
    BS.AgentPoolProjectsDialog._agentPoolId = poolId;

    var poolBox = $j("#pool-"+poolId);
    $("agent_pool_visibleFormTitle").innerHTML = "Configure projects associated with agent pool \"" + poolBox.find(".pool-name").html() + "\"";

    var projectIds = {};
    poolBox.find(".project-name").each(function() {
      projectIds[this.id.replace(/^.*project-/, '')] = 1;
    });

    this.doMoveTo(this.getVisibleObjects(), this.getHiddenObjects(), true, function() { return true; });
    this.doMoveTo(this.getHiddenObjects(), this.getVisibleObjects(), true, function(option) {
      return projectIds[option.value] != null;
    });

    this.selectAll(this.getVisibleObjects(), false);
    this.selectAll(this.getHiddenObjects(), false);

    var filter = $(this.getHiddenObjectsFilter());
    filter.value = "";
    filter.onkeyup();

    this.showCentered();
    this.updateButtons();

    filter.focus();

    this.bindCtrlEnterHandler(this.submitForm.bind(this));
  },

  submitForm: function() {
    var params = "action=configureProjectsAction&pool=" + BS.AgentPoolProjectsDialog._agentPoolId;

    var dissociateSelected = $("dissociateFromOtherPoolsCheckbox").checked;

    this.iterateOptions(this.getVisibleObjects(), function(option) {
      var projectId = option.value;
      params += "&associatedProject=" + projectId;
      if (dissociateSelected && option.selected) {
        params += "&dissociatedFromOthersProject=" + projectId;
      }
    });

    this.iterateOptions(this.getHiddenObjects(), function(option) {
      params += "&dissociatedProject=" + option.value;
    });

    BS.AP.ajax("agentPoolProjectsProgress", params);
  }
});
