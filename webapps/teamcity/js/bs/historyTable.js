
BS.HistoryTable = OO.extend(BS.AbstractWebForm, {
  setSaving: function(saving) {
    var savingElem = $("savingFilter");
    if (savingElem) {
      if (saving) {
        BS.Util.show(savingElem);
      } else {
        BS.Util.hide(savingElem);
      }
    }
  },

  formElement: function() {
    return $('historyFilter');
  },

  update: function(moreParams) {
    $('historyTable').refresh('savingFilter', moreParams, function() {
      Form.enable(BS.HistoryTable.formElement());
    });
  },

  doSearch: function() {
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SimpleListener, {
      onBeginSave: function(form) {
        Form.disable(form.formElement());
        form.setSaving(true);
      },
      onCompleteSave: function() {
        BS.HistoryTable.update();
      }
    }));
    return false;
  }
});
