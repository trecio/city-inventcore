BS.Projects = {
  updateProjectView: function(projectId, firstProjectInList, force) {
    force = !!force || false;
    BS.reload(force, function() {
      BS.Projects._updateProjectView("p_" + projectId, projectId, firstProjectInList)
    });
  },

  _updateProjectView: function(elementId, projectId, firstProjectInList, moreParams) {
    moreParams = moreParams || {};
    BS.Branch.addBranchToParams(moreParams, projectId);
    var params = $H(_.extend(moreParams, {
      projectId: projectId,
      firstProjectInList: !!firstProjectInList
    }));

    BS.ajaxUpdater(elementId, window['base_uri'] + '/overview.html', {
      parameters: params.toQueryString(),
      evalScripts: true,
      insertion: function(container, response) {
        BS.stopObservingInContainers(container);
        container.update(response);
        BS.SystemProblems.updateNow();
      }
    });
  },

  updateView: function() {
    BS.reload(false, function() {
      $('projectContainer').refresh();
    });
  },

  hideProject: function(projectId) {
    BS.ajaxRequest("ajax.html?hideProject=" + projectId, {
      onSuccess: function() {
        var projectEl = $('p_' + projectId);
        new Effect.Fade(projectEl, {
          afterFinish: function() {
            projectEl.remove();

            var divs = $('buildTypes').getElementsByTagName('DIV');
            for (var i =0; i < divs.length; i++) {
              if (divs[i].className == 'projectHeader') return;
            }

            new Effect.Highlight("configVisibleProjects");
          }
        });
      }
    });
    return false;
  },

  addNewProject: function(projectId) {
    this.markProject(projectId, true);
    return this.addOrHideProject("addProject", projectId, "new_" + projectId, true);
  },

  hideNewProject: function(projectId) {
    this.markProject(projectId, false);
    return this.addOrHideProject("hideProject", projectId, "new_" + projectId, false);
  },

  addAllProjects: function(projectIds) {
    projectIds = this.filterMarked(projectIds, false);
    return this.addOrHideProject("addProject", projectIds, "new-projects", true)
  },

  hideAllProjects: function(projectIds) {
    projectIds = this.filterMarked(projectIds, true);
    return this.addOrHideProject("hideProject", projectIds, "new-projects", false);
  },

  markProject: function(projectId, value) {
    if (!this._marked) {
      this._marked = [];
    }
    this._marked.push([projectId, value]);
  },

  filterMarked: function(projectIds, valueToFilter) {
    if (this._marked) {
      for (var i = 0; i < this._marked.length; ++i) {
        var current = this._marked[i];
        if (current[1] == valueToFilter) {
          projectIds = projectIds.replace(current[0] + ",", "");
        }
      }
    }
    return projectIds;
  },

  addOrHideProject: function(action, arg, fadeElement, refresh) {
    BS.ajaxRequest("ajax.html", {
      parameters: action + "=" + arg,
      onSuccess: function() {
        if (refresh) {
          $("overviewMain").refresh();
        }
        var visibleNumber = jQuery("#new-projects-list .entry").filter(":visible").length;
        if (fadeElement != "new-projects" && visibleNumber == 1) {
          new Effect.Fade("new-projects", {
            afterFinish: function() {
              new Effect.Highlight("configVisibleProjects");
            }
          });
        } else {
          new Effect.Fade(fadeElement);
        }

        if ((fadeElement == "new-projects" || visibleNumber == 1) &&
            $("sp_span_newProjectsPopupContent")) {
          new Effect.Fade("sp_span_newProjectsPopupContent");
        }
      }
    });
    return false;
  },

  installShowHideLink: function(projectVisible, projectId) {
    var link = $j("<a/>").attr({
      href: "#",
      "class": "btn btn_mini pinLink",
      title: projectVisible ? "Project is shown on the overview page. Click to hide" : "Click to show project on the overview page"
    }).click(function() {
      return BS.AllProjectsPopup.toggleOverview(this, projectId, {
        loadingTop: 2,
        loadingLeft: 32,
        updateParent: false
      });
    });

    var img = $j("<img/>").attr({
      src: window["base_uri"] + (projectVisible ? "/img/watched.png" : "/img/watch.png"),
      alt: "",
      "class": "pinLinkImage"
    }).appendTo(link);

    link.prependTo('#topWrapper .quickLinks');
    link.wrap('<div class="toolbarItem"/>');
  }
};

jQuery(function() {
  BS.lazyLoadProject = function(projectId, isFirst) {
    var blockId = "btbovr_" + projectId,
        block = jQuery(BS.Util.escapeId(blockId));

    if (!jQuery.trim(block.html()) && block.is(":visible")) {
      block.html("<span style='margin: 0.5em;'>" + BS.loadingIcon + " Loading...</span>");

      BS.Projects._updateProjectView(blockId, projectId, isFirst, {
        __fragmentId: blockId,
        force_show_build_types: true
      });
    }
  }
});
