
BS.ServerConfigForm = OO.extend(BS.AbstractWebForm, {

  setupEventHandlers: function() {
    var that = this;
    this.removeUpdateStateHandlers();
    this.setUpdateStateHandlers({
      updateState: function() {
        that.storeInSession();
      },
      saveState: function() {
        that.submitSettings();
      }
    })
  },

  storeInSession: function() {
    $('submitSettings').value = 'storeInSession';
    BS.FormSaver.save(this, this.formElement().action, BS.StoreInSessionListener);
  },

  submitSettings: function() {
    $('submitSettings').value = 'store';

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onInvalidRootUrlError: function(elem) {
        $("invalidRootUrl").innerHTML = elem.firstChild.nodeValue;
        BS.ServerConfigForm.highlightErrorField($("rootUrl"));
      },

      onInvalidMaxArtifactSizeError: function(elem) {
        $("invalidMaxArtifactSize").innerHTML = elem.firstChild.nodeValue;
        BS.ServerConfigForm.highlightErrorField($("maxArtifactSize"));
      },

      onInvalidDefaultExecutionTimeoutError: function(elem) {
        $("invalidDefaultExecutionTimeout").innerHTML = elem.firstChild.nodeValue;
        BS.ServerConfigForm.highlightErrorField($("defaultExecutionTimeout"));
      },

      onInvalidDefaultModificationCheckIntervalError: function(elem) {
        $("invalidDefaultModificationCheckInterval").innerHTML = elem.firstChild.nodeValue;
        BS.ServerConfigForm.highlightErrorField($("defaultModificationCheckInterval"));
      },

      onInvalidDefaultQuietPeriodError: function(elem) {
        $("invalidDefaultQuietPeriod").innerHTML = elem.firstChild.nodeValue;
        BS.ServerConfigForm.highlightErrorField($("defaultQuietPeriod"));
      },

      onEmptyGuestUsernameError: function(elem) {
        $("invalidGuestUsername").innerHTML = elem.firstChild.nodeValue;
        BS.ServerConfigForm.highlightErrorField($("guestUsername"));
      },

      onGuestUsernameUsedError: function(elem) {
        $("invalidGuestUsername").innerHTML = elem.firstChild.nodeValue;
        BS.ServerConfigForm.highlightErrorField($("guestUsername"));
      },

      onSaveError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onCompleteSave: function(form, responseXml, wereErrors) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXml, wereErrors);
        if (!wereErrors) {
          $('serverSettings').refresh();
        }
      }
    }));

    return false;
  }
});
