
/*
 * Copyright 2000-2012 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

BS.AbstractModalDialog = {
  _attachedToRoot: false,

  /** should return $('element id') for modal dialog element **/
  getContainer: function() {},

  beforeShow: function() {},

  afterClose: function() {},

  getDragHandle: function() {
    return $(this.getContainer()).getElementsBySelector('.dialogHandle')[0];
  },

  bindCtrlEnterHandler: function(handler) {
    var f = function() {
      this._ctrlEnterHandler = function(event) {
        if (event.ctrlKey && event.keyCode == Event.KEY_RETURN) {
          handler && handler();
        }
      }.bindAsEventListener(this);

      $(document).on("keydown", this._ctrlEnterHandler);
    }.bind(this);

    f.defer();
  },

  bindEnterHandler: function(handler) {
    var f = function() {
      this._enterHandler = function(event) {
        if (event.keyCode == Event.KEY_RETURN) {
          handler && handler();
        }
      }.bindAsEventListener(this);

      $(document).on("keydown", this._enterHandler);
    }.bind(this);

    f.defer();
  },

  unbindCtrlEnterHandler: function() {
    if (!this._ctrlEnterHandler) return;
    Event.stopObserving(document, "keydown", this._ctrlEnterHandler, false);
    this._ctrlEnterHandler = null;
  },

  _getDialogForm: function(dialog) {
    return dialog.parentNode.tagName.toLowerCase() == 'form' && dialog.parentNode.getAttribute('id') + 'Dialog' == dialog.getAttribute('id')
        ? dialog.parentNode
        : dialog;
  },

  _removeDuplicateElements: function() {
    var el = $(this.getContainer());
    var origId = el.id;
    el.id = '_' + origId;
    var dialog = $(origId);
    while(dialog) {
      var dialogForm = this._getDialogForm(dialog);
      dialogForm.parentNode.removeChild(dialogForm);
      dialog = $(origId);
    };

    el.id = origId;
  },

  _fixElementPlacement: function() {
    this._removeDuplicateElements();
    if (this._attachedToRoot) return;

    var dialog = $(this.getContainer());
    var dialogForm = this._getDialogForm(dialog);

    var mainContent = $('mainContent');

    if (dialogForm.parentNode != mainContent) {
      mainContent.appendChild(dialogForm);
      this._attachedToRoot = true;
    }
  },

  showAt: function(x, y) {
    this._fixElementPlacement();

    var dialog = $(this.getContainer());
    this.beforeShow();

    BS.Util.place(dialog, x, y);

    this._showDialog(dialog);
  },

  showAtFixed: function(dialog) {
    this._fixElementPlacement();

    this.beforeShow();
    dialog.addClassName('modalDialogFixed');
    this.positionAtFixed(dialog);

    $j(window).off('resize.modalDialog scroll.modalDialog').on('resize.modalDialog scroll.modalDialog', function() {
      this.positionAtFixed(dialog);
    }.bind(this));

    this._showDialog(dialog);
  },

  positionAtFixed: function(dialog) {
    if (!dialog) return;

    var $dialog = $j(dialog);
    var $window = $j(window);

    function pos() {
      $dialog.position({
        my: "center",
        at: "center",
        of: window,
        using: function(position) {
          $dialog.css({
            top: Math.max(0, position.top), 
            left: position.left,
            visibility: 'visible'
          });

          // Positioning workaround for jQuery 1.7/jQuery UI 1.8
          if ($window.scrollTop() > position.top) {
            setTimeout(function() {
              $window.trigger('scroll.modalDialog');
            }, 0);
          }
        }
      });
    }

    if (BS.Browser.opera) {
      $dialog.css('visibility', 'hidden');
      setTimeout(pos, 0);
    } else {
      pos();
    }
  },

  _showDialog: function(dialog) {
    var afterHide = function() {
      this.removeOverlappingDiv(dialog);
      this.resetPosition(dialog);
      this.afterClose();
      this.unbindCtrlEnterHandler();
      this._attachedToRoot = false;
    }.bind(this);

    BS.Hider.showDivWithTimeout(dialog.id, {
      hideOnMouseOut: false,
      hideOnMouseClickOutside: false,
      afterHideFunc: afterHide,
      draggable: true,
      dragHandle: this.getDragHandle()
    });

    dialog.setAttribute('data-modal', true);
    this.showOverlappingDiv(dialog);
  },

  isVisible: function() {
    return BS.Util.visible(this.getContainer());
  },

  showCentered: function() {
    var dialog = $(this.getContainer());
    this._fixElementPlacement();
    this.showAtFixed(dialog);
  },

  recenterDialog: function() {
    this._fixElementPlacement();

    var dialog = $(this.getContainer());
    var dimensions = dialog.getDimensions();
    if (dimensions.height > dialog.getAttribute('data-height') || 0) {
      this.positionAtFixed(dialog);
      this.updateDialog();
    }
  },

  close: function() {
    this.doClose();
  },

  doClose: function() {
    if (this.getContainer()) {
      BS.Hider.hideDiv(this.getContainer().id);
    }
  },

  _overlappingDivId: function() {
    return 'overlappingDiv';
  },

  getOverlappingDiv: function() {
    var id = this._overlappingDivId();
    var overlappingDiv = $(id);
    if (overlappingDiv) {
      return overlappingDiv;
    }

    var newOverlappingDiv =
      $j("<div/>").attr("id", id)
                  .hide()
                  .appendTo($j("#mainContent"));

    return newOverlappingDiv.get(0);
  },

  showOverlappingDiv: function(dialog) {
    var overlappingDiv = this.getOverlappingDiv();
    var dialogZindex = dialog.style.zIndex;

    overlappingDiv.style.zIndex = "" + (parseInt(dialogZindex, 10) - 1);

    if (!BS.Util.visible(overlappingDiv)) {
      BS.Util.show(overlappingDiv);
    }
  },

  removeOverlappingDiv: function(dialog) {
    var overlappingDiv = $(this._overlappingDivId());
    var keepOverlappingDiv = false;
    var previousDialog;

    for (var i in BS.Hider.allDivs) {
      previousDialog = $(BS.Hider.allDivs[i]);
      if (previousDialog && previousDialog.getAttribute('data-modal')) {
        keepOverlappingDiv = true;
      }
    }

    if (overlappingDiv) {
      if (!keepOverlappingDiv) {
        $('mainContent').removeChild(overlappingDiv);
      } else {
        if (previousDialog) {
          overlappingDiv.style.zIndex = "" + (parseInt(previousDialog.style.zIndex, 10) - 1);
        }
      }
    }
  },

  resetPosition: function(dialog) {
    $j(window).off('resize.modalDialog scroll.modalDialog');
    dialog.style.cssText = "";
  },

  // updates dialog iframe if needed
  updateDialog: function() {
    BS.Util.moveDialogIFrame(this.getContainer());
  }
};

BS.DialogWithProgress = OO.extend(BS.AbstractModalDialog, {
  showProgress: function(event) {
    BS.ProgressPopup.showProgress(Event.element(event), "Loading...", {shift: {x: 15, y: 20}});
  },

  hideProgress: function() {
    BS.ProgressPopup.hidePopup(0, true);
  },

  showCentered: function() {
    this.hideProgress();
    BS.AbstractModalDialog.showCentered.apply(this, arguments);
  }
});
