BS.IssueDetails = function(elementId, issueId, providerId) {
  BS.Popup.call(this, elementId, {
    issueId: issueId,
    providerId: providerId,

    shift: {x : -80},
    zIndex: 200,

    textProvider: function(popup) {
      if (!$('issueDetailsTemplate')) return "";
      var text = $('issueDetailsTemplate').innerHTML;
      text = text.replace(/##ISSUE_ID##/g, popup.options.issueId);
      setTimeout(function() { this.showDetailedInfo(); }.bind(popup), 50);
      return text;
    }
  });
};

_.extend(BS.IssueDetails.prototype, BS.Popup.prototype);

BS.IssueDetails.prototype.showDetailedInfo = function() {
  this._loaded = false;
  var issueId = this.options.issueId;
  var providerId = this.options.providerId;
  var url = window['base_uri'] + "/issueDetailsPopup.html?issueId=" + issueId + "&providerId=" + providerId;

  BS.ajaxUpdater($('detailedSummary:' + issueId), url, {
    evalScripts: true,
    onComplete: function() {
      this.updatePopup();
      this._loaded = true;
    }.bind(this)
  });
};
