if (!BS.Queue) {
  BS.Queue = OO.extend(BS.QueueLikeSorter, {
    containerId: 'queueTableRows',

    personalBuilds: [],

    getActionUrl: function(node) {
      return "queue.html?queueOrder=" + this.computeOrder(node, "queue_");
    },

    afterOrderSaving: function() {
      var container = $('buildQueueContainer');
      if (container) {
        container.refresh();
      } else {
        // We're inside a popup. Let's try to refresh it by opening again.
        var btId = BS.QueuedBuildsPopup["_currentBuildTypeId"];
        if (btId) {
          setTimeout(function() {
            BS.QueuedBuildsPopup.showQueuedBuilds($("queuedBuilds:" + btId), btId);
          }, 3100);
        }
      }
    }
  });
}

BS.Queue = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, _.extend(BS.Queue, {
  getContainer: function() {
    return $('removeFromQueueDialog');
  },

  formElement: function() {
    return $('removeFromQueue');
  },

  showRemoveDialog: function(buildTypeId) {
    this.formElement().removeFromQueue.value = buildTypeId;

    this.formElement().comment.value = this.formElement().comment.defaultValue;
    this.showCentered();
    this.formElement().comment.focus();
    this.formElement().comment.select();

    this.bindCtrlEnterHandler(this.removeFromQueue.bind(this));
  },

  removeFromQueue: function(buildTypeId) {
    if (buildTypeId) {
      this.formElement().removeFromQueue.value = buildTypeId;
    }

    if (this.formElement().comment.value == this.formElement().comment.defaultValue) {
      this.formElement().comment.value = "";
    }

    if (this.isVisible()) {
      this.setOrderSaving(true);
      this.disable();
    }
    var that = this;
    BS.ajaxRequest(this.formElement().action, {
      parameters: this.serializeParameters(),
      onComplete: function(transport) {
        if (that.isVisible()) {
          that.setOrderSaving(false);
          that.enable();
          that.close();
        }
        that.removeBuildTypeRow(that.formElement().removeFromQueue.value);
        if (BS.QueuedBuildsPopup && BS.QueuedBuildsPopup.isShown()) {
          BS.QueuedBuildsPopup.hidePopup();
        }
      }
    });

    return false;
  },

  removeBuildTypeRow: function(buildTypeId) {
    var rowId = "queue_" + buildTypeId;
    if (!$(rowId)) return;

    var parent = $(rowId).parentNode;
    parent.removeChild($(rowId));

    // remove table if it is empty
    if (parent.getElementsByTagName("div").length == 0) {
      var container = $('queueTable').parentNode;
      container.removeChild($('queueTable'));
      container.innerHTML = '<p>Build queue is empty.</p>';
    }

    this.updateMoveTopIcons();
    BS.Queue.refreshEstimates();
  },

  refreshEstimates: function() {
    BS.Queue.requestNewQueueEstimates();
  },

  updateQueueEstimatesFromData: function() {
    for(var key in BS.QueueEstimates._estimates) {
      var estimate = BS.QueueEstimates._estimates[key];
      var element = $('estimate' + key + ":text");
      if (element) {
        element.innerHTML = estimate != null ? estimate.getEstimate() : 'N/A';
      }
    }
  },

  requestNewQueueEstimates: function() {
    BS.ajaxRequest("queue.html?estimatesRequest=1", {
      onSuccess: function(transport, object) {
        transport.responseText.evalScripts();
        BS.Queue.updateQueueEstimatesFromData();
      },
      method: "get"
    });
  },

  initEstimatesAutoupdate: function() {
    var updateEstimates = function() {
      setTimeout(updateEstimates, 10 * 1000);
      BS.Queue.requestNewQueueEstimates();
    };

    updateEstimates();
  }

})));

BS.QueueFilter = {
  filter: function(agentPoolKey) {
    BS.QueueFilter._prepare();
    BS.QueueFilter._updateProperty(agentPoolKey, BS.QueueFilter._getValue($("queuePool")), {
      afterComplete: function() {
        $("buildQueueContainer").refresh();
      }
    });
  },

  resetFilter: function(agentPoolKey) {
    BS.QueueFilter._prepare();
    BS.User.deleteProperty(agentPoolKey, {
      afterComplete: function () {
        $("buildQueueContainer").refresh();
      }
    });
  },

  _prepare: function() {
    BS.Util.show("queueFilterProgress");
    BS.Util.disableInputTemp($("queuePool"));
  },

  _getValue: function(selector) {
    return selector.options[selector.selectedIndex].value;
  },

  _updateProperty: function(key, value, options) {
    if (value == "ALL") {
      BS.User.deleteProperty(key, options);
    }
    else {
      BS.User.setProperty(key, value, options);
    }
  }
};

BS.QueueEstimates = {};
BS.QueueEstimates._estimates = {};
BS.QueueEstimates.getEstimate = function(queueItemId) {
  var est = this._estimates[queueItemId];
  if(est == null)
    return null;

  return OO.extend(est, {
    getDescription: function() {
      var title = '';
      if (this.secondsToStart == null) {
        title = "Cannot estimate this build<br/>";
      }
      else if (this.secondsToStart > 0) {
        var duration = this.getDurationAsString();
        if (duration == "???") {
          duration = "";
        }
        else {
          duration = " (" + duration + ")";
        }
        title = "" +
                "Estimated start/finish: " + this.getTimeFrame() +duration+"<br/>" +
                "Planned agent: " + this.getAgentLink() + "<br/>";
      }
      else if (this.secondsToStart == 0) {
        title = "The build should start shortly.<br/>";
      }

      if(this.isDelayed) {
        var buildLink = this.getBuildLink();
        var agentLink = this.getAgentLink();
        if(buildLink != '' && agentLink != '') {
          title += "Delayed by "+ buildLink + " on "+ agentLink + "<br/>";
        }
        title += "Expected build duration: " + this.getDurationAsString() +"<br/>";
      }

      if (this.getWaitReason()) {
        title += '<em>' + this.getWaitReason() + '</em>';
      }
      return title;
    }
  });
};

$j(document).ready(function() {
  var mouseoverHandler = function(e) {
    var itemId = this.getAttribute('data-itemId');
    var estimate = BS.QueueEstimates.getEstimate(itemId);
    var description = estimate != null ? estimate.getDescription() : 'N/A';
    BS.Tooltip.showMessageAtCursor(e, {shift:{x:2, y:2}}, description);
  };

  var mouseoutHandler = function() {
    BS.Tooltip.hidePopup();
  };

  var selectBuildHandler = function() {
    var selectedBuilds = BS.Util.getSelectedValues('buildQueueForm', 'removeItem');

    if (selectedBuilds.length > 0) {
      BS.blockRefreshPermanently();
    } else {
      BS.unblockRefresh();
    }
  };

  // TW-19878
  var fixDragHandler = function() {
    var draggedRow = $j(this).closest('.queueRowDragged');

    if (draggedRow.length > 0) {
      draggedRow.removeClass('queueRowDragged');
      return false;
    }
  };

  $j(document)
      .on('mouseover', '#buildQueueContainer td.remaining span.remaining, #queuedBuildsPopup td.remaining span.remaining', mouseoverHandler)
      .on('mouseout', '#buildQueueContainer td.remaining span.remaining, #queuedBuildsPopup td.remaining span.remaining', mouseoutHandler)
      .on('click', '#buildQueueContainer #removeItem, #buildQueueContainer #removeAll, #queuedBuildsPopup #removeItem', selectBuildHandler)
      .on('click', '#buildQueueContainer #queueTableRows a, #queuedBuildsPopup #queueTableRows a', fixDragHandler);
});
