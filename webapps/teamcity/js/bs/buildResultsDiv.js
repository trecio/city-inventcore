
BS.BuildResults = {
  _compilerErrors: [],
  _compilerWarnings: [],
  _expanded_count: 0,

  expandStacktrace: function(element, buildId, testId, onCompleteFun) {
    element = $(element);
    if (!element.firstChild) {
      BS.Log.info("Loading stacktrace for build " + buildId + " testId " + testId);

      new BS.ajaxUpdater(element, "failedTestText.html?buildId=" + buildId + "&testId=" + testId, {
        onComplete: function() {
          BS.BuildResults.doExpand(element);
          if (onCompleteFun) { onCompleteFun(); }
        }
      });
    } else {
      this.doExpand(element);
    }
  },

  doExpand: function(element) {
    if (element.innerHTML == "") {
      element.innerHTML = "No stacktrace available"
    }
  },

  stacktraceShown: function() {
    this._expanded_count++;
    BS.blockRefreshPermanently('stacktrace');
    if ($('noRefreshWarning'))
      $('noRefreshWarning').show();
  },

  stacktraceHidden: function() {
    if (this._expanded_count > 0) {
      this._expanded_count--;
    }
    if (this._expanded_count == 0) {
      BS.unblockRefresh('stacktrace');
      if ($('noRefreshWarning'))
        $('noRefreshWarning').hide();
    }
  },

  showCompilerWarnings: function(buildId) {
    BS.Util.show('showCompilerMessagesProgress');
    new BS.ajaxUpdater($('compileErrorsData'), "compilerOutput.html?buildId=" + buildId);
  },

  installBuildDetailsPopup: function(buildId) {
    var popupPrefix = "sp_span_",
        popupId = "buildDetails";

    var popupArrowIcon = $j("<img/>").attr({
      id: popupId,
      src: window['base_uri'] + '/img/popUpControl.gif'
    }).addClass("toggle");
    var popupSpan = $j("<span/>").attr({
      id: popupPrefix + popupId
    }).addClass("pc");
    $j("#main_navigation").children('.last').append(popupArrowIcon).wrapInner(popupSpan);

    BS.Util.runWithElement(popupPrefix + popupId, function() {
      BS.install_simple_popup(popupPrefix + popupId, {
        url: window['base_uri'] + '/buildDetails.html?buildId=' + buildId,
        shift: {y: 20},
        afterShowFunc: function() {
          $j("#sp_span_buildDetailsContent").addClass("statusBlockContainer").css("margin-left", $j("#main_navigation").offset().left + "px");
        }
      });
    });
  },

  minorUpdate: function() {
    var newIcon = $j("#buildDataIcon");
    $j("#main_navigation img.icon").attr("src", newIcon.attr("src"));
    newIcon.remove();
  }
};

BS.BuildArtifacts = {
  updateDetails: function(url, sizeId, linkId, noteId) {
    BS.ajaxRequest(url, {
      method : "get",
      onComplete: function(transport) {
        var data = transport.responseText.evalJSON();
        $(sizeId).innerHTML = data.size;
        if (data.showLink) {
          BS.Util.show(linkId);
        }
        if (data.hasHidden) {
          BS.Util.show(noteId);
        }
      }
    });
  }
};

