BS.EditCheckoutRulesForm = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {

  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('saveRulesProgress');
    } else {
      BS.Util.hide('saveRulesProgress');
    }
  },

  getContainer: function() {
    return $('editCheckoutRulesFormDialog');
  },

  formElement: function() {
    return $('editCheckoutRulesForm');
  },

  showDialog: function(vcsRootId, vcsRootName, checkoutRules, readOnly) {
    $('editCheckoutRulesFormTitle').innerHTML = (readOnly ? "View" : "Edit") + " Checkout Rules";
    if (readOnly) {
      BS.Util.hide('editCheckoutRulesFormSaveBlock');
      $('checkoutRules').readOnly = "readonly";
    } else {
      BS.Util.show('editCheckoutRulesFormSaveBlock');
      $('checkoutRules').readOnly = "";
    }

    this.formElement().vcsRootId.value = vcsRootId;
    this.formElement().checkoutRules.value = checkoutRules;
    $('vcsRootName').innerHTML = vcsRootName.replace(/\//g, "/<wbr>");
    this.showCentered();
    this.formElement().checkoutRules.focus();
    this.bindCtrlEnterHandler(this.submit.bind(this));
  },

  cancelDialog: function() {
    this.close();
  },

  afterClose: function() {
    this.clearErrors();
  },

  submit: function() {
    var that = this;

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onInvalidCheckoutRuleError: function(elem) {
        $('errorCheckoutRules').innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($('checkoutRules'));
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          that.enable();
          that.close();
          BS.EditVcsRootsForm.update();
        }
      }
    }));

    return false;
  }
}));
