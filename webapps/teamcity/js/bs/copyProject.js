
BS.CopyProjectForm = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {

  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('copyProjectProgress');
    } else {
      BS.Util.hide('copyProjectProgress');
    }
  },

  getContainer: function() {
    return $('copyProjectFormDialog');
  },

  formElement: function() {
    return $('copyProjectForm');
  },

  showDialog: function(sourceProjectId, newName, hasTemplates) {
    BS.AdminActions.checkProjectHasLocalVcsRoots(sourceProjectId, function(hasLocalVcsRoots) {
      if (hasLocalVcsRoots) {
        BS.Util.show('projectVcsRootsSection');
      } else {
        BS.Util.hide('projectVcsRootsSection');
      }

      BS.CopyProjectForm.showCentered();
      BS.CopyProjectForm.formElement().newName.value = newName;
      BS.CopyProjectForm.formElement().projectId.value = sourceProjectId;
      BS.CopyProjectForm.formElement().newName.focus();
      BS.CopyProjectForm.bindCtrlEnterHandler(BS.CopyProjectForm.submitCopy.bind(BS.CopyProjectForm));

      var projectTemplatesSection = $("projectTemplatesSection");
      if (hasTemplates) {
        projectTemplatesSection.show();
      } else {
        projectTemplatesSection.hide();
      }
    });
  },

  cancelDialog: function() {
    this.close();
  },

  afterClose: function() {
    this.clearErrors();
  },

  submitCopy: function() {
    var that = this;

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onSaveProjectErrorError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onMaxNumberOfBuildTypesReachedError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onProjectNotFoundError: function(elem) {
        alert(elem.firstChild.nodeValue);
        BS.XMLResponse.processRedirect(elem.ownerDocument);
      },

      onEmptyProjectNameError: function(elem) {
        $('error_newProjectName').innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField(that.formElement().newName);
      },

      onInvalidProjectNameError: function(elem) {
        $('error_newProjectName').innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField(that.formElement().newName);
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          BS.XMLResponse.processRedirect(responseXML);
        }
      }
    }));

    return false;
  }
}));


BS.CopyBuildTypeForm = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {

  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('copyBuildTypeProgress');
    } else {
      BS.Util.hide('copyBuildTypeProgress');
    }
  },

  getContainer: function() {
    return $('copyBuildTypeFormDialog');
  },

  formElement: function() {
    return $('copyBuildTypeForm');
  },

  showDialog: function(sourceBuildTypeId, sourceProjectId, newName, targetProjectId) {
    BS.AdminActions.checkBuildTypeHasLocalVcsRoots(sourceBuildTypeId, function(hasLocalVcsRoots) {
      BS.CopyBuildTypeForm.formElement().setShowVcsRootsSection(hasLocalVcsRoots);
      BS.CopyBuildTypeForm.formElement().buildTypeId.value = sourceBuildTypeId;
      BS.CopyBuildTypeForm.formElement().sourceProjectId.value = sourceProjectId;
      BS.CopyBuildTypeForm.formElement().newName.value = newName;
      BS.CopyBuildTypeForm.formElement().projectId.selectProject(targetProjectId);

      BS.CopyBuildTypeForm.showCentered();
      BS.CopyBuildTypeForm.formElement().projectId.onchange();
      BS.CopyBuildTypeForm.formElement().projectId.focus();
      BS.CopyBuildTypeForm.bindCtrlEnterHandler(BS.CopyBuildTypeForm.submitCopy.bind(BS.CopyBuildTypeForm));
    });
  },

  cancelDialog: function() {
    this.close();
  },

  afterClose: function() {
    this.clearErrors();
  },

  submitCopy: function() {
    var that = this;

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onSaveProjectErrorError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onMaxNumberOfBuildTypesReachedError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onProjectNotFoundError: function(elem) {
        $('error_projectId').innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField(that.formElement().projectId);
      },

      onEmptyBuildTypeNameError: function(elem) {
        $('error_newName').innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField(that.formElement().newName);
      },

      onBuildTypeNotFoundError: function(elem) {
        alert(elem.firstChild.nodeValue);
        BS.XMLResponse.processRedirect(elem.ownerDocument);
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          BS.XMLResponse.processRedirect(responseXML);
        }
      }
    }));

    return false;
  }
}));

