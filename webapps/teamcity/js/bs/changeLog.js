/*
 * Taken from buildType.js to be reused in projectChangeLog.jsp
 */
BS.ChangeLog = OO.extend(BS.AbstractWebForm, {

  graph: null,
  graphContainer: null,

  formElement: function() {
    return $('changeLogFilter');
  },

  sanitizeFilter: function() {
    var fromBuild = this.formElement().from;
    var toBuild = this.formElement().to;

    if (fromBuild) {
      fromBuild.value = fromBuild.value.replace(/^#/, '');
    }

    if (toBuild) {
      toBuild.value = toBuild.value.replace(/^#/, '');
    }
  },

  submitFilter: function(trigger) {
    // Hide visible sections without a full refresh
    if (trigger && trigger.type == 'checkbox' && !trigger.checked) {
      this.hideSections(trigger.name);
      this._saveFilterSettingsInSession();
      this.redrawGraph();
      return false;
    }

    var that = this;
    this.setSaving(true);
    this.sanitizeFilter();
    this.disable();
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SimpleListener, {
      onCompleteSave: function() {
        $('changeLogTable').refresh(that.savingIndicator(), null, function() {
          that.enable(function (element) {
            //enable 'showGraph' checkbox only if path is empty
            return (element.id !== 'showGraph') || jQuery('#path').val() === '';
          });
          that.setSaving(false);
          that.redrawGraph();
        });
      }
    }));

    return false;
  },

  initChangeLogTable: function() {
      var changesTable = $j('#changesTable'),
        artifactDescriptionCaptions = changesTable.find('.artifactDescriptionCaption');

    artifactDescriptionCaptions.click(function() {
      var caption = $j(this);

      caption.toggleClass('expanded').toggleClass('collapsed');
      caption.parent('tr').nextUntil(":not(.artifacts-row)").toggleClass('hidden');

      BS.ChangeLog.redrawGraph();
    });

    artifactDescriptionCaptions.mousedown(function() {
      return false;
    });
  },

  hideSections: function(sectionName) {
    var changesTable = $j('#changesTable');

    if (sectionName == 'showBuilds') {
      changesTable.find('.modification-row-header').remove();
    }

    if (sectionName == 'showFiles') {
      changesTable.find('.files-row').remove();
    }
  },

  graphTooltipInstance: function() {
    return this._graphTooltipInstance = this._graphTooltipInstance || $j('<div class="graph-tooltip graph-tooltip-s graph-tooltip-hidden">' +
                                                                         '<div class="graph-tooltip-arrow graph-tooltip-arrow-s"></div>' +
                                                                         '<div class="graph-tooltip-inner"><span class="mono mono-12px"></span></div>' +
                                                                         '</div>');
  },

  toggleGraph: function() {
    var graph = $j('#graph'),
        graphToggle = $j('#showGraph');

    this.graphContainer = graph;

    if (graphToggle.attr('checked')) {
      if (this.isEmptyGraph()) {
        this.submitFilter();
      } else {
        this._saveFilterSettingsInSession();
        graph.show();
        graph.parent().show();
      }
    } else {
      this._saveFilterSettingsInSession();
      graph.hide();
      graph.parent().hide();
    }
  },

  _saveFilterSettingsInSession: function() {
    //post request just saves filter settings and doesn't do any calculations, it should be fast
    BS.FormSaver.save(this, this.formElement().action, BS.SimpleListener);
  },

  showNextBigPage: function(url) {
    var params = {
      __fragmentId: "changeLogTableInner",
      addToSamePage: "true"
    };

    BS.Util.show("progress");
    var that = this;

    BS.ajaxRequest(url, {
      method: "get",
      parameters: params,
      onComplete: function(transport) {
        BS.Util.hide("progress");

        var response = transport.responseText;
        that.insertNewRows(response);
        that.updateNumbersOnPage(response);
      }
    });
  },

  insertNewRows: function(response) {
    if (response.indexOf("<table") >= 0) {
      var changesTable = $j("#changesTable");
      if (changesTable.length == 0) {
        // Insert the whole table.
        var table = $j(response).find("#changesTable");
        $j("#changeLogTable .resultsTitle:first").after(table);
      } else {
        // Insert the rows only.
        $j(response).find('#changesTable tbody tr').each(function(index, tr) {
          changesTable.append(tr);
        });
      }
    }
  },

  updateNumbersOnPage: function(response) {
    var fromIdx, toIdx, num;

    // Changes found.
    fromIdx = response.indexOf('<strong id="changes-num-found">') + 31;
    toIdx = response.indexOf('</strong>', fromIdx);
    num = parseInt(response.substring(fromIdx, toIdx)) + parseInt(jQuery("#changes-num-found").text());
    $j("#changes-num-found").text(num);

    fromIdx = response.indexOf('<strong class="changes-total">');
    if (fromIdx == -1) {
      BS.Util.hide("search-more");
      return;
    }

    fromIdx += 30;
    toIdx = response.indexOf('</strong>', fromIdx);
    num = response.substring(fromIdx, toIdx);
    $j(".changes-total").text(num);
  },

  overlappingLineShift: 3,
  commitRadius: 4,
  commitDiameter: 8,
  vOffsetFromRowTop: 10,
  hOffsetFromRowLeft: 10,
  spaceBetweenColumns: 10,
  spaceBetweenPositions: 15,

  selectedVertex: null,
  selectedVertexType: null,
  selectedVertexNode: null,
  selectedVertexPaths: [],
  selectedColumn: null,
  selectedColumnNode: null,

  // columnId -> its vertices' nodes
  columnVerticesNodes: {},

  initGraph: _.once(function() {
    $j(window).resize(function() {
      BS.ChangeLog.redrawGraph();
    });

    BS.LoadStyleSheetDynamically(window['base_uri'] + '/css/changeLog.css', function() {
      BS.ChangeLog.redrawGraph();
    });
  }),

  isEmptyGraph: function() {
    return this.graph == null || this.graph.columns.length == 0;
  },

  redrawGraph: function() {
    this.columnVerticesNodes = {};

    var graphToggle = $j('#showGraph'),
        graphContainer = $j('#graph');

    this.graphContainer = graphContainer;

    graphContainer.empty().css('width', 0);

    if (this.isEmptyGraph()) {
      graphContainer.parent('.graphTd').css('display', 'none');
      return;
    }

    if (graphToggle.attr('checked') && !this.isEmptyGraph()) {
      this.drawGraph(this.graph);
      if (BS.ChangeLog.selectedVertexNode) {
        $j(BS.Util.escapeId(BS.ChangeLog.selectedVertexNode.id)).click();
      }
      if (BS.ChangeLog.selectedColumnNode) {
        $j(BS.Util.escapeId(BS.ChangeLog.selectedColumnNode.id)).click();
      }
    }
  },

  drawGraph: function(graph) {
    this.initGraphGeometry(graph);
    var paper = Raphael('graph', graph.width, graph.height);
    for (var i = 0; i < graph.columns.length; i++) {
      this.drawColumn(paper, graph.columns[i]);
    }
  },

  initGraphGeometry: function(graph) {
    this.initGraphWidth(graph);
    this.graphContainer.width(graph.width - 5 + 'px').css({position: 'relative', 'left': '-10px'});
    this.initGraphHeight(graph);
    this.initCommitOffsets(graph);
  },

  initGraphWidth: function(graph) {
    var totalWidth = 0;
    for (var i = 0; i < graph.columns.length; i++) {
      var width = (graph.columns[i].maxWidth) * this.spaceBetweenPositions + 2 * this.hOffsetFromRowLeft;
      graph.columns[i].offsetX = totalWidth + this.spaceBetweenColumns;
      graph.columns[i].width = width;
      totalWidth = totalWidth + width + this.spaceBetweenColumns;
    }
    graph.width = totalWidth + 1;
  },

  initGraphHeight: function(graph) {
    var height = $j('#changesTable').height();
    for (var i = 0; i < graph.columns.length; i++) {
      graph.columns[i].height = height;
    }
    graph.height = height;
  },

  initCommitOffsets: function(graph) {
    var tableOffset = $j('#changesTable').offset().top;
    for (var i = 0; i < graph.columns.length; i++) {
      var column = graph.columns[i];
      for (var j = 0, len = column.vertices.length; j < len; j++) {
        var commit = column.vertices[j];
        commit.offsetY = this.getVertexRow(commit).offset().top - tableOffset;
      }
    }
  },

  drawColumn: function(paper, column) {
    column.horizontalParts = [];
    this.drawColumnRect(paper, column);
    this.drawColumnCommits(paper, column);
    this.drawLinesFromPreviousPage(paper, column);
  },

  drawColumnRect: function(paper, column) {
    var rect = paper.rect(column.offsetX, 0, column.width, column.height, 4);
    rect.attr({title: column.name,
               fill: '#E9F3FD',
               stroke: '#DFDFDF',
               cursor: 'pointer'});

    var $rectNode = jQuery(rect.node);

    $rectNode.attr('id', 'column' + column.id).click(this.makeColumnClickHandler(paper, column));

    this.initColumnTooltip($rectNode, column);
  },

  // We want to avoid showing the column tooltip right after hiding the commit tooltip
  blockColumnTooltip: false,
  blockColumnTooltipTimeout: null,

  initColumnTooltip: function($rectNode, column) {
    var that = this;
    $rectNode.mouseenter(function() {
      if (BS.ChangeLog.blockColumnTooltip) return;

      var offset = $rectNode.offset();
      var tooltip = that.graphTooltipInstance();

      // Cleanup
      tooltip.addClass('graph-tooltip-hidden').removeClass('graph-tooltip-commit');

      // Create tooltip
      $rectNode.removeAttr('title');
      $rectNode.parent().removeAttr('title');
      tooltip.attr('id', 'tooltip-column-' + column.id);
      tooltip.find('.graph-tooltip-inner .mono').html(column.name);
      tooltip.appendTo(document.body);
      tooltip.css({left: offset.left - 6 + 'px', top: offset.top - tooltip.height() - 12 + 'px'});
      tooltip.addClass('graph-tooltip-column').removeClass('graph-tooltip-hidden');
    });

    $rectNode.mouseleave(function() {
      $j('#tooltip-column-' + column.id).remove();
    });
  },

  initCommitTooltip: function($vertexNode, column, vertex) {
    var that = this;
    $vertexNode.mouseenter(_.throttle(function() {
      var title = $vertexNode.attr('title');
      var position = $vertexNode.offset();
      var horizontalOffset = vertex.type === 'build' ? 12 : 11;
      var verticalOffset = vertex.type === 'build' ? 12 : 14;
      var tooltip = that.graphTooltipInstance();

      // Cleanup
      tooltip.addClass('graph-tooltip-hidden').removeClass('graph-tooltip-column');

      // Create tooltip
      $vertexNode.removeAttr('title');
      $vertexNode.parent().removeAttr('title');
      tooltip.attr('id', 'tooltip-vertex-' + vertex.id);
      tooltip.find('.graph-tooltip-inner .mono').html(vertex.description);
      tooltip.appendTo(document.body);
      tooltip.css({left: position.left - horizontalOffset + 'px', top: position.top - tooltip.height() - verticalOffset + 'px'});
      tooltip.addClass('graph-tooltip-commit').removeClass('graph-tooltip-hidden');
    }, 500));

    $vertexNode.mouseleave(_.throttle(function() {
      $j('#tooltip-vertex-' + vertex.id).remove();

      BS.ChangeLog.blockColumnTooltip = true;

      window.clearTimeout(BS.ChangeLog.blockColumnTooltipTimeout);

      BS.ChangeLog.blockColumnTooltipTimeout = window.setTimeout(function() {
        BS.ChangeLog.blockColumnTooltip = false;
      }, 1000);
    }, 500));
  },

  drawColumnCommits: function(paper, column) {
    var drawnCommitIds = [];
    var drawnLinesCommitIds = [];
    var commit, commitIndex;
    for (var i = 0, len = column.startVertices.length; i < len; i++) {
      commitIndex = column.startVertices[i];
      commit = column.vertices[commitIndex];
      this.drawCommitAndItsAncestors(paper, column, commit, commitIndex, drawnCommitIds, drawnLinesCommitIds);
    }

    for (i = 0, len = column.vertices.length; i < len; i++) {
      commitIndex = i;
      commit = column.vertices[commitIndex];
      this.drawCommitAndItsAncestors(paper, column, commit, commitIndex, drawnCommitIds, drawnLinesCommitIds);
    }
  },

  drawCommitAndItsAncestors: function(paper, column, commit, commitIndex, drawnCommitIds, drawnLinesCommitIds) {
    var that = this;
    if (!drawnCommitIds[commit.id]) {
      this.depthFirstSearch(column, commit, commitIndex,
      function(commit, commitIndex) {
        if (!drawnCommitIds[commit.id]) {
          that.drawVertex(paper, column, commit, commitIndex);
          drawnCommitIds[commit.id] = 1;
          return true;
        } else {
          return false;
        }
      },
      function(commit, commitIndex) {
        if (!drawnLinesCommitIds[commit.id]) {
          that.drawCommitLines(paper, column, commit, commitIndex);
          drawnLinesCommitIds[commit.id] = 1;
        }
      });
    }
  },

  drawLinesFromPreviousPage: function(paper, column) {
    for (var i = 0; i < column.lines.length; i++) {
      this.drawLineFromPreviousPage(paper, column, column.lines[i]);
    }
  },

  drawLineFromPreviousPage: function(paper, column, line) {
    var pathOffsets = this.getLineSegmentOffsets(line, column.vertices, column, true);
    var path = paper.path(this.getPathSpec(column, pathOffsets));
    path.attr({stroke: '#003366'});
  },

  drawVertex: function(paper, column, vertex, vertexIndex) {
    var vertexRaphaelObj;
    if (vertex.type === 'commit') {
      vertexRaphaelObj = this.drawCommit(paper, column, vertex);
    } else {
      vertexRaphaelObj = this.drawBuild(paper, column, vertex);
    }

    if (!this.columnVerticesNodes[column.id]) {
      this.columnVerticesNodes[column.id] = [];
    }
    if (this.columnVerticesNodes[column.id].indexOf(vertexRaphaelObj.node) == -1) {
      this.columnVerticesNodes[column.id].push(vertexRaphaelObj.node);
    }

    var $vertexNode = jQuery(vertexRaphaelObj.node);
    $vertexNode.attr('id', vertex.type + vertex.id);
    $vertexNode.click(BS.ChangeLog.makeVertexClickHandler(column, vertex, vertexIndex));
    // Visual feedback on hover
    $vertexNode.mouseenter(this.getVertexMouseEnterHandler(vertex, vertexRaphaelObj));
    $vertexNode.mouseleave(this.getVertexMouseLeaveHandler(vertex, vertexRaphaelObj));
    // Commit node tooltip
    this.initCommitTooltip($vertexNode, column, vertex);
  },

  drawCommit: function(paper, column, commit) {
    var commitCenterX = this.getOffsetX(column, commit.position),
        commitCenterY = commit.offsetY + BS.ChangeLog.vOffsetFromRowTop + BS.ChangeLog.commitRadius;

    var commitCircle = paper.circle(commitCenterX, commitCenterY, BS.ChangeLog.commitRadius);
    commitCircle.attr({fill: '#003366',
                       stroke: '#003366',
                       cursor: 'pointer'});
    commitCircle.node.setAttribute('data-type', 'commit');
    return commitCircle;
  },

  drawBuild: function(paper, column, vertex) {
    var commitCenterX = this.getOffsetX(column, vertex.position),
        commitCenterY = vertex.offsetY + BS.ChangeLog.vOffsetFromRowTop + BS.ChangeLog.commitRadius;

    var color = vertex.status === 'success' ? '#71D458' : '#F23535';
    var buildCircle = paper.circle(commitCenterX, commitCenterY, BS.ChangeLog.commitRadius - 1);
    buildCircle.attr({fill: color,
                        stroke: color,
                        cursor: 'pointer'});
    buildCircle.node.setAttribute('data-type', 'build');
    buildCircle.node.setAttribute('data-status', vertex.status);

    return buildCircle;
  },

  getVertexMouseEnterHandler: function(vertex, vertexRaphaelObj) {
    if (vertex.type === 'commit') {
      return _.throttle(function() {
        if (BS.ChangeLog.selectedVertexNode != vertexRaphaelObj.node) {
          vertexRaphaelObj.animate({r: BS.ChangeLog.commitRadius * 1.3}, 250);
        }
      }, 500);
    } else {
      //do nothing yet
    }
  },

  getVertexMouseLeaveHandler: function(vertex, vertexRaphaelObj) {
    if (vertex.type === 'commit') {
      return _.throttle(function() {
        if (BS.ChangeLog.selectedVertexNode != vertexRaphaelObj.node) {
          vertexRaphaelObj.animate({r: BS.ChangeLog.commitRadius}, 250);
        }
      }, 500);
    } else {
      //do nothing yet
    }
  },

  drawCommitLines: function(paper, column, commit, commitIndex) {
    commit.paths = [];
    for (var i = 0; i < commit.lines.length; i++) {
      this.drawCommitLine(paper, column, commit, commit.lines[i], commitIndex);
    }
  },

  drawCommitLine: function(paper, column, commit, line, commitIndex) {
    var subsequentCommits = column.vertices.slice(commitIndex);
    var pathOffsets = this.getLineSegmentOffsets(line, subsequentCommits, column);
    var path = paper.path(this.getPathSpec(column, pathOffsets));
    path.attr({stroke: '#003366'});
    commit.paths.push(path);
  },

  getLineSegmentOffsets: function(line, commitsInColumn, column, startFromTheTop) {
    var i = 0, j,
        len = line.length,
        position,
        lastX,
        y,
        result = [];

    if (startFromTheTop) {
      position = line[0];
      result.push({x: this.getOffsetX(column, position),
                   y: 0});
      i = 1;
    }
    for (j = 0; i < len; i++, j++) {
      position = line[i];
      if (commitsInColumn[j]) {
        if (i == 0) {
          y = commitsInColumn[j].offsetY + BS.ChangeLog.vOffsetFromRowTop + BS.ChangeLog.commitDiameter;
        } else if (i == len - 1) {
          y = commitsInColumn[j].offsetY + BS.ChangeLog.vOffsetFromRowTop;
        } else {
          y = commitsInColumn[j].offsetY + BS.ChangeLog.vOffsetFromRowTop + BS.ChangeLog.commitDiameter;
        }
        result.push({x: this.getOffsetX(column, position),
                     y: y});
      }
    }
    if (commitsInColumn.length < len) {
      lastX = result && result[result.length - 1] ? result[result.length - 1].x : 0;
      result.push({x: lastX,
                   y: column.height});
    }
    return result;
  },


  getOffsetX: function(column, position) {
    return column.offsetX + BS.ChangeLog.hOffsetFromRowLeft + position * BS.ChangeLog.spaceBetweenPositions;
  },

  getPathSpec: function(column, points) {
    var i, j,
        len = points.length,
        point,
        x, y,
        prevX = null, prevY, prevPrevY,
        horizontals,
        prevSpecSegment,
        pathSpec = [],
        bendHeight,
        bendOffsetX,
        bendOffsetY;
    for (i = 0; i < len; i++) {
      bendHeight = 16;
      bendOffsetX = 2;
      bendOffsetY = 8;
      point = points[i];
      x = point.x;
      y = point.y;
      horizontals = [];
      if (prevX !== null) {
        if (x === prevX) {
          pathSpec.push("L" + (x + ", " + y));
        } else if (x > prevX) {
          pathSpec.push("C" + (prevX + ", " + (prevY + bendOffsetY)) +
                        " " + ((x - bendOffsetX) + ", " + (prevY + bendHeight - bendOffsetY)) +
                        " " + (x + ", " + (prevY + bendHeight)) +
                        "L" + (x + ", " + y));
        } else {
          if ((prevX - x) / BS.ChangeLog.spaceBetweenColumns > 5) {
            bendOffsetY = 15;
            bendOffsetX = 10;
          }
          pathSpec.push("C" + (prevX + ", " + (prevY + bendOffsetY)) +
                        " " + (x + ", " + (prevY + bendHeight - bendOffsetY)) +
                        " " + (x + ", " + (prevY + bendHeight)) +
                        "L" + (x + ", " + y));
        }
      } else {
        pathSpec.push("M" + (x + ", " + y));
      }
      prevPrevY = prevY;
      prevX = x;
      prevY = y;
    }
    return pathSpec.join('');
  },


  getVertexRow: function(vertex) {
    return $j(BS.Util.escapeId(this.getVertexRowId(vertex)));
  },


  getVertexRowId: function(vertex) {
    if (vertex.type === 'commit') {
      return 'tr-mod-' + vertex.id;
    } else {
      return 'tr-build-' + vertex.id.substring(0, vertex.id.indexOf('_'));
    }
  },


  getVertexRows: function() {
    return $j('#changesTable').find('tr').filter('[id|=tr-mod], [id|=tr-build]');
  },


  makeColumnClickHandler: function(paper, column) {
    return function() {
      //this is bound to the column rect node
      var oldSelectedColumn = BS.ChangeLog.selectedColumnNode;
      BS.ChangeLog.restoreDefaultGraphView();
      if (this !== oldSelectedColumn) {
        BS.ChangeLog.selectedColumnNode = this;
        BS.ChangeLog.selectedColumn = column;
        BS.ChangeLog.highlightSelectedColumn();
        BS.ChangeLog.highlightSelectedColumnModifications();
      }
    };
  },


  highlightSelectedColumn: function() {
    if (BS.ChangeLog.selectedColumn) {
      for (var i = 0; i < this.graph.columns.length; i++) {
        var column = this.graph.columns[i];
        if (column != BS.ChangeLog.selectedColumn) {
          $j('#column' + column.id).attr('opacity', 0.5);
          this.fadeVertices(column.id);
        } else {
          this.resetFadedVertices(column.id);
        }
      }
      BS.ChangeLog.selectedColumnNode.setAttribute('stroke-width', 2);
      BS.ChangeLog.selectedColumnNode.setAttribute('stroke', '#BFCFE2');
    }
  },


  resetSelectedColumn: function() {
    for (var i = 0; i < this.graph.columns.length; i++) {
      var column = this.graph.columns[i];
      $j('#column' + column.id).attr('opacity', 1);
    }

    if (BS.ChangeLog.selectedColumnNode) {
      BS.ChangeLog.selectedColumnNode.setAttribute('stroke-width', 1);
      BS.ChangeLog.selectedColumnNode.setAttribute('stroke', '#DFDFDF');
      BS.ChangeLog.selectedColumnNode = null;
      BS.ChangeLog.selectedVertexType = null;
    }

    this.resetFadedVertices();
  },


  highlightSelectedColumnModifications: function() {
    if (this.selectedColumn) {
      var rowIds = [];
      for (var i = 0, len = this.selectedColumn.vertices.length; i < len; i++) {
        var commit = this.selectedColumn.vertices[i];
        rowIds[this.getVertexRowId(commit)] = 1;
      }
      this.highlightModificationRows(rowIds);
    }
  },


  highlightModificationRows: function(rowIds) {
    this.getVertexRows().each(function(index, tr) {
      if (!rowIds[tr.id]) {
        $j(tr).css("opacity", 0.25);
      }
    });
  },


  resetHighlightedModificationRows: function() {
    this.getVertexRows().each(function(index, tr) {
      $j(tr).css("opacity", 1);
    });
  },


  fadeVertices: function(columnId) {
    var vertexNode,
        i;

    if (this.columnVerticesNodes[columnId]) {
      for (i=0; i < this.columnVerticesNodes[columnId].length; i++) {
        vertexNode = this.columnVerticesNodes[columnId][i];
        this.setVertexStyle(vertexNode, 'faded');
      }
    }
  },


  resetFadedVertices: function(columnId) {
    var vertexNode,
        i;

    if (columnId) {
      if (this.columnVerticesNodes[columnId]) {
        for (i=0; i < this.columnVerticesNodes[columnId].length; i++) {
          vertexNode = this.columnVerticesNodes[columnId][i];
          this.setVertexStyle(vertexNode, 'normal');
        }
      }
    } else {
      for (var column in this.columnVerticesNodes) {
        if (this.columnVerticesNodes[column]) {
          for (i=0; i<this.columnVerticesNodes[column].length; i++) {
            vertexNode = this.columnVerticesNodes[column][i];
            this.setVertexStyle(vertexNode, 'normal');
          }
        }
      }
    }
  },


  setVertexStyle: function(vertexNode, style) {
    var vertexType = vertexNode.getAttribute('data-type');
    var vertexColor;

    if (vertexType == 'commit') {
      if (style == 'faded') {
        vertexColor = '#7F99B3';
      } else if (style == 'normal') {
        vertexColor = '#003366';
      }
    } else if (vertexType == 'build') {
      if (vertexNode.getAttribute('data-status') === 'success') {
        vertexColor = '#71D458';
      } else {
        vertexColor = '#F23535';
      }
    }

    vertexNode.setAttribute('fill', vertexColor);
    vertexNode.setAttribute('stroke', vertexColor);
  },


  makeVertexClickHandler: function(column, vertex, vertexIndex) {
    return function () {
      var oldSelectedVertexNode = BS.ChangeLog.selectedVertexNode;
      BS.ChangeLog.restoreDefaultGraphView();
      if (this !== oldSelectedVertexNode) {//'this' is bound to the vertex node
        BS.ChangeLog.selectedVertexPaths = [];

        var rowIds = [];
        BS.ChangeLog.depthFirstSearch(column, vertex, vertexIndex, function(commit) {
          rowIds[BS.ChangeLog.getVertexRowId(commit)] = 1;
          for (var i = 0, len = commit.paths.length; i < len; i++) {
            BS.ChangeLog.selectedVertexPaths.push(commit.paths[i]);
          }
          return true;
        });
        BS.ChangeLog.selectedVertex = vertex;
        BS.ChangeLog.selectedVertexType = vertex.type;
        BS.ChangeLog.selectedVertexNode = this;
        BS.ChangeLog.highlightSelectedCommits();
        BS.ChangeLog.highlightModificationRows(rowIds);
      }
    };
  },

  depthFirstSearch: function(column, commit, commitIndex, discoverCommitCallback, finishProcessingCommitCallback) {
    var stack = [commit],
        commitIndices = [],
        i, len,
        white = 0, grey = 1, black = 2, colors = [];

    commitIndices[commit.id] = commitIndex;
    var getColor = function (commitModId) {
      var color = colors[commitModId];
      if (!color) {
        color = white;
        setColor(commitModId, color);
      }
      return color;
    };
    var setColor = function(commitModId, color) {
      colors[commitModId] = color;
    };
    var finishCallback = finishProcessingCommitCallback || function(c, index) {};

    while (stack.length > 0) {
      var c = stack[stack.length - 1];
      var commitModId = c.id;
      var cIndex = commitIndices[commitModId];
      var color = getColor(commitModId);
      if (color == white) {
        setColor(commitModId, grey);
        if (discoverCommitCallback(c, commitIndices[commitModId])) {
          for (len = c.parents.length, i = len - 1; i >= 0; i--) {
            var parentIndex = cIndex + c.parents[i];
            var parent = column.vertices[parentIndex];
            if (parent) {
              var parentModId = parent.id;
              var parentColor = getColor(parentModId);
              if (parentColor == white) {
                stack.push(parent);
                commitIndices[parentModId] = parentIndex;
              } else if (parentColor == grey) {
                BS.Log.error('Found cicle in the graph');
              }
            }
          }
        }
      } else if (color == grey) {
        stack.pop();
        setColor(commitModId, black);
        finishCallback(c, cIndex);
      } else {
        stack.pop();
      }
    }
  },

  restoreDefaultGraphView: function() {
    this.resetSelectedColumn();
    this.resetHighlightedModificationRows();
    if (BS.ChangeLog.selectedVertexNode) {
      this.restoreUnselectedVertex();
      this.getVertexRow(BS.ChangeLog.selectedVertex).removeClass('selectedCommitRow');
    }
    $j("path", this.graphContainer).each(function() {
      this.setAttribute('stroke-opacity', 1);
      this.setAttribute('stroke-width', 1);
    });
    BS.ChangeLog.selectedVertex = null;
    BS.ChangeLog.selectedVertexNode = null;
    BS.ChangeLog.selectedVertexPaths = [];
  },


  highlightSelectedCommits: function() {
    if (BS.ChangeLog.selectedVertexNode) {
      $j("path:not([id^='build']", this.graphContainer).each(function() {
        this.setAttribute('stroke-opacity', 0.5);
      });
      BS.ChangeLog.getVertexRow(BS.ChangeLog.selectedVertex).addClass('selectedCommitRow');
      this.highlightSelectedVertex();
      var paths = BS.ChangeLog.selectedVertexPaths;
      for (var i = 0, len = paths.length; i < len; i++) {
        var node = paths[i].node;
        node.setAttribute('stroke-opacity', 1);
        node.setAttribute('stroke-width', 2);
      }
    }
  },

  highlightSelectedVertex: function() {
    if (BS.ChangeLog.selectedVertexType === 'commit') {
      BS.ChangeLog.selectedVertexNode.setAttribute('r', 1.25 * BS.ChangeLog.commitRadius);
    } else {
      BS.ChangeLog.selectedVertexNode.setAttribute('stroke-width', 3);
    }
  },

  restoreUnselectedVertex: function() {
    if (BS.ChangeLog.selectedVertexType === 'commit') {
      BS.ChangeLog.selectedVertexNode.setAttribute('r', BS.ChangeLog.commitRadius);
    } else {
      BS.ChangeLog.selectedVertexNode.setAttribute('stroke-width', 1);
    }
  }
});
