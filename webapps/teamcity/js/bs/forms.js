
/*
 * Copyright 2000-2012 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

BS.AbstractWebForm = {
  formElement: function() {
    return document.forms[0];
  },

  disable: function(elementsFilter) {
    // do not allow to disable form twice because in this case we will remember that
    // fields were disabled and will not enable them when enable will be called
    // (see Form.disable and _wasDisabled property for more details)
    if (this._formDisabled) return;
    var disabledElems = BS.Util.disableFormTemp(this.formElement(), elementsFilter);
    this._formDisabled = true;
    return disabledElems;
  },

  enable: function(elementsFilter) {
    if (this._formDisabled) {
      BS.Util.reenableForm(this.formElement(), elementsFilter);
    }
    var modifiedMessageForm = this._modifiedMessageForm();
    if (modifiedMessageForm) {
      BS.Util.reenableForm(modifiedMessageForm);
    }
    this._formDisabled = false;
  },

  savingIndicator: function() {
    return $('saving');
  },

  setSaving: function(saving) {
    var savingElem = this.savingIndicator();
    if (savingElem) {
      if (!this.progress_saving) {
        this.progress_saving = new BS.DelayedShow(savingElem);
      }

      if (saving) {
        this.progress_saving.start();
      } else {
        this.progress_saving.stop();
      }
    }
  },

  clearErrors: function() {
    if ($("errorMessage")) {
      $("errorMessage").innerHTML = '&nbsp;';
      BS.Util.hide("errorMessage");
    }

    var errorFields = $$(".errorField");
    if (errorFields != null) {
      for (var i = 0; i < errorFields.length; i++) {
        var errorField = errorFields[i];
        if (errorField == null) continue;
        errorField.className = errorField.className.replace("errorField", "");
      }
    }

    var errorMessages = $$(".error");
    if (errorMessages != null) {
      for (var i = 0; i < errorMessages.length; i++) {
        var msg = errorMessages[i];
        if (msg == null) continue;
        this.clearTextInsideElement(msg);
      }
    }
    BS.VisibilityHandlers.updateVisibility(this.formElement());
  },

  trimSpacesInTextFields: function() {
    var inputs = Form.getInputs(this.formElement(), "text");
    for (var i=0; i<inputs.length; i++) {
      inputs[i].value = BS.Util.trimSpaces(inputs[i].value);
    }
  },

  serializeParameters: function() {
    return BS.Util.serializeForm(this.formElement())
  },

  _modifiedMessageForm: function() {
    return $("modifiedMessageForm");
  },

  setModified: function(modified) {
    if (modified) {
      BS.Util.show("modifiedMessage");
      this.hideSuccessMessages();
      var modifiedMessageForm = this._modifiedMessageForm();
      if (modifiedMessageForm) {
        if (this._modifiedHandlers && this._modifiedHandlers.saveState) {
          modifiedMessageForm.style.display = 'inline';
        } else {
          BS.Util.hide("modifiedMessage");
        }
      }
    } else {
      BS.Util.hide("modifiedMessage");
    }
    this.modified = modified;
  },

  hideSuccessMessages: function() {
    BS.Util.hideSuccessMessages();
  },

  clearTextInsideElement: function(elem) {
    if (!elem || !elem.firstChild) return;
    elem.normalize();
    if (!elem.firstChild || !elem.firstChild.nodeType == 3) return;
    elem.firstChild.nodeValue = '';
  },

  highlightErrorField: function(field) {
    field.className = field.className + " errorField";
    if (BS.MultilineProperties) {
      BS.MultilineProperties.updateVisible(); 
    }
  },

  focusFirstErrorField: function() {
    var errorFields = $$(".errorField");
    if (errorFields && errorFields.length > 0) {
      for (var i=0; i<errorFields[i]; i++) {
        var f = errorFields[i];
        if (f == null) continue;
        if (BS.Util.visible(f)) {
          f.focus();
        }
        break;
      }
    }
  },

  _clearIdleHandler: function() {
    if (!this._idleHandlerInfo) return;

    Event.stopObserving(this._idleHandlerInfo._object, 'keyup', this._idleHandler, false);
    Event.stopObserving(this._idleHandlerInfo._object, 'click', this._idleHandler, false);

    this._idleHandlerInfo = null;
    this._idleHandler = null;
  },

  _setupIdleHandler: function(handler, delay) {
    this._clearIdleHandler();

    if (delay == undefined) delay = 1000;

    var that = this;
    this._idleHandlerInfo = {
      _delay : delay,
      _object : that.formElement(),
      _func : handler
    };

    this._idleHandler = function() {
      if (!that._idleHandlerInfo) return;

      if (that._idleHandlerInfo._timeout) {
        clearTimeout(that._idleHandlerInfo._timeout);
      }

      var func = that._idleHandlerInfo._func;
      var arg = that._idleHandlerInfo._object;
      that._idleHandlerInfo._timeout = setTimeout(function(event) { func() }, delay);
    }.bindAsEventListener(this);

    $(this.formElement()).on('keyup', this._idleHandler);
    $(this.formElement()).on('click', this._idleHandler);
  },

  _setupExitHandler: function() {
    this._exitHandler = function(event) {
      if (this.modified) {
        if (!this._shouldCheckForExit(event))  {
          return;
        }

        if (!confirm("Discard your changes?")) {
          Event.stop(event);
        }
      }
    }.bindAsEventListener(this);

    $(document.body).on("click", this._exitHandler);
  },

  _shouldCheckForExit: function(event) {
    var element = Event.element(event);
    if ("true" == element.getAttribute("showdiscardchangesmessage")) {
      return true;
    }

    if (element.tagName != "A") {
      element = Event.findElement(event, "A");
    }

    if (!element || element.nodeType != 1) {
      return false;
    }

    if ("false" == element.getAttribute("showdiscardchangesmessage")) {
      return false;
    }

    // by default treat all links as exit points
    return element && element.href;
  },

  // accepts two handlers: one for session state update and one for state saving. Handler for state
  // saving is optional - in this case Save button won't appear in "Changes not yet saved" message
  setUpdateStateHandlers: _.once(function(handlers) {
    this._modifiedHandlers = handlers;
    this._setupIdleHandler(handlers.updateState);
    if (handlers.saveState) {
      var form = this._modifiedMessageForm();
      if (form && form.save) {
        $(form.save).on("click", handlers.saveState);
      }
    }
    this._setupExitHandler();
  }),

  checkStateModified: function() {
    if (this._idleHanlder) {
      this._idleHanlder();
    }
  },

  removeUpdateStateHandlers: function() {
    this._clearIdleHandler();
    if (this._modifiedHandlers && this._modifiedHandlers.saveState) {
      var form = this._modifiedMessageForm();
      if (form && form.save) {
        Event.stopObserving(form.save, "click", this._modifiedHandlers.saveState, false);
      }
    }
    if (this._exitHandler != null) {
      Event.stopObserving(document.body, "click", this._exitHandler, false);
    }
    this._modifiedHandlers = null;
    this._exitHandler = null;
  }
};


BS.SimpleListener = {
  onBeginSave: function(form) {},

  onCompleteSave: function(form, responseXML, err, responseText) {},

  onException: function(form, e) {
    BS.Util.processError(e);
  },

  onFailure: function(form) {
    alert("Error accessing server");
  }
};

BS.ErrorsAwareListener = OO.extend(BS.SimpleListener, {
  onBeginSave: function(form) {
    form.trimSpacesInTextFields();
    form.clearErrors();
    form.hideSuccessMessages();
    form.disable();
    form.setSaving(true);
  },

  onCompleteSave: function(form, responseXML, err) {
    form.setSaving(false);
    if (err) {
      form.enable();
      form.focusFirstErrorField();
    } else {
      this.onSuccessfulSave();
    }
  },

  onSuccessfulSave: function() {}
});


BS.StoreInSessionListener = OO.extend(BS.SimpleListener, {
  onCompleteSave: function(form, responseXML, errStatus) {
    if (!errStatus) {
      BS.XMLResponse.processModified(form, responseXML);
    }
  },

  onException: function(form, e) {
  },

  onFailure: function(form) {
  }
});


BS.AbstractPasswordForm = OO.extend(BS.AbstractWebForm, {
  publicKey: function() {
    return $F("publicKey");
  },

  serializeParameters: function() {
    var params = BS.AbstractWebForm.serializeParameters.bind(this)();
    var passwordFields = Form.getInputs(this.formElement(), "password");
    if (!passwordFields) return params;
    for (var i = 0; i < passwordFields.length; i++) {
      var name = passwordFields[i].name;
      var encryptedName = "encrypted" + name.charAt(0).toUpperCase() + name.substring(1);
      params += "&" + encryptedName + "=";

      if (passwordFields[i].value.length == 0) continue;

      var encryptedValue = "";
      if (passwordFields[i].getEncryptedPassword != null) {
        encryptedValue = passwordFields[i].getEncryptedPassword(this.publicKey());
      } else {
        encryptedValue = BS.Encrypt.encryptData(passwordFields[i].value, this.publicKey());
      }
      params += encryptedValue;
    }

    return params;
  }
});


BS.FormSaver = {
  save: function(form, submitUrl, listener, debug) {
    try {
      listener.onBeginSave(form);
      var params = form.serializeParameters();
      if (debug) {
        alert(params);
      }

      BS.ajaxRequest(submitUrl, {
        method: "post",

        parameters: params,

        onComplete: function(transport) {
          if (debug) {
            alert(transport.responseText);
          }

          if (!transport.responseXML) {
            listener.onCompleteSave(form, null, null, transport.responseText);
          } else {
            var responseXML = transport.responseXML;
            var err = BS.XMLResponse.processErrors(responseXML, listener);
            listener.onCompleteSave(form, responseXML, err, transport.responseText);
          }
        },
        onFailure: function() {
          listener.onFailure(form);
        },
        onException: function(obj, e) {
          listener.onException(form, e);
        }
      });
    }
    catch (e) {
      listener.onException(form, e);
    }
  }
};

BS.PasswordFormSaver = OO.extend(BS.FormSaver, {
  save: function(form, submitUrl, listener, debug) {
    var that = this;

    if (!listener.onPublicKeyExpiredError) {
      listener.onPublicKeyExpiredError = function(elem) {
        $("publicKey").value = elem.firstChild.nodeValue;
        setTimeout(function() {
          that.save(form, submitUrl, listener, debug);
        }, 100);
      }
    }

    BS.FormSaver.save(form, submitUrl, listener, debug);
  }
});

if (!BS.CustomCheckbox) {
  $j(document).ready(function() {
    new BS.CustomCheckbox();
  });
}

BS.CustomCheckbox = Class.create({
  initialize: function() {
    var $document = $j(document);

    $document.find('.custom-checkbox_input').each(function() {
      if (this.disabled) {
        $j(this).parent().addClass('custom-checkbox_disabled');
      }

      if (this.checked) {
        $j(this).parent().addClass('custom-checkbox_checked');
      }
    });

    $document.on('change', '.custom-checkbox', function() {
      var checkbox = $j(this);

      if (checkbox.hasClass('custom-checkbox_disabled')) return false;

      var input = checkbox.find('.custom-checkbox_input'),
          checked = input.prop('checked');

      checkbox.toggleClass('custom-checkbox_checked custom-checkbox_focused', checked);
    });
  }
});
