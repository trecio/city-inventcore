BS.AgentsParametersReport = {

  completionUrl: window['base_uri'] + '/agentParametersAutocompletion.html',


  initForm: function() {
    $j('#paramName').autocomplete({source: BS.AgentsParametersReport.createSource(),
                                   completeOnCtrlSpace: true,
                                   minLength: 0});
    $j('#paramName').focus();
    $j(document).ready(function() {
      var hash = BS.AgentsParametersReport.getHash();
      if (hash) {
        $j('#paramName').val(hash);
        BS.AgentsParametersReport.group();
      }
    });
  },


  createSource: function() {
    return function(request, response) {
      $j('#reportResultLoading').show();
      jQuery.ajax({
        type: "GET",
        url: BS.AgentsParametersReport.completionUrl + '?what=name&term=' + encodeURIComponent(request.term),
        data: null,
        success: function(data) {
          $j('#reportResultLoading').hide();
          response(data);
        },
        error: function(xhr, status, e) {
          $j('#reportResultLoading').hide();
          $j('#errors').append(xhr.responseText);
        },
        dataType: 'json'
      });
    };
  },


  group: function() {
    var paramName  = BS.Util.trimSpaces($j('#paramName').val());
    if (paramName) {
      $j('#paramName').removeClass("errorField");
      $j('#paramNameError').hide();
      $j('#reportResultLoading').show();
      $('queryResult').refresh(null, 'name=' + encodeURIComponent(paramName), function() {
        BS.AgentsParametersReport.setHash(paramName);
        $j('#reportResultLoading').hide();
      });
    } else {
      BS.AgentsParametersReport.setHash('');
      $j('#paramName').addClass("errorField");
      $j('#paramNameError').css({"margin-left": $j('#paramNameLabel').outerWidth(true)});
      $j('#paramNameError').show();
    }
    return false;
  },


  getHash: function() {
    if (window.location.hash) {
      return window.location.hash.substr(1);
    } else {
      return '';
    }
  },


  setHash: function(hash) {
    window.location.hash = hash;
  }
};