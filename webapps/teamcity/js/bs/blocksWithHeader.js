BS.BlocksWithHeader = Class.create(BS.Blocks, {

  blocksType: "blocks",
  collapsedByDefault: false,

// Constructor:
  initialize: function($super, elementId, dontRestorePreviousState) {
    $super();
    this.blocksType = "Block_" + elementId;

    var blockHeaderElement = $(elementId);
    if (!blockHeaderElement) return;

    this.headerId = elementId;
    if (!$(this.headerId).title && $(this.headerId).className.indexOf('no_title') == -1) {
      $(this.headerId).title = "Click to expand/collapse this block";
    }

    this.collapsedByDefault = blockHeaderElement.className.indexOf("expanded") == -1;

    blockHeaderElement.on("click", this._onclick.bindAsEventListener(this));

    blockHeaderElement = null; // prevent memory leak

    this.changeState(elementId, true, !this.collapsedByDefault);

    if (!dontRestorePreviousState) {
      this.restoreSavedBlocks();
    }
  },

  getId: function() {
    return this.headerId;
  },

  show: function() {
    this.changeState(this.headerId, true, true);
    this._saveState();
  },

  _onclick: function(e) {
    this.toggleBlock(this.headerId, false);
  },

  getBlockContentElement: function(id) {
    var el = $(id);

    if (!el || !el.parentNode) return;

    $(el.parentNode).cleanWhitespace();
    var contentDiv = el.nextSibling;
    if (!contentDiv) alert("No contentDiv for " + el.id);
    return contentDiv;
  },

  onHideBlock: function(contentElement, id) {
    id = $(id);

    id.addClassName("collapsed");
    id.removeClassName("expanded");
  },

  onShowBlock: function(contentElement, id) {
    id = $(id);

    id.addClassName("expanded");
    id.removeClassName("collapsed");
  }
});


