//
// See class GetRunningBuildsAction
//


BS.RunningBuilds = {
  startUpdates: function(processorCallback) {
    if (this._updater) this._updater.stop();

    this._updater = new BS.PeriodicalUpdater(null, window['base_uri'] + "/ajax.html?getRunningBuilds=1", {
      frequency: 6,
      evalScripts: false,
      onSuccess: function(transport) {
        var root = BS.Util.documentRoot(transport);
        if (!root) return;

        var buildNodes = root.childNodes;

        // Map from buildTypeId to array of running buildIds
        var buildTypesToRunning = {};

        for (var i = 0; i < buildNodes.length; i++) {
          var node = buildNodes[i];
          var buildId = node.getAttribute("buildId");
          var buildTypeId = node.getAttribute("buildTypeId");

          if (!buildTypesToRunning[buildTypeId]) {
            buildTypesToRunning[buildTypeId] = [];
          }
          buildTypesToRunning[buildTypeId].push(buildId);

          BS.RunningBuilds.processNode(node, buildId);

          if (BS.ProblemsSummary) {
            BS.ProblemsSummary.requestBuildTypeUpdate(buildTypeId);
          }
        }

        if (processorCallback) {
          processorCallback(buildTypesToRunning);
        }
      }
    });
  },

  processNode: function(node, buildId) {
    this.updateStatusIcon(node, buildId);
    this.updateDescription(node, buildId);
    this.updateDuration(node, buildId);
    this.updateProgress(node, buildId);
    this.updateArtifactsLink(node, buildId);
    this.updateBuildTypeStatus(node);
  },

  findNestedDiv: function(element) {
    return element.getElementsByTagName("div")[0];
  },

  updateStatusIcon: function(node, buildId) {
    var icon = $('build:' + buildId + ":img");

    if (icon) {
      var successful = "true" == node.getAttribute("successful");
      var personal = node.getAttribute("personal");

      var newSrc = icon.src;
      if (personal) {
        if (successful) {
          newSrc = newSrc.replace('personalRunningFailing.gif', 'personalRunning.gif');
        } else {
          newSrc = newSrc.replace('personalRunning.gif', 'personalRunningFailing.gif');
        }
      } else {
        newSrc = window['base_uri'] + "/img/buildStates/" + (successful ? "running_green_transparent.gif" : "running_red_transparent.gif");
      }

      if (icon.src != newSrc) {
        icon.src = newSrc;
      }
    }
  },

  updateDescription: function(node, buildId) {
    var txtNode = $('build:' + buildId + ":text");

    if (txtNode) {
      var textContent = node.getAttribute("text");
      textContent = textContent.escapeHTML();
      if (textContent.length > 100) {
        textContent = textContent.substring(0, 100) + "&hellip;";
      }
      txtNode.innerHTML = textContent;
    }
  },

  updateDuration: function(node, buildId) {
    var elapsedTime = node.getAttribute("elapsedTime");
    var durationNode = $('build:' + buildId + ":duration");

    if (durationNode) {
      durationNode.innerHTML = elapsedTime;
    }
  },

  updateProgress: function(node, buildId) {
    var progressNode = $('build:' + buildId + ":progress");

    if (progressNode && progressNode.nextSibling) {
      var elapsedTime = node.getAttribute("elapsedTime");
      var title = elapsedTime + " passed";
      title = "started: " + progressNode.nextSibling.innerHTML + "<br>" + elapsedTime + " passed";

      var nestedDiv = this.findNestedDiv(progressNode);
      var remainingTime = node.getAttribute("remainingTime");

      if (nestedDiv) {
        if ('N/A' == remainingTime) {
          nestedDiv.style.width = "100%";
        } else {
          if (remainingTime && remainingTime != "< 1s") {
            title += "<br>" + remainingTime + " left";

            nestedDiv = this.setTextInProgress(progressNode, remainingTime + " left"); // refresh after HTML change
          }

          var exceededDurationTime = node.getAttribute("exceededEstimatedDurationTime");
          if (exceededDurationTime && exceededDurationTime != "< 1s") {
            title += "<br>" + exceededDurationTime + " overtime";

            nestedDiv = this.setTextInProgress(progressNode, "overtime: " + exceededDurationTime); // refresh after HTML change
          }

          var successful = "true" == node.getAttribute("successful");
          nestedDiv.style.backgroundColor = successful ? "green" : "#c90a00";
          nestedDiv.style.width = node.getAttribute("completedPercent") + "%";
        }
      }

      var agentLink = $('aLink:' + buildId);
      if (agentLink && agentLink.innerHTML != '') {
        title += "<br>agent: " + agentLink.innerHTML;
      }

      var progressId = progressNode.id;
      progressNode._title = title;
      if (!progressNode._eventsBound) {
        progressNode.on("mouseenter", function() {
          BS.Tooltip.showMessage($(progressId), {shift:{x:10,y:20}}, $(progressId)._title);
        });
        progressNode.on("mouseleave", function() {
          BS.Tooltip.hidePopup();
        });
        progressNode._eventsBound = true;
      }
    }
  },

  setTextInProgress: function(progressNode, text) {
    var t = "&nbsp;&nbsp;" + text.replace(/ /g, "&nbsp;");
    progressNode.innerHTML = t + "<div class='progressInner'>" + t + "</div>";

    return this.findNestedDiv(progressNode); // refresh after HTML change
  },

  updateArtifactsLink: function(node, buildId) {
    var hasArtifacts = node.getAttribute('hasArtifacts') == 'true';

    if (hasArtifacts) {
      var artifactsLink = $('build:' + buildId + ':artifactsLink');
      var noArtifactsText = $('build:' + buildId + ':noArtifactsText');
      if (artifactsLink && noArtifactsText) {
        artifactsLink.style.display = 'inline';
        noArtifactsText.style.display = 'none';
      }
    }
  },

  updateBuildTypeStatus: function(node) {
    var buildTypeId = node.getAttribute('buildTypeId');
    var breadcrumbNode = $j('#main_navigation').children('.buildType');

    if (breadcrumbNode.attr('data-buildTypeId') == buildTypeId) {
      breadcrumbNode.toggleClass('failed', node.getAttribute('buildTypeSuccessful') == 'false');
    }
  }
};


