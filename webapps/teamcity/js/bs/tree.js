/*
 * Content-seperated javascript tree widget
 * Copyright (C) 2005 SilverStripe Limited
 * Feel free to use this on your websites, but please leave this message in the fies
 * http://www.silverstripe.com/blog
*/

/*
 * Initialise all trees identified by <ul class="tree">
 */
(function() {
function autoInit_trees() {
	var candidates = document.getElementsByTagName('ul');
        for(var i=0;i<candidates.length;i++) {
		if(candidates[i].className && candidates[i].className.indexOf('tree') != -1) {
			initTree(candidates[i]);
			candidates[i].className = candidates[i].className.replace(/ ?unformatted ?/, ' ');
		}
	}
}

/*
 * Initialise a tree node, converting all its LIs appropriately
 */
function initTree(el) {
	var i,j;
	var spanA, spanB, spanC;
	var startingPoint, stoppingPoint, childUL;

    // Find all LIs to process
	for(i=0;i<el.childNodes.length;i++) {
		if(el.childNodes[i].tagName && el.childNodes[i].tagName.toLowerCase() == 'li') {
			var li = el.childNodes[i];

			// Create our extra spans
			spanA = document.createElement('span');
			spanB = document.createElement('span');
			spanC = document.createElement('span');
			spanA.appendChild(spanB);
			spanB.appendChild(spanC);
			spanA.className = 'a ' + li.className.replace('closed','spanClosed');
			spanA.onMouseOver = function() {};
			spanB.className = 'b';
			spanB.onclick = treeToggle;
			spanC.className = 'c';


			// Find the UL within the LI, if it exists
			stoppingPoint = li.childNodes.length;
			startingPoint = 0;
			childUL = null;
			for(j=0;j<li.childNodes.length;j++) {
				if(li.childNodes[j].tagName && li.childNodes[j].tagName.toLowerCase() == 'div') {
					startingPoint = j + 1;
					continue;
				}

				if(li.childNodes[j].tagName && li.childNodes[j].tagName.toLowerCase() == 'ul') {
					childUL = li.childNodes[j];
					stoppingPoint = j;
					break;
				}
			}

			// Move all the nodes up until that point into spanC
			for(j=startingPoint;j<stoppingPoint;j++) {
				spanC.appendChild(li.childNodes[startingPoint]);
			}

			// Insert the outermost extra span into the tree
			if(li.childNodes.length > startingPoint) li.insertBefore(spanA, li.childNodes[startingPoint]);
			else li.appendChild(spanA);

			// Process the children
			if(childUL != null) {
				if(initTree(childUL)) {
					addClass(li, 'children', 'closed');
					addClass(spanA, 'children', 'spanClosed');
				}
			}
		}
	}

	if(li) {
		// li and spanA will still be set to the last item

		addClass(li, 'last', 'closed');
		addClass(spanA, 'last', 'spanClosed');
		return true;
	} else {
		return false;
	}

}

/*
 * +/- toggle the tree, where el is the <span class="b"> node
 * force, will force it to "open" or "close"
 */
function treeToggle(el, force) {
	el = this;

	while(el != null && (!el.tagName || el.tagName.toLowerCase() != "li")) el = el.parentNode;

	// Get UL within the LI
	var childSet = findChildWithTag(el, 'ul');
	var topSpan = findChildWithTag(el, 'span');

	if( force != null ) {

		if( force == "open") {
			treeOpen( topSpan, el )
		}
		else if( force == "close" ) {
			treeClose( topSpan, el )
		}

	}

	else if( childSet != null) {
		// Is open, close it
		if(!el.className.match(/(^| )closed($| )/)) {
			treeClose( topSpan, el );
		// Is closed, open it
		} else {
			treeOpen( topSpan, el );
		}
	}
}

function treeOpen(a, b) {
	removeClass(a,'spanClosed');
	removeClass(b,'closed');
}

function treeClose(a, b) {
	addClass(a,'spanClosed');
	addClass(b,'closed');
}

/*
 * Find the a child of el of type tag
 */
function findChildWithTag(el, tag) {
	for(var i=0;i<el.childNodes.length;i++) {
		if(el.childNodes[i].tagName != null && el.childNodes[i].tagName.toLowerCase() == tag) return el.childNodes[i];
	}
	return null;
}

/*
 * Functions to add and remove class names
 * Mac IE hates unnecessary spaces
 */
function addClass(el, cls, forceBefore) {
	if(forceBefore != null && el.className.match(new RegExp('(^| )' + forceBefore))) {
		el.className = el.className.replace(new RegExp("( |^)" + forceBefore), '$1' + cls + ' ' + forceBefore);

	} else if(!el.className.match(new RegExp('(^| )' + cls + '($| )'))) {
		el.className += ' ' + cls;
		el.className = el.className.replace(/(^ +)|( +$)/g, '');
	}
}
function removeClass(el, cls) {
	var old = el.className;
	var newCls = ' ' + el.className + ' ';
	newCls = newCls.replace(new RegExp(' (' + cls + ' +)+','g'), ' ');
	el.className = newCls.replace(/(^ +)|( +$)/g, '');
}

// Export
window.autoInit_trees = autoInit_trees;
})();

BS.VCS = {
  _vcsTreeHandlers : [],
  _vcsTreeCallbacks : [],

  registerTree: function(vcsTreeId, buildFormId, callback, fieldId, controlElementId) {
    var callbackName = this._getCallbackName(vcsTreeId, callback, fieldId);
    this._vcsTreeHandlers[vcsTreeId] = function() {
      var url = window['base_uri'] + "/buildTypeVcsBrowser.html?lazy-tree-update=true&id=" + buildFormId + "&treeId=" + vcsTreeId + "&callback=" + callbackName;
      BS.LazyTree.treeUrl = url;
      BS.LazyTree.ignoreHashes = true;
      var popup = new BS.Popup("vcsTreePopup", {
        hideOnMouseOut: false,
        hideOnMouseClickOutside: true,
        shift: {x: 0, y: 20},
        url: url
      });
      popup.showPopupNearElement($(controlElementId));
    };
    if (fieldId) {
      var visibilityHandler = {
        updateVisibility: function() {
          if ($j(BS.Util.escapeId(fieldId)).is(':disabled')) {
            $j(BS.Util.escapeId(controlElementId)).hide();
          } else {
            $j(BS.Util.escapeId(controlElementId)).show();
          }
        }
      };
      BS.VisibilityHandlers.attachTo(fieldId, visibilityHandler);
    }
  },

  showTree: function(vcsTreeId) {
    (this._vcsTreeHandlers[vcsTreeId])();
  },

  fileChosen: function(file, vcsTreeId, treeElement) {
    var callback = this._vcsTreeCallbacks[vcsTreeId];
    if (callback) {
      callback(file, treeElement);
    }
  },

  _getCallbackName: function(vcsTreeId, callback, fieldId) {
    if (fieldId) {
      this._vcsTreeCallbacks[vcsTreeId] = function(chosenFile, treeElement) {
        $j(BS.Util.escapeId(fieldId)).val(chosenFile);
        BS.Highlight(treeElement, {restorecolor: '#ffffff'});
      };
      return "BS.VCS.fileChosen";
    }
    return callback;
  }
};

BS.LazyTree = {
  treeUrl : null,
  ignoreHashes: false,
  options: {},

  getUrl: function() {
    var url = (this.treeUrl) ? this.treeUrl : document.location.href;
    var hashIdx = url.indexOf('#');
    if (hashIdx >= 0) {
      url = url.substring(0, hashIdx);
    }
    return url;
  },

  getHash: function() {
    var hash = window.location.hash;
    if (hash) {
      if (this.ignoreHashes || !hash.startsWith("#!")) {
        // The hash may be not related to the tree (TW-20305).
        return "";
      }
      hash = hash.substring(2);
    }
    return hash;
  },

  setHash: function(hash) {
    if (!this.ignoreHashes) {
      window.location.hash = '#!' + hash;
    }
  },

  loadTree: function(containerId) {
    containerId = $(containerId);

    containerId.innerHTML = BS.loadingIcon + " Loading...";
    var that = this;
    setTimeout(function() {
      BS.ajaxRequest(that.getUrl(), {
        parameters: "lazy-tree-update=1&lazy-tree-open=" + that.getHash(),
        method : "get",
        onComplete: function(transport) {
          containerId.innerHTML = "";
          containerId.insert(transport.responseText);
          $j(document).trigger("bs.treeLoaded", [containerId, that.options]);
        }
      });
    }, 500);
  },

  toggleShow: function(id) {
    var elem = $('U' + id);
    if (elem) {
      if (elem.visible()) {
        elem.hide();
        this.updateClass(id, "open", "closed");
        this.removeHash(id);
        if (this.isErrorNode(elem)) {       // In case of error, remove the node, so it will requested again
          elem.remove();
        }
      } else {
        elem.show();
        this.updateClass(id, "closed", "open");
        this.addHash(id);
      }
    } else {
      if (this.updateClass(id, "closed", "open")) {   // Open for the first time
        this.loadSubtree(id);
        this.addHash(id);
      }
    }
  },

  updateClass: function(id, prevValue, newValue) {
    id = $(id);

    var classes = id.className.split(" ");
    if (classes.indexOf(prevValue) == -1 && classes.indexOf(newValue) != -1) {
      // Already opened.
      return false;
    }

    classes.splice(classes.indexOf(prevValue), 1);
    classes.push(newValue);
    id.className = classes.join(" ");
    return true;
  },

  addHash: function(id) {
    var current = this.getHash();
    if (!current) {
      this.setHash(id);
    } else {
      this.setHash(current + "," + id);
    }
  },

  removeHash: function(id) {
    var current = this.getHash();
    if (!current) {
      return;
    }
    var ids = current.split(",");
    var idx = ids.indexOf(id);
    if (idx >= 0) {
      ids.splice(idx, 1);
    }
    this.setHash(ids.join(","));
  },

  loadSubtree: function(id) {
    var elem = $(id);

    var loading = $j("<i/>").css({
      marginLeft: "16px",
      display: "none"
    }).append(BS.loadingIcon).append(" Loading...").appendTo(elem);

    setTimeout(function() {
      loading.show();
    }, 200);

    var path = [];
    while (elem && elem.className.lastIndexOf("lazy-tree") == -1) {
      if (elem.className.lastIndexOf("lazy-subtree") == -1) {
        path.push(elem.id);
      }
      elem = elem.parentNode;
    }
    path.reverse();

    var that = this;
    BS.ajaxRequest(this.getUrl(), {
      parameters: "lazy-tree-update=1&lazy-tree-root=" + id + "&lazy-tree-path=" + path.join(","),
      method : "get",
      onComplete: function(transport) {
        loading.remove();
        $(id).insert(transport.responseText);
        $j(document).trigger("bs.subtreeLoaded", [$(id), that.options]);
      }
    });
  },

  isErrorNode: function(elem) {
    var children = elem.childNodes;
    return children && children.length == 1 && children[0].className == "tree-error";
  }
};

BS.FileBrowse = {
  error: function(msg) {
    this.setSaving(false);
    $j("#uploadError").html(msg).show();
  },

  clear: function() {
    var container = $j(this.getContainer());
    container.find("input[type=file]").val("");
    $j("#uploadError").hide();
  },

  show: function() {
    this.clear();
    this.showCentered();
    return false;
  },

  validate: function() {
    var fileName = $j("#fileName").val(),
        file = $j("#file\\:fileToUpload").val();

    if (fileName == "") {
      this.error("Please set the file name");
      return false;
    }
    if (!this.checkFileNotExists(fileName)) {
      return false;
    }
    if (file == "") {
      this.error("Please select file to upload");
      return false;
    }

    this.setSaving(true);
    return true;
  },

  deleteFile: function(actionUrl, homeUrl, message, fileName) {
    var s = (!message) ? "Delete \"" + fileName + "\"?" :
                         message + "\n" + "Delete anyway?";
    if (!confirm(s)) {
      return false;
    }

    BS.ajaxRequest(actionUrl, {
      method: "post",
      parameters: 'action=delete&fileName=' + fileName,
      onComplete: function() {
        document.location.href = homeUrl;
      }
    });
    return false;
  },

  setFiles: function(files) {
    this.files = files;
  },

  prepareFileUpload: function() {
    var container = $j(this.getContainer());
    container.find("input[type=file]").change(function() {
      var fullPath = $j(this).val();
      if (fullPath) {
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        var filename = fullPath.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
          filename = filename.substring(1);
        }
        $j("#fileName").val(filename);
      }
    });
  },

  checkFileNotExists: function(file) {
    for (var i = 0; i < this.files.length; ++i) {
      if (file == this.files[i]) {
        return confirm("File \"" + file + "\" already exists. Replace it?");
      }
    }
    return true;
  },

  closeAndRefresh: function() {
    this.setSaving(false);
    this.close();
    $j("#refreshing").show();
    this.refresh();
  }
};
