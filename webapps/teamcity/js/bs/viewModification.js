
BS.buildTypeId_changed = function(select) {
  var parameters = select.options[select.selectedIndex].value;
  $('viewModificationContainerId').refresh('changeBuildTypeProgress', parameters);
};

jQuery(document).ready(function() {
  BS.RunningBuilds.startUpdates(function(newRunning) {
    //alert(newRunning.toSource());
    if (!BS.buildTypes) return;

    var arrays_equals = function(a1, a2) {
      if (!a1) a1 = [];
      if (!a2) a2 = [];
      if (a1.length != a2.length) return false;
      a1 = a1.sort();
      a2 = a2.sort();
      for(var i = 0; i < a1.length; i++) {
        if (a1[i] != a2[i]) return false;
      }
      return true;
    };

    for(var bt in BS.buildTypes) {

      if (BS.buildTypes[bt].successful) continue;

      var runningInBT = newRunning[bt];

      if (!arrays_equals(runningInBT, BS.buildTypes[bt].running) ) {
        BS.buildTypes[bt].running = runningInBT;
        $('buildTypesContainerId').refresh(null, '');
        break;
      }
    }
  });
});

jQuery(document).ready(function() {
  if ($('failedTestsSection')) {
    BS.PeriodicalRefresh.start(15, function() {
      $('failedTestsSection').refresh(null, '');
    });
  }
});
