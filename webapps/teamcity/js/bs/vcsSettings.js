

BS.EditVcsUsername = OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('editVcsSettingsDialog');
  },

  showEditDialog: function(key, vcsUsername, vcsDisplayName) {
    BS.Util.hide('vcsRootSelector');

    $('editVcsSettingsTitle').innerHTML = vcsDisplayName;
    $('vcsUsername').value = vcsUsername;
    $('vcsUsernameKey').value = key;

    this.showCentered();
    this.bindCtrlEnterHandler(this.submitUsername.bind(this));

    $('vcsUsername').focus();
  },

  showAddDialog: function(defaultVcsUsername) {
    BS.Util.show('vcsRootSelector');

    $('editVcsSettingsTitle').innerHTML = "Add VCS username";
    $('vcsUsername').value = defaultVcsUsername;
    $('vcsUsernameKey').value = "";
    $("vcsRoot").onchange();

    this.showCentered();
    this.bindCtrlEnterHandler(this.submitUsername.bind(this));

    $("vcsRoot").focus();
  },

  deleteUsername: function(vcsUsernameKey) {
    if (!confirm("Are you sure you want to delete this username?")) {
      return;
    }

    var form = $('editVcsSettings');
    BS.ajaxRequest(form.action, {
      parameters: "vcsUsernameKey=" + vcsUsernameKey + "&vcsUsername=" + "&userId=" + form.userId.value,
      onComplete: function() {
        $('vcsUsernames').refresh();
      }
    });
  },

  submitUsername: function() {
    var form = $('editVcsSettings');
    BS.Util.show('savingUsername');
    Form.disable(form);
    BS.ajaxRequest(form.action, {
      parameters: BS.Util.serializeForm(form),
      onComplete: function() {
        BS.Util.hide('savingUsername');
        BS.EditVcsUsername.close();
        $('vcsUsernames').refresh();
      }
    });
    return false;
  }
});