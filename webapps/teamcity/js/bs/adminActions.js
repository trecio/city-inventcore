BS.SaveConfigurationListener = OO.extend(BS.ErrorsAwareListener, {
  onSaveProjectErrorError: function(elem) {
    alert(elem.firstChild.nodeValue);
  },

  onProjectNotFoundError: function(elem) {
    BS.reload(true);
  },

  onBuildTypeNotFoundError: function(elem) {
    BS.reload(true);
  },

  onMaxNumberOfBuildTypesReachedError: function(elem) {
    alert(elem.firstChild.nodeValue);
  }
});

BS.AdminActions = {
  url: window['base_uri'] + "/admin/action.html",

  deleteBuildType: function (buildTypeId, forceDelete, cameFromUrl) {
    if (!forceDelete && !confirm('Are you sure you want to delete this build configuration?')) return false;

    BS.ajaxRequest(this.url, {
      parameters: "buildTypeId=" + buildTypeId + "&removeBuildType=1" + (forceDelete ? "&forceDelete=1" : "") + (cameFromUrl ? "&cameFromUrl=" + encodeURIComponent(cameFromUrl) : ""),
      onComplete: function(transport) {
        var errors = BS.XMLResponse.processErrors(transport.responseXML, {
          onReferencesExistError: function(elem) {
            var confirmMessage = elem.firstChild.nodeValue + "\nAre you sure you want to delete this build configuration?";
            if (confirm(confirmMessage)) {
              BS.AdminActions.deleteBuildType(buildTypeId, true, cameFromUrl);
            }
          },
          onUndeletableReferencesExistError: function(elem) {
            alert(elem.firstChild.nodeValue);
          },
          onRunningBuildsExistError: function(elem) {
            alert(elem.firstChild.nodeValue);
          }
        });

        if (!errors) {
          BS.XMLResponse.processRedirect(transport.responseXML);
        }
      }
    });
  },

  deleteBuildTypeTemplate: function (buildTypeTemplateId) {
    if (!confirm('Are you sure you want to delete this template?')) return false;

    BS.ajaxRequest(this.url, {
      parameters: "templateId=" + buildTypeTemplateId + "&removeTemplate=1",
      onComplete: function(transport) {
        var errors = BS.XMLResponse.processErrors(transport.responseXML, {
          cannotBeRemovedError: function(elem) {
            alert(elem.firstChild.nodeValue);
          }
        });

        if (!errors) {
          BS.XMLResponse.processRedirect(transport.responseXML);
        }
      }
    });
  },

  deleteProject: function(projectId, forceDelete) {
    if (!forceDelete && !confirm('Are you sure you want to delete this project?')) return false;

    BS.ajaxRequest(this.url, {
      parameters: "projectId=" + projectId + "&removeProject=1" + (forceDelete ? "&forceDelete=1" : ""),
      onComplete: function(transport) {
        var errors = BS.XMLResponse.processErrors(transport.responseXML, {
          onProjectRemoveFailedError: function(elem) {
            alert(elem.firstChild.nodeValue);
          },

          onReferencesExistError: function(elem) {
            var confirmMessage = elem.firstChild.nodeValue + "\nAre you sure you want to delete this project?";
            if (confirm(confirmMessage)) {
              BS.AdminActions.deleteProject(projectId, true);
            }
          },

          onUndeletableReferencesExistError: function(elem) {
            alert(elem.firstChild.nodeValue);
          },

          onRunningBuildsExistError: function(elem) {
            alert(elem.firstChild.nodeValue);
          }
        });

        if (!errors) {
          BS.XMLResponse.processRedirect(transport.responseXML);
        }
      }
    });

    return false;
  },

  archiveProject: function(projectId, doNotRedirect) {
    if (!confirm('Are you sure you want to archive this project?')) return false;

    BS.ajaxRequest(this.url, {
      parameters: "projectId=" + projectId + "&archiveProject=1",
      onComplete: function(transport) {
        var errors = BS.XMLResponse.processErrors(transport.responseXML);

        if (!errors && !doNotRedirect) {
          BS.XMLResponse.processRedirect(transport.responseXML);
        } else {
          BS.reload(true);
        }
      }
    });

    return false;
  },

  dearchiveProject: function(projectId, doNotRedirect) {
    if (!confirm('Are you sure you want to dearchive this project?')) return false;

    BS.ajaxRequest(this.url, {
      parameters: "projectId=" + projectId + "&dearchiveProject=1",
      onComplete: function(transport) {
        var errors = BS.XMLResponse.processErrors(transport.responseXML);

        if (!errors && !doNotRedirect) {
          BS.XMLResponse.processRedirect(transport.responseXML);
        } else {
          BS.reload(true);
        }
      }
    });

    return false;
  },

  deleteVcsRoot: function(vcsRootId) {
    if (!confirm('Are you sure you want to delete this VCS root?')) return false;

    BS.Util.hideSuccessMessages();
    BS.Util.show('vcsRootProgress_' + vcsRootId);
    BS.ajaxRequest(this.url, {
      parameters: "removeVcsRoot=" + vcsRootId,
      onComplete: function(transport) {
        var errors = BS.XMLResponse.processErrors(transport.responseXML, {
          onReferencesExistError: function(elem) {
            alert(elem.firstChild.nodeValue);
          }
        });

        if (!errors) {
          BS.AdminActions._refreshAllRoots();
        } else {
          BS.Util.hide('vcsRootProgress_' + vcsRootId);
        }
      }
    });
  },

  detachVcsRoot: function(vcsRootId, settingsId, confirmMsg) {
    if (!confirm(confirmMsg ? confirmMsg : 'Are you sure you want to detach this VCS root?')) return false;

    BS.Util.hideSuccessMessages();

    BS.ajaxRequest(this.url, {
      parameters: "detachVcsRoot=" + vcsRootId + "&settingsId=" + settingsId,
      onComplete: function() {
        BS.AdminActions._refreshVcsRoot(vcsRootId);
      }
    });
  },

  _refreshVcsRoot: function(vcsRootId) {
    $('vcsRootContainer_' + vcsRootId).refresh('vcsRootProgress_' + vcsRootId);
  },

  _refreshAllRoots: function() {
    BS.reload(true);
  },

  deleteIssueProvider: function(providerId) {
    if (!confirm('Are you sure you want to remove an issue tracker connection?')) return false;

    BS.Util.hideSuccessMessages();

    BS.ajaxRequest(this.url, {
      parameters: "deleteIssueProvider=1&providerId=" + providerId,
      onComplete: function() {
        BS.reload(true);
      }
    });
  },

  checkProjectHasLocalVcsRoots: function(projectId, oncompletefunc) {
    this._checkHasLocalVcsRoots("projectId=" + projectId, oncompletefunc);
  },

  checkBuildTypeHasLocalVcsRoots: function(buildTypeId, oncompletefunc) {
    this._checkHasLocalVcsRoots("buildTypeId=" + buildTypeId, oncompletefunc);
  },

  checkTemplateHasLocalVcsRoots: function(templateId, oncompletefunc) {
    this._checkHasLocalVcsRoots("templateId=" + templateId, oncompletefunc);
  },

  _checkHasLocalVcsRoots: function(objectIdParam, oncompletefunc) {
    BS.ajaxRequest(window['base_uri'] + "/admin/action.html", {
      parameters: "checkHasLocalVcsRoots=true&" + objectIdParam,

      onComplete: function(transport) {
        var xml = transport.responseXML;
        if (xml) {
          var result = xml.documentElement.firstChild.firstChild.nodeValue;
          if (result == "true") {
            oncompletefunc(true);
          } else {
            oncompletefunc(false);
          }
        }
      }
    });
  },

  setSettingsEnabled: function(btId, settingsId, enabled, settingsName) {
    BS.ajaxRequest(this.url, {
      parameters: "id=" + btId + "&setEnabled=" + settingsId + "&enabled=" + enabled + "&settingName=" + settingsName,
      onSuccess: function() {
        BS.reload(true);
      }
    });
  }
};


BS.AttachBuildTypeForm = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('attachBuildTypeProgress');
    } else {
      BS.Util.hide('attachBuildTypeProgress');
    }
  },

  getContainer: function() {
    return $('attachBuildTypeFormDialog');
  },

  formElement: function() {
    return $('attachBuildTypeForm');
  },

  showDialog: function(vcsRootId) {
    this.formElement().vcsRootId.value = vcsRootId;
    this.showCentered();
    this.bindCtrlEnterHandler(this.submit.bind(this));
    $('chooseBuildTypeSelector').fill(vcsRootId);
  },

  submit: function() {
    var idx = this.formElement().buildTypeId.selectedIndex;
    var buildTypeId = this.formElement().buildTypeId.options[idx].value;
    var vcsRootId = this.formElement().vcsRootId.value;
    this.setSaving(true);

    BS.Util.hideSuccessMessages();

    BS.ajaxRequest(BS.AdminActions.url, {
      parameters: "attachVcsRoot=" + vcsRootId + "&buildTypeId=" + buildTypeId,
      onComplete: function() {
        BS.AdminActions._refreshVcsRoot(vcsRootId);
        BS.AttachBuildTypeForm.setSaving(false);
        BS.AttachBuildTypeForm.close();
      }
    });

    return false;
  }
}));

BS.BaseAssignRoleDialog = OO.extend(BS.AbstractModalDialog, {
  getFormElement: function() {},

  getTitle: function() {},

  show: function(rolesHolderIds, oncompletefunction) {
    this.showCentered();
    this.bindCtrlEnterHandler(this.save.bind(this));
    this.getFormElement().role.onchange();
    this._rolesHolderIds = rolesHolderIds;
    this._oncompletefunction = oncompletefunction;

    $(this.getFormElement().id + 'Title').innerHTML = this.getTitle();
  },

  scopeChoosen: function() {
    if (this.scopeChooserAvailable()) {
      var globalScope = this.getFormElement().roleScope[0].checked;
      var projectScope = this.getFormElement().roleScope[1].checked;
      return globalScope || projectScope;
    }

    return true;
  },

  scopeChooserAvailable: function() {
    return this.getFormElement().roleScope.length > 0;
  },

  projectScopeChoosen: function() {
    if (this.getFormElement().roleScope.length > 0) {
      return this.getFormElement().roleScope[1].checked;
    }

    return true;
  },

  save: function() {
    BS.Util.hideSuccessMessages();

    if (!this.scopeChoosen()) {
      alert('Please choose scope of the role.');
      return false;
    }

    if (this.projectScopeChoosen()) {
      var selected = false;
      var options = this.getFormElement().projectId.options;
      for (var i=0; i<options.length; i++) {
        if (options[i].selected) {
          selected = true;
        }
      }

      if (!selected) {
        alert("Please select at least one project.");
        return false;
      }
    }

    if (this.getFormElement().replaceRoles &&
        this.getFormElement().replaceRoles.checked &&
        !confirm("Are you sure you want to completely replace all of the current roles with newly selected roles?")) return false;

    Form.disable(this.getFormElement());
    BS.Util.show('savingRole');

    var additionalParams = "";
    for (i=0; i<this._rolesHolderIds.length; i++) {
      additionalParams += "&rolesHolderId=" + this._rolesHolderIds[i];
    }

    var that = this;
    BS.ajaxRequest(this.getFormElement().action, {
      parameters: BS.Util.serializeForm(this.getFormElement()) + additionalParams,

      onComplete: function() {
        BS.Util.hide(that.getFormElement().id + '_savingRole');
        that._oncompletefunction();
        Form.enable(that.getFormElement());
        that.close();
      }
    });

    return false;
  },

  _projectBasedRoles: {},
  _roles: {},

  roleChanged: function() {
    var selector = this.getFormElement().role;
    var idx = selector.selectedIndex;
    var roleId = selector.options[idx].value;
    this.loadProjects(roleId, this._projectBasedRoles[roleId]);

    $(this.getFormElement().id + '_roleName').innerHTML = selector.options[idx].text;
  },

  loadProjects: function(roleId, projectBasedRole) {
    var selected = {};
    var selector = this.getFormElement().projectId;
    while (selector.options.length > 0) {
      var wasSelected = selector.options[0].selected;
      if (wasSelected) {
        selected[selector.options[0].value] = true;
      }
      selector.options[0] = null;
    }

    var perProjectScope = this.scopeChooserAvailable() ? this.getFormElement().roleScope[1] : null;
    if (!projectBasedRole) {
      selector.options[0] = new Option("This role is not project-based", "", false, false);
      selector.disabled = 'disabled';
      if (perProjectScope) {
        Form.Element.disable(perProjectScope);
        perProjectScope.checked = false;
      }

      if (this.scopeChooserAvailable()) {
        this.getFormElement().roleScope[0].checked = true;
      }

      return;
    } else {
      if (perProjectScope) {
        Form.Element.enable(perProjectScope);
      }

      if (this.scopeChooserAvailable() && !this.getFormElement().roleScope[0].checked) {
        selector.disabled = '';
      }
    }

    var projects = this._roles[roleId].projects;
    var k = 0;
    for (var i=0; i<projects.length; i++) {
      if (projects[i].id == '') continue;
      selector.options[k++] = new Option(projects[i].name, projects[i].id, false, selected[projects[i].id]);
      selector.options[selector.options.length - 1].className = 'inplaceFiltered';
    }
  },

  showRolesDescription: function() {
    var roleSelector = this.getFormElement().role;
    var selectedId = roleSelector.options[roleSelector.selectedIndex].value;
    var url = window['base_uri'] + '/rolesDescription.html?';
    for (var i=0; i<roleSelector.options.length; i++) {
      url += 'role=' + roleSelector.options[i].value + '&';
    }
    url += '#' + selectedId;
    BS.Util.popupWindow(url, "_blank", {width: 550, height: 600});
  }
});

BS.AssignRoleDialog = OO.extend(BS.BaseAssignRoleDialog, {
  getContainer: function() {
    return $('assignRoleDialog');
  },

  getFormElement: function() {
    return $('assignRole');
  },

  getProgressIndicator: function() {
    return $('assignRole_saving');
  },

  getTitle: function() {
    if (this._rolesHolderIds.length > 1) {
      return "Assign roles to " + this._rolesHolderIds.length + " users";
    }
    return "Assign roles";
  }
});

BS.UnassignRoleDialog = OO.extend(BS.BaseAssignRoleDialog, {
  getContainer: function() {
    return $('unassignRoleDialog');
  },

  getFormElement: function() {
    return $('unassignRole');
  },

  getProgressIndicator: function() {
    return $('unassignRole_saving');
  },

  getTitle: function() {
    if (this._rolesHolderIds.length > 1) {
      return "Unassign roles from " + this._rolesHolderIds.length + " users";
    }
    return "Unassign roles";
  }
});

BS.UnassignRolesForm = OO.extend(BS.AbstractWebForm, {
  getFormElement: function() {
    return $('unassignRolesForm');
  },

  selectAll: function(select) {
    if (select) {
      BS.Util.selectAll(this.getFormElement(), "unassign");
    } else {
      BS.Util.unselectAll(this.getFormElement(), "unassign");
    }
  },

  selected: function() {
    var checkboxes = Form.getInputs(this.getFormElement(), "checkbox", "unassign");
    for (var i=0; i<checkboxes.length; i++) {
      if (checkboxes[i].checked) {
        return true;
      }
    }

    return false;
  },

  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('unassignInProgress');
    } else {
      BS.Util.hide('unassignInProgress');
    }
  },

  submit: function() {
    if (!this.selected()) {
      alert("Please select at least one role.");
      return false;
    }

    if (!confirm("Are you sure you want to unassign selected roles?")) return false;

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function(form, responseXML, errStatus) {
        $('authorityRolesContainer').refresh();
      }
    }));

    return false;
  }
});

BS.AvailableParams = {
  url: window['base_uri'] + "/admin/parameterAutocompletion.html",

  attachPopups: function(editableObjId, classNames) {
    var classes = [],
        i;

    for (i = 1; i < arguments.length; i++) {
      classes.push(arguments[i]);
    }
    if ($j.inArray('buildTypeParams', classes) === -1) {
      classes.push('buildTypeParams');
    }

    $j(document).ready(function() {
      $j(classes).each(function(index, className) {
        var selector = '.' + className + ':not(.disableBuildTypeParams):not([type=password])';
        BS.AvailableParams.attachPopupsTo(editableObjId, selector);
      });
    });
  },

  attachPopupsTo: function(editableObjId, jQuerySelector) {
    var completionUrl = BS.AvailableParams.url + '?' + editableObjId;

    var inputFields = $j(jQuerySelector);

    var initAutocomplete = function(inputField) {
      inputField = $j(inputField);
      inputField.autocomplete({minLength: 0,
                                insertItemWithFocus: false,
                                source: BS.AvailableParams.sourceForElement(inputField, completionUrl),
                                select: BS.AvailableParams.selectForElement(inputField),
                                isInCompletionPosition: BS.AvailableParams.createCompletionPositionPredicate(inputField),
                                more: BS.AvailableParams.moreForElement(inputField, completionUrl),
                                completeOnCtrlSpace: true
                              });
    };

    inputFields.each(function() {
      var parentClass = this.parentNode.className;
      if (parentClass.indexOf('completionIconWrapper') == -1 && parentClass.indexOf('posRel') == -1) {
        var inputField = $j(this);
        var wrapper = BS.Util.wrapRelative(inputField);

        if (inputField.prop('style').width == '100%') {
          wrapper.css('display', 'block');
        }
      }

      BS.AvailableParams.showCompletionAvailableImage(this);
    });

    inputFields.on('focus', function(e) {
      // Init the autocomplete component when the field is first focused
      if (!this._autocompleteInited) {
        initAutocomplete(this);
        this._autocompleteInited = true;
      }
    });
  },

  sourceForElement: function(element, autocompletionUrl) {
    element = $j(element);

    return function(request, response) {
      var sliced = BS.AvailableParams.sliceForCompletion(element);
      if (sliced) {
        element.data("ctrlSpace", false);
      } else if (element.data("ctrlSpace")) {
        sliced = BS.AvailableParams.sliceForCtrlSpaceCompletion(element);
      } else if (element.data("showAll")) {
        sliced = BS.AvailableParams.sliceForShowAllCompletion(element);
      }
      element.data("sliced_text", sliced);

      if (sliced !== null) {
        var img = BS.AvailableParams.showLoadingImage(element);
        $j.getJSON(autocompletionUrl + "&term=" + encodeURIComponent(sliced.term), null, function(data) {
          BS.Util.fadeOutAndDelete(img);
          response(data);
        });
      } else {
        response([]);
      }
    };
  },

  selectForElement: function(element) {
    element = $j(element);
    return function(event, ui) {
      var sliced = element.data("sliced_text");
      var position = sliced.before_length + ui.item.value.length + 1;
      this.value = sliced.before + ui.item.value + '%' + sliced.after;
      BS.AvailableParams.setCursorPosition(element, position);
      return false;
    };
  },

  moreForElement: function(element, autocompletionUrl) {
    element = $j(element);
    return function(from, response) {
      var sliced = element.data("sliced_text");
      if (sliced !== null) {
        var img = BS.AvailableParams.showLoadingImage(element);
        $j.getJSON(autocompletionUrl + "&term=" + encodeURIComponent(sliced.term) + "&from=" + encodeURIComponent(from), null, function(data) {
          BS.Util.fadeOutAndDelete(img);
          response(data);
        });
      } else {
        response([]);
      }
    }
  },

  showCompletionAvailableImage: function(elem) {
    if (elem.className && elem.className.indexOf('__popupAttached') != -1) return;

    var elemId = elem.id,
        imgId = elemId + "_params";

    var img = this.getOrCreateCompletionImage().clone(true);

    img.attr({
      'id': imgId
    });

    elem.className += ' __popupAttached';
    elem.style.marginRight = '20px';

    img.appendTo(elem.parentNode);

    var handler = {
      updateVisibility: function() {
        var control = $(elemId);

        if (control.disabled || control.readOnly) {
          BS.Util.hide(imgId);
        } else {
          var img = $(imgId);
          if (img != null) {
            var dim = control.getDimensions(),
                layout = control.getLayout();

            var xshift = dim.width + 5;
            var pos = control.positionedOffset();

            var x = pos[0] + xshift + layout.get('margin-left');
            var y = pos[1] + 6 + layout.get('margin-top');
            BS.Util.show(img);
            BS.Util.place(img, x, y);
          }
        }
      }
    };
    handler.updateVisibility();
    BS.VisibilityHandlers.attachTo(elemId, handler);
  },

  _completionImage: null,
  getOrCreateCompletionImage: function() {
    function createCompletionImage() {
      var img = $j('<img/>');

      img.attr({
        'src': window['base_uri'] + '/img/paramsPopup.gif',
        'title': 'Parameter references are supported in this field, type % to see available parameters'
      });

      img.css({
        'position': 'absolute',
        'display': 'none',
        'cursor': 'pointer'
      });

      img.on('mouseover', function() {
        this.src = window['base_uri'] + '/img/paramsPopupHover.gif';
      });

      img.on('mouseout', function() {
        this.src = window['base_uri'] + '/img/paramsPopup.gif';
      });

      img.on('click', function() {
        $j(this)
            .prevAll('input, textarea')
            .focus()
            .data('showAll', true)
            .autocomplete('search');
      });

      return img;
    }

    this._completionImage = this._completionImage || createCompletionImage();

    return this._completionImage;
  },

  showLoadingImage: function(element) {
    element = $j(element);
    var position = element.position();
    var imgId = element.attr('id') + '_completion_img';
    return $j("<img>")
        .attr({
          id: imgId,
          src: BS.loadingUrl
        })
        .css({
          width: 16,
          height: 16,
          position: 'absolute',
          left: position.left + element.outerWidth(true) - 40, // 20px of right margin plus 20px of offset
          top: position.top + 2
        })
        .insertAfter(element);
  },

  /* Use elementId instead of element or jQuery wrapper because sometimes IE throw exception
  * when range.moveToElement() is called with DOM element not from rendered DOM tree. So we
  * always get element using document.getElementById()*/
  getCursorPosition: function(elementId) {
    var element = document.getElementById(elementId);
    var start = element.selectionStart;
    var range, stored_range, newline_counter = 0;
    if (start || start === 0) {
      return start;
    } else if (document.selection && document.selection.createRangeCollection) {//IE
      range = document.selection.createRange();
      if (range === null) {
        return 0;
      } else {
        stored_range = range.duplicate();
        if (element.tagName.toLowerCase() === "textarea") {
          stored_range.moveToElementText(element);
          stored_range.setEndPoint('EndToEnd', range);
          var result = stored_range.text.length - range.text.length;
          var text = element.value;
          //IE does not take newline character into account
          for (var i = 0; i < result; i++) {
            if (text.charAt(i) === '\n') {
              newline_counter++;
            }
          }
          return result - newline_counter;
        } else {
          start = 0 - stored_range.moveStart('character', -100000);
          return start + range.text.length;
        }
      }
    } else {
      return 0;
    }
  },

  setCursorPosition: function(element, position) {
    if (!BS.Browser.msie) {
      $j(element).attr({
        'selectionStart': position,
        'selectionEnd': position
      });
    }
  },

  createCompletionPositionPredicate: function(element) {
    element = $j(element);
    return function(event) {
      var text = element.val(),
          cursor = BS.AvailableParams.getCursorPosition(element.attr('id')),
          percent_count = 0,
          percent_pressed = event.shiftKey && event.which === 53;

      for (var i = 0; i < cursor; i++) {
        if (text.charAt(i) === '%') {
          percent_count++;
        }
      }

      if (percent_count % 2 === 0) {
        //all percents are closed, we are in a completion position if '%' was pressed
        return percent_pressed;
      } else {
        //there is unclosed percent, we are in a completion position if '%' was not pressed
        return !percent_pressed;
      }
    };
  },

  /* returns {before: //text before term for completion or empty string
                    before_length: //length of the prefix - to not calculate it twice
                    term: //term for completion
                    after: //text after term for completion or empty string
                   } or null if cursor is not in the completion position*/
  sliceForCompletion: function(element) {
    element = $j(element);

    var text = element.val(),
        cursor = BS.AvailableParams.getCursorPosition(element.attr('id')),
        term_start,
        last_percent, percent_count;

    percent_count = 0;
    last_percent = -1;
    for (var i = 0; i < cursor; i++) {
      if (text.charAt(i) === '%') {
        last_percent = i;
        percent_count++;
      }
    }
    if (percent_count % 2 === 0) {
      //all opened references already closed, nothing to complete
      return null;
    } else if (last_percent === -1) {
      //no '%' found
      return null;
    } else {
      term_start = last_percent;
      return {before: text.slice(0, term_start+1),
              before_length: term_start+1,
              term: text.slice(term_start+1, cursor),
              after: text.slice(cursor, text.length)};
    }
  },

  sliceForCtrlSpaceCompletion: function(element) {
    element = $j(element);

    var text = element.val(),
        cursor = BS.AvailableParams.getCursorPosition(element.attr('id')),
        i, c;

    for (i = cursor-1; i >= 0; i--) {
      c = text.charAt(i);
      if (c === ' ' || c === '\n' || c === '\t') {
        i++;
        break;
      }
    }
    if (i === -1) {
      i = 0;
    }

    return {before: text.slice(0, i) + '%',
            before_length: i + 1,
            term: text.slice(i, cursor),
            after: text.slice(cursor, text.length)};
  },

  sliceForShowAllCompletion: function(element) {
    element = $j(element);

    var text = element.val(),
        cursor = BS.AvailableParams.getCursorPosition(element.attr('id')),
        i, c;

    for (i = cursor-1; i >= 0; i--) {
      c = text.charAt(i);
      if (c === ' ' || c === '\n' || c === '\t') {
        i++;
        break;
      }
    }
    if (i === -1) {
      i = 0;
    }

    return {before: text.slice(0, cursor) + '%',
            before_length: cursor,
            term: '',
            after: text.slice(cursor, text.length)};
  }
};