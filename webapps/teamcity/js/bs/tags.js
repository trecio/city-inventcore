/*
 * Copyright (c) 2006, JetBrains, s.r.o. All Rights Reserved.
 */

BS.Tags = OO.extend(BS.AbstractModalDialog, {
  
  getContainer: function() {
    return $('editTagsFormDialog');
  },

  showEditDialog: function(buildId, escapedTags) {
    var form = $('editTagsForm');
    form.editTagsForBuild.value = buildId;
    form.buildTagsInfo.value = escapedTags;
    this.showCentered();

    $(form.buildTagsInfo).activate();

    this.bindCtrlEnterHandler(this.submitTags.bind(this));
  },
  
  appendTag: function(tag) {
    BS.Util.addWordToTextArea($('editTagsForm').buildTagsInfo, tag);
  },
  
  submitTags: function(){
    var f = $('editTagsForm');
    Form.disable(f);
    BS.Util.show('savingTags');
    BS.ajaxRequest(f.action, {
      parameters: BS.Util.serializeForm(f),
      onComplete: function() {
        BS.Util.hide('savingTags');
        BS.Tags.close();
        BS.reload(true);
        Form.enable(f);
      }
    });

    return false;
  }
});
