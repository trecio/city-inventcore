
BS.CreateUserForm = OO.extend(BS.AbstractPasswordForm, {
  submitCreateUser: function() {
    var that = this;

    BS.PasswordFormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onDuplicateAccountError: function(elem) {
        $("errorUsername1").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("username1"));
      },

      onEmptyUsernameError: function(elem) {
        $("errorUsername1").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("username1"));
      },

      onPasswordsMismatchError: function(elem) {
        $("errorPassword1").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("password1"));
        that.highlightErrorField($("retypedPassword"));
      },

      onEmptyPasswordError: function(elem) {
        $("errorPassword1").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("password1"));
      },

      onMaxNumberOfUserAccountsReachedError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        if (!err) {
          BS.XMLResponse.processRedirect(responseXML);
        }
      }
    }));

    return false;
  }
});

BS.AdminCreateUserForm = OO.extend(BS.CreateUserForm, {
  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('saving1');
    } else {
      BS.Util.hide('saving1');
    }
  }
});
