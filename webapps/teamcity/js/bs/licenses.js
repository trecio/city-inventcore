BS.LicensesForm = {

  showEmptyForm: function() {
    BS.Util.show("licensesForm");
    $("licenseKeys").value = '';
    $("licenseKeys").focus();
  },

  submitLicenses: function() {
    var that = this;
    var form = document.forms[0];
    Form.disable(form);
    $("submitKeys").value = "Save";

    BS.ajaxRequest(form.action, {
      method: "post",

      parameters: BS.Util.serializeForm(form),

      onComplete: function(transport) {
        if (!BS.XMLResponse.processRedirect(transport.responseXML)) {
          try {
            var doc = transport.responseXML;
            var verificationResult = doc.getElementsByTagName("verificationResult")[0];
            if (verificationResult) {
              var html = "<table class='verificationResults' width='100%'>";
              var licenses = verificationResult.getElementsByTagName("licenseKey");
              if (licenses.length > 0) {
                for (var i=0; i<licenses.length; i++) {
                  var key = licenses[i].getElementsByTagName("key")[0].firstChild.nodeValue;
                  var valid = "true" == licenses[i].getAttribute("valid");
                  html += "<tr><td valign='top' width='50%'>" + key + ":</td>";
                  if (valid) {
                    html += "<td valign='top'><span class='validKey'>Valid license key</span>";
                  } else {
                    var reason = licenses[i].getElementsByTagName("invalidReason")[0].firstChild.nodeValue;
                    html += "<td valign='top'><span class='invalidKey'>" + reason + "</span></td>";
                  }
                }

                $("verificationStatus").innerHTML = html;
              }
            }

            Form.enable(form);
            $("licenseKeys").focus();
          } catch(ex) {
            BS.reload(true);
          }
        }
      },

      onException: function(obj, e) {
        BS.Util.processError(e);
      },

      onFailure: function() {
        alert("Error accessing server");
      }
    })
  },

  removeLicenseKey: function(key) {
    if (!confirm("Are you sure you want to remove this license key?")) return;

    BS.ajaxRequest('licenses.html', {
      parameters: "removeKey=" + key,
      onComplete: function(transport) {
        $('availableLicenses').refresh();
      }
    })
  }
};
