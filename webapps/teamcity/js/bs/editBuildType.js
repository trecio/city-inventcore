
BS.SaveGeneralBuildTypeInfoListener = OO.extend(BS.SaveConfigurationListener, {
  _invalidBuildTypeNameError: function(elem) {
    if ($("errorName")) {
      $("errorName").innerHTML = elem.firstChild.nodeValue;
      this.getForm().highlightErrorField($("name"));
    } else {
      alert(elem.firstChild.nodeValue);
    }
  },

  onEmptyNameError: function(elem) {
    this._invalidBuildTypeNameError(elem);
  },

  onInvalidNameError: function(elem) {
    this._invalidBuildTypeNameError(elem);
  },

  onCannotRenameError: function(elem) {
    this._invalidBuildTypeNameError(elem);
  },

  onInvalidBuildNumberFormatError: function(elem) {
    if ($("errorBuildNumberFormat")) {
      $("errorBuildNumberFormat").innerHTML = elem.firstChild.nodeValue;
      this.getForm().highlightErrorField($("buildNumberFormat"));
    } else {
      alert(elem.firstChild.nodeValue);
    }
  },

  onInvalidBuildCounterError: function(elem) {
    if ($("errorBuildCounter")) {
      $("errorBuildCounter").innerHTML = elem.firstChild.nodeValue;
      this.getForm().highlightErrorField($("buildCounter"));
    } else {
      alert(elem.firstChild.nodeValue);
    }
  },

  onInvalidTimeoutValueError: function(elem) {
    if ($("errorExecutionTimeout")) {
      $("errorExecutionTimeout").innerHTML = elem.firstChild.nodeValue;
      this.getForm().highlightErrorField($("executionTimeout"));
    } else {
      alert(elem.firstChild.nodeValue);
    }
  },

  onInvalidMaxBuildsValueError: function(elem) {
    if ($("errorMaxBuilds")) {
      $("errorMaxBuilds").innerHTML = elem.firstChild.nodeValue;
      this.getForm().highlightErrorField($("maxBuilds"));
    } else {
      alert(elem.firstChild.nodeValue);
    }
  }
});

BS.CreateBuildTypeForm = OO.extend(BS.PluginPropertiesForm, {
  back: function() {
    BS.PasswordFormSaver.save(this, this.formElement().action + "&back=1", OO.extend(BS.SaveConfigurationListener, {
      onCompleteSave: function(form, responseXML, err) {
        BS.XMLResponse.processRedirect(responseXML);
      }
    }));

    return false;
  },

  forward: function() {
    var that = this;

    BS.PasswordFormSaver.save(this, this.formElement().action, OO.extend(BS.SaveGeneralBuildTypeInfoListener, {
      getForm: function() {
        return that;
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.SaveGeneralBuildTypeInfoListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          BS.XMLResponse.processRedirect(responseXML);
        }
      }
    }));

    return false;
  },

  submitCreateBuildType: function() {
    var that = this;

    BS.PasswordFormSaver.save(this, this.formElement().action, OO.extend(BS.SaveGeneralBuildTypeInfoListener, {
      onCompleteSave: function(form, responseXML, err) {
        BS.SaveGeneralBuildTypeInfoListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          if (!BS.XMLResponse.processRedirect(responseXML)) {
            that.reloadRunnerForm(form);
          }
        }
      }
    }));

    return false;
  },

  reloadRunnerForm: function(form) {
    form.setSaving(false);
    form.enable();
    var select = $('runnerType');
    $('runnerParams').updateContainer(select.options[select.selectedIndex].value);
  }
});

BS.BaseEditBuildTypeForm = OO.extend(BS.AbstractWebForm, {
  setTemplateBased: function(exceptions, keepUpdateStateHandlers) {
    this.templateBased = true;
    this._formDisabled = false; // always disable form

    var excludedClasses = {};
    var excludedNames = {};

    if (exceptions != null) {
      for (var i = 0; i < exceptions.length; i++) {
        var className = exceptions[i].className;
        var name = exceptions[i].name;
        if (className != null) {
          excludedClasses[className] = true;
        }
        if (name != null) {
          excludedNames[name] = true;
          excludedNames[BS.jQueryDropdown.namePrefix + name] = true;
          excludedNames[name + BS.jQueryDropdown.namePrefix] = true;
          excludedClasses[BS.jQueryDropdown.namePrefix + name] = true;
          excludedClasses[name + BS.jQueryDropdown.namePrefix] = true;
        }
      }
    }

    var filter = function(elem) {
      if (excludedNames[elem.name]) return false;
      var classes = elem.className.split(' ');
      for (var i=0; i<classes.length; i++) {
        if (excludedClasses[BS.Util.trimSpaces(classes[i])]) return false;
      }
      return true;
    };

    var disabled = this.disable(filter);

    for (i=0; i<disabled.length; i++) {
      disabled[i]._inherited = true;
    }

    if (!keepUpdateStateHandlers) {
      this.removeUpdateStateHandlers();
    }
  },

  enable: function() {
    var filter = function(elem) {
      return !elem._inherited;
    };
    BS.AbstractWebForm.enable.call(this, filter);
  }
});

BS.EditBuildTypeForm = OO.extend(BS.BaseEditBuildTypeForm, {
  formElement: function() {
    return $('editBuildTypeForm');
  },

  setupEventHandlers: function() {
    var that = this;
    this.setUpdateStateHandlers({
      updateState: function() {
        that.saveInSession();
      },

      saveState: function() {
        that.submitBuildType();
      }
    });
  },

  saveInSession: function() {
    $("submitBuildType").value = 'storeInSession';

    BS.PasswordFormSaver.save(this, this.formElement().action, BS.StoreInSessionListener);
  },

  submitBuildType: function() {
    var that = this;
    $("submitBuildType").value = 'store';

    BS.PasswordFormSaver.save(this, this.formElement().action, OO.extend(BS.SaveGeneralBuildTypeInfoListener, {
      getForm: function() {
        return that;
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.SaveGeneralBuildTypeInfoListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          BS.reload(true);
        }
      }
    }), false);

    return false;
  }
});

BS.EditVcsRootsForm = OO.extend(BS.BaseEditBuildTypeForm, {
  formElement: function() {
    return $('editVcsSettingsForm');
  },

  attachExistingVcsRoot: function() {
    var value = $('vcsRootId').options[$('vcsRootId').selectedIndex].value;
    if (value == '') return;

    $('doAttach').value = "true";
    $("submitBuildType").value = 'store';

    var that = this;
    BS.Util.show('attachProgress');
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SaveConfigurationListener, {
      onVcsRootNotFoundError: function(elem) {
        $("errorVcsRootId").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("vcsRootId"));
      },

      onInvalidVcsRootScopeError: function(elem) {
        $("errorVcsRootId").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("vcsRootId"));
      },

      onCompleteSave: function(form, responseXML, err) {
        $('doAttach').value = "false";
        BS.Util.hide('attachProgress');
        BS.SaveConfigurationListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          BS.EditVcsRootsForm.update();
        }
      }
    }))
  },

  detachVcsRoot: function(vcsRootId) {
    if (confirm("Are you sure you want to detach this VCS root?")) {
      BS.ajaxRequest(this.formElement().action, {
        parameters: "detachVcsRoot=" + vcsRootId,
        onComplete: function() {
          BS.EditVcsRootsForm.update();
        }
      })
    }
  },

  setupEventHandlers: function() {
    var that = this;
    this.setUpdateStateHandlers({
      updateState: function() {
        that.saveInSession();
      },

      saveState: function() {
        that.applyVcsSettings();
      }
    });
  },

  saveInSession: function() {
    $("submitBuildType").value = 'storeInSession';

    BS.FormSaver.save(this, this.formElement().action, BS.StoreInSessionListener);
  },

  applyVcsSettings: function() {
    $("submitBuildType").value = 'store';
    $('doApplyVcsSettings').value = 'true';
    BS.Util.show('applyVcsSettingsProgress');
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SaveConfigurationListener, {
      onCompleteSave: function(form, responseXML, err) {
        $('doApplyVcsSettings').value = 'false';
        BS.Util.hide('applyVcsSettingsProgress');
        BS.SaveConfigurationListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          BS.EditVcsRootsForm.update();
        }
      }
    }));

    return false;
  },

  updateCheckoutDirectoryWarning: function() {
    var show = $('cleanBuild').checked;
    if (show) {
      $('deletionWarning').innerHTML = $('deletionWarning').innerHTML.gsub("might be", "will be");
    }
    else {
      show = $('checkoutType').selectedIndex < 2;
      $('deletionWarning').innerHTML = $('deletionWarning').innerHTML.gsub("will be", "might be");
    }

    if (BS.Util.trimSpaces($('checkoutDir').value) == '') {
      show = false;
    }
    $('deletionWarning').style.display = show ? 'block' : 'none';

    BS.VisibilityHandlers.updateVisibility('mainContent');
  },

  update: function() {
    this.enable();
    this.removeUpdateStateHandlers();
    $('vcsRootsBody').refresh("", "", function() {
      BS.EditVcsRootsForm.updateCheckoutDirectoryWarning();
    });
  },

  updateCheckoutSharing: function() {
    var customCheckout = $j('#customCheckout'),
        pathField = $j("#checkoutDir"),
        autoCheckout = $j("#autoCheckout");

    if (customCheckout.is(":visible")) {
      this._checkoutDir = pathField.val();
      pathField.val("");
    } else {
      pathField.val(this._checkoutDir || "");
    }

    customCheckout.toggle();
    autoCheckout.toggle();
    BS.VisibilityHandlers.updateVisibility(pathField[0]);
  }
});


BS.EditBuildRunnerForm = OO.extend(BS.BaseEditBuildTypeForm, OO.extend(BS.PluginPropertiesForm, {
  formElement: function() {
    return $('editBuildTypeForm');
  },

  setupCtrlEnterForTextareas: function(settingsId) {
    var that = this;
    $j('#editBuildTypeForm textarea').each(function() {
      var ctrl = false;
      $j(this).keydown(function(event) {
        if (event.keyCode === $j.ui.keyCode.CONTROL) {
          ctrl = true;
        } else if (ctrl && event.keyCode === $j.ui.keyCode.ENTER) {
          that.submitBuildRunner(settingsId)
        }
      }).keyup(function(event) {
        if (event.keyCode === $j.ui.keyCode.CONTROL) {
          ctrl = false;
        }
      });
    });
  },

  setupEventHandlers: function(settingsId) {
    var that = this;

    this.setUpdateStateHandlers({
      updateState: function() {
        that.saveInSession();
      },

      saveState: function() {
        that.submitBuildRunner(settingsId);
      }
    });
  },

  saveInSession: function() {
    $("submitBuildType").value = 'storeInSession';

    BS.PasswordFormSaver.save(this, this.formElement().action, BS.StoreInSessionListener);
  },

  submitBuildRunner: function(settingsId) {
    var that = this;
    $("submitBuildType").value = 'store';

    BS.PasswordFormSaver.save(this, this.formElement().action, OO.extend(BS.SaveGeneralBuildTypeInfoListener, {
      onCompleteSave: function(form, responseXML, err) {
        that.setSaving(false);
        var wereErrors = BS.XMLResponse.processErrors(responseXML, {}, that.propertiesErrorsHandler) || err;

        if (wereErrors) {
          that.enable();
          that.focusFirstErrorField();
        } else {
          document.location.href = 'editBuildRunners.html?init=1&id=' + settingsId;
        }
      }
    }));

    return false;
  }
}));

BS.RequirementsForm = OO.extend(BS.BaseEditBuildTypeForm, {
  formElement: function() {
    return $('editRequirement');
  },

  saveRequirement: function() {
    this.clearErrors();

    $('submitAction').value = 'updateRequirement';
    var that = this;

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.BaseSaveParameterListener, {
      onEmptyParameterNameError: function(elem) {
        $("error_parameterName").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($('parameterName'));
      },

      onEmptyParameterValueError: function(elem) {
        $("error_parameterValue").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($('parameterValue'));
      },

      onInvalidParameterValueError: function(elem) {
        $("error_parameterValue").innerHTML = elem.firstChild.nodeValue.replace(/(\n\n)+/g, '<br/>').replace(/ /g, '&nbsp;'); // workaround for regex errors
        that.highlightErrorField($('parameterValue'));
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.SaveConfigurationListener.onCompleteSave(form, responseXML, err);
        if (!err) {
          BS.reload(true);
        }
      }
    }));

    return false;
  },

  doRemoveRequirement:function (paramId) {
    var url = this.formElement().action + "&submitBuildType=1&submitAction=removeRequirement&removedReqParameterId=" + paramId;

    BS.ajaxRequest(url, {
      onComplete: function() {
        BS.reload(true);
      }
    });

  },

  removeRequirement: function(paramId) {
    if (!confirm("Are you sure you want to delete this requirement?")) return;
    this.doRemoveRequirement(paramId);
  },

  resetRequirement: function(paramId) {
    if (!confirm("Are you sure you want to reset this requirement to default?")) return;
    this.doRemoveRequirement(paramId);
  }
});


BS.EditRequirementDialog = OO.extend(BS.AbstractModalDialog, {

  parameterValueRequired: {},

  setParameterValueRequired: function(settings) {
    this.parameterValueRequired = settings;
  },

  createFindNameFunction: function(baseUrl) {
    var that = this;
    return function(request, response) {
      var term = request.term;
      var url = baseUrl + '?what=name&term=' + encodeURIComponent(term);
      $j.getJSON(url, function(data) {
        BS.Util.fadeOutAndDelete(BS.Util.escapeId(that.getLoadingImageId('parameterName')));
        response(data);
      });
    }
  },

  createFindValueFunction: function(baseUrl) {
    var that = this;
    return function(request, response) {
      var term = request.term;
      var parameterName = $j("#parameterName").val();
      var url = baseUrl + '?what=value&term=' + encodeURIComponent(term) + '&name=' + encodeURIComponent(parameterName);
      $j.getJSON(url, function(data) {
        BS.Util.fadeOutAndDelete(BS.Util.escapeId(that.getLoadingImageId('parameterValue')));
        response(data);
      });
    }
  },

  getLoadingImageId: function(fieldId) {
    return $j(BS.Util.escapeId(fieldId)).attr('id') + '_compl_loading_img';
  },

  showLoadingImage: function(fieldjQuerySelector) {
    var that = this;
    return function () {
      var position = $j(fieldjQuerySelector).position();
      var imgId = that.getLoadingImageId(fieldjQuerySelector);
      return $j("<img>")
          .attr("id", imgId)
          .attr("src", BS.loadingUrl)
          .css({width:    16,
                height:   16,
                position: 'absolute',
                left:     position.left + $j(fieldjQuerySelector).outerWidth(true) + 2,
                top:      position.top + 2})
          .insertAfter(fieldjQuerySelector);
    };
  },

  showSlowly: function(query) {
    return function(event, ui) {
      $j(query).fadeIn('slow', function() {});
    };
  },

  getContainer: function() {
    return $('editRequirementDialog');
  },

  showDialog: function(name, value, reqType, inherited) {
    BS.RequirementsForm.enable();
    BS.RequirementsForm.clearErrors();

    $('currentName').value = name;
    $('parameterName').value = name;
    $('parameterValue').value = value;
    this.selectRequirement($('requirementType'), reqType);

    $('editRequirementTitle').innerHTML = name.length == 0 ? "Add Requirement" : "Edit Requirement";

    $j("#parameterName").autocomplete("readTerm");
    $j("#parameterValue").autocomplete("readTerm");
    $j("#parameterValue").autocomplete({close: function(event, ui) {
      $j('#parameterValue').placeholder("refresh");
    }});
    this.showCentered();
    $j('#parameterName').placeholder("refresh");
    $j('#parameterValue').placeholder("refresh");
    if (inherited) {
      Form.Element.disable($('parameterName'));
      $('inheritedParamName').show();
    } else {
      Form.Element.enable($('parameterName'));
      $('inheritedParamName').hide();
      $('parameterName').focus();
    }
  },

  selectRequirement: function (requirementTypeElement, type) {
    var i;
    for (i = 0; i < requirementTypeElement.options.length; i++) {
      if (requirementTypeElement.options[i].value === type) {
        requirementTypeElement.selectedIndex = i;
        BS.jQueryDropdown(requirementTypeElement).ufd("changeOptions");
        this.requirementChanged(requirementTypeElement);
        return;
      }
    }
    requirementTypeElement.selectedIndex = 0;
    requirementTypeElement.value = requirementTypeElement.options[0].value;
    this.requirementChanged(requirementTypeElement);
  },

  requirementChanged: function(requirementTypeElement) {
    if (this.parameterValueRequired[requirementTypeElement.value]) {
      BS.Util.show('editParameterValue');
    } else {
      BS.Util.hide('editParameterValue');
    }
    $j('#parameterValue').placeholder("refresh");
  },

  cancelDialog: function() {
    this.close();
    BS.Util.hide("parameterNameAutocompleteLoading");
    BS.Util.hide("parameterValueAutocompleteLoading");
  },

  afterClose: function() {
    $j("#parameterName").autocomplete("close");
    $j("#parameterValue").autocomplete("close");
  }
});

BS.FrequentlyUsedReqsDialog = OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('frequentlyUsedReqsDialog');
  },

  formElement: function() {
    return $('frequentlyUsedReqs');
  },

  showDialog: function(elem) {
    this.showCentered();
  },

  cancelDialog: function() {
    this.close();
  },

  addRequirement: function(name, reqType, value, progressId) {
    var formElement = this.formElement();
    formElement.parameterName.value = name;
    formElement.parameterValue.value = value;
    formElement.requirementType.value = reqType;

    Form.disable(formElement);
    BS.Util.show(progressId);

    BS.ajaxRequest(formElement.action, {
      parameters : BS.Util.serializeForm(formElement),
      onComplete: function() {
        BS.Util.hide(progressId);
        BS.reload(true);
      }
    });

    return false;
  }
});

BS.ArtifactDependencyForm = OO.extend(BS.DialogWithProgress, OO.extend(BS.BaseEditBuildTypeForm, {
  getContainer: function() {
    return $('artifactDependencyFormDialog');
  },

  formElement: function() {
    return $('artifactDependencyForm');
  },

  removeDependencies: function(indexes) {
    if (!BS.EditDepsUtil.confirmDepsRemove(indexes.length, "remove")) return;

    this.hideSuccessMessages();
    this.setSaving(true);
    BS.ajaxRequest(this.formElement().action, {
      parameters: "artDepDelete=" + indexes.join("&artDepDelete="),
      onSuccess: function() {
        BS.ArtifactDependencyForm.reloadDependencies();
      }
    });
  },

  refreshDialog: function() {
    $('artifactDependencyDialog').refresh(null, "", function() {
      BS.ArtifactDependencyForm.showCentered();
    });
  },

  dependenciesSelected: function() {
    return BS.EditDepsUtil.itemsSelected($('artifactDeps'), "depIndex");
  },

  getSelectedDeps: function() {
    return BS.EditDepsUtil.getSelectedItems($('artifactDeps'), "depIndex");
  },

  editDependency: function(event, idx) {
    if (BS.EditDepsUtil.eventTargetIsLink(event)) return true;

    this.hideSuccessMessages();
    this.showProgress(event);
    BS.ajaxRequest(this.formElement().action, {
      parameters: "artDepEdit=" + idx,
      onSuccess: function() {
        BS.ArtifactDependencyForm.refreshDialog();
      }
    });
  },

  addDependency: function(event) {
    this.hideSuccessMessages();
    this.showProgress(event);
    BS.ajaxRequest(this.formElement().action, {
      parameters: "artDepAdd=1",
      onSuccess: function() {
        BS.ArtifactDependencyForm.refreshDialog();
      }
    });
  },

  reloadDependencies: function() {
    BS.reload(true);
  },

  updateTargetDirectoryWarning: function() {
    if ($('cleanDestination')) {
      var show = $('cleanDestination').checked;
      $('deletionWarning').style.display = show ? 'block' : 'none';
    }
  },

  _counter: 0,

  addArtifactPath: function() {
    var item = document.createElement("li");
    item.innerHTML = $('artifactPathTemplate').innerHTML;
    var input = item.getElementsByTagName("input")[0];
    input.name = 'artifactPath';
    item.style.display = 'block';
    input.className += ' buildTypeParams';
    input.id = 'artifactPath_new_' + (this._counter++);
    $('artifactsPaths').insertBefore(item, $('artifactsPaths_lastItem'));

    BS.VisibilityHandlers.updateVisibility(this.getContainer());
  },

  savingIndicator: function() {
    return $('saveArtifactDependencyProgress');
  },

  saveDependency: function() {
    this.formElement().revisionRuleName.value = $('revisionRules').options[$('revisionRules').selectedIndex].value;
    this.formElement().revisionRuleValue.value = '';
    if (!$('buildNumberPattern').disabled) {
      this.formElement().revisionRuleValue.value = $('buildNumberPattern').value;
    } else if (!$('buildTag').disabled) {
      this.formElement().revisionRuleValue.value = $('buildTag').value;
    }

    var that = this;

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onEmptyBuildNumberPatternError: function(elem) {
        $("errorBuildNumberPattern").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("buildNumberPattern"));
      },

      onEmptyBuildTagError: function(elem) {
        $("errorBuildTag").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("buildTag"));
      },

      onNoArtifactsPathsError: function(elem) {
        $("errorArtifactsPaths").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("artifactsPaths").getElementsByTagName("textarea")[0]);
      },

      onDuplicateArtifactsPathsError: function(elem) {
        $("errorArtifactsPaths").innerHTML = elem.firstChild.nodeValue;
      },

      onBadDestinationPathError: function(elem) {
        $("errorArtifactsPaths").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("artifactsPaths").getElementsByTagName("textarea")[0]);
      },

      onSimilarDependencyExistsError: function(elem) {
        $("errorSimilarDependencyExists").innerHTML = elem.firstChild.nodeValue;
      },

      onBuildTypeNotExistsError: function(elem) {
        $("errorSourceBuildTypeNotExists").innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField($("sourceBuildTypeId"));
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        if (!err) {
          BS.ArtifactDependencyForm.close();
          form.enable();
          BS.ArtifactDependencyForm.reloadDependencies();
        }
      }
    }));

    return false;
  }
}));

BS.ArtifactDependencyVerificationForm = OO.extend(BS.BaseEditBuildTypeForm, {
  _password: null,

  formElement: function() {
    return $('verifyDependenciesForm');
  },

  verifyDependencies: function() {
    var that = this;

    BS.EnterCredentialsDialog.showDialog(function(formElement) {
      var username = formElement.username1.value;
      var encrypted = formElement.password1.getEncryptedPassword();

      BS.EnterCredentialsDialog.disable();
      BS.EnterCredentialsDialog.setSaving(true);
      BS.ajaxRequest(that.formElement().action, {
        parameters: "artDepVerifyDependencies=1&encryptedPassword=" + encrypted + "&username=" + username,
        onSuccess: function(transport) {
          BS.EnterCredentialsDialog.enable();
          BS.EnterCredentialsDialog.setSaving(false);

          var hasError = BS.XMLResponse.processErrors(transport.responseXML, {
            onInvalidCredentialsError: function(elem) {
              $('invalidCredentials').innerHTML = elem.firstChild.nodeValue;
            }
          });

          if (!hasError) {
            BS.EnterCredentialsDialog.close();
            BS.EnterCredentialsDialog.formElement().password1.maskPassword();
            that.disable();
            that.setSaving(true);
            $('dependencyResolverMessages').refresh();
          }
        }
      })
    });

    return false;
  },

  verificationFinished: function() {
    this.enable();
    this.setSaving(false);
  }
});

BS.EnterCredentialsDialog = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  formElement: function() {
    return $('enterCredentials');
  },

  getContainer: function() {
    return $('enterCredentialsDialog');
  },

  showDialog: function(onSubmit) {
    this.showCentered();
    this.clearErrors();
    this._onSubmit = onSubmit;

    this.bindCtrlEnterHandler(this.submit.bind(this));
  },

  submit: function() {
    this._onSubmit(this.formElement());
    return false;
  },

  clearErrors: function() {
    $('invalidCredentials').innerHTML = "";
  },

  savingIndicator: function() {
    return $('submitCredentials');
  }
}));

BS.EditDepsUtil = {
  eventTargetIsLink: function(event) {
    var target = Event.element(event || window.event);
    return target.tagName == 'A' && target.innerHTML != 'edit';
  },

  itemsSelected: function(container, itemName) {
    if (BS.EditDepsUtil.getSelectedItems(container, itemName).length == 0) {
      alert("Please select at least one dependency.");
      return false;
    }

    return true;
  },

  getSelectedItems: function(container, itemName) {
    var values = [];
    var checkboxes = Form.getInputs(container, "checkbox", itemName);
    for (var i=0; i<checkboxes.length; i++) {
      if (checkboxes[i].checked) {
        values.push(checkboxes[i].value);
      }
    }

    return values;
  },

  confirmDepsRemove: function(numDeps, action) {
    return confirm("Are you sure you want to " + action + " " + numDeps + " dependenc" + (numDeps == 1 ? 'y' : 'ies') + "?");
  }
};

BS.SourceDependencyForm = OO.extend(BS.DialogWithProgress, OO.extend(BS.BaseEditBuildTypeForm, {
  getContainer: function() {
    return $('sourceDependenciesDialog');
  },

  showDialog: function() {
    this.showCentered();
    this.bindCtrlEnterHandler(this.saveDependency.bind(this));
  },

  formElement: function() {
    return $('sourceDependencies');
  },

  savingIndicator: function() {
    return $('addSourceDependencyProgress');
  },

  dependenciesSelected: function() {
    return BS.EditDepsUtil.itemsSelected($('snapshotDeps'), "snDepChkbox");
  },

  getSelectedDeps: function() {
    return BS.EditDepsUtil.getSelectedItems($('snapshotDeps'), "snDepChkbox");
  },

  removeDependencies: function(buildTypeIds, action) {
    if (!BS.EditDepsUtil.confirmDepsRemove(buildTypeIds.length, action)) return;

    this._doRemoveDependencies(buildTypeIds);
  },

  _doRemoveDependencies: function(buildTypeIds) {
    this.hideSuccessMessages();
    this.setSaving(true);
    BS.ajaxRequest(this.formElement().action, {
      parameters: "srcDepDelete=" + buildTypeIds.join("&srcDepDelete="),
      onSuccess: function() {
        BS.reload(true);
      }
    });
  },

  editDependency: function(event, buildTypeId) {
    if (BS.EditDepsUtil.eventTargetIsLink(event)) return true;

    this.hideSuccessMessages();
    this.showProgress(event);
    BS.ajaxRequest(this.formElement().action, {
      parameters: "srcDepEdit=" + buildTypeId,
      onSuccess: function() {
        BS.SourceDependencyForm.refreshDialog();
      }
    });
  },

  addDependency: function(event, selectedBuildTypes) {
    this.hideSuccessMessages();
    this.showProgress(event);
    BS.ajaxRequest(this.formElement().action, {
      parameters: "srcDepAdd=1" + (selectedBuildTypes ? "&selectedBuildTypes=" + selectedBuildTypes.join("&selectedBuildTypes=") : ""),
      onSuccess: function() {
        BS.SourceDependencyForm.refreshDialog();
      }
    });
  },

  refreshDialog: function() {
    $('sourceEditingDependencyDialog').refresh(null, "", function() {
      BS.SourceDependencyForm.showCentered();
    });
  },

  saveDependency: function() {
    if (this.formElement().srcDependOn.selectedIndex == -1) {
      alert("Please choose at least one build configuration from the list.");
      return false;
    }

    var that = this;
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SaveConfigurationListener, {
      onCyclicDependencyFoundError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },
      onCompleteSave: function(form, responseXML, err) {
        BS.SaveGeneralBuildTypeInfoListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          that.close();
          BS.reload(true);
          //$('sourceDependenciesTable').refresh(BS.SourceDependencyForm.savingIndicator());
        }
      }
    }));

    return false;
  }
}));

BS.AttachDetachTemplateAction = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('useTemplateFormDialog');
  },

  formElement: function() {
    return $('useTemplateForm');
  },

  showDialog: function(buildTypeId) {
    this.clearErrors();
    $('useTemplateForm').buildTypeId.value = buildTypeId;
    $('templateId').selectedIndex = 0;
    this.templateChanged(null, null, null);
    this.showCentered();
    this.bindCtrlEnterHandler(this.submit.bind(this));
  },

  savingIndicator: function() {
    return $('associateWithTemplateProgress');
  },

  submit: function() {
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      attachFailed: function(elem) {
        $('error_attachFailed').innerHTML = elem.firstChild.nodeValue;
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          document.location.href = document.location.href + "&init=1";
        }
      }
    }));
    return false;
  },

  cancelDialog: function() {
    this.close();
  },

  detachFromTemplate: function(buildTypeId) {
    if (!confirm("Are you sure you want to detach this build configuration from the template?\n" +
                 "Upon detach template settings will be copied to the build configuration.")) return false;

    BS.ajaxRequest(this.formElement().action, {
      parameters: "buildTypeId=" + buildTypeId + "&detachFromTemplate=1",
      onSuccess: function(transport) {
        var hasError = BS.XMLResponse.processErrors(transport.responseXML, {
          detachFailed: function(elem) {
            alert(elem.firstChild.nodeValue);
          }
        });

        if (!hasError) {
          document.location.href = document.location.href + "&init=1";
        }
      }
    });
  },

  templateChanged: function(buildTypeId, templateId, sameProject) {
    $("attachTemplate_vcsRootsSection").hide();
    $('templateParameters').innerHTML = '';
    this.submitEnabled(false);

    if (templateId) {
      this.loadTemplateData(buildTypeId, templateId, sameProject);
    } else {
      BS.jQueryDropdown('#templateId').ufd("changeOptions");
    }
  },

  loadTemplateData: function(buildTypeId, templateId, sameProject) {
    BS.TemplateParametersLoader.loadParameters(
        "templateId=" + templateId + "&buildTypeId=" + buildTypeId,
        'templateParameters',
        'templateParamsUpdateProgress', function() {
      if (sameProject) {
        BS.AttachDetachTemplateAction.submitEnabled(true);
      } else {
        BS.AttachDetachTemplateAction.checkLocalVcsRoots(templateId);
      }
      BS.AttachDetachTemplateAction.recenterDialog();
      BS.VisibilityHandlers.updateVisibility(BS.AttachDetachTemplateAction.getContainer());
    });
  },

  submitEnabled: function(enabled) {
    if (enabled) {
      Form.Element.enable(this.formElement().associateWithTemplate);
    } else {
      Form.Element.disable(this.formElement().associateWithTemplate);
    }
  },

  checkLocalVcsRoots: function(templateId) {
    BS.AdminActions.checkTemplateHasLocalVcsRoots(templateId, function(hasLocalVcsRoots) {
      var attachTemplate_vcsRootsSection = $("attachTemplate_vcsRootsSection");

      if (hasLocalVcsRoots) {
        attachTemplate_vcsRootsSection.show();
        attachTemplate_vcsRootsSection.onshow();
      } else {
        attachTemplate_vcsRootsSection.hide();
        BS.AttachDetachTemplateAction.submitEnabled(true);
      }
    });
  }
}));

BS.ExtractTemplateAction = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('extractTemplateFormDialog');
  },

  formElement: function() {
    return $('extractTemplateForm');
  },

  showDialog: function(buildTypeId) {
    this.clearErrors();
    this.formElement().buildTypeId.value = buildTypeId;
    this.showCentered();
    $('templateName').focus();

    this.bindCtrlEnterHandler(this.submit.bind(this));
  },

  savingIndicator: function() {
    return $('extractTemplateProgress');
  },

  submit: function() {
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      extractFailed: function(elem) {
        $('error_extractFailed').innerHTML = elem.firstChild.nodeValue;
      },

      invalidName: function(elem) {
        $('error_templateName').innerHTML = elem.firstChild.nodeValue;
        BS.ExtractTemplateAction.highlightErrorField($('templateName'));
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          document.location.href = document.location.href + "&init=1";
        }
      }
    }));
    return false;
  },

  cancelDialog: function() {
    this.close();
  }
}));

BS.TemplateParametersLoader = {
  loadParameters: function(reqParams, containerId, progressId, afterLoadFunc) {
    progressId = $(progressId);
    progressId.show();
    BS.ajaxUpdater($(containerId), window['base_uri'] + '/admin/showTemplateParams.html', {
      parameters: reqParams,
      evalScripts: true,
      onComplete: function() {
        progressId.hide();
        if (afterLoadFunc) {
          afterLoadFunc();
        }
      }
    });
  }
};

BS.CreateFromTemplateAction = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('createBuildTypeFromTemplateDialog');
  },

  formElement: function() {
    return $('createBuildTypeFromTemplate');
  },

  showDialog: function(templateId) {
    this.clearErrors();
    this.formElement().template.value = templateId;
    this._prepareForTemplateId(templateId);
    this.showCentered();
    this.formElement().buildTypeName.focus();
    this.bindCtrlEnterHandler(this.submit.bind(this));
    return false;
  },

  showDialogWithChooser: function() {
    this.clearErrors();

    var that = this;
    $j("#template").change(function() {
      var selectedTemplateId = $j(this).find("option:selected").val();
      that._prepareForTemplateId(selectedTemplateId);
    }).prop("selectedIndex", 0)
      .trigger("change");

    this.showCentered();
    this.bindCtrlEnterHandler(this.submit.bind(this));
    return false;
  },

  _prepareForTemplateId: function(templateId) {
    var that = this,
        form = that.formElement();

    Form.Element.disable(form.createFromTemplate);
    BS.TemplateParametersLoader.loadParameters(
      "templateId=" + templateId,
      "createFromTemplateParams",
      "createFromTemplateParamsProgress",
      function () {
        Form.Element.enable(form.createFromTemplate);
        that.recenterDialog();
        BS.VisibilityHandlers.updateVisibility(that.getContainer());
      }
    );
  },

  savingIndicator: function() {
    return $('createFromTemplateProgress');
  },

  submit: function() {
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      createFailed: function(elem) {
        $('error_createFailed').innerHTML = elem.firstChild.nodeValue;
      },

      invalidName: function(elem) {
        $('error_buildTypeName').innerHTML = elem.firstChild.nodeValue;
        BS.CreateFromTemplateAction.highlightErrorField($("buildTypeName"));
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          BS.XMLResponse.processRedirect(responseXML);
        }
      }
    }));
    return false;
  },

  cancelDialog: function() {
    this.close();
  }
}));

BS.EditArtifactDependencies = {
  currentTextField : null,
  expandNestedArchives : false,

  attachPopups: function(className, idPrefix, scopeName) {
    var scope = $(scopeName);
    var elems = scope.select('.' + className);
    var elemsToUpdate = {};

    for (var j = 0; j < elems.length; j++) {
      var elem = elems[j];
      if (elem.className.indexOf('disableBuildTypeParams') != -1) continue;
      if (!elem.id.startsWith(idPrefix)) continue;
      var imgId = elem.id + "_insertArtifact";
      if (scope.select('#' + imgId).length > 0) continue;

      elemsToUpdate[elem.id] = elem;
      this.attachPopup(elem);
    }

    for (var id in elemsToUpdate) {
      BS.VisibilityHandlers.updateVisibility(elemsToUpdate[id]);
    }
  },

  attachPopup: function(elem) {
    var popupControlId = elem.id + "_insertArtifact";
    var popupControl = document.createElement('IMG');
    popupControl.src = window['base_uri'] + '/img/tree/popup-artifacts-tree.png';
    popupControl.id = popupControlId;
    popupControl.style.position = 'absolute';
    popupControl.style.display = 'none';
    popupControl.style.cursor = 'pointer';
    popupControl.title = "Choose artifact";

    var that = this;
    popupControl.onclick = function() {
      that.currentTextField = elem;
      that.showArtifactsTree(popupControl, that.expandNestedArchives);
    };

    var parentClass = elem.parentNode.className;

    if (parentClass.indexOf('completionIconWrapper') == -1 && parentClass.indexOf('posRel') == -1) {
      var inputField = $j(elem);
      var wrapper = BS.Util.wrapRelative(inputField);

      if (inputField.prop('style').width == '100%') {
        wrapper.css('display', 'block');
      }
    }

    elem.parentNode.appendChild(popupControl);

    var handler = {
      updateVisibility: function() {
        var control = $(elem.id);
        if (control.disabled || control.readOnly) {
          BS.Util.hide(popupControl);
        } else {
          var popupControl = $(popupControlId);
          if (popupControl != null) {
            var dim = control.getDimensions(),
                layout = control.getLayout();

            var xshift = dim.width + 20; // Put next to the completion icon
            var pos = control.positionedOffset();

            var x = pos[0] + xshift + layout.get('margin-left');
            var y = pos[1] + 3 + layout.get('margin-top');
            BS.Util.show(popupControl);
            BS.Util.place(popupControl, x, y);
          }
        }
      }
    };
    BS.VisibilityHandlers.attachTo(elem.id, handler);
  },

  showArtifactsTree: function(elem, expandNestedArch) {
    var buildTypeId = this.findOutBuildTypeId();
    var buildRevision = this.findOutBuildRevision();
    var buildNumber = this.findOutBuildNumber();
    var buildTag = this.findOutBuildTag();

    var url = window['base_uri'] + "/editArtifactDepsHelper.html?buildTypeId=" + buildTypeId;
    if (buildRevision) { url += "&buildId=" + buildRevision; }
    if (buildNumber) { url += "&buildNumber=" + buildNumber; }
    if (buildTag) { url += "&buildTag=" + buildTag; }
    if (expandNestedArch != undefined) { url += "&expandNestedArch=" + expandNestedArch; }

    BS.LazyTree.treeUrl = url;
    BS.LazyTree.ignoreHashes = true;
    var popup = new BS.Popup("artifactsTreePopup", {
      hideOnMouseOut: false,
      hideOnMouseClickOutside: true,
      shift: {x: 20, y: 0},
      url: url
    });
    popup.showPopupNearElement(elem);
  },

  findOutBuildTypeId: function() {
    // NOTE: there are two elements with id='sourceBuildTypeId'!
    var select = $('artifactDependencyForm').sourceBuildTypeId;
    return select.selectedIndex !== -1 && select.options[select.selectedIndex].value;
  },

  findOutBuildRevision: function() {
    var select = $('revisionRules');
    return select.selectedIndex > 3 ? null : select.options[select.selectedIndex].value;
  },

  findOutBuildNumber: function() {
    var textField = $('buildNumberPattern');
    return textField.disabled ? null : textField.value;
  },

  findOutBuildTag: function() {
    var textField = $('buildTag');
    return textField == null || textField.disabled ? null : textField.value;
  },

  appendPath: function(path) {
    if (this.currentTextField) {
      var value = $(this.currentTextField).value;
      if (value.length > 0) {
      var lines = value.split("\n");
      lines.push(path);
      $(this.currentTextField).value = lines.join("\n");
      } else {
        $(this.currentTextField).value = path;
      }
    }
  },

  setPath: function(path) {
    if (this.currentTextField) {
      $(this.currentTextField).value = path;
    }
  },

  showHideValueFields: function() {
    var form = $('artifactDependencyForm');
    var buildNumberSelected = form.revisionRules.selectedIndex == 4;
    var buildTagSelected = form.revisionRules.selectedIndex == 5;

    if (buildNumberSelected) {
      $('buildNumberPattern').disabled = false; BS.Util.show('buildNumberField');
      $('buildTag').disabled = true; BS.Util.hide('buildTagField');
    } else if (buildTagSelected) {
      $('buildNumberPattern').disabled = true; BS.Util.hide('buildNumberField');
      $('buildTag').disabled = false; BS.Util.show('buildTagField');
    } else {
      $('buildNumberPattern').disabled = true; BS.Util.hide('buildNumberField');
      $('buildTag').disabled = true; BS.Util.hide('buildTagField');
    }
    BS.VisibilityHandlers.updateVisibility('buildNumberField');
    BS.VisibilityHandlers.updateVisibility('buildTagField');
  },

  showHideLastFinishedNote: function() {
    var form = $('artifactDependencyForm');
    if (form.sourceBuildTypeId.options.length == 0) {
      BS.Util.hide('lastFinishedNote');
      return;
    }

    var selectedBuildTypeId = form.sourceBuildTypeId.options[form.sourceBuildTypeId.selectedIndex].value;
    if (sourceDepsMap[selectedBuildTypeId] && form.revisionRules.selectedIndex != 3) {
      BS.Util.show('lastFinishedNote');
    } else {
      BS.Util.hide('lastFinishedNote');
    }

    BS.VisibilityHandlers.updateVisibility('artifactDependencyForm');
  },

  updateBuildTypeTagsList: function() {
    var tagListHtml = "";
    var form = $('artifactDependencyForm');
    if (form.sourceBuildTypeId.options.length == 0) {
      BS.Util.hide('buildTagList');
      return;
    }

    var selectedBuildTypeId = form.sourceBuildTypeId.options[form.sourceBuildTypeId.selectedIndex].value;
    var url = window['base_uri'] + "/editArtifactDepsHelper.html";

    BS.ajaxRequest(url, {
      parameters: {
        listTags : '',
        buildTypeId : selectedBuildTypeId
      },
      onComplete: function(transport) {
        var responseXML = transport.responseXML;
        var tagData = responseXML.firstChild.firstChild;
        if (tagData) {
          while (tagData) {
            function renderTagLink(label, escapedLabel) {
              return "<a href=\"#\" onclick=\"BS.EditArtifactDependencies.setBuildTagValue('" +
                     escapedLabel + "'); return false\">" + label.escapeHTML() + "</a> ";
            }
            tagListHtml = tagListHtml + renderTagLink(tagData.attributes.original.value,
                                                      tagData.attributes.escaped.value);
            tagData = tagData.nextSibling;
          }
          $('buildTagListSpan').innerHTML = tagListHtml;
          BS.Util.show('buildTagList')
        } else {
          BS.Util.hide('buildTagList');
        }
      }
    });
  },

  setBuildTagValue: function(tagValue) {
    var textField = $('buildTag');
    if (!textField.disabled) {
      textField.value = tagValue;
    }
  }
};

BS.MultipleRunnersForm = {
  deleteRunner: function(settingsId, runnerId) {
    if (!confirm("Are you sure you want to delete this build step?")) return;

    BS.ajaxRequest('editBuildRunners.html', {
      parameters: "id=" + settingsId + "&deleteRunner=" + runnerId,
      onSuccess: function() {
        if ($('buildStepsContainer')) {
          $('buildStepsContainer').refresh();
          $('sidebarAdmin').refresh();
        } else {
          document.location.href = 'editBuildRunners.html?init=1&id=' + settingsId;
        }
      }
    });
  },

  setEnabled: function(settingsId, runnerId, enabled) {
    BS.ajaxRequest('editBuildRunners.html', {
      parameters: "id=" + settingsId + "&setEnabled=" + runnerId + "&enabled=" + enabled,
      onSuccess: function() {
        BS.reload(true);
      }
    });
  }
};

BS.CopyBuildStepForm = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('copyBuildStepProgress');
    } else {
      BS.Util.hide('copyBuildStepProgress');
    }
  },

  getContainer: function() {
    return $('copyBuildStepFormDialog');
  },

  formElement: function() {
    return $('copyBuildStepForm');
  },

  showDialog: function(runnerId) {
    $('sourceRunnerId').value = runnerId;
    this.showCentered();
    this.bindCtrlEnterHandler(this.submitCopy.bind(this));
  },

  cancelDialog: function() {
    this.close();
  },

  afterClose: function() {
    this.clearErrors();
  },

  submitCopy: function() {
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onCopyStepFailed: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

        if (!err) {
          BS.CopyBuildStepForm.close();
          BS.XMLResponse.processRedirect(responseXML);
        }
      }
    }));

    return false;
  }
}));

BS.BuildFeatureDialog = OO.extend(BS.BaseEditBuildTypeForm, OO.extend(BS.AbstractModalDialog, OO.extend(BS.PluginPropertiesForm, {
  formElement: function() {
    return $('buildFeatures');
  },

  getContainer: function() {
    return $('buildFeaturesDialog');
  },

  savingIndicator: function() {
    return $('saveFeatureProgress');
  },

  showAddDialog: function(featurePlace, title) {
    this.formElement().featurePlace.value = featurePlace;

    $('featureParams').innerHTML = '';
    $('featureSaveButtonsBlock').show();

    this._updateOptionsListAccordingToFeaturePlace();

    if ($('featureTypeSelector').childNodes.length == 2) {
      this._initializeDialogWithSingleOption();
    }
    else {
      this._initializeDialogWithMultipleOptions($j(title).text());
    }

    this.formElement().featureId.value = '';
    this.showCentered();
    this.bindCtrlEnterHandler(this.save.bind(this));
  },

  _updateOptionsListAccordingToFeaturePlace: function() {
    var featurePlace = this.formElement().featurePlace.value;

    var options = $('featureTypeOptionsData').select("option");
    $('featureTypeSelector').innerHTML = '';
    for (var i = 0; i < options.length; i++) {
      var option = options[i];
      var fp = option.getAttribute('data-feature-place');
      if (!fp || featurePlace == fp) {
        $('featureTypeSelector').appendChild(option.cloneNode(true));
      }
    }
  },

  _initializeDialogWithSingleOption: function() {
    $('buildFeaturesTitle').innerHTML = $('featureTypeSelector').childNodes[1].innerHTML;
    $('featureTypeSelector').selectedIndex = 1;
    BS.BuildFeatureDialog.onTypeChange($('featureTypeSelector'));
    $('featureTypeSelector').up("table").hide();
    $('submitBuildFeatureId').enable();
  },

  _initializeDialogWithMultipleOptions: function(title) {
    $('buildFeaturesTitle').innerHTML = BS.Util.capitalize(title);
    $('featureTypeSelector').selectedIndex = 0;
    $('featureTypeSelector').options[0].innerHTML = "-- Please select " + title.substr(4) + " --";
    BS.jQueryDropdown($('featureTypeSelector'));
    $('featureTypeSelector').up("table").show();
    $('submitBuildFeatureId').disable();
  },

  showEditDialog: function(featurePlace, id, type, name) {
    this.formElement().featurePlace.value = featurePlace;

    $('buildFeaturesTitle').innerHTML = name;
    $('featureParams').innerHTML = '';
    $('featureSaveButtonsBlock').hide();
    $('submitBuildFeatureId').enable();

    $('featureTypeSelector').up("table").hide();

    this.formElement().featureId.value = id;
    this.loadParams('featureId=' + id);
    this.showCentered();
    this.bindCtrlEnterHandler(this.save.bind(this));
  },

  onTypeChange: function(selector) {
    if (selector.selectedIndex <= 0) {
      $('featureParams').innerHTML = '';
      $('submitBuildFeatureId').disable();
      return;
    }
    $('submitBuildFeatureId').enable();
    var type = selector.options[selector.selectedIndex].value;
    this.loadParams('featureId=&featureType=' + type);
  },

  loadParams: function(param) {
    var buildTypeId = window.location.search.substring(1).split('&').grep(/id=(.*)/).join('').split('=')[1];
    $('loadParamsProgress').show();

    BS.ajaxUpdater($('featureParams'), window['base_uri'] + '/admin/showFeatureParams.html', {
      parameters: "id=" + buildTypeId + "&" + param,
      evalScripts: true,
      onComplete: function() {
        BS.BuildFeatureDialog.recenterDialog();
        BS.AvailableParams.attachPopups('settingsId=' + buildTypeId, 'textProperty', 'multilineProperty');
        $('loadParamsProgress').hide();
        BS.VisibilityHandlers.updateVisibility('buildFeaturesDialog');
      }
    });
  },

  deleteFeature: function(featurePlace, id, messagePrefix) {
    if (!messagePrefix) {
      messagePrefix = "Build feature";
    }
    if (!confirm("Are you sure you want to delete this "  + messagePrefix.toLowerCase() + "?")) return;

    var url = this.formElement().getAttribute('action');
    var buildTypeId = window.location.search.substring(1).split('&').grep(/id=(.*)/).join('').split('=')[1];

    BS.ajaxRequest(url, {
      parameters: "featurePlace=" + featurePlace + "&id=" + buildTypeId + "&removeFeature=true&featureId=" + id + "&messagePrefix=" + encodeURIComponent(messagePrefix),
      onComplete: function() {
        $('buildTypeBuildFeatures_' + featurePlace).refresh();
      }
    });
  },

  save: function() {
    var that = this;
    var url = this.formElement().getAttribute('action');

    BS.PasswordFormSaver.save(that, url, OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function(form, responseXML, err) {
        var wereErrors = BS.XMLResponse.processErrors(responseXML, {}, that.propertiesErrorsHandler) || err;

        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, wereErrors);

        if (wereErrors) {
          return;
        }

        var featurePlace = that.formElement().featurePlace.value;

        $('buildTypeBuildFeatures_' + featurePlace).refresh();
        that.enable();
        that.close();
      }
    }));
    return false;
  }
})));

BS.BuildStepsOrderDialog = OO.extend(BS.AbstractModalDialog, OO.extend(BS.AbstractWebForm, {
  getContainer: function() {
    return $('buildStepsOrderDialog');
  },

  formElement: function() {
    return $('buildStepsOrder');
  },

  savingIndicator: function() {
    return $('saveOrderProgress');
  },

  fixPageScroll: function() {
    window.scrollTo(0,0);
  },

  submitForm: function(buildTypeId) {
    this.disable();
    this.setSaving(true);

    var runners = OO.extend(BS.QueueLikeSorter, {
      containerId: 'buildRunners'
    });

    var order = runners.computeOrder($('buildRunners'), 'r_');

    BS.ajaxRequest(this.formElement().action, {
      parameters: 'id=' + buildTypeId + '&runnersOrder=' + order,
      onComplete: function(transport) {
        BS.BuildStepsOrderDialog.enable();
        BS.BuildStepsOrderDialog.setSaving(false);
        BS.BuildStepsOrderDialog.close();
        $('buildStepsContainer').refresh();
      }
    });

    return false;
  }
}));

BS.EditTriggersDialog = OO.extend(BS.PluginPropertiesForm, OO.extend(BS.BaseEditBuildTypeForm, OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('editTriggerDialog');
  },

  formElement: function() {
    return $('editTrigger');
  },

  showAddDialog: function(title) {
    this.formElement().editMode.value = "addTrigger";
    this.formElement().triggerId.value = '';
    $('editTriggerTitle').innerHTML = title;

    $('triggerNameSelector').up("table").show();
    $('triggerNameSelector').selectedIndex = 0;
    BS.jQueryDropdown('#triggerNameSelector').ufd("changeOptions");
    this.triggerChanged('');

    this.showDialog();
  },

  showDialog: function() {
    this.showAtFixed($(this.getContainer()));
  },

  showEditDialog: function(triggerName, triggerId, title) {
    this.formElement().editMode.value = "editTrigger";
    this.formElement().triggerId.value = triggerId;
    $('editTriggerTitle').innerHTML = title;

    $('triggerNameSelector').up("table").hide();

    this.triggerChanged(triggerName);

    this.showDialog();
  },

  triggerChanged: function(triggerName, buildTypeId) {
    buildTypeId = buildTypeId || window.location.search.substring(1).split('&').grep(/id=(.*)/).join('').split('=')[1];

    this.formElement().triggerName.value = triggerName;
    $('triggerParams').innerHTML = '';

    if (triggerName == '') {
      $('editTriggerSubmit').disable();
      return;
    }

    $('editTriggerSubmit').enable();

    $('loadParamsProgress').show();
    var that = this;
    BS.ajaxUpdater('triggerParams', window['base_uri'] + '/admin/showTriggerParams.html', {
      parameters: 'triggerName=' + encodeURIComponent(triggerName) + "&triggerId=" + this.formElement().triggerId.value + "&id=" + buildTypeId,
      evalScripts: true,
      onSuccess: function() {
        $('loadParamsProgress').hide();

        window.setTimeout(function() {
          BS.AvailableParams.attachPopups('settingsId=' + buildTypeId, 'textProperty', 'multilineProperty');
          that.recenterDialog();
        }, 100);
      }
    });
  },

  submitForm: function() {
    var that = this;
    var url = this.formElement().getAttribute('action');
    BS.PasswordFormSaver.save(that, url, OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function(form, responseXML, err) {
        var wereErrors = BS.XMLResponse.processErrors(responseXML, {}, that.propertiesErrorsHandler) || err;

        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, wereErrors);

        if (wereErrors) {
          return;
        }

        $('triggersTable').refresh();
        that.enable();
        that.close();
      }
    }));
    return false;
  },

  removeTrigger: function(id) {
    if (!confirm("Are you sure you want to delete this trigger?")) return;

    BS.ajaxRequest(this.formElement().getAttribute('action'), {
      parameters: 'removeTrigger=' + id,
      onComplete: function() {
        $('triggersTable').refresh();
      }
    })
  },

  allTriggers: {}
})));

BS.TestOnFinishedBuildDialog = OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('testOnFinishedBuildDialog');
  },

  submit: function() {
    $j('#testOnFinishedBuildResult').html('');
    BS.Util.show("testOnFinishedBuildProgress");

    BS.ajaxRequest(window['base_uri'] + "/admin/testOnFinishedBuild.html?" + $j(BS.Util.escapeId("buildFailureOnMessage.finishedBuildId")).serialize(), {
      method: "post",
      parameters: BS.BuildFeatureDialog.serializeParameters(),

      onComplete: function(transport) {
        BS.Util.hide("testOnFinishedBuildProgress");
        var responseXML = transport.responseXML;
        var err = BS.XMLResponse.processErrors(responseXML, {}, BS.BuildFeatureDialog.propertiesErrorsHandler); // show errors on the initial dialog
        if (!err) {
          $j('#testOnFinishedBuildResult').html(responseXML.documentElement.childNodes[0].nodeValue);
        } else {
          BS.TestOnFinishedBuildDialog.close();
        }
      }
    });
    return false;
  },

  afterClose: function() {
    $j('#testOnFinishedBuildResult').html('');
  },

  showBuildLog: function(buildId) {
    window.location.href = window['base_uri'] + '/viewLog.html?tab=buildLog&logTab=tree&buildId=' + buildId;
  }
});

BS.AddTriggerRuleDialog = OO.extend(BS.AbstractModalDialog, {
  attachedToRoot: false,

  getContainer: function() {
    return $('addTriggerRuleDialog');
  },

  showDialog: function() {
    this.showCentered();
    this.bindCtrlEnterHandler(this.submit.bind(this));
  },

  submit: function() {
    if (!this.validate()) return false;

    var rule = $('triggerBuild1').checked ? '+:' : '-:';
    if (!$('username').value.empty()) {
      rule += 'user=' + $('username').value;
    }
    if ($('vcsRoot').selectedIndex > 0) {
      if (!rule.endsWith(';') && !rule.endsWith(':')) rule += ';';
      rule += 'root=' + $('vcsRoot').options[$('vcsRoot').selectedIndex].value;
    }
    if (!$('comment').value.empty() && $('comment').value != '<comment regexp>') {
      if (!rule.endsWith(';') && !rule.endsWith(':')) rule += ';';
      rule += 'comment=' + $('comment').value;
    }
    if (!rule.endsWith(':')) rule += ':';
    rule += $('wildcard').value;

    if (!$('triggerRules').value.empty()) {
      $('triggerRules').value += '\n';
    }
    $('triggerRules').value += rule;
    BS.MultilineProperties.show('triggerRules', true);
    this.close();
    return false;
  },

  validate: function() {
    if (!$('triggerBuild1').checked && !$('triggerBuild2').checked) {
      alert('Please choose rule type');
      return false;
    }
    if ($('wildcard').value.empty()) {
      alert('Wildcard cannot be empty');
      $('wildcard').focus();
      return false;
    }
    return true;
  }
});
