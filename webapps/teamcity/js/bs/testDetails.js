
BS.TestDetails = {
  toggleDetails: function(element, urlToLoad) {
    var parentTr = $(element).up("tr");
    var tdCount = parentTr.select("> td").size();

    var detailsTrId = this._detailsTrId(parentTr);
    if ($(detailsTrId)) {
      // Process case when details block is already available:
      if (!this.hideDetailsRow(parentTr)) {
        // Show available block
        $(detailsTrId).show();
        this.updateTestRow(parentTr, true);
      }
      return;
    }

    this.updateTestRow(parentTr, true);
    var detailsTr = new Template("<tr id='#{rowId}' class='testDetailsRow'><td colspan='#{colspan}' class='data'>#{content}</td></tr>").evaluate(
        {
          colspan: tdCount,
          rowId: detailsTrId,
          content: '<div class="testLoading">' + BS.loadingIcon + ' Loading test details, please wait...' + '</div>'
        }
    );

    parentTr.insert({after: detailsTr});
    BS.ajaxUpdater($(detailsTrId).down("td.data"), window['base_uri'] + urlToLoad, {
      evalScripts: true
    });
  },

  _detailsTrId: function(parentTr) {
    return "testDetails_" + $(parentTr).identify();
  },

  hideDetailsRow: function(parentTr) {
    var detailsTrId = this._detailsTrId(parentTr);

    if (BS.Util.visible(detailsTrId)) {
      $(detailsTrId).hide();
      this.updateTestRow(parentTr, false);
      return true;
    }
    return false;
  },

  updateTestRow: function(tr, detailsOn) {
    tr = $(tr);
    if (detailsOn) {
      BS.Log.debug("expanding stacktrace " + tr.id);
      tr.addClassName("testDetailsShownRow");

      // Add a listener which runs while stacktrace is expanded
      // When stacktrace becomes non-visible on the page, it is collapsed to enable page refresh, (for BS.canReload() function)
      tr.store('on_hide_listener', setInterval(function() {
        var detailsTrId = this._detailsTrId(tr);
        if ($j(BS.Util.escapeId(detailsTrId) + ":hidden").length > 0) {
          this.hideDetailsRow(tr);
          this.updateTestRow(tr, false);
        }
      }.bind(this), 200));

      BS.BuildResults.stacktraceShown();
    } else {
      var listener = tr.retrieve('on_hide_listener');
      if (listener) {
        BS.Log.debug("collapsing stacktrace " + tr.id);

        tr.removeClassName("testDetailsShownRow");
        BS.BuildResults.stacktraceHidden();

        clearInterval(listener);
        tr.store('on_hide_listener', null);
      }
    }
  },

  closeDetails: function(element) {
    var detailsTr = $(element).up("tr");
    detailsTr.hide();
    var testRow = $(detailsTr.previousSibling);
    this.updateTestRow(testRow, false);
    BS.Highlight(testRow.down("td"), {duration: 2});
  },

  loadFFIInformation: function(buildId, testId, tableWithData) {

    //BS.Log.debug("loadFFIInformation buildId:" + buildId + " testId:" +testId);

    var that = this;
    that.changeCurrentRow(tableWithData, tableWithData.down('tr.selectedBuild'));

    new BS.ajaxUpdater(tableWithData, "firstFailedInfo.html?buildId=" + buildId + "&testId=" + testId, {
      onComplete: function() {
        tableWithData.select("tr").each(function(tr) {
          if (tr.getAttribute('data-testId')) {

            tr.addClassName("clickable");
            tr.title = "Click to see stacktrace from this build";

            tr.on("click", "td", function() {
              that.changeCurrentRow(tableWithData, tr);
            })
          }
        });
      }
    });

  },

  changeCurrentRow: function(table, tr_selected) {
    var buildId = tr_selected.getAttribute('data-buildId');
    var testId = tr_selected.getAttribute('data-testId');
    var traceBlock = table.up('div.testBlock').down('.fullStacktrace');
    var testListWidth = traceBlock.up('.testList').getDimensions().width;
    traceBlock.style.height = traceBlock.getDimensions().height + 'px';
    traceBlock.update();

    this.changeCurrentRowStyle(table, tr_selected);

    BS.BuildResults.expandStacktrace(traceBlock, buildId, testId, function() {
      traceBlock.style.height = 'auto';
      // 54 = 27px margin + 27px padding
      traceBlock.parentNode.style.width = testListWidth - 54 + 'px';
    });
  },

  changeCurrentRowStyle: function(table, tr_selected) {
    table.select('tr').each(function(tr) {
      tr.removeClassName('selectedBuild');
    });
    tr_selected.addClassName('selectedBuild');
    var radioselector = tr_selected.down('td.selector input');
    if (radioselector) radioselector.click();
  },

  toggleBuildDetails: function(element, event) {
    if (event) {
      element = Event.element(event);
      if (element.tagName == 'A') return;
    }

    $(element).up('.testBlock').select('.rightBlock').invoke('toggle');
  }


};