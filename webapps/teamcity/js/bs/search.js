BS.Search = {
  initEvents: function() {
    var searchField = $j('#searchField, #headerSearchField');

    searchField.on('focus', function(e) {
      BS.Search.onfocus(e.target);
    });

    searchField.on('blur', function(e) {
      BS.Search.onblur(e.target);
    });

    searchField.on('keydown', function() {
      BS.Tooltip.hidePopup();
    });

    searchField.on('keypress', function(e) {
      if (e.keyCode == 13) {
        BS.Search.go(e.shiftKey);
        return false;
      }
    });
  },

  installSearchField:function () {
    var userPanel = $('userPanel'),
        inputIdF = $('inputIdF');

    if (!userPanel || !inputIdF) return;

    userPanel.cleanWhitespace();
    inputIdF.cleanWhitespace();
    inputIdF.firstChild.id = 'headerSearchField';
    userPanel.insert({bottom:"<span class='search'>" + inputIdF.innerHTML + "</span>"});
    inputIdF.innerHTML = '';

    $j('#headerSearchField').on('keypress paste', function (e) {
      if (BS.Search.timer) clearTimeout(BS.Search.timer);
      var url = BS.Search.getUrl();
      if (e.keyCode == Event.KEY_RETURN) {
        if (e.shiftKey) {
          document.location.href = url;
        } else {
          BS.Search.showPopup(url);
        }
      } else if (e.keyCode == Event.KEY_ESC) {
        return false;
      } else {
        BS.Search.timer = setTimeout(function () {
          BS.Search.showPopup(BS.Search.getUrl());
        }, 700);
      }
    });
  },

  disableSearchTooltip:function () {
    BS.Cookie.set("n_s_h", 'true', 60);
    BS.Tooltip.hidePopup();
  },

  onfocus:function (field) {
    $j(field).parent().addClass('searchFocused');
  },

  onblur:function (field) {
    $j(field).parent().removeClass('searchFocused');
  },

  go:function (newPage) {
    var searchField = $j("#searchField"),
        headerSearchField = $j("#headerSearchField"),
        inputIdF = $j("#inputIdF"),
        searchByTime = $j('#searchByTime');

    var url = window['base_uri'] + '/searchResults.html?query='
              + encodeURIComponent(searchField.length > 0 ? searchField.val() : headerSearchField.val())
              + '&buildTypeId=' + inputIdF.attr("data-btId")
              + '&byTime=' + (searchByTime.length > 0 ? searchByTime.val() : false);

    if (newPage) {
      window.open(url);
    } else {
      document.location.href = url;
    }
  },

  getUrl:function () {
    return window['base_uri'] + '/searchResults.html?query='
               + encodeURIComponent($j('#headerSearchField').val())
               + '&buildTypeId=' + $j("#inputIdF").attr("data-btId");
  },

  timer:null,

  showPopup:function (url) {
    new BS.Popup('searchResultsPopup', {
      delay:0,
      shift:{x:-150, y:22},
      hideOnMouseOut:false,
      loadingText:"Loading search results...",
      url:url + '&popupMode=true',
      afterShowFunc:function (popup) {
      }
    }).showPopupNearElement('headerSearchField', {forceReload: true});
  },

  highlightResult:function (searchIn) {
    var items = $j(searchIn);
    var zones;

    items.each(function() {
      var item = $j(this);
      zones = item.data('zones');

      if (zones) {
        zones = zones.split(' ');
        var searchableElements;

        if (zones.length > 0) {
          searchableElements = item.find(zones[0]);

          if (zones.length > 1) {
            for (var i=1; i<zones.length; i++) {
              searchableElements.add(item.find(zones[i]));
            }
          }

          searchableElements.find('span.pc').addClass('highlightChanges');
        }
      }
    });
  }
};

// Tricky part - insert search field into existing tag - userPanel (top right navigation)
BS.WaitFor(function () {
  return $('header-ready');
}, BS.Search.installSearchField);

$j(document).ready(BS.Search.initEvents);