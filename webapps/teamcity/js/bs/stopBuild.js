/*
 * Copyright 2000-2012 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

if (BS.AbstractModalDialog && BS.AbstractWebForm) {

BS.StopBuildDialog = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  formElement: function() {
    return $('stopBuildForm');
  },

  getContainer: function() {
    return $('stopBuildFormDialog');
  },

  handleBuildNotFound: function() {
    $j('#removeQueuedBuildComment').hide();
    $j('#submitRemoveQueuedBuild').hide();
    this.refreshPageAfterClose = true;
  },

  afterClose: function() {
    if (this.refreshPageAfterClose) {
      this.refreshPageAfterClose = false;
      $j('#removeQueuedBuildComment').show();
      $j('#submitRemoveQueuedBuild').show();
      $j('#moreToStopFragment').hide();
      BS.reload(true);
    }
  },

  showStopBuildDialog: function(promoIds, defaultMessage, buildIsInQueue, reStopMessage) {
    if (promoIds.length == 0) {
      alert("Please select at least one build.");
      return;
    }

    this.enable();

    this.formElement().kill.value = promoIds.join(',');

    if (!!reStopMessage && $(reStopMessage)) {
      $("moreToStopRetryNotice").innerHTML = $(reStopMessage).innerHTML;
      BS.Util.show($("moreToStopRetryNotice"));
    } else {
      $("moreToStopRetryNotice").innerHTML = '';
      BS.Util.hide($("moreToStopRetryNotice"));
    }

    var builds = promoIds.length > 1 ? promoIds.length + ' builds' : 'build';
    $('stopBuildFormTitle').innerHTML = buildIsInQueue ? 'Remove ' + builds + ' from the queue' : 'Stop running build';
    this.formElement().submit.value = buildIsInQueue ? 'Remove' : 'Stop';

    $('moreToStopFragment').innerHTML = '';
    $('moreToStop').style.display = 'none';

    if (promoIds.length == 1) {
      $('moreToStop').style.display = 'block';
      $('moreToStopLoader').style.display = 'inline';

      this.disable();
      BS.ajaxUpdater('moreToStopFragment', window['base_uri'] + "/promotionGraph.html", {
        parameters: "buildPromotionId=" + new String(promoIds[0]) + "&jsp=moreToStop.jsp" + "&buildIsInQueue=" + buildIsInQueue,
        evalScripts: true,
        onComplete: function() {
          $('moreToStop').style.display = 'none';
          BS.StopBuildDialog.initSelectAll();
          BS.StopBuildDialog.enable();
        }
      });
    }

    this.formElement().comment.value = defaultMessage != null && defaultMessage.length > 0 ? defaultMessage : this.formElement().comment.defaultValue;
    this.showCentered();
    $(this.formElement().comment).activate();

    this.bindCtrlEnterHandler(this.killBuild.bind(this));
    
    this.setSaving(false);
  },

  killBuild: function(buildPromotionId) {
    if (buildPromotionId) {
      this.formElement().kill.value = buildPromotionId;
    }

    if (this.formElement().comment.value == this.formElement().comment.defaultValue) {
      this.formElement().comment.value = "";
    }

    if (this.isVisible()) {
      this.setSaving(true);
      this.disable();
    }

    var that = this;
    BS.ajaxRequest(this.formElement().action, {
      parameters: this.serializeParameters(),
      onSuccess: function(transport) {
        if (that.isVisible()) {
          that.setSaving(false);
          that.enable();
          that.close();
        }

        var doc = BS.Util.documentRoot(transport);

        // Update UI for stopped builds:
        var stopped = doc.getElementsByTagName("stoppedBuild");
        if (stopped) {
          for (var i = 0; i < stopped.length; i ++) {
            var promoId = stopped[i].firstChild.nodeValue;
            that.setStopping(promoId);
          }
        }

        // Update UI for builds removed from queue:
        var unqueued = doc.getElementsByTagName("unqueuedBuild");
        if (unqueued && BS.Queue) {
          for (var i = 0; i < unqueued.length; i ++) {
            var itemId = unqueued[i].firstChild.nodeValue;
            BS.Queue.removeBuildTypeRow(itemId);
          }
        }

        if (BS.EventTracker) {
          BS.EventTracker.checkEvents();
        }
      }

    });

    this.close();
    if (BS.QueuedBuildsPopup && BS.QueuedBuildsPopup.isShown()) {
      BS.QueuedBuildsPopup.hidePopup();
    }

    return false;
  },

  setStopping: function(buildPromotionId) {
    var updateLink = function(newText) {
      var link = $('stopBuild:' + buildPromotionId + ':link');
      if (link) {
        link.innerHTML = newText;
        link.addClassName('stopping');
      }
    };

    //TODO: reuse texts generated from HTML
    updateLink('<span class="stopping">Stopping</span>');
    if ($('stopBuild:' + buildPromotionId + ':message')) {
      $('stopBuild:' + buildPromotionId + ':message').innerHTML = "This build has already been stopped by <em>you</em>";
    }

    setTimeout(function() {
      updateLink('<span class="stopping" title="TeamCity cannot stop this build. Please log in to the build agent and resolve the situation manually.">Cannot stop</span>');
    }, 5 * 60* 1000)
  },

  // 'All builds' checkbox
  initSelectAll: function() {
    var otherBuilds = $j("#moreToStopFragment input[name='kill']");
    $j("#killAll").change(function() {
      var checked = this.checked;

      otherBuilds.each(function() {
        this.checked = checked;
      });
    });
  }

}));

}
