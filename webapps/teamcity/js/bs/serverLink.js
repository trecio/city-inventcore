BS.ServerLink = OO.extend(BS.AbstractModalDialog, {
  _failuresNum: 0,

  getContainer: function() {
    return $('shutdownDialog');
  },

  onShutdown: function() {
    if (this.isVisible() || this._wasClosed) return;
    BS.Util.hide('onCommunicationFailure');
    BS.Util.show('onServerShutdown');
    $('shutdownDetails').innerHTML = "Server is unavailable since " + new Date().toLocaleString();
    this.showCentered();
  },

  onFailure: function(ex) {
    if (this.isVisible() || this._wasClosed) return;
    if (++this._failuresNum > 5) {
      BS.Util.hide('onServerShutdown');
      BS.Util.show('onCommunicationFailure');
      $('shutdownDetails').innerHTML = "Server is unavailable since " + new Date().toLocaleString();
      this.showCentered();
    }
  },

  isConnectionAvailable: function() {
    return this._failuresNum == 0
  },

  onSuccess: function() {
    this._failuresNum = 0;
    this._wasClosed = false;
    this.doClose();
  },

  close: function() {
    this._wasClosed = true;
    this.doClose();
  }
});
