BS.UserListForm = OO.extend(BS.AbstractWebForm, {
  formElement: function() {
    return $('filterForm');
  },

  reSort: function(event) {
    var element = Event.element(event);
    var sortBy = element.id;
    if (!sortBy) {
      element = element.firstChild;
      sortBy = element.id;
    }
    var sortAsc = element.className.indexOf('sortedDesc') != -1;
    this.formElement().sortBy.value = sortBy;
    this.formElement().sortAsc.value = sortAsc;
    this.doSearch();
  },

  doSearch: function() {
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SimpleListener, {
      onCompleteSave: function() {
        $('userTable').refresh($('saving'));
      }
    }), false);
    return false;
  },

  resetFilter: function() {
    this.formElement().keyword.value = '';
    $('groupCode').selectedIndex = 0;
    if ($('roleId')) {
      $('roleId').selectedIndex = 0;
    }
    if ($('projectId')) {
      $('projectId').selectedIndex = 0;
    }

    if ($('rolesConditionInverted')) {
      $('rolesConditionInverted').checked = false;
    }
  },

  usersSelected: function() {
    if (this.getSelectedUsers().length == 0) {
      alert("Please select at least one user.");
      return false;
    }

    return true;
  },

  getSelectedUsers: function () {
    return BS.Util.getSelectedValues($('filterForm'), "userId");
  },

  addSelected: function() {
    if (BS.UserListForm.usersSelected()) {
      BS.AttachToGroupsDialog.showAttachUsersDialog(BS.UserListForm.getSelectedUsers(), function() {
        $('userTable').refresh();
      })
    };
    return false;
  }
});
