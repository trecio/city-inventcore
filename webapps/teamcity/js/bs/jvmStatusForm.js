BS.JvmStatusForm = OO.extend(BS.AbstractWebForm, {
  formElement: function() {
    return $('jvmStatusForm');
  },

  savingIndicator: function() {
    return $('progressIndicator');
  },

  takeMemoryDump: function() {
    if (!confirm("This operation can take up to several minutes.\nDuring this period TeamCity will not respond to any requests.\nAre you sure you want to continue?")) return;

    this.formElement().actionName.value = 'memoryDump';
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      memoryDumpFailed: function(elem) {
        $('errorsHolder').innerHTML = elem.firstChild.nodeValue;
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        if (!err) {
          BS.reload(true);
        }
      }
    }));
  },

  takeThreadDump: function() {

    this.formElement().actionName.value = 'threadDump';
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      threadDumpFailed: function(elem) {
        $('errorsHolder').innerHTML = elem.firstChild.nodeValue;
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        if (!err) {
          BS.reload(true);
        }
      }
    }));
  },

  loadPreset: function() {
    this.formElement().actionName.value = 'loadPreset';
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      presetNotFound: function(elem) {
        $('errorsHolder').innerHTML = elem.firstChild.nodeValue;
      },

      presetNotSelected: function(elem) {
        $('errorsHolder').innerHTML = elem.firstChild.nodeValue;
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        if (!err) {
          BS.reload(true);
        }
      }
    }));
  }
});