(function($) {

  $.widget("teamcity.autocomplete", $.ui.autocomplete, {

    options: {
      /** should autocomplete arise on focus */
      showOnFocus: false,
      /** should widget highlight match, or match already highlighted on server (default) -- not implemented yet */
      highlightMatch: false,
      /** should widget display dropdown with 'No suggestions found' when no completion found */
      showEmpty: true,
      /** should autocomplete arise on Ctrl-Space even if not in completion position; when invoked with this option $j(input).data('ctrlSpace') will return true */
      completeOnCtrlSpace: null,
      /** set field value to the autocompletion item that get focus, or not */
      insertItemWithFocus: true,
      /** predicat to test if we are in the completion possition, by default always return true, so every keydown will cause a completion */
      isInCompletionPosition: function(event) {return true;},
      /** function to get next portion of completion, will be called when completion menu is scrolled closer to the end,
       * if null - will not be called, signature: more(from, callback), from - is last element in menu */
      more: null
      // + options from $.ui.autocomplete widget
    },

    readTerm: function() {
      this.term = this.element.val();
    },

    //redefine _create because we need special handling of escape key.
    //If espace is pressed when autocompletion was open - do not bubble up this event,
    //so if autocompletion is used in modal dialog - dialog will not be closed.
    _create: function() {
      var self = this;
      var input = this.element;
      self.isCtrl = false;

      if (!this.options.insertItemWithFocus) {
        this.options.focus = function() {
          return false;
        }
      }

      $.ui.autocomplete.prototype._create.apply(self);
      $(input).off('keydown.autocomplete').on('keydown.autocomplete', function(event) {
        if (self.options.disabled) {
          return;
        }

        var keyCode = $.ui.keyCode;
        switch (event.keyCode) {
          case keyCode.LEFT:
          case keyCode.RIGHT:
          case keyCode.HOME:
          case keyCode.END:
            if (self.menu.element.is(":visible") && self.nothingFound) {
              $(input).autocomplete("close");
            }
            break;
          case keyCode.PAGE_UP:
            if (self.menu.element.is(":visible")) {
              if (self.nothingFound) {
                $(input).autocomplete("close");
              } else {
                self._move("previousPage", event);
                event.preventDefault();
              }
            }
            break;
          case keyCode.PAGE_DOWN:
            if (self.menu.element.is(":visible")) {
              if (self.nothingFound) {
                $(input).autocomplete("close");
              } else {
                self._move("nextPage", event);
                event.preventDefault();
              }
            }
            break;
          case keyCode.UP:
            if (self.menu.element.is(":visible")) {
              if (self.nothingFound) {
                $(input).autocomplete("close");
              } else {
                self._move("previous", event);
                // prevent moving cursor to beginning of text field in some browsers
                event.preventDefault();
              }
            }
            break;
          case keyCode.DOWN:
            if (self.menu.element.is(":visible")) {
              if (self.nothingFound) {
                $(input).autocomplete("close");
              } else {
                self._move("next", event);
                // prevent moving cursor to end of text field in some browsers
                event.preventDefault();
              }
            }
            break;
          case keyCode.ENTER:
          case keyCode.NUMPAD_ENTER:
            if (!self.menu.active) {
              if (self.menu.element.is(":visible")) {
                self.close(event);
              }
              return;
            }
            // when menu is open or has focus
            if (self.menu.element.is(":visible")) {
              event.preventDefault();
            }
          //passthrough - ENTER and TAB both select the current element
          case keyCode.TAB:
            if (!self.menu.active) {
              return;
            }
            self.menu.select(event);
            break;
          case keyCode.ESCAPE:
            if (self.menu.element.is(":visible")) {
              self.element.val(self.term);
              self.close(event);
            }
            break;
          default:
            if (self.options.isInCompletionPosition(event) || $(input).data('ctrlSpace')) {
              // keypress is triggered before the input value is changed
              clearTimeout(self.searching);
              self.searching = setTimeout(function() {
                // only search if the value has changed
                if (self.term != self.element.val()) {
                  self.selectedItem = null;
                  self.search(null, event);
                }
              }, self.options.delay);
              break;
            }
        }
      });


      $(input).keyup(function(event) {
        //handle escape key : prevent close of dialog with autocompletion field
        if (event.keyCode === $.ui.keyCode.ESCAPE) {
          if (self.menu.element.is(":visible") || self.justClosed) {
            self.justClosed = false;
            event.stopPropagation();
          }
        } else {
          self.justClosed = false;
        }
        return true;
      });

      if (this.options.completeOnCtrlSpace) {
        $(input).keydown(function(event) {
          if (event.keyCode === $.ui.keyCode.CONTROL) {
            self.isCtrl = true;
          } else if (self.isCtrl && event.keyCode === $.ui.keyCode.SPACE) {
            $(input).data('ctrlSpace', true);
            $(input).autocomplete("search");
            return false;
          }
        }).keyup(function(event) {
          if (event.keyCode === $.ui.keyCode.CONTROL) {
            self.isCtrl = false;
          }
        });
      }

      if (this.options.showOnFocus) {
        $(input).focus(function() {
          //this check prevents reopen of autocompletion when user click on it, instead of type Enter
          if (!self.menu.active) {
            $(input).autocomplete("search");
          }
        });
      }
    },

    //redefine _renderItem to allow html in label and to show meta info on item
    _renderItem: function(ul, item) {
      var item_line = $("<a></a>");
      if (item.meta) {
        item_line.append('<span class="ui-autocomplete-meta-info">' + item.meta + '</span>');
      }
      item_line.append(item.label);
      return $("<li></li>")
            .data("item.autocomplete", item)
            .append(item_line)
            .appendTo(ul);
    },

    _renderSeparator: function(ul, item) {
      var item_line = $('<li class="ui-menu-separator"></li>');
      if (item.meta) {
        item_line.append('<span class="ui-autocomplete-meta-info">' + item.meta + '</span>');
      }
      item_line.append(item.label);
      item_line.appendTo(ul);
    },

    //redefine _renderMenu to show message that nothing found
    _renderMenu: function(ul, items) {
      var self = this;
      if (items.length != 0) {
        $.each(items, function(index, item) {
          if (item.selectable) {
            self._renderItem(ul, item);
          } else {
            self._renderSeparator(ul, item);
          }
        });
        this.nothingFound = false;
      } else {
        ul.append("<li class='ui-autocomplete-nothing'>No suggestions found</li>");
        this.nothingFound = true;
      }
    },

    //redefine it to show popup with 'Nothing found' if there is no suggestions
    //original function show popup only if there are variants
    _response: function(content) {
      if (content.length || this.options.showEmpty) {
        content = this._normalize(content);
        this._suggest(content);
        this._trigger("open");
      } else {
        this.close();
      }
      this.element.removeClass("ui-autocomplete-loading");
    },

    //redefine to take menu scroll into account, it seems to be the reason of TW-14337
    _suggest: function(items) {
      var ul = this.menu.element.empty().zIndex(this.element.zIndex() + 1),
          menuWidth,
          textWidth;
      this._renderMenu(ul, items);
      // TODO refresh should check if the active item is still in the dom, removing the need for a manual deactivate
      this.menu.deactivate();
      this.menu.refresh();
      this.menu.element.show().position($.extend({of: this.element}, this.options.position));

      menuWidth = ul.width("").outerWidth();
      if (this.menu.hasScroll()) {
        $(this.menu.element).scrollTop(0);
        menuWidth = menuWidth + 20;
        this.askMore = true;
      }
      textWidth = this.element.outerWidth();
      ul.outerWidth(Math.max(menuWidth, textWidth));
      if (this.options.more && items.length > 0)
        this._bindMenuScrollHandler(ul, items[items.length - 1].value);
	},

    _bindMenuScrollHandler: function(ul, lastItem) {
      var itemsSize = $(ul).children().size();
      var itemHeight = $(ul).children().first().height();
      var loadMoreOffset = itemsSize * itemHeight * 0.5;
      var self = this;
      $(ul).unbind('scroll');
      var haveRun = false;
      $(ul).scroll(function() {
        var scrollFromTop = $(ul).scrollTop();
        if (self.askMore && scrollFromTop >= loadMoreOffset) {
          if (!haveRun) {
            haveRun = true;
            self._loadMore(ul, lastItem);
          }
        }
      });
    },

    _loadMore: function(ul, lastItem) {
      var self = this;
      this.options.more(lastItem, function(items) {
        if (items.length > 0) {
          var menuWidth, textWidth;
          self._renderMenu(ul, items);
          self.menu.refresh();
          menuWidth = ul.width("").outerWidth();
          if (self.menu.hasScroll()) {
            menuWidth = menuWidth + 20
          }
          textWidth = self.element.outerWidth();
          ul.outerWidth(Math.max(menuWidth, textWidth));
          if (items.length > 0 )
            self._bindMenuScrollHandler(ul, items[items.length - 1].value);
        } else {
          self.askMore = false;
          $(self.menu).unbind('scroll');
        }
      });
    },

    //redefine close to set associate data with element
    close: function(event) {
      clearTimeout(this.closing);
      if (this.menu.element.is(":visible")) {
        this._trigger("close", event);
        this.menu.element.hide();
        this.menu.deactivate();
        this.justClosed = true;
      }
      this.isCtrl = false;
      if (this.options.completeOnCtrlSpace) {
        $j(this.element).data("ctrlSpace", false);
      }
      $j(this.element).data("showAll", false);
    },

    /* override it to ignore any moves when nothing found */
    _move: function(direction, event) {
      if (this.nothingFound) {
        return;
      }
      var self = this;
      $.ui.autocomplete.prototype._move.apply(self, [direction, event]);
    }
  });

} (jQuery));