/*
 * Copyright 2000-2012 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
* Generic popup component which supports everything ;))
* */
BS.Popup = function(elementId, options) {
  this._name = elementId;
  options = options || {};

  var user_shift = options.shift || {};
  delete options.shift;

  this.options = _.extend({

    delay: 300,     // msecs before popup is shown
    hideDelay: 600, // msecs before popup is hidden on mouse out; -1 to disable hiding
    width: null,    // if not null, corresponding width is set to the popup

    // positioning options, depend on how you show a popup, either
    // with showPopup or showPopupNearElement
    shift: {
      x: -200,
      y: 15
    },

    loadingText: "Loading...",

    fitWithinWindowHeight: false,   // set to true if popup window height should be limited by the current window height
    fitWithinWindowBottomMargin: 0, // margin below the bottom part of the popup
    fitWithinWindowTopMargin: 5,    // margin above the bottom part of the popup (we'll scroll the page)
    doScroll: true,                 // set to false if you do not need scroll on open popup
    forceReload: false,             // set to true if a specific dialog needs to be reloaded/redisplayed even if already visible

    // Content options. If none provided, current content of the element is shown in the popup
    // Priority: innerText, url, textProvider, htmlProvider
    innerText: null,
    url: null,
    parameters: null,
    textProvider: null, // function(popup) { return "<b>some html</b>"; },
    htmlProvider: null, // function(popup) { return jQuery("<b>some html</b>"); }

    // Function is called after the content is loaded
    afterShowFunc: function(popup) {},

    // Function is called after the popup is hidden
    afterHideFunc: function() {}

  }, options);

  _.extend(this.options.shift, user_shift);
  this._timer = null;
};

_.extend(BS.Popup.prototype, {

  // Popup DOM element
  element: function() {
    return $(this._name);
  },

  // Show this popup near the mouse event location
  showPopup: function(event, moreOptions) {
    _.extend(this.options, moreOptions || {});

    var element = Event.element(event);

    // Don't re-trigger popup if it's already displayed
    if (!this.options.forceReload && this.isShown() && this.isLoaded() && element.getAttribute('data-popup') == this._name) {
      BS.Hider.stopHidingDiv(this._name);
      return;
    }

    var x = Event.pointerX(event) + this.options.shift.x;
    var y = Event.pointerY(event) + this.options.shift.y;

    var onShown = function() {
      // Store popup id in the toggle element
      element.setAttribute('data-popup', this._name);
    }.bind(this);

    this._showWithDelay(x, y, onShown);

    return this;
  },

  // Show this popup near given element
  showPopupNearElement: function(element, moreOptions) {
    _.extend(this.options, moreOptions || {});

    element = $j($(element));

    // Don't re-trigger popup if it's already displayed
    if (!this.options.forceReload && this.isShown() && this.isLoaded() && element.attr('data-popup') == this._name) {
      BS.Hider.stopHidingDiv(this._name);
      return;
    }

    var pos = element.offset();

    var x = pos.left + this.options.shift.x;
    var y = pos.top + this.options.shift.y;

    var onShown = function() {
      // Store popup id in the toggle element
      element.attr('data-popup', this._name);
    }.bind(this);

    this._showWithDelay(x, y, onShown);

    return this;
  },

  isLoaded: function() {
    return this._loaded === undefined || this._loaded;
  },

  hidePopup: function(delay, force) {
    this._clearPopupTimer();
    if (delay === undefined) delay = this.options.hideDelay;

    if (this.isShown() && (this.isLoaded() || force)) {
      if (delay > 0) {
        BS.Hider.startHidingDiv(this._name, delay);
      }
      else {
        BS.Hider.stopHidingDiv(this._name);
        BS.Hider.hideDivSingle(this._name);
      }
    }
  },

  stopHidingPopup: function() {
    if (this.isShown()) {
      BS.Hider.stopHidingDiv(this._name);
    }
  },

  isShown: function() {
    return BS.Util.visible(this.element());
  },

  _getMaxHeight: function() {
    if (this.options.fitWithinWindowHeight) {
      var popupPos = $(this.element()).cumulativeOffset();
      var height = BS.Util.windowSize()[1] - this.options.fitWithinWindowBottomMargin;
      height = height - (popupPos[1] - BS.Util._scrollTop());
      return height > 100 ? height : 100;
    }

    return -1;
  },

  _showWithDelay: function(x, y, callback) {
    if (this.isShown() && !this.isLoaded()) {
      _.isFunction(callback) && callback();
      return;
    }

    var delay = this.options.delay;
    this._clearPopupTimer();

    if (delay == 0) {
      this._showPopupNow(x, y, callback);
    } else {
      this._timer = setTimeout(
        function() {
          this._showPopupNow(x, y, callback);
        }.bind(this)
      , delay);
    }
  },

  _clearPopupTimer: function() {
    if (this._timer) {
      clearTimeout(this._timer);
      delete this._timer;
    }
  },

  _showPopupNow: function(x, y, showWithDelayCallback) {
    var div = this.element();
    if (!div) {
      div = document.createElement("div");
      div.id = this._name;
      div.className = 'popupDiv';
      document.body.appendChild(div);
    }
    else {
      if (div.parentNode != document.body) {
        div.remove();
        document.body.appendChild(div);
      }
    }

    div = $(div);
    if (this.options.className) {
      div.addClassName(this.options.className);
    }

    this._oldBackground = div.getStyle("backgroundColor");

    if (this.options.innerText) {
      div.update(this.options.innerText);
    }
    else if (this.options.url || this.options.textProvider) {
      var loadingText = ' ' + jQuery.trim(this.options.loadingText);
      div.addClassName('popupLoading');
      div.innerHTML = BS.loadingIcon + loadingText;
    }

    if (this.options.beforeShow) {
      this.options.beforeShow(div);
    }
    BS.Util.place(div, x, y);

    if (this._getMaxHeight() != -1) {
      div.style.overflow = 'hidden';
      div.style.height = 'auto';
    }

    this._loaded = false;

    BS.Hider.showDivWithTimeout(div.id, _.extend({
      hideOnMouseOut: function() {
        return this.options.hideDelay >= 0 && this._loaded;
      }.bind(this)
    }, this.options));

    if (this.options.url) {
      BS.ajaxUpdater(this._name, this.options.url, {
        method: 'get',
        evalScripts: true,
        parameters: this.options.parameters,
        onComplete: function() {
          div.removeClassName('popupLoading');
          this._onComplete(showWithDelayCallback);
        }.bind(this)
      });
    }
    else {
      div.removeClassName('popupLoading');

      if (this.options.textProvider) {
        div.innerHTML = this.options.textProvider(this);
      }

      if (this.options.htmlProvider) {
        $j(div).append(this.options.htmlProvider(this));
      }

      this._initEvents();
      this._onComplete(showWithDelayCallback);
    }
  },

  _onComplete: function(showWithDelayCallback) {
    this.element().style.backgroundColor = this._oldBackground;
    if (this.options.width) {
      this.element().style.width = this.options.width;
    }
    
    this._fixHeight();
    this.updatePopup();
    this.options.afterShowFunc(this);
    _.isFunction(showWithDelayCallback) && showWithDelayCallback();
    this._loaded = true;
  },

  _fixHeight: function() {
    var container = this.element();
    var maxHeight = this._getMaxHeight(container);
    if (maxHeight > 0 && container.offsetHeight > maxHeight) {
      if (this.options.doScroll) {
        window.scrollTo(0, container.cumulativeOffset()[1] - this.options.fitWithinWindowTopMargin);
      }
      container.style.height = this._getMaxHeight(container) + 'px';
      container.style.overflowY = "auto";
    }
  },

  // ensures that popup fits the page and updates popup iframe if needed
  // should be called each time after popup content changes
  updatePopup: function() {
    BS.Util.shiftToFitPage(this.element());
    BS.Util.moveDialogIFrame(this.element());
  },

  _initEvents: function() {
    var that = this;

    if (_.isString(this.options.className) && this.options.className.indexOf('quickLinksMenuPopup') > -1) {
      var $element = jQuery(this.element());

      $element.on("mouseup", function(e) {
        var rightclick;
        if (e.which) rightclick = (e.which == 3);
        else if (e.button) rightclick = (e.button == 2);

        if (!rightclick) {
          setTimeout(function() {
            BS.Hider.hideDivSingle(that._name);
          }, 0);
        }
      });

      this._initKeyboardEvents($element);
    }
  },

  _initKeyboardEvents: function($element) {
    function selectNextItem(el) {
      var currentItem = jQuery(el).closest('.menuItem'),
          nextItem = currentItem.next();

      if(nextItem.length > 0 && nextItem.hasClass('menuItem')) {
        currentItem.removeClass('menuItemSelected');
        nextItem.addClass('menuItemSelected');
        nextItem.find('a').focus();
      }
    }

    function selectPreviousItem(el) {
      var currentItem = jQuery(el).closest('.menuItem'),
          previousItem = currentItem.prev();

      if(previousItem.length > 0 && previousItem.hasClass('menuItem')) {
        currentItem.removeClass('menuItemSelected');
        previousItem.addClass('menuItemSelected');
        previousItem.find('a').focus();
      }
    }

    var menuItems = $element.find('.menuItem');

    menuItems.first().addClass('menuItemSelected');
    menuItems.first().find('a').focus();

    menuItems.keydown(function(e) {
      switch(e.keyCode) {
        case 38:
            selectPreviousItem(e.target);
            return false;

        case 40:
            selectNextItem(e.target);
            return false;

        default:
            return true;
      }
    });
  }
});



//-------------------------------------------------------------------------
//  Some Specific popups:

BS.ChangesPopup = new BS.Popup("changesPopup", {
  url: window['base_uri'] + "/changesPopup.html?limit=25",
  method: "get"
});

BS.ChangesPopup._switchTab = function(container, promoId, tab) {
  container.style.overflow = 'hidden';
  container.style.height = 'auto';
  container.innerHTML = BS.loadingIcon + " Loading...";
  this.updatePopup();

  var that = this;
  setTimeout(function() {
    new BS.ajaxUpdater(container, window['base_uri'] + "/changesPopupTab.html", {
      method: 'get',
      evalScripts: true,
      parameters: "limit=25&promoId=" + promoId + "&tab=" + tab,
      onComplete: function() {
        that.updatePopup();
      }
    });
  }, 500);
};

BS.ChangesPopup.showCurrentBuild = function(container, promoId) {
  this._switchTab(container, promoId, "");
};

BS.ChangesPopup.showSinceLastSuccessful = function(container, promoId) {
  this._switchTab(container, promoId, "sinceLastSuccessful");
};

BS.ChangesPopup.showBuildChangesPopup = function(nearestElement, promoId, tab) {
  this.options.parameters = "promoId=" + promoId + (tab != null ? "&tab=" + tab : "");
  this.showPopupNearElement(nearestElement);
};

BS.ChangesPopup.showPendingChangesPopup = function(nearestElement, buildTypeId, allBranches, branch) {
  this.options.parameters = "buildTypeId=" + buildTypeId;
  if (allBranches) {
    this.options.parameters += "&allBranches=true";
  }
  if (branch != null) {
    this.options.parameters += "&branch=" + encodeURIComponent(branch);
  }
  this.showPopupNearElement(nearestElement);
};

BS.ChangesPopup.expandComment = function(elem) {
  $j(elem)
      .parent().hide()
      .next().show();
  return false;
};

BS.ChangesPopup.initArtifactChangesBlocks = function () {
  var artifactDescriptionCaptions = $j('.artifactsChangeHeader');

  artifactDescriptionCaptions.click(function() {
    var caption = $j(this);

    caption.toggleClass('expanded').toggleClass('collapsed');
    caption.parent().find(".artifactChange").toggleClass('hidden');
  });

  artifactDescriptionCaptions.mousedown(function() {
    return false;
  });
};


BS.FilesPopup = new BS.Popup("filesPopup", {
  url: window['base_uri'] + "/filesPopup.html",
  shift: {x: -300, y: 10}
});


BS.BuildResultsSummary = function(buildId, buildTypeId, finished, skipChangesArtifacts) {
  BS.Popup.call(this, "popupRes" + buildId, {
    buildId: buildId,
    skipChangesArtifacts: skipChangesArtifacts,
    buildTypeId: buildTypeId,

    shift: {x: -80},

    textProvider: function(popup) {
      var template = $('buildResultsSummaryTemplate');
      if (!template) return;

      var text = template.innerHTML;
      text = text.replace(/##BUILD_ID##/g, popup.options.buildId);
      text = text.replace(/##BUILD_TYPE_ID##/g, popup.options.buildTypeId);

      setTimeout(function() { this.showDetailedInfo(); }.bind(popup), 20);
      return text;
    }
  })
};

_.extend(BS.BuildResultsSummary.prototype, BS.Popup.prototype);

BS.BuildResultsSummary.prototype.showDetailedInfo = function() {
  this._loaded = false;
  var buildId = this.options.buildId;

  $('summaryProgress:' + buildId).innerHTML = BS.loadingIcon + " Loading details...";

  var ajaxOptions = {
    evalScripts: true,
    onComplete: function() {
      if ($('summaryProgress:' + buildId))
        $('summaryProgress:' + buildId).innerHTML = "";
      this.updatePopup();
      this._loaded = true;
    }.bind(this)
  };

  BS.ajaxUpdater($('detailedSummary:' + buildId),
                 window['base_uri'] + "/buildResultsSummary.html?buildId=" + buildId + "&skipChangesArtifacts=" + this.options.skipChangesArtifacts,
                 ajaxOptions);
};


BS.BuildTypeSummary = new BS.Popup("buildTypeSummary", {
  shift: {x: 5, y: 20},

  textProvider: function(popup) {
    if (!$('buildTypeMenuTemplate')) return "";
    var text = $('buildTypeMenuTemplate').innerHTML;
    text = text.replace(/##BUILD_TYPE_ID##/g, popup.options.buildTypeId);
    text = text.replace(/##BUILD_TYPE_NAME##/g, popup.options.buildTypeName.replace(/['"]/g, '`').escapeHTML());
    text = text.replace(/##PROJECT_ID##/g, popup.options.projectId);

    // Also inject branch name to the build type links: /viewType.html?buildTypeId=[popup.options.buildTypeId]
    var branch = BS.Branch.getBranchFromUrl(popup.options.projectId);
    var replace = (branch) ? "&branch_" + popup.options.projectId + "=" + branch : "";
    text = text.replace(/##BRANCH##/g, replace);

    if (!popup.options.withSelf) {
      text = text.replace(/data\-self="true"/g, "style='display:none'");
    }
    if (!popup.options.withAdmin) {
      text = text.replace(/data\-admin="true"/g, "style='display:none'");
    }
    if (!popup.options.showResponsibility || BS.Branch.isCustomBranch(branch)) {
      text = text.replace(/data\-responsibility="true"/g, "style='display:none'");
    }

    return text;
  }
});

BS.BuildTypeSummary.showSummaryPopup = function(nearestElement, buildTypeId, projectId, withSelf, withAdmin, showResponsibility, buildTypeName) {
  this.options.buildTypeId = buildTypeId;
  this.options.buildTypeName = buildTypeName;
  this.options.projectId = projectId;
  this.options.withSelf = withSelf;
  this.options.withAdmin = withAdmin;
  this.options.showResponsibility = showResponsibility;
  this.showPopupNearElement(nearestElement);
};


BS.AgentInfoPopup = new BS.Popup("agentInfoPopup", {
  url: window['base_uri'] + "/showCompatibleAgents.html",
  shift: {x: -150}
});

BS.AgentInfoPopup.showAgentsPopup = function(nearestElement, itemId) {
  this.options.parameters = "itemId=" + itemId;
  this.showPopupNearElement(nearestElement);
};


BS.ArtifactsPopup = new BS.Popup("artifactsPopup", {
  shift: {x: -60},
  url: window['base_uri'] + "/viewArtifactsPopup.html"
});

BS.ArtifactsPopup.showArtifactsPopup = function(nearestElement, buildTypeId, buildId) {
  BS.ArtifactsPopup.options.parameters = "buildId=" + buildId;
  BS.ArtifactsPopup.showPopupNearElement(nearestElement);
};

BS.DependentArtifactsPopup = new BS.Popup("dependentArtifactsPopup", {
  shift: {x: -60},
  url: window['base_uri'] + "/viewDependentArtifactsPopup.html"
});
BS.DependentArtifactsPopup.showPopup = function(nearestElement, buildId, targetBuildId, mode) {
  this.options.parameters = "buildId=" + buildId + "&targetBuildId=" + targetBuildId + "&mode=" + mode;
  this.showPopupNearElement(nearestElement);
};


BS.BuildTypeSettingsPopup = {};
BS.BuildTypeSettingsPopup.showMultilineValue = function(element, title, text) {
  BS.Util.center('valuePopup');
  $('valueContainer').value = text;
  $('valuePopupTitle').innerHTML = title;
  BS.Hider.showDivWithTimeout('valuePopup', {
    hideOnMouseOut: false,
    draggable: true,
    dragHandle: $('valuePopupHeader')
  });

  $('valueContainer').focus();
};

BS.BuildTypeSettingsPopup.closeMultilineValuePopup = function() {
  BS.Hider.hideDiv('valuePopup');
};


BS.ProgressPopup = new BS.Popup("progressPopup", {
  delay: 0,
  hideDelay: 0
});

BS.ProgressPopup.showProgress = function(nearestElem, message, options) {
  message = jQuery.trim(message);

  this.options.innerText = "<nobr>" + BS.loadingIcon + " " + message + "</nobr>";
  if (this.options.delay === undefined) {
    this.options.delay = 0;
  }

  _.extend(this.options, options);
  this.showPopupNearElement(nearestElem);
};


BS.WarningPopup = new BS.Popup("warningPopup", {
  delay: 0,
  hideOnMouseOut: false
});

BS.WarningPopup.showWarning = function(nearestElem, shift, message, moreOptions) {
  this.options.innerText = message;
  this.options.shift = shift;
  setTimeout(function() {
    this.showPopupNearElement(nearestElem, moreOptions)
  }.bind(this), 50)
};


/* simple popup for displaying messages */
BS.Tooltip = {
  _id: 1,

  getId: function() {
    return "messagePopup_" + this._id++;
  }
};

BS.Tooltip.create = function(options, message) {
  if  (BS.Tooltip.popup) return null;

  options = _.extend({
                            delay: options.delay || 400,
                            shift: options.shift,
                            hideOnMouseOut: true,
                            hideOnMouseClickOutside: false,
                            innerText: message
                          }, options);

  BS.Tooltip.popup = new BS.Popup(BS.Tooltip.getId(), options);
  return BS.Tooltip.popup;
};

BS.Tooltip.showMessage = function(nearestElem, options, message) {
  var popupToggle = $(nearestElem);

  var popup = BS.Tooltip.create(options, message);
  if (popup) popup.showPopupNearElement(popupToggle);
  BS.Tooltip.popupToggle = popupToggle;
  BS.Tooltip.deleteTitles(popupToggle);
};

BS.Tooltip.showMessageFromContainer = function(nearestElem, options, container) {
  BS.Tooltip.showMessage(nearestElem, options, $(container).innerHTML);
};

BS.Tooltip.showMessageAtCursor = function(event, options, message) {
  var popupToggle = $(event.target);

  var popup = BS.Tooltip.create(options, message);
  if (popup) popup.showPopup(event);
  BS.Tooltip.popupToggle = popupToggle;
  BS.Tooltip.deleteTitles(popupToggle);
};

BS.Tooltip.hidePopup = function() {
  if (BS.Tooltip.popup) {
    BS.Tooltip.popup.hidePopup(BS.Tooltip.popup.options.delay, true);
    BS.Tooltip.popup = null;
  }

  if (BS.Tooltip.popupToggle) {
    BS.Tooltip.recreateTitles(BS.Tooltip.popupToggle);
  }
};

// See TW-11045. Here `3` is a magic number controlling JS performance. The greater the value, the more accurate
// the result will be, the slower the method. But I have never seen that a higher parent has the `title`.
BS.Tooltip.deleteTitles = function(elem) {
  if (!elem) return;
  elem = elem.up();
  for (var i = 0; i < 3; ++i) {
    if (!elem) return;
    if (elem.getAttribute("title")) {
      elem.setAttribute("data-original-title", elem.getAttribute("title"));
      elem.removeAttribute("title");
    }
    elem = elem.up();
  }
};

BS.Tooltip.recreateTitles = function(elem) {
  if (!elem) return;
  elem = elem.up();
  for (var i = 0; i < 3; ++i) {
    if (!elem) return;
    if (elem.getAttribute("data-original-title")) {
      elem.setAttribute("title", elem.getAttribute("data-original-title"));
    }
    elem = elem.up();
  }
};

BS.QueuedBuildsPopup = new BS.Popup("queuedBuildsPopup", {
  url: window['base_uri'] + "/queue.html",
  method: "get"
});

BS.QueuedBuildsPopup.showQueuedBuilds = function(nearestElem, buildTypeId, branch) {
  var parameters = "forBuildTypeId=" + buildTypeId;
  if (branch != null) {
    parameters += "&branch=" + encodeURIComponent(branch);
  }
  this.showPopupNearElement(nearestElem, {parameters: parameters, shift: {x: -600, y: 15}});
};

BS.RunningBuildsPopup = new BS.Popup("runningBuildsPopup");

BS.RunningBuildsPopup.showBuilds = function(nearestElem, buildTypeId) {
  var container = $j(BS.Util.escapeId("btb" + buildTypeId));

  this.showPopupNearElement(nearestElem, {
    shift: {x: -800, y: 15},
    afterShowFunc: function() {
      var popup = $j("#runningBuildsPopup");

      // The popup may still contain the running builds for the previous build type
      // (happens when user triggers several popups in a row quickly).
      // In this case we should move the builds back to the corresponding container *before*
      // inserting builds for this build type.
      // A kind of hacky fallback. Normally the `if` should be false.
      if (!popup.is(":empty")) {
        var btId = popup.attr("bt-id");
        if (btId == buildTypeId) return;
        popup.children("table").appendTo(BS.Util.escapeId("btb" + btId));
      }

      popup.append(container.children("table")).attr("bt-id", buildTypeId);
    },
    afterHideFunc: function() {
      $j("#runningBuildsPopup").children("table").appendTo(container);
    }
  });
};

BS.ClickPopupSupport = {
  clickPopups: {},

  togglePopup: function(id, showPopupCommand) {
    var popup = BS.ClickPopupSupport.clickPopups[id];
    if (!popup || !popup.isShown()) {
      popup = eval(showPopupCommand);
      BS.ClickPopupSupport.clickPopups[id] = popup;

      var linkPos = $(this).cumulativeOffset();
      var linkDim = $(this).getDimensions();
      var elDim = popup.element().getDimensions();

      BS.Util.place(popup.element(), linkPos[0] + linkDim.width - elDim.width, linkPos[1] + linkDim.height - 1);
      popup.updatePopup();

      BS.Hider.addHideFunction(popup._name, function() {
        delete BS.ClickPopupSupport.clickPopups[id];
      }.bind(this));
    }
    else {
      popup.hidePopup(0, true);
    }
  }
};


BS.PromoDetailsPopup = new BS.Popup("promoDetailsPopup", {
  url: window['base_uri'] + "/promoDetailsPopup.html",
  method: "get",
  shift: {x: -50}
});

BS.PromoDetailsPopup.showDetailsPopup = function(nearestElement, promoId) {
  BS.PromoDetailsPopup.options.parameters = "promoId=" + promoId;
  BS.PromoDetailsPopup.showPopupNearElement(nearestElement);
};


BS.AuthorityRolesPopup = {};
BS.AuthorityRolesPopup = new BS.Popup("authorityRolesPopup", {
  url: window['base_uri'] + "/admin/authorityRolesPopup.html",
  method: "get"
});

BS.AuthorityRolesPopup.showPopup = function(nearestElement, userId) {
  this.options.parameters = "rolesHolderId=" + userId;
  this.showPopupNearElement(nearestElement);
};


BS.GroupUsersPopup = {};

BS.GroupUsersPopup = new BS.Popup("groupUsersPopup", {
  url: window['base_uri'] + "/admin/groupUsersPopup.html",
  method: "get"
});

BS.GroupUsersPopup.showPopup = function(nearestElement, groupCode) {
  this.options.parameters = "groupCode=" + encodeURIComponent(groupCode);
  this.showPopupNearElement(nearestElement);
};


BS.ParentGroupsPopup = {};
BS.ParentGroupsPopup = new BS.Popup("parentGroupsPopup", {
  url: window['base_uri'] + "/admin/parentGroupsPopup.html",
  method: "get"
});

BS.ParentGroupsPopup.showUserParentGroups = function(nearestElement, userId) {
  this.options.parameters = "userId=" + userId;
  this.showPopupNearElement(nearestElement);
};

BS.ParentGroupsPopup.showGroupParentGroups = function(nearestElement, groupCode) {
  this.options.parameters = "groupCode=" + encodeURIComponent(groupCode);
  this.showPopupNearElement(nearestElement);
};


BS.TemplateUsagesPopup = new BS.Popup("templateUsagesPopup", {
  url: window['base_uri'] + "/admin/templateUsagesPopup.html",
  method: "get"
});

BS.TemplateUsagesPopup.showPopup = function(nearestElement, templateId, selectedStep) {
  this.options.parameters = "templateId=" + encodeURIComponent(templateId) + "&selectedStep=" + selectedStep;
  this.showPopupNearElement(nearestElement);
};


BS.ShowBuildTypesPopup = new BS.Popup('showBuildTypesPopup', {
  hideDelay: 0,
  shift: {x: -230, y: 20},
  className: 'flatView'
});

BS.ShowBuildTypesPopup.showPopup = function(nearestElement, projectId) {
  var params = {
    projectId: projectId
  };
  BS.Branch.addBranchToParams(params, projectId);

  this.options.url = window['base_uri'] + '/showBuildTupesPopup.html?' + Object.toQueryString(params);
  this.showPopupNearElement(nearestElement);
};


BS.InstallAgentsPopup = {
  showNearElement: function(element) {
    $('installAgents').style.position = 'absolute';
    BS.Util.placeNearElement('installAgents', element, {x: -180, y: 21});
    BS.Hider.showDivWithTimeout('installAgents', {hideOnMouseOut: false});
  }
};


BS.TogglePopup = new BS.Popup('togglePopup', {
  delay: 0,
  hideDelay: -1,
  shift: {x: 0}
});

BS.TogglePopup.toggle = function(link) {
  if (this.isShown()) {
    this.hidePopup();
  } else {
    this.options.innerText = link.nextSibling.innerHTML;
    this.showPopupNearElement(link);
  }
  return false;
};


BS.JumpTo = new BS.Popup("jumpToPopup", {
  delay: 0,
  hideDelay: -1,
  shift: {x: -209, y: 20}
});

BS.JumpTo.jump = function(label) {
  BS.HistoryTable.update('jumpTo=' + label);
  this.hidePopup();
};


BS.PopupDialog = {
  _popups: {},

  show: function(nearestElement, popupDialogTypeId, contextParams, afterApply, moreOptions) {
    if (this._popups[popupDialogTypeId]) {
      this.hide(popupDialogTypeId, false);
    }

    var popupOptions = OO.extend({
      method: "get",
      url: window['base_uri'] + "/popupDialog.html",
      parameters: "init=1&typeId=" + encodeURIComponent(popupDialogTypeId) + "&" + contextParams,
      hideOnMouseOut: false,
      hideOnMouseClickOutside: false,
      width: '25em',
      delay: 0,
      shift: {
        x: 0,
        y: nearestElement ? nearestElement.offsetHeight + 5 : 16
      },
      hideDelay: -1
    }, moreOptions || {});

    var popup = new BS.Popup("popupDialog_" + popupDialogTypeId, popupOptions);
    popup._afterApply = afterApply;
    this._popups[popupDialogTypeId] = popup;
    popup.showPopupNearElement(nearestElement);
  },

  hide: function(popupDialogTypeId, applied) {
    var popup = this._popups[popupDialogTypeId];
    this._popups[popupDialogTypeId] = null;
    if (popup) {
      popup.hidePopup();
      if (applied && popup._afterApply) {
        popup._afterApply();
      }
    }
  }
};

/*===========================================================================*/
/*   Handler for simplePopup.tag     */
/*===========================================================================*/
BS.install_simple_popup = function(elementId, options) {
  options = options || {};

  var el = $j(BS.Util.escapeId(elementId));
  if (el.length == 0) {
    BS.Log.warn("install_simple_popup: Cannot find element with id " + elementId);
    return;
  }

  var popup = new BS.Popup(elementId + "Content", options);

  el.on("mouseenter", function() {
    popup.stopHidingPopup();
  });

  el.on("mouseenter", ".toggle", function() {
    popup.showPopupNearElement(this);
    if (!this.src.match(/_hover\.(gif|png)/)) {
      this.src = this.src.replace(/\.(gif|png)/, '_hover.$1');
    }
  });

  el.on("mouseleave", ".toggle", function() {
    popup.hidePopup(300);
    this.src = this.src.replace('_hover', '');
  });

  el.on("click", ".toggle", function() {
    return false;
  });

  el.on("click", ".popupLink", function() {
    popup.showPopupNearElement(this, {delay: 0});
  });
};

// A shortcut. Used in popupControl.tag
window._tc_es = function(e) {
  Event.element(e).setAttribute('data-pinned', 'true');
  Event.stop(e);
};

window._pc_over = function(img, hidePopupCode) {
  if (!img.src.match(/_hover\.(gif|png)/)) {
    img.src = img.src.replace(/\.(gif|png)/, '_hover.$1');
  }

  var handler = function() {
    if (hidePopupCode) eval(hidePopupCode);
    _pc_out(this)
  }.bind(img);

  $(img).on("mouseout", handler);
  $(img.parentNode).on("mouseout", handler);
};

window._pc_out = function(img) {
  img.src = img.src.replace('_hover', '');
};
