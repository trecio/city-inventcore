BS.CleanupDisableForm = OO.extend(BS.AbstractWebForm, {
  formElement: function() {
    return $('cleanupDisableForm');
  },

  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('savingSettings');
    } else {
      BS.Util.hide('savingSettings');
    }
  },

  submit: function() {
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function(form, responseXML, err) {
        if (err) {
          form.enable();
          form.setSaving(false);
        }

        if (!err) {
          BS.reload(true);
        }
      }
    }));
    return false;
  }
});

BS.CleanupPoliciesForm = OO.extend(BS.AbstractWebForm, {
  formElement: function() {
    return $('cleanupTimeForm');
  },

  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('savingSettings');
    } else {
      BS.Util.hide('savingSettings');
    }
  },

  startingCleanup: function(starting) {
    if (starting) {
      BS.Util.show('startingCleanup');
    } else {
      BS.Util.hide('startingCleanup');
    }
  },

  submitCleanupStartTime: function() {
    var that = this;
    $('cleanupPageAction0').value = 'storeSettings';

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onSaveServerConfigError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onSaveCleanupTimeError: function(elem) {
        $('error_cleanupTime').innerHTML = elem.firstChild.nodeValue;
        that.highlightErrorField('hour');
        that.highlightErrorField('minute');
      },

      onCompleteSave: function(form, responseXML, err) {
        if (err) {
          form.enable();
          form.setSaving(false);
        }

        if (!err) {
          BS.reload(true);
        }
      }
    }));
    return false;
  },

  submitStartCleanup: function() {
    if (!confirm("This operation may require significant time, and the server will be unavailable for all users.\n" +
      "Are you sure you want to start clean-up process now?")) return false;

    $('cleanupPageAction0').value = 'startCleanup';

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SimpleListener, {
      onCannotStartCleanupError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onBeginSave: function(form) {
        form.startingCleanup(true);
        form.disable();
      },

      onCompleteSave: function() {
        setTimeout(function() {
          BS.reload(true);
        }, 1500);
      }
    }));
    
    return false;
  }
});

BS.CleanupPolicyDialog = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {

  incorrectInputs : {},

  formElement: function() {
    return $('editPolicy');
  },

  getContainer: function() {
    return $('editPolicyDialog');
  },

  showDialog: function(buildTypeId, title, policies, levels, defPolicies, holdDependencies /*true|false|<empty>*/) {
    this.clearErrors();
    $('buildTypeId').value = buildTypeId;
    $('cleanupPageAction1').value = 'savePolicy';
    $('editPolicyTitle').innerHTML = title;
    $('artifactPatternsDiv').hide();
    for(var i = 0; i<levels.length; i++ ) {
      var level = levels[i];
      var policy = policies[level];
      var daysCount = 0;
      var buildsCount = 0;
      var hasDefault = defPolicies && defPolicies[level];

      $('default'+level+'text').innerHTML = hasDefault ? defPolicies[level] : 'Never<br>';
      if (!hasDefault) {
        $('defPrefix'+level).hide();
      }
      else {
        $('defPrefix'+level).show();
      }
      if(buildTypeId!='') {
        $('apply'+level).show();
      } else {
        $('apply'+level).hide();
        policy = policy || true;
      }
      if(policy) {
        $('edit'+level).show();
        $('default'+level).hide();
        daysCount = policy['keepDays.count'];
        buildsCount = policy['keepBuilds.count'];
        $('custom_C'+level).checked = true;
        $('custom_D'+level).checked = false;

        if (level == 'ARTIFACTS') {
          $('artifactPatternsDiv').show();
          var p = policy['artifactPatterns'];
          $('artifactPatterns').value = p ? p : '';
        }
      } else {
        $('edit'+level).hide();
        $('default'+level).show();
        $('custom_C'+level).checked = false;
        $('custom_D'+level).checked = true;
      }
      $('daysCount['+level+"]").value = daysCount ? daysCount : '';
      $('buildsCount['+level+"]").value = buildsCount ? buildsCount : '';
    }

    if (defPolicies != null) {
      BS.Util.hide('default_rule');
      BS.Util.show('non_default_rule');

      if (holdDependencies == 'true') {
        $('holdDependencies_true').checked = true;
      } else if (holdDependencies == 'false') {
        $('holdDependencies_false').checked = true;
      } else {
        $('holdDependencies_default').checked = true;
      }
    } else {
      BS.Util.hide('non_default_rule');
      BS.Util.show('default_rule');

      if (holdDependencies == 'true') {
        $('holdDependencies_def_true').checked = true;
      } else if (holdDependencies == 'false') {
        $('holdDependencies_def_false').checked = true;
      }
    }

    this.showCentered();
    this.recheckIntegerInputs();

    this.bindCtrlEnterHandler(this.submitCleanupPolicy.bind(this));
  },

  cancelDialog: function() {
    this.close();
  },

  submitCleanupPolicy: function() {
    var that = this;
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onFieldError: function(elem) {
        $(elem.firstChild.nodeValue).addClassName("errorField"); 
      },

      onSaveServerConfigError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onBuildTypeNotFoundError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onSaveProjectFailedError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        if (!err) {
          form.enable();
          that.close();
          $('cleanupPoliciesTable').refresh();
        }
      }
    }));

    return false;
  },


  isInputIntegerAndPositive: function (inputId) {
    var v = $(inputId).value;
    v = v.replace(/^\s+|\s+$/g, '');
    return /^[0-9]*$/.test(v) && (v >= 1 || v == "");
  },


  checkIntegerInput: function (inputId, msgId) {
    var inputOk = this.isInputIntegerAndPositive(inputId);

    if (inputOk) {
      BS.Util.hide(msgId);
      delete this.incorrectInputs[inputId];
    }
    else {
      BS.Util.show(msgId);
      this.incorrectInputs[inputId] = msgId;
    }

    this.updateInnerSaveButtonStatus();
  },


  updateInnerSaveButtonStatus: function () {
    var ok = true;                                    // guess the map incorrectInputs is empty
    for (var i in this.incorrectInputs) ok = false;   // if the map is not empty then ok == false

    if (ok) Form.Element.enable($("innerSaveButton"));
    else    Form.Element.disable($("innerSaveButton"));
  },


  recheckIntegerInputs: function () {
    var theIncorrectInputs = this.incorrectInputs; // I will update this.incorrectInputs and wanna get a copy
    for (var i in theIncorrectInputs)
      this.checkIntegerInput(i, theIncorrectInputs[i]);
  }



}));


BS.ProjectListSelector = OO.extend(BS.AbstractWebForm, {
  formElement: function() {
    return $(document.forms['projectSelectorForm']);
  },

  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('projectSelectorProgress');
    } else {
      BS.Util.hide('projectSelectorProgress');
    }
  },

  submit: function() {

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        if (!err) {
          $('cleanupPoliciesTable').refresh('projectSelectorProgress');
        }
      }
    }));
    return false;
  },

  submitStartCleanup: function() {
    if (!confirm("This operation may require significant time, and the server will be unavailable for all users.\n" +
      "Are you sure you want to start clean-up process now?")) return false;

    $('cleanupPageAction0').value = 'startCleanup';

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SimpleListener, {
      onCannotStartCleanupError: function(elem) {
        alert(elem.firstChild.nodeValue);
      },

      onBeginSave: function(form) {
        form.startingCleanup(true);
        form.disable();
      },

      onCompleteSave: function() {
        setTimeout(function() {
          BS.reload(true);
        }, 1500);
      }
    }));

    return false;
  }
});
