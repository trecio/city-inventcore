// Facility for build branches.

BS.Branch = {
  WILDCARD_NAME: "__all_branches__",  // BranchUtil.WILDCARD_BRANCH
  DEFAULT_NAME: "<default>",          // BranchUtil.DEFAULT_BRANCH_NAME and Branch.DEFAULT_BRANCH_NAME

  baseUrl: null,                      // The URL where branch links should lead to (by default, build type overview page).
  _branches: {},                      // Holds the branches per project (on a current page).

  makeDropdown: function(userBranch, enabled, id, activeBranches, otherBranches, options) {
    var select = $j("<select class='branchNameSelector' id='" + id + "'/>");
    select.prop("disabled", !enabled);

    var tab = (options || {})["tab"];
    var wildcardName = tab == "buildTypeStatusDiv" || tab == "buildTypeChains" || tab == "buildTypeHistoryList" ||
                       tab == "projectBuildChains" || tab == "testDetails" ?
                          "&lt;All branches&gt;" :
                          "&lt;Active branches&gt;";
    this._addOption(select, wildcardName, this.WILDCARD_NAME, userBranch);
    this._addOption(select, "&lt;Default branch&gt;", this.DEFAULT_NAME, userBranch);

    var i, group;
    if (activeBranches) {
      group = this._addOptionGroup(select, "Active branches");
      for (i = 0; i < activeBranches.length; ++i) {
        this._addBranchOption(group, activeBranches[i], userBranch);
      }
    }

    if (otherBranches) {
      if (activeBranches) {
        group = this._addOptionGroup(select, "Inactive branches");
      } else {
        group = select;
      }
      for (i = 0; i < otherBranches.length; ++i) {
        this._addBranchOption(group, otherBranches[i], userBranch);
      }
    }

    return select;
  },

  addChangeHandler: function(select, options) {
    var that = this,
        projectId = options.projectId;

    select.change(function() {
      var selectedBranch = $j(this).find("option:selected").attr("value");

      var url = BS.Branch._getUrl(window.location.href, selectedBranch, projectId);
      if (selectedBranch != BS.Branch.WILDCARD_NAME) {
        url = url.replace(/tab=buildTypeBranches/g, "tab=buildTypeStatusDiv");    // See TW-22583.
      }

      if (options.save) {  // overview page
        BS.User.setProperty("ui.overview.branch." + projectId, selectedBranch, {
          afterComplete: function() {
            that._branches[projectId] = selectedBranch;
            BS.Projects.updateProjectView(projectId, options.isFirst, true);
          }
        });
      } else {
        window.location.href = url;
      }
    });
  },

  installDropdownToBreadcrumb: function(userBranch, enabled, activeBranches, otherBranches, options) {
    var tabsContainer = $j("#tabsContainer3");
    var branchNameSelector = $j("#branchNameSelector");

    if (!tabsContainer.length || branchNameSelector.length) {
      return;
    }

    this._branches[options.projectId] = userBranch;
    var dropdown = this.makeDropdown(userBranch, enabled, "branchNameSelector", activeBranches, otherBranches, options);
    this.addChangeHandler(dropdown, options);

    branchNameSelector = $j("<p class='branchNameSelector'/>").append(dropdown).wrap('<li/>').parent();

    tabsContainer
        .addClass("simpleTabsWithSelector")
        .find("ul.tabs")
        .prepend(branchNameSelector);

    if (document.documentElement.className.indexOf('ua-ie') > -1) {
      $j(document).ready(function() {
        BS.jQueryDropdown(dropdown, {skin: "popup"});
      });
    } else {
      BS.jQueryDropdown(dropdown, {skin: "popup"});
    }

    this.injectBranchParamToLinks(tabsContainer, options.projectId);
  },

  installDropDownToProjectPane: function(pane, userBranch, activeBranches, otherBranches, options) {
    pane = $j(pane);

    var id = "branchNameSelector" + options.projectId;
    var select = this.makeDropdown(userBranch, true, id, activeBranches, otherBranches, options);

    this._branches[options.projectId] = userBranch;
    options.save = true;
    this.addChangeHandler(select, options);

    pane.append(select);
    BS.jQueryDropdown(select, {skin: "popup", propagateTrigger: false});
    pane.width(select.parent().outerWidth());
  },

  _addBranchOption: function(select, branch, userBranch) {
    this._addOption(select, branch.escapeHTML(), branch, userBranch);
  },

  _addOption: function(select, name, value, userBranch) {
    $j("<option/>").attr("value", value)
                   .append(name)
                   .prop("selected", userBranch == value)
                   .appendTo(select);
  },

  _addOptionGroup: function(select, name) {
    return $j("<optgroup/>").attr("label", name)
                            .appendTo(select);
  },

  injectBranchParamToLinks: function(container, projectId) {
    var branch = this.getBranchFromUrl(projectId);
    if (branch == null || branch == undefined) {
      return;
    }

    var that = this;
    container.find("a").each(function() {
      var link = this;
      var href = link.href;

      if (!href || href.startsWith("#") || href.startsWith("javascript:")) {
        return;
      }

      // One more exception. See TW-22583.
      if (href.indexOf("tab=buildTypeBranches") >= 0) {
        return;
      }

      if (href.indexOf("viewType.html") >= 0 || href.indexOf("project.html") >= 0/* || href.indexOf("viewLog.html") >= 0*/) {
        link.href = that._getUrl(href, branch, projectId);
      }
    });
  },

  setLink: function(link, buildTypeId, projectId, branch) {
    if (link.getAttribute('href') == "#") {
      var url = this.baseUrl;
      if (!url) {
        url = window["base_uri"] + "/viewType.html?buildTypeId=" + buildTypeId;
      }
      link.setAttribute("href", this._getUrl(url, branch, projectId));
    }
  },

  _getUrl: function(url, branch, projectId) {
    var query = url.indexOf("?") >= 0 ? url.toQueryParams() : {};
    query["branch_" + projectId] = branch;  // No need to escape. toQueryString() will do that.
    var basePath = url.indexOf("?") >= 0 ? url.substring(0, url.indexOf("?") + 1) : url + "?";
    return basePath + Object.toQueryString(query);
  },

  getBranchFromUrl: function(projectId) {
    return this._branches[projectId];
  },

  addBranchToParams: function(params, projectId) {
    var branch = this.getBranchFromUrl(projectId);
    if (branch) {
      params["branch_" + projectId] = branch;
    }
  },

  getAllBranchesFromUrl: function() {
    var result = {};
    for (var key in this._branches) {
      result["branch_" + key] = this._branches[key];
    }
    return result;
  },

  isCustomBranch: function(branch) {
    return branch && !(branch == this.WILDCARD_NAME || branch == this.DEFAULT_NAME);
  }
};
