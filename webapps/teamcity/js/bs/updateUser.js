
BS.UpdateUserListener = OO.extend(BS.ErrorsAwareListener, {
  onBeginSave: function(form) {
    BS.UpdateUserListener.form = form;
    BS.ErrorsAwareListener.onBeginSave(form);
  },

  onDuplicateAccountError: function(element) {
    $("errorUsername1").innerHTML = element.firstChild.nodeValue;
    BS.UpdateUserListener.form.highlightErrorField($("username1"));
  },

  onEmptyUsernameError: function(element) {
    $("errorUsername1").innerHTML = element.firstChild.nodeValue;
    BS.UpdateUserListener.form.highlightErrorField($("username1"));
  },

  onPasswordsMismatchError: function(element) {
    $("errorPassword1").innerHTML = element.firstChild.nodeValue;
    BS.UpdateUserListener.form.highlightErrorField($("password1"));
    BS.UpdateUserListener.form.highlightErrorField($("retypedPassword"));
  }
});


BS.UpdateUserForm = OO.extend(BS.AbstractPasswordForm, {
  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('saving1');
    } else {
      BS.Util.hide('saving1');
    }
  },

  saveInSession: function() {
    var that = this;
    $("submitUpdateUser").value = 'storeInSession';

    BS.PasswordFormSaver.save(this, this.formElement().action, OO.extend(BS.StoreInSessionListener, {
      onPublicKeyExpiredError: function(elem) {
        $("publicKey").value = elem.firstChild.nodeValue;
      }
    }));
  }
});


BS.UpdatePersonalProfileForm = OO.extend(BS.UpdateUserForm, {
  formElement: function() {
    return $("profileForm");
  },

  setupEventHandlers: function() {
    var that = this;

    this.setUpdateStateHandlers({
      updateState: function() {
        that.saveInSession();
      },

      saveState: function() {
        that.submitPersonalProfile();
      }
    });
  },

  submitPersonalProfile: function() {
    var that = this;
    $("submitUpdateUser").value = 'storeInDatabase';

    BS.PasswordFormSaver.save(this, this.formElement().action, OO.extend(BS.UpdateUserListener, {
      onUserNotFoundError: function(element) {
        BS.reload(true);
      },

      onCompleteSave: function(form, responseXML, errStatus) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, errStatus);

        if (!errStatus) {
          that.removeUpdateStateHandlers();
          BS.reload(true);
        }
      }
    }));

    return false;
  }
});

BS.AdminUpdateUserForm = OO.extend(BS.UpdateUserForm, {
  setupEventHandlers: function() {
    var that = this;

    this.setUpdateStateHandlers({
      updateState: function() {
        that.saveInSession();
      },

      saveState: function() {
        that.submitUserProfile();
      }
    });
  },

  submitUserProfile: function() {
    var that = this;
    $("submitUpdateUser").value = 'storeInDatabase';

    BS.PasswordFormSaver.save(this, this.formElement().action, OO.extend(BS.UpdateUserListener, {
      onUserNotFoundError: function(element) {
        document.location.href = "admin.html?item=users";
      },

      onCompleteSave: function(form, responseXML, errStatus) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, errStatus);

        if (!errStatus) {
          that.removeUpdateStateHandlers();
          BS.reload(true);
        }
      }
    }));

    return false;
  },

  deleteUserAccount: function() {
    if (!confirm('Are you sure you want to delete this user account?')) return false;

    var that = this;

    BS.FormSaver.save(this, this.formElement().action + "&removeUserAccount=1", OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function(form, responseXML, err) {
        if (!err) {
          BS.XMLResponse.processRedirect(responseXML);
        }
      }
    }));

    return false;
  }
});

