BS.ReportTabsForm = {
  deleteTab: function(tabId, projectId) {
    if (!confirm("Are you sure you want to remove this report tab?")) return;

    var url = window['base_uri'] + "/admin/action.html?deleteReportTab=" + tabId;
    if (projectId) {
      url += "&projectId=" + projectId;
    }
    BS.ajaxRequest(url, {
      onSuccess: function() {
        BS.ReportTabsForm.refreshTabsList();
      }
    });
  },

  refreshTabsList: function() {
    $('reportTabsList').refresh();
  }
};

BS.ReportTabSettingsDialog = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('editReportTabDialog');
  },

  formElement: function() {
    return $('editReportTab');
  },

  show: function(tabId, title, startPage, buildTypeId, revisionName, revisionValue) {
    this.tabId = tabId;
    var form = this.formElement();
    Form.reset(form);
    if (tabId) {
      form.tabTitle.value = title;
      form.tabStartPage.value = startPage;
    }
    else {
      form.tabStartPage.value = "index.html";
    }

    this.showCentered();
    this.clearErrors();
    var revisionRules = $('revisionRules');
    if (revisionRules) {
      this.selectOption($('buildTypeId'), buildTypeId);
      this.selectOption(revisionRules, revisionName);
      var buildNumberRule = revisionRules.selectedIndex == 3;
      var buildTagRule = revisionRules.selectedIndex == 4;
      if (buildNumberRule) {
        $('buildNumberPattern').value = revisionValue;
      }
      if (buildTagRule) {
        $('buildTag').value = revisionValue;
      }
    }
    form.tabTitle.focus();
    this.bindCtrlEnterHandler(this.save.bind(this));
    this.updateBuildTypeTagsList();
    this.updateFieldVisibility();
    BS.ReportTabSettingsDialog.BuildFinder.attachOnFirstShow();
  },

  selectOption: function(element, v) {
    for (var i = 0; i < element.options.length; i++) {
      if (element.options[i].value == v) {
        element.selectedIndex = i;
        break;
      }
    }
    BS.jQueryDropdown(element).ufd("changeOptions");
  },

  save: function() {
    BS.Util.show('saving_reportTab');
    Form.disable(this.formElement());

    var params = BS.Util.serializeForm(this.formElement());

    var revisionRules = $('revisionRules');
    if (revisionRules) {
      params += "&revisionRule=" + revisionRules.options[revisionRules.selectedIndex].value;
    }

    if (this.tabId) {
      params = params + "&tabId=" + this.tabId + "&create=false";
    }
    else {
      params = params + "&create=true";
    }

    var that = this;
    BS.ajaxRequest(this.formElement().action, {
      parameters: params,

      onComplete: function (transport) {
        BS.Util.hide('saving_reportTab');
        Form.enable(that.formElement());
        that.clearErrors();
        var errors = BS.XMLResponse.processErrors(transport.responseXML, {
          onInvalidTitleError: function(elem) {
            that.highlightTitleError(elem.firstChild.nodeValue);
          },
          onDuplicateTabTitleError: function(elem) {
            that.highlightTitleError(elem.firstChild.nodeValue);
          },
          onEmptyBuildNumberPatternError: function(elem) {
            that.highlightBuildNumberPattern(elem.firstChild.nodeValue);
          }
        });
        BS.VisibilityHandlers.updateVisibility('editReportTab');

        if (!errors) {
          that.close();
          BS.ReportTabsForm.refreshTabsList();
        }
      }
    });

    return false;
  },

  highlightTitleError: function(message) {
    $("error_tabTitle").innerHTML = message;
    this.highlightErrorField($("tabTitle"));
  },

  highlightBuildNumberPattern: function(message) {
    $("error_buildNumberPattern").innerHTML = message;
    this.highlightErrorField($("buildNumberPattern"));
  },

  updateFieldVisibility: function() {
    var form = $('editReportTab');
    if (!form.revisionRules) {
      return;
    }

    var buildNumberSelected = form.revisionRules.selectedIndex == 3;
    var buildTagSelected = form.revisionRules.selectedIndex == 4;

    if (buildNumberSelected) {
      $('buildNumberPattern').disabled = false; BS.Util.show('buildNumberField');
      $('buildTag').disabled = true; BS.Util.hide('buildTagField');
    } else if (buildTagSelected) {
      $('buildNumberPattern').disabled = true; BS.Util.hide('buildNumberField');
      $('buildTag').disabled = false; BS.Util.show('buildTagField');
    } else {
      $('buildNumberPattern').disabled = true; BS.Util.hide('buildNumberField');
      $('buildTag').disabled = true; BS.Util.hide('buildTagField');
    }
    BS.VisibilityHandlers.updateVisibility('buildNumberField');
    BS.VisibilityHandlers.updateVisibility('buildTagField');
  },

  updateBuildTypeTagsList: function() {
    var tagListHtml = "";
    var form = $('editReportTab');

    if (!form.buildTypeId || form.buildTypeId.options.length == 0) {
      BS.Util.hide('buildTagList');
      return;
    }

    var selectedBuildTypeId = form.buildTypeId.options[form.buildTypeId.selectedIndex].value;
    var url = window['base_uri'] + "/editArtifactDepsHelper.html";

    BS.ajaxRequest(url, {
      parameters: {
        listTags : '',
        buildTypeId : selectedBuildTypeId
      },
      onComplete: function(transport) {
        var responseXML = transport.responseXML;
        var tagData = responseXML.firstChild.firstChild;
        if (tagData) {
          while (tagData) {
            function renderTagLink(label, escapedLabel) {
              return "<a href=\"#\" onclick=\"BS.ReportTabSettingsDialog.setBuildTagValue('" +
                     escapedLabel + "'); return false\">" + label.escapeHTML() + "</a> ";
            }
            tagListHtml = tagListHtml + renderTagLink(tagData.attributes.original.value,
                                                      tagData.attributes.escaped.value);
            tagData = tagData.nextSibling;
          }
          $('buildTagListSpan').innerHTML = tagListHtml;
          BS.Util.show('buildTagList')
        } else {
          BS.Util.hide('buildTagList');
        }
      }
    });
  },

  setBuildTagValue: function(tagValue) {
    var textField = $('buildTag');
    if (!textField.disabled) {
      textField.value = tagValue;
    }
  }
}));

// Reusing javascript for the same functionality on edit dependencies page.
BS.ReportTabSettingsDialog.BuildFinder = OO.extend(BS.EditArtifactDependencies, {
  shouldAttach: function() {
    var that = this;
    that._shouldAttach = true;

    // A quick-fix. The controller is hardcoded to call "BS.EditArtifactDependencies.appendPath()" for tree children.
    BS.EditArtifactDependencies.appendPath = function(path) {
      that.setPath(path);
    };
  },

  attachOnFirstShow: function() {
    if (this._shouldAttach && !this._attached) {
      this.expandNestedArchives = true;
      this.attachPopups('longField', 'tabStartPage', 'editReportTab');
      this._attached = true;
    }
  },

  findOutBuildTypeId: function() {
    var select = $('buildTypeId');
    return select.options[select.selectedIndex].value;
  }
});
