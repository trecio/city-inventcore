
BS.BaseSaveParameterListener = OO.extend(BS.SaveConfigurationListener, {
  onBeginSave: function(form) {
    form.formElement().parameterName.value = BS.Util.trimSpaces(form.formElement().parameterName.value);
    form.clearErrors();
    form.hideSuccessMessages();
    form.disable();
    form.setSaving(true);
  }
});

BS.EditParameterForm = OO.extend(BS.AbstractWebForm, {
  setSaving: function(saving) {
    if (saving) {
      BS.Util.show('userParamsSaving');
    } else {
      BS.Util.hide('userParamsSaving');
    }
  },

  formElement: function() {
    return $('editParamForm');
  },

  saveParameter: function() {
    this.formElement().submitAction.value = 'updateParameter';
    var that = this;

    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.BaseSaveParameterListener, {
      onEmptyParameterNameError: function(elem) {
        $("error_parameterName").innerHTML = elem.firstChild.nodeValue.escapeHTML();
        that.highlightErrorField($('parameterName'));
      },

      onParameterSpecError: function(elem) {
        $('error_parameterSpec').innerHTML = elem.firstChild.nodeValue.escapeHTML();
        that.highlightErrorField($('parameterSpec'));
      },

      onParameterValueError: function(elem) {
        $('error_parameterValue').innerHTML = elem.firstChild.nodeValue.escapeHTML();
        that.highlightErrorField($('parameterValue'));
      },

      onCompleteSave: function(form, responseXML, err) {
        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

        form.enable();
        if (!err) {
          that.updateParamLists();
          BS.EditParameterDialog.close();
        }
      }
    }));

    return false;
  },

  doRemoveParameter: function(paramId, paramType) {
    var that = this;

    var url = this.formElement().action + "&submitBuildType=1&submitAction=removeParameter&removedPropertyId=" + paramId + "&paramType=" + paramType;

    BS.ajaxRequest(url, {
      onComplete: function() {
        that.updateParamLists();
        BS.EditParameterDialog.close();
      }
    });
  },

  removeParameter: function(paramId, paramType) {
    if (!confirm("Are you sure you want to delete this parameter?")) return;
    this.doRemoveParameter(paramId, paramType);
  },

  resetParameter: function(paramId, paramType) {
    if (!confirm("Are you sure you want to reset this parameter to default value?")) return;
    this.doRemoveParameter(paramId, paramType);
  },

  updateParamLists: function() {
    $('userParams').refresh();
  }
});


BS.EditParameterDialog = OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('editParamFormDialog');
  },

  showSpecEditFields : function(shown, updateVisibility) {
    if (shown) {
      BS.Util.hide('parameterSpecHolderExpand');
      BS.Util.show('parameterSpecHolderEdit');
    } else {
      BS.Util.show('parameterSpecHolderExpand');
      BS.Util.hide('parameterSpecHolderEdit');
    }
    if (updateVisibility) {
      this.updateVisibilityHandlers();
    }
  },

  showDialog: function(nameEl, valueEl, paramType, inherited, undefinedParam, specEl) {
    BS.EditParameterForm.clearErrors();

    var name = nameEl.firstChild ? nameEl.firstChild.nodeValue : '';
    var value = valueEl.firstChild ? valueEl.firstChild.nodeValue : '';
    var spec = specEl.firstChild ? specEl.firstChild.nodeValue : '';
    var addNew = name.length == 0;

    var replaceNewLines = function(x) {
      x = x.replace(/##10##/g, "\n");
      x = x.replace(/##13##/g, "\r");
      return x;
    };

    value = replaceNewLines(value);
    spec = replaceNewLines(spec);

    $('currentName').value = name;
    $('parameterName').value = name;
    $('currentNameWithPrefix').value = name;
    $('paramType').value = paramType;
    $('parameterSpec').value = spec;

    $('editParamFormTitle').innerHTML = addNew ? "Add New Parameter" : "Edit Parameter";

    this.showSpecEditFields(spec.length > 0);
    this.showCentered();

    $('parameterValue').value = value;

    this.bindCtrlEnterHandler(function() {
      BS.EditParameterForm.saveParameter();
    });
    if (inherited) {
      Form.Element.disable($('parameterName'));
      Form.Element.disable($('parameterSpec'));
      Form.Element.disable($('editParameterSpec'));
      $('inheritedParamName').show();
      $('parameterValue').focus();
    } else if (undefinedParam) {
      Form.Element.disable($('parameterName'));
      Form.Element.enable($('parameterSpec'));
      Form.Element.enable($('editParameterSpec'));
      $('parameterValue').focus();
    } else if (!addNew) {
      Form.Element.enable($('parameterName'));
      Form.Element.enable($('parameterSpec'));
      Form.Element.enable($('editParameterSpec'));
      $('parameterValue').focus();
    } else {
      Form.Element.enable($('parameterName'));
      Form.Element.enable($('parameterSpec'));
      Form.Element.enable($('editParameterSpec'));
      $('inheritedParamName').hide();
      $('parameterName').focus();
    }

    this.updateVisibilityHandlers();
  },

  updateVisibilityHandlers : function() {
    BS.VisibilityHandlers.updateVisibility(this.getContainer());
  },

  cancelDialog: function() {
    this.close();
  },

  completionItemSelected: function(event, ui) {
    var sliced = $j(this).data("sliced_text");
    var position = sliced.before_length + ui.item.value.length + 1;
    this.value = sliced.before + ui.item.value + '%' + sliced.after;
    if (!BS.Browser.msie) {
      $j("#parameterValue").attr({selectionStart: position,
                                  selectionEnd:   position});
    }
    return false;
  },

  paramNameChangeHandler: function(event) {
    var text = $j("#parameterName").attr("value"),
        prefix4 = text.slice(0, 4),
        prefix7;
    if (prefix4 === "env.") {
      $j("#paramType").attr("value", "env");
    } else {
      prefix7 = text.slice(0, 7);
      if (prefix7 === "system.") {
        $j("#paramType").attr("value", "system");
      } else {
        $j("#paramType").attr("value", "conf");
      }
    }
  },

  removeSystemPrefix: function(str) {
    if (str.length >=7 && str.substring(0, 7) === 'system.') {
      return str.substring(7);
    } else {
      return str;
    }
  },

  removeEnvPrefix: function(str) {
    if (str.length >=4 && str.substring(0, 4) === 'env.') {
      return str.substring(4);
    } else {
      return str;
    }
  },

  removeSystemAndEnvPrefixes: function(str) {
    if (str.length >=7 && str.substring(0, 7) === 'system.') {
      return BS.EditParameterDialog.removeSystemAndEnvPrefixes(str.substring(7));
    } else if (str.length >=4 && str.substring(0, 4) === 'env.') {
      return BS.EditParameterDialog.removeSystemAndEnvPrefixes(str.substring(4));
    } else {
      return str;
    }
  },

  removePrefixes: function(type, str) {
    if (type === 'system') {
      return BS.EditParameterDialog.removeEnvPrefix(str);
    } else if (type === 'env') {
      return BS.EditParameterDialog.removeSystemPrefix(str);
    } else {
      return BS.EditParameterDialog.removeSystemAndEnvPrefixes(str);
    }
  },

  paramTypeChangedHandler: function() {
    var type = $j("#paramType").attr("value"),
        name = $j("#parameterName").attr("value");
    name = BS.EditParameterDialog.removePrefixes(type, name);
    name = BS.EditParameterDialog.addTypePrefixMaybe(type, name);
    $j("#parameterName").attr("value", name);
  },

  createParamTypeChangeHandler: function(paramTypeEl, paramNameEl) {
    return function() {
      var type = $j(paramTypeEl).val(),
          name = $j(paramNameEl).val();
      name = BS.EditParameterDialog.removePrefixes(type, name);
      name = BS.EditParameterDialog.addTypePrefixMaybe(type, name);
      $j(paramNameEl).val(name);
    };
  },

  createParamNameChangeHandler: function(parameterNameEl, paramTypeEl) {
    return function(event) {
      var text = parameterNameEl.attr("value"),
          len = text.length,
          prefix4,
          prefix7;
      if (len < 4) {
        paramTypeEl.attr("value", "conf");
      } else {
        prefix4 = text.slice(0, 4);
        if (prefix4 === "env.") {
          paramTypeEl.attr("value", "env");
        } else {
          if (len < 7) {
            paramTypeEl.attr("value", "conf");
          } else {
            prefix7 = text.slice(0, 7);
            if (prefix7 === "system.") {
              paramTypeEl.attr("value", "system");
            } else {
              paramTypeEl.attr("value", "conf");
            }
          }
        }
      }
    };
  },

  splitAt: function(str, index) {
    return {
      prefix: str.slice(0, index),
      suffix: str.slice(index + 1, str.length)
    };
  },

  addTypePrefixMaybe: function(type, name) {
    if (this.hasTypePrefix(type, name)) {
      return name;
    } else {
      return type + '.' + name;
    }
  },

  hasTypePrefix: function(type, str) {
    return type === 'conf'
        || str.substring(0, type.length + 1) === type + '.';
  },

  canChangeSelectValue: function(event) {
    return event.keyCode === $j.ui.keyCode.UP
        || event.keyCode === $j.ui.keyCode.DOWN
        || event.keyCode === $j.ui.keyCode.PAGE_UP
        || event.keyCode === $j.ui.keyCode.PAGE_DOWN
        || event.keyCode === $j.ui.keyCode.HOME
        || event.keyCode === $j.ui.keyCode.END
        || event.keyCode === 67 //c
        || event.keyCode === 69 //e
        || event.keyCode === 83;//s
  }
});

BS.EditParametersSpecDialog = OO.extend(BS.AbstractWebForm, OO.extend(BS.AbstractModalDialog, {
  formElement: function() {
    return $("parameterSpecEditForm");
  },

  getContainer: function() {
    return $("parameterSpecEditFormDialog");
  },

  showDialog: function(spec) {
    var that = this;
    $('error_parameterSpec').update('');
    BS.Util.show("parameterSpecEditFormContentLoading");
    BS.ajaxUpdater('parameterSpecEditFormContent', this.formElement().action, {
      method: 'get',
      evalScripts : true,
      parameters : {spec : encodeURIComponent(spec), init : 1 },
      onComplete: function(transport) {
        BS.Util.hide("parameterSpecEditFormContentLoading");

        var xml = transport.responseXML;
        if (xml) {
          var error = false;
          var setErrorText = function(text) {
            $('error_parameterSpec').update(text.escapeHTML());
            error = true;
          };

          BS.XMLResponse.processErrors(xml, {
            onSPEC_ERRORError: function() {
              setErrorText('Failed to parse parameter specification');
            },
            onUNKNOWN_TYPEError: function() {
              setErrorText('Unknown parameter type');
            },
            onNO_EDITORError: function() {
              setErrorText('Specified parameter type does not support edit');
            }
          });
          if (error) {
            return;
          }
        }

        that.initializeParameterEdit();
        that.showCentered();
        that.bindCtrlEnterHandler(that.submitDialog.bind(that));
        BS.Util.reenableForm(that.formElement());
        that.updateSelectedType();
        BS.VisibilityHandlers.updateVisibility(that.formElement());
        BS.MultilineProperties.updateVisible();
      }
    });
    return false;
  },

  updateSelectedType: function() {
    var that = this;
    var v = $('specParameterTypeChooser').value;
    var paramz = "type=" + v;

    var disabled = (v == '');
    var specEditForm = $j('#parameterSpecEditFormContent .runnerFormTable').first();
    $('parameterSpecEditFormSubmit').disabled = disabled;
    $('parameterSpecEditFormSubmit').title = disabled ? 'Select parameter type' : '';
    $('specParameterEditorContainer').refresh(null, paramz, function(){
      that.showCentered();
      BS.VisibilityHandlers.updateVisibility(that.formElement());
      BS.MultilineProperties.updateVisible();
    });
  },

  initializeParameterEdit: function() {
    jQuery("#specParameterTypeChooser").change(this.updateSelectedType.bind(this));
  },

  closeDialog: function() {
    this.close();
  },

  submitDialog: function() {
    var that = this;

    BS.Util.show('parameterSpecEditFormSaving');
    BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
      onCompleteSave: function(form, responseXML, err) {
        BS.Util.hide('parameterSpecEditFormSaving');
        var wereErrors = BS.XMLResponse.processErrors(responseXML, {}, BS.PluginPropertiesForm.propertiesErrorsHandler);

        BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);
        if (wereErrors) {
          BS.Util.reenableForm(that.formElement());
          return;
        }

        var spec = responseXML.getElementsByTagName("paramter-spec")[0].getAttribute('spec');
        if (spec) {
          $('parameterSpec').value = spec;
          BS.EditParameterDialog.showSpecEditFields(true);
          BS.EditParameterDialog.updateVisibilityHandlers();
        }
        form.close();
      }
    }));

    return false;
  }


}));