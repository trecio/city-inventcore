
BS.ChangePageData = {};

BS.CarpetSequence = {
  ROW_TEMPLATE: new Template('<td class="#{statusText}" data-btId="#{buildTypeId}" title="#{configName} (click for builds)"><a href="#{buildLink}" onclick="return !Event.isLeftClick(event);"> </a></td>'),
  ROW_EMPTY_TEMPLATE: new Template('<td title="#{title}"><div> </div></td>'),
  carpetBuildTypes: [],

  reset: function() {
    this.carpetBuildTypes = [];
  },

  count: function() {
    return this.carpetBuildTypes.length;
  },

  insertBuildTypes: function(table, carpetData) {
    this._convertToMap(carpetData);
    this._insertExistingBuildTypesToCarpet(table, carpetData);

    this.carpetBuildTypes = this.carpetBuildTypes || [];
    var oldCarpetLength = this.carpetBuildTypes.length;

    this._insertNewBuildTypesToCarpet(table, carpetData);

    if (oldCarpetLength != this.carpetBuildTypes.length) {
      this._sortCarpetSequenceAccordingToUser();
      this._updateAllCarpetsSequence();
    }
  },

  _convertToMap: function(carpetData) {
    for(var i = 0; i < carpetData.length; i++) {
      carpetData[carpetData[i].buildType.id] = carpetData[i];
    }
  },

  _insertExistingBuildTypesToCarpet: function(table, carpetData) {
    // First, insert existing data for buildTypes from already shown carpets:
    var existing = this.carpetBuildTypes || [];
    for(var i = 0; i < existing.length; i++) {
      var square = carpetData[existing[i]];
      var row = square ? this._row(square) : this._emptyTD(existing[i]);
      table.insert(row);
    }
  },

  _emptyTD: function(buildTypeId) {
    var btObject = BS.UserBuildTypesMap[buildTypeId];
    var title = !btObject ? "" : (this._configName(btObject) + " is not affected by this change");
    return this.ROW_EMPTY_TEMPLATE.evaluate({ title:title });
  },

  _insertNewBuildTypesToCarpet:function(table, carpetData) {
    // Now, add new buildTypes to the carpet
    var existing = this.carpetBuildTypes || [];
    for(var i = 0; i < carpetData.length; i++) {
      var square = carpetData[i];
      if (!existing[square.buildType.id]) {
        table.insert(this._row(square));

        // mark build type as used now
        existing.push(square.buildType.id);
        existing[square.buildType.id] = true;
      }
    }
    this.carpetBuildTypes = existing;
  },

  _sortCarpetSequenceAccordingToUser: function() {
    var sortedCarpetOrder = [];
    // Apply user order:
    for(var j = 0; j < BS.UserBuildTypes.length; j ++) {
      var ubt = BS.UserBuildTypes[j];
      if (this.carpetBuildTypes[ubt.id]) {
        sortedCarpetOrder.push(ubt.id);
        sortedCarpetOrder[ubt.id] = true;
      }
    }

    // Apply remaining build types (for which there is no order specified)
    for(var j = 0; j < this.carpetBuildTypes.length; j ++) {
      var ubt = this.carpetBuildTypes[j];
      if (!sortedCarpetOrder[ubt]) {
        sortedCarpetOrder.push(ubt);
        sortedCarpetOrder[ubt] = true;
      }
    }

    this.carpetBuildTypes = sortedCarpetOrder;
  },

  _updateAllCarpetsSequence: function() {
    var top_nodes = BS.changeTree.getChildren();
    for(var i = 0; i < top_nodes.length; i ++) {
      var node = BS.changeTree.getNode(top_nodes[i]);
      if (node) {
        node.setCarpetSequenceTo(this.carpetBuildTypes);
      }
    }
  },

  _row: function(square) {
    return this.ROW_TEMPLATE.evaluate({
                                        statusText:square.statusText,
                                        buildTypeId:square.buildType.id,
                                        buildLink:square.firstBuildId ? ("viewLog.html?buildId=" + square.firstBuildId +
                                                                         "&tab=buildResultsDiv&buildTypeId=" + square.buildType.id) : "#",
                                        configName:this._configName(square.buildType)}
    );
  },

  _configName: function(btObject) {
    return btObject != null ? btObject.fullName.replace(/"/, "&quot;") : "Unknown build configuration";
  },

  f:null
};

BS.ChangePageFilter = {
  _projects: {},

  useFilter: true,
  projectId: '',

  setProject: function(projectId) {
    BS.ChangePageFilter.projectId = projectId;
    this.refresh();
  },

  refresh: function() {
    BS.Hider.hideAll();
    $("updatableChangesContainer").update();
    $("moreChangesContainer").update();

    BS.AsyncRunner.cancelAll();
    BS.ChangePage.carpetWidth = 0;
    BS.CarpetSequence.reset();
    BS.changeTree.dispose();

    BS.ChangePageFilter.hasSkippedChanges = false;
    BS.ChangePage.showFirstChanges();
  },

  urlParams: function() {
    return this._urlParams(this.projectId);
  },

  _urlParams: function(projectId) {
    return this._urlParamsWithoutProject() +
           (projectId && projectId.length > 0 ? "&projectId=" + projectId : "");
  },

  _urlParamsWithoutProject: function() {
    return "useFilter=" + this.useFilter;
  },

  /** project_data: id, name, usageCount */
  addProject: function(project_data) {
    if (!this._projects[project_data.id]) {
      this._projects[project_data.id] = project_data;
      this._sorted_projects = null;
    }
  },

  calculateProjectOrderMap: function() {
    var user_bt_sequence = BS.UserBuildTypes;
    var position = 1;
    var current_projectId = null;
    var projectOrderMap = {};
    for(var i = 0; i < user_bt_sequence.length; i ++){
      var projectId = user_bt_sequence[i].projectId;
      if (current_projectId != projectId) {
        projectOrderMap[projectId] = position++;
        current_projectId = projectId;
      }
    }
    return projectOrderMap;
  },

  updateFilterView: function() {

    if (!this._sorted_projects) {

      var projectOrderMap = this.calculateProjectOrderMap();

      this._sorted_projects = $H(this._projects).values().sort(function(a,b) {
        var res = (projectOrderMap[a.id] || 0) - (projectOrderMap[b.id] || 0);
        return res == 0 ? (b.usageCount - a.usageCount) : res;
      });


      if (!this.pane) {
        this.pane = new TabbedPane();
      }
      else {
        this.pane.clearTabs();
      }

      if (this._sorted_projects.length > 1) {
        this.addProjectTab({id: '', name: "All projects", usageCount: 10000});

        for(var i = 0; i < Math.min(7, this._sorted_projects.length); i ++) {
          var prj_data = this._sorted_projects[i];

          this.addProjectTab(prj_data);
          if (this.projectId == prj_data.id) {
            this.setTitle(prj_data);
          }
        }
      }
      this.pane.showIn("projectsFilter");
    }
    this.pane.setActiveCaption('prj_node_' + this.projectId);
    this.updateHiddenConfigurationsMessage();
  },

  updateHiddenConfigurationsMessage: function() {
    if (this.useFilter) {
      $('showingFilteredBlock').show();
      $('showingAllBlock').hide();

      var action = this.hasSkippedChanges ? "show" : "hide";
      $('hasSkippedChanges')[action]();
    }
    else {
      $('showingAllBlock').show();
      $('showingFilteredBlock').hide();
    }
  },

  addProjectTab: function(prj) {
    this.pane.addTab('prj_node_' + prj.id, {
      caption: prj.name,
      onselect: function() {
        this.setProject(prj.id);
        this.setTitle(prj);
        document.location.hash = "#" + prj.id;
        BS.ChangePage.carpetWidth = 0;
        BS.CarpetSequence.reset();

        return false;
      }.bind(this),
      url: "changes.html?" + this._urlParamsWithoutProject() + "#" + prj.id
    });
  },

  setTitle: function(project_data) {
    if (project_data.id != '') {
      BS.Util.setTitle("My Changes > " + project_data.name);
    }
    else {
      BS.Util.setTitle("My Changes");
    }
  }

};

BS.ChangePage = {
  firstRecord: null,
  lastRecord: null,

  lastDay : null,
  haveMoreChanges : true,

  lastUpdatableRecord : null, // For full AJAX update operation

  showFirstChanges: function() {
    $("showMoreLinkDiv").hide();
    var progress = new BS.ChangePage.Progress("Loading change list... ");

    BS.ajaxRequest('changes.html', {
      method: 'get',
      parameters: "firstChanges=true&" + BS.ChangePageFilter.urlParams(),
      onComplete: function() {
        progress.stop();
      },

      onSuccess: function(response) {
        $("updatableChangesContainer").update();
        this.__showRows(response.responseText, false);
        this.__updateMoreLink();

        if (!BS.changeTree.hasNonPersonalChanges()){
          $('no_regular_changes').show();
        }
        else {
          $('no_regular_changes').hide();
        }

        BS.ChangePageFilter.updateHiddenConfigurationsMessage();

        if (BS.changeTree.getChildren().length > 0) {
          $('carpetLegend').show();
        }
        else {
          $('carpetLegend').hide();
        }

        this.restoreTreeSelection();
      }.bind(this)
    });
  },

  restoreTreeSelection: function() {
    var sel = BS.Cookie.get("changesTreeSel");
    if ($(sel)) {
      BS.changeSel.setSelection(sel);
    }
    else {
      BS.changeSel.setSelection(BS.changeTree.getFirstNode());
    }
  },

  showMoreChanges: function() {
    if (this.noChangesLoaded()) {
      this.showFirstChanges();
    } else {
      if (this.loadingMore) return;

      this.loadingMore = true;
      $('showMoreChangesProgress').show();

      var lastRec = BS.ChangePage.lastRecord;

      BS.ajaxRequest('changes.html', {
        method: 'get',
        parameters: "moreChanges=true" + this._appendModificationAndFilter(lastRec) + "&lastDay=" + BS.ChangePage.lastDay,
        onSuccess: function(response) {
          this.__showRows(response.responseText, true);
          new Effect.ScrollTo($('showMoreChangesProgress'));
          $('showMoreChangesProgress').hide();
          this.__updateMoreLink();
        }.bind(this),
        onComplete: function() {
          this.loadingMore = false;
        }.bind(this)
      });
    }
  },

  _appendModificationAndFilter: function(record) {
    return "&lastModificationId=" + record.modId +
           "&personal=" + record.personal +
           "&" + BS.ChangePageFilter.urlParams()
        ;
  },

  updateChanges: function() {
    if (this.noChangesLoaded()) {
      this.showFirstChanges();
    } else {

      // Process case of multiple updateChanges requests in parallel:
      if (this.updateInProgress || !BS.canReload()) {
        this.updateRequested = true;
        return;
      }
      this.updateRequested = false;
      this.updateInProgress = true;

      var params;
      if (BS.ChangePage.lastUpdatableRecord) {
        params = "updateChanges=true" + this._appendModificationAndFilter(BS.ChangePage.lastUpdatableRecord);
      } else {
        params = "updateChanges=true" + this._appendModificationAndFilter(BS.ChangePage.firstRecord) + "&exclusive=true";
      }
      BS.ajaxRequest('changes.html', {
        method: 'get',
        parameters: params,
        onSuccess: function(response) {

          var toInsert = new Element("div");
          BS.ChangePage.processChangeRows(response.responseText, function(divElement) {
            var rowId = divElement.id;
            var row = BS.ChangePageData[rowId];
            if (row) {
              BS.ChangePage.lastUpdatableRecord = row.lastRowRecord;
              var rowElement = $(rowId);
              var newRow = !rowElement;
              var existingPersonal = rowElement && ('true' == rowElement.getAttribute('data-personal'));

              if (existingPersonal) {
                BS.Log.info("Updating personal change row: " + rowId);
                BS.stopObservingInContainers(rowElement);
                // Javascript is already evaluated in BS.ChangePage.processChangeRows call:
                rowElement.innerHTML = divElement.innerHTML;
              }
              else if (newRow) {
                BS.Log.info("New change row: " + rowId);
                toInsert.insert(divElement);

                // Remove node with the same date as inserted one
                var dateDiv = divElement.down("div.dateMarker");
                if (dateDiv && $(dateDiv.id)) {
                  $(dateDiv.id).remove();
                }

                // Remove existing content with same ID as inserted:
                BS.ChangePage.for_each_related_record(rowId.substr('jc_'.length), function(record) {

                  // Remove existing change node TR and details TR:
                  var ct_node_id = 'ct_node_' + record.modId + "_" + record.personal;
                  var existingToRemove = $(ct_node_id);
                  if (existingToRemove) {
                    BS.Log.info("Removing existing change row " + ct_node_id);
                    var detailsTr = $("details_" + ct_node_id);
                    BS.stopObservingInContainers([existingToRemove, detailsTr]);
                    existingToRemove.remove();
                    detailsTr.remove();
                  }

                  // Remove surrounding table as well, if needed
                  existingToRemove = $('jc_' + record.modId + "_" + record.personal);
                  if (existingToRemove) {
                    BS.Log.info("Removing existing joining table " + 'jc_' + record.modId + "_" + record.personal);

                    BS.stopObservingInContainers(existingToRemove);
                    existingToRemove.remove();
                  }
                });

              }
            }
          });

          // don't use insert() here! It evaluates Javascript, but we did this already:
          $("updatableChangesContainer").innerHTML = toInsert.innerHTML + $("updatableChangesContainer").innerHTML;

          BS.ChangePage.updateTreeAfterUpdate();
          BS.changeTree.updateTopNodesOrder();

          if (BS.changeTree.hasNonPersonalChanges()){
            $('no_regular_changes').hide();
          }
        },

        onComplete: function() {

          // Process case when updateChanges was called once more during previous update
          this.updateInProgress = false;
          if (this.updateRequested) {
            this.updateChanges.bind(this).defer();
          }
        }.bind(this)
      });
    }
  },

  showUsingFilter: function() {
    BS.ChangePageFilter.useFilter = true;
    BS.ChangePageFilter.refresh();
  },

  showAll: function() {
    BS.ChangePageFilter.useFilter = false;
    BS.ChangePageFilter.refresh();
  },

  noChangesLoaded: function() {
    return !this.firstRecord;
  },

  __showRows: function(htmlData, appendData) {

    var updatable = new Element("div");
    var more = new Element("div");

    this.processChangeRows(htmlData, function(divElement) {
      var rowId = divElement.id;
      var row = BS.ChangePageData[rowId];
      if (row) {
        if (row.updatable) {
          updatable.insert(divElement);
          this.lastUpdatableRecord = row.lastRowRecord;
        } else {
          more.insert(divElement);
        }
      }
    });

    // don't use insert() here! It evaluates Javascript, but we did this already:
    if (appendData) {
      $("updatableChangesContainer").innerHTML += updatable.innerHTML;
      $("moreChangesContainer").innerHTML += more.innerHTML;
    }
    else {
      $("updatableChangesContainer").innerHTML = updatable.innerHTML;
      $("moreChangesContainer").innerHTML = more.innerHTML;
    }

    BS.ChangePage.updateTreeAfterUpdate();
  },

  processChangeRows: function(htmlData, rowProcessor) {
    var tempDiv = new Element("div");
    tempDiv.innerHTML = htmlData;
    htmlData.evalScripts();

    tempDiv.childElements().each(rowProcessor, this);
  },

  updateTreeAfterUpdate: function() {
    BS.changeTree.synchronizeViewAndModel();
    BS.changeTree.preloadBlocks();
    BS.ChangePage.updateTableLayoutForOpera();

  },

  __updateMoreLink: function() {
    if (this.haveMoreChanges) {
      $("showMoreLinkDiv").show();
    } else {
      $("showMoreLinkDiv").hide();
    }
  },

  carpetElement: function(record) {
    return $("carpet_" + record.modId + "_" + record.personal);
  },

  updateTableLayoutForOpera: function() {
    // This is required for Opera to reflect setting of carpet table cell width with Javascript
    // But we have to keep table-layout=fixed in IE8
    if (BS.Browser.opera) {
      $$('.joinedChangeTable').each(function(table) {
        table.style.tableLayout = 'auto';
      });
    }
  },

  /** Run 'action' for each line in changes page which has same build set as 'record'
   * @param record_key key of the first change which joins all other changes with the same build set. Has form {modificationId}_{isPersonal}
   * @param action action to run, function will get record object as parameter, like {modId: 3, personal: false}
   * */
  for_each_related_record: function(record_key, action) {
    var data = BS.ChangePageData['jc_' + record_key];
    if (!data) return;
    var related_records = data.records_with_same_builds;
    for(var i = 0; i < related_records.length; i++) {
      action.call(this, related_records[i]);
    }
  },

  _f: null
};

/*---------------------------------------------------------------------------------------*/
/*-------------------- Here comes support for tree of changes -------------------------*/
/*---------------------------------------------------------------------------------------*/
BS.ChangeNode = Class.create(BS.TreeNode, {
  REFRESH_INTERVAL: 10 * 1000,

  initialize: function($super, id, id_record, first_record, expanded) {
    this.record = id_record;
    this.first_record = first_record;

    this.content_cache = null;
    this.updater = null;
    this.setupVisibilityHandler();
    $super(id, expanded);
  },

  destroy: function($super) {
    this._stop_ajax_update_of_expanded_block();
    this.content_cache = null;
    $super();
  },

  getDetailsContainer: function() {
    return this.getContainer();
  },

  getContainer: function() {
    return $("expandedViewContainer_" + this.key());
  },

  _setProgressIcon: function(show_progress_icon) {

    if (!this.progress_delay) {
      this.progress_delay = new BS.DelayedAction(function() {
        this._setHandle("ajax-loader.gif", "Loading content...", 1);
      }.bind(this), function() {
        this._setHandle("eluruDesign/projectNeutralSmallExpanded.png", "Collapse (arrow left key)");
      }.bind(this));
    }

    if (show_progress_icon) {
      this.progress_delay.start();
    }
    else {
      this.progress_delay.stop();
    }
  },

  onExpandChange: function() {
    this.updateView();
  },

  updateView: function() {
    if (!this.tree) return;
    if (!$(this.getId())) {
      this.destroy();
      return;
    }

    this._updateIconAndDetailsBlock();
    this.updateFullComment();

    if (this.isExpanded()) {
      this.expandBlock();
    }
    else {
      this.collapseBlock();
    }
  },

  _updateIconAndDetailsBlock: function() {
    var block = this.getDetailsContainer();

    if (this.isExpanded()) {
      $(this.getId()).addClassName("expandedBlock");

      this._setHandle("eluruDesign/projectNeutralSmallExpanded.png", "Collapse (arrow left key)");
    }
    else {
      $(this.getId()).removeClassName("expandedBlock");

      var icon = this.has_big_problem ? "eluruDesign/projectFailingSmall.png" : "eluruDesign/projectNeutralSmall.png";
      this._setHandle(icon, "Expand (arrow right key)");
      if (block) {
        block.hide();
      }
    }
  },

  _setHandle: function(img_src, title, hspace) {
    var img = this.getHandle();
    hspace = hspace || 0;
    if (img) {
      img.src = window['base_uri'] + "/img/" + img_src;
      img.className = 'progressRing';
      img.setStyle({padding: hspace});
      img.parentNode.title = title;
    }
  },

  getHandle: function() {
    return $(this.getId()).down("img");
  },

  setBuildTypes: function(buildTypes) {
    this.changeBuildTypes = buildTypes;
  },

  refreshHideSuccessful: function(defaultHideFailed) {

    var hideNotFailed = defaultHideFailed;

    // Restore runtime state of the option (if was changed without page reload)
    var storedHiddenSuccessful = BS.HideSuccessfulSupport.storedHiddenSuccessful();
    if (storedHiddenSuccessful.hasOwnProperty(this.key())) {
      hideNotFailed = storedHiddenSuccessful[this.key()];
    }

    BS.Util.runWithElement('builds_' + this.key(), function() {
      if ($('builds_' + this.key())) {
        var checkbox = $('builds_' + this.key()).down(".hideSuccessfulBlock input");
        if (checkbox) {
          checkbox.checked = hideNotFailed;
          BS.HideSuccessfulSupport.setSuccessfulVisible(checkbox, this.key(), this.changeBuildTypes, true);
        }
      }
    }.bind(this))
  },

  showDetailsTab: function(tabCode) {
    var tabId = tabCode + "_" + this.key();
    if (this.getSelectedTabId() == tabId) {
      this.toggle();
    }
    else {
      this.setExpanded(true);
      this.selectTab(tabId);
    }
  },

  gotoNextTab: function(shift) {
    if (!this.detailsTabs) return;
    this.detailsTabs.gotoTextTab(shift);
  },

  selectTab: function(tab) {
    BS.Util.runWithElement('changeTabs_' + this.key(), function() {
      if (!this.detailsTabs) return;
      if (this.detailsTabs.getTabs().length == 0) return;

      this.detailsTabs.setActiveTab(tab);
    }.bind(this), 6000);
  },

  getSelectedTabId: function() {
    if (!this.detailsTabs) return null;
    if (this.detailsTabs.getTabs().length == 0) return null;

    var activeTab = this.detailsTabs.getActiveTab();
    return activeTab ? activeTab.getId() : this.detailsTabs.getTabs()[0].getId();
  },

  expandBlock: function() {
    if (this.content_cache) {
      this._updateContentFromCache();
    }
    this._refreshChangeDetails(this._updatable());

    if (this._updatable()) {
      this._start_ajax_update_of_expanded_block();
    }
  },

  collapseBlock: function() {
    this._stop_ajax_update_of_expanded_block();
  },

  _updateContentFromCache: function() {
    var data = this.content_cache;
    BS.stopObservingInContainers(this.getContainer());
    this.getContainer().update(data);

    this._setProgressIcon(false);
    this.getDetailsContainer().show();
    delete this.content_cache;
  },

  setupVisibilityHandler: function() {
    var pageVisibilityAPI = BS.PageVisibility.detect();
    var pageVisibilitySupported = pageVisibilityAPI.supported;
    var hiddenProperty = pageVisibilityAPI.hiddenProperty;
    var visibilityChangeEvent = pageVisibilityAPI.visibilityChangeEvent;

    var that = this;
    function handleVisibilityChange() {
      if (document[hiddenProperty]) {
        that.REFRESH_INTERVAL = that.REFRESH_INTERVAL * 5;
      } else {
        that.REFRESH_INTERVAL = 10 * 1000;
      }
    }

    if (BS.Cookie.get('disable-visibility-api') == 1) return false;

    if (pageVisibilitySupported) {
      document.addEventListener(visibilityChangeEvent, handleVisibilityChange, false);
    }
  },

  refreshChangeDetails: function() {
    BS.reload(false, function() {
      this._refreshChangeDetails(true);
    }.bind(this));

    this.__start_ajax_update_of_expanded_block();
  },

  _refreshChangeDetails: function(force) {

    var no_content_yet = this.getContainer().innerHTML.blank();
    var progress = new BS.DelayedShow('updating_view_' + this.key());

    if (no_content_yet) {
      this._setProgressIcon(true);
    }
    else if (force) {
      progress.start();
    }

    if (force || no_content_yet) {

      var onSuccess = function(response) {
        this.content_cache = response.responseText;
        this._updateContentFromCache();

        progress.stop();

      }.bind(this);

      this.loadChangeDetails(onSuccess);

    } else {
      this.getDetailsContainer().show();
    }
  },

  loadChangeDetails: function(onSuccess) {
    var record = this.record;
    var params = "modId=" + record.modId + "&personal=" + record.personal + "&" + BS.ChangePageFilter.urlParams();

    if (this.detailsTabs) {
      params = params + "&tab=" + this.getSelectedTabId();
    }

    return BS.ajaxRequest('changeExpandedView.html', {
      method: 'get',
      parameters: params,
      onSuccess: onSuccess
    });
  },

  setTabs: function(tabs, currentTabId) {
    var curr = this.getSelectedTabId();
    var key = this.key();
    this.detailsTabs = tabs;
    if (!currentTabId) currentTabId = curr;

    BS.Util.runWithElement('changeTabs_' + key, function() {
      if (tabs.getTabs().length == 0) return;

      tabs.showIn('changeTabs_' + key);
      if (currentTabId) {
        tabs.setActiveTab(currentTabId);
      }
      else {
        tabs.setActiveTab(tabs.getTabs()[0].getId());
      }
    })

  },


  _loadCarpet: function(asyncContext) {

    if (this._shouldLoadCarpet()) {
      this._loadCarpetForRelatedNodes(asyncContext);
    }
    else {
      if (asyncContext) asyncContext.done();
    }
  },

  _shouldLoadCarpet: function() {
    var carpetElement = BS.ChangePage.carpetElement(this.record);
    if (!carpetElement) return;
    return this.record.modId == this.first_record.modId;
  },

  /**
   * @param asyncContext an object of type BS.AsyncContext
   * */
  _loadCarpetForRelatedNodes: function(asyncContext) {

    var loadCarpetRequest = this.loadCarpetAndStatus(function(response) {
      if (asyncContext && asyncContext.isCanceled()) {
        BS.Log.info("Carpet loading canceled");
        return;
      }

      response.responseText.evalScripts();

    }.bind(this), asyncContext ? asyncContext.done.bind(asyncContext) : null);

    this._setAbortCancelCallback(asyncContext, loadCarpetRequest);
  },

  updateCarpetAndStatusText: function(carpetData, has_problem, problemText) {
    if (carpetData.length > 0) {

      BS.ChangePage.for_each_related_record(this.first_record.modId + "_" + this.first_record.personal, function(record) {
        var node = BS.changeTree.getNode('ct_node_' + record.modId + "_" + record.personal);
        if (node) {
          node.updateCarpet(carpetData);
          node.updateStatusText(has_problem, problemText);
        }
      });

    }
  },

  updateFullComment: function() {
    var text = $(this.getId()).down("span.shortText");
    if (text) {
      if (text.scrollHeight > (text.getHeight() * 2 + 1) ) {
        this.hideFullComment();
      }
      else {
        this.showFullComment();
      }
    }
  },

  showFullComment: function() {
    var shortText = $(this.getId()).down("span.shortText");
    var arrow = $(this.getId()).down('.textExpandArrow');
    arrow.hide();
    shortText.hide();
    $(shortText.nextSibling).show(); // show full comment node
    arrow.stopObserving();
  },

  hideFullComment: function() {
    var shortText = $(this.getId()).down("span.shortText");
    var arrow = $(this.getId()).down('.textExpandArrow');
    arrow.title = "Click to see full commit message";
    arrow.show();
    shortText.show();
    $(shortText.nextSibling).hide(); // hide full comment
    arrow.on("click", function() { this.showFullComment();}.bind(this) )
  },

  updateCarpet: function(carpetData) {
    var carpetEl = BS.ChangePage.carpetElement(this.record);
    if (carpetEl) {
      var carpetText = new Template('<div class="carpetTd"\
          onclick="if (Event.isLeftClick(event)) BS.changeTree.getNode(\'ct_node_#{carpetId}\').showDetailsTab(\'builds\'); return false;"\
          title="Click to see builds">\
        <table class="carpetTable"><tr>\
        </tr></table></div>');

      var carpetId = this.record.modId + "_" + this.record.personal;
      carpetEl.update(carpetText.evaluate({carpetId: carpetId}));
      var table = carpetEl.down("tr");

      BS.CarpetSequence.insertBuildTypes(table, carpetData);
      this.updateCarpetWidthsGlobally(BS.CarpetSequence.count() * 10 + 5, carpetEl);
    }
  },

  setCarpetSequenceTo: function(buildTypeIdSequence) {
    var carpetEl = BS.ChangePage.carpetElement(this.record);
    if (!carpetEl) return;
    var tr = carpetEl.down("tr");
    if (!tr) return;

    var tds = tr.childElements();

    var findTd = function(btId) {
      for(var i = 0; i < tds.length; i++) {
        if (btId == tds[i].getAttribute('data-btId')) {
          return tds[i]
        }
      }
      return BS.CarpetSequence._emptyTD(btId);  // not found this build type square
    };

    // collect new sequence of td's for the tr
    var newTr = [];
    for(var i = 0; i < buildTypeIdSequence.length; i ++) {
      newTr.push(findTd(buildTypeIdSequence[i]))
    }

    // update TR with new sequence
    tr.innerHTML = '';
    for(var i = 0; i < buildTypeIdSequence.length; i ++) {
      $(tr).insert(newTr[i]);
    }

    //console.info(buildTypeIdSequence);
  },

  loadCarpetAndStatus: function(onSuccess, onComplete) {
    var record = this.first_record;
    var params = "modId=" + record.modId + "&personal=" + record.personal + "&" + BS.ChangePageFilter.urlParams();
    if (!onComplete) onComplete = Prototype.emptyFunction;

    return BS.ajaxRequest('carpet.html', {
      method: 'get',
      parameters: params,
      onSuccess: onSuccess,
      onComplete: onComplete
    });
  },



  _setAbortCancelCallback: function(context, request) {
    if (context && request && request.transport && (typeof request.transport.abort == 'function' )) {
      context.setCancelCallback(function() {
        request.transport.abort();
      });
    }
  },

  updateCarpetWidthsGlobally: function(newWidth, carpetElement) {
    if (!BS.ChangePage.carpetWidth || newWidth > BS.ChangePage.carpetWidth) {
      BS.ChangePage.carpetWidth = newWidth > 350 ? 350 : newWidth;
    }

    var top_nodes = BS.changeTree.getChildren();
    for(var i = 0; i < top_nodes.length; i ++) {
      var node = BS.changeTree.getNode(top_nodes[i]);
      if (node) {
        var carpetEl = BS.ChangePage.carpetElement(node.record);
        var w = BS.ChangePage.carpetWidth;
        this._setCarpetCellWidth(carpetEl, w);
      }
    }
  },

  _setCarpetCellWidth: function(carpetEl, w) {
    if (BS.Browser.webkit) {
      // In WebKit, paddings are included into width - and that's strange
      w += (26+3);
    }
    carpetEl.setStyle({ width: w + "px" });
  },

  _updatable: function() {
    return this.getContainer().descendantOf($("updatableChangesContainer"));
  },

  _start_ajax_update_of_expanded_block: function() {
    this._stop_ajax_update_of_expanded_block();
    this.__start_ajax_update_of_expanded_block();
  },

  __start_ajax_update_of_expanded_block: function() {
    this.updater = setTimeout(this.refreshChangeDetails.bind(this),
                              this.REFRESH_INTERVAL * (1 + Math.random()/2));
  },

  _stop_ajax_update_of_expanded_block: function() {
    if (this.updater) {
      clearTimeout(this.updater);
      this.updater = null;
    }
  },

  updateStatusText: function(has_problem, problemText) {
    BS.Util.runWithElement(this.getId(), function() {
      var el = $(this.getId());
      if (!el) return;
      this.has_big_problem = has_problem;

      this._updateIconAndDetailsBlock();

      var commitTextNode = el.down("td.commitText");
      var problemSummaryNode = commitTextNode.down(".changeFailuresLink");
      if (problemSummaryNode) {
        problemSummaryNode.update(problemText);
        if (problemText.blank()) {
          problemSummaryNode.hide();
        }
        else {
          problemSummaryNode.show();
        }
      }
    }.bind(this), 5000);
  },

  key: function() {
    if (!this._key) {
      this._key = this.record.modId + "_" + this.record.personal;
    }
    return this._key;
  },

  _f: null
});

BS.changeTree = new BS.Tree();
BS.changeSel = new BS.TreeSelection(BS.changeTree);

BS.treeNav = new BS.TreeKeyboardSupport(document, BS.changeSel);
BS.treeNav.switchToTabFromKeyboard = function(e, tab) {
  var selected = BS.changeSel.getSelection();
  if (!selected || e.ctrlKey || e.altKey || e.metaKey) return;

  var node =  BS.changeTree.getNode(selected);
  if (!node) return;

  node.showDetailsTab(tab);
  return true;
};
BS.treeNav.addKeyHandler(84, BS.treeNav.switchToTabFromKeyboard.bindAsEventListener(BS.treeNav, 'problems'));
BS.treeNav.addKeyHandler(66, BS.treeNav.switchToTabFromKeyboard.bindAsEventListener(BS.treeNav, 'builds'));
BS.treeNav.addKeyHandler(70, BS.treeNav.switchToTabFromKeyboard.bindAsEventListener(BS.treeNav, 'files'));
BS.treeNav.addKeyHandler(9, function(e) {  // Tab
  var selected = BS.changeSel.getSelection();
  if (!selected || e.ctrlKey || e.altKey || e.metaKey) return;

  var node =  BS.changeTree.getNode(selected);
  if (!node) return;

  node.gotoNextTab(e.shiftKey ? -1 : 1);
  return true;
});




BS.changeTree.addListener({
                            onExpandChange: function() {
                              if (this.initializing) return;

                              if (this.expandTimeout) {
                                clearTimeout(this.expandTimeout);
                              }
                              this.expandTimeout = setTimeout(function() {
                                this.expandTimeout = null;
                                BS.User.setProperty("changesTreeState", this.collectExpandedNodes());
                              }.bind(this), 500);
                            },

                            onNodeAdded: function() {
                            },

                            onNodeDestroyed: function() {
                            },

                            onSelectionChange: function(newSelId, oldSelId) {
                              if (!$(oldSelId)) return;

                              var next = $(oldSelId).nextSiblings()[0];
                              if (next) {
                                next.removeClassName('selectedTreeNode');
                              }
                              var parentTable = $(oldSelId).up("table");
                              if (parentTable) {
                                parentTable.removeClassName("selectedGroup");
                              }

                              BS.Cookie.set("changesTreeSel", newSelId, 1.0/24);
                            },

                            onSelectionRepaint: function(newSelId) {
                              var next = $(newSelId).nextSiblings()[0];
                              if (next) {
                                next.addClassName('selectedTreeNode');
                              }

                              var parentTable = $(newSelId).up("table");
                              if (parentTable && $j(parentTable).find("> tbody > tr").size() > 2) {
                                parentTable.addClassName("selectedGroup");
                              }
                            },

                            f: null
                          });



BS.changeTree.synchronizeViewAndModel = function() {
  BS.changeSel.repaintSelection();
  this.forEachNode(function(node) {
    node.updateView();
  });
  BS.changeSel.repaintSelection();
}.bind(BS.changeTree);

BS.changeTree.updateTopNodesOrder = function() {
  var seq = [];
  $$('.joinedChangeTable tr').each(function(tr) {
    if (tr.id.startsWith('ct_node_')) {
      seq.push(tr.id);
    }
  });
  this._root_sequence = seq;
}.bind(BS.changeTree);


BS.changeTree.preloadBlocks = function() {

  var progress;
  if ($('loadingProgress') && BS.ChangePreloadBlocks.length > 0) {
    progress = new BS.ChangePage.Progress("Loading change details... ");
  }

  BS.AsyncRunner.setCallbackOnFinish(function() {
    if (this.isEmpty() && progress) {
      progress.stop();
    }
  });

  $A(BS.ChangePreloadBlocks).each(function(record) {
    var node = this.getNode(record);
    if (node) {
      BS.AsyncRunner.runAsync(function(asyncContext) {
        node._loadCarpet(asyncContext);
      }, 1);
    }
  }, this);

}.bind(BS.changeTree),

    BS.changeTree.hasNonPersonalChanges = function() {
      return $A(this.getChildren()).detect(function(node_id) {
        var node = this.getNode(node_id);
        return node && !node.record.personal;
      }, this);
    }.bind(BS.changeTree),


    BS.Util.runWithElement('moreChangesContainer', function() {
      $('updatableChangesContainer').on('click', BS.changeSel.clickHandler.bindAsEventListener(BS.changeSel));
      $('moreChangesContainer').on('click', BS.changeSel.clickHandler.bindAsEventListener(BS.changeSel));
    });


BS.ChangePage.Progress = function(text) {
  text = jQuery.trim(text);

  BS.DelayedAction.call(this, function() {
    if ($("loadingProgress")) $("loadingProgress").update(BS.loadingIcon + ' ' + text);
  }, function() {
    if ($("loadingProgress")) $("loadingProgress").update("&nbsp;"); // nbsp to avoid layout jumping
  });
  this.start();
};
BS.ChangePage.Progress.prototype = new BS.DelayedAction();
