
/*
 * Copyright 2000-2012 JetBrains s.r.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

window.name = 'tcMain';

// Additionally define $j in JS to make IDEA happier
window.$j = window.$j || jQuery.noConflict;

var OO = {
  extend: function(parent, extension) {
    return _.extend(_.extend({}, parent), extension);
  },

  bindAll: function(object) {
    var dest = new Object();
    for (var property in object) {
      if (typeof(object[property]) == "function") {
        dest[property] = object[property].bind(dest);
      } else {
        dest[property] = object[property];
      }
    }
    return dest;
  }
};

var BS = {
  Browser: {
    msie: $j.browser.msie,
    msie6: $j.browser.msie && parseInt($j.browser.version, 10) == 6,
    mozilla: $j.browser.mozilla,
    opera: $j.browser.opera,
    webkit: $j.browser.webkit
  },

  Log: {
    log: function(msg, level) {
      var wc = window.console;
      if (wc && wc[level]) {
        wc[level](msg);
        if (msg.stack) {
          wc[level](msg.stack);
        }
      }
    },

    error:  function(msg) { this.log(msg, 'error') },
    warn:   function(msg) { this.log(msg, 'warn') },
    info:   function(msg) { this.log(msg, 'info') },
    debug:  function(msg) { this.log(msg, 'debug') }
  },

  loadingUrl: window['base_uri'] + "/img/ajax-loader.gif",
  loadingIcon: "<img class='progressRing' src='" + window['base_uri'] + "/img/ajax-loader.gif' alt=''/>",

  _canReload: true,
  _temporaryBlocked: false,
  _tempBlockedTimer: null,

  stopObservingInContainers: function(elements) {
    if (elements.nodeType == 1) {
      elements = [elements];
    }
    var i = elements.length;

    var purge = function(el) {
      jQuery.cleanData([el]);
    };

    while (i--) {
      var el = elements[i];

      var descendants = el.getElementsByTagName('*');
      var j = descendants.length;
      while (j--) {
        Element.purge(descendants[j]);
        purge(descendants[j]);
      }

      purge(el);
    }
  },

  refreshBlocked: function() {
    return !BS._canReload || BS._temporaryBlocked;
  },

  canReload: function() {
    return !BS.Hider.hasVisiblePopups() && !BS.refreshBlocked() &&
           document.readyState != 'loading' && document.readyState != 'uninitialized';
  },

  initReloadBlocker: function() {
    if($j.browser.mozilla) {
      $(document.body).on("mousedown", function(event, element) {
        if (typeof BS == 'object') {
          if (element.tagName.toUpperCase() == 'A' && element.href && (element.href.indexOf("#") == -1 || element.href.indexOf("javascript://") == -1)) {
            BS.blockRefreshTemporary(1000);
          }
        }
      });
    }

    $(document.body).on("mousemove", function() {
      if (typeof BS != 'object') return;
      BS.blockRefreshTemporary(100);
    });

    Event.observe(window, "scroll", function() {
      if (typeof BS != 'object') return;
      BS.blockRefreshTemporary(100);
    });
  },

  blockRefreshTemporary: function(milliseconds) {
    // refresh blocked permanently ?
    if (!BS._canReload) return;

    var millis = milliseconds || 10*1000;

    if (BS._tempBlockedTimer) {
      clearTimeout(BS._tempBlockedTimer);
    }

    BS._temporaryBlocked = true;
    BS._tempBlockedTimer = setTimeout(function() {
      BS._temporaryBlocked = false;
    }, millis);
  },

  /**
   * A page might have several reasons for blocking refresh simultaneously. For example, some tests may be
   * selected and some stacktraces may be expanded. So instead of simply blocking/unblocking,
   * we need to take into account those various reasons.
   */
  _blockingActions: {},
  blockRefreshPermanently: function(action) {
    BS._blockingActions[action || 'default'] = true;
    BS._canReload = false;
  },

  unblockRefresh: function(action) {
    delete BS._blockingActions[action || 'default'];

    if (_.isEmpty(BS._blockingActions)) {
      BS._canReload = true;
      if (BS._tempBlockedTimer) {
        clearTimeout(BS._tempBlockedTimer);
      }
      BS._temporaryBlocked = false;
    }
  },

  _reloadTimeout: null,

  reload: function(force, reloadFunc) {
    if (BS._reloadTimeout) {
      clearTimeout(BS._reloadTimeout);
      BS._reloadTimeout = null;
    }

    if (BS.canReload() || force) {
      if (reloadFunc) {
        reloadFunc();
      } else {
        window.location.reload(true);
      }
    }
    else {
      BS._reloadTimeout = setTimeout(function() {
        BS.reload(force, reloadFunc);
      }, 100);
    }
  },

  ajaxRequest: function(url, options) {
    var opts = BS._patchOptions(url, options);

    return new Ajax.Request(url, opts);
  },

  ajaxUpdater: function(container, url, options) {
    var opts = BS._patchOptions(url, options);

    return new Ajax.Updater(container, url, opts);
  },

  _getRequestKey: function(url, parameters) {
    if (_.isObject(parameters)) {
      return url + Object.toQueryString(parameters);
    } else if (_.isString(parameters)) {
      return url + parameters;
    } else {
      return url;
    }
  },

  _patchOptions: function(url, options) {
    if (options == null) {
      options = {};
    }

    var oldOnComplete = options.onComplete;

    options.onComplete = function(response, json) {
      var respXML = response.responseXML;
      if (respXML) {
        var handled = BS.XMLResponse.processErrors(respXML, {
          onAccessDeniedError: function(elem) {
            BS.XMLResponse.processRedirect(elem.ownerDocument);
          }
        });
        if (handled) return;
      } else {
        var text = response.responseText;
        if (text.match(/^\s*<!DOCTYPE html/)) {
          BS.reload(true);
          return;
        }
      }

      if (oldOnComplete) {
        oldOnComplete.call(options, response, json);
      }
    };

    return options;
  }
};

BS.PeriodicalUpdater = Class.create(Ajax.PeriodicalUpdater, {
  initialize: function($super, container, url, options) {
    this.initialFrequency = options.frequency || 2;
    this.initialOnSuccess = options.onSuccess || Prototype.emptyFunction();
    this.initialOnFailure = options.onFailure || Prototype.emptyFunction();
    options.onSuccess = this.onSuccess.bind(this);
    options.onFailure = this.onFailure.bind(this);
    options.onException = this.onException.bind(this);
    this.pageVisibilitySupported = this.setupVisibilityHandler();
    $super(container || {update:function() {}}, url, options);
  },

  onSuccess: function(response, json) {
    if (response && response.status == 0) {
      // workaround, see http://dev.rubyonrails.org/ticket/11508
      this.onFailure(response, json);
      return;
    }

    if (!this.pageVisibilitySupported) {
      this.frequency = this.initialFrequency; // reset frequency
    }
    this.failureState = false;

    if (BS.ServerLink) {
      BS.ServerLink.onSuccess();
    }
    if (this.initialOnSuccess) {
      this.initialOnSuccess.call(this.options, response, json);
    }
  },

  onFailure: function(response, json) {
    this.frequency = this.initialFrequency * 5; // reduce frequency
    this.failureState = true;
    if (BS.ServerLink) {
      BS.ServerLink.onFailure();
    }
    if (this.initialOnFailure) {
      this.initialOnFailure.call(this.options, response, json);
    }
  },

  onException: function(obj, ex) {
    this.frequency = this.initialFrequency * 5; // reduce frequency
    this.failureState = true;
    if (BS.ServerLink) {
      BS.ServerLink.onFailure(ex);
    }
  },

  // https://developer.mozilla.org/en/DOM/Using_the_Page_Visibility_API
  setupVisibilityHandler: function() {
    var pageVisibilityAPI = BS.PageVisibility.detect();
    var pageVisibilitySupported = pageVisibilityAPI.supported;
    var hiddenProperty = pageVisibilityAPI.hiddenProperty;
    var visibilityChangeEvent = pageVisibilityAPI.visibilityChangeEvent;

    var that = this;
    function handleVisibilityChange() {
      if (document[hiddenProperty]) {
        that.frequency = that.initialFrequency * 5;
      } else {
        if (that.failureState === false) {
          that.frequency = that.initialFrequency;
        }
      }
    }

    if (BS.Cookie.get('disable-visibility-api') == 1) return false;

    if (pageVisibilitySupported) {
      document.addEventListener(visibilityChangeEvent, handleVisibilityChange, false);
    }

    return pageVisibilitySupported;
  }
});


BS.PageVisibility = {
  detect: function() {
    var hiddenProperty, visibilityChangeEvent;
    if (typeof document.hidden !== "undefined") {
      hiddenProperty = "hidden";
      visibilityChangeEvent = "visibilitychange";
    } else if (typeof document.mozHidden !== "undefined") {
      hiddenProperty = "mozHidden";
      visibilityChangeEvent = "mozvisibilitychange";
    } else if (typeof document.msHidden !== "undefined") {
      hiddenProperty = "msHidden";
      visibilityChangeEvent = "msvisibilitychange";
    } else if (typeof document.webkitHidden !== "undefined") {
      hiddenProperty = "webkitHidden";
      visibilityChangeEvent = "webkitvisibilitychange";
    }

    var pageVisibilitySupported = typeof document.addEventListener !== "undefined" && typeof hiddenProperty !== "undefined";

    return {
      supported: pageVisibilitySupported,
      hiddenProperty: hiddenProperty,
      visibilityChangeEvent: visibilityChangeEvent
    };
  }
};


BS.EventTracker = {
  _subscriptions: {},
  _options: {
    frequency: 6,
    evalScripts: false,
    parameters: ""
  },

  subscribeOnEvent: function(eventName, currentValue, listener) {
    this._subscribeOnEvent(eventName, currentValue, "", listener)
  },

  subscribeOnProjectEvent: function(eventName, currentValue, projectId, listener) {
    this._subscribeOnEvent(eventName, currentValue, "p:" + projectId, listener)
  },

  subscribeOnBuildTypeEvent: function(eventName, currentValue, buildTypeId, listener) {
    this._subscribeOnEvent(eventName, currentValue, "b:" + buildTypeId, listener)
  },

  subscribeOnUserEvent: function(eventName, currentValue, userId, listener) {
    this._subscribeOnEvent(eventName, currentValue, "u:" + userId, listener)
  },

  _subscribeOnEvent: function(eventName, currentValue, parameters, listener) {
    var subscr = {
      currentValue: currentValue,
      listeners: [listener],
      id: eventName + ";" + parameters
    };
    this._addSubscription(subscr);
  },

  addCustomSubscription: function(subscriptionId, listener, currentValue) {
    this._addSubscription({
      currentValue: currentValue,
      listeners: [listener],
      id: subscriptionId
    });
  },

  _addSubscription: function(subscr) {
    var curSubscr = this._subscriptions[subscr.id];
    if (curSubscr != null) {
      for (var i=0; i<curSubscr.listeners.length; i++) {
        if (curSubscr.listeners[i].toString().strip() == subscr.listeners[0].toString().strip()) return;
      };

      curSubscr.listeners.push(subscr.listeners[0]);
    } else {
      this._subscriptions[subscr.id] = subscr;
    }

    this._addParameter(subscr.id);
  },

  _addParameter: function(id) {
    if (this._options.parameters == null || this._options.parameters.length == 0) {
      this._options.parameters = "subscriptionId=";
    }

    var subs = id + ",";
    if (this._options.parameters.indexOf(subs) == -1) {
      this._options.parameters += subs;
    }
  },

  startTracking: function(trackerUrl) {
    if (!this.isStopped()) {
      this.stop();
    }

    this._trackerUrl = trackerUrl;
    this._options.onSuccess = function(transport) {
      var response = transport.responseText;
      if (response.indexOf('__tc_cleanupInProgressMarker') != -1) {
        BS.reload(true);
        return;
      }
      var listeners = [];
      var lines = response.split("\n");
      for (var i=0; i<lines.length; i++) {
        var line = lines[i];
        var idx = line.indexOf('=');
        if (idx == -1) continue;
        var id = line.substring(0, idx);
        var counter = line.substring(idx+1);
        var curSubscr = BS.EventTracker._subscriptions[id];
        if (curSubscr != null) {
          if (curSubscr.currentValue != counter) {
            curSubscr.currentValue = counter;
            for (var j=0; j<curSubscr.listeners.length; j++) {
              var lr = curSubscr.listeners[j];
              if (!listeners.include(lr)) {
                listeners.push(lr);
              }
            }
          }
        }
      }

      for (var k=0; k<listeners.length; k++) {
        listeners[k]();
      }
    };

    this._updater = new BS.PeriodicalUpdater(null, trackerUrl, this._options);
  },

  checkEvents: function(oncomplete) {
    new Ajax.Request(this._trackerUrl, this._options);
  },

  stop: function() {
    this._stopped = true;
    if (this._updater) {
      this._updater.stop();
    }
  },

  dispose: function() {
    this.stop();
    this._subscriptions = {};
  },

  isStopped: function() {
    return this._stopped;
  }
};


BS.StatisticsMonitor = {
  start: function(url) {
    var that = this;
    this._updater = new BS.PeriodicalUpdater(null, url, {
      frequency: 10,
      onSuccess: function(transport) {
        var doc = BS.Util.documentRoot(transport);
        var agentsNumTags = doc.getElementsByTagName("agentsNum");
        if (!agentsNumTags || agentsNumTags.length == 0) return;

        var agentsNum = that.getIntValue(agentsNumTags[0]);
        var freeAgentsNum = that.getIntValue(doc.getElementsByTagName("freeAgentsNum")[0]);
        var runningNum = that.getIntValue(doc.getElementsByTagName("runningBuildsNum")[0]);
        var buildQueueSize = that.getIntValue(doc.getElementsByTagName("buildQueueSize")[0]);

        if (agentsNum != null) {
          var agentsLoad = 0;
          if (runningNum > 0 || freeAgentsNum > 0) {
            agentsLoad = Math.round(runningNum * 100.0 / (freeAgentsNum + runningNum) );
          }
          if (agentsLoad < 0) {
            agentsLoad = 100;
          }
          var idle = freeAgentsNum;
          if (idle < 0) idle = 0;
          var title = '';
          if (agentsNum == 0) {
            title = "There are no agents";
          } else if (idle == 0) {
            title = "All agents are busy";
          } else if (idle == agentsNum) {
            title = "All agents are idle";
          } else {
            title = runningNum + (runningNum > 1 ? " builds are" : " build is") + " running, ";
            title += idle + (idle > 1 ? ' agents are' : ' agent is') + ' idle';
          }

          var content = '<div class="mug" onmouseover="BS.Tooltip.showMessage(this, {shift:{x:10,y:20}}, \'' + title + '\')" onmouseout="BS.Tooltip.hidePopup()">';
          if (agentsLoad > 0) {
            var maxheight = 16; // maximum height of the progress bar (see tabs.css)
            var top = Math.round((100 - agentsLoad) / 100 * maxheight);
            content += '<div class="mugStuff" style="top: ' + top + 'px; height: ' + (maxheight - top) + 'px;"></div>';
          }
          content += "</div>";
          var agentsTab = BS.topNavPane.getTab("agents");
          agentsTab.setPostLinkContent(content);
          agentsTab.setCaption("Agents (" + agentsNum + ")");
        }

        if (buildQueueSize != null && BS.topNavPane.getTab("queue")) {
          BS.topNavPane.getTab("queue").setCaption("Build Queue (" + buildQueueSize + ")");
        }

        that.processInvestigations(doc);

      }
    });
  },

  getIntValue: function(element) {
    if (element != null) {
      return parseInt(element.getAttribute("value"), 10);
    }

    return null;
  },

  processInvestigations: function(doc) {
    var investNodes = doc.getElementsByTagName("investigations");
    if (!investNodes || investNodes.length != 1) return;

    var count = investNodes[0].getAttribute("activeCount");
    var hasNew = investNodes[0].getAttribute("hasNew") == "true";

    var node = $('beforeUserId');
    if (node) {
      node.removeClassName("newInvestigations");
      node.removeClassName("oldInvestigations");
      if (count == 0) {
        node.innerHTML = "";
      }
      else {
        var title = hasNew ? "You have some recently updated investigations, click to view" : "Review your active investigations";
        node.innerHTML = "<a href='" + window['base_uri'] + "/investigations.html' title='" +title+ "'>" + count + "</a>";
        node.addClassName(hasNew ? "newInvestigations" : "oldInvestigations");
      }
    }
  }

};

BS.Util = {
  // Wraps an element into a position: relative container
  wrapRelative: function(element) {
    return $j(element).wrap('<div class="posRel"/>').parent();
  },

  place: function(element, x, y) {
    $(element).setStyle({left: x + 'px', top: y + 'px'});
  },

  center: function(elementToPlace, container) {
    var pos = BS.Util.computeCenter(elementToPlace, container);
    BS.Util.place(elementToPlace, pos[0], pos[1]);
  },

  // computes x and y so that element is shown centered vertically relative to the window
  // and horizontally relative to the container
  computeCenter: function(elementToPlace, container) {
    if (!container) {
      container = $('mainContent');
    }

    elementToPlace = $(elementToPlace);

    var containerDim = container.getDimensions();
    var containerPos = container.cumulativeOffset();
    var windowSize = BS.Util.windowSize();

    // to obtain element dimensions we have to show it
    var oldVisibility = elementToPlace.style.visibility;
    var oldDisplay = elementToPlace.style.display;
    elementToPlace.setStyle({visibility: 'hidden', display: 'block'});
    var dim = elementToPlace.getDimensions();
    elementToPlace.setStyle({visibility: oldVisibility, display: oldDisplay});
    var x = 0;
    var y = Math.round(this._scrollTop() + (windowSize[1] - dim.height) / 2);

    // check if container is wider than visible area
    if (containerDim.width > windowSize[0]) {
      // position within window instead of container
      x = Math.round(this._scrollLeft() + (windowSize[0] - dim.width) / 2);
    } else {
      x = Math.round(containerPos[0] + (containerDim.width - dim.width) / 2);
    }

    // Make sure dialog fits into screen boundaries
    if (_.isElement(container) && container.id == 'mainContent') {
      y = Math.max(y, 0);
    }

    return [x, y];
  },

  _scrollTop: function() {
    return $j(window).scrollTop();
  },

  _scrollLeft: function() {
    return $j(window).scrollLeft();
  },

  windowSize: function(win) {
    var $window = $j(win || window);

    return [$window.width(), $window.height()];
  },

  placeNearElement: function(elementToPlace, element, shift) {
    if (!shift) {
      shift = {};
      shift.x = 0;
      shift.y = 15;
    }

    element = $(element);

    var pos = element.positionedOffset();
    var x = pos[0] + shift.x;
    var y = pos[1] + shift.y;

    BS.Util.place(elementToPlace, x, y);
  },

  showNearElement: function(near_element, element_to_show, x_shift) {
    var menuDiv = $(element_to_show);
    if (typeof x_shift == 'undefined') {
      x_shift = -180;
    }
    BS.Util.placeNearElement(menuDiv, near_element, {x: x_shift, y: 21});
    BS.Hider.showDivWithTimeout(menuDiv, {hideOnMouseOut: false});
  },

  visible: function(element) {
    element = $(element);

    return jQuery(element).is(':visible');
  },

  show: function() {
    for (var i = 0; i < arguments.length; i++) {
      var element = $(arguments[i]);
      jQuery(element).show();
    }
  },

  hide: function() {
    for (var i = 0; i < arguments.length; i++) {
      var element = $(arguments[i]);
      jQuery(element).hide();
    }
  },

  toggleVisible: function() {
    for (var i = 0; i < arguments.length; i++) {
      var element = $(arguments[i]);
      jQuery(element).toggle();
    }
  },

  //neuro: this is basically copy fo prototype.Form.serialize but with 1.5 contract (send disabled and all submits)
  serializeForm: function(form) {
    var elements = Form.getElements(form);
    var jQueryElementPrefix = BS.jQueryDropdown.namePrefix;
    elements = elements.filter(function(element) {
      return element.type!='password'
             && element.name.indexOf('prop:encrypted') == -1
             && !element.name.endsWith(jQueryElementPrefix);
    });

    var key, value, submitted = false, submit;

    var data = elements.inject({ }, function(result, element) {
      if (element.name) {
        key = element.name; value = $(element).getValue();
        if (value != null && (element.type != 'submit' || (!submitted &&
            submit !== false && (!submit || key == submit) && (submitted = true)))) {
          if (key in result) {
            // a key is already present; construct an array of values
            if (!_.isArray(result[key])) result[key] = [result[key]];
            result[key].push(value);
          }
          else result[key] = value;
        }
      }
      return result;
    });

    return Object.toQueryString(data);
  },

  //neuro: using _wasDisabled to remember state
  disableFormTemp: function(form, elemsFilter) {
    var disabledElems = [];
    for (var i = 0; i < form.elements.length; i++) {
      var element = form.elements[i];
      if (!elemsFilter || elemsFilter(element)) {
        this.disableInputTemp(element);
        disabledElems.push(element);
      }
    }
    BS.VisibilityHandlers.updateVisibility(form);
    return disabledElems;
  },

  isDisabled: function(input) {
    return input.disabled == 'disabled' || input.disabled;
  },

  disableInputTemp: function(input) {
    input.blur();
    if (this.isDisabled(input)) {
      input._wasDisabled = true;
    } else {
      input.disabled = 'disabled';
    }
  },

  reenableInput: function(input) {
    if (typeof input._wasDisabled == 'undefined') {
      input.disabled = '';
    } else {
      input._wasDisabled = undefined;
    }
  },

  //neuro: using _wasDisabled to restore state
  reenableForm: function(form, elemsFilter) {
    for (var i = 0; i < form.elements.length; i++) {
      var element = form.elements[i];
      if (!elemsFilter || elemsFilter(element)) {
        this.reenableInput(element);
      }
    }
    BS.VisibilityHandlers.updateVisibility(form);
  },

  shiftToFitPage: function(el) {
    el = $(el);

    if (el.hasClassName('modalDialogFixed')) return;

    // Fix for TW-10259.
    var overflow = el.style.overflow;
    if (BS.Browser.opera || BS.Browser.msie) { el.style.overflow = 'visible'; }

    var winSize = BS.Util.windowSize();
    var scrollLeft = BS.Util._scrollLeft();
    var pos = el.positionedOffset();
    var dim = el.getDimensions();
    var maxPageX = winSize[0] + scrollLeft;
    var minElemX = pos[0];
    var maxElemX = pos[0] + dim.width;

    if (minElemX < scrollLeft) {
      // if element is hidden by scroller
      el.style.left = (10 + scrollLeft) + 'px';
    } else if (maxElemX > maxPageX && dim.width < maxPageX) {
      // element maximum position by X is outside visible area
      el.style.left = (maxPageX - dim.width - 20) + 'px';
    } else if (dim.width >= maxPageX) {
      // element width is more than visible area
      el.style.left = '10px';
    }

    if (BS.Browser.opera || BS.Browser.msie) { el.style.overflow = overflow; }
  },

  showHelp: function(event, url, options) {
    Event.stop(event);
    var parts = url.split('#');
    if (parts.length == 2) {
      BS.Util.popupWindow(parts[0] + '#' + parts[1].replace(/\+/g, ''), "tcHelp", options);
    }
    else {
      BS.Util.popupWindow(url, "tcHelp", options);
    }
  },

  popupWindow: function(url, target, options) {
    target = target || '_blank';
    options = options || {};

    var width = options.width || 1000;
    var height = options.height || 600;

    var w = window.open(url, target, 'toolbar=no,scrollbars=yes,resizable=yes,width=' + width + ',height=' + height);

    try {
      w.focus();
    } catch(e) {}

    return w;
  },

  hideSuccessMessages: function() {
    if (window._shownMessages) {
      for (var id in window._shownMessages) {
        if ($(id) && window._shownMessages[id] == 'info') {
          $(id).style.visibility = 'hidden';
        }
      }
      window._shownMessages = {};
    }
  },

  showIFrameIfNeeded: function(div) {
    div = $(div);

    var iframe = BS.Util.getOrCreateDialogIFrame(div);
    if (iframe == null) return;

    iframe.absolutize();
    iframe.clonePosition(div);
    var zIndex = parseInt(div.style.zIndex, 10);
    iframe.style.zIndex = "" + (zIndex - 1);
    BS.Util.show(iframe);
  },

  hideIFrameIfNeeded: function(div) {
    div = $(div);

    var iframe = BS.Util.getOrCreateDialogIFrame(div);
    if (iframe == null) return;
    BS.Util.hide(iframe);
  },

  moveDialogIFrame: function(div) {
    div = $(div);

    var iframe = BS.Util.getOrCreateDialogIFrame(div);
    if (iframe == null) return;

    iframe.absolutize();
    iframe.clonePosition(div);
  },

  getOrCreateDialogIFrame: function(div) {
    if (!BS.Browser.msie6) return null;
    var iframe = $('iframe_' + div.id);
    if (iframe == null) {
      iframe = document.createElement("iframe");
      iframe.src = 'javascript:false'; // this is required to disable insecure items warning in IE for https protocol
      iframe.id = 'iframe_' + div.id;
      document.body.appendChild(iframe);
    }

    return $(iframe);
  },

  addWordToTextArea: function(textArea, word) {
    var initialValue = textArea.value;
    if (initialValue.length == 0) {
      textArea.value = word;
    } else {
      textArea.value = initialValue + " " + word;
    }
  },

  processError: function(e) {
    if (e.message) {
      alert(e.message);
    } else {
      alert(e.toString());
    }
  },

  changeChildrenColor: function(parent, options) {
    var color = options.color;
    var bgColor = options.backgroundColor;
    var filter = options.filter;

    var childNodes = parent.childNodes;
    for (var i=0; i<childNodes.length; i++) {
      if (filter && !filter(childNodes[i])) continue;
      if (childNodes[i].style) {
        if (color != null) {
          childNodes[i].style.color = color;
        }

        if (bgColor != null) {
          childNodes[i].style.backgroundColor = bgColor;
        }
      }
    }
  },

  documentRoot: function(transport) {
    if (!transport.responseXML) return null;
    return transport.responseXML.documentElement;
  },

  trimSpaces: function(str) {
    return str.replace(/^\s+(.*)/, "$1").replace(/(.*?)\s+$/, "$1");
  },

  makeBreakable: function(text, regex) {
    regex = regex ? new RegExp("(" + regex + ")", "g") : /(.{60})/g;

    return text.replace(regex, "$1<wbr/>");
  },

  /*
  * Formats time in format 23h:33m:21s
  * If includeSeconds == false, seconds are not shown
  * */
  formatSeconds: function(seconds, includeSeconds) {
    if (seconds < 0) return "N/A";
    if (seconds == 0) return "&lt;1s";

    var result = "";
    var t = parseInt(seconds);

    if (t >= 3600) {
      var hours = Math.floor(t / 3600);
      t -= hours*3600;
      result += hours + "h"
    }

    if (t >= 60) {
      var mins = Math.floor(t / 60);
      t -= mins*60;
      if (result != "") {
        result += ":";
      }
      result += mins + "m";
    }

    if (t > 0 && includeSeconds) {
      seconds = t;
      if (result != "") {
        result += ":";
      }
      result += seconds + "s";
    }
    else if (result == "") {
      result = "&lt;1m";
    }
    return result;
  },

  /**
   * Turns ON all checkboxes with specified name in the specified form
   * @param form
   * @param checkboxName
   */
  selectAll: function(form, checkboxName) {
    this._setChecked(form, checkboxName, true);
  },

  /**
   * Turns OFF all checkboxes with specified name in the specified form
   * @param form
   * @param checkboxName
   */
  unselectAll: function(form, checkboxName) {
    this._setChecked(form, checkboxName, false);
  },

  /**
   * Turns array of the selected checkboxes values
   * @param form
   * @param checkboxName
   */
  getSelectedValues: function(form, checkboxName) {
    var result = [];
    var checkboxes = Form.getInputs(form, "checkbox", checkboxName);
    for (var i=0; i<checkboxes.length; i++) {
      if (checkboxes[i].checked) {
        result.push(checkboxes[i].value);
      }
    }
    return result;
  },

  _setChecked: function(form, checkboxName, checked) {
    var checkboxes = Form.getInputs(form, "checkbox", checkboxName);
    for (var i=0; i<checkboxes.length; i++) {
      if (!checkboxes[i].disabled) {
        checkboxes[i].checked = checked;
      }
    }
  },

  descendantOf: function(child, parent) {
    while (child != null) {
      if (child === parent) return true;
      child = child.parentNode;
    }
    return false;
  },

  isDetached: function(elem) {
    if (elem.id) return $(elem.id) !== elem;

    if (elem === document.documentElement || elem === window || elem === document) return false;
    if (elem.parentNode == null) return true;

    return BS.Util.isDetached(elem.parentNode);
  },

  runWithElement: function(element_id, toRun, timeout) {
    if (!timeout) timeout = 1000;

    BS.WaitFor(function() {
      return $(element_id) && !BS.Util.isDetached($(element_id));
    }, toRun, timeout / 1000.0);
  },

  setTitle: function(title) {
    document.title = title ? title + " -- TeamCity" : "TeamCity";
  },

  capitalize: function(s) {
    return s.replace(/\s+(\w)/g, function(match, chr) {
      return ' ' + chr.toUpperCase();
    });
  },

  fadeOutAndDelete: function(jQuerySelector) {
    $j(jQuerySelector).fadeOut("fast", function() {
      $j(this).remove();
    });
  },

  createDelayedInvocator: function(fun, delay) {
    return {
      _timeoutId: null,
      invoke: function() {
        if (this._timeoutId) {
          clearTimeout(this._timeoutId);
        }
        var that = this;
        this._timeoutId = setTimeout(function() {
          that._timeoutId = null;
          fun();
        }, delay);
      }
    };
  },

  // Escapes IDs containing dots and colons to make them usable as jQuery selectors
  escapeId: function(id) {
    if (id.toString().match(/^#/)) return id;

    //TODO: consider all possible variants: !"#$%&'()*+,./:;<=>?@[\]^`{|}~
    return '#' + id.toString().replace(/(:|\.)/g,'\\$1');
  },

  // Returns element's direct children that are text nodes
  getTextChildren: function(elemId) {
    return $j(BS.Util.escapeId(elemId)).contents().filter(function () {
      return this.nodeType == 3;
    });
  },

  // OS-specific line feed
  getLineFeed: function() {
    return navigator.userAgent.toLowerCase().match(/windows/) ? '\r\n' : '\n';
  }
};

//==========================================================================
/**
Public API:

BS.Hider.showDivWithTimeout('id');
BS.Hider.startHidingDiv('id', delay = 500);
BS.Hider.stopHidingDiv('id');
BS.Hider.hideDiv('id');


BS.Hider.hideAll('id');

*/

BS.Hider = {
  hidingDivs: {},
  allDivs: {},
  afterHideFuncs: {},

  hasVisiblePopups: function() {
    return !$j.isEmptyObject(this.allDivs);
  },

  _currentZindex: function() {
    if (this._shownStack.length == 0) return 10;
    var id = this._shownStack[this._shownStack.length - 1];
    return parseInt($(id).style.zIndex, 10) + 5;
  },

  addHideFunction: function(id, hideFunction) {
    if (typeof this.afterHideFuncs[id] == 'function') {
      var old = this.afterHideFuncs[id];
      this.afterHideFuncs[id] = function() {
        hideFunction();
        old();
      }
    }
    else {
      this.afterHideFuncs[id] = hideFunction;
    }
  },

  showDivWithTimeout: function(id, options) {
    var element = $(id);
    id = element.id;

    if (!options) {
      options = {};
    }

    if (options.hideOnMouseOut === undefined) {
      options.hideOnMouseOut = true;
    }

    if (options.hideOnMouseClickOutside === undefined) {
      options.hideOnMouseClickOutside = true;
    }

    if (options.draggable === undefined) {
      options.draggable = false;
    }

    if (!_.isElement(options.dragHandle)) {
      options.draggable = false;
    }

    var currentZIndex = this._currentZindex();
    if (options.zIndex === undefined || options.zIndex < currentZIndex) {
      options.zIndex = currentZIndex;
    }

    if (options.afterHideFunc != undefined) {
      this.addHideFunction(id, options.afterHideFunc);
    }

    // Modal dialog's position is defined in CSS
    if (!element.hasClassName('modalDialog')) {
      element.style.position = 'absolute';
    }

    element.style.zIndex = "" + options.zIndex;

    BS.Util.show(id);
    BS.Util.shiftToFitPage(id);
    BS.Util.showIFrameIfNeeded(id);

    this.allDivs[id] = id;

    this.stopHidingDiv(id); // if was started previously

    this._shownStack.push(id);
    this._setupHandlers(id, options);

    BS.VisibilityHandlers.updateVisibility(id);

    element._hideOnMouseClickOutside = false;
    if (options.hideOnMouseClickOutside) {
      setTimeout(function() {
        element._hideOnMouseClickOutside = true;
      }.bind(this), 10);
    }

    if (options.draggable && !element._draggable) {
      element._draggable = new Draggable(id, {
        starteffect: function() {},
        endeffect: function() {},
        change: function(queue) {
          BS.Util.moveDialogIFrame(id);
        },
        handle: options.dragHandle
      });
    }
  },

  startHidingDiv: function(id, delay) {
    if (delay == undefined) {
      delay = 500;
    }

    id = $(id).id;

    this.stopHidingDiv(id, true);

    var that = this;
    this.hidingDivs[id] = setTimeout(function() {
      if (that.hidingDivs[id]) {

        // Hide popups that are not pinned
        var isPinned = false, popupToggle = null;
        $j('img.toggle').each(function() {
          if (this.getAttribute('data-popup') == id && this.getAttribute('data-pinned') === 'true') {
            isPinned = true;
            popupToggle = this;
          }
        });

        if (!isPinned) {
          that.hideDivSingle(id);
          if (popupToggle) {
            popupToggle.removeAttribute('data-pinned');
            popupToggle.removeAttribute('data-popup');
          }
        }
      }
    }, delay);
  },

  /* Hides the whole stack of open popups */
  hideDiv: function(id) {
    var divPos = -1;
    for (var i=0; i<this._shownStack.length; i++) {
      if (this._shownStack[i] == id) {
        divPos = i;
        break;
      }
    }

    if (divPos != -1) {
      var numShown = this._shownStack.length - divPos;
      while (numShown > 0) {
        var topId = this._shownStack.pop();
        this.hideDivSingle(topId);
        numShown--;
      }
    }

    if (this.afterHideFuncs[id]) {
      this.afterHideFuncs[id]();
      delete this.afterHideFuncs[id];
    }
  },

  /* Hides a single popup */
  hideDivSingle: function(id) {
    BS.Util.hideIFrameIfNeeded(id);
    this.stopHidingDiv(id, true);
    delete this.allDivs[id];

    var elem = $(id);

    if (elem && elem._draggable) {
      elem._draggable.destroy();
      elem._draggable = null;
    }
    Event.stopObserving(elem);
    BS.Util.hide(elem);

    if (this.afterHideFuncs[id]) {
      this.afterHideFuncs[id]();
      delete this.afterHideFuncs[id];
    }
  },

  stopHidingDiv: function(id, thisDivOnly) {
    var idAttr = $(id).id;
    if (this.hidingDivs[idAttr]) {
      if (thisDivOnly) {
        clearTimeout(this.hidingDivs[idAttr]);
      } else {
        for (var i=0; i<this._shownStack.length; i++) {
          clearTimeout(this._shownStack[i]);
          if (this._shownStack[i] == idAttr) {
            break;
          }
        }
      }

      delete this.hidingDivs[idAttr];
    }
  },

  hideAll: function(stopOn) {
    var divs = [];
    for(var divId in this.allDivs) {
      if (!$(divId)) continue;
      divs.push($(divId));
    }

    // sort divs according to their z-index
    divs.sort(function(div1, div2) {
      var zI1 = parseInt(div1.style.zIndex, 10);
      var zI2 = parseInt(div2.style.zIndex, 10);
      return zI1 - zI2;
    });

    for (var i=divs.length-1; i>=0; i--) {
      var div = divs[i];
      if (div == stopOn) {
        break;
      }
      if (!div._hideOnMouseClickOutside) break;
      this.hideDiv(div.id);
      div._hideOnMouseClickOutside = true;
    }

    // Reset 'pinned' state
    $j('img.toggle').each(function() {
      if (stopOn && this.getAttribute('data-popup') == stopOn.id) return false;

      this.removeAttribute('data-pinned');
      this.removeAttribute('data-popup');
    })
  },

  _shownStack: [],

  _setupHandlers: function(id, options) {
    var el = $(id);

    if (options.hideOnMouseOut) {
      el.on("mouseout", function() {
        // don't hide the popup if a certain element is provided as override (TW-18908)
        if (document.activeElement) {
          if (options.overrideHideIfActive && document.activeElement == $(options.overrideHideIfActive)) {
            return;
          }
        }

        // we should not hide popup on mouse out if there are visible popups shown after this popup
        var top = BS.Hider._shownStack[BS.Hider._shownStack.length - 1];

        if (top != id) {
          return;
        }

        if (typeof options.hideOnMouseOut == 'function') {
          if (!options.hideOnMouseOut())
            return;
        }

        BS.Hider.startHidingDiv(id);
      });
    }

    el.on("mouseover", function() {
      BS.Hider.stopHidingDiv(id);
    });
  },

  _escapeHandler: function(event) {
    if (this._shownStack.length == 0) return;
    if (event.keyCode == Event.KEY_ESC) {
      var id = this._shownStack[this._shownStack.length - 1];
      BS.Hider.hideDiv(id);
      Event.stop(event);
    }
  }
};

jQuery(document).ready(function() {
  jQuery('#bodyWrapper').on("click", function(e) {
    var t = e.target;
    // Don't hide popups if click occurred inside one of the popups
    var stopOn = null;
    for(var div in BS.Hider.allDivs) {
      var elem = jQuery(div).get(0);
      if (!elem) continue;
      if (BS.Util.descendantOf(t, elem)) {
        stopOn = elem;
        break;
      };
    }
    BS.Hider.hideAll(stopOn);
  });

  $(document).on("keyup", BS.Hider._escapeHandler.bindAsEventListener(BS.Hider));
});


//==========================================================================

BS.Navigation = {
    items: [],

    siblingsNavType: null,

    writeBreadcrumbs: function() {
      if (this.items.length == 0) return;

      $j('#main_navigation').html(this.getItemsHtml()).css('visibility', 'visible');

      // Breadcrumbs can be rendered before domready,
      // but for the siblings navigation we have to wait until the DOM has fully loaded.
      var self = this;
      $j(document).ready(function() {
        self._renderSiblingsNav();
      });

      this._selectHeaderTabIfNeeded();
      $j(document).trigger("bs.navigationRendered");
    },

    getItemsHtml: function() {
      var result = "";
      for (var i = 0; i < this.items.length - 1; i ++) {
        result += this._writeItem(this.items[i]);
      }
      if (this.items.length > 0) {
        result += this._writeItem(this.items[this.items.length - 1], "last");
      }
      return result;
    },

    _writeItem: function(navItem, li_class) {
      function trimDescription(content, idx) {
        var description = content.substr(idx);

        if (description.length > 80) {
          if (description.indexOf("<script") > -1 || description.indexOf("<a") > -1) {
            return description;
          }

          var descriptionText = $j(description).text();
          descriptionText = descriptionText.substr(0, 40) + '&hellip;' + descriptionText.substr(descriptionText.length - 40, descriptionText.length);
          description = '<small>' + descriptionText + '</small>';
        }

        return description;
      }

      var content = navItem.title, idx = content.indexOf("<small>");
      var li_classes = [], li_attributes = [];

      li_classes.push(li_class);

      if (navItem.selected) {
        li_classes.push("selected");
      }

      if (navItem.itemClass) {
        li_classes.push(navItem.itemClass);
      }

      if (navItem.url) {
        if (idx >= 0) {
          content = '<a href="' + navItem.url + '">' + $j.trim(navItem.title.substr(0, idx)) + '</a>' + '&nbsp;' + trimDescription(content, idx);
        }
        else {
          content = '<a href="' + navItem.url + '">' + $j.trim(navItem.title) + '</a>';
        }
      } else {
        if (idx >= 0) {
          content = content.substr(0, idx) + trimDescription(content, idx);
        }
      }

      if (navItem.siblings) {
        this._prepareSiblingsNav(navItem);
      }

      if (navItem.buildTypeId) {
        li_attributes.push('data-buildTypeId="' + navItem.buildTypeId + '"');
      }

      li_classes = li_classes.join(" ");
      li_attributes = li_attributes.join(" ");

      return '<li class="' + $j.trim(li_classes) +'"' + li_attributes + '>' + content + '</li>';
    },

    _prepareSiblingsNav: function(navItem) {
      this.siblingsNav = navItem;
    },

    _renderSiblingsNav: function() {
      var container = $j('#main_navigation'), subContainer = null;

      if (this.siblingsNav) {
        if (this.siblingsNav.siblings.type == 'buildType') {
          subContainer = container.find('.buildType');

          // if matching breadcrumb item not found, use the last one
          if (subContainer.length == 0) {
            subContainer = container.find('li').last();
          }

          $j('.siblingBuildTypes').appendTo(subContainer).css('display', '');
          BS.SiblingsPopup.install(this.siblingsNav.siblings);
        }
      }
    },

    _selectHeaderTabIfNeeded: function() {
      var navItem = this.items[0];
      if (navItem.url && navItem.url.endsWith("/admin/admin.html")) {
        this.selectAdminTab();
      }
      if (navItem.itemClass && navItem.itemClass == "project") {
        this.selectProjectsTab();
      }
    },

    selectProjectsTab: function() {
      jQuery("#overview_Tab").addClass("selected");
    },

    selectAdminTab: function() {
      jQuery("#userPanel .info:nth-child(2)").addClass("selected");
    },

    selectMySettingsTab: function() {
      jQuery("#userPanel .info:nth-child(1)").addClass("selected");
    }
};

//==========================================================================

BS.Highlight = function(element, options) {
  new Effect.Highlight(element, _.extend(options || {}, {
    startcolor: '#ffffcc', duration: 1.0
  }));
};


BS.Logout = function(logoutUrl) {
  BS.ajaxRequest(logoutUrl, {
    onComplete: function(transport) {
      BS.XMLResponse.processRedirect(transport.responseXML);
    }
  });
};

BS.XMLResponse = {
  processModified: function(form, responseXML) {
    if (!responseXML) return;

    var rootElement = responseXML.documentElement;
    form.setModified(rootElement.firstChild && rootElement.firstChild.nodeValue == "modified");
  },

  processRedirect: function(responseXML) {
    if (!responseXML) return false;

    var rootElement = responseXML.documentElement;
    var redirect = rootElement.getElementsByTagName("redirect")[0];
    if (redirect) {
      document.location.href = redirect.firstChild.nodeValue;
      return true;
    }
    return false;
  },

  /** Error handlers is an object with error handlers with names like onFieldError,
   * where 'field' is 'id' of the 'error' element in XML response.
   * The handler is called with 'this' == errorHandlers and XML error element goes as the first parameter.
   * To access text of the error node, use syntax 'param.firstChild.nodeValue'
   * */
  processErrors: function(responseXML, errorHandlers, generalErrorHandler) {
    var eNodes = this._getErrorNodes(responseXML);
    if (!eNodes || eNodes.length == 0) return false;

    var handled = false;
    for (var i=0; i<eNodes.length; i++) {
      var elem = eNodes.item(i);
      var id = elem.getAttribute("id");
      var funcName = "on" + id.charAt(0).toUpperCase() + id.substring(1) + "Error";
      var handler = errorHandlers[funcName] || errorHandlers[id];

      if (handler && typeof(handler) == 'function') {
        if (elem)
        handler.apply(errorHandlers, [elem]);
        handled = true;
      } else if (generalErrorHandler) {
        generalErrorHandler(id, elem);
        handled = true;
      }
    }

    return handled;
  },

  _xmlErrorsXml: function(responseXml) {
    var errs = responseXml.getElementsByTagName("errors");
    if (!errs || errs.length == 0 || errs[0].getElementsByTagName("error").length == 0) return [];
    var errorElements = errs[0].getElementsByTagName("error");
    var result = [];
    for (var i = 0; i < errorElements.length; i ++) {
      var e = errorElements[i];
      result.push([e.getAttribute("id"), e.firstChild.nodeValue]);
    }
    return result;
  },

  _getErrorNodes: function(responseXML) {
    var parentElement = responseXML.documentElement;
    if (parentElement == null) return null;

    var errorsNodes = parentElement.getElementsByTagName("errors");
    if (!errorsNodes || errorsNodes.length == 0) return null;
    var errorsNode = errorsNodes.item(0);
    return errorsNode.getElementsByTagName("error");
  }
};

BS.StopBuild = function(actionUrl, id, form) {
  if (form) {
    Form.disable(form);
  }

  BS.ajaxRequest(actionUrl + "?kill=" + id, {
    onComplete: function(transport) {
      setTimeout(function() {
        BS.reload(true);
      }, 3000);
    }
  });

  return false;
};

BS.TableHighlighting = {};
BS.TableHighlighting.createInitElementFunction = function(mouseovertitle) {
  var element = this;

  // New calling convention (jQuery)
  if (arguments.length == 2) {
    mouseovertitle = arguments[1];
  }

  var f = function(element) {
    $(element).on("mouseover", function() {
      if (typeof(BS) == "undefined") return;
      var noTitle = element.className.indexOf("noTitle") != -1 || element.parentNode.className.indexOf("noTitle") != -1;
      var hasTitle = element.title || element.parentNode.title;
      if (!noTitle && !hasTitle) {
        element.title = mouseovertitle;
      }

      BS.Util.changeChildrenColor(this.parentNode, {
        color: '#254193',
        backgroundColor: '#ffffcc',
        filter: function(elem) {
          return elem.nodeType == 1 && elem.className.indexOf("highlight") >= 0;
        }
      });
    }.bind(element));

    $(element).on("mouseout", function() {
      if (typeof(BS) == "undefined") return;
      BS.Util.changeChildrenColor(this.parentNode, {
        color: '',
        backgroundColor: '',
        filter: function(elem) {
          return elem.nodeType == 1 && elem.className.indexOf("highlight") >= 0;
        }
      });
    }.bind(element));
  };

  if (!element.nodeType) {
    // Old calling convention (Behaviour.js)
    return f;
  } else {
    // jQuery
    return f(element);
  }
};

BS.Refreshable = {
  createRefreshFunction: function(containerId, pageUrl, passJsp) {
    var container = $(containerId);

    if (!container) return Prototype.emptyFunction;

    return function(progressId, moreParameters, afterComplete) {
      if (BS.ServerLink && !BS.ServerLink.isConnectionAvailable()) {
        BS.Log.info("Connection to server is not available. Refresh is not called.");
        return;
      }

      if (!moreParameters) {
        moreParameters = "";
      }

      if (progressId && $(progressId)) {
        BS.Util.show(progressId);
      }

      var url = container.refreshUrl;
      if (!url) {
        url = pageUrl;
      }

      var myParams = passJsp ? "jsp=" + passJsp : "__fragmentId=" + containerId + "Inner";
      BS.ajaxRequest(url, {
        method: 'get',
        parameters: myParams + (moreParameters ? "&" + moreParameters : ""),
        onFailure: function() {
          BS.Log.warn("Failure while refreshing " + containerId);
        },
        onComplete: function(response) {
          var status = response.request.getStatus();
          var success = !status || (status >= 200 && status < 300);

          // Do nothing if request has failed (server was unavailable)
          if (!success)
            return;

          BS.stopObservingInContainers(container);
          container.update(response.responseText);
          BS.Util.hide(progressId);
          _.isFunction(afterComplete) && afterComplete();
        }
      });
    }
  }
};


BS.WaitFor = function(condition, runWhenDone, waitSeconds) {
  if (!waitSeconds) waitSeconds = 10;
  var maxCount = waitSeconds * 1000 / 50;
  var counter = 0;

  var _waitForHandler = function() {
    if (!condition() && maxCount > counter++) {
      setTimeout(_waitForHandler, 50);
    }
    else {
      runWhenDone();
    }
  };
  _waitForHandler();
};

Event.observe(window, "unload", function() {
  BS.PeriodicalRefresh.stop();
});

BS.PeriodicalRefresh = Class.create();
BS.PeriodicalRefresh._removeIntervalUpdate = function() {
  if (this._timeoutId) {
    clearTimeout(this._timeoutId);
  }
};

BS.PeriodicalRefresh.start = function(interval, refreshFunc) {
  this._removeIntervalUpdate();
  this._timeoutId = null;

  var that = this;
  var refreshFunction = function() {
    that._timeoutId = setTimeout(function() {
      BS.reload(false, refreshFunc);
      refreshFunction();
    }, interval * 1000);
  };

  if (interval > 0) {
    refreshFunction();
  }
};

BS.PeriodicalRefresh.stop = function() {
  this._removeIntervalUpdate();
};

BS.InPlaceFilter = {
  applyFilter: function(containerId, filterField, afterFilterFunc) {
    var container = $j(BS.Util.escapeId(containerId));

    var keyword = filterField.value.toUpperCase();
    if (keyword == this.prevKeyword) return;

    var narrowSearch = this.prevKeyword != null && keyword.indexOf(this.prevKeyword) != -1;
    this.prevKeyword = keyword;

    var elementsToFilter = container.find('.inplaceFiltered');

    var that = this;
    // Search and toggle option visibility
    elementsToFilter.each(function(i) {
      var elem = this;
      if (narrowSearch && !that.isVisible(elem)) return;

      var text = that.getContent(elem);
      that.performSearch(elem, text, keyword, i);
    });

    // Toggle optgroup visibility
    if (container.prop('tagName').toUpperCase() == 'SELECT') {
      elementsToFilter.each(function(index) {
        var thisClassName = this.className;

        if (thisClassName.indexOf('optgroup') == -1) return;

        var hasVisible = false,
            next = this.nextSibling;

        while (next) {
          if (next.nodeType == 1) {
            // Next group reached
            if (next.disabled) {
              break;
            }

            // Visible option reached
            if (next.className.indexOf('hiddenOption') == -1) {
              hasVisible = true;
              break;
            }
          }

          next = next.nextSibling;
        }

        if (hasVisible) {
          thisClassName.indexOf('hiddenOption') > -1 && that.showOption(this, index);
        } else {
          thisClassName.indexOf('hiddenOption') == -1 && that.hideOption(this, index);
        }
      });
    }

    if (afterFilterFunc) {
      afterFilterFunc.call(this, filterField);
    }
  },

  performSearch: function(elem, text, keyword, idx) {
    var found = text.indexOf(keyword) > -1;
    if (!found) {
      this.hideElement(elem, idx);
    } else {
      this.showElement(elem, idx);
    }
    return found;
  },

  hideElement: function(elem, idx) {
    if (elem.tagName.toUpperCase() == 'OPTION') {
      if (elem.className.indexOf('optgroup') == -1) {
        this.hideOption(elem, idx);
      }
    } else {
      if (elem.tagName.toUpperCase() != 'OPTGROUP') {
        BS.Util.hide(elem);
      }
    }
  },

  showElement: function(elem, idx) {
    if (this.isHiddenOption(elem)) {
      this.showOption(elem, idx);
    } else {
      BS.Util.show(elem);
    }
  },

  isVisible: function(elem) {
    return elem.style.display != 'none';
  },

  /*
   * Retrieves text contents of the element, including child nodes.
   * If a child node contains text that should not be searchable, add a 'noSearch' class to such element.
   */
  getContent: function(elem) {
    var content = elem._cachedContent;
    if (content != null) return content;

    content = "";
    
    // Take content from data-title, if none is present - search in child nodes,
    // omitting the nodes with noSearch class
    if (elem.getAttribute('data-title')) {
      content = elem.getAttribute('data-title');
    } else {
      for (var i=0; i<elem.childNodes.length; i++) {
        var node = elem.childNodes[i];
        if (node.nodeType == 1 && node.className.indexOf('noSearch') == -1) {
          content += this.getContent(node);
        }
        if (node.nodeType == 3) {
          content += node.nodeValue;
        }
      }
    }

    elem._cachedContent = content.replace(/[\s]+/g, ' ').strip().toUpperCase();
    return elem._cachedContent;
  },

  isHiddenOption: function(elem) {
    return elem.className && elem.className.indexOf('hiddenOption') != -1;
  },

  hideOption: function(elem, idx) {
    var selectElem = elem.parentNode;
    if (!selectElem._hiddenOptions) {
      selectElem._hiddenOptions = {};
    }

    selectElem._hiddenOptions[idx] = elem;

    var newElem = document.createElement("SPAN");
    newElem.className = 'inplaceFiltered hiddenOption';
    newElem._cachedContent = this.getContent(elem);
    if (elem.disabled) {
      newElem.disabled = true;
    }

    if (elem.className.indexOf('optgroup') > -1) {
      newElem.className += ' optgroup';
    }

    selectElem.insertBefore(newElem, elem);
    selectElem.removeChild(elem);
  },

  showOption: function(elem, idx) {
    var selectElem = elem.parentNode;
    if (selectElem._hiddenOptions[idx]) {
      selectElem.insertBefore(selectElem._hiddenOptions[idx], elem);
      selectElem.removeChild(elem);

      selectElem._hiddenOptions[idx] = null;
    }
  },

  prepareFilter: function(containerId) {
    var container = $j(BS.Util.escapeId(containerId));

    if(container.length > 0 && container.prop('tagName').toUpperCase() == 'SELECT') {
      container.children('optgroup').each(function() {
        var optgroup = $j(this),
            options = this.getElementsByTagName('option');

        if (options.length == 0) {
          optgroup.remove();
          return;
        }

        for (var i=options.length-1; i>=0; i--) {
          var option = options[i];
          optgroup.after(option);
          option.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;' + option.innerHTML;
        }

        optgroup.replaceWith('<option class="inplaceFiltered optgroup" disabled="disabled">' + optgroup.attr('label') + '</option>');
      });
    }
  }
};

BS.VisibilityHandlers = {
  _emptyHandler: { updateVisibility: function() {} },
  _handlers: {},

  attachTo: function(control, handler) {
    var controlId = $(control).id;
    var controlHandlers = this._handlers[controlId];
    if (controlHandlers == null) {
      controlHandlers = [];
      this._handlers[controlId] = controlHandlers;
    }
    controlHandlers.push(handler);
  },

  detachFrom: function(elementId) {
    this._handlers[elementId] = null;
  },

  _visibilityHandler: function(element) {
    if (!element || !element.id) return this._emptyHandler;

    var controlHandlers = this._handlers[element.id];
    if (controlHandlers == null) return this._emptyHandler;

    return {
      updateVisibility: function() {
        for (var i=0; i<controlHandlers.length; i++) {
          controlHandlers[i].updateVisibility();
        }
      }
    };
  },

  _collectElements: function(parentEl) {
    var elems = [];
    if (parentEl == null) return elems;

    if (parentEl.id) {
      elems.push(parentEl);
    }

    if (parentEl.id == 'mainContent') {
      for (var id in this._handlers) {
        if (!this._handlers.hasOwnProperty(id)) continue;
        elems.push($(id));
      }
    } else {
      for (var id in this._handlers) {
        if (!this._handlers.hasOwnProperty(id)) continue;
        var elem = $(id);
        if (BS.Util.descendantOf(elem, parentEl)) {
          elems.push(elem);
        }
      }
    }
    return elems;
  },

  updateVisibility: function(el) {
    if (el == null) return;
    var elems = this._collectElements($(el));
    for (var i=0; i<elems.length; i++) {
      this._visibilityHandler(elems[i]).updateVisibility();
    }
  }
};

BS.User = {
  setProperty: function(key, value, options) {
    this._setProperty(key, value, options, "setUserProperty");
  },

  deleteProperty: function(key, options) {
    this._beforeChange(options);

    BS.ajaxRequest(window['base_uri'] + '/ajax.html', {
      parameters: 'deleteUserProperty=' + key,
      onComplete: function() {
        BS.User._afterChange(options);
      }
    })
  },

  setBooleanProperty: function(key, value, options) {
    if (value) {
      this.setProperty(key, value, options);
    } else {
      this.deleteProperty(key, options);
    }
  },

  /**
   * @param key string key property, you can get property value on the server-side using session[key] in JSP
   * @param options have keys 'progress' - ID of progress element; afterComplete - function to call after property is set
   * */
  setSessionProperty: function(key, value, options) {
    this._setProperty(key, value, options, "setSessionProperty");
  },

  _setProperty: function(key, value, options, methodName) {
    if (!options) options = {};
    this._beforeChange(options);

    BS.ajaxRequest(window['base_uri'] + '/ajax.html', {
      parameters: methodName + '=' + key + '&value=' + value,
      onComplete: function() {
        BS.User._afterChange(options);
      }
    })
  },


  _beforeChange: function(options) {
    if (options && options.progress) {
      BS.Util.show(options.progress);
    }
  },

  _afterChange: function(options) {
    options = options || {};
    if (options.afterComplete) {
      options.afterComplete();
    }
    if (options.progress) {
      BS.Util.hide(options.progress);
    }
  }
};

/**-------------------------------------------------------*/
/*-------- Simple delayed action support -----------------*/
/**-------------------------------------------------------*/
BS.DelayedAction = function(action_start, action_stop, delay) {
  if (!delay) delay = 300;
  this.delay = delay;
  this.action = action_start;
  this.stop_action = action_stop;
};

BS.DelayedAction.prototype.start = function() {
  this.timeout = setTimeout(this.action, this.delay);
};

BS.DelayedAction.prototype.stop = function() {
  clearTimeout(this.timeout);
  this.stop_action.call(this);
};

/**
 Usage:
 var progress = new BS.DelayedShow(element_id); // == new BS.DelayedShow(element_id, 300);
 progress.show();

 // some ajax call
 // onComplete: function() { progress.hide();}

 */

BS.DelayedShow = function(element) {
  BS.DelayedAction.call(this, function() {
    if ($(element)) $(element).show();
  }, function() {
    if ($(element)) $(element).hide();
  });
};

BS.DelayedShow.prototype = new BS.DelayedAction();
BS.DelayedShow.prototype.show = BS.DelayedShow.prototype.start;
BS.DelayedShow.prototype.hide = BS.DelayedShow.prototype.stop;

/**-------------------------------------------------------*/


/* Catch Javascript problems in AJAX handlers: */
Ajax.Responders.register({
  onException: function(r, e) {
    BS.Log.error(e);
  }
});

// Depends on jquery UFD plugin.
// See http://code.google.com/p/ufd/
BS.jQueryDropdown = function(selector, options) {
  var dropDown = jQuery(selector);
  var id = dropDown.attr('id');

  // Check if already initialized.
  if ($(BS.jQueryDropdown.namePrefix + id)) {
    try {
      jQuery($(id)).ufd("destroy");
    } catch(e) {
      BS.Log.warn(e);
    }
  }

  var selectElem = dropDown.get(0);
  var name = selectElem.name;
  id = selectElem.id;

  dropDown.ufd(jQuery.extend(true, options || {}, {
    css: { button: BS.jQueryDropdown.namePrefix + (name ? name : id) },
    calculateZIndex: true,
    zIndexPopup: BS.Hider._currentZindex()
  }));

  return dropDown;
};

// jQuery ufd creates temporary elements with names
// that ends with that prefix. We need to filter those
// fields out when sending parameters to the server.
BS.jQueryDropdown.namePrefix = "-ufd-teamcity-ui-";

BS.jQueryDropdown.setJQueryOptions = function($) {
  if ($.ui && $.ui.ufd) {
    $.ui.ufd.defaults.skin = "default";
    $.ui.ufd.defaults.prefix = BS.jQueryDropdown.namePrefix;
    $.ui.ufd.prototype.options = $.ui.ufd.defaults; // 1.8 default options location
  }
};

BS.jQueryDropdown.setJQueryOptions(jQuery);

// Please consider NOT using this method if possible. An "A" element with display: block is usually
// a better idea than making a DIV or a TD clickable
BS.openUrl = function(event, url) {
  if (Event.element(event) && Event.element(event).tagName.toUpperCase() == 'A') {
    // Handle links normally
    return true;
  } else {
    if (Event.isLeftClick(event)) {
      // New tab requested
      if (event.ctrlKey || event.metaKey) {
        window.open(url);
      // Plain normal click
      } else {
        document.location.href = url;
      }
    // New tab requested
    } else if (Event.isMiddleClick(event)) {
      window.open(url);
    // If we made it until here, and the following check is false - assume the left button
    // was clicked because we ran out of buttons.
    // Why? Because button detection is unstable for click events (looking at you, IE)
    // http://www.quirksmode.org/js/events_properties.html#button
    } else if (!Event.isRightClick(event)) {
      document.location.href = url;
    }
  }
  return false;
};

BS.LoadStyleSheetDynamically = function (url, callback) {
  var head = document.getElementsByTagName('head')[0];
  var existingStylesheet = null;
  var stylesheet;

  jQuery('link[type="text/css"], style').each(function() {
    if (this.tagName.toLowerCase() == 'link' && this.href == url || this.getAttribute('data-href') == url) {
      existingStylesheet = this;
      return false;
    }
  });

  var intervalId;
  var waitForStylesheet = function(stylesheet, callback) {
    if (stylesheet.getAttribute('data-loaded')) {
      clearInterval(intervalId);
      callback();
    }
  };

  if (!callback) {
    if (existingStylesheet) return;
    stylesheet = document.createElement('link');
    stylesheet.type = 'text/css';
    stylesheet.rel = 'stylesheet';
    stylesheet.title = 'dynamicLoadedSheet';
    stylesheet.href = url;
    if (!BS.Browser.msie) {
      head.appendChild(stylesheet);
    } else {
      head.insertBefore(stylesheet, head.firstChild);
    }
  } else {
    if (!existingStylesheet) {
      stylesheet = document.createElement('style');
      stylesheet.type = 'text/css';
      stylesheet.setAttribute('data-href', url);
      if (!BS.Browser.msie) {
        head.appendChild(stylesheet);
      } else {
        head.insertBefore(stylesheet, head.firstChild);
      }
      jQuery.get(url, function(contents) {
        if (BS.Browser.msie && parseInt($j.browser.version, 10) < 9) {
          stylesheet.styleSheet.cssText = contents;
        } else {
          stylesheet.textContent = contents;
        }
        stylesheet.setAttribute('data-loaded', true);
        callback();
      });
    } else {
      if (existingStylesheet.tagName.toLowerCase() == 'style') {
        intervalId = setInterval(function() {
          waitForStylesheet(existingStylesheet, callback);
        }, 50);
      } else {
        callback();
      }
    }
  }
};

BS.stopPropagation = function(event) {
  if ($j.browser.msie) {
    event.cancelBubble = true;
  } else {
    event.stopPropagation();
  }
};
