BS.SiblingsPopup = function(siblings) {
  /**
   * Replaces build type id in the URL with a new value
   * @param buildTypeId e.g. "bt4"
   * @param urlParamName e.g. "buildTypeId"
   * @param urlValuePrefix e.g. "buildType:"
   */

  var menuItems;
  if (siblings.type == 'buildType') {
    menuItems = siblings.buildTypes;
  }

  var menuList = $j('<ul class="menuList"/>');

  function setBuildTypeId(buildTypeId, urlParamName, urlValuePrefix) {
    var params = document.location.search.substr(1).split("&");

    for (var i=0; i<params.length; i++) {
      var param = params[i].split("=");

      if (param[0] == urlParamName) {
        param[1] = urlValuePrefix + buildTypeId;
        params[i] = param.join("=");
        break;
      }
    }

    return '?' + params.join("&");
  }

  BS.Popup.call(this, 'siblingsPopup', {
    className: 'quickLinksMenuPopup buildTypesPopup custom-scroll',
    shift: {x: 9, y: 25},
    fitWithinWindowHeight: true,
    fitWithinWindowBottomMargin: 100,
    doScroll: false,

    htmlProvider: function() {
      menuList.empty();
      for (var i=0; i<menuItems.length; i++) {
        var menuItem = $j('<li class="menuItem"/>');
        var menuLink = $j('<a href="#"/>');

        menuLink.text(menuItems[i].name);

        if (siblings.urlParamName) {
          menuLink.attr('href', setBuildTypeId(menuItems[i].id, siblings.urlParamName, siblings.urlValuePrefix));
        } else if (siblings.url) {
          menuLink.attr('href', siblings.url + menuItems[i].id);
        }

        if (menuItems[i].selected === true) {
          menuItem.addClass('current');
        }

        menuLink.appendTo(menuItem);
        menuItem.appendTo(menuList);
      }

      return menuList;
    }
  });
};

_.extend(BS.SiblingsPopup.prototype, BS.Popup.prototype);

BS.SiblingsPopup.install = function(siblings) {
  this.popup = new BS.SiblingsPopup(siblings);

  var toggle = this.toggle = $j('.siblingBuildTypes');

  var that = this;
  toggle.mouseover(function() {
    that.popup.showPopupNearElement(that.toggle.get(0));
  });

  toggle.mouseout(function() {
    that.popup.hidePopup();
  });

  toggle.attr('title', 'Jump to build configuration (keyboard shortcut [j])');

  // Keyboard invocation ('j' shortcut)
  jQuery(document).keydown(function(e) {
    if (e.keyCode == 74) {

      var element = jQuery(e.target);
      if (element.is("input") || element.is("textarea") || element.is("select")) {
        return;
      }
      toggle.mouseover();
    }
  });

};