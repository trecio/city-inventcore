<%@ page import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/include-internal.jsp"%>
<jsp:useBean id="serverBaseUrl" scope="request" type="java.lang.String"/>
<jsp:useBean id="eclipseUpdatePath" scope="request" type="java.lang.String"/>
<html>
<head>
  <title>Eclipse Plugin -- TeamCity</title>
  <bs:linkCSS>
    /css/main.css
  </bs:linkCSS>
</head>
<body>
<div id="mainContent">
  <p>
    <c:set var="url"><c:url value="${serverBaseUrl}${eclipseUpdatePath}"/></c:set>
    Use the link <a href="${url}">${url}</a> as Eclipse update site.
  </p>
  <p>
    To install the plugin, use "Help | Software Updates" in Eclipse.
  </p>
  <p>
    See TeamCity <a href="<bs:helpUrlPrefix/>/Eclipse+Plugin">online help</a> for more details.
  </p>

</div>
</body>
</html>