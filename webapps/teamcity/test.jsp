<%--
  ~ Copyright (c) 2006, JetBrains, s.r.o. All Rights Reserved.
  --%>

<%@ include file="include-internal.jsp" %>
<bs:page>
<jsp:attribute name="head_include">
  <bs:linkCSS>
    /css/tabs.css
  </bs:linkCSS>
  <bs:linkScript>
    /js/bs/tabs.js
  </bs:linkScript>
  <script type="text/javascript">
    $j(document).ready(function(){
      var tabs = new TabbedPane();

      tabs.addTab("tab1", {
        caption: "<b>Some caption1</b>",
        url: "http://www.google.com"
      });

      tabs.addTab("tab2", {
        captionFrom: "captionDiv",
        onselect: function(tab) {
          alert("myId: " + tab.getId());
        }
      });

      tabs.showIn('x');
      tabs.setActiveCaption('tab1');

      window.tabs = tabs;
    });
  </script>
</jsp:attribute>

<jsp:attribute name="body_include">
  <div id='captionDiv'>

    some <i>extended text</i>
    <img src="img/artifacts.png" alt="" width="18" height="18"/>
    <br/>



  </div>


  sfdg<br><br><br><br><br>
  fdg
  dfsgsd
  fg
  <br/>

  <a class="expandable" href="#">this is an expandable link</a>
  <a class="collapsable" href="#">this is a collapsable link</a>
  <a class="popup" href="#">this is a popup link</a>

  <br/>

  <a href="#" onclick="window.tabs.setActiveTab('tab1');">tab 1</a>
  <a href="#" onclick="window.tabs.setActiveTab('tab2');">tab 2</a>
  <a href="#" onclick="BS.Hider.showDivWithTimeout('captionDiv', false);">show div</a>


  <div id="x"></div>

</jsp:attribute>

</bs:page>

