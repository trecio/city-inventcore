<%@ include file="../include-internal.jsp"

%><bs:page
  ><jsp:attribute name="page_title">My Changes</jsp:attribute>
    <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/progress.css
      /css/filePopup.css
      /css/overviewTable.css
      /css/viewModification.css

      /css/changes.css
      /css/buildQueue.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/tree/tree_model.js
      /js/tree/tree_selection.js
      /js/bs/async.js
      /js/bs/changes.js
      /js/bs/buildResultsDiv.js
      /js/bs/testDetails.js
      /js/bs/hideSuccessfulBuildTypes.js

      /js/bs/blocks.js
      /js/bs/blockWithHandle.js
      /js/bs/testGroup.js

      /js/bs/queueLikeSorter.js
      /js/bs/buildQueue.js
    </bs:linkScript>
    <script type="text/javascript">
      BS.Navigation.items = [
        {title: "My Changes", selected:true}
      ];

      BS.topNavPane.setActiveCaption('changes');
    </script>

  <et:subscribeOnEvents>
    <jsp:attribute name="eventNames">
      BUILD_FINISHED
    </jsp:attribute>
    <jsp:attribute name="eventHandler">
      BS.ChangePage.updateChanges();
    </jsp:attribute>
  </et:subscribeOnEvents>
  <et:subscribeOnUserEvents userId="${currentUser.id}">
    <jsp:attribute name="eventNames">
      CHANGE_ADDED
      PERSONAL_BUILD_CHANGED_STATUS
      PERSONAL_BUILD_STARTED
      PERSONAL_BUILD_FINISHED
      PERSONAL_BUILD_ADDED_TO_QUEUE
      PERSONAL_BUILD_INTERRUPTED
    </jsp:attribute>
    <jsp:attribute name="eventHandler">
      BS.ChangePage.updateChanges();
    </jsp:attribute>
  </et:subscribeOnUserEvents>

  </jsp:attribute
  ><jsp:attribute name="body_include">

    <div id="showingAllBlock" style="display: none;">
      All your commits and configurations are shown
      (<a title="Show filtered" href="#" onclick="BS.ChangePage.showUsingFilter(); return false">show as filtered on the Projects dashboard</a>)
    </div>
    <div id="showingFilteredBlock" style="display: none;">
      This page doesn't show
      <span id="hasSkippedChanges"><span class="highlightChanges">your commits</span> to the</span>
      build configurations hidden from the Projects dashboard
      (<a title="Show all" href="#" onclick="BS.ChangePage.showAll(); return false">show all</a>)
    </div>

    <div class="simpleTabs clearfix" id="projectsFilter"></div>

    <div id="carpetLegend" >
      <bs:helpPopup linkText="What do colors mean?" helpFile="Viewing+Your+Changes" >
        <jsp:attribute name="helpContent">

          <table class="carpetHelp">
            <tr><td class="criticalProblem firstCell">&nbsp;</td><td>Build failure <em>with</em> new problems</td></tr>
            <tr><td class="notCriticalProblem firstCell">&nbsp;</td><td>Build failure <em>without</em> new problems</td></tr>
            <tr><td class="successful firstCell">&nbsp;</td><td>There is a successful build with this change</td></tr>
            <tr><td class="pending firstCell">&nbsp;</td><td>No build has run with this change</td></tr>
          </table>

        </jsp:attribute>
      </bs:helpPopup>
    </div>

    <c:url var="vcsLink" value="/vcsSettings.html?init=1"></c:url>
    <div class="attentionComment" style="display: none;" id="no_regular_changes">
      We didn't find any VCS changes committed by you.
      Please make sure <a href="${vcsLink}">your VCS username settings</a> are correct and you have permissions to view relevant projects.
    </div>

    <div id="loadingProgress">
    </div>

    <div id="updatableChangesContainer"></div>
    <div id="moreChangesContainer"></div>

    <div id="showMoreLinkDiv" style="display: none;">
      <a title="Load more change records" href="#" onclick="BS.ChangePage.showMoreChanges(); return false" >More&nbsp;

      <forms:progressRing id="showMoreChangesProgress" style="display: none;"/></a>
    </div>

    <script type="text/javascript">
      (function() {
        <%@ include file="updateFilter.jspf"%>
        BS.UserBuildTypes = ${userBuildTypesJson};
        BS.UserBuildTypesMap = {};
        for(var i = 0; i < BS.UserBuildTypes.length; i ++) {
          BS.UserBuildTypesMap[BS.UserBuildTypes[i].id] = BS.UserBuildTypes[i];
        }
        BS.ChangePage.showFirstChanges();

      })();
    </script>
  </jsp:attribute>
</bs:page>
