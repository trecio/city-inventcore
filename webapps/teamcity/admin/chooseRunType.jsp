<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>

<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.CreateBuildTypeForm" scope="request"/>
<admin:createBuildTypePage>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/admin/runParams.css
    </bs:linkCSS>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <div id="container">
      <div class="editBuildPageGeneral">
        <form action="<c:url value='${buildForm.actionPath}?projectId=${buildForm.project.projectId}'/>" method="post" onsubmit="return BS.CreateBuildTypeForm.submitCreateBuildType()">

        <div id="runnerParams">
          <jsp:include page="/admin/runnerParams.html">
            <jsp:param name="projectId" value="${buildForm.project.projectId}"/>
            <jsp:param name="runnerType" value="${buildForm.buildRunnerBean.runnerType}"/>
            <jsp:param name="template" value="${buildForm.template}"/>
          </jsp:include>
        </div>


        <script type="text/javascript">
          $('runnerParams').updateContainer = function(runType) {
            BS.Util.show('chooseRunnerProgress');
            BS.MultilineProperties.clearProperties();
            <c:url value="/admin/runnerParams.html?projectId=${buildForm.project.projectId}&template=${buildForm.template}&runnerType=" var="showParamsUrl"/>
            BS.ajaxUpdater(this, '${showParamsUrl}' + runType, {
              evalScripts : true,
              onComplete: function() {
                BS.Util.hide('chooseRunnerProgress');
                BS.AvailableParams.attachPopups('projectId=${buildForm.project.projectId}', 'buildTypeParams', 'textProperty', 'multilineProperty');
              }
            });
          }
        </script>

        <div class="saveButtonsBlock">
          <forms:cancel cameFromSupport="${buildForm.cameFromSupport}"/>
          <input type="hidden" name="step" value="${param['step']}"/>
          <forms:submit name="submitButton" label="Save"/>
          <forms:submit type="button" name="" label="<< VCS settings" onclick="BS.CreateBuildTypeForm.back();"/>
          <forms:saving/>
        </div>

        </form>
      </div>
    </div>
  </jsp:attribute>
</admin:createBuildTypePage>
