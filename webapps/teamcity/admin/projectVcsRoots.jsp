<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="admfn" uri="/WEB-INF/functions/admin" %>
<jsp:useBean id="vcsRootsForm" type="jetbrains.buildServer.controllers.admin.projects.BaseVcsRootsBean" scope="request"/>
<c:set var="cameFromUrl" value="${param['cameFromUrl']}"/>
<c:set var="allRootsPage" value="${empty vcsRootsForm.ownerProjectId}"/>
<c:set var="numOfAccessibleRoots" value="${vcsRootsForm.numberOfAvailableRoots}"/>
<bs:linkScript>
  /js/bs/systemProblemsMonitor.js
</bs:linkScript>

<div id="existingVcsRoots">
  <c:if test="${allRootsPage}">
    <form action="${pageUrl}" method="get" id="vcsRootsFilterForm">
      <div class="actionBar">
        <span class="nowrap">
          <label class="firstLabel" for="keyword">Filter: </label>
          <forms:textField name="keyword" value="${vcsRootsForm.keyword}" size="20"/>
        </span>

        <input class="btn btn_mini" type="submit" name="submitFilter" value="Filter"/>
        <input type="hidden" name="item" value="vcsRoots"/>

        <c:if test="${not empty vcsRootsForm.keyword}">
          <forms:resetFilter resetHandler="$('vcsRootsFilterForm').keyword.value = ''; $('vcsRootsFilterForm').submit();"/>
        </c:if>

      </div>
      <div>
        <forms:checkbox name="showUnusedOnly" checked="${vcsRootsForm.showUnusedOnly}" onclick="$('vcsRootsFilterForm').submit();"/> <label for="showUnusedOnly">Show unused VCS roots only</label>
      </div>
    </form>
    <br/>
  </c:if>

  <div class="clearfix">
  <c:if test="${empty vcsRootsForm.visibleVcsRoots}">
    No VCS roots found.

    <c:if test="${not allRootsPage}">
    <p>
      <admin:createVcsRootLink editingScope="editProject:${vcsRootsForm.ownerProjectId}" cameFromUrl="${cameFromUrl}" cameFromTitle="Edit Project">Create VCS root</admin:createVcsRootLink>
    </p>
    </c:if>
  </c:if>

  <c:if test="${not empty vcsRootsForm.visibleVcsRoots}">
    <c:if test="${allRootsPage}">
      Found ${numOfAccessibleRoots}
      VCS root<bs:s val="${numOfAccessibleRoots}"/>.
    </c:if>

    <c:if test="${not allRootsPage}">
      Found ${numOfAccessibleRoots}
      VCS root<bs:s val="${numOfAccessibleRoots}"/> in this project.
    </c:if>

    <c:if test="${not allRootsPage}">
    <p style="float: right;">
      <admin:createVcsRootLink editingScope="editProject:${vcsRootsForm.ownerProjectId}" cameFromUrl="${cameFromUrl}" cameFromTitle="Edit Project">Create VCS root</admin:createVcsRootLink>
    </p>
    </c:if>

  </c:if>
  </div>

  <bs:messages key="vcsRootRemoved"/>
  <bs:messages key="vcsRootsUpdated"/>
  <bs:messages key="vcsRootDetached"/>
  <bs:messages key="vcsRootAttached"/>

  <script type="text/javascript">
    var BuildTypesToAttach = {};
  </script>

  <c:forEach items="${vcsRootsForm.visibleVcsRoots}" var="vcsRoot" varStatus="status">
    <c:set var="canBeAttachedTo" value="${vcsRootsForm.canBeAttachedTo[vcsRoot]}"/>

  <bs:refreshable containerId="vcsRootContainer_${vcsRoot.id}" pageUrl="${cameFromUrl}">
  <script type="text/javascript">
    <c:set var="canBeAttachedTo" value="${vcsRootsForm.canBeAttachedTo[vcsRoot]}"/>
    BuildTypesToAttach['${vcsRoot.id}'] = new Array();
    <c:forEach items="${canBeAttachedTo}" var="buildType">
    BuildTypesToAttach['${vcsRoot.id}'].push({ id: '${buildType.buildTypeId}', name: '<c:out value="${buildType.fullName}"/>'});
    </c:forEach>
  </script>

    <bs:messages key="vcsRootUpdateFailure"/>

  <c:set var="inheritedVcsRoots" value="${vcsRootsForm.inheritedVcsRoots}"/>
  <l:tableWithHighlighting className="settings projectVcsRoots" mouseovertitle="" id="vcsRoot_${vcsRoot.id}" highlightImmediately="true">
    <c:set var="canEditRoot" value="${afn:canEditVcsRoot(vcsRoot)}"/>
    <c:set var="canDeleteRoot" value="${afn:canEditVcsRoot(vcsRoot)}"/>
    <c:set var="templateUsages" value="${vcsRootsForm.templateUsages[vcsRoot]}"/>
    <c:set var="btUsages" value="${vcsRootsForm.buildTypeUsages[vcsRoot]}"/>
    <c:choose>
      <c:when test="${canEditRoot}">
        <c:choose>
          <c:when test="${not empty vcsRootsForm.ownerProjectId}"><c:set var="editingScope">editProject:${vcsRootsForm.ownerProjectId}</c:set></c:when>
          <c:when test="${vcsRoot.scope.global}"><c:set var="editingScope">none</c:set></c:when>
          <c:otherwise><c:set var="editingScope">editProject:${vcsRoot.scope.ownerProjectId}</c:set></c:otherwise>
        </c:choose>
        <c:set var="editVcsRootLink"><admin:editVcsRootLink editingScope="${editingScope}"
                                                            vcsRoot="${vcsRoot}"
                                                            withoutLink="true"
                                                            cameFromUrl="${cameFromUrl}"
                                                            cameFromTitle="${allRootsPage ? 'VCS Roots' : 'Edit Project'}"/></c:set>
        <c:set var="onclick">BS.openUrl(event, '${editVcsRootLink}');</c:set>
        <tr class="vcsRoot">
          <td class="highlight noRightBorder" onclick="${onclick}">
            <span id="vcsRootProgress_${vcsRoot.id}" style="float: right; display: none; margin: 0; padding: 0;"><forms:saving id="vcsRootProgress_${vcsRoot.id}:img" className="progressRingInline"/> Saving...</span>
            <strong><c:out value="${vcsRoot.name}"/></strong>
            <c:choose>
              <c:when test="${allRootsPage}">
                <c:if test="${vcsRoot.scope.global}">(shared among all of the projects)</c:if>
                <c:if test="${not vcsRoot.scope.global}">(belongs to <strong>${vcsRootsForm.projectNames[vcsRoot.scope.ownerProjectId]}</strong> project)</c:if>
              </c:when>
              <c:otherwise><c:if test="${vcsRoot.scope.global}">(shared among all of the projects)</c:if></c:otherwise>
            </c:choose>&nbsp;<i>${vcsRootsForm.status[vcsRoot]}</i>
            <div class="clr"></div>
          </td>
          <td class="edit highlight vcsRoot" onclick="${onclick}"><a href="${editVcsRootLink}" title="Click to edit this VCS root">edit</a></td>
          <c:choose>
            <c:when test="${canDeleteRoot}">
              <td class="edit"><a href="#" onclick="BS.AdminActions.deleteVcsRoot(${vcsRoot.id}); return false">delete</a></td>
            </c:when>
            <c:otherwise><td class="noPerms" title="You do not have enough permissions to delete this VCS root">cannot be deleted</td></c:otherwise>
          </c:choose>
        </tr>
      </c:when>
      <c:otherwise>
        <tr class="vcsRoot">
          <td><strong><c:out value="${vcsRoot.name}"/></strong>
            <c:choose>
              <c:when test="${allRootsPage}">
                <c:if test="${vcsRoot.scope.global}">(shared among all of the projects)</c:if>
                <c:if test="${not vcsRoot.scope.global}">(belongs to <strong>${vcsRootsForm.projectNames[vcsRoot.scope.ownerProjectId]}</strong> project)</c:if>
              </c:when>
              <c:otherwise><c:if test="${vcsRoot.scope.global}">(shared among all of the projects)</c:if></c:otherwise>
            </c:choose>&nbsp;<i>${vcsRootsForm.status[vcsRoot]}</i></td>
          <td colspan="2" class="noPerms"><span title="You do not have enough permissions to edit this VCS root">cannot be edited</span></td>
        </tr>
      </c:otherwise>
    </c:choose>
    <c:forEach items="${templateUsages}" var="btSettings" varStatus="pos">
    <c:set var="canEdit" value="${afn:permissionGrantedForProject(btSettings.parentProject, 'EDIT_PROJECT')}"/>
    <c:set var="editUrl"><admin:editTemplateLink step="vcsRoots" templateId="${btSettings.id}" withoutLink="true"/></c:set>
    <c:set var="onclick" value=""/>
    <c:set var="highlight" value=""/>
    <c:if test="${canEdit}">
      <c:set var="onclick">f = function(event){ if (Event.element(event).tagName != 'A') { document.location.href='${editUrl}'; } }; f.call(this, event)</c:set>
      <c:set var="highlight" value="highlight"/>
    </c:if>
    <c:set var="settingsId" value='${admfn:getTemplateSettingsId(btSettings)}'/>
    <tr>
      <td class="${highlight} buildConfiguration noRightBorder" onclick="${onclick}">
        <admin:templateUsageInfo template="${btSettings}"/>
        <c:choose>
          <c:when test="${allRootsPage or btSettings.parentProject.projectId != vcsRootsForm.ownerProjectId}"><c:out value="${btSettings.fullName}"/></c:when>
          <c:otherwise><c:out value="${btSettings.name}"/></c:otherwise>
        </c:choose>
      </td>
      <c:choose>
        <c:when test="${canEdit}">
          <td class="edit ${highlight}" onclick="${onclick}"><a href="${editUrl}" title="Click to edit template VCS settings">edit</a></td>
          <td class="edit noRightBorder" style="white-space: nowrap; "><a href="#" onclick="BS.AdminActions.detachVcsRoot(${vcsRoot.id}, '${settingsId}'); return false">detach</a></td>
        </c:when>
        <c:otherwise>
          <td colspan="2" class="noPerms"><span title="You do not have enough permissions to edit VCS settings of this template">cannot be edited</span></td>
        </c:otherwise>
      </c:choose>
    </tr>
    </c:forEach>
    <c:forEach items="${btUsages}" var="btSettings" varStatus="pos">
    <c:set var="canEdit" value="${afn:permissionGrantedForBuildType(btSettings, 'EDIT_PROJECT') and (not btSettings.templateBased or btSettings.templateAccessible)}"/>
    <c:set var="editUrl"><admin:editBuildTypeLink step="vcsRoots" buildTypeId="${btSettings.buildTypeId}" withoutLink="true"/></c:set>
    <c:set var="onclick" value=""/>
    <c:set var="highlight" value=""/>
    <c:if test="${canEdit}">
      <c:set var="onclick">f = function(event){ if (Event.element(event).tagName != 'A') { document.location.href='${editUrl}'; } }; f.call(this, event)</c:set>
      <c:set var="highlight" value="highlight"/>
    </c:if>
    <c:set var="settingsId" value='${admfn:getBuildTypeSettingsId(btSettings)}'/>
    <tr>
      <td class="${highlight} buildConfiguration noRightBorder" onclick="${onclick}">
        <c:choose>
          <c:when test="${allRootsPage or btSettings.projectId != vcsRootsForm.ownerProjectId}"><bs:buildTypeLinkFull buildType="${btSettings}"/></c:when>
          <c:otherwise><bs:buildTypeLink buildType="${btSettings}"/></c:otherwise>
        </c:choose>
        <bs:systemProblemMarker buildTypeId="${btSettings.buildTypeId}" problemType="<%=jetbrains.buildServer.serverSide.systemProblems.StandardSystemProblemTypes.VCS_CONFIGURATION%>"/>
      </td>
      <c:choose>
        <c:when test="${canEdit}">
          <td class="edit ${highlight}" onclick="${onclick}"><a href="${editUrl}" title="Click to edit build configuration VCS settings">edit</a></td>
          <td class="edit noRightBorder" style="white-space: nowrap; ">
            <c:set var="inheritedRoot" value="${inheritedVcsRoots[vcsRoot].inheritedStatus[btSettings]}"/>
            <c:choose>
              <c:when test="${inheritedRoot}">
                <c:set var="tpl" value="${btSettings.template}"/>
                <c:set var="tplName"><bs:escapeForJs text="${tpl.fullName}" forHTMLAttribute="true"/></c:set>
                <c:set var="tplSettingsId" value="${admfn:getTemplateSettingsId(tpl)}"/>
                <a href="#"
                   onclick="BS.AdminActions.detachVcsRoot(${vcsRoot.id}, '${tplSettingsId}', 'This VCS root is inherited from template ${tplName} and will be detached from the template and all inherited configurations.\nAre you sure you want to continue?'); return false">detach</a>
              </c:when>
              <c:otherwise>
                <a href="#" onclick="BS.AdminActions.detachVcsRoot(${vcsRoot.id}, '${settingsId}'); return false">detach</a>
              </c:otherwise>
            </c:choose>
          </td>
        </c:when>
        <c:otherwise>
          <td colspan="2" class="noPerms"><span title="You do not have enough permissions to edit this build configuration VCS settings">cannot be edited</span></td>
        </c:otherwise>
      </c:choose>
    </tr>
    </c:forEach>
  </l:tableWithHighlighting>
  <c:if test="${not empty canBeAttachedTo}">
    <c:set var="attachOnClick" value="BS.AttachBuildTypeForm.showDialog(${vcsRoot.id})"/>
    <p>
      <forms:addButton onclick="${attachOnClick}; return false">Attach to build configuration...</forms:addButton>
    </p>
  </c:if>

  <br/>

  </bs:refreshable>
  </c:forEach>

  <c:if test="${allRootsPage}">
    <bs:pager place="bottom" urlPattern="admin.html?item=vcsRoots&page=[page]&keyword=${vcsRootsForm.keyword}&showUnusedOnly=${vcsRootsForm.showUnusedOnly}" pager="${vcsRootsForm.pager}"/>
  </c:if>

</div>


<bs:modalDialog formId="attachBuildTypeForm"
              title="Choose Build Configuration"
              action="#"
              closeCommand="BS.AttachBuildTypeForm.close()"
              saveCommand="BS.AttachBuildTypeForm.submit()">

  <label for="chooseBuildTypeSelector" class="tableLabel">Build configuration:</label>
  <select name="buildTypeId" id="chooseBuildTypeSelector" style="width: 17em;"></select>

  <script type="text/javascript">
    $('chooseBuildTypeSelector').fill = function(vcsRootId) {
      while (this.options.length > 0) {
        this.options[0] = null;
      }

      var buildTypes = BuildTypesToAttach[vcsRootId];
      if (buildTypes != null) {
        for (var i=0; i<buildTypes.length; i++) {
          this.options[i] = new Option(buildTypes[i].name, buildTypes[i].id, false, false);
        }
      }
    }
  </script>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.AttachBuildTypeForm.close()"/>
    <forms:submit label="OK" />
    <forms:saving id="attachBuildTypeProgress"/>
  </div>

  <input type="hidden" name="vcsRootId" value=""/>
</bs:modalDialog>

<script type="text/javascript">
  BS.SystemProblems.startUpdates('<%=jetbrains.buildServer.serverSide.systemProblems.StandardSystemProblemTypes.VCS_CONFIGURATION%>');
</script>
