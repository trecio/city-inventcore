<%@ include file="/include-internal.jsp" %>

<jsp:useBean id="vcsRootStatusBean" type="jetbrains.buildServer.controllers.admin.DiagnosticVcsStatusBean" scope="request"/>
<c:set var="total" value="${vcsRootStatusBean.totalNumberOfInstances}"/>

<form action="${pageUrl}" method="get" style="margin-top: 0.5em;">
<table class="runnerFormTable" style="width: 100%;">
<tr class="groupingTitle">
  <td>Checking for changes status</td>
</tr>
<tr>
  <td>
    <div>
      Number of monitored VCS Roots (after parameters resolution): <strong><c:out value="${total}"/></strong>
    </div>
    <div>
      Waiting in the queue: <strong><c:out value="${vcsRootStatusBean.scheduledInstances}"/></strong>
    </div>
    <div>
      Started checking for changes: <strong><c:out value="${vcsRootStatusBean.checkingForChangesStartedInstances}"/></strong>
    </div>
    <br/>
  </td>
</tr>
<c:if test="${vcsRootStatusBean.totalNumberOfInstances > 0}">
<tr class="groupingTitle">
  <td>Checking for changes duration</td>
</tr>
<tr>
  <td>
      Checking for changes duration threshold: <forms:textField name="durationThresholdSecs" value="${vcsRootStatusBean.durationThresholdSecs}" style="width: 5em;"/> seconds <input type="submit" value="Find VCS roots"/>
      <br/>

      <c:set var="foundRoots" value="${fn:length(vcsRootStatusBean.slowInstances)}"/>

      Found <strong><c:out value="${foundRoots}"/></strong> VCS Root<bs:s val="${foundRoots}"/> with checking for changes duration &gt; <strong>${vcsRootStatusBean.durationThresholdSecs}</strong> seconds.
      <br/>

      <c:if test="${not empty vcsRootStatusBean.slowInstances}">
        <l:tableWithHighlighting className="settings runnerFormTable" style="width: 70%">
        <tr>
          <th class="name" style="width: 7em;">Parent Id - Id</th>
          <th class="name">VCS Root name</th>
          <th class="name" style="width: 10em;">Duration (seconds)</th>
        </tr>
        <c:forEach items="${vcsRootStatusBean.slowInstances}" var="vcsRootStat">
        <tr>
          <td class="highlight" style="vertical-align: top;"><c:out value="${vcsRootStat.rootInstance.parent.id}"/> - <c:out value="${vcsRootStat.rootInstance.id}"/></td>
          <td class="highlight" style="vertical-align: top;">
            <a href="#" style="float: right" onclick="$('parameters_${vcsRootStat.rootInstance.id}').toggle(); if (this.innerHTML.indexOf('show') != -1 ) { this.innerHTML = '&laquo; hide parameters' } else { this.innerHTML = 'show parameters &raquo;' }">show parameters &raquo;</a>
            <admin:editVcsRootLink vcsRoot="${vcsRootStat.rootInstance.parent}" editingScope="" cameFromUrl="${pageUrl}"><c:out value="${vcsRootStat.rootInstance.name}"/></admin:editVcsRootLink>
            <div id="parameters_${vcsRootStat.rootInstance.id}" style="display: none;">
              <c:forEach items="${vcsRootStat.rootInstanceParameters}" var="e">
                <c:out value="${e.key}"/>: <c:out value="${e.value}"/><br/>
              </c:forEach>
            </div>
          </td>
          <td class="highlight" style="vertical-align: top;">
            <fmt:formatNumber value="${vcsRootStat.durationMillis / 1000.0}" maxFractionDigits="2"/> <c:if test="${not vcsRootStat.finished}">(in progress)</c:if>
            <c:if test="${vcsRootStat.durationMillis / 1000.0 > vcsRootStat.rootInstance.modificationCheckInterval}">
              <span class="error">checking for changes interval (<strong>${vcsRootStat.rootInstance.modificationCheckInterval}</strong> seconds) exceeded</span>
            </c:if>
          </td>
        </tr>
        </c:forEach>
        </l:tableWithHighlighting>
      </c:if>

    <input type="hidden" name="item" value="diagnostics"/>
    <input type="hidden" name="tab" value="vcsStatus"/>
  </td>
</tr>

</c:if>
</table>

</form>
