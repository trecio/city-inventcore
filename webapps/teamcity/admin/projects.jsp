<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %>
<%@ taglib prefix="ext" tagdir="/WEB-INF/tags/ext" %>
<ext:showTabs placeId="<%=PlaceId.ADMIN_PROJECTS_TAB%>"
              urlPrefix="/admin/admin.html?item=projects"
              tabContainerId="tabsContainer4"/>
