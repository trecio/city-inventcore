<%@ page import="jetbrains.buildServer.serverSide.VersionChecker" %><%@
    page import="jetbrains.buildServer.web.util.WebUtil" %><%@
    include file="/include-internal.jsp" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin"

%><c:set var="pageTitle" value="Administration" scope="request"
/><c:set var="newVersion" value="<%=VersionChecker.getNewVersion()%>" scope="request"
/><jsp:useBean id="adminOverviewForm" type="jetbrains.buildServer.controllers.admin.AdminOverviewForm" scope="request"
/><jsp:useBean id="pageUrl" type="java.lang.String" scope="request"
/><c:set var="encodedCameFrom" value="<%=WebUtil.encode(pageUrl)%>"
/><c:set var="activeProjectsNum" value="${adminOverviewForm.totalNumberOfProjects - adminOverviewForm.numberOfArchivedProjects}"
/><c:set var="archivedProjectsNum" value="${adminOverviewForm.numberOfArchivedProjects}"/>

<div id="container">
  <c:set var="editableProjects" value="${adminOverviewForm.allProjects}"/>
  <c:if test="${archivedProjectsNum == 0}">
    <div class="descr">There are no archived projects.</div>
  </c:if>

  <c:if test="${archivedProjectsNum > 0}">
    <div class="descr">You have <strong>${archivedProjectsNum}</strong> archived project<bs:s val="${archivedProjectsNum}"/>.</div>
  </c:if>

  <bs:messages key="projectRemoved" style="text-align: left;"/>
  <bs:messages key="buildTypeRemoved" style="text-align: left;"/>

  <c:if test="${archivedProjectsNum > 0}">
    <admin:projectsFilter adminOverviewForm="${adminOverviewForm}" tab="archived"/>
    <c:forEach items="${adminOverviewForm.archivedProjects}" var="project">
      <%@ include file="project.jsp" %>
    </c:forEach>
    <bs:pager place="bottom" urlPattern="admin.html?tab=archived&page=[page]&keyword=${adminOverviewForm.keyword}"
              pager="${adminOverviewForm.pager}"/>
  </c:if>
</div>

<admin:copyBuildTypeForm editableProjects="${editableProjects}"/>
<admin:moveBuildTypeForm editableProjects="${editableProjects}"/>
<admin:copyProjectForm/>
