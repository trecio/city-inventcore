<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="profile" tagdir="/WEB-INF/tags/userProfile"%>
<jsp:useBean id="adminEditUserForm" type="jetbrains.buildServer.controllers.admin.users.AdminEditUserForm" scope="request"/>
<div id="profilePage">
<form action="<c:url value='/admin/editUser.html?userId=${adminEditUserForm.userId}'/>" onsubmit="return BS.AdminUpdateUserForm.submitUserProfile()" method="post" autocomplete="off">

  <authz:authorize allPermissions="DELETE_USER">
  <c:if test="${adminEditUserForm.canRemoveUserAccount}">
    <input class="btn btn_mini submitButton" type="button" value="Delete User Account" onclick="BS.AdminUpdateUserForm.deleteUserAccount()" style="margin-right: 10px"/>
    <div class="clr"></div>
    <br/>
  </c:if>
  </authz:authorize>

  <bs:messages key="userChanged"/>

  <l:t2x2>
    <jsp:attribute name="b1">
      <profile:general profileForm="${adminEditUserForm}"/>
      <c:if test="${not adminEditUserForm.perProjectPermissionsEnabled}">
        <div style="font-size: 0; height: 6px;"></div>
        <l:settingsBlock title="Administrator status">
          <admin:perProjectRolesNote/>
          <forms:checkbox name="administrator" checked="${adminEditUserForm.administrator}" disabled="${not adminEditUserForm.canEditPermissions or adminEditUserForm.administratorStatusInherited}"/>
          <label for="administrator">Give this user administrative privileges</label>
          <c:if test="${adminEditUserForm.administratorStatusInherited}">
            <div class="smallNote" style="margin-left: 0;">Administrative privileges are inherited from one or more parent groups</div>
          </c:if>
        </l:settingsBlock>
      </c:if>
    </jsp:attribute>
    <jsp:attribute name="b2">
      <profile:vcsPlugins profileForm="${adminEditUserForm}" adminMode="true"/>
    </jsp:attribute>
  </l:t2x2>

  <div class="center" style="width: 20em; padding-top:1em;">
    <forms:cancel cameFromSupport="${adminEditUserForm.cameFromSupport}"/>
    <forms:submit label="Save changes"/>
    <forms:saving id="saving1"/>
  </div>

  <input type="hidden" id="submitUpdateUser" name="submitUpdateUser" value="storeInSession"/>
  <input type="hidden" name="userId" value="${adminEditUserForm.userId}"/>
  <input type="hidden" name="tab" value="${currentTab}"/>

</form>

<forms:modified/>
<script type="text/javascript">
  BS.AdminUpdateUserForm.setupEventHandlers();
  BS.AdminUpdateUserForm.setModified(${adminEditUserForm.stateModified});
</script>
</div>