<%@ page import="jetbrains.buildServer.buildFailures.BuildFailureOnMetricFeature"
    %>
<%@ page import="jetbrains.buildServer.buildFailures.BuildFailureOnMetricCondition" %>
<%@ include file="/include.jsp"
    %><%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>

<jsp:useBean id="bean" type="jetbrains.buildServer.buildFailures.BuildFailureOnMetricBean" scope="request" />
<c:set var="metricThresholdKey" value="<%= BuildFailureOnMetricCondition.METRIC_THRESHOLD%>"/>
<c:set var="moreLessKey" value="<%= BuildFailureOnMetricCondition.MORE_OR_LESS%>"/>
<c:set var="metricKey" value="<%= BuildFailureOnMetricCondition.METRIC_KEY%>"/>
<c:set var="withBuildAnchorKey" value="<%= BuildFailureOnMetricCondition.WITH_ANCHOR_KEY%>"/>
<c:set var="anchorBuildKey" value="<%= BuildFailureOnMetricCondition.ANCHOR_BUILD_KEY%>"/>
<style>
  #anchorBuild option {
    text-transform: lowercase;
  }

  tr.editBuildFailureOnMetric th {
    vertical-align: top;
  }

  tr.editBuildFailureOnMetric select {
    height: 1.7em;
  }

  tr.editBuildFailureOnMetric div.topShift {
    margin-top: 0.75em;
  }

  tr.editBuildFailureOnMetric label.anchorLabel {
    display: inline-block;
  }

  tr.editBuildFailureOnMetric label.anchorLabel.anchorRight {
    text-align: right;
    padding: 2px 0;
  }

  tr.editBuildFailureOnMetric .buildNumberPattern span.error {
    padding-left: 8.3em;
  }
</style>

<tr class="editBuildFailureOnMetric">
    <th style="width: 20%;">Fail build if:</th>
    <td>
      <div>
        <!-- Metric selector:-->
        <props:selectProperty name="${metricKey}">
          <c:forEach items="${bean.metricKey2Description}" var="metric">
            <props:option value="${metric.key}"><c:out value="${metric.value}"/></props:option>
          </c:forEach>
        </props:selectProperty>
        &nbsp;
        <props:selectProperty name="${moreLessKey}" style="min-width: 4em;">
          <props:option value="more"> is more than </props:option>
          <props:option value="less"> is less than </props:option>
        </props:selectProperty>
        &nbsp;
        <props:textProperty name="${metricThresholdKey}" style="width: 4em;" expandable="${false}" className="disableBuildTypeParams"/>
      </div>

      <div>
        <span class="error" id="error_${metricKey}"></span>
        <span class="error" id="error_${metricThresholdKey}"></span>
      </div>

      <div class="topShift">
      <label class="anchorLabel">
      <props:checkboxProperty name="${withBuildAnchorKey}" onclick="BS.EditBuildFailure.enableAnchor(this.checked);"/>
       compared to &nbsp;</label>
      <props:selectProperty name="${anchorBuildKey}" onchange="BS.EditBuildFailure.updateFieldVisibility();">
        <forms:buildAnchorOptions selected=""/>
      </props:selectProperty>
      </div>

      <div id="buildNumberField" class="buildNumberPattern topShift" style="display:none">
        <label for="buildNumberPattern" class="anchorLabel anchorRight">#</label>
        <props:textProperty name="buildNumberPattern" size="12" maxlength="100"
                            style="width: 17em"/><bs:help file="Build+Number"/>
        <span class="error" id="error_buildNumberPattern"></span>
      </div>

      <div id="buildTagField" class="buildNumberPattern topShift"  style="display:none">
        <label for="buildTag" class="anchorLabel anchorRight">Tag:</label>
        <props:textProperty name="buildTag" size="12" maxlength="100"
                            style="width: 17em" /><bs:help file="Build+Tag"/>
        <span class="error" id="error_buildTag"></span>
      </div>

    </td>
</tr>
<script>
  // TODO move to external JS
  BS.EditBuildFailure = {
    enableAnchor: function(enable) {
      $('anchorBuild').disabled = !enable;
      $('buildTag').disabled = !enable;
      $('buildNumberPattern').disabled = !enable;
    },

    updateFieldVisibility: function() {
      var buildNumberSelected = $('${anchorBuildKey}').selectedIndex == 3;
      var buildTagSelected = $('${anchorBuildKey}').selectedIndex == 4;

      $('buildNumberField').hide();
      $('buildTagField').hide();
      if (buildNumberSelected) {
        $('buildNumberField').show();
      }
      if (buildTagSelected) {
        $('buildTagField').show();
      }

      BS.VisibilityHandlers.updateVisibility('featureParams');
    }
  };

  BS.EditBuildFailure.updateFieldVisibility();
  BS.EditBuildFailure.enableAnchor($('${withBuildAnchorKey}').checked);
  BS.jQueryDropdown('#${metricKey},#${anchorBuildKey}').ufd("changeOptions");

</script>
