<%@ page import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<c:set var="cameFrom" value='<%=WebUtil.encode(request.getAttribute("pageUrl") + "&init=1")%>'/>
<admin:editBuildTypePage selectedStep="runType">
  <jsp:attribute name="head_include">
    <bs:linkScript>
      /js/bs/queueLikeSorter.js
    </bs:linkScript>
    <style type="text/css">
      #buildStepsOrderDialog {
        width: 35em;
      }

      #buildRunners {
        max-height: 50em;
        overflow: auto;
        overflow-x: hidden;
      }

      .buildRunnerRow {
        background-color: #f1f4f7;
        width: 100%;
        margin: 0 0 3px;
        padding: 2px 0;
        border: 2px solid #e3e9ef;
        cursor: move;
      }

      .buildRunnerRow .name {
        padding: 2px 0 0 34px;
        overflow: hidden;
      }

      .draggable {
        background: url("../img/dragAndDrop/dots.gif") no-repeat #f1f4f7 4px 8px;
      }

      .inherited {
        background-color: #fff;
        cursor: auto;
      }

      .messagesHolder {
        padding-top: 0;
        margin-top: -0.5em;
      }
    </style>
  </jsp:attribute>
  <jsp:attribute name="body_include">

    <bs:refreshable containerId="buildStepsContainer" pageUrl="${pageUrl}">

    <h2 class="noBorder">Build Steps</h2>

    <c:set var="numRunners" value="${fn:length(buildForm.multipleRunnersBean.buildRunners)}"/>

    <bs:messages key="buildRunnerRemoved"/>
    <bs:messages key="buildRunnerEnabledDisabled"/>
    <bs:messages key="buildRunnerSettingsUpdated"/>
    <bs:messages key="buildRunnersOrderApplied"/>
    <bs:messages key="buildRunnerCopied"/>

    <c:if test="${numRunners == 0}">
      <p>There are no build steps defined.</p>
    </c:if>

    <c:set var="numInherited" value="0"/>
    <c:if test="${numRunners > 0}">

    <l:tableWithHighlighting highlightImmediately="true" className="parametersTable">
      <tr>
        <th style="width: 30%">Build Step</th>
        <th colspan="3">Description</th>
      </tr>
      <c:forEach items="${buildForm.multipleRunnersBean.buildRunners}" var="runner">
        <c:url value='/admin/editRunType.html?init=1&id=${buildForm.settingsId}&runnerId=${runner.id}&cameFromUrl=${cameFrom}' var="onclickUrl"/>
        <c:set var="onclick">onclick="BS.openUrl(event, '${onclickUrl}')"</c:set>
        <c:if test="${runner.inherited}"><c:set var="numInherited" value="${numInherited+1}"/></c:if>
        <tr>
          <td class="highlight stepName" ${onclick} style="vertical-align:top;">
            <admin:runnerInfo runner="${runner}"/>
          </td>
          <td class="highlight stepDescription" ${onclick}>
            <admin:runnerInfo runner="${runner}"/>
          </td>
          <c:if test="${not runner.inherited}">
          <td class="edit highlight" ${onclick}><a href="${onclickUrl}">edit</a></td>
          </c:if>
          <c:if test="${runner.inherited}">
          <td class="edit highlight" ${onclick}><a href="${onclickUrl}">view</a></td>
          </c:if>
          <td class="edit">
            <c:set var="runId" value="${fn:replace(runner.id, '.', '_')}"/>
            <bs:simplePopup controlId="runnerActions${runId}" linkOpensPopup="true"
                            popup_options="shift: {x: -150, y: 20}, className: 'quickLinksMenuPopup'">
              <jsp:attribute name="content">
                <div>
                  <ul class="menuList">
                    <l:li>
                      <a href="#" onclick="BS.CopyBuildStepForm.showDialog('${runner.id}'); return false">Copy build step</a>
                    </l:li>
                    <c:if test="${runner.enabled}">
                      <l:li>
                        <a href="#" onclick="BS.AdminActions.setSettingsEnabled('${buildForm.settingsId}', '${runner.id}', false, 'Build step'); return false">Disable build step</a>
                      </l:li>
                    </c:if>
                    <c:if test="${not runner.enabled}">
                      <l:li>
                        <a href="#" onclick="BS.AdminActions.setSettingsEnabled('${buildForm.settingsId}', '${runner.id}', true, 'Build step'); return false">Enable build step</a>
                      </l:li>
                    </c:if>
                    <c:if test="${not runner.inherited}">
                    <l:li>
                      <a href="#" onclick="BS.MultipleRunnersForm.deleteRunner('${buildForm.settingsId}', '${runner.id}'); return false">Delete build step</a>
                    </l:li>
                    </c:if>
                  </ul>
                </div>
              </jsp:attribute>
              <jsp:body>more</jsp:body>
            </bs:simplePopup>
          </td>
        </tr>
      </c:forEach>
    </l:tableWithHighlighting>

    </c:if>

    <div>
      <c:url value='/admin/editRunType.html?init=1&id=${buildForm.settingsId}&runnerId=__NEW_RUNNER__&cameFromUrl=${cameFrom}' var="addStepUrl"/>
      <forms:addButton href="${addStepUrl}">Add build step</forms:addButton>
      <c:if test="${numRunners > 1 and numRunners > numInherited}">
        <span style="margin-left: 2em;">
          <a href="#" onclick="BS.BuildStepsOrderDialog.showCentered(); BS.BuildStepsOrderDialog.fixPageScroll(); return false">Reorder build steps</a>
        </span>
      </c:if>
    </div>
    <br/>

    <%@ include file="editBuildFeaturesResources.jspf" %>
    <jsp:include page="/admin/editBuildFeatures.html?id=${buildForm.settingsId}&featurePlace=GENERAL"/>

    <c:url var="action" value="/admin/editBuildRunners.html"/>
    <bs:modalDialog formId="buildStepsOrder" title="Reorder Build Steps" action="${action}" closeCommand="BS.BuildStepsOrderDialog.close()" saveCommand="BS.BuildStepsOrderDialog.submitForm('${buildForm.settingsId}')">
      <div>Use drag and drop to change build steps order.</div>

      <div class="messagesHolder">
        <div id="savingData">Saving...</div>
        <div id="dataSaved">New build steps order applied.</div>
      </div>

      <div id="buildRunners" class="custom-scroll">
      <c:forEach items="${buildForm.multipleRunnersBean.buildRunners}" var="runner">
        <div class="buildRunnerRow ${runner.inherited ? 'inherited' : 'draggable'}" id="r_${runner.id}">
          <div class="name">
            <admin:runnerInfo runner="${runner}"/>
          </div>
        </div>
      </c:forEach>
      </div>

      <div class="popupSaveButtonsBlock">
        <forms:cancel onclick="BS.BuildStepsOrderDialog.close()"/>
        <forms:submit label="Apply"/>
        <forms:saving id="saveOrderProgress"/>
      </div>
    </bs:modalDialog>

    <script type="text/javascript">
      (function() {
        var draggableElems = [];
        <c:forEach items="${buildForm.multipleRunnersBean.buildRunners}" var="runner" varStatus="pos"><c:if test="${not runner.inherited}">draggableElems.push('r_${runner.id}');</c:if></c:forEach>
        Position.includeScrollOffsets = true;
        Sortable.create('buildRunners', {
          tag : "div",
          scroll: window,
          dropOnEmpty : true,
          elements : draggableElems,
          onUpdate: function(el) {}
        });
      })();
    </script>

    <admin:copyBuildStepDialog buildTypeForm="${buildForm}"/>

    </bs:refreshable>

  </jsp:attribute>
</admin:editBuildTypePage>
