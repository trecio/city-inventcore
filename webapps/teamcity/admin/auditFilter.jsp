<%@ include file="/include-internal.jsp"%>
<jsp:useBean id="auditLogData" scope="request" type="jetbrains.buildServer.controllers.audit.AuditLogData"/>
<form action="<c:url value="/admin/audit.html"/>" method="post" id="auditLogFilterForm">
  <div class="actionBar">
    <span class="farRight">
      <label for="actionsPerPage">Number of rows: </label>
      <select id="actionsPerPage" name="actionsPerPage" onchange="BS.AuditLogFilterForm.submit();">
        <admin:_actionsPerPageOption value="10" auditLogData="${auditLogData}"/>
        <admin:_actionsPerPageOption value="20" auditLogData="${auditLogData}"/>
        <admin:_actionsPerPageOption value="50" auditLogData="${auditLogData}"/>
        <admin:_actionsPerPageOption value="100" auditLogData="${auditLogData}"/>
        <admin:_actionsPerPageOption value="200" auditLogData="${auditLogData}"/>
        <admin:_actionsPerPageOption value="500" auditLogData="${auditLogData}"/>
      </select>
      <input type="hidden" name="reset" id="reset"/>
    </span>

    <span class="nowrap">
      <label class="firstLabel" for="actionTypeSet">Show:</label>
      <forms:select id="actionTypeSet" name="actionTypeSet" className="comboBox" enableFilter="true">
        <c:set var="groupStarted" value="${false}"/>
        <c:forEach items="${auditLogData.actionTypeSets}" var="actionTypeSetWrapper">
          <c:set var="object" value="${actionTypeSetWrapper.object}"/>
          <c:if test="${object.fake}">
            <c:if test="${groupStarted}"></optgroup></c:if>
            <optgroup label="${actionTypeSetWrapper.description}">
            <c:set var="groupStarted" value="${true}"/>
          </c:if>
          <c:if test="${!object.fake}">
            <option <c:if test="${auditLogData.selectedActionTypeSet.id == object.id}">selected="true"</c:if> value="${object.id}">
              <c:out value="${actionTypeSetWrapper.description}"/>
            </option>
          </c:if>
        </c:forEach>
        <c:if test="${groupStarted}"></optgroup></c:if>
      </forms:select>
    </span>

    <span class="nowrap">
      <label for="filterScopeId">in:</label>
      <forms:select id="filterScopeId" name="filterScopeId" className="comboBox" enableFilter="true" filterOptions="{maxWidth: 300}">
        <option <c:if test="${!auditLogData.filterScopeIdFilterApplied}">selected="true"</c:if> value="-1"><c:out value="All build configurations"/></option>
        <c:set var="groupStarted" value="${false}"/>
        <c:forEach items="${auditLogData.filterScopes}" var="filterScope">
          <c:if test="${filterScope.fake}">
            <c:if test="${groupStarted}"></optgroup></c:if>
            <optgroup label="${filterScope.name}">
            <c:set var="groupStarted" value="${true}"/>
          </c:if>
          <c:if test="${!filterScope.fake}">
            <c:set var="optionTitle"><c:out value="${filterScope.description}"/></c:set>
            <c:set var="optionFullName"><c:out value="${filterScope.fullName}"/></c:set>
            <option title="${optionTitle}" data-title="${optionFullName}" <c:if test="${auditLogData.filterScopeId == filterScope.id}">selected="true"</c:if> value="${filterScope.id}">
              <c:out value="${filterScope.name}"/>
            </option>
          </c:if>
        </c:forEach>
        <c:if test="${groupStarted}"></optgroup></c:if>
      </forms:select>
    </span>

    <span class="nowrap">
      <label for="userId">by:</label>
      <forms:select id="userId" name="userId" className="comboBox" enableFilter="true">
        <option <c:if test="${!auditLogData.userIdFilterApplied}">selected="true"</c:if> value="-1"><c:out value="All users"/></option>
        <c:forEach items="${auditLogData.users}" var="user">
          <option <c:if test="${auditLogData.selectedUserId == user.id}">selected="true"</c:if> value="${user.id}"><c:out value="${user.descriptiveName}"/></option>
        </c:forEach>
      </forms:select>
    </span>

    <input class="btn btn_mini" type="button" value="Filter" onclick="BS.AuditLogFilterForm.submit();"/>
    <c:if test="${auditLogData.filterApplied}">
      <forms:resetFilter resetHandler="BS.AuditLogFilterForm.clearFilter();"/>
    </c:if>
    <forms:saving id="auditLogFilterApplyingProgressIcon" className="progressRingInline"/>
  </div>

  <div id="auditPermalink">
    <admin:_auditLogPermalink data="${auditLogData}"/>
  </div>
</form>
