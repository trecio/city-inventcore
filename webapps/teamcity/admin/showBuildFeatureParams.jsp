<%@include file="/include-internal.jsp"%>
<jsp:useBean id="buildFeature" type="jetbrains.buildServer.serverSide.BuildFeature" scope="request"/>

<table class="runnerFormTable featureDetails">
  <c:if test="${not empty buildFeature.editParametersUrl}">
    <jsp:include page="${buildFeature.editParametersUrl}"/>
  </c:if>
</table>
<c:choose>
  <c:when test="${not empty buildFeatureBean and buildFeatureBean.inherited}">
    <script type="text/javascript">$('featureSaveButtonsBlock').hide(); BS.BuildFeatureDialog.setTemplateBased([{name: 'featureTypeSelector'}, {className: 'submitButton'}])</script>
  </c:when>
  <c:otherwise><script type="text/javascript">$('featureSaveButtonsBlock').show()</script></c:otherwise>
</c:choose>