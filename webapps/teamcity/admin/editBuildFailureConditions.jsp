<%@ page import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<c:set var="cameFrom" value='<%=WebUtil.encode(request.getAttribute("pageUrl") + "&init=1")%>'/>
<admin:editBuildTypePage selectedStep="buildFailureConditions">
  <jsp:attribute name="head_include">
    <style type="text/css">
      .failureConditions {
        margin:  1em 0;
      }

      .failureConditionItem {
        margin-bottom: .3em;
      }

      .failureConditionItem label {
        display: inline-block;
        vertical-align: baseline;
      }

      .failureConditionItem span.error {
        margin-left: 10em;
      }
    </style>
    <script>
      $j(document).ready(function() {
        BS.EditBuildTypeForm.setupEventHandlers();
        BS.EditBuildTypeForm.setModified(${buildForm.stateModified});
      });
    </script>
    <%@ include file="editBuildFeaturesResources.jspf" %>
  </jsp:attribute>
  <jsp:attribute name="body_include">

    <h2 class="noBorder">Build Failure Conditions</h2>

    <p class="sectionDescription">In this section you can specify when your build should fail.
      You can also force your build to fail using <bs:helpLink file="Build+Script+Interaction+with+TeamCity" anchor="ReportingBuildStatus">TeamCity service messages</bs:helpLink>.
    </p>

    <jsp:include page="/admin/editBuildFeatures.html?id=${buildForm.settingsId}&featurePlace=FAILURE_REASON&messagePrefix=Build+failure+condition"/>

    <form action="<c:url value='/admin/editBuild.html?id=${buildForm.settingsId}'/>" method="post"
          onsubmit="return BS.EditBuildTypeForm.submitBuildType()" id="editBuildTypeForm" class="clearfix">

      <input type="hidden" id="submitBuildType" name="submitBuildType" value="1"/>

      <h3>Fail build if:</h3>

      <div class="failureConditions">
      <div class="failureConditionItem">
          <forms:checkbox name="shouldFailOnExitCode" checked="${buildForm.shouldFailOnExitCode}"/>
          <label for="shouldFailOnExitCode">build process exit code is not zero</label>
      </div>

      <div class="failureConditionItem">
          <forms:checkbox name="shouldFailIfTestFailed" checked="${buildForm.shouldFailIfTestFailed}"/>
          <label for="shouldFailIfTestFailed">at least one test failed</label>
      </div>

      <div class="failureConditionItem">
          <forms:checkbox name="shouldFailBuildOnAnyErrorMessage" checked="${buildForm.shouldFailBuildOnAnyErrorMessage}"/>
          <label for="shouldFailBuildOnAnyErrorMessage">an error message is logged by build runner</label>
      </div>

      <div class="failureConditionItem">
          <forms:checkbox name="executionTimeoutEnabled" checked="${buildForm.executionTimeoutEnabled}"
                      onclick="$('executionTimeout').disabled = !this.checked; if (this.checked) $('executionTimeout').focus();"/>
          <label for="executionTimeoutEnabled"> it runs longer than </label>
          <forms:textField name="executionTimeout" style="width: 4em; font-size:90%;" maxlength="8" value="${buildForm.executionTimeout}"
                       disabled="${not buildForm.executionTimeoutEnabled}"/> minutes
          <br/>
          <span class="error" id="errorExecutionTimeout"></span>
          <c:if test="${buildForm.defaultExecutionTimeout > 0}"><span class="smallNote" style="margin-left: 2em;">Default execution timeout specified in the server settings: ${buildForm.defaultExecutionTimeout} minute<bs:s val="${buildForm.defaultExecutionTimeout}"/></span></c:if>
      </div>

      <div class="failureConditionItem">
          <forms:checkbox name="shouldFailBuildOnOOMEOrCrash" checked="${buildForm.shouldFailBuildOnOOMEOrCrash}"/>
          <label for="shouldFailBuildOnOOMEOrCrash">an out-of-memory or crash is detected (Java only)</label>
      </div>
      </div>

      <div class="saveButtonsBlock">
        <forms:submit label="Save"/>
        <input type="hidden" value="${buildForm.numberOfSettingsChangesEvents}" name="numberOfSettingsChangesEvents"/>
      </div>
    </form>

    <admin:copyBuildStepDialog buildTypeForm="${buildForm}"/>

    <forms:modified/>
    <script>
    <c:if test="${buildForm.templateBased}">
      BS.EditBuildRunnerForm.setTemplateBased();
    </c:if>
    </script>
  </jsp:attribute>
</admin:editBuildTypePage>
