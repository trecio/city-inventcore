<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="projectForm" type="jetbrains.buildServer.controllers.admin.projects.CreateProjectForm" scope="request"/>
<c:set var="pageTitle" value="Create New Project" scope="request"/>
<bs:page>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/admin/projectForm.css
      /css/admin/adminMain.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/bs/editProject.js
      /js/bs/testConnection.js
    </bs:linkScript>
    <script type="text/javascript">
      BS.Navigation.items = [
        {title: "Administration", url: '<c:url value="/admin/admin.html"/>'},
        {title: "${pageTitle}", selected:true}
      ];

      $j(document).ready(function() {
        document.forms[0].name.focus();
      });
    </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <div id="container" class="clearfix">
      <div class="editProjectPage">
        <form action="<c:url value='/admin/createProject.html'/>" onsubmit="return BS.CreateProjectForm.submitCreateProject()" method="post">
          <admin:projectForm projectForm="${projectForm}"/>
          <div class="saveButtonsBlock">
            <c:url value="/admin/admin.html" var="adminUrl"/>
            <forms:cancel href="${adminUrl}"/>
            <forms:submit id="createProject" name="submitCreateProject" label="Create"/>
            <forms:saving/>
          </div>
        </form>
      </div>
      <div id="sidebarAdmin">
        <div class="disabledConfigurationSection">
          <h2>Further steps</h2>
          <p>You need to create a project first and then proceed to configure its VCS settings and build configurations.</p>
        </div>
      </div>
    </div>
  </jsp:attribute>
</bs:page>
