<%--@elvariable id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary"--%>
<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.CreateBuildTypeForm" scope="request"/>

<admin:createBuildTypePage>
  <jsp:attribute name="head_include">
    <script type="text/javascript">
      $j(document).ready(function() {
      if (document.forms.length > 0 && document.forms[0].name) {
        document.forms[0].name.focus();
      }
    });
    </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
  <div id="container" class="clearfix">
    <div class="editBuildPageGeneral">

      <c:set var="content">
        <form action="<c:url value='${buildForm.actionPath}?projectId=${buildForm.project.projectId}'/>" method="post" onsubmit="return BS.CreateBuildTypeForm.forward()">

        <admin:buildTypeForm buildForm="${buildForm}" title="${title}"/>

        <div class="saveButtonsBlock">
          <forms:cancel cameFromSupport="${buildForm.cameFromSupport}"/>
          <input type="hidden" name="step" value="0"/>
          <forms:submit name="submitButton" label="VCS settings >>"/>
          <forms:saving/>
        </div>

        </form>
      </c:set>

      <c:choose>
        <c:when test="${buildForm.template}">
          ${content}
        </c:when>
        <c:otherwise>
          <bs:professionalLimited shouldBePositive="${serverSummary.buildConfigurationsLeft}">
            <jsp:attribute name="disabled">
              <div class="attentionComment">
                You cannot create another build configuration, because TeamCity Professional only allows maximum of
                <strong>${serverSummary.licensingPolicy.maxNumberOfBuildTypes}</strong> build configurations.<bs:help file="TeamCity+Editions"/>
              </div>
            </jsp:attribute>

            <jsp:body>
              ${content}
            </jsp:body>
          </bs:professionalLimited>
        </c:otherwise>
      </c:choose>

    </div>
  </div>
  </jsp:attribute>
</admin:createBuildTypePage>
