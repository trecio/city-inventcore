<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags"

%>

<c:if test="${internalDbWarning}">
  <div class="attentionComment">
    The server currently uses the internal storage engine.
    For production purposes it is highly recommended to use a standalone database which provides better stability as the storage engine.
    <bs:help file="Setting up an External Database"/>
  </div>
</c:if>

<c:if test="${sybaseDbWarning}">
  <div class="attentionComment">
    The server currently uses Sybase database.
    The future versions of TeamCity will not support Sybase,
    so it's recommended to migrate to another external database
    (otherwise, upgrade to the next version will not be possible).
    <bs:help file="Migrating to an External Database"/>
  </div>
</c:if>

<c:if test="${not empty newVersion}">
  <div class="attentionComment">
    New version of TeamCity has been released.
    <a target="_blank" title="Download TeamCity ${newVersion}"
       href="http://www.jetbrains.com/teamcity/download/index.html"><strong>Download</strong></a>
  </div>
</c:if>
