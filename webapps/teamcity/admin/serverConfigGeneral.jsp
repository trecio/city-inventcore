<%@ page import="jetbrains.buildServer.serverSide.db.DatabaseType" %>
<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %>
<%@ page import="jetbrains.buildServer.web.util.WebUtil" %>

<%@ include file="/include-internal.jsp" %>

<jsp:useBean id="pageUrl" type="java.lang.String" scope="request"/>
<c:set var="escapedPageUrl" value="<%=WebUtil.encode(pageUrl)%>"/>
<jsp:useBean id="loginConfig" scope="request" type="jetbrains.buildServer.serverSide.impl.ServerSettings"/>
<jsp:useBean id="serverConfigForm" scope="request" type="jetbrains.buildServer.controllers.admin.ServerConfigForm"/>
<c:set var="internalDbWarn"><% if (DatabaseType.HSQLDB.humanReadableName.equals(serverConfigForm.getDbTypeName())) { %><div class="smallNote unscalableDbWarning">
    For production purposes it is recommended to use a standalone database which provides better stability, please refer to <a href="<bs:helpUrlPrefix/>/Migrating+to+an+External+Database" target="_blank">Migration instructions</a>.
  </div><% } %></c:set>

<div>
  <div class="serverConfigPage">

    <bs:refreshable pageUrl="${pageUrl}" containerId="serverSettings">

    <bs:messages key="serverSettingsSaved"/>

    <form action="<c:url value='/admin/serverConfigGeneral.html'/>" method="post" onsubmit="return BS.ServerConfigForm.submitSettings()">

      <table class="runnerFormTable">

        <tr class="groupingTitle">
          <td colspan="2">TeamCity Configuration</td>
        </tr>

        <tr>
          <th>Database: </th>
          <td>
            <c:out value="${serverConfigForm.dbTypeName}"/> <c:if test="${fn:length(serverConfigForm.dbUserName) > 0}">(user: <c:out value="${serverConfigForm.dbUserName}"/>)</c:if>
            ${internalDbWarn}
          </td>
        </tr>

        <tr>
          <th>Data directory: <bs:help file="TeamCity+Data+Directory"/></th>
          <td><c:out value="${serverConfigForm.dataDirectory}"/></td>
        </tr>
        <tr>
          <th>Artifacts directory: </th>
          <td><c:out value="${serverTC.artifactsDirectory.canonicalPath}"/></td>
        </tr>
        <tr>
          <th>Caches directory: </th>
          <td>
            <c:out value="${serverConfigForm.cacheDirectory}"/>
          </td>
        </tr>
        <tr>
          <th><label for="rootUrl">Server URL:</label> <bs:help file="Configuring+Server+URL"/></th>
          <td>
            <forms:textField name="rootUrl" style="width: 370px;" value="${serverConfigForm.rootUrl}"/>
            <span class="error" id="invalidRootUrl"></span>
          </td>
        </tr>

        <tr class="groupingTitle">
          <td colspan="2">Builds Settings</td>
        </tr>
        <tr>
          <th><label for="maxArtifactSize">Maximum build artifact file size:</label></th>
          <td>
            <forms:textField name="maxArtifactSize" value="${serverConfigForm.maxArtifactSize}" size="10" maxlength="20"/>
            <span class="error" id="invalidMaxArtifactSize"></span>
            <div class="smallNote" style="margin-left: 0;">in bytes. <it>Kb, Mb, Gb or Tb</it> suffixes are allowed, -1 indicates no limit</div>
          </td>
        </tr>
        <tr>
          <th><label for="defaultExecutionTimeout">Default build execution timeout:</label></th>
          <td>
            <forms:textField name="defaultExecutionTimeout" value="${serverConfigForm.defaultExecutionTimeout}" size="6" maxlength="8"/> minutes
            <span class="error" id="invalidDefaultExecutionTimeout"></span>
            <div class="smallNote" style="margin-left: 0;">0 indicates no execution timeout</div>
          </td>
        </tr>

        <tr class="groupingTitle">
          <td colspan="2">Version Control Settings</td>
        </tr>
        <tr>
          <th><label for="defaultModificationCheckInterval">Default VCS changes check interval:</label></th>
          <td><forms:textField name="defaultModificationCheckInterval" size="6" maxlength="8" value="${serverConfigForm.defaultModificationCheckInterval}"/> seconds
            <span class="error" id="invalidDefaultModificationCheckInterval"></span></td>
        </tr>
        <tr>
          <th><label for="defaultQuietPeriod">Default VCS trigger quiet period:</label></th>
          <td><forms:textField name="defaultQuietPeriod" size="6" maxlength="8" value="${serverConfigForm.defaultQuietPeriod}"/> seconds
            <span class="error" id="invalidDefaultQuietPeriod"></span></td>
        </tr>

        <tr class="groupingTitle">
          <td colspan="2">Permissions and authentication</td>
        </tr>

        <tr>
          <th>
            <label>Per-project permissions:</label>
          </th>
          <td><forms:checkbox name="perProjectPermissions" checked="${serverConfigForm.perProjectPermissions}"/> <label for="perProjectPermissions">Enable per-project permissions</label></td>
        </tr>
        <tr>
          <th>
            <label>Current scheme: <bs:help file="Configuring+Authentication+Settings"/></label>
          </th>
          <td>${serverConfigForm.currentLoginModuleDescriptor.displayName}</td>
        </tr>
      </table>
      <div id="userRegistration" style="display: ${loginConfig.defaultLoginConfigured ? 'block' : 'none'};">
        <table class="runnerFormTable">
        <tr>
          <th>Registration on the login page:</th>
          <td>
            <forms:checkbox name="userRegistrationAllowed" checked="${serverConfigForm.userRegistrationAllowed}"/>
            <label for="userRegistrationAllowed" class="rightLabel">Allow user registration from the login page</label>
            <c:if test="${serverConfigForm.perProjectPermissions}">
            <authz:authorize anyPermission="CHANGE_USER_ROLES_IN_PROJECT">
            <span class="smallNote">You can
              <a href="<c:url value='/admin/editGroup.html?groupCode=ALL_USERS_GROUP&tab=groupRoles'/>">configure default roles</a>
              that will be associated with newly registered users.
            </span>
            </authz:authorize>
            </c:if>
          </td>
        </tr>
        </table>
        </div>
        <table class="runnerFormTable">
        <tr>
          <th><label for="textForLoginPage">Welcome text on the login page:</label></th>
          <td><forms:textField name="textForLoginPage" style="width: 370px;" value="${serverConfigForm.textForLoginPage}" expandable="true"/></td>
        </tr>

        <tr class="groupingTitle">
          <td colspan="2">Guest User</td>
        </tr>

        <tr>
          <th>Guest user login:</th>
          <td><forms:checkbox name="guestLoginAllowed" checked="${serverConfigForm.guestLoginAllowed}"/>
            <label for="guestLoginAllowed">Allow to login as a guest user</label>

            <span class="smallNote">Guest users can log into TeamCity without authorization.
            <c:if test="${serverConfigForm.perProjectPermissions}">
            <authz:authorize anyPermission="CHANGE_USER, CHANGE_USER_ROLES_IN_PROJECT">
            <a href="<c:url value='/admin/editUser.html?init=1&userId=${serverConfigForm.guestUser.id}&cameFromTitle=${title}&cameFromUrl=${escapedPageUrl}'/>">Configure guest user roles</a>
            to set permissions for these users.
            </authz:authorize>
            </c:if>
            </span>
          </td>
        </tr>

        <tr>
          <th class="noBorder"><label for="guestUsername">Guest user username:</label></th>
          <td class="noBorder"><forms:textField name="guestUsername" value="${serverConfigForm.guestUsername}"/>
          <span class="error" id="invalidGuestUsername"></span></td>
        </tr>

      </table>

        <div class="saveButtonsBlock">
          <input type="hidden" id="submitSettings" name="submitSettings" value="store"/>
          <forms:submit label="Save"/>
          <forms:saving/>
        </div>

    </form>

    <forms:modified/>

    <script type="text/javascript">
      BS.ServerConfigForm.setupEventHandlers();
      BS.ServerConfigForm.setModified(${serverConfigForm.stateModified});
      BS.VisibilityHandlers.updateVisibility('container');
    </script>

    </bs:refreshable>

    <div class="clr"></div>

    <ext:includeExtensions placeId="<%=PlaceId.ADMIN_SERVER_CONFIGURATION%>"/>
  </div>
</div>
