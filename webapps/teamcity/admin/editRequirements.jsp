<%@ page import="jetbrains.buildServer.controllers.buildType.ParameterInfo" %>
<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<c:set var="requirementsBean" value="${buildForm.requirementsBean}"/>
<admin:editBuildTypePage selectedStep="requirements">
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/compatibilityList.css
      /css/admin/requirements.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/bs/blocks.js
      /js/bs/blocksWithHeader.js
      /js/jquery/jquery.ui.position.js
    </bs:linkScript>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <h2 class="noBorder">Agent Requirements <bs:help file="Agent+Requirements"/></h2>
    <p class="sectionDescription">This page lists all requirements that build agents should meet to run your builds.</p>

    <bs:messages key="requirementsUpdated"/>
    <bs:messages key="requirementRemoved"/>
    <admin:requirementsList requirements="${requirementsBean.requirements}" requirementsBean="${requirementsBean}" editable="true"/>

    <table class="requirementsActions clearfix">
      <tr>
        <td>
          <admin:newRequirement linkText="Add new requirement"/>
        </td>
        <td class="moreActions">
          or pick from <a href="#" onclick="BS.FrequentlyUsedReqsDialog.showDialog(this); return false">frequently used requirements</a>
        </td>
      </tr>
    </table>

    <c:set var="runnerRequirements" value="${requirementsBean.runTypeRequirements}"/>
      <c:if test="${not empty runnerRequirements and fn:length(runnerRequirements) > 0}">
      <br/>

      <p class="blockHeader" id="idrunner_requirements">
        <strong>Build runners requirements (${fn:length(runnerRequirements)})</strong>
      </p>

      <div class="predefinedBlock" id="runner_requirements">
        <p>Build runners impose additional agent requirements:</p>
        <admin:requirementsList requirementsBean="${requirementsBean}" requirements="${runnerRequirements}" editable="false"/>
      </div>

      <script type="text/javascript">
        <l:blockState blocksType="Block_idrunner_requirements"/>
        new BS.BlocksWithHeader('idrunner_requirements');
      </script>
    </c:if>

    <c:set var="undefinedParameters" value="${requirementsBean.undefinedParameters}"/>
    <c:if test="${not empty undefinedParameters}">
      <br/>

      <p class="blockHeader" id="idimplicit_requirements">
        <strong>Implicit requirements (${fn:length(undefinedParameters)})</strong>
      </p>

      <div class="predefinedBlock" id="implicit_requirements">
        <p>This section lists parameters used in configuration settings without actual values provided:</p>
        <admin:editBuildTypeNavSteps settings="${buildForm.settings}"/>
        <l:tableWithHighlighting className="parametersTable">
          <tr style="background-color: #f5f5f5;">
            <th>Parameter Name</th>
            <th colspan="2">Parameter Source</th>
          </tr>
          <c:forEach items="${undefinedParameters}" var="e">
          <tr>
            <td class="name"><c:out value="${e.key}"/></td>
            <td class="value">
              <c:set var="settingDescr" value="${e.value}"/>
              <c:choose>
                <c:when test="${settingDescr.type.name == 'ARTIFACTS' or settingDescr.type.name == 'BUILD_TYPE_OPTIONS'}">
                  <a href="<c:url value='${buildConfigSteps[0].url}'/>"><c:out value="${settingDescr.description}"/></a>
                </c:when>
                <c:when test="${settingDescr.type.name == 'VCS_ROOT'}">
                  <c:set var="vcsRoot" value="${settingDescr.additionalData}"/>
                  <admin:editVcsRootLink vcsRoot="${settingDescr.additionalData}" editingScope="none" cameFromUrl="${pageUrl}"><c:out value="${settingDescr.description}"/></admin:editVcsRootLink>
                </c:when>
                <c:when test="${settingDescr.type.name == 'CHECKOUT_RULES'}">
                  <a href="<c:url value='${buildConfigSteps[1].url}'/>"><c:out value="${settingDescr.description}"/></a>
                </c:when>
                <c:when test="${settingDescr.type.name == 'CHECKOUT_DIR'}">
                  <a href="<c:url value='${buildConfigSteps[1].url}'/>"><c:out value="${settingDescr.description}"/></a>
                </c:when>
                <c:when test="${settingDescr.type.name == 'LABEL_PATTERN'}">
                  <a href="<c:url value='${buildConfigSteps[1].url}'/>"><c:out value="${settingDescr.description}"/>a>
                </c:when>
                <c:when test="${settingDescr.type.name == 'BUILD_FEATURE'}">
                  <a href="<c:url value='${buildConfigSteps[2].url}'/>"><c:out value="${settingDescr.description}"/></a>
                </c:when>
                <c:when test="${settingDescr.type.name == 'BUILD_STEP'}">
                  <c:set var="stepName" value="${settingDescr.additionalData.name}"/>
                  <c:set var="stepType" value="${settingDescr.additionalData.runType.displayName}"/>
                  <c:url value='/admin/editRunType.html?init=1&id=${buildForm.settingsId}&runnerId=${settingDescr.additionalData.id}&cameFromUrl=${pageUrl}' var="stepUrl"/>
                  <a href="${stepUrl}"><c:out value="${settingDescr.description}"/></a>
                </c:when>
                <c:when test="${settingDescr.type.name == 'BUILD_FAILURE_CONDITION'}">
                  <a href="<c:url value='${buildConfigSteps[3].url}'/>"><c:out value="${settingDescr.description}"/></a>
                </c:when>
                <c:when test="${settingDescr.type.name == 'ARTIFACT_DEPENDENCY'}">
                  <a href="<c:url value='${buildConfigSteps[5].url}'/>"><c:out value="${settingDescr.description}"/></a>
                </c:when>
                <c:when test="${settingDescr.type.name == 'PARAMETER'}">
                  <a href="<c:url value='${buildConfigSteps[6].url}'/>#${settingDescr.additionalData}"><c:out value="${settingDescr.description}"/></a>
                </c:when>
                <c:otherwise><c:out value="${settingDescr.description}"/></c:otherwise>
              </c:choose>
            </td>
            <c:set var="paramName" value="${e.key}"/>
            <c:set var="paramId" value='<%=ParameterInfo.makeParameterId((String)jspContext.getAttribute("paramName"))%>'/>
            <td class="edit"><a href="<c:url value='${buildConfigSteps[6].url}'/>#edit_${paramId}">define</a></td>
          </tr>
          </c:forEach>
        </l:tableWithHighlighting>
      </div>

      <script type="text/javascript">
        <l:blockState blocksType="Block_idimplicit_requirements"/>
        new BS.BlocksWithHeader('idimplicit_requirements');
      </script>
    </c:if>

    <br/>
    <br/>

    <h2 class="noBorder">Agents compatibility</h2>

    <p class="sectionDescription">In this section you can see which agents are compatible with requirements and which are not.</p>

    <jsp:useBean id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary" scope="request"/>
    <c:set var="numberOfAgents" value="${serverSummary.authorizedAgentsCount}"/>
    <p>There <bs:are_is val="${numberOfAgents}"/> ${numberOfAgents} agent<bs:s val="${numberOfAgents}"/> authorized.</p>
    <bs:buildTypeCompatibility compatibleAgents="${buildForm.settings}" project="${buildForm.project}"/>

    <!--here go two popup dialogs-->
    <c:url value="/admin/editRequirements.html?id=${buildForm.settingsId}" var="editRequirementAction"/>
    <bs:modalDialog formId="editRequirement"
                    title="Edit Requirement"
                    action="${editRequirementAction}"
                    closeCommand="BS.EditRequirementDialog.cancelDialog()"
                    saveCommand="BS.RequirementsForm.saveRequirement()">

      <c:url var="autocompletionUrl" value="/agentParametersAutocompletion.html"/>

      <label class="editRequirementLabel" for="parameterName">Parameter Name:&nbsp;<l:star/></label>
      <forms:autocompletionTextField name="parameterName" value="" style="width:25em;" maxlength="1024" id="parameterName"
                                     autocompletionSource="BS.EditRequirementDialog.createFindNameFunction('${autocompletionUrl}')"
                                     autocompletionSearchAction="BS.EditRequirementDialog.showLoadingImage('#parameterName')"/>
      <span class="error" id="error_parameterName" style="margin-left: 10.5em;"></span>
      <div class="clr"></div>

      <div class="smallNote" id="inheritedParamName" style="display: none;">Name and type of the inherited parameter cannot be changed.</div>
      <div class="clr spacing"></div>

      <label class="editRequirementLabel" for="requirementType">Condition:</label>
      <forms:select id="requirementType" name="requirementType" onchange="BS.EditRequirementDialog.requirementChanged(this)" style="width: 15em;" enableFilter="true">
        <c:forEach items="${requirementsBean.availableRequirementTypes}" var="reqType">
          <forms:option value="${reqType.name}"><c:out value="${reqType.displayName}"/></forms:option>
        </c:forEach>
      </forms:select>
      <div class="clr spacing"></div>

      <div style="margin-bottom: 0.5em;" id="editParameterValue">
        <label class="editRequirementLabel" for="parameterValue">Value:</label>
        <forms:autocompletionTextField name="parameterValue" value="" style="width:25em;" maxlength="1024" id="parameterValue"
                                       autocompletionSource="BS.EditRequirementDialog.createFindValueFunction('${autocompletionUrl}')"
                                       autocompletionSearchAction="BS.EditRequirementDialog.showLoadingImage('#parameterValue')"
                                       autocompletionShowOnFocus="true"
                                       autocompletionShowEmpty="false"/>
        <div class="error" id="error_parameterValue" style="margin-left: 10.5em;"></div>
      </div>

      <div class="popupSaveButtonsBlock">
        <forms:cancel onclick="BS.EditRequirementDialog.cancelDialog()"/>
        <forms:submit label="Save"/>
        <forms:saving/>
      </div>

      <input type="hidden" id="currentName" name="currentName" value=""/>
      <input type="hidden" id="submitAction" name="submitAction" value=""/>
      <input type="hidden" id="submitBuildType" name="submitBuildType" value="1"/>
    </bs:modalDialog>

    <script type="text/javascript">
      BS.EditRequirementDialog.setParameterValueRequired({
      <c:forEach items="${requirementsBean.availableRequirementTypes}" var="reqType" varStatus="pos">'${reqType.name}': ${reqType.parameterRequired}${pos.last ? '' : ', '}</c:forEach>
      });
      (function($) {
        $(document).ready(function() {
          $('#requirementType').bind('keyup', function() {
            BS.EditRequirementDialog.requirementChanged($(this));
          });
        });
      }(jQuery));
    </script>

    <c:url value="/admin/editRequirements.html?id=${buildForm.settingsId}" var="frequentReqsAction"/>
    <bs:modalDialog formId="frequentlyUsedReqs"
                    title="Frequently Used Requirements"
                    action="${frequentReqsAction}"
                    closeCommand="BS.FrequentlyUsedReqsDialog.cancelDialog()"
                    saveCommand="false">

      <table class="frequentlyUsedReqsTable">
        <tr>
          <td class="requirement">Agent operating system is Windows</td>
          <td class="progress"><forms:saving id="progress1"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('system.os.name', 'contains', 'Windows', 'progress1')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">Agent operating system is Mac OS X</td>
          <td class="progress"><forms:saving id="progress2"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('system.os.name', 'contains', 'Mac OS X', 'progress2')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">Agent operating system is Linux</td>
          <td class="progress"><forms:saving id="progress3"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('system.os.name', 'contains', 'Linux', 'progress3')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">.NET Framework 1.1 is installed on agent</td>
          <td class="progress"><forms:saving id="progress4"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('DotNetFramework1.1_x86', 'exists', '', 'progress4')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">.NET Framework 2.0 is installed on agent</td>
          <td class="progress"><forms:saving id="progress5"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('DotNetFramework2.0_x86', 'exists', '', 'progress5')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">.NET Framework 3.5 is installed on agent</td>
          <td class="progress"><forms:saving id="progress6"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('DotNetFramework3.5_x86', 'exists', '', 'progress6')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">.NET Framework 4.0 is installed on agent</td>
          <td class="progress"><forms:saving id="progress7"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('DotNetFramework4.0_x86', 'exists', '', 'progress7')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">.NET Framework 4.5 is installed on agent</td>
          <td class="progress"><forms:saving id="progress_net_4_5"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('DotNetFramework4.5_x86', 'exists', '', 'progress_net_4_5')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">Visual Studio 2003 is installed on agent</td>
          <td class="progress"><forms:saving id="progress8"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('VS2003', 'exists', '', 'progress8')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">Visual Studio 2005 is installed on agent</td>
          <td class="progress"><forms:saving id="progress9"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('VS2005', 'exists', '', 'progress9')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">Visual Studio 2008 is installed on agent</td>
          <td class="progress"><forms:saving id="progressA"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('VS2008', 'exists', '', 'progressA')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">Visual Studio 2010 is installed on agent</td>
          <td class="progress"><forms:saving id="progressB"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('VS2010', 'exists', '', 'progressB')"/>
          </td>
        </tr>
        <tr>
          <td class="requirement">Visual Studio 2012 is installed on agent</td>
          <td class="progress"><forms:saving id="progressVS2012"/></td>
          <td>
            <input type="button" class="btn btn_mini" value="Add" onclick="BS.FrequentlyUsedReqsDialog.addRequirement('VS2012', 'exists', '', 'progressVS2012')"/>
          </td>
        </tr>
      </table>

      <input type="hidden" name="parameterName" value=""/>
      <input type="hidden" name="parameterValue" value=""/>
      <input type="hidden" name="requirementType" value=""/>
      <input type="hidden" name="submitAction" value="updateRequirement"/>
      <input type="hidden" name="submitBuildType" value="1"/>
    </bs:modalDialog>

  </jsp:attribute>
</admin:editBuildTypePage>
