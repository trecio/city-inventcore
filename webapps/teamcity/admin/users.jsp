<%@ page import="jetbrains.buildServer.controllers.profile.AuthorityRolesBean" %><%@
    page import="jetbrains.buildServer.serverSide.auth.RolesHolder" %><%@
    page import="jetbrains.buildServer.web.util.WebUtil" %><%@
    include file="/include-internal.jsp" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin"

%><jsp:useBean id="userListForm" type="jetbrains.buildServer.controllers.admin.users.UserListForm" scope="request"
/><jsp:useBean id="availableRolesBean" type="jetbrains.buildServer.controllers.user.AvailableRolesBean" scope="request"
/><jsp:useBean id="pageUrl" type="java.lang.String" scope="request"
/><c:set var="currentTab" value="users"
/><c:set var="userListPager" value="${userListForm.pager}" scope="request"
/><c:set var="encodedCameFromTitle" value='<%=WebUtil.encode("Users")%>'
/><c:set var="encodedCameFromUrl" value="<%=WebUtil.encode(pageUrl)%>"
/><c:set var="usersCount" value="${userListForm.numOfRegisteredUsers}" scope="request"/>

<div>
  <div id="userList">
    <c:url var="licensesUrl" value="/admin/admin.html?item=license"/>
    <div class="messageWrapper">
      <div class="note">
        There <bs:are_is val="${usersCount}"/> <strong>${usersCount}</strong> user<bs:s val="${usersCount}"/> registered.
      </div>
    </div>

  <table class="userListTopTable">
    <tr>
      <authz:authorize allPermissions="CREATE_USER">
        <c:if test="${userListForm.showCreateUserLink}">
          <td>
            <c:url var="createUserUrl" value="/admin/createUser.html?init=1"/>
            <forms:addButton href="${createUserUrl}">Create user account</forms:addButton>
          </td>
        </c:if>
      </authz:authorize>

      <c:if test="${not userListForm.showCreateUserLink}">
        <c:url var="serverConfig" value="/admin/admin.html?item=serverConfigGeneral"/>
        <authz:authorize allPermissions="CHANGE_SERVER_SETTINGS">
          <jsp:attribute name="ifAccessGranted">
            <td>
              <p class="addNewImposs">User accounts in the current <a href="${serverConfig}">authentication scheme</a> are created automatically upon login.</p>
            </td>
          </jsp:attribute>
          <jsp:attribute name="ifAccessDenied">
            <td>
              <p class="addNewImposs">the current authentication scheme are created automatically upon login.</p>
            </td>
          </jsp:attribute>
        </authz:authorize>
      </c:if>

      <c:if test="${userListForm.guestEnabled}">
      <td>
        <p style="text-align: right">
          <bs:editUserLink user="${userListForm.guestUser}" tab="userGroups"
                           cameFromUrl="${pageUrl}" cameFromTitle="${title}">Guest user settings</bs:editUserLink>
        </p>
      </td>
      </c:if>
    </tr>
  </table>

  <bs:messages key="userCreated" />
  <bs:messages key="userAccountRemoved" />

  <c:url var="usersUrl" value="/admin/admin.html?item=users"/>
  <form id="filterForm" action="${usersUrl}" onsubmit="return BS.UserListForm.doSearch()">

  <%--@elvariable id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary"--%>
  <bs:refreshable containerId="userTable" pageUrl="${usersUrl}">
  <table class="userListFilter">
    <tr>
      <td class="label firstLabel">
        <label for="keyword">Find:</label>
      </td>
      <td class="keywordField">
        <forms:textField name="keyword" size="20" maxlength="1024" value="${userListForm.keyword}"/>
      </td>
      <td class="label">
        <label for="groupCode">Group:</label>
      </td>
      <td class="group">
        <forms:select name="groupCode" id="groupCode" className="longField" enableFilter="true">
        <c:forEach items="${userListForm.groups}" var="group">
          <forms:option value="${group.key}"
                        selected="${userListForm.groupCode == group.key}"><c:out value="${group.name}"/></forms:option>
        </c:forEach>
        </forms:select>
      </td>
      <td class="button">
        <input type="hidden" name="tab" value="userList"/>
        <input class="btn btn_mini" type="submit" name="submitFilter" value="Filter"/>
        <forms:resetFilter resetHandler="BS.UserListForm.resetFilter(); BS.UserListForm.doSearch();"/>
      </td>
    </tr>
    <c:if test="${serverSummary.perProjectPermissionsEnabled}">
    <tr>
      <td colspan="2">&nbsp;</td>
      <td class="label roleProjectLabel">
        <label for="roleId">Role</label>&nbsp;/&nbsp;<label for="projectId">Project:</label>
      </td>
      <td class="roleProject" colspan="2">
        <forms:select name="roleId" id="roleId" style="width: 14em;" enableFilter="true">
          <option value="">-- Choose role --</option>
        <c:forEach items="${userListForm.roles}" var="role">
          <forms:option value="${role.id}" selected="${userListForm.roleId == role.id}"><c:out value="${role.name}"/></forms:option>
        </c:forEach>
        </forms:select>&nbsp;<forms:select name="projectId" id="projectId" style="width: 15.5em;" enableFilter="true">
          <option value="">-- Choose project --</option>
          <forms:option value="__ALL__" selected="${userListForm.projectId == '__ALL__'}">&lt;All projects&gt;</forms:option>
        <c:forEach items="${userListForm.projects}" var="project">
          <forms:option value="${project.projectId}"
                        selected="${userListForm.projectId == project.projectId}"><c:out value="${project.extendedName}"/></forms:option>
        </c:forEach>
        </forms:select>
      </td>
    </tr>
    </c:if>
    <tr>
      <c:if test="${serverSummary.perProjectPermissionsEnabled}">
      <td colspan="3" class="label firstLabel">
        <forms:saving className="progressRingInline"/>
        Found <strong>${userListForm.usersCount}</strong> user<bs:s val="${userListForm.usersCount}"/>
      </td>
      <td colspan="2" style="padding-left: 0">
        <forms:checkbox name="rolesConditionInverted" checked="${userListForm.rolesConditionInverted}"/>
        <label for="rolesConditionInverted">Search for users without selected role</label>
      </td>
      </c:if>
      <c:if test="${not serverSummary.perProjectPermissionsEnabled}">
        <td colspan="6" class="label firstLabel">
          <forms:saving className="progressRingInline"/>
          Found <strong>${userListForm.usersCount}</strong> user<bs:s val="${userListForm.usersCount}"/>
        </td>
      </c:if>
    </tr>
  </table>

  <c:if test="${userListForm.usersCount > 0}">
    <table class="usersActions">
      <tr>
        <td>
          <forms:addButton onclick="return BS.UserListForm.addSelected();">Add selected users to groups</forms:addButton>

          <c:if test="${serverSummary.perProjectPermissionsEnabled}">
          <c:if test="${afn:permissionGrantedGlobally('CHANGE_USER') or afn:permissionGrantedForAnyProject('CHANGE_USER_ROLES_IN_PROJECT')}">
            <span class="moreActions">
              <bs:simplePopup controlId="userActions" linkOpensPopup="true"
                              popup_options="delay: 0, shift: {x: -80, y: 20}, className: 'quickLinksMenuPopup'">
                <jsp:attribute name="content">
                  <ul class="menuList">
                    <l:li onclick="if (BS.UserListForm.usersSelected()) BS.AssignRoleDialog.show(BS.UserListForm.getSelectedUsers(), function() { $('userTable').refresh(); });">
                      <a href="#" onclick="return false">Assign roles to the selected users</a>
                    </l:li>
                    <l:li onclick="if (BS.UserListForm.usersSelected()) BS.UnassignRoleDialog.show(BS.UserListForm.getSelectedUsers(), function() { $('userTable').refresh(); });">
                      <a href="#" onclick="return false">Unassign roles from the selected users</a>
                    </l:li>
                  </ul>
                </jsp:attribute>
                <jsp:body>More Actions</jsp:body>
              </bs:simplePopup>
            </span>
          </c:if>
          </c:if>
        </td>

        <td class="usersToShow">
          <bs:messages key="userRolesUpdated"/>
          <bs:messages key="usersAttached"/>
          <bs:messages key="userAssigned"/>
            <label for="usersToShow">Users to show:</label>&nbsp;
            <forms:select name="usersToShow" id="usersToShow" style="width: 5em;" onchange="BS.UserListForm.doSearch()" enableFilter="true">
              <forms:option value="20" selected="${userListForm.usersToShow == 20}">20</forms:option>
              <forms:option value="50" selected="${userListForm.usersToShow == 50}">50</forms:option>
              <forms:option value="100" selected="${userListForm.usersToShow == 100}">100</forms:option>
              <forms:option value="500" selected="${userListForm.usersToShow == 500}">500</forms:option>
              <forms:option value="-1" selected="${userListForm.usersToShow == -1}">All</forms:option>
            </forms:select>
        </td>
      </tr>
    </table>
  </c:if>

  <c:set var="linkTitle" value="Click to edit user ${afn:permissionGrantedGlobally('CHANGE_USER') ? 'settings' : 'groups & roles'}"/>
    <l:tableWithHighlighting className="dark sortable userList borderBottom" mouseovertitle="${linkTitle}" highlightImmediately="true">
      <tr>
        <c:set var="firstCellClass" value="firstCell"/>
        <th class="selectAll firstCell">
          <forms:checkbox name="selectAll"
                          onmouseover="BS.Tooltip.showMessage(this, {shift: {x: 10, y: 20}, delay: 600}, 'Click to select / unselect all users')"
                          onmouseout="BS.Tooltip.hidePopup()"
                          onclick="if (this.checked) BS.Util.selectAll($('filterForm'), 'userId'); else BS.Util.unselectAll($('filterForm'), 'userId')"/>
        </th>
        <c:set var="firstCellClass" value=""/>
        <th class="sortable ${firstCellClass}" <c:if test="${not serverSummary.perProjectPermissionsEnabled}">style="padding: 3px 3px 3px 6px;"</c:if>>
          <span id="SORT_BY_USERNAME">Username</span>
        </th>
        <th class="sortable"><span id="SORT_BY_NAME">Name</span></th>
        <authz:authorize anyPermission="VIEW_USER_PROFILE, CHANGE_USER">
        <th class="sortable" <bs:allEmails users="${userListForm.shownUsers}"/>><span id="SORT_BY_EMAIL">Email</span></th>
        </authz:authorize>
        <th>Groups</th>
        <c:choose>
          <c:when test="${serverSummary.perProjectPermissionsEnabled}">
            <th>Roles</th>
          </c:when>
          <c:otherwise>
            <th class="sortable"><span id="SORT_BY_ADMIN_STATUS">Administrator</span></th>
          </c:otherwise>
        </c:choose>
        <th class="sortable lastCell"><span id="SORT_BY_LAST_ACCESS_TIME">Last access time</span></th>
      </tr>
      <c:forEach items="${userListForm.shownUsers}" var="user">
        <c:set var="onclick">BS.openUrl(event, '<bs:editUserLink user="${user}" cameFromTitle="${title}" cameFromUrl="${pageUrl}" noLink="true"/>');</c:set>
        <c:set var="highlightClass">highlight</c:set>
        <tr>
          <td class="selectUser ${highlightClass}"><forms:checkbox name="userId" value="${user.id}" disabled="${currentUser.id == user.id}"/></td>
          <td class="username ${highlightClass}" onclick="${onclick}">
            <bs:editUserLink user="${user}" cameFromTitle="${title}" cameFromUrl="${pageUrl}"><c:out value="${user.username}"/></bs:editUserLink>
          </td>
          <td class="fullname ${highlightClass}" onclick="${onclick}"><c:if test="${fn:length(user.name) == 0}">N/A</c:if> <c:out value="${user.name}"/></td>
          <authz:authorize anyPermission="VIEW_USER_PROFILE, CHANGE_USER">
          <td class="email ${highlightClass}" onclick="${onclick}"><c:if test="${fn:length(user.email) == 0}">N/A</c:if> <c:out value="${user.email}"/></td>
          </authz:authorize>
          <td class="groups noTitle ${highlightClass}" onclick="${onclick}">
            <c:set var="groupsNum" value="${fn:length(user.userGroups)}"/>
            <bs:popupControl showPopupCommand="BS.ParentGroupsPopup.showUserParentGroups(this, ${user.id})"
                             hidePopupCommand="BS.ParentGroupsPopup.hidePopup()"
                             stopHidingPopupCommand="BS.ParentGroupsPopup.stopHidingPopup()"
                             controlId="userGroups:${user.id}"
              ><bs:editUserLink user="${user}" tab="userGroups"
                ><c:choose
                  ><c:when test="${groupsNum > 0}">View groups (${groupsNum})</c:when
                  ><c:otherwise>No groups</c:otherwise
                ></c:choose
              ></bs:editUserLink
            ></bs:popupControl>
          </td>
          <c:choose>
            <c:when test="${serverSummary.perProjectPermissionsEnabled}">
              <td class="${highlightClass} roles noTitle">
                <c:set var="rolesCount" value='${fn:length(user.roles)}'/>
                <c:set var="inheritedRolesCount" value='<%=AuthorityRolesBean.getNumberOfInheritedRoles((RolesHolder)jspContext.getAttribute("user"))%>'/>
                <bs:popupControl showPopupCommand="BS.AuthorityRolesPopup.showPopup(this, ${user.id})"
                                 hidePopupCommand="BS.AuthorityRolesPopup.hidePopup()"
                                 stopHidingPopupCommand="BS.AuthorityRolesPopup.stopHidingPopup()"
                                 controlId="userRoles:${user.id}"
                  ><bs:editUserLink user="${user}" tab="userRoles"
                    ><c:choose
                      ><c:when test="${rolesCount + inheritedRolesCount> 0}"
                        >View roles (<c:if test="${rolesCount > 0}"><strong>${rolesCount}</strong></c:if
                        ><c:if test="${rolesCount == 0}">0</c:if><c:if test="${inheritedRolesCount > 0}">/${inheritedRolesCount}</c:if>)</c:when
                      ><c:otherwise>No roles</c:otherwise
                    ></c:choose
                  ></bs:editUserLink
                ></bs:popupControl>
              </td>
            </c:when>
            <c:otherwise>
              <td class="${highlightClass} roles" onclick="${onclick}"><c:if test="${user.systemAdministratorRoleGranted}">Admin</c:if>&nbsp;</td>
            </c:otherwise>
          </c:choose>
          <td class="date ${highlightClass}" onclick="${onclick}"><bs:date value="${user.lastLoginTimestamp}" pattern="dd MMM yy HH:mm:ss"/></td>
        </tr>
      </c:forEach>
    </l:tableWithHighlighting>

    <c:set var="pagerUrlPattern" value="admin.html?item=users&page=[page]"/>
    <bs:pager place="bottom" urlPattern="${pagerUrlPattern}" pager="${userListPager}"/>

    <script type="text/javascript">
      (function() {
        <c:forEach items="${userListForm.sortOptions}" var="sortOption">
        var sortOption = $('${sortOption}');
        if (sortOption) {
        <c:if test="${userListForm.sortBy == sortOption}">
          sortOption.className = sortOption.className + ' ${userListForm.sortAsc ? "sortedAsc" : "sortedDesc"}';
        </c:if>
          sortOption.on("click", BS.UserListForm.reSort.bindAsEventListener(BS.UserListForm));
        }
        </c:forEach>
      })();
    </script>
  </bs:refreshable>

    <input type="hidden" name="sortBy" value="${userListForm.sortBy}"/>
    <input type="hidden" name="sortAsc" value="${userListForm.sortAsc}"/>
    <input type="hidden" name="page" value="${userListPager.currentPage}"/>
  </form>

    <admin:assignRolesDialog availableRolesBean="${availableRolesBean}"/>
    <admin:unassignRolesDialog availableRolesBean="${availableRolesBean}"/>
  </div>
</div>

<jsp:include page="/admin/attachToGroups.html"/>
