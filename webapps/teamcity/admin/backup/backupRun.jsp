<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>

<jsp:useBean id="backupPath" type="java.lang.String" scope="request"/>
<jsp:useBean id="settings" type="jetbrains.buildServer.serverSide.maintenance.BackupSettings" scope="request"/>
<jsp:useBean id="current" type="jetbrains.buildServer.controllers.admin.backup.BackupCurrentInfo" scope="request"/>

<script type="text/javascript">
  // TODO Move to external JS
  var BackupRunForm = OO.extend(BS.AbstractWebForm, {
    _backupStarted: false,
    _inited: false,

    init2: function() {
      var initialPreset = '${settings.preset}';
      if (initialPreset == null || initialPreset == '')
        initialPreset = 'CD';
      $('settings.preset').setValue(initialPreset);

      this.refreshPresetPanels();
      this._inited = true;

      <c:if test="${current.canStartBackup}">
        this.updateButtonStatusPeriodically();
      </c:if>

    },

    formElement: function() {
      return $('BackupRunForm');
    },

    savingIndicator: function() {
      return $('startBackup');
    },

    justModified: function() {
      this.updateButtonStatus();
    },

    updateButtonStatus: function() {
      <c:if test="${current.canStartBackup}">
        scopeSelected = $("settings.customIncludeConfiguration").checked ||
                        $("settings.customIncludeDatabase").checked ||
                        $("settings.customIncludeBuildLogs").checked ||
                        $("settings.customIncludePersonalChanges").checked ||
                        $("settings.preset").value == "CD" ||
                        $("settings.preset").value == "CDLP";
        $("settings.preset").disabled = false;
        fileNameSelected = $("settings.fileName").value.length > 0;
        $("submitStartBackup").disabled = !(scopeSelected && fileNameSelected);
      </c:if>
    },

    updateButtonStatusPeriodically: function() {
      BackupRunForm.updateButtonStatus();
      setTimeout(function() {
        BackupRunForm.updateButtonStatusPeriodically()
      }, 250);
    },

    doSubmitStart: function() {
      $("submitStartBackup").disabled = true;
      // this.formElement().backupAction.value = "start";
      this.doSubmit();
    },

    doSubmitCancel: function() {
      // this.formElement().backupAction.value = "cancel";
      this.doSubmit();
    },

    doSubmit: function() {
      BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.ErrorsAwareListener, {
        onSuccessfulSave: function() {
          BS.reload(true);
        }
      }));
    },

    backupInProgress: function() {
      this._backupStarted = true;
      window.setTimeout(function() {
        $('backupProgress').refresh();
      }, 3000);
    },

    backupStopped: function() {
      if (this._backupStarted) {
        window.setTimeout(function() {
          BS.reload(true);
        }, 800);
      }
    },

    refreshPresetPanels: function() {
      var curPreset = $('settings.preset').value;
      if (!BackupRunForm._inited) {
        $('settings.preset').setValue(curPreset);
      }

      if (curPreset == "CD") jQuery("#noteForPresetCD").removeClass("hidden"); else jQuery("#noteForPresetCD").addClass("hidden");
      if (curPreset == "CDLP") jQuery("#noteForPresetCDLP").removeClass("hidden"); else jQuery("#noteForPresetCDLP").addClass("hidden");
      if (curPreset == "X") jQuery("#customScopeCheckBoxes").removeClass("hidden"); else jQuery("#customScopeCheckBoxes").addClass("hidden");
      this.updateButtonStatus();
    }

  });
</script>

<div id="container">

<form method="post" action="<c:url value='/admin/backupPage.html'/>" id="BackupRunForm" onsubmit="return false;">

  <c:if test="${current.canStartBackup}">
  </c:if>
  
  <h2>Start backup</h2>

  <div <c:if test="${not current.canStartBackup}">class="disabled"</c:if>>

    <table class="runnerFormTable">
      <tr>
        <th><label for="config.fileName">Backup file:</label></th>
        <td>
          <forms:textField name="settings.fileName" value="${settings.fileName}" style="width: 25em" maxlength="240" onchange="BackupRunForm.justModified()" disabled="${not current.canStartBackup}" /> &nbsp;
          <forms:checkbox name="settings.addTimestampSuffix" checked="${settings.addTimestampSuffix}" onclick="BackupRunForm.justModified()" disabled="${not current.canStartBackup}" /> <label for="settings.addTimestampSuffix" disabled="${not current.canStartBackup}">add timestamp suffix</label>
          <div style="margin:0;" class="smallNote">
            Directory for backup files: <strong><c:out value='${backupPath}'/></strong><br/>
          </div>
        </td>
      </tr>
      <tr>
        <th>Backup scope:</th>
        <td>
          <forms:select name="settings.preset" onchange="BackupRunForm.refreshPresetPanels()" disabled="true">
            <forms:option value="CD">Basic</forms:option>
            <forms:option value="CDLP">All except build artifacts</forms:option>
            <forms:option value="X">Custom</forms:option>
          </forms:select><br/>
          <ul id="noteForPresetCD" class="presetDescription hidden">
            <li>server settings, projects and builds configurations, plugins</li>
            <li>database</li>
          </ul>
          <ul id="noteForPresetCDLP" class="presetDescription hidden">
            <li>server settings, projects and builds configurations, plugins</li>
            <li>database</li>
            <li>build logs</li>
            <li>personal builds changes</li>
          </ul>
          <div id="customScopeCheckBoxes" class="presetDescriptionCustom hidden">
            <forms:checkbox name="settings.customIncludeConfiguration" checked="${settings.customIncludeConfiguration}" onclick="BackupRunForm.justModified();" disabled="${not current.canStartBackup}" /> <label for="settings.customIncludeConfiguration" class="<c:if test='${not current.canStartBackup}'>disabled</c:if>" >server settings, projects and builds configurations, plugins</label> <br/>
            <forms:checkbox name="settings.customIncludeDatabase" checked="${settings.customIncludeDatabase}" onclick="BackupRunForm.justModified();" disabled="${not current.canStartBackup}" /> <label for="settings.customIncludeDatabase" class="<c:if test='${not current.canStartBackup}'>disabled</c:if>" >database</label> <br/>
            <forms:checkbox name="settings.customIncludeBuildLogs" checked="${settings.customIncludeBuildLogs}" onclick="BackupRunForm.justModified();" disabled="${not current.canStartBackup}" /> <label for="settings.customIncludeBuildLogs" class="<c:if test='${not current.canStartBackup}'>disabled</c:if>" >build logs</label> <br/>
            <forms:checkbox name="settings.customIncludePersonalChanges" checked="${settings.customIncludePersonalChanges}" onclick="BackupRunForm.justModified();" disabled="${not current.canStartBackup}" /> <label for="settings.customIncludePersonalChanges" class="<c:if test='${not current.canStartBackup}'>disabled</c:if>" >personal builds changes</label>
          </div>
          <p style="margin:0;" class="smallNote">
            Note: running builds and build queue state will not be included in the backup.
            If you want to backup these, use the command line <bs:helpLink file='Creating+Backup+via+maintainDB+command-line+tool'>maintainDB tool</bs:helpLink> while the TeamCity server is offline.
          </p>
        </td>
      </tr>
    </table>

    <c:if test="${current.databaseIsHSQL}">
      <br/>
      <p class="attentionComment">
        The system is currently configured to work with an internal database (HSQL), which has some limitations.
        As a result, backup of a large database can cause an <strong>OutOfMemory</strong> error.
        See the <bs:helpLink file='TeamCity+Data+Backup'>TeamCity Data Backup</bs:helpLink> page for details.
      </p>
    </c:if>

    <div class="saveButtonsBlock">
      <forms:saving id="startBackup"/>
      <forms:submit id="submitStartBackup" name="submitStartBackup" label="Start Backup" onclick="BackupRunForm.doSubmitStart()" disabled="${not current.canStartBackup}"/>
      <c:if test="${current.disabledNow}">
        <br>
        Cannot start backup process: ${current.disabledReason}
      </c:if>
    </div>

  </div>

  <c:if test="${not current.canStartBackup and not current.backupInProgress}">
    <div>
      Cannot start backup process: ${current.disabledReason} 
    </div>
  </c:if>


  <c:if test="${current.showReport}">

    <h2>
      <c:choose>
        <c:when test="${current.backupInProgress}">
          Current
        </c:when>
        <c:otherwise>
          The last
        </c:otherwise>
      </c:choose>
      backup report
    </h2>

    <bs:refreshable containerId="backupProgress" pageUrl="${pageUrl}">

      <table style="margin-top: 1em">

        <tr>
          <td>Backup file:</td>
          <td>
            <c:if test="${current.canBeDownloaded}">
              <a href="<c:url value='${current.downloadURL}'/>"><span class="mono mono-12px">${current.report.zipFileName}</span></a>
            </c:if>
            <c:if test="${not current.canBeDownloaded}">
              <span class="mono mono-12px">${current.report.zipFileName}</span>
            </c:if>
            <c:if test="${current.done}">
              &emsp; (&thinsp;${current.archiveSizeForHuman}&thinsp;)
            </c:if>
          </td>
        </tr>

        <c:forEach var="record" items="${current.stageRecords}">
          <c:if test='${record.state != "NotSelected"}'>
            <tr>
              <td nowrap="true" style="padding-right:0.5cm">
                ${record.stage.phrase}:
              </td>
              <td>
                <c:choose>
                  <c:when test='${record.state == "InProgress"}'>
                    <span class="running">${record.state.decsription}</span>
                    <c:if test='${record.stage.objectsAreTables or record.stage.objectsAreFiles}'>
                      (${record.progress}% in <bs:printTime time="${record.timeSpent/1000}"/><c:if
                        test='${record.stage.objectsAreTables}'>, exporting table (${record.objectsProcessed+1} out of ${record.objectsEstimated}) <i>${record.currentObjectName}</i></c:if>)
                    </c:if>
                  </c:when>
                  <c:when test='${record.state == "Done"}'>
                    ${record.state.decsription}
                    in <bs:printTime time="${record.timeSpent/1000}"/><c:if test='${record.stage == "Database"}'>,
                                                                         exported ${record.objectsProcessed} tables
                                                                      </c:if>
                  </c:when>
                  <c:when test='${record.state == "Failed"}'>
                    <span class="error-hl">${record.state.decsription}</span>
                    <c:if test="${current.hasExceptions}">
                      <p>
                        <c:forEach var="ex" items="${current.exceptions}">
                          <span class="error-details">
                            <c:forEach var="exm" items="${ex}" varStatus="vs">
                              <c:if test="${not vs.first}"> &nbsp;&nbsp;&nbsp; caused by </c:if>
                              <span class="mono mono-12px">${exm.a}:</span>  <c:out value="${exm.b}" escapeXml="true"/><br/>
                            </c:forEach>
                          </span>
                        </c:forEach>
                      </p>
                    </c:if>
                  </c:when>
                  <c:otherwise>
                    ${record.state.decsription}
                  </c:otherwise>
                </c:choose>
              </td>
            </tr>
          </c:if>
        </c:forEach>
      </table>

      <c:if test='${current.report.overallStatus == "Done" or current.report.overallStatus == "Cancelled" or current.report.overallStatus == "Failed"}'>
        <p>
          <c:choose>
            <c:when test='${current.report.overallStatus == "Done"}'>Backup completed successfully </c:when>
            <c:when test='${current.report.overallStatus == "Cancelled"}'>Backup canceled by user </c:when>
            <c:when test='${current.report.overallStatus == "Failed"}'>Backup <span class="error-hl">failed </span> </c:when>
          </c:choose>
          in <bs:printTime time="${current.report.timeSpent/1000}"/>
        </p>
      </c:if>

      <c:if test="${current.backupInProgress}">
        <div class="saveButtonsBlock">
          <forms:saving id="cancelBackup"/>
          <forms:submit type="button" label="Cancel current backup" id="submitCancelBackup" name="submitCancelBackup" onclick="BackupRunForm.doSubmitCancel();" disabled="${!current.backupInProgress || current.cancelling}"/>
        </div>
      </c:if>

      <script type="text/javascript">
        <c:if test="${current.backupInProgress}">
        BackupRunForm.backupInProgress();
        </c:if>
        <c:if test="${not current.backupInProgress}">
        BackupRunForm.backupStopped();
        </c:if>
      </script>
      
    </bs:refreshable>

  </c:if>

</form>

</div>

<script type="text/javascript">
  BackupRunForm.init2();
</script>
