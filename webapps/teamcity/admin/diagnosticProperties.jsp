<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.admin.DiagnosticPropertiesExtension.PropertiesBean"/>

<table class="runnerFormTable" id="internalProperties">
  <tr class="groupingTitle">
    <td>File properties (from <code>${propertiesBean.fullPathToPropertiesFile}</code>)</td>
  </tr>
  <tr>
    <td class="values">
      <admin:_propertiesList properties="${propertiesBean.internalProperties}"/>
    </td>
  </tr>
  <tr class="groupingTitle">
    <td>Java system properties</td>
  </tr>
  <tr>
    <td class="noBorder values">
      <admin:_propertiesList properties="${propertiesBean.relatedSystemProperties}"/>
      <div><a href="#" id="otherPropToggle">Show all properties</a></div>
      <admin:_propertiesList properties="${propertiesBean.otherSystemProperties}"
                             style="display: none"
                             id="otherProperties"/>
    </td>
  </tr>
</table>

<script type="text/javascript">
  jQuery("#otherPropToggle").click(function() {
    jQuery("#otherProperties").toggle();
    jQuery(this).hide();
    return false;
  });
</script>
