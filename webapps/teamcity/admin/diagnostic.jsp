<%@ page import="jetbrains.buildServer.util.StringUtil" %>
<%@ page import="jetbrains.buildServer.util.DiagnosticUtil" %>
<%@include file="/include-internal.jsp"%>
<style type="text/css">
  .runnerFormTable td {
    padding: 15px 0 15px 4px;
  }
</style>

<bs:linkScript>
  /js/bs/jvmStatusForm.js
</bs:linkScript>

<div style="display: none;" id="progressIndicator"><forms:saving className="progressRingInline"/> Please wait...</div>

<div style="width: 100%; margin-top: 0.5em;">
  <bs:messages key="loggingPresetLoaded"/>
  <bs:messages key="threadDumpSucceeded"/>
  <bs:messages key="memoryDumpSucceeded"/>
  <div class="error" id="errorsHolder"></div>
</div>

<form action="<c:url value='/admin/diagnostic.html'/>" method="post" id="jvmStatusForm" style="padding:0; margin: 0.5em 0">

  <table class="runnerFormTable">

    <tr class="groupingTitle">
      <td>Debug logging</td>
    </tr>

    <tr>
      <td>
        <c:url var="url" value="/admin/admin.html?item=diagnostics"/>
        Active logging preset:
        <forms:select name="loggingPreset" onchange="BS.JvmStatusForm.loadPreset()">
          <c:forEach items="${loggingPresets}" var="preset">
            <forms:option value="${preset}" selected="${preset == currentLoggingPreset}"><c:out value="${preset}"/></forms:option>
          </c:forEach>
        </forms:select>
        <span class="smallNote">Changes will be effective until server restart.</span>
      </td>
    </tr>

    <tr class="groupingTitle">
      <td>Hangs and Thread Dumps</td>
    </tr>

    <tr>
      <td>
        <input class="btn" type="button" name="threadDump" value="Save Thread Dump" onclick="BS.JvmStatusForm.takeThreadDump()" style="float: right;"/>
        If the TeamCity server appears slow or is not responding, please try taking several thread dumps with some interval.
        <br/>
        On this page you can either <a target="_blank" href="<c:url value='/admin/diagnostic.html?actionName=threadDump&save=false'/>">view a server thread dump</a> in a new browser window
        or save a thread dump to a file.
      </td>
    </tr>

    <tr class="groupingTitle">
      <td>OutOfMemory Problems</td>
    </tr>

    <tr>
      <td>
        <c:set var="memUsage" value="<%=DiagnosticUtil.getHeapMemoryUsage()%>"/>
        <c:set var="memUsageUsedStr" value="<%=StringUtil.formatFileSize(DiagnosticUtil.getHeapMemoryUsage().getUsed())%>"/>
        <c:set var="memUsageMaxStr" value="<%=StringUtil.formatFileSize(DiagnosticUtil.getHeapMemoryUsage().getMax())%>"/>

        <c:if test="${memoryDumpAvailable}">
          <input class="btn" type="button" name="memoryDump" value="Dump Memory Snapshot" onclick="BS.JvmStatusForm.takeMemoryDump()" style="float: right;"/>
        </c:if>

        <div>
          Current memory usage: <strong>${memUsageUsedStr}</strong> (<strong><fmt:formatNumber value="${100.0 * memUsage.used / memUsage.max}" maxFractionDigits="1"/>%</strong>
          of maximum available memory: <strong>${memUsageMaxStr}</strong>).
        </div>

        <c:choose>
          <c:when test="${memoryDumpAvailable}">
            If you notice that TeamCity server is consuming too much memory,
            please dump a memory snapshot and <a href="<bs:helpUrlPrefix/>/Reporting+Issues#ReportingIssues-sendingLargeFiles">send</a> it to us.
          </c:when>
          <c:otherwise>
            If you notice that TeamCity server is consuming too much memory,
            read more on how to take a memory snapshot in TeamCity <a href="<bs:helpUrlPrefix/>/Reporting+Issues#ReportingIssues-OutOfMemoryProblems">documentation</a>.
          </c:otherwise>
        </c:choose>
      </td>
    </tr>

    <tr class="groupingTitle">
      <td>JVM arguments</td>
    </tr>
    <tr>
      <td><pre style="white-space: pre-wrap;"><c:forEach items="${jvmArgs}" var="arg"
        ><c:out value="${arg}"/> </c:forEach></pre></td>
    </tr>
  </table>

  <input type="hidden" name="actionName" value=""/>
</form>
