<%@include file="/include-internal.jsp"%>
<jsp:useBean id="reportTabsForm" type="jetbrains.buildServer.controllers.admin.reportTabs.EditProjectReportTabsForm" scope="request"/>

Here you can define custom artifact-based tabs <bs:help file="Including+Third-Party+Reports+in+the+Build+Results"/> for the <bs:projectLink project="${projectForm.originalProject}">Project Home</bs:projectLink> page.
<authz:authorize allPermissions="CHANGE_SERVER_SETTINGS">You can also define <a href="<c:url value='/admin/admin.html?item=reportTabs'/>">artifact-based tabs for build results</a>.</authz:authorize>

<admin:editReportTabsTable pageUrl="${pageUrl}" reportTabsForm="${reportTabsForm}" isProjectTabs="true" project="${reportTabsForm.project}"/>
<admin:editReportTabDialog title="Edit Report Tab Settings" isProjectTab="true" project="${reportTabsForm.project}"/>
