<%@ include file="/include-internal.jsp" %>
<jsp:useBean id="editGroupBean" type="jetbrains.buildServer.controllers.admin.groups.EditGroupBean" scope="request"/>
<form action="${pageUrl}" id="editGroup" onsubmit="return BS.EditGroupForm.submit()" method="post">

<bs:messages key="groupUpdated"/>

<span class="error" id="error_editGroup"></span>
<bs:allUsersGroup group="${editGroupBean.group}">
  <jsp:attribute name="ifAllUsersGroup">
    <div class="attentionComment">
      <strong><c:out value="${editGroupBean.group.name}"/></strong> is a special group which contains all users and cannot be deleted.
      This group name and description cannot be changed too. 
    </div>
  </jsp:attribute>
  <jsp:attribute name="ifUsualGroup">
    <authz:authorize allPermissions="DELETE_USERGROUP">
      <input class="btn btn_mini submitButton" type="button" value="Delete Group" style="margin: 0 0 0.5em 0;"
             onclick="BS.GroupActions.deleteGroup('${editGroupBean.group.key}', ${fn:length(editGroupBean.group.allUsers)}, function() {document.location.href='admin.html?item=groups'})"/>
      <div class="clr"></div>
    </authz:authorize>
  </jsp:attribute>
</bs:allUsersGroup>

<c:set var="readOnly">
  <tr>
    <th>Name:</th>
    <td><c:out value="${editGroupBean.group.name}"/></td>
  </tr>
  <tr>
    <th><label for="groupDescription">Description:</label></th>
    <td><c:out value="${editGroupBean.group.description}"/></td>
  </tr>
</c:set>
<c:set var="readOnlyMode" value="false"/>

<table class="runnerFormTable">
  <tr>
    <th>Group Key:</th>
    <td><c:out value="${editGroupBean.group.key}"/></td>
  </tr>
  <bs:allUsersGroup group="${editGroupBean.group}">
    <jsp:attribute name="ifAllUsersGroup">
      ${readOnly}
      <c:set var="readOnlyMode" value="true"/>
    </jsp:attribute>
    <jsp:attribute name="ifUsualGroup">
      <c:choose>
        <c:when test="${not afn:permissionGrantedGlobally('CHANGE_USERGROUP')}">
          ${readOnly}
          <c:set var="readOnlyMode" value="true"/>
        </c:when>
        <c:otherwise>
          <tr>
            <th><label for="groupName">Name:<l:star/></label></th>
            <td>
              <forms:textField name="groupName" className="longField" maxlength="256" value="${editGroupBean.groupName}"/>
              <span class="error" id="error_groupName"></span>
            </td>
          </tr>
          <tr>
            <th><label for="groupDescription">Description:</label></th>
            <td>
              <forms:textField name="groupDescription" className="longField" maxlength="256" value="${editGroupBean.groupDescription}"/>
            </td>
          </tr>
        </c:otherwise>
      </c:choose>
    </jsp:attribute>
  </bs:allUsersGroup>
  <c:if test="${not editGroupBean.perProjectPermissionsEnabled}">
  <tr>
    <td colspan="2">
      <admin:perProjectRolesNote/>
      <forms:checkbox name="administrator" checked="${editGroupBean.administrator}"/> <label for="administrator">Group with administrative privileges (included users and groups will have administrative privileges too)</label>
      <c:if test="${editGroupBean.administratorStatusInherited}">
        <div class="smallNote" style="margin-left: 0;">Administrative privileges are inherited from one or more parent groups</div>
      </c:if>
    </td>
  </tr>
  </c:if>
</table>

<admin:_attachToGroups attachToGroupsBean="${editGroupBean.attachGroupBean}" formId="editGroup"/>
<c:if test="${editGroupBean.canBeMoved or not readOnlyMode or not serverSummary.perProjectPermissionsEnabled}">
  <input type="hidden" name="groupCode" value="${editGroupBean.group.key}"/>
  <input type="hidden" name="submitGroup" value=""/>

  <div class="saveButtonsBlock">
    <forms:cancel cameFromSupport="${editGroupBean.cameFromSupport}"/>
    <forms:submit name="submitSettings" label="Save"/>
    <forms:saving/>
  </div>

</c:if>
</form>

<c:if test="${editGroupBean.canBeMoved or not readOnlyMode or not serverSummary.perProjectPermissionsEnabled}">
<forms:modified/>
<script type="text/javascript">
  BS.EditGroupForm.setUpdateStateHandlers({
    updateState: function() {
      BS.EditGroupForm.storeInSession();
    },
    saveState: function() {
      BS.EditGroupForm.submit();
    }
  });
</script>
</c:if>