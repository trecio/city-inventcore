<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<admin:editBuildTypePage selectedStep="general">
  <jsp:attribute name="head_include">

    <script type="text/javascript">
      $j(document).ready(function() {
        $('editBuildTypeForm').name.focus();

        BS.EditBuildTypeForm.setupEventHandlers();
        BS.EditBuildTypeForm.setModified(${buildForm.stateModified});
      });
    </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <form action="<c:url value='/admin/editBuild.html?id=${buildForm.settingsId}'/>" method="post"
          onsubmit="return BS.EditBuildTypeForm.submitBuildType()" id="editBuildTypeForm">

    <admin:buildTypeForm buildForm="${buildForm}" title="${title}"/>

    <div class="saveButtonsBlock">
      <forms:cancel cameFromSupport="${buildForm.cameFromSupport}"/>
      <input type="hidden" id="submitBuildType" name="submitBuildType" value="store"/>
      <forms:submit name="submitButton" label="Save"/>
      <input type="hidden" value="${buildForm.numberOfSettingsChangesEvents}" name="numberOfSettingsChangesEvents"/>
      <forms:saving/>
    </div>
    </form>

    <forms:modified/>

    <c:if test="${buildForm.templateBased}">
      <script type="text/javascript">
        BS.EditBuildTypeForm.setTemplateBased([{name: 'name'}, {name: 'description'}, {name: 'buildCounter'}, {name: 'submitButton'}]);
      </script>
    </c:if>
  </jsp:attribute>
</admin:editBuildTypePage>

