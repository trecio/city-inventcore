<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>

<c:set var="pageTitle" value="Server Internal Counters" scope="request"/>

<bs:page>
<jsp:attribute name="head_include">
  <script type="text/javascript">
    BS.Navigation.items = [
        {title: "Administration", url: '<c:url value="/admin/admin.html"/>'},
        {title: "${pageTitle}", selected:true}
    ];
  </script>
  <style type="text/css">
    .countersData td, .countersData th {
      padding-left: 1em;
      padding-right: 1em;
    }
    .countersData td.r {
      text-align: center;
    }
    .countersData td.d {
      white-space: nowrap;
    }
  </style>
</jsp:attribute>
<jsp:attribute name="body_include">

<table class="countersData">
  <tr>
    <th>Counter name</th>
    <th>Value</th>
    <th>5-mins density</th>
    <th>Last modified</th>
  </tr>
  <c:forEach var="v" items="${vars}">
  <jsp:useBean id="v" type="jetbrains.buildServer.util.Vars"/>
  <tr>
    <td>${v.name}</td>
    <td class="r">${v.count}</td>
    <td class="r">${v.recentCount}</td>
    <td class="d"><bs:date value="${v.lastModified}" smart="true"/></td>
  <%--
    <td><bs:printTime time="${v.interval}"/></td>
    --%>
  </tr>
</c:forEach>
</table>

</jsp:attribute>
</bs:page>
