<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>

<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.CreateBuildTypeForm" scope="request"/>
<jsp:useBean id="pageUrl" type="java.lang.String" scope="request"/>
<c:set var="step" value="${param['step']}"/>
<admin:createBuildTypePage>
  <jsp:attribute name="head_include">
    <bs:linkScript>
      /js/bs/editCheckoutRules.js
    </bs:linkScript>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <bs:refreshable containerId="vcsRootsBody" pageUrl="${pageUrl}">
      <div id="container">
        <div class="editBuildPageGeneral">
          <c:url value='${buildForm.actionPath}?projectId=${buildForm.project.projectId}&step=${step}' var="formAction"/>
          <form id="editVcsSettingsForm" action="${formAction}" method="post" onsubmit="return BS.CreateBuildTypeForm.forward()">

          <admin:buildTypeVcsRootsForm vcsRootsBean="${buildForm.vcsRootsBean}" pageUrl="${pageUrl}" mode="${buildForm.template ? 'createTemplate' : 'createBuildType'}"/>

          <div class="saveButtonsBlock">
            <forms:cancel cameFromSupport="${buildForm.cameFromSupport}"/>
            <input type="hidden" name="step" value="${step}"/>
            <forms:submit name="submitButton" label="Add Build Step >>"/>
            <forms:submit type="button" name="" label="<< General Settings" onclick="BS.CreateBuildTypeForm.back();"/>
            <forms:saving/>
          </div>

          </form>
          <admin:editCheckoutRulesForm formAction="${formAction}"/>
        </div>

        <script type="text/javascript">
          BS.AvailableParams.attachPopups('settingsId=${buildForm.project.projectId}', 'buildTypeParams');
        </script>
      </div>
    </bs:refreshable>
  </jsp:attribute>
</admin:createBuildTypePage>
