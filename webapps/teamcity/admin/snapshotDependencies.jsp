<%@ page import="jetbrains.buildServer.serverSide.dependency.DependencyOptions" %>
<%@ include file="/include-internal.jsp" %>
<jsp:useBean id="sourceDependenciesBean" type="jetbrains.buildServer.controllers.admin.projects.EditableSourceDependenciesBean" scope="request"/>
<c:set var="takeStartedBuildOption"><%=DependencyOptions.TAKE_STARTED_BUILD_WITH_SAME_REVISIONS.getKey()%></c:set>
<c:set var="takeSuccessfulBuildsOnlyOption"><%=DependencyOptions.TAKE_SUCCESSFUL_BUILDS_ONLY.getKey()%></c:set>
<c:set var="runBuildIfDependencyFailed"><%=DependencyOptions.RUN_BUILD_IF_DEPENDENCY_FAILED.getKey()%></c:set>
<c:set var="runBuildOnTheSameAget"><%=DependencyOptions.RUN_BUILD_ON_THE_SAME_AGENT.getKey()%></c:set>
<form action="#">
<h3 class="noBorder">Snapshot Dependencies</h3>
<p class="sectionDescription">Build configurations linked by snapshot dependency will take sources on the same moment of time.
  The build of this configuration will run after all the dependencies are built.
  If necessary, the dependencies will be triggered automatically.
  <bs:help file="Dependent+Build" anchor="SnapshotDependency"/></p>
<c:if test="${empty sourceDependenciesBean.visibleDependencies}">
  <p>There are no snapshot dependencies.</p>
</c:if>
<c:if test="${not empty sourceDependenciesBean.visibleDependencies}">
<l:tableWithHighlighting highlightImmediately="true" id="snapshotDeps" className="parametersTable">
  <tr>
    <th class="checkbox">
      <forms:checkbox name="selectAll"
                      onmouseover="BS.Tooltip.showMessage(this, {shift: {x: 10, y: 20}, delay: 600}, 'Click to select / unselect all dependencies')"
                      onmouseout="BS.Tooltip.hidePopup()"
                      onclick="if (this.checked) BS.Util.selectAll($('snapshotDeps'), 'snDepChkbox'); else BS.Util.unselectAll($('snapshotDeps'), 'snDepChkbox')"/>
    </th>
    <th class="sourceBuildType">Depends on</th>
    <th colspan="3" class="dependencyOptions">Dependency options</th>
  </tr>
  <c:forEach items="${sourceDependenciesBean.visibleDependencies}" var="dependency" varStatus="pos">
    <c:set var="highlight" value='${dependency.sourceBuildTypeAccessible ? "highlight" : ""}'/>
    <c:set var="onclick"><c:if test="${dependency.sourceBuildTypeAccessible}">BS.SourceDependencyForm.editDependency(event, '${dependency.sourceBuildTypeId}')</c:if></c:set>
    <tr>
      <td class="checkbox">
        <forms:checkbox name="snDepChkbox" value="${dependency.sourceBuildTypeId}" disabled="${not dependency.sourceBuildTypeAccessible}"/>
      </td>
      <td class="${highlight}" onclick="${onclick}">
        <c:choose>
          <c:when test="${dependency.sourceBuildTypeAccessible}">
            <c:set var="dependOn" value="${dependency.sourceBuildType}"/>
            <strong>
              <bs:buildTypeLinkFull buildType="${dependOn}" />
            </strong>
          </c:when>
          <c:otherwise><em title="You do not have enough permissions for this build configuration">&laquo;inaccessible build configuration&raquo;</em></c:otherwise>
        </c:choose>
        <c:if test="${dependency.inherited}"><span class="inheritedParam">(inherited)</span></c:if>
      </td>
      <td class="dependencyOptions ${highlight}" onclick="${onclick}">
        <c:if test="${dependency.setOptions[takeStartedBuildOption]}"><div>Do not run new build if there is a suitable one</div></c:if>
        <c:if test="${dependency.setOptions[takeSuccessfulBuildsOnlyOption]}"><div style="margin-left: 1.5em">Only use successful builds from suitable ones</div></c:if>
        <c:if test="${dependency.setOptions[runBuildIfDependencyFailed]}"><div>Run build even if dependency has failed</div></c:if>
        <c:if test="${dependency.setOptions[runBuildOnTheSameAget]}"><div>Run build on the same agent</div></c:if>
        <c:if test="${empty dependency.setOptions}"><em>Always run a new build</em></c:if>
      </td>
      <td class="edit ${highlight}" onclick="${onclick}">
        <c:choose>
          <c:when test="${not dependency.sourceBuildTypeAccessible}">
            cannot be edited
          </c:when>
          <c:otherwise>
            <a href="#" onclick="BS.SourceDependencyForm.editDependency(event, '${dependency.sourceBuildTypeId}'); Event.stop(event)" showdiscardchangesmessage="false">edit</a>
          </c:otherwise>
        </c:choose>
      </td>
      <td class="edit">
        <c:choose>
          <c:when test="${dependency.inherited and not dependency.redefined}"><span title="Inherited dependencies cannot be deleted">undeletable</span></c:when>
          <c:otherwise>
            <a href="#" onclick="BS.SourceDependencyForm.removeDependencies(['${dependency.sourceBuildTypeId}'], ${dependency.redefined ? '\'reset\'' : '\'remove\''}); return false" showdiscardchangesmessage="false">${dependency.redefined ? 'reset' : 'delete'}</a>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
  </c:forEach>
</l:tableWithHighlighting>
</c:if>
</form>

<table class="dependencyActions">
  <tr>
    <c:set var="buildTypesForAddDependency" value="${sourceDependenciesBean.buildTypesForAddDependency}"/>
    <c:if test="${not empty buildTypesForAddDependency}">
    <td>
      <forms:addButton onclick="BS.SourceDependencyForm.addDependency(event); return false" showdiscardchangesmessage="false">Add new snapshot dependency</forms:addButton>
    </td>
    </c:if>
    <c:if test="${not empty sourceDependenciesBean.visibleDependencies}">
    <td class="moreActions">
      <bs:simplePopup controlId="snapshotDepsActions" linkOpensPopup="true"
                      popup_options="delay: 0, shift: {x: -50, y: 20}, className: 'quickLinksMenuPopup'">
        <jsp:attribute name="content">
          <ul class="menuList">
            <l:li onclick="if (BS.SourceDependencyForm.dependenciesSelected()) BS.SourceDependencyForm.addDependency(event, BS.SourceDependencyForm.getSelectedDeps());">
              <a href="#" onclick="return false">Set dependencies options</a>
            </l:li>
            <c:set var="removeOnClick">if (BS.SourceDependencyForm.dependenciesSelected()) BS.SourceDependencyForm.removeDependencies(BS.SourceDependencyForm.getSelectedDeps(), ${buildForm.templateBased ? '\'remove / reset\'' : '\'remove\''});</c:set>
            <l:li onclick="${removeOnClick}">
              <a href="#" onclick="return false">Remove${buildForm.templateBased ? ' / Reset' : ''} selected dependencies</a>
            </l:li>
          </ul>
        </jsp:attribute>
        <jsp:body>More Actions</jsp:body>
      </bs:simplePopup>
    </td>
    </c:if>
  </tr>
</table>

<bs:refreshable containerId="sourceEditingDependencyDialog" pageUrl="${pageUrl}&snapshotDepsDialog=true">
<c:url var="action" value='/admin/editDependencies.html?id=${buildForm.settingsId}'/>
<c:set var="takeStartedBuildOptionParam">option:${takeStartedBuildOption}</c:set>
<c:set var="takeSuccessfulBuildsOnlyOptionParam">option:${takeSuccessfulBuildsOnlyOption}</c:set>
<c:set var="runBuildIfDependencyFailedOptionParam">option:${runBuildIfDependencyFailed}</c:set>
<c:set var="runBuildOnTheSameAgetOptionParam">option:${runBuildOnTheSameAget}</c:set>
<c:set var="selectedBuildTypes" value="${sourceDependenciesBean.selectedBuildTypes}"/>
<c:set var="dialogTitle">
  <c:choose>
    <c:when test="${sourceDependenciesBean.addDependencyMode and empty selectedBuildTypes}">Add New Snapshot Dependency</c:when>
    <c:when test="${sourceDependenciesBean.addDependencyMode and not empty selectedBuildTypes}">Set Dependencies Options</c:when>
    <c:when test="${sourceDependenciesBean.editDependencyMode}">Edit Snapshot Dependency</c:when>
  </c:choose>
</c:set>
<c:set var="editingDep" value="${sourceDependenciesBean.editingDependency}"/>
<bs:modalDialog formId="sourceDependencies"
                action="${action}"
                title="${dialogTitle}"
                closeCommand="BS.SourceDependencyForm.close()"
                saveCommand="BS.SourceDependencyForm.saveDependency()">
  <table style="width: 100%;">
    <tr>
    <c:choose>
      <c:when test="${not empty selectedBuildTypes}">
      <th style="width: 30%">
        <label for="srcDependOn">Selected dependencies:</label>
      </th>
      </c:when>
      <c:otherwise>
      <th style="width: 20%">
        <label for="srcDependOn">Depend on:</label>
      </th>
      </c:otherwise>
    </c:choose>
      <td>
        <c:choose>
          <c:when test="${sourceDependenciesBean.addDependencyMode}">
            <c:choose>
              <c:when test="${not empty selectedBuildTypes}">
                <c:forEach items="${selectedBuildTypes}" var="bt">
                  <bs:buildTypeLinkFull buildType="${bt}" /><br/>
                  <input type="hidden" name="srcDependOn" value="${bt.buildTypeId}"/>
                </c:forEach>
              </c:when>
              <c:otherwise>
                <bs:inplaceFilter containerId="srcDependOn" activate="false" filterText="&lt;filter build configurations>"/>
                <select name="srcDependOn" id="srcDependOn" multiple="multiple" size="10" style="width: 100%">
                  <c:if test="${param['snapshotDepsDialog'] == 'true'}">
                  <c:forEach items="${buildTypesForAddDependency}" var="entry">
                    <optgroup class="inplaceFiltered" label="${entry.key.name}">
                      <c:forEach var="buildType" items="${entry.value}">
                        <c:if test="${buildForm.template or (not buildForm.template and buildForm.settingsBuildType.buildTypeId != buildType.buildTypeId)}">
                          <forms:option value="${buildType.buildTypeId}"
                                        className="inplaceFiltered"
                                        title="${entry.key.name} :: ${buildType.name}"><c:out value="${buildType.name}"/></forms:option>
                        </c:if>
                      </c:forEach>
                    </optgroup>
                  </c:forEach>
                  </c:if>
                </select>
              </c:otherwise>
            </c:choose>
          </c:when>
          <c:otherwise>
            <forms:select name="srcDependOn" id="srcDependOn" disabled="${editingDep.inherited}" enableFilter="true">
              <c:if test="${param['snapshotDepsDialog'] == 'true'}">
              <c:forEach items="${sourceDependenciesBean.availableBuildTypes}" var="entry">
                <optgroup class="inplaceFiltered" label="${entry.key.name}">
                  <c:forEach var="buildType" items="${entry.value}">
                    <forms:option value="${buildType.buildTypeId}"
                                  className="inplaceFiltered"
                                  title="${entry.key.name} :: ${buildType.name}"
                                  selected="${editingDep.sourceBuildTypeId == buildType.id}"><c:out value="${buildType.name}"/></forms:option>
                  </c:forEach>
                </optgroup>
              </c:forEach>
              </c:if>
            </forms:select>
            <c:if test="${editingDep.inherited}"><span class="smallNote" style="margin-left: 0;">Build configuration of the inherited dependency can be changed in template only.</span></c:if>
          </c:otherwise>
        </c:choose>
      </td>
    </tr>
    <tr>
      <th>Options:</th>
      <td>
        <div>
          <forms:checkbox name="${takeStartedBuildOptionParam}" checked="${editingDep.setOptions[takeStartedBuildOption]}"
                          onclick="if (!this.checked) $('${takeSuccessfulBuildsOnlyOptionParam}').checked = false;"/>
          <label for="${takeStartedBuildOptionParam}">Do not run new build if there is a suitable one</label>
        </div>
        <div style="margin-left: 1.5em">
          <forms:checkbox name="${takeSuccessfulBuildsOnlyOptionParam}" checked="${editingDep.setOptions[takeSuccessfulBuildsOnlyOption]}"
                          onclick="if (this.checked && !$('${takeStartedBuildOptionParam}').checked) $('${takeStartedBuildOptionParam}').click();"/>
          <label for="${takeSuccessfulBuildsOnlyOptionParam}">Only use successful builds from suitable ones</label>
        </div>
        <div>
          <forms:checkbox name="${runBuildIfDependencyFailedOptionParam}" checked="${editingDep.setOptions[runBuildIfDependencyFailed]}"/>
          <label for="${runBuildIfDependencyFailedOptionParam}">Run build even if dependency has failed</label>
        </div>
        <div>
          <forms:checkbox name="${runBuildOnTheSameAgetOptionParam}" checked="${editingDep.setOptions[runBuildOnTheSameAget]}"/>
          <label for="${runBuildOnTheSameAgetOptionParam}">Run build on the same agent</label>
        </div>
      </td>
    </tr>
  </table>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.SourceDependencyForm.close()"/>
    <forms:submit label="Save"/>
    <forms:saving id="addSourceDependencyProgress"/>
  </div>
  <c:choose>
    <c:when test="${artifactDependenciesBean.addDependencyMode}">
      <input type="hidden" name="saveSourceDependency" value="add"/>
    </c:when>
    <c:otherwise>
      <input type="hidden" name="saveSourceDependency" value="edit:${editingDep.sourceBuildTypeId}"/>
    </c:otherwise>
  </c:choose>
</bs:modalDialog>
</bs:refreshable>