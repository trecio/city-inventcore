<%@include file="/include-internal.jsp"%>
<%--@elvariable id="project" type="jetbrains.buildServer.serverSide.SProject"--%>

<c:set var="clickHandler">BS.openUrl(event, '<admin:editProjectLink projectId="${project.projectId}"
                                                                    title="Edit project"
                                                                    withoutLink="true"/>');</c:set>
<l:tableWithHighlighting id="project${project.projectId}"
                         className="projectTable"
                         mouseovertitle="Click to edit build configuration">
  <bs:trimWhitespace>
    <tr class="project">
      <td class="highlight noRightBorder" onclick="${clickHandler}" title="Click to edit project">
        <span class="projectName"><c:out value="${project.name}"/></span>
        <c:if test="${not empty project.description}">
          <div class="smallNote" style="margin-left: 0;"><c:out value="${project.description}"/></div>
        </c:if>
      </td>
      <authz:authorize allPermissions="CREATE_CLONE_PROJECT">
        <jsp:attribute name="ifAccessGranted">
          <td class="edit highlight" onclick="${clickHandler}" title="Click to edit project">
            <admin:editProjectLink projectId="${project.projectId}"
                                   title="Click to edit project">edit</admin:editProjectLink>
          </td>
          <td class="edit" style="vertical-align: top;">
            <admin:projectActions project="${project}"/>
          </td>
        </jsp:attribute>
        <jsp:attribute name="ifAccessDenied">
          <td class="highlight noRightBorder" onclick="${clickHandler}" title="Click to edit project">&nbsp;</td>
          <td class="edit highlight">
            <admin:editProjectLink projectId="${project.projectId}"
                                   title="Click to edit project">edit</admin:editProjectLink>
          </td>
        </jsp:attribute>
      </authz:authorize>
    </tr>

    <c:forEach items="${project.buildTypes}" var="buildType">
      <c:set var="clickHandler">BS.openUrl(event, '<admin:editBuildTypeLink buildTypeId="${buildType.buildTypeId}"
                                                                            cameFromUrl="${encodedCameFrom}"
                                                                            withoutLink="true"/>')</c:set>
      <tr class="buildConfigurationRow">
        <authz:editBuildTypeGranted buildType="${buildType}">
          <jsp:attribute name="ifAccessDenied">
            <td class="buildConfiguration noRightBorder" colspan="3">
              <c:out value="${buildType.name}"/>
              <bs:smallNote><c:out value="${buildType.description}"/></bs:smallNote>
              <admin:buildTypeTemplateInfo buildType="${buildType}"/>
            </td>
          </jsp:attribute>
          <jsp:attribute name="ifAccessGranted">
            <td class="buildConfiguration highlight noRightBorder" onclick="${clickHandler}">
              <admin:buildTypeTemplateInfo buildType="${buildType}"/>
              <c:out value="${buildType.name}"/>
              <bs:smallNote><c:out value="${buildType.description}"/></bs:smallNote>
            </td>
            <td class="edit highlight" onclick="${clickHandler}" style="vertical-align: top;">
              <admin:editBuildTypeMenu buildType="${buildType}"
                                       cameFromUrl="${encodedCameFrom}">edit</admin:editBuildTypeMenu>
            </td>
            <td class="edit" style="vertical-align: top;">
              <admin:buildTypeActions buildType="${buildType}"
                                      editableProjects="${editableProjects}"/>
            </td>
          </jsp:attribute>
        </authz:editBuildTypeGranted>

      </tr>
    </c:forEach>
  </bs:trimWhitespace>
</l:tableWithHighlighting>
<p>
  <admin:createBuildTypeLink projectId="${project.projectId}"
                             cameFromUrl="${encodedCameFrom}">Create build configuration</admin:createBuildTypeLink>
  <admin:bcLeft/>
</p>
<br/>
