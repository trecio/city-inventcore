<%--@elvariable id="serverTC" type="jetbrains.buildServer.serverSide.SBuildServer"--%>
<%@ page import="jetbrains.buildServer.web.util.WebUtil" %>
<%@
    include file="/include-internal.jsp" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin"

%><jsp:useBean id="adminOverviewForm" type="jetbrains.buildServer.controllers.admin.AdminOverviewForm" scope="request"
/><jsp:useBean id="pageUrl" type="java.lang.String" scope="request"
/><c:set var="encodedCameFrom" value="<%=WebUtil.encode(pageUrl)%>"
/><c:set var="activeProjectsNum" value="${adminOverviewForm.totalNumberOfProjects - adminOverviewForm.numberOfArchivedProjects}"
/><c:set var="archivedProjectsNum" value="${adminOverviewForm.numberOfArchivedProjects}"/>

<div id="container" class="clearfix">
  <c:set var="editableProjects" value="${adminOverviewForm.allActiveProjects}"/>

  <c:if test="${activeProjectsNum > 0}">
  <authz:authorize allPermissions="CREATE_CLONE_PROJECT">
    <c:url value='/admin/createProject.html?init=1' var="createUrl"/>
    <p class="createProject"><forms:addButton href="${createUrl}">Create project</forms:addButton></p>
  </authz:authorize>
  </c:if>

  <c:if test="${activeProjectsNum > 0}">
    <c:set var="activeBuildConfsNum" value="${adminOverviewForm.totalNumberOfBuildConfigurations - adminOverviewForm.numberOfArchivedConfigurations}"/>
    <div class="descr">
      You have <strong>${activeProjectsNum}</strong> active project<bs:s val="${activeProjectsNum}"/>
      with <strong>${activeBuildConfsNum}</strong> build configuration<bs:s val="${activeBuildConfsNum}"
      /><c:set var="archivedNum" value="${adminOverviewForm.numberOfArchivedConfigurations}"
      /><c:if test="${archivedNum > 0}"
       > (plus <strong>${archivedNum}</strong> archived build configuration<bs:s val="${archivedNum}"/>)</c:if>.
      <bs:professionalFeature>
        <br/>
        <span>
          In TeamCity Professional you can have maximum of
          <strong>${serverTC.licensingPolicy.maxNumberOfBuildTypes}</strong> Build Configurations (active and archived).
        </span>
        <bs:help file="TeamCity+Editions"/>
      </bs:professionalFeature>
    </div>
  </c:if>
  <c:if test="${activeProjectsNum == 0}">
    <div class="descr">There are no active projects.</div>
    <authz:authorize allPermissions="CREATE_CLONE_PROJECT">
      <c:url value='/admin/createProject.html?init=1' var="createUrl"/>
      <p><forms:addButton href="${createUrl}">Create project</forms:addButton></p>
    </authz:authorize>
  </c:if>

  <bs:messages key="projectRemoved" style="text-align: left;"/>
  <bs:messages key="buildTypeRemoved" style="text-align: left;"/>
  <bs:messages key="buildTypeNotFound" style="text-align: left;"/>

  <c:if test="${adminOverviewForm.numberOfActiveProjects > 0}">
    <admin:projectsFilter adminOverviewForm="${adminOverviewForm}" tab="active"/>
    <c:forEach items="${adminOverviewForm.activeProjects}" var="project">
      <%@ include file="project.jsp" %>
    </c:forEach>
    <bs:pager place="bottom" urlPattern="admin.html?page=[page]&keyword=${adminOverviewForm.keyword}" pager="${adminOverviewForm.pager}"/>
  </c:if>

  <admin:copyBuildTypeForm editableProjects="${editableProjects}"/>
  <admin:moveBuildTypeForm editableProjects="${editableProjects}"/>
  <admin:copyProjectForm/>
</div>

<script type="text/javascript">
  (function($) {
    var link = $("<a href='#'>Install Build Agents</a>").addClass("quickLinksControlLink").click(function() {
      BS.InstallAgentsPopup.showNearElement(this);
      return false;
    });
    $("#topWrapper div.quickLinks").append(link);
  })(jQuery);
</script>
<l:popupWithTitle id="installAgents" title="Install Build Agents">
  <%@ include file="../installLinks.jspf" %>
</l:popupWithTitle>
