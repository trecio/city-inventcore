<%@ include file="/include-internal.jsp"

%><jsp:useBean id="adminOverviewBean" scope="request" type="jetbrains.buildServer.controllers.admin.AdminOverviewController.AdminOverviewBean"
/><bs:page disableScrollingRestore="true">
  <jsp:attribute name="head_include">
    <script type="text/javascript">
      BS.Navigation.items = [
        { title: "Administration",
          url: "<c:url value="/admin/admin.html"/>",
          selected: false
        },
        { title: "${adminOverviewBean.selectedExtension.tabTitle}",
          url: "",
          selected: true
        }
      ];
      (function() {
        var tabTitle = '<c:out value="${adminOverviewBean.selectedExtension.tabTitle}"/> -- TeamCity';

        if (document.title.indexOf(tabTitle) == -1) {
          document.title = document.title.replace(/TeamCity$/, tabTitle);
        }
      })();
    </script>
    <bs:linkCSS>
      /css/admin/adminMain.css
      /css/admin/serverConfig.css
      /css/pager.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/bs/serverConfig.js
    </bs:linkScript>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <table id="admin-container">
      <tr>
        <td class="admin-sidebar">
          <ext:showSidebar extensions="${adminOverviewBean.extensions}"
                           selectedExtension="${adminOverviewBean.selectedExtension}"
                           urlPrefix="/admin/admin.html"/>
        </td>
        <td class="admin-content">
          <div id="tabsContainer4" class="simpleTabs clearfix"></div>
          <ext:includeExtension extension="${adminOverviewBean.selectedExtension}"/>
        </td>
      </tr>
    </table>
  </jsp:attribute>
</bs:page>