<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="profile" tagdir="/WEB-INF/tags/userProfile"%>

<jsp:useBean id="adminEditUserForm" type="jetbrains.buildServer.controllers.admin.users.AdminEditUserForm" scope="request"/>
<l:defineCurrentTab defaultTab="${afn:permissionGrantedGlobally('CHANGE_USER') and not adminEditUserForm.guestUser ? 'userGeneralSettings' : 'userGroups'}"/>
<c:choose>
  <c:when test="${currentTab == 'userGeneralSettings'}"><c:set var="pageTitle" scope="request" value="Edit General Settings of ${adminEditUserForm.editee.descriptiveName}"/></c:when>
  <c:when test="${currentTab == 'userRoles'}"><c:set var="pageTitle" scope="request" value="Edit Roles of ${adminEditUserForm.editee.descriptiveName}"/></c:when>
  <c:when test="${currentTab == 'userNotifications'}"><c:set var="pageTitle" scope="request" value="Edit Notification Rules of ${adminEditUserForm.editee.descriptiveName}"/></c:when>
  <c:otherwise><c:set var="pageTitle" scope="request" value="View Groups of ${adminEditUserForm.editee.descriptiveName}"/></c:otherwise>
</c:choose>
<bs:page>
<jsp:attribute name="head_include">
  <bs:linkCSS>
    /css/profilePage.css
    /css/settingsBlock.css
    /css/userRoles.css
    /css/admin/adminMain.css
    /css/admin/userGroups.css
    /css/notificationRules.css
  </bs:linkCSS>
  <bs:linkScript>
    /js/bs/updateUser.js
    /js/bs/userGroups.js
    /js/bs/queueLikeSorter.js
    /js/bs/notificationRules.js
  </bs:linkScript>
  <script type="text/javascript">
    BS.Navigation.items = [
      {title: "Administration", url: '<c:url value="/admin/admin.html"/>'},
      <forms:cameBackNav cameFromSupport="${adminEditUserForm.cameFromSupport}"/>
      {title: '<bs:escapeForJs text="${adminEditUserForm.editee.descriptiveName}" forHTMLAttribute="true"/>', selected:true}
    ];
  </script>
</jsp:attribute>

<jsp:attribute name="body_include">
  <script type="text/javascript">
    (function() {
      var paneNav = new TabbedPane();

      function initialize3dLevel() {
        var baseUrl = "<c:url value='/admin/editUser.html?init=1&userId=${adminEditUserForm.editee.id}&tab='/>";

      <c:if test="${not adminEditUserForm.guestUser and afn:permissionGrantedGlobally('CHANGE_USER')}">
        paneNav.addTab("userGeneralSettings", {
          caption: "General",
          titleText: "General",
          url: baseUrl + "userGeneralSettings"
        });
      </c:if>

        paneNav.addTab("userGroups", {
          caption: "Groups",
          titleText: "Groups",
          url: baseUrl + "userGroups"
        });

      <c:if test="${adminEditUserForm.perProjectPermissionsEnabled}">
        paneNav.addTab("userRoles", {
          caption: "Roles",
          titleText: "Roles",
          url: baseUrl + "userRoles"
        });
      </c:if>

      <c:if test="${not adminEditUserForm.guestUser}">
        paneNav.addTab("userNotifications", {
          caption: "Notification Rules",
          titleText: "Notification Rules",
          url: baseUrl + "userNotifications"
        });
      </c:if>

        paneNav.setActiveCaption('${currentTab}');
      }

      initialize3dLevel();

      paneNav.showIn('tabsContainer3');
    })();
  </script>

  <div id="container" class="clearfix">

    <jsp:include page="${currentTab}.jsp"/>

  </div>

</jsp:attribute>

</bs:page>

