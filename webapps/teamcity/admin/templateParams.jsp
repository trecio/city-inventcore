<%@include file="/include-internal.jsp"%>
<jsp:useBean id="template" scope="request" type="jetbrains.buildServer.serverSide.BuildTypeTemplate"/>
<jsp:useBean id="paramNames" scope="request" type="java.util.Collection"/>
<jsp:useBean id="templateParameters" scope="request" type="java.util.Map"/>
<c:if test="${not empty paramNames}">
  <table class="runnerFormTable">
    <tr>
      <td colspan="2" class="name noBorder">Please review the parameters:</td>
    </tr>
  <c:forEach items="${paramNames}" var="paramName">
    <c:set var="paramId" value="${template.id}_${paramName}"/>
    <tr>
      <td class="name"><label for="${paramId}"><bs:makeBreakable text="${paramName}" regex="[._:-]+"/>:</label></td>
      <td class="value">
        <div class="completionIconWrapper">
          <forms:textField name="param:${paramName}" id="${paramId}" value="${templateParameters[paramName]}" style="width: 100%;" className="buildTypeParams" expandable="true"/>
        </div>
      </td>
    </tr>
  </c:forEach>
  </table>
  <script type="text/javascript">
    BS.AvailableParams.attachPopups("settingsId=template:${template.id}");
  </script>
</c:if>
