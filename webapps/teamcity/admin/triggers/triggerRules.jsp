<%@ include file="/include.jsp" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<jsp:useBean id="propertiesBean" type="jetbrains.buildServer.controllers.BasePropertiesBean" scope="request"/>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<l:settingsGroup title="VCS Trigger Rules"/>
<tr>
  <th style="vertical-align: top;">
    <label for="triggerRules" class="rightLabel">Trigger rules:</label><bs:help file="Configuring+VCS+Triggers" anchor="buildTriggerRules"/>
  </th>
  <td style="vertical-align: top;">
    <props:multilineProperty name="triggerRules" linkTitle="Edit Trigger Rules" cols="35" rows="5"/>
    <span class="error" id="error_triggerRules"></span>
    <span class="smallNote">Newline-delimited set of rules: +|-:[Ant-like wildcard]</span>

    <forms:addButton id="addNewRule" onclick="BS.AddTriggerRuleDialog.showDialog(); return false">Add new rule</forms:addButton>
    <style type="text/css">
    #addTriggerRuleDialog {
      width: 36em;
    }

    #addTriggerRuleDialog table {
      width: 98%;
    }

    #addTriggerRuleDialog table th {
      width: 30%;
      font-weight:normal;
    }

    #addTriggerRuleDialog table td, #addTriggerRuleDialog table th {
      padding: 5px;
    }
    </style>
    <bs:dialog dialogId="addTriggerRuleDialog" title="Add Trigger Rule" closeCommand="BS.AddTriggerRuleDialog.close()">
    <table class="runnerFormTable">
      <l:settingsGroup title="Rule Type" mandatory="true"/>
      <tr>
        <th colspan="2"><forms:radioButton name="triggerBuild" id="triggerBuild1" value="true"/> <label for="triggerBuild1">Trigger build</label></th>
      </tr>
      <tr>
        <th colspan="2"><forms:radioButton name="triggerBuild" id="triggerBuild2" value="false"/> <label for="triggerBuild2">Do not trigger build</label></th>
      </tr>
      <l:settingsGroup title="Condition"/>
      <tr>
        <th><label for="username">VCS username:</label></th>
        <td><forms:textField name="username" style="width: 98%" className="buildTypeParams"/></td>
      </tr>
      <tr>
        <th><label for="vcsRoot">VCS root:</label></th>
        <td>
          <select name="vcsRoot" id="vcsRoot" style="width: 98%">
            <forms:option value="">&lt;any VCS root&gt;</forms:option>
            <c:forEach items="${buildForm.vcsRootsBean.vcsRoots}" var="vcsRoot">
              <forms:option value="${vcsRoot.vcsRoot.name}"><c:out value="${vcsRoot.vcsRoot.name}"/></forms:option>
            </c:forEach>
          </select>
        </td>
      </tr>
      <tr>
        <th><label for="comment">VCS comment:</label></th>
        <td><forms:textField name="comment" style="width: 98%" className="buildTypeParams" defaultText="<comment regexp>"/></td>
      </tr>
      <tr>
        <th><label for="wildcard">Wildcard:</label> <l:star/></th>
        <td><forms:textField name="wildcard" style="width: 98%" value="**/*" className="buildTypeParams"/></td>
      </tr>
    </table>

    <div class="popupSaveButtonsBlock">
      <forms:cancel onclick="BS.AddTriggerRuleDialog.close()" showdiscardchangesmessage="false"/>
      <forms:submit type="button" label="Add Rule" onclick="BS.AddTriggerRuleDialog.submit()"/>
    </div>
    </bs:dialog>
    <script type="text/javascript">
      BS.MultilineProperties.updateVisible();
    </script>
  </td>
</tr>
