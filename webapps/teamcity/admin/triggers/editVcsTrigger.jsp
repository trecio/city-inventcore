<%@ include file="/include.jsp" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<jsp:useBean id="propertiesBean" type="jetbrains.buildServer.controllers.BasePropertiesBean" scope="request"/>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>

<c:set var="noneMode" value="${propertiesBean.properties['quietPeriodMode'] eq 'DO_NOT_USE'}"/>
<c:set var="defMode" value="${propertiesBean.properties['quietPeriodMode'] eq 'USE_DEFAULT'}"/>
<c:set var="customMode" value="${propertiesBean.properties['quietPeriodMode'] eq 'USE_CUSTOM'}"/>

<style type="text/css">
  #quietPeriodModeTable td {
    padding: 2px;
  }
</style>

<tr>
  <td colspan="2">
    <em>VCS Trigger will add a build to the queue when a VCS check-in is detected.</em>
  </td>
</tr>
<tr>
  <td colspan="2">
    <props:checkboxProperty name="watchChangesInDependencies" disabled="${not buildForm.template and empty buildForm.sourceDependenciesBean.dependencies}" onclick="checkCheckInterval();"/>
    <label for="watchChangesInDependencies" class="rightLabel">Trigger on changes in snapshot dependencies</label>
  </td>
</tr>
<l:settingsGroup title="Per-checkin Triggering"/>
<tr>
  <td colspan="2">
    <props:checkboxProperty name="perCheckinTriggering" onclick="if (!this.checked) $('groupCheckinsByCommitter').checked = false;"/>
    <label for="perCheckinTriggering" class="rightLabel">Trigger a build on each check-in</label>
    <div style="padding: 0.5em 0 0 1.5em;">
      <props:checkboxProperty name="groupCheckinsByCommitter" onclick="if (this.checked && !$('perCheckinTriggering').checked) $('perCheckinTriggering').checked = true;"/>
      <label for="groupCheckinsByCommitter" class="rightLabel">Include several check-ins in a build if they are from the same committer</label>
    </div>
  </td>
</tr>
<l:settingsGroup title="Quiet Period Settings"/>
<tr>
  <th style="vertical-align: top;">
    <label class="rightLabel">Quiet period mode:</label><bs:help file="Configuring+VCS+Triggers" anchor="QuietPeriodMode"/>
  </th>
  <td style="vertical-align: top;">
    <table style="width: 100%;" id="quietPeriodModeTable">
      <tr>
        <td>
          <props:radioButtonProperty name="quietPeriodMode" id="quietPeriod1" value="DO_NOT_USE" onclick="$('quietPeriod').disable(); checkCheckInterval(); BS.VisibilityHandlers.updateVisibility('quietPeriodModeTable')"/>
          <label for="quietPeriod1">Do not use</label>
        </td>
      </tr>
      <tr>
        <td>
        <props:radioButtonProperty name="quietPeriodMode" id="quietPeriod2" value="USE_DEFAULT" onclick="$('quietPeriod').disable(); checkCheckInterval(); BS.VisibilityHandlers.updateVisibility('quietPeriodModeTable')"/>
        <label for="quietPeriod2">Use default value (${buildForm.defaultQuietPeriod} seconds)</label>
        </td>
      </tr>
      <tr>
        <td>
        <props:radioButtonProperty name="quietPeriodMode" id="quietPeriod3" value="USE_CUSTOM" onclick="$('quietPeriod').enable(); checkCheckInterval(); BS.VisibilityHandlers.updateVisibility('quietPeriodModeTable')"/>
        <label for="quietPeriod3">Custom</label>

        <props:textProperty name="quietPeriod" maxlength="50" size="10" disabled="${not customMode}" onchange="checkCheckInterval()"/><label for="quietPeriod" class="rightLabel">seconds</label>
        <span class="error" id="error_quietPeriod"></span>
        </td>
      </tr>
      <jsp:include page="/admin/triggers/vcsCheckInterval.html"/>
    </table>
    <script type="text/javascript">
      checkCheckInterval();
    </script>
  </td>
</tr>
<jsp:include page="/admin/triggers/triggerRules.jsp"/>

