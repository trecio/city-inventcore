<%@ include file="/include.jsp" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<jsp:useBean id="dependencyTriggerBean" type="jetbrains.buildServer.controllers.admin.projects.triggers.FinishBuildTriggerBean" scope="request"/>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<jsp:useBean id="propertiesBean" type="jetbrains.buildServer.controllers.BasePropertiesBean" scope="request"/>

<tr>
  <td colspan="2"><em>Finished Build Trigger will add a build to the queue after a build finishes in the selected configuration.</em></td>
</tr>
<tr>
  <td style="vertical-align: top;">
    <label for="dependsOn">Build configuration:</label>
  </td>
  <c:set var="onchange" value=""/>
  <td style="vertical-align: top;">
    <props:selectProperty name="dependsOn" style="width: 100%;" enableFilter="true">
      <props:option value="">-- Please select a build configuration --</props:option>
      <c:forEach items="${dependencyTriggerBean.buildTypes}" var="entry">
        <optgroup label="${entry.key.name}">
          <c:forEach items="${entry.value}" var="buildType">
            <props:option value="${buildType.buildTypeId}" title="${entry.key.name} :: ${buildType.name}"><c:out value="${buildType.name}"/></props:option>
          </c:forEach>
        </optgroup>
      </c:forEach>
    </props:selectProperty>
    <span class="error" id="error_dependsOn"></span>
  </td>
</tr>
<tr>
  <td class="noBorder">&nbsp;</td>
  <td class="noBorder">
    <props:checkboxProperty name="afterSuccessfulBuildOnly" checked="${propertiesBean.properties['afterSuccessfulBuildOnly']}"/>
    <label for="afterSuccessfulBuildOnly">Trigger after successful build only</label>
  </td>
</tr>
<%--
<tr>
  <td class="noBorder">&nbsp;</td>
  <td class="noBorder">
    <props:checkboxProperty name="afterPinnedBuildOnly" checked="${propertiesBean.properties['afterPinnedBuildOnly']}"/>
    <label for="afterPinnedBuildOnly">Trigger after pinned build only</label>
  </td>
</tr>
<tr>
  <td class="noBorder">&nbsp;</td>
  <td class="noBorder">
    <props:checkboxProperty name="afterTaggedBuildOnly" checked="${propertiesBean.properties['afterTaggedBuildOnly']}" onclick="if (this.checked) { $('buildTagSection').show(); } else { $('buildTagSection').hide(); }"/>
    <label for="afterTaggedBuildOnly">Trigger after tagged build only</label>
    <div id="buildTagSection" style="margin-top: 0.5em; display: ${propertiesBean.properties['afterTaggedBuildOnly'] == 'true' ? 'block' : 'none'}">
      <label for="buildTag">Build tag:</label> <props:textProperty name="buildTag" style="width: 80%;"/>
    </div>
  </td>
</tr>
--%>
