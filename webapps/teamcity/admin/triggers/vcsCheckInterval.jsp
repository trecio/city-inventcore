<%@ include file="/include.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<tr id="checkIntervalWarning" style="display: none;">
  <td>
    <script type="text/javascript">
      window._vcsRoots = {};
      <c:forEach items="${allVcsRoots}" var="vcsRoot">
      window._vcsRoots[${vcsRoot.id}] = ${vcsRoot.modificationCheckInterval};
      </c:forEach>
      window.checkCheckInterval = function() {
        $('checkIntervalWarning').hide();
        BS.MultilineProperties.updateVisible();

        var qp =0;
        if ($('quietPeriod2').checked) {
          qp = ${buildForm.defaultQuietPeriod};
        } else if ($('quietPeriod3').checked) {
          qp = parseInt($('quietPeriod').value, 10);
        }

        var foundRoots = [];
        for (var id in window._vcsRoots) {
          var interval = window._vcsRoots[id];

          if (qp > 0 && qp <= interval) {
            foundRoots.push(id);
            $('rootId:' + id).show();
          } else {
            $('rootId:' + id).hide();
          }
        }

        if (foundRoots.length > 0) {
          $('checkIntervalWarning').show();
          BS.MultilineProperties.updateVisible();
        }
      }
    </script>

    <div class="smallNoteAttention">
      Some VCS roots have a checking for changes interval larger or the same as the specified quiet period.
      In this case it may take longer for a build to be added to the queue.
    </div>
    <ul>
      <c:forEach items="${allVcsRoots}" var="vcsRoot">
        <li id="rootId:${vcsRoot.id}" style="display: none;">
          <c:choose>
            <c:when test="${afn:canEditVcsRoot(vcsRoot)}">
              <admin:editVcsRootLink vcsRoot="${vcsRoot}" editingScope="none" cameFromUrl="${buildForm.cameFromSupport.cameFromUrl}"><c:out value="${vcsRoot.name}"/></admin:editVcsRootLink>: ${vcsRoot.modificationCheckInterval} seconds
            </c:when>
            <c:otherwise><c:out value="${vcsRoot.name}"/>: ${vcsRoot.modificationCheckInterval} seconds</c:otherwise>
          </c:choose>
        </li>
      </c:forEach>
    </ul>
  </td>
</tr>
