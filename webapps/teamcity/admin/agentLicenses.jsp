<%@include file="/include-internal.jsp"%>
<jsp:useBean id="agentLicenseData" type="jetbrains.buildServer.controllers.license.AgentLicenseData" scope="request"/>
<c:set var="currentTab" value="licenses" scope="request"/>
<lic:agentLicenses licenseData="${agentLicenseData}"/>
<script type="text/javascript">
  <c:if test="${agentLicenseData.enterKeysMode}">
    $j(document).ready(function() {
      if ($("licenseKeys")) {
        $("licenseKeys").focus();
      }
    });
  </c:if>
</script>
