<%@ include file="/include-internal.jsp"%>

<%--@elvariable id="providers" type="java.util.List"--%>
<%--@elvariable id="pageUrl" type="java.lang.String"--%>
<%--@elvariable id="types" type="java.util.List"--%>
<%--@elvariable id="pluginConfigJsp" type="java.lang.String"--%>
<%--@elvariable id="publicKey" type="java.lang.String"--%>

<bs:refreshable containerId="providersTable" pageUrl="${pageUrl}">
<div class="providersListContainer">
  <c:set var="providersCount">${fn:length(providers)}</c:set>
  <div>
    You have ${providersCount} connection<bs:s val="${providersCount}"/>.<bs:help file="Integrating+TeamCity+with+Issue+Tracker"/>
  </div>

  <c:choose>
    <c:when test="${not empty providers}">
      <l:tableWithHighlighting className="settings" id="providers"
                               mouseovertitle="Click to edit connection" highlightImmediately="true">
        <thead>
          <tr>
            <th colspan="3">Issue tracker connections</th>
          </tr>
        </thead>
        <tbody>
          <c:forEach items="${providers}" var="provider">
            <%--@elvariable id="provider"
                            type="jetbrains.buildServer.issueTracker.SIssueProvider"--%>
            <c:set var="type" value="${provider.type}"/>
            <c:set var="name" value="${provider.name}"/>
            <c:set var="id" value="${provider.id}"/>
            <c:set var="onclick" value="BS.EditIssueProviderForm.show('${type}', '${id}');"/>
            <tr>
              <td class="highlight noRightBorder" onclick="${onclick}">
                <em>${type}:</em> <c:out value='${name}'/>
                <c:if test="${not empty provider.configurationSummary}">
                  <span class="providerSummary">(${provider.configurationSummary})</span>
                </c:if>
              </td>
              <td class="highlight edit" onclick="${onclick}">
                <a href="#" onclick="${onclick}; Event.stop(event)">edit</a>
              </td>
              <td class="edit">
                <a href="#" onclick="BS.AdminActions.deleteIssueProvider('${id}'); return false">
                  delete
                </a>
              </td>
            </tr>
          </c:forEach>
        </tbody>
      </l:tableWithHighlighting>
    </c:when>
  </c:choose>

  <p>
    <forms:addButton onclick="BS.IssueProviderForm.showDialog(); return false">Create new connection</forms:addButton>
  </p>
</div>
</bs:refreshable>

<bs:modalDialog formId="newIssueProviderForm"
                title="Create New Issue Tracker Connection"
                action="#"
                closeCommand="BS.IssueProviderForm.close();"
                saveCommand="BS.IssueProviderForm.submit();">
  <table class="editProviderTable">
    <tr>
      <th><label for="providerType" class="shortLabel">Connection Type:</label></th>
      <td>
        <select name="providerType" id="providerType"
                onchange="BS.IssueProviderForm.refreshDialog('${pageUrl}');">
          <option value="" id="noType">--- Choose the type ---</option>
          <c:forEach items="${types}" var="type">
            <option value="${type}">${type}</option>
          </c:forEach>
        </select>
        <forms:saving id="newProviderSaving" className="progressRingInline"/>
      </td>
    </tr>
  </table>

  <bs:refreshable containerId="newIssueProviderContainer" pageUrl="">
    <div id="newProviderDiv">
      <c:if test="${not empty pluginConfigJsp}">
        <jsp:include page="${pluginConfigJsp}"/>
      </c:if>
    </div>
    <input type="hidden" id="publicKey" name="publicKey" value="${publicKey}"/>
  </bs:refreshable>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.IssueProviderForm.cancelDialog()" showdiscardchangesmessage="false"/>
    <forms:submit label="Create" id="createButton"/>
    <forms:submit label="Test connection" id="connectionButton"
           onclick="BS.IssueProviderForm.testConnection(); return false;"/>
    <forms:saving id="newIssueProviderProgress"/>
  </div>
</bs:modalDialog>

<bs:modalDialog formId="editIssueProviderForm"
                title="Edit Issue Tracker Connection"
                action="#"
                closeCommand="BS.EditIssueProviderForm.close();"
                saveCommand="BS.EditIssueProviderForm.submit();">
  <bs:refreshable containerId="editIssueProviderContainer" pageUrl="">
    <div id="editProviderDiv"></div>
    <input type="hidden" id="publicKey" name="publicKey" value="${publicKey}"/>
  </bs:refreshable>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.EditIssueProviderForm.cancelDialog()" showdiscardchangesmessage="false"/>
    <forms:submit label="Save" id="saveProvider"/>
    <forms:submit label="Test connection" id="connectionButton"
           onclick="BS.EditIssueProviderForm.testConnection(); return false;"/>
    <forms:saving id="editIssueProviderProgress"/>
  </div>
</bs:modalDialog>

<bs:modalDialog formId="testConnection"
                title="Test connection"
                action="#"
                closeCommand="BS.IssueProviderTestConnection.cancelDialog();"
                saveCommand="">

  <label for="issueId" class="tableLabel" style="width: 6em;">Issue id:</label>
  <forms:textField id="issueId" value="" defaultText="" name="issueId"/>
  <div class="clr"></div>

  <div id="issueDetails"></div>
  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.IssueProviderTestConnection.cancelDialog()"/>
    <forms:submit label="Test"
           onclick="BS.IssueProviderTestConnection.testConnection(); return false;"/>
  </div>
</bs:modalDialog>
