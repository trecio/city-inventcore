<%@ page import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="admfn" uri="/WEB-INF/functions/admin" %>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<jsp:useBean id="pageUrl" type="java.lang.String" scope="request"/>
<c:set var="vcsRootsBean" value="${buildForm.vcsRootsBean}"/>
<admin:editBuildTypePage selectedStep="vcsRoots">
  <jsp:attribute name="head_include">
    <bs:linkScript>
      /js/bs/editCheckoutRules.js
    </bs:linkScript>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <forms:modified/>
    <bs:refreshable containerId="vcsRootsBody" pageUrl="${pageUrl}">
      <c:url value='/admin/editBuildTypeVcsRoots.html?id=${buildForm.settingsId}' var="formAction"/>
      <form id="editVcsSettingsForm" action="${formAction}" method="post" onsubmit="return BS.EditVcsRootsForm.applyVcsSettings()" class="clearfix">

      <admin:buildTypeVcsRootsForm vcsRootsBean="${buildForm.vcsRootsBean}"
                                   mode="${buildForm.template ? 'editTemplate' : 'editBuildType'}"
                                   pageUrl="${pageUrl}"/>

      </form>
      <admin:editCheckoutRulesForm formAction="${formAction}"/>
      <script type="text/javascript">
        BS.EditVcsRootsForm.setModified(${buildForm.vcsRootsBean.stateModified});
        BS.EditVcsRootsForm.setupEventHandlers();
        BS.AvailableParams.attachPopups('settingsId=${buildForm.settingsId}', 'buildTypeParams');

      <c:if test="${buildForm.templateBased}">
        BS.EditVcsRootsForm.setTemplateBased([{name: 'vcsRootId', className: 'labelCheck'}, {className: 'submitButton'}], true);
      </c:if>
      </script>
    </bs:refreshable>
  </jsp:attribute>
</admin:editBuildTypePage>
