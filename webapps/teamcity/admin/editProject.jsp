<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %>
<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="currentProject" type="jetbrains.buildServer.serverSide.SProject" scope="request"/>
<jsp:useBean id="pageUrl" type="java.lang.String" scope="request"/>
<c:set var="pageTitle" value="${currentProject.name} Project" scope="request"/>
<bs:page>
<jsp:attribute name="head_include">
  <bs:linkCSS>
    /css/project.css
    /css/admin/adminMain.css
    /css/admin/projectForm.css
  </bs:linkCSS>
  <style type="text/css">
    td.name, th.name {
      width: 50%;
    }
  </style>
  <bs:linkScript>
    /js/bs/blocks.js
    /js/bs/blocksWithHeader.js
    /js/bs/copyProject.js
    /js/bs/moveBuildType.js
    /js/bs/editProject.js
    /js/bs/testConnection.js
  </bs:linkScript>
  <script type="text/javascript">
    BS.Navigation.items = [
        {title: "Administration", url: '<c:url value="/admin/admin.html"/>'},
        {title: "<bs:escapeForJs text="${pageTitle}" forHTMLAttribute="true"/>", selected:true}
    ];

  </script>
</jsp:attribute>

<jsp:attribute name="quickLinks_include">
    <bs:projectLink project="${currentProject}" style="float:left;">Project Home</bs:projectLink>
</jsp:attribute>

<jsp:attribute name="body_include">
  <bs:projectArchived project="${currentProject}" style="margin: 5px 0 5px 0;"/>
  <div id="projectTabsContainer" class="simpleTabs clearfix"></div>
  <div id="container">
    <ext:showTabs placeId="<%=PlaceId.EDIT_PROJECT_PAGE_TAB%>" urlPrefix="/admin/editProject.html?projectId=${currentProject.projectId}" tabContainerId="projectTabsContainer"/>
  </div>

</jsp:attribute>

</bs:page>

