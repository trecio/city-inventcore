<%@include file="/include-internal.jsp"%>
<jsp:useBean id="emailSettings" scope="request" type="jetbrains.buildServer.controllers.email.SettingsBean"/>

<h2>Comment Transformation Patterns</h2>
<p>
  <table id="commentTransformationPatterns" class="settings">
  <tr style="background-color: rgb(245, 245, 245);">
  <th class="name">description</th>
  <th class="name">search regex</th>
  <th class="name">replace regex</th>
  </tr>
  <tbody>
    <%--@elvariable id="serverTC" type="jetbrains.buildServer.serverSide.impl.BuldServerImpl"--%>
    <c:forEach var="pattern" items="${serverTC.commentTransformer.patterns}">
    <tr>
      <td class="name">${pattern.description}</td>
      <td class="name">${fn:escapeXml(pattern.search)}</td>
      <td class="name">${fn:escapeXml(pattern.replace)}</td>
    </tr>
    </c:forEach>
  </tbody>
  </table>
</p>

<div class="clr"></div>


<script>
BS.EditTransformationPatternDialog = OO.extend(BS.AbstractModalDialog, {
  getContainer: function() {
    return $('editTSDialog');
  },

  showDialog: function(elem, name, value, systemProperty) {
    $('currentName').value = name;

    var title = name.length == 0 ? "Add New" : "Edit";
    if (systemProperty == 'true') {
      title += " Property";
    } else {
      title += " Variable";
    }

    $('parameterDialogTitle').innerHTML = title;

    this.showAtFixed($(this.getContainer()));

    $('parameterName').focus();
  },

  cancelDialog: function() {
    this.close();
  }
});
</script>



<div id="editTSDialog" class="editParameterDialog modalDialog">
  <div class="dialogHeader">
    <div class="closeWindow">
      <a title="Close dialog window" href="#" showdiscardchangesmessage="false"
         onclick="BS.EditTransformationPatternDialog.cancelDialog(); return false"><img src='<c:url value="/img/close.gif"/>'/></a>
    </div>
    <div class="dialogHandle">
      <h3 id="parameterDialogTitle" class="dialogTitle"></h3>
    </div>
  </div>

  <div class="modalDialogBody">
    <form id='buildParamsForm' action="<c:url value='/admin/editBuildParams.html?buildTypeId=${buildType.buildTypeId}'/>"
          method="post">
      <label class="editParameterLabel" for="parameterName">Name: <l:star/></label>
      <forms:textField name="parameterName" value="" style="width:22em;" maxlength="512"/>
      <span class="error" id="error_parameterName" style="margin-left: 5.5em;"></span>

      <div class="clr spacing"></div>

      <label class="editParameterLabel" for="parameterValue">Value:</label>
      <forms:textField name="parameterValue" value="" style="width:22em;" maxlength="512"/>

      <div class="popupSaveButtonsBlock">
        <forms:cancel showdiscardchangesmessage="false" onclick="BS.EditTransformationPatternDialog.cancelDialog()"/>
        <forms:submit label="Save"/>
        <forms:saving id='buildParamsSaving'/>
      </div>

      <input type="hidden" id="systemProperty" name="systemProperty" value=""/>
      <input type="hidden" id="currentName" name="currentName" value=""/>
      <input type="hidden" id="submitAction" name="submitAction" value=""/>
      <input type="hidden" id="submitBuildType1" name="submitBuildType" value="1"/>
    </form>
  </div>
</div>
