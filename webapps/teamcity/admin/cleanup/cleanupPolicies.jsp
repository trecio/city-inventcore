<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<c:set var="pageTitle" value="Build History Clean-up Policy" scope="request"/>
<jsp:useBean id="now" class="java.util.Date"/>
<jsp:useBean id="cleanupPoliciesForm"
             type="jetbrains.buildServer.controllers.admin.cleanup.CleanupPoliciesForm" scope="request"/>
<jsp:useBean id="preventDependenciesFromCleanupOptions" type="java.util.Map" scope="request"/>

<h2>Clean-up process settings <bs:help file="Clean-Up"/></h2>

<table class="runnerFormTable">
  <tr>
    <th>Clean-up process:</th>
    <td>
      <%@ include file="_cleanupEnableDisable.jspf" %>
    </td>
  </tr>
</table>

<c:if test="${cleanupPoliciesForm.cleanupEnabled}">
  <%@ include file="_cleanupPoliciesForm.jspf" %>
  <h2>Configure clean-up rules for specific build configurations</h2>
  <%--@elvariable id="pageUrl" type="javaUtilString"--%>
  <bs:refreshable containerId="cleanupPoliciesTable" pageUrl="${pageUrl}">
    <bs:messages key="policyRemoved"/>
    <bs:messages key="policiesUpdated"/>
    <%@ include file="_cleanupDefaultPolicy.jspf" %>
    <br/>
    <%@ include file="_cleanupProjectSelector.jspf" %>
    <l:tableWithHighlighting className="settings" highlightImmediately="true">
      <jsp:useBean id="defaultPolicies"
                   type="java.util.Collection<jetbrains.buildServer.serverSide.impl.cleanup.HistoryRetentionPolicy>" scope="request"/>
      <jsp:useBean id="defaultOptions"
                   type="java.util.Map" scope="request"/>
      <c:if test="${not empty cleanupPoliciesForm.projectsToShow}">
        <tr class="tableHead">
          <th class="project">Project</th>
          <th class="buildType">Build configuration or template</th>
          <th class="description" colspan="2">
            What to clean-up<br/>
            <span class="cleanupRuleNote">
              * <span class="defaults">Light gray oblique text</span> <span style="color: #888">is used for inherited rules. Customised rules are in</span>
              darker, regular font.
            </span>
          </th>
        </tr>
        <%@ include file="_cleanupProjectsPolicies.jspf" %>
      </c:if>
    </l:tableWithHighlighting>
  </bs:refreshable>
  <%@ include file="_cleanupPolicyDialogForm.jspf" %>
</c:if>
