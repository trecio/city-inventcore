<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/include-internal.jsp" %>

<jsp:useBean id="bean" type="jetbrains.buildServer.controllers.admin.DiagnosticServerLogsController.ServerLogsBean" scope="request"/>
<bs:fileBrowsePage id="serverLogs"
                   dialogId=""
                   dialogTitle=""
                   bean="${bean}"
                   actionPath="/admin/serverLogs.html"
                   homePath="/admin/admin.html?item=diagnostics&tab=logs"
                   pageUrl="${pageUrl}"
                   jsBase="">
  <jsp:attribute name="belowFileName">
    <c:if test="${bean.clipped}"><div class="clippedMessage">Showing last ${bean.clipSize} only.</div></c:if>
  </jsp:attribute>
  <jsp:attribute name="headMessage">
    Choose a log file:
  </jsp:attribute>
  <jsp:attribute name="headMessageNoFiles">
    No log files available
  </jsp:attribute>
</bs:fileBrowsePage>
