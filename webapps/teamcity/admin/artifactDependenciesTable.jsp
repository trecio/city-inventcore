<%@ page import="jetbrains.buildServer.artifacts.RevisionRules" %>
<%@ include file="/include-internal.jsp" %>
<jsp:useBean id="artifactDependenciesBean" type="jetbrains.buildServer.controllers.admin.projects.EditableArtifactDependenciesBean" scope="request"/>
<jsp:useBean id="credentialsBean" type="jetbrains.buildServer.controllers.admin.projects.CredentialsBean" scope="request"/>
<c:set var="sourceDepsMap" value="${sourceDependenciesBean.dependOnIdsMap}"/>
<h3 class="noBorder">Artifact Dependencies</h3>

<c:if test="${empty artifactDependenciesBean.dependencies}">
  <p>There are no artifact dependencies.</p>
</c:if>
<c:if test="${not empty artifactDependenciesBean.dependencies}">
<form action="#">
<l:tableWithHighlighting highlightImmediately="true" id="artifactDeps" className="parametersTable">
  <tr>
    <th class="checkbox">
      <forms:checkbox name="selectAll"
                      onmouseover="BS.Tooltip.showMessage(this, {shift: {x: 10, y: 20}, delay: 600}, 'Click to select / unselect all dependencies')"
                      onmouseout="BS.Tooltip.hidePopup()"
                      onclick="if (this.checked) BS.Util.selectAll($('artifactDeps'), 'depIndex'); else BS.Util.unselectAll($('artifactDeps'), 'depIndex')"/>
    </th>
    <th class="artifactsSource">Artifacts source</th>
    <th class="artifactsPaths" colspan="3">Artifacts paths</th>
  </tr>
  <c:set var="sameChainRuleWarning"/>
  <c:forEach items="${artifactDependenciesBean.dependencies}" var="dependency" varStatus="pos">
    <c:set var="highlight" value='${not dependency.inherited and dependency.sourceBuildTypeAccessible ? "highlight" : ""}'/>
    <c:set var="onclick"><c:if test="${not dependency.inherited and dependency.sourceBuildTypeAccessible}">BS.ArtifactDependencyForm.editDependency(event, ${pos.index})</c:if></c:set>
    <tr>
      <td class="checkbox">
        <forms:checkbox name="depIndex" value="${pos.index}" disabled="${dependency.inherited or not dependency.sourceBuildTypeAccessible}"/>
      </td>
      <td class="${highlight}" onclick="${onclick}">
        <c:choose>
          <c:when test="${dependency.sourceBuildTypeAccessible}">
            <c:set var="sourceBuildType" value="${dependency.sourceBuildType}"/>
            <strong>
              <c:choose>
                <c:when test="${sourceBuildType != null}">
                  <bs:buildTypeLinkFull buildType="${sourceBuildType}" />
                </c:when>
                <c:otherwise>
                  <c:out value="${dependency.sourceName}"/>
                </c:otherwise>
              </c:choose>
            </strong>
          </c:when>
          <c:when test="${not dependency.sourceBuildTypeExists}"><em title="Build configuration does not exist">&laquo;build configuration does not exist&raquo;</em></c:when>
          <c:otherwise><em title="You do not have enough permissions for this build configuration">&laquo;inaccessible build configuration&raquo;</em></c:otherwise>
        </c:choose><c:if test="${dependency.inherited}"><span class="inheritedParam">(inherited)</span></c:if>
        <br/>
        (<c:if test="${not dependency.revisionRuleIsLastFinishedSameChainRule and sourceDepsMap[dependency.sourceBuildTypeId]}"
          ><c:set var="note">You have snapshot dependency (possibly transitive) configured for the selected build configuration.
           <br/>To obtain artifacts from a build with the same sources you should use "Build from the same chain".</c:set
          ><img src="<c:url value='/img/attentionComment.gif'/>" class="icon" <bs:tooltipAttrs text="${note}" />
          /> </c:if><bs:_artifactDependencyLink dependency="${dependency}"/>)
      </td>
      <td class="${highlight}" onclick="${onclick}">
        <ul class="artifactsPaths">
          <c:forEach items="${dependency.artifactsPaths}" var="path">
            <li><bs:out value="${path}" multilineOnly="true"/></li>
          </c:forEach>
          <c:if test="${dependency.cleanDestination}"><br><i>Destinations will be cleaned</i></c:if>
        </ul>
      </td>
      <c:choose>
        <c:when test="${dependency.inherited}">
          <td colspan="2">
            cannot be edited
          </td>
        </c:when>
        <c:otherwise>
          <td class="edit ${highlight}" onclick="${onclick}">
            <c:choose>
              <c:when test="${dependency.sourceBuildTypeAccessible}"><a href="#" onclick="${onclick}; Event.stop(event)" showdiscardchangesmessage="false">edit</a></c:when>
              <c:otherwise>cannot be edited</c:otherwise>
            </c:choose>
          </td>
          <td class="edit" title="Remove dependency">
            <a href="#" onclick="BS.ArtifactDependencyForm.removeDependencies([${pos.index}]); return false" showdiscardchangesmessage="false">delete</a>
          </td>
        </c:otherwise>
      </c:choose>
    </tr>
    <c:if test="${dependency.revisionRuleIsLastFinishedSameChainRule and not sourceDepsMap[dependency.sourceBuildTypeId]}">
      <c:set var="sameChainRuleWarning">You have "Build from the same chain" artifact dependency configured, but do not have corresponding snapshot dependency.</c:set>
    </c:if>
  </c:forEach>
</l:tableWithHighlighting>
<c:if test="${not empty sameChainRuleWarning}"><div class="attentionComment">${sameChainRuleWarning}</div></c:if>
</form>
</c:if>

<table class="dependencyActions">
  <tr>
    <td>
      <forms:addButton onclick="BS.ArtifactDependencyForm.addDependency(event); return false" showdiscardchangesmessage="false">Add new artifact dependency</forms:addButton>
    </td>
    <c:if test="${not empty artifactDependenciesBean.dependencies}">
    <td class="moreActions">
      <a href="#" id="deleteAllSelected"
         onclick="if (BS.ArtifactDependencyForm.dependenciesSelected()) BS.ArtifactDependencyForm.removeDependencies(BS.ArtifactDependencyForm.getSelectedDeps()); return false"
          >Remove selected dependencies</a>
    </td>
    </c:if>
  </tr>
</table>

<form id="verifyDependenciesForm" action="<c:url value='/admin/editDependencies.html?id=${buildForm.settingsId}'/>" method="post">
  <c:if test="${not empty artifactDependenciesBean.dependencies}">
    <p>
      Click "Check dependencies" button to ensure the specified dependencies are correct.
      You need to provide valid user credentials to download artifacts from the server and perform the dependencies verification,
      since access to the artifacts requires authorization.</p>
  <div class="saveButtonsBlock">
    <forms:submit type="button" label="Check artifact dependencies" onclick="BS.ArtifactDependencyVerificationForm.verifyDependencies();"/>
  </div>
  </c:if>
</form>

<bs:refreshable containerId="dependencyResolverMessages" pageUrl="${pageUrl}">
<c:if test="${artifactDependenciesBean.showVerificationLog}">
<div class="dependencyResolverMessagesLog mono">
  <c:forEach items="${artifactDependenciesBean.dependencyResolverMessages}" var="message">
    ${message}<br/>
  </c:forEach>
</div>
<div class="downloadStats">
  <c:if test="${artifactDependenciesBean.numberOfDownloadedArtifacts > 0}">
  Number of downloaded artifacts: <strong>${artifactDependenciesBean.numberOfDownloadedArtifacts}</strong><br/>
  </c:if>
  <c:if test="${artifactDependenciesBean.numberOfErrors > 0}">
  Number of errors: <a href="#errorNum:1" onclick="window.focus($('errorNum:1'))" showdiscardchangesmessage="false"><span class="errorMessage"><strong>${artifactDependenciesBean.numberOfErrors}</strong></span></a> (<a href="#errorNum:1" onclick="window.focus($('errorNum:1'))" showdiscardchangesmessage="false">navigate</a> to the first error)<br/>
  </c:if>
</div>
<script type="text/javascript">
<c:choose>
  <c:when test="${not artifactDependenciesBean.resolvingFinished}">
      window.setTimeout(function() {
        $('dependencyResolverMessages').refresh()
      }, 500);
  </c:when>
  <c:otherwise>
    BS.ArtifactDependencyVerificationForm.verificationFinished();
  </c:otherwise>
</c:choose>
</script>
</c:if>
</bs:refreshable>

<bs:refreshable containerId="artifactDependencyDialog" pageUrl="${pageUrl}&artDepsDialog=true">
<c:set var="title"><c:choose><c:when test="${artifactDependenciesBean.addDependencyMode}">Add New Artifact Dependency</c:when><c:when test="${artifactDependenciesBean.editDependencyMode}">Edit Artifact Dependency</c:when></c:choose></c:set>
<c:url value='/admin/editDependencies.html?id=${buildForm.settingsId}' var="action"/>
<script type="text/javascript">
  var sourceDepsMap = {};
  <c:forEach items="${sourceDependenciesBean.visibleDependencies}" var="dep">
  <c:if test="${dep.sourceBuildTypeAccessible}">
  sourceDepsMap['${dep.sourceBuildTypeId}'] = true;
  </c:if>
  </c:forEach>
</script>

<bs:modalDialog formId="artifactDependencyForm"
                action="${action}"
                closeCommand="BS.ArtifactDependencyForm.close()"
                saveCommand="BS.ArtifactDependencyForm.saveDependency()"
                title="${title}">
  <div class="error" id="errorSimilarDependencyExists" style="margin-left: 0;"></div>
  <c:set var="editingDependency" value="${artifactDependenciesBean.editingDependency}"/>
  <table class="artifactDependencyFormTable">
    <tr>
      <td class="header"><label for="sourceBuildTypeId">Depend on:</label></td>
      <td>
        <forms:select name="sourceBuildTypeId" id="sourceBuildTypeId" style="width: 25em;" onchange="BS.EditArtifactDependencies.showHideLastFinishedNote(); BS.EditArtifactDependencies.updateBuildTypeTagsList();" enableFilter="true">
          <c:if test="${param['artDepsDialog'] == 'true'}">
          <c:forEach items="${artifactDependenciesBean.buildTypes}" var="entry">
            <optgroup label="${entry.key.name}">
              <c:forEach var="buildType" items="${entry.value}">
                <c:if test="${buildForm.template or (not buildForm.template and buildForm.settingsBuildType.buildTypeId != buildType.id)}">
                  <forms:option value="${buildType.buildTypeId}"
                                title="${entry.key.name} :: ${buildType.name}"
                                selected="${buildType.buildTypeId == editingDependency.sourceBuildTypeId}">
                    <c:out value="${buildType.name}"/>
                  </forms:option>
                </c:if>
              </c:forEach>
            </optgroup>
          </c:forEach>
          </c:if>
        </forms:select>
        <span class="error" id="errorSourceBuildTypeNotExists" style="margin-left: 0;"></span>
      </td>
    </tr>
    <tr>
      <td class="header" style="vertical-align: top;"><label for="revisionRules">Get artifacts from:</label></td>
      <td>

        <forms:select name="revisionRules" style="width: 25em;" onchange="BS.EditArtifactDependencies.showHideValueFields(); BS.EditArtifactDependencies.showHideLastFinishedNote();" enableFilter="${true}">
          <forms:buildAnchorOptions selected="${editingDependency.revisionRuleName}" withChainOption="true"/>
        </forms:select>
        <div id="lastFinishedNote" class="attentionComment" style="display: none;">
        Snapshot dependency is configured for the selected build configuration.
        To obtain artifacts from a build with the same sources you should select "Build from the same chain".
        </div>

        <c:set var="buildNumber" value="<%=RevisionRules.BUILD_NUMBER_NAME%>"/>
        <c:set var="buildTag" value="<%=RevisionRules.BUILD_TAG_NAME%>"/>
        <div id="buildNumberField" class="buildNumberPattern">
          <label for="buildNumberPattern">Build number:</label>
          <forms:textField className="buildTypeParams" disabled="${editingDependency.revisionRuleName != buildNumber}" name="buildNumberPattern" size="12"
                           maxlength="100" style="width: 18em" value="${editingDependency.revisionRuleName == buildNumber ? editingDependency.revisionRuleValue : ''}"/><bs:help file="Build+Number"/>
          <span class="error" id="errorBuildNumberPattern" style="margin-left: 10em;"></span>
        </div>
        <div id="buildTagField" class="buildNumberPattern">
          <label for="buildTag">Build tag:</label>
          <forms:textField className="buildTypeParams" disabled="${editingDependency.revisionRuleName != buildTag}" name="buildTag" size="12"
                           maxlength="100" style="width: 20em" value="${editingDependency.revisionRuleName == buildTag ? editingDependency.revisionRuleValue : ''}"/>
          <div id="buildTagList">Available tags: <span id="buildTagListSpan"></span></div>
          <span class="error" id="errorBuildTag" style="margin-left: 10em;"></span>
        </div>
        <script type="text/javascript">
          BS.EditArtifactDependencies.showHideValueFields();
          BS.EditArtifactDependencies.showHideLastFinishedNote();
          BS.EditArtifactDependencies.updateBuildTypeTagsList();
        </script>
      </td>
    </tr>
    <tr>
      <td class="header" style="vertical-align: top;">
        <label onclick="$('artifactsPaths').getElementsByTagName('input')[0].focus()">Artifacts rules:</label>
      </td>
      <td>
        <ul class="artifactsPaths" id="artifactsPaths">
            <li>
              <textarea rows="4" cols="30" style="width: 25em; resize: vertical;" class="buildTypeParams" name="artifactPath" id="artifactPaths" wrap="soft"><c:out value="${editingDependency.artifactsPaths}"/></textarea>
            </li>
        </ul>
        <span class="error" id="errorArtifactsPaths" style="margin-left: 0;"></span>
        <div class="smallNote" style="margin-left: 0;">
          Newline-delimited set or rules in the form of<br/>[+:|-:]SourcePath[!ArchivePath][=>DestinationPath]<bs:help file="Artifact+Dependencies" width="1220"/>
        </div>
      </td>
    </tr>
    <tr>
      <td></td>
      <td style="padding: 0;">
        <forms:checkbox name="cleanDestination" checked="${editingDependency.cleanDestination}"/>
        <label for="cleanDestination" class="rightLabel">Clean destination paths before downloading artifacts</label>
      </td>
    </tr>
  </table>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.ArtifactDependencyForm.close()" showdiscardchangesmessage="false"/>
    <forms:submit label="Save"/>
    <forms:saving id="saveArtifactDependencyProgress"/>
  </div>

  <input type="hidden" name="revisionRuleName" value="${editingDependency.revisionRuleName}"/>
  <input type="hidden" name="revisionRuleValue" value="<c:out value="${editingDependency.revisionRuleValue}"/>"/>
  <c:choose>
    <c:when test="${artifactDependenciesBean.addDependencyMode}">
      <input type="hidden" name="saveArtifactDependency" value="add"/>
    </c:when>
    <c:otherwise>
      <input type="hidden" name="saveArtifactDependency" value="edit:${artifactDependenciesBean.selectedDependencyIndex}"/>
    </c:otherwise>
  </c:choose>

  <div id="artifactsTreePopup" class="popupDiv"></div>
</bs:modalDialog>

<script type="text/javascript">
  BS.AvailableParams.attachPopups('settingsId=${buildForm.settingsId}', 'buildTypeParams');
  BS.EditArtifactDependencies.attachPopups('buildTypeParams', 'artifactPaths', 'artifactDependencyDialog');
</script>
</bs:refreshable>

<bs:modalDialog formId="enterCredentials" title="Specify username and password" action="#" closeCommand="BS.EnterCredentialsDialog.close()" saveCommand="BS.EnterCredentialsDialog.submit()">
  <div style="margin-top: 0.5em;">
    <label for="username1" class="tableLabel" style="width: 6em;">Username:</label>
    <forms:textField name="username1" value="${credentialsBean.username}"/>
  </div>

  <div style="margin-top: 0.5em;">
    <label for="password1" class="tableLabel" style="width: 6em;">Password:</label>
    <forms:passwordField name="password1"
                         encryptedPassword="${credentialsBean.encryptedPassword}"
                         publicKey="${credentialsBean.hexEncodedPublicKey}"/>
  </div>

  <span class="error" id="invalidCredentials" style="margin: 0;"></span>

  <div class="smallNote" style="margin: 0;">
    Please type valid username and password to download artifacts from server.
  </div>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.EnterCredentialsDialog.close()" showdiscardchangesmessage="false"/>
    <forms:submit label="Check dependencies"/>
    <forms:saving id="submitCredentials"/>
  </div>
</bs:modalDialog>
