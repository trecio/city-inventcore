<%@include file="/include-internal.jsp"%>
<jsp:useBean id="reportTabsForm" type="jetbrains.buildServer.controllers.admin.reportTabs.EditReportTabsForm" scope="request"/>
<jsp:useBean id="pageUrl" type="java.lang.String" scope="request"/>

<div>
  Here you can define custom artifact-based tabs<bs:help file="Including+Third-Party+Reports+in+the+Build+Results"/> for build results.
  <br/>
  Note that these settings are system-wide, i.e. each build producing a corresponding artifact will get a custom tab.
</div>

<admin:editReportTabsTable pageUrl="${pageUrl}" reportTabsForm="${reportTabsForm}"/>
<admin:editReportTabDialog title="Edit Report Tab Settings"/>