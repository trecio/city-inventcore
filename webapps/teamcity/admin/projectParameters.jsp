<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<jsp:useBean id="currentProject" type="jetbrains.buildServer.serverSide.SProject" scope="request"/>
<jsp:useBean id="projectParametersBean" type="jetbrains.buildServer.controllers.admin.projects.EditableUserDefinedParametersBean" scope="request"/>

<c:url var="actionUrl" value="/admin/editProjectParams.html?projectId=${currentProject.projectId}"/>
<c:url value="/admin/parameterAutocompletion.html?projectId=${currentProject.projectId}" var="autocompletionUrl"/>
<admin:userDefinedParameters userParametersBean="${projectParametersBean}" parametersActionUrl="${actionUrl}" parametersAutocompletionUrl="${autocompletionUrl}"/>

<script type="text/javascript">
  $j(document).ready(function() {
    BS.AvailableParams.attachPopups('projectId=${currentProject.projectId}', 'buildTypeParams');
    BS.VisibilityHandlers.updateVisibility('mainContent');
  });
</script>
