<%@ page import="jetbrains.buildServer.controllers.admin.projects.EditVcsRootsController" %>
<%@ include file="/include-internal.jsp"%>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="vcsPropertiesBean" scope="request" type="jetbrains.buildServer.controllers.admin.projects.VcsPropertiesBean"/>
<jsp:useBean id="pageUrl" scope="request" type="java.lang.String"/>
<c:set var="pageTitle" scope="request"><c:choose><c:when test="${vcsPropertiesBean.newRoot}">New </c:when><c:otherwise>Edit </c:otherwise></c:choose>VCS Root</c:set>
<c:set var="tplUsages" value="${vcsPropertiesBean.templateVcsRootUsages}"/>
<c:set var="btUsages" value="${vcsPropertiesBean.buildTypeVcsRootUsages}"/>
<c:set var="totalUsages" value="${fn:length(tplUsages) + fn:length(btUsages)}"/>
<bs:page>
<jsp:attribute name="head_include">
  <bs:linkCSS>
    /css/admin/adminMain.css
    /css/admin/vcsSettings.css
  </bs:linkCSS>
  <bs:linkScript>
    /js/bs/editProject.js
    /js/bs/testConnection.js
    /js/jquery/jquery.ui.position.js
  </bs:linkScript>
  <script type="text/javascript">
    BS.Navigation.items = [
        {title: "Administration", url: '<c:url value="/admin/admin.html"/>'},
        <forms:cameBackNav cameFromSupport="${vcsPropertiesBean.cameFromSupport}"/>
        {title: "${pageTitle}", selected:true}
    ];
    <c:if test="${not vcsPropertiesBean.newRoot}">
    $j(document).ready(function() {
      BS.VcsSettingsForm.setupEventHandlers();
    });
    </c:if>
  </script>
</jsp:attribute>

<jsp:attribute name="body_include">
  <forms:modified/>

  <div id="container" class="clearfix">

    <div class="editVcsRootPage">

      <form id="vcsSettingsForm" action="<c:url value='/admin/editVcsRoot.html?action=${vcsPropertiesBean.newRoot ? "addVcsRoot" : "editVcsRoot"}'/>"
            method="post" onsubmit="return BS.VcsSettingsForm.submitVcsSettings(${vcsPropertiesBean.newRoot});" autocomplete="off">

      <div class="vcsSettings clearfix">
        <table class="runnerFormTable">
          <l:settingsGroup title="Type of VCS">
            <tr>
              <th class="noBorder"><label for="vcsRootName">Type of VCS:</label></th>
              <td class="noBorder"><select name="vcsName" onchange="BS.VcsSettingsForm.setSelectedVcs(this.options[this.selectedIndex].value)">
                <c:if test="${vcsPropertiesBean.newRoot}">
                  <forms:option value="">-- Choose type of VCS --</forms:option>
                </c:if>
                <c:forEach items="${vcsPropertiesBean.availableVcsTypes}" var="vcs">
                  <forms:option value="${vcs.name}" selected="${vcs.name == vcsPropertiesBean.vcsName}"><c:out value="${vcs.displayName}"/></forms:option>
                </c:forEach>
              </select><forms:saving id="chooseVcsTypeProgress" className="progressRingInline"/></td>
            </tr>
          </l:settingsGroup>
        </table>
      </div>

      <bs:refreshable containerId="vcsRootProperties" pageUrl="${pageUrl}">
        <script type="text/javascript">
          BS.MultilineProperties.clearProperties();
        </script>
        <c:if test="${vcsPropertiesBean.vcsTypeSelected}">
          <div class="vcsSettings clearfix">
            <table class="runnerFormTable">
            <l:settingsGroup title="VCS Root Name">
              <th class="noBorder"><label for="vcsRootName">VCS Root Name:</label></th>
              <td class="noBorder">
                <forms:textField name="vcsRootName" value="${vcsPropertiesBean.vcsRootName}" maxlength="256" className="textProperty disableBuildTypeParams longField"/>
                <span class="error" id="errorVcsRootName"></span>
                <span class="smallNote">Enter a unique name to distinguish this VCS root from other roots. If not specified, the name will be generated automatically.</span>
              </td>
            </l:settingsGroup>
            </table>

            <c:set var="propertiesBean" value="${vcsPropertiesBean.propertiesBean}" scope="request"/>
            <jsp:include page="${vcsPropertiesBean.vcsSettingsJspPath}">
              <jsp:param name="propertiesBean" value="${propertiesBean}"/>
            </jsp:include>

            <table class="runnerFormTable">
              <l:settingsGroup title="Changes Checking Interval">
                <tr>
                  <c:set var="defaultMode" value="${vcsPropertiesBean.useDefaultModificationCheckInterval}"/>
                  <c:set var="onclick">
                    $('modificationCheckInterval').disabled = true;
                  </c:set>
                  <th><label>Checking interval:</label></th>
                  <td>
                    <forms:radioButton name="modificationCheckIntervalMode"
                                       id="mod-check-interval-default"
                                       checked="${defaultMode}"
                                       value="DEFAULT"
                                       onclick="${onclick}"/>
                    <label for="mod-check-interval-default">use global server setting (${vcsPropertiesBean.defaultModificationCheckInterval} seconds)</label>
                </tr>

                <tr>
                  <c:set var="onclick">
                    $('modificationCheckInterval').disabled = false;
                    $('modificationCheckInterval').focus();
                  </c:set>
                  <td>&nbsp;</td>
                  <td>
                    <forms:radioButton name="modificationCheckIntervalMode"
                                       value="SPECIFIED"
                                       id="mod-check-interval-specified"
                                       checked="${not defaultMode}"
                                       onclick="${onclick}"/>
                    <label for="mod-check-interval-specified">custom:
                    <forms:textField name="modificationCheckInterval" size="4" maxlength="8" value="${vcsPropertiesBean.modificationCheckInterval}" disabled="${defaultMode}"/>
                    seconds</label><span class="error" id="invalidModificationCheckInterval"></span>
                  </td>
                </tr>
                <tr><td colspan="2"><span class="smallNote">Please note that certain servers can refuse access if polled too frequently.
                  Consider intervals greater than 1800 seconds (30 minutes) for public servers.</span></td></tr>
              </l:settingsGroup>

              </table>

            <table class="runnerFormTable">
            <l:settingsGroup title="VCS Root Sharing">
              <tr>
                <th colspan="2" class="noBorder">
                  <authz:authorize allPermissions="CHANGE_GLOBAL_VCS_ROOT">
                    <jsp:attribute name="ifAccessGranted">
                      <forms:checkbox name="globalScope" checked="${vcsPropertiesBean.globalScope}" disabled="${not vcsPropertiesBean.canChangeScope}"/>
                      <label for="globalScope">Make this VCS root available to all of the projects</label>
                      <c:if test="${not vcsPropertiesBean.canChangeScope}">
                        <c:if test="${vcsPropertiesBean.numberOfProjectUsages > 1}">
                          <div class="smallNote" style="font-weight: normal; margin-bottom: 4px; margin-left: 0;">This VCS root cannot be made local because it is being used by more than one project.</div>
                        </c:if>
                        <c:if test="${vcsPropertiesBean.numberOfProjectUsages == 0}">
                          <div class="smallNote" style="font-weight: normal; margin-bottom: 4px; margin-left: 0;">This VCS root cannot be made local because it is not being used by any project.</div>
                        </c:if>
                      </c:if>
                    </jsp:attribute>
                    <jsp:attribute name="ifAccessDenied">
                      <span style="font-weight:normal;">
                      <c:choose>
                        <c:when test="${vcsPropertiesBean.globalScope}">This VCS root is available to all of the projects</c:when>
                        <c:otherwise>This VCS root is available to <strong><c:out value="${vcsPropertiesBean.targetProject.name}"/></strong> project only</c:otherwise>
                      </c:choose>
                      </span>
                    </jsp:attribute>
                  </authz:authorize>

                </th>
              </tr>
            </l:settingsGroup>
            </table>
          </div>

          <div>
            <c:if test="${not vcsPropertiesBean.newRoot}">
            <c:set var="allProjectsOptionShown" value="${false}"/>
            <c:if test="${vcsPropertiesBean.editTemplateMode}">
              <c:set var="tplUsageCount" value="${vcsPropertiesBean.targetSettings.numberOfUsages}"/>
              <c:set var="tplUsagesSuffix"><c:if test="${tplUsageCount ge 0}"> and ${tplUsageCount} inherited configuration<bs:s val="${tplUsageCount}"/></c:if></c:set>
            </c:if>
            <c:set var="targetName" value=""/>
            <c:choose>
              <c:when test="${vcsPropertiesBean.createBuildTypeMode}"><c:set var="targetName" value="newly created build configuration"/></c:when>
              <c:when test="${vcsPropertiesBean.createTemplateMode}"><c:set var="targetName" value="newly created template"/></c:when>
              <c:when test="${vcsPropertiesBean.editBuildTypeMode}"><c:set var="targetName">${vcsPropertiesBean.targetSettings.name} only</c:set></c:when>
              <c:when test="${vcsPropertiesBean.editTemplateMode}"><c:set var="targetName">${vcsPropertiesBean.targetSettings.name} ${tplUsagesSuffix}</c:set></c:when>
            </c:choose>
            <c:choose>
              <c:when test="${vcsPropertiesBean.globalScope and afn:permissionGrantedGlobally('CHANGE_GLOBAL_VCS_ROOT')}">
                <c:set var="saveOptionsShown" value="${vcsPropertiesBean.editBuildTypeOrTemplateMode or not empty vcsPropertiesBean.targetProject}"/>
                <c:if test="${saveOptionsShown or vcsPropertiesBean.numberOfProjectUsages > 1}">
                  <div class="attentionComment">
                    <c:if test="${vcsPropertiesBean.numberOfProjectUsages > 1}">
                      This VCS root is used in more than one project. Changes to this VCS root may affect all of them.
                    </c:if>
                    <c:if test="${saveOptionsShown}">
                      Please choose corresponding option for applying changes in this VCS root.
                    </c:if>
                  </div>
                </c:if>
                <table class="saveVcsRootOptions">
                  <c:if test="${vcsPropertiesBean.createBuildTypeOrTemplateMode or vcsPropertiesBean.editBuildTypeOrTemplateMode}">
                  <tr>
                    <td>
                      <forms:radioButton name="saveOption" value="<%=EditVcsRootsController.SaveOptions.COPY_TO_TARGET_BUILD_TYPE.name()%>" id="saveOptionCopyToTargetConf"/>
                      <label for="saveOptionCopyToTargetConf">Apply to <strong><c:out value="${targetName}"/></strong> (a copy of this VCS root will be created)</label>
                    </td>
                  </tr>
                  </c:if>
                  <c:if test="${not empty vcsPropertiesBean.targetProject}">
                    <tr>
                      <td>
                      <forms:radioButton name="saveOption" value="<%=EditVcsRootsController.SaveOptions.COPY_TO_PROJECT_BUILD_TYPES.name()%>" id="saveOptionCopyToProjectConfs"/>
                      <label for="saveOptionCopyToProjectConfs">Apply to all templates & configurations of <strong><c:out value="${vcsPropertiesBean.targetProject.name}"/></strong> project where this VCS root is used (a copy of this VCS root will be created)</label>
                      </td>
                    </tr>
                  </c:if>
                  <c:if test="${saveOptionsShown}">
                  <tr>
                    <td>
                      <c:set var="allProjectsOptionShown" value="${true}"/>
                    <forms:radioButton name="saveOption" value="<%=EditVcsRootsController.SaveOptions.SAVE_FOR_ALL.name()%>" id="saveOptionSave"/>
                    <label for="saveOptionSave">Apply to all projects</label>
                    </td>
                  </tr>
                  </c:if>
                </table>
              </c:when>
              <c:when test="${vcsPropertiesBean.globalScope and not afn:permissionGrantedGlobally('CHANGE_GLOBAL_VCS_ROOT')}">
                <div class="attentionComment">This VCS root is shared among all of the projects. You do not have enough permissions to update shared VCS roots. Please choose corresponding option for applying changes in this VCS root.</div>
                <table class="saveVcsRootOptions">
                  <c:if test="${vcsPropertiesBean.createBuildTypeOrTemplateMode or vcsPropertiesBean.editBuildTypeOrTemplateMode}">
                  <tr>
                    <td>
                      <forms:radioButton name="saveOption" value="<%=EditVcsRootsController.SaveOptions.COPY_TO_TARGET_BUILD_TYPE.name()%>" id="saveOptionCopyToTargetConf"/>
                      <label for="saveOptionCopyToTargetConf">Apply to <strong><c:out value="${targetName}"/></strong> (a copy of this VCS root will be created)</label>
                    </td>
                  </tr>
                  </c:if>
                  <c:if test="${not empty vcsPropertiesBean.targetProject}">
                  <tr>
                    <td>
                      <forms:radioButton name="saveOption" value="<%=EditVcsRootsController.SaveOptions.COPY_TO_PROJECT_BUILD_TYPES.name()%>" id="saveOptionCopyToProjectConfs"/>
                      <label for="saveOptionCopyToProjectConfs">Apply to all templates & configurations of <strong><c:out value="${vcsPropertiesBean.targetProject.name}"/></strong> project where this VCS root is used (a copy of this VCS root will be created)</label>
                    </td>
                  </tr>
                  </c:if>
                  <tr>
                    <td>
                      <c:set var="allProjectsOptionShown" value="${true}"/>
                    <forms:radioButton name="saveOption" value="<%=EditVcsRootsController.SaveOptions.SAVE_FOR_ALL.name()%>" id="saveOptionSave" disabled="true"/>
                    <label for="saveOptionSave">Apply to all projects (<span style="color: #ED2C10;">you do not have enough permissions to choose this option, please contact your system administrator</span>)</label>
                    </td>
                  </tr>
                </table>
              </c:when>
              <c:when test="${totalUsages > 1 and vcsPropertiesBean.editBuildTypeOrTemplateMode}">
                <div class="attentionComment">This VCS root is used in more than one build configuration or template. Please choose corresponding option for applying changes in this VCS root.</div>
                <table class="saveVcsRootOptions">
                  <c:if test="${vcsPropertiesBean.createBuildTypeOrTemplateMode or vcsPropertiesBean.editBuildTypeOrTemplateMode}">
                  <tr>
                    <td>
                      <forms:radioButton name="saveOption" value="<%=EditVcsRootsController.SaveOptions.COPY_TO_TARGET_BUILD_TYPE.name()%>" id="saveOptionCopyToTargetConf"/>
                      <label for="saveOptionCopyToTargetConf">Apply to <strong><c:out value="${targetName}"/></strong> (a copy of this VCS root will be created)</label>
                    </td>
                  </tr>
                  </c:if>
                  <tr>
                    <td>
                    <forms:radioButton name="saveOption" value="<%=EditVcsRootsController.SaveOptions.SAVE_FOR_ALL.name()%>" id="saveOptionSave" checked="true"/>
                    <label for="saveOptionSave">Apply to all templates & configurations of <strong><c:out value="${vcsPropertiesBean.targetProject.name}"/></strong> project</label>
                    </td>
                  </tr>
                </table>
              </c:when>
            </c:choose>
            </c:if>
            <div class="saveButtonsBlock">
              <forms:cancel cameFromSupport="${vcsPropertiesBean.cameFromSupport}"/>
              <forms:submit label="Save"/>
              <c:if test="${vcsPropertiesBean.testConnectionSupported}">
                <forms:submit id="testConnectionButton" type="button" label="Test connection" onclick="BS.VcsSettingsForm.submitTestConnection(${vcsPropertiesBean.newRoot});"/>
              </c:if>
              <forms:saving/>
              <c:if test="${not allProjectsOptionShown}">
                <input type="hidden" name="saveOption" value="SAVE_FOR_ALL"/>
              </c:if>
            </div>
          </div>

        </c:if>

        <input type="hidden" name="submitVcsRoot" id="submitVcsRoot" value="store"/>
        <input type="hidden" name="publicKey" id="publicKey" value="${vcsPropertiesBean.publicKey}"/>
        <input type="hidden" name="editingScope" id="editingScope" value="<c:out value="${vcsPropertiesBean.editingScope}"/>"/>
        <input type="hidden" name="vcsRootId" id="vcsRootId" value="${vcsPropertiesBean.vcsRootId}"/>

        <script type="text/javascript">
          <c:choose>
          <c:when test="${not vcsPropertiesBean.newRoot}">
          BS.VcsSettingsForm.setModified(${vcsPropertiesBean.stateModified});
          BS.AvailableParams.attachPopups('vcsRootId=${vcsPropertiesBean.vcsRootId}', 'textProperty', 'multilineProperty');
          </c:when>
          <c:otherwise>
          BS.AvailableParams.attachPopups('', 'textProperty', 'multilineProperty');
          </c:otherwise>
          </c:choose>
          BS.VisibilityHandlers.updateVisibility('vcsRootProperties');
        </script>

        <bs:dialog dialogId="testConnectionDialog" dialogClass="vcsRootTestConnectionDialog" title="Test Connection" closeCommand="BS.TestConnectionDialog.close();"
          closeAttrs="showdiscardchangesmessage='false'">
          <div id="testConnectionStatus"></div>
          <div id="testConnectionDetails" class="mono"></div>
        </bs:dialog>
      </bs:refreshable>

    </form>

    </div>

    <div id="sidebarAdmin">
      <c:if test="${totalUsages > 0}">
        <table class="usefulLinks">
          <tr>
            <td>
              <c:if test="${not empty tplUsages}">
              <p>Attached to templates:</p>
              <ul>
              <c:forEach items="${tplUsages}" var="tpl">
                <li>
                <c:choose>
                  <c:when test="${afn:permissionGrantedForProject(tpl.parentProject,'EDIT_PROJECT')}">
                    <admin:editTemplateLink step="vcsRoots" templateId="${tpl.id}"><c:out value="${tpl.fullName}"/></admin:editTemplateLink>
                  </c:when>
                  <c:otherwise><span title="You do not have enough permissions to edit this build configuration"><em><c:out value="${tpl.fullName}"/></em></span></c:otherwise>
                </c:choose>
                </li>
              </c:forEach>
              </ul>
              </c:if>

              <c:if test="${not empty btUsages}">
              <p>Attached to build configurations:</p>
              <ul>
              <c:forEach items="${btUsages}" var="buildType">
                <li>
                <c:choose>
                  <c:when test="${afn:permissionGrantedForBuildType(buildType,'EDIT_PROJECT')}">
                    <admin:editBuildTypeLink step="vcsRoots" buildTypeId="${buildType.buildTypeId}"><c:out value="${buildType.fullName}"/></admin:editBuildTypeLink>
                  </c:when>
                  <c:otherwise><span title="You do not have enough permissions to edit this build configuration"><c:out value="${buildType.fullName}"/></span></c:otherwise>
                </c:choose>
                </li>
              </c:forEach>
              </ul>
              </c:if>
            </td>
          </tr>
        </table>
      </c:if>
      <c:set var="inaccessibleNum" value="${vcsPropertiesBean.numberOfInaccessibleUsages}"/>
      <c:if test="${inaccessibleNum > 0}">
      <table class="usefulLinks">
        <tr>
          <td>
            <span class="smallNote" style="margin-left: 0;">This VCS root is also used in ${inaccessibleNum} project<bs:s val="${inaccessibleNum}"/> you do not have access to.</span>
          </td>
        </tr>
      </table>
      </c:if>
    </div>

  </div>
</jsp:attribute>
</bs:page>
