<%@ include file="/include-internal.jsp"%>
<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %>
<jsp:useBean id="currentProject" type="jetbrains.buildServer.serverSide.impl.ProjectEx" scope="request"/>
<jsp:useBean id="projectForm" type="jetbrains.buildServer.controllers.admin.projects.EditProjectForm" scope="request"/>
<jsp:useBean id="editableProjects" type="java.util.Collection" scope="request"/>
<jsp:useBean id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary" scope="request"/>
<%--@elvariable id="cameFromUrl" type="java.lang.String"--%>
<c:set var="originalProject" value="${projectForm.originalProject}"/>

<div id="sidebarAdmin">
  <c:if test="${serverSummary.perProjectPermissionsEnabled}">
  <c:if test="${afn:permissionGrantedForProject(originalProject, 'CHANGE_USER_ROLES_IN_PROJECT') or
                afn:permissionGrantedGlobally('CHANGE_USER')}">
  <table class="usefulLinks">
    <tr>
      <td colspan="2" style="background-color: #f5f5f5;">
        Configure user roles in this project on the
        <strong><a href="<c:url value='/admin/admin.html?item=users&projectId=${originalProject.projectId}'/>">user accounts</a></strong> page.
      </td>
    </tr>
  </table>
  </c:if>
  </c:if>
  <c:if test="${afn:permissionGrantedGlobally('CREATE_CLONE_PROJECT') or
                afn:permissionGrantedGlobally('DELETE_PROJECT') or
                afn:permissionGrantedForProject(currentProject, 'ARCHIVE_PROJECT')}">
  <table class="usefulLinks">
    <bs:professionalLimited shouldBePositive="${serverSummary.buildConfigurationsLeft - fn:length(originalProject.buildTypes) + 1}">
      <authz:authorize allPermissions="CREATE_CLONE_PROJECT">
      <tr>
        <td class="button">
          <input class="btn btn_mini action" type="button" value="Copy"
                 onclick="BS.CopyProjectForm.showDialog('${originalProject.projectId}', '${originalProject.name}', ${fn:length(originalProject.buildTypeTemplates) > 0})"/>
        </td>
        <td>Copy this project</td>
      </tr>
      </authz:authorize>
    </bs:professionalLimited>
    <authz:authorize allPermissions="DELETE_PROJECT">
    <tr>
      <td class="button">
        <input class="btn btn_mini action" type="button" value="Delete" onclick="BS.AdminActions.deleteProject('${originalProject.projectId}')"/>
      </td>
      <td>Delete this project and all its data (build history, artifacts, etc.)</td>
    </tr>
    </authz:authorize>
    <authz:authorize allPermissions="ARCHIVE_PROJECT" projectId="${currentProject.projectId}">
    <tr>
    <c:choose>
      <c:when test="${originalProject.archived}">
        <td class="button">
          <input class="btn btn_mini action" type="button" value="Unarchive" onclick="BS.AdminActions.dearchiveProject('${originalProject.projectId}')"/>
        </td>
        <td>Unarchive this project<bs:help file="Archiving+Projects"/></td>
      </c:when>
      <c:otherwise>
        <td class="button">
          <input class="btn btn_mini action
          " type="button" value="Archive" onclick="BS.AdminActions.archiveProject('${originalProject.projectId}')"/>
        </td>
        <td style="white-space: nowrap;">Archive this project<bs:help file="Archiving+Projects"/></td>
      </c:otherwise>
    </c:choose>
    </tr>
    </authz:authorize>
  </table>
  </c:if>
  <admin:configModificationInfo auditLogAction="${currentProject.lastConfigModificationAction}"/>
</div>

<div class="editProjectPage">
  <bs:messages key="projectCopied"/>
  <bs:messages key="projectArchived"/>
  <bs:messages key="projectDearchived"/>

  <form id="editProjectForm" action="<c:url value='/admin/editProject.html?projectId=${originalProject.projectId}'/>"
        onsubmit="return BS.EditProjectForm.submitProject()" method="post" class="clearfix">
    <admin:projectForm projectForm="${projectForm}"/>
    <div class="saveButtonsBlock">
      <forms:cancel cameFromSupport="${projectForm.cameFromSupport}"/>
      <forms:submit name="submitButton" label="Save"/>
      <forms:saving/>
    </div>
  </form>
  <div class="configurationSection clearfix">
    <h3 class="title">Build Configurations<admin:bcLeft/></h3>
    <bs:messages key="buildTypeRemoved" style="text-align: left;"/>
    <div class="clearfix">
      <admin:createBuildTypeLink projectId="${originalProject.projectId}"
                                 cameFromUrl="${cameFromUrl}">Create build configuration</admin:createBuildTypeLink>
      <c:if test="${not empty originalProject.buildTypeTemplates}">
        <span class="shift">
          <a class="btn" href="#" onclick="return BS.CreateFromTemplateAction.showDialogWithChooser();"><span class="addNew">Create from template</span></a>
        </span>
      </c:if>
      <ext:forEachExtension placeId="<%=PlaceId.ADMIN_PROJECT_CREATE_BUILD_TYPE%>">
        <span class="shift"><ext:includeExtension extension="${extension}"/></span>
      </ext:forEachExtension>
    </div>
    <c:if test="${not empty originalProject.buildTypes}">
      <l:tableWithHighlighting className="settings" mouseovertitle="Click to edit build configuration" id="configurations">
        <c:set var="colspan" value="${fn:length(editableProjects) > 1 ? 5 : 4}"/>
        <tr>
          <th class="name">Configuration name</th>
          <th class="runner" colspan="${colspan}">Build Steps</th>
        </tr>
        <c:forEach items="${originalProject.buildTypes}" var="buildType">
          <c:set var="onclick">BS.openUrl(event, '<admin:editBuildTypeLink withoutLink="true"
                                                                           buildTypeId="${buildType.buildTypeId}"
                                                                           cameFromUrl="${cameFromUrl}"/>');</c:set>
          <tr>
            <authz:editBuildTypeGranted buildType="${buildType}">
              <jsp:attribute name="ifAccessDenied">
                <td class="name" colspan="${colspan+1}">
                  <admin:buildTypeTemplateInfo buildType="${buildType}"/>
                  <strong><c:out value="${buildType.name}"/></strong>
                  <bs:smallNote><c:out value="${buildType.description}"/></bs:smallNote>
                </td>
              </jsp:attribute>
              <jsp:attribute name="ifAccessGranted">
                <td class="name highlight" onclick="${onclick}">
                  <admin:buildTypeTemplateInfo buildType="${buildType}"/>
                  <strong><c:out value="${buildType.name}"/></strong>
                  <bs:smallNote><c:out value="${buildType.description}"/></bs:smallNote>
                </td>
                <td class="runner highlight" onclick="${onclick}">
                  <admin:buildRunnersInfo buildRunners="${buildType.buildRunners}"/>
                </td>
                <td class="edit highlight" onclick="${onclick}">
                  <admin:editBuildTypeMenu buildType="${buildType}" cameFromUrl="${cameFromUrl}">edit</admin:editBuildTypeMenu>
                </td>
                <td class="edit">
                  <admin:buildTypeActions buildType="${buildType}" editableProjects="${editableProjects}"/>
                </td>
              </jsp:attribute>
            </authz:editBuildTypeGranted>
          </tr>
        </c:forEach>
      </l:tableWithHighlighting>
    </c:if>
  </div>

  <div class="configurationSection clearfix">
    <h3 class="title">Build Configuration Templates</h3>
    <bs:messages key="templateRemoved" style="text-align: left;"/>
    <c:set var="templates" value="${currentProject.buildTypeTemplates}"/>

    <div class="clearfix">
      <c:url var="url" value="/admin/createBuildTypeTemplate.html?init=1&projectId=${currentProject.projectId}&cameFromUrl=${cameFromUrl}"/>
      <forms:addButton href="${url}">Create template</forms:addButton>
    </div>
    <c:if test="${not empty templates}">
    <l:tableWithHighlighting className="settings" mouseovertitle="Click to edit build configuration template" id="templates">
      <tr>
        <th class="name">Template name</th>
        <th class="runner" colspan="${fn:length(editableProjects) > 1 ? 4 : 3}">Build Steps</th>
      </tr>
      <c:forEach items="${templates}" var="template">
        <c:set var="onclick">BS.openUrl(event, '<admin:editTemplateLink withoutLink="true"
                                                                        templateId="${template.id}"
                                                                        cameFromUrl="${cameFromUrl}"/>');</c:set>
        <c:set var="numUsages" value="${template.numberOfUsages}"/>
        <tr>
          <td class="name highlight" onclick="${onclick}">
            <admin:templateUsageInfo template="${template}"/>
            <strong><c:out value="${template.name}"/></strong>
          </td>
          <td class="runner highlight" onclick="${onclick}">
            <admin:buildRunnersInfo buildRunners="${template.buildRunners}"/>
          </td>
          <td class="edit highlight" onclick="${onclick}">
            <admin:editTemplateMenu template="${template}" cameFromUrl="${cameFromUrl}">edit</admin:editTemplateMenu>
          </td>
          <td class="edit">
            <admin:templateActions template="${template}" editableProjects="${editableProjects}"/>
          </td>
        </tr>
      </c:forEach>
    </l:tableWithHighlighting>
    </c:if>
  </div>

  <admin:copyBuildTypeForm editableProjects="${editableProjects}"/>
  <admin:moveBuildTypeForm editableProjects="${editableProjects}"/>
  <admin:moveBuildTypeTemplateForm editableProjects="${editableProjects}"/>
</div>

<forms:modified/>
<admin:copyProjectForm/>
<admin:createBuildTypeFromTemplate project="${originalProject}" showTemplatesChooser="true"/>
