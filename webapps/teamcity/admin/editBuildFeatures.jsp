<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>

<c:set var="featurePlace" value="${param['featurePlace']}"/>
<c:choose>
  <c:when test="${featurePlace == 'GENERAL'}">
    <c:set var="sectionTitle">Additional Build Features</c:set>
    <c:set var="noFeaturesMessage">There are no build features configured.</c:set>
    <c:set var="settingName">build feature</c:set>
  </c:when>
  <c:otherwise>
    <c:set var="settingName">build failure condition</c:set>
  </c:otherwise>
</c:choose>

<br/>

<bs:refreshable containerId="buildTypeBuildFeatures_${featurePlace}" pageUrl="${pageUrl}">

<c:if test="${not empty sectionTitle}">
  <h2 class="noBorder">${sectionTitle}</h2>
</c:if>

<bs:messages key="featureUpdated_${featurePlace}"/>
<bs:messages key="featureAdded_${featurePlace}"/>
<bs:messages key="featureRemoved_${featurePlace}"/>

  <c:set var="tableRows">
  <c:forEach items="${buildForm.buildFeaturesBean.buildFeatureDescriptors}" var="featureInfo">
    <c:if test="${featureInfo.descriptor.buildFeature.placeToShow == featurePlace}">

      <c:set var="onclick" value=""/>
      <c:set var="onclick">onclick="BS.BuildFeatureDialog.showEditDialog('${featurePlace}', '${featureInfo.descriptor.id}', '${featureInfo.descriptor.type}', '${featureInfo.descriptor.buildFeature.displayName}'); Event.stop(event)"</c:set>
      <tr>
        <td class="highlight" ${onclick} style="vertical-align:top;">
          <admin:featureInfo feature="${featureInfo}" showDescription="false"/>
        </td>
        <td class="highlight" ${onclick}>
          <admin:featureInfo feature="${featureInfo}" showName="false"/>
        </td>
        <c:choose>
          <c:when test="${featureInfo.inherited}">
            <td class="highlight edit" ${onclick}><a href="#" ${onclick}>view</a></td>
          </c:when>
          <c:otherwise>
            <td class="highlight edit" ${onclick}><a href="#" ${onclick}>edit</a></td>
          </c:otherwise>
        </c:choose>
        <td class="edit">
          <c:set var="featureId" value="${fn:replace(featureInfo.descriptor.id, '.', '_')}"/>
          <bs:simplePopup controlId="featureActions${featureId}" linkOpensPopup="true"
                          popup_options="shift: {x: -150, y: 20}, className: 'quickLinksMenuPopup'">
            <jsp:attribute name="content">
              <div>
                <ul class="menuList">
                  <c:if test="${featureInfo.enabled}">
                    <l:li>
                      <a href="#" onclick="BS.AdminActions.setSettingsEnabled('${buildForm.settingsId}', '${featureInfo.descriptor.id}', false, '${settingName}'); return false">Disable ${settingName}</a>
                    </l:li>
                  </c:if>
                  <c:if test="${not featureInfo.enabled}">
                    <l:li>
                      <a href="#" onclick="BS.AdminActions.setSettingsEnabled('${buildForm.settingsId}', '${featureInfo.descriptor.id}', true, '${settingName}'); return false">Enable ${settingName}</a>
                    </l:li>
                  </c:if>
                  <c:if test="${not featureInfo.inherited}">
                  <l:li>
                    <a href="#" onclick="BS.BuildFeatureDialog.deleteFeature('${featurePlace}', '${featureInfo.descriptor.id}', '${param['messagePrefix']}'); return false">Delete ${settingName}</a>
                  </l:li>
                  </c:if>
                </ul>
              </div>
            </jsp:attribute>
            <jsp:body>more</jsp:body>
          </bs:simplePopup>
        </td>

      </tr>

    </c:if>
  </c:forEach>
</c:set>
<c:set var="tableRows" value="${fn:trim(tableRows)}"/>

<c:if test="${empty tableRows and not empty noFeaturesMessage}"><p>${noFeaturesMessage}</p></c:if>

<c:if test="${not empty tableRows}">

  <l:tableWithHighlighting highlightImmediately="true" className="parametersTable">
    <tr>
      <th style="width: 30%;">Type</th>
      <th colspan="3">Parameters Description</th>
    </tr>
    ${tableRows}
  </l:tableWithHighlighting>
</c:if>

<c:if test="${fn:length(buildForm.buildFeaturesBean.availableBuildFeatures) > 0}">
<div>
    <forms:addButton onclick="BS.BuildFeatureDialog.showAddDialog('${featurePlace}', this.innerHTML); return false">Add ${settingName}</forms:addButton>
</div>
</c:if>

<%@ include file="editBuildFeaturesDialog.jspf" %>

</bs:refreshable>

