<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<admin:editBuildTypePage selectedStep="">
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/admin/iprSettings.css
      /css/admin/runParams.css
    </bs:linkCSS>
  </jsp:attribute>
  <jsp:attribute name="body_include">

    <form id="editBuildTypeForm" action="<c:url value='/admin/editRunType.html?id=${buildForm.settingsId}&runnerId=${buildForm.buildRunnerBean.id}'/>"
          method="post" onsubmit="return BS.EditBuildRunnerForm.submitBuildRunner('${buildForm.settingsId}')">

    <div id="runnerParams" class="clearfix">
      <jsp:include page="/admin/runnerParams.html">
        <jsp:param name="runnerType" value="${buildForm.multipleRunnersBean.currentBuildRunnerBean.runnerType}"/>
      </jsp:include>
    </div>

    <script type="text/javascript">
      $('runnerParams').updateContainer = function(runType) {
        BS.Util.show('chooseRunnerProgress');
        BS.MultilineProperties.clearProperties();
        BS.EditBuildRunnerForm.removeUpdateStateHandlers();

        BS.ajaxUpdater(this, '<c:url value="/admin/runnerParams.html?id=${buildForm.settingsId}&runnerId=${buildForm.buildRunnerBean.id}&runnerType="/>' + runType, {
          evalScripts : true,
          onComplete: function() {
            if (runType == '') {
              $('saveButtons').hide();
            } else {
              $('saveButtons').show();
            }

            BS.EditBuildRunnerForm.setupEventHandlers('${buildForm.settingsId}');
            BS.Util.hide('chooseRunnerProgress');
            BS.EditBuildRunnerForm.saveInSession();
            BS.AvailableParams.attachPopups('settingsId=${buildForm.settingsId}', 'textProperty', 'multilineProperty');
            BS.EditBuildRunnerForm.setupCtrlEnterForTextareas('${buildForm.settingsId}');
          }
        });
      };

      <c:if test="${empty buildForm.multipleRunnersBean.currentBuildRunnerBean.runnerType}">
        $j(document).ready(function() {
          $j('#runnerType').prevAll('input').focus();
        });
      </c:if>
    </script>

    <c:if test="${not buildForm.buildRunnerBean.inherited}">
    <div class="saveButtonsBlock" id="saveButtons" style="${not buildForm.buildRunnerBean.runnerTypeSelected ? 'display:none' : 'display:block'}">
      <forms:cancel cameFromSupport="${buildForm.cameFromSupport}"/>
      <input type="hidden" id="submitBuildType" name="submitBuildType" value="1"/>
      <forms:submit name="submitButton" label="Save"/>
      <forms:saving/>

      <c:if test="${not buildForm.createMode}">
        <input type="hidden" value="${buildForm.numberOfSettingsChangesEvents}" name="numberOfSettingsChangesEvents"/>
      </c:if>
    </div>
    </c:if>

    </form>
    <forms:modified/>

    <script type="text/javascript">
    <c:if test="${buildForm.buildRunnerBean.inherited}">
    BS.EditBuildRunnerForm.setTemplateBased();
    </c:if>
    <c:if test="${not buildForm.buildRunnerBean.inherited}">
    BS.MultilineProperties.updateVisible();
    BS.EditBuildRunnerForm.setupEventHandlers('${buildForm.settingsId}');
    BS.EditBuildRunnerForm.setModified(${buildForm.buildRunnerBean.stateModified});
    </c:if>
    BS.AvailableParams.attachPopups('settingsId=${buildForm.settingsId}', 'textProperty', 'multilineProperty');
    BS.EditBuildRunnerForm.setupCtrlEnterForTextareas('${buildForm.settingsId}');
    </script>
  </jsp:attribute>
</admin:editBuildTypePage>
