<%@ include file="/include-internal.jsp" %>
<jsp:useBean id="editGroupBean" type="jetbrains.buildServer.controllers.admin.groups.EditGroupBean" scope="request"/>

<l:defineCurrentTab defaultTab="groupGeneralSettings"/>
<c:choose>
  <c:when test="${currentTab == 'groupGeneralSettings'}"><c:set var="pageTitle" scope="request" value="Edit Group ${editGroupBean.group.name}"/></c:when>
  <c:when test="${currentTab == 'groupUsers'}"><c:set var="pageTitle" scope="request" value="Edit Users of Group ${editGroupBean.group.name}"/></c:when>
  <c:when test="${currentTab == 'groupNotifications'}"><c:set var="pageTitle" scope="request" value="Edit Notification Rules of Group ${editGroupBean.group.name}"/></c:when>
  <c:otherwise><c:set var="pageTitle" scope="request" value="Edit Roles of ${editGroupBean.group.name}"/></c:otherwise>
</c:choose>
<bs:page>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/admin/userListFilter.css
      /css/admin/userGroups.css
      /css/userRoles.css
      /css/admin/adminMain.css
      /css/notificationRules.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/bs/userGroups.js
      /js/bs/queueLikeSorter.js
      /js/bs/notificationRules.js
    </bs:linkScript>
    <script type="text/javascript">
    BS.Navigation.items = [
      {title: "Administration", url: '<c:url value="/admin/admin.html"/>'},
      <forms:cameBackNav cameFromSupport="${editGroupBean.cameFromSupport}"/>
      {title: '<bs:escapeForJs text="${editGroupBean.group.name}" forHTMLAttribute="true"/>', selected:true}
    ];
    </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <script type="text/javascript">
      (function() {
        var paneNav = new TabbedPane();

        function initialize3dLevel() {
          var baseUrl = "<c:url value='/admin/editGroup.html?init=1&groupCode=${editGroupBean.group.key}&tab='/>";
          paneNav.addTab("groupGeneralSettings", {
            caption: "General",
            titleText: "General",
            url: baseUrl + "groupGeneralSettings"
          });

          paneNav.addTab("groupUsers", {
            caption: "Users",
            titleText: "Users",
            url: baseUrl + "groupUsers"
          });

        <c:if test="${editGroupBean.perProjectPermissionsEnabled}">
          paneNav.addTab("groupRoles", {
            caption: "Roles",
            titleText: "Roles",
            url: baseUrl + "groupRoles"
          });
        </c:if>

          paneNav.addTab("groupNotifications", {
            caption: "Notification Rules",
            titleText: "Notification Rules",
            url: baseUrl + "groupNotifications"
          });

          paneNav.setActiveCaption('${currentTab}');
        }
        initialize3dLevel();

        paneNav.showIn('tabsContainer3');
      })();
    </script>

    <div id="container" class="clearfix">
      <jsp:include page="${currentTab}.jsp"/>
    </div>

  </jsp:attribute>
</bs:page>
