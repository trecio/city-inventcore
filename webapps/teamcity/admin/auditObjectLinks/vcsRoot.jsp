<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="afn" uri="/WEB-INF/functions/authz"
%><c:set var="vcsRoot" value="${_object}"
/><c:choose><c:when test="${afn:canEditVcsRoot(vcsRoot)}"><admin:editVcsRootLink vcsRoot="${vcsRoot}" cameFromUrl="${pageUrl}" editingScope="none"><c:out value="${vcsRoot.name}"/></admin:editVcsRootLink></c:when><c:otherwise><c:out value="${vcsRoot.name}"/></c:otherwise></c:choose>