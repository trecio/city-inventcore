<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>

<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<c:set var="buildTriggersBean" value="${buildForm.buildTriggerDescriptorBean}"/>
<admin:editBuildTypePage selectedStep="buildTriggers">
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/admin/editTriggers.css
    </bs:linkCSS>
    <style type="text/css">
      #editTriggerDialog {
        width: 40em;
      }

      .buildTriggersTable td.edit {
        width: 7%;
      }
    </style>
    <script type="text/javascript">
      <c:forEach items="${buildTriggersBean.allTriggerServices}" var="tr">
      BS.EditTriggersDialog.allTriggers['${tr.name}'] = '<bs:escapeForJs text="${tr.displayName}"/>';
      </c:forEach>
    </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <h2 class="noBorder">Build Triggering</h2>

    <p class="sectionDescription">Build triggers are used to add builds to the queue either when an event occurs (like a VCS check-in) or periodically with some configurable interval.</p>

    <bs:refreshable containerId="triggersTable" pageUrl="${pageUrl}">

    <bs:messages key="buildTriggersUpdated"/>

    <c:if test="${empty buildTriggersBean.triggers}">
      <p>There are no build triggers.</p>
    </c:if>

    <c:if test="${not empty buildTriggersBean.triggers}">
      <l:tableWithHighlighting highlightImmediately="true" id="buildTriggersTable" className="parametersTable">
        <tr>
          <th style="width: 30%;">Build Trigger</th>
          <th colspan="3">Parameters Description</th>
        </tr>
        <c:forEach items="${buildTriggersBean.triggers}" var="trigger">
          <c:set var="onclick">onclick="BS.EditTriggersDialog.showEditDialog('${trigger.triggerName}', '${trigger.id}', '${trigger.originalTrigger.buildTriggerService.displayName}'); Event.stop(event)"</c:set>
          <tr>
            <td class="highlight" ${onclick} style="white-space: nowrap;">
              <admin:triggerInfo trigger="${trigger}" showDescription="false"/>
            </td>
            <td class="highlight" ${onclick}>
              <admin:triggerInfo trigger="${trigger}" showName="false"/>
            </td>
            <c:choose>
              <c:when test="${trigger.inherited}">
                <td class="highlight edit" ${onclick}><a href="#" ${onclick}>view</a></td>
              </c:when>
              <c:otherwise>
                <td class="highlight edit" ${onclick}><a href="#" ${onclick}>edit</a></td>
              </c:otherwise>
            </c:choose>
            <td class="edit">
              <c:set var="triggerId" value="${fn:replace(trigger.id, '.', '_')}"/>
              <bs:simplePopup controlId="triggerActions${triggerId}" linkOpensPopup="true"
                              popup_options="shift: {x: -150, y: 20}, className: 'quickLinksMenuPopup'">
                <jsp:attribute name="content">
                  <div>
                    <ul class="menuList">
                      <c:if test="${trigger.enabled}">
                        <l:li>
                          <a href="#" onclick="BS.AdminActions.setSettingsEnabled('${buildForm.settingsId}', '${trigger.id}', false, 'Build trigger'); return false">Disable build trigger</a>
                        </l:li>
                      </c:if>
                      <c:if test="${not trigger.enabled}">
                        <l:li>
                          <a href="#" onclick="BS.AdminActions.setSettingsEnabled('${buildForm.settingsId}', '${trigger.id}', true, 'Build trigger'); return false">Enable build trigger</a>
                        </l:li>
                      </c:if>
                      <c:if test="${not trigger.inherited}">
                      <l:li>
                        <a href="#" onclick="BS.EditTriggersDialog.removeTrigger('${trigger.id}'); return false">Delete build trigger</a>
                      </l:li>
                      </c:if>
                    </ul>
                  </div>
                </jsp:attribute>
                <jsp:body>more</jsp:body>
              </bs:simplePopup>
            </td>
          </tr>
        </c:forEach>
      </l:tableWithHighlighting>
    </c:if>

    <div>
      <forms:addButton onclick="BS.EditTriggersDialog.showAddDialog(this.innerHTML); return false" showdiscardchangesmessage="false">Add new trigger</forms:addButton>
    </div>

    <c:url var="actionUrl" value="/admin/editTriggers.html?id=${buildForm.settingsId}"/>
    <bs:modalDialog formId="editTrigger"
                    action="${actionUrl}"
                    title=""
                    closeCommand="BS.EditTriggersDialog.close()"
                    saveCommand="BS.EditTriggersDialog.submitForm()">

      <table class="runnerFormTable" style="width: 99%;">
        <tr>
          <td>
            <forms:select name="triggerNameSelector" enableFilter="true" onchange="BS.EditTriggersDialog.triggerChanged($('triggerNameSelector').options[$('triggerNameSelector').selectedIndex].value, '${buildForm.settingsId}');" style="width: 96%;">
              <option value="">-- Please choose a trigger --</option>
              <c:forEach items="${buildTriggersBean.availableTriggerServices}" var="triggerService">
                <forms:option value="${triggerService.name}"><c:out value="${triggerService.displayName}"/></forms:option>
              </c:forEach>
            </forms:select><forms:saving id="loadParamsProgress" className="progressRingInline"/>
          </td>
        </tr>
      </table>

      <div id="triggerParams"></div>

      <div class="popupSaveButtonsBlock" id="triggerSaveButtonsBlock">
        <forms:cancel onclick="BS.EditTriggersDialog.close()"/>
        <forms:submit name="editTrigger" id="editTriggerSubmit" label="Save"/>
        <input type="hidden" name="editMode" value=""/>
        <input type="hidden" name="triggerId" value=""/>
        <input type="hidden" name="triggerName" value=""/>
        <input type="hidden" name="publicKey" value="${buildForm.publicKey}"/>
        <forms:saving id="savingTrigger"/>
      </div>
    </bs:modalDialog>
  </bs:refreshable>
  </jsp:attribute>
</admin:editBuildTypePage>
