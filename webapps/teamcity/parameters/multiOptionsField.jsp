<%@include file="/include-internal.jsp"%>
<jsp:useBean id="context" scope="request" type="jetbrains.buildServer.controllers.parameters.ParameterRenderContext"/>
<jsp:useBean id="options" scope="request" type="java.util.Collection< jetbrains.buildServer.controllers.parameters.types.SelectParameterTypeBase.KeyValue >"/>
<jsp:useBean id="valueSeparator" scope="request" type="java.lang.String"/>

<c:set var="containerId" value="custom_control_${context.id}_container"/>

<input type="hidden" name="${context.id}" id="${context.id}" value="<c:out value='${context.parameter.value}'/>" />
<div id="${containerId}" style="width: 100%">
  <c:forEach var="it" items="${options}" varStatus="vs">
    <div>
      <c:set var="cbId" value="mcb_${containerId}_${vs.index}"/>
      <forms:checkbox name="" id="${cbId}" value="${it.key}" style="vertical-align:middle; margin:0; padding:0; width: auto;"/>
      <label for="${cbId}"><c:out value="${it.value}"/></label>
    </div>
  </c:forEach>
</div>

<script type="text/javascript">
    (function($) {
      var sep = '<bs:forJs>${valueSeparator}</bs:forJs>';

      var selectRoot = function() { return $(BS.Util.escapeId("${containerId}")); };
      var selectVal = function() { return $(BS.Util.escapeId("${context.id}")); };

      var updateFromCheckboxes = function() {
        var checked = selectRoot().find("input:checked");

        var str = "";
        checked.each(function () {
          if (str.length != 0) str += sep;
          str += $(this).val();
        });
        selectVal().val(str);
      };

      var updateToCheckboxes = function() {
        var selectedSet = {};
        selectVal().val().split(sep).each( function(v,i) { selectedSet[v] = v; });

        selectRoot().find("input[type='checkbox']").each(function() {
          var attr = $(this).attr("value");
          var selected = !!selectedSet[attr];
          $(this).prop('checked', selected);
        })
      };

      selectRoot().find("input[type='checkbox']").change(updateFromCheckboxes);
      selectVal().change(updateToCheckboxes);

      updateToCheckboxes();

    })(jQuery);
</script>
