<%@include file="/include-internal.jsp"%>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props"  %>
<jsp:useBean id="context" scope="request" type="jetbrains.buildServer.controllers.parameters.ParameterEditContext"/>
<jsp:useBean id="itemsList" scope="request" type="java.lang.String"/>

<tr>
  <th>Allow multiple:</th>
  <td>
    <props:checkboxProperty name="multiple"/>
    <label for="multiple">Allow multiple selection</label>
  </td>
</tr>

<tr id="multiselectSeparator">
  <th>Value separator:</th>
  <td>
    <props:textProperty name="valueSeparator" className="longField"/>
    <span class="smallNote">Specify multiple selected items separator. Leave blank to use ','.</span>
  </td>
</tr>

<tr>
  <th>Items<l:star/>:</th>
  <td>
    <props:multilineProperty
        name="itemsList"
        linkTitle="Edit items"
        cols="60"
        rows="5"
        value="${itemsList}"
        expanded="${true}"/>
    <span class="smallNote">
      Specify items for the control. Each item on a new line. Use <em>label => value</em> or <em>value</em>
    </span>
  </td>
</tr>



<script type="text/javascript">
  (function(){
    var update = function() {
      if ($('multiple').checked) {
        BS.Util.show('multiselectSeparator');
      } else {
        BS.Util.hide('multiselectSeparator');
      }
    };

    $('multiple').on('change', update);
    update();
  })();
</script>