<%@include file="/include-internal.jsp"%>
<jsp:useBean id="context" scope="request" type="jetbrains.buildServer.controllers.parameters.ParameterRenderContext"/>

<forms:textField expandable="${true}" name="${context.id}" id="${context.id}" value="${context.parameter.value}" style="width:100%"/>
