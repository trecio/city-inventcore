<%@include file="/include-internal.jsp"%>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props"  %>
<jsp:useBean id="context" scope="request" type="jetbrains.buildServer.controllers.parameters.ParameterEditContext"/>
<jsp:useBean id="regexp" scope="request" type="java.lang.String"/>

<tr>
  <th>Pattern: </th>
  <td>
    <props:textProperty name="regexp" className="longField" value="${regexp}"/>
    <span class="smallNote">Specify a Java-style regular expression to validate field value</span>
    <span id="error_regexp" class="error"></span>
  </td>
</tr>

