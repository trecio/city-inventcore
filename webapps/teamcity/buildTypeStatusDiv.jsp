<%@ include file="include-internal.jsp" %><%@
    taglib prefix="resp" tagdir="/WEB-INF/tags/responsible"%><%@
    taglib prefix="responsible" uri="/WEB-INF/functions/resp" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin"%><%@
    taglib prefix="user" tagdir="/WEB-INF/tags/userProfile"%><%@
    taglib prefix="t" tagdir="/WEB-INF/tags/tags"

%><jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.SBuildType" scope="request"
/><jsp:useBean id="currentUser" type="jetbrains.buildServer.users.SUser" scope="request"
/><jsp:useBean id="pinnedBuild" type="jetbrains.buildServer.controllers.buildType.BuildTypeController.PinnedBuildBean" scope="request"
/><jsp:useBean id="shortHistory" type="java.util.List" scope="request"
/><jsp:useBean id="hasCompatibleAgentsOrTypesToRun" type="java.lang.Boolean" scope="request"
/><jsp:useBean id="historyForm" type="jetbrains.buildServer.controllers.buildType.tabs.HistorySearchBean" scope="request"
/><jsp:useBean id="showHistoryLink" type="java.lang.Boolean" scope="request"
/><c:set var="buildTypeId" value="${buildType.buildTypeId}"
/>
<div id="buildTypeStatusDiv">
  <%--@elvariable id="serverTC" type="jetbrains.buildServer.serverSide.BuildServerEx"--%>
  <%--@elvariable id="pendingChanges" type="java.util.Collection"--%>
  <%--@elvariable id="buildTypeBranch" type="jetbrains.buildServer.serverSide.BranchEx"--%>
  <c:choose>
    <c:when test="${not empty buildTypeBranch}">
      <jsp:useBean id="buildTypeBranch" type="jetbrains.buildServer.serverSide.BranchEx" scope="request"/>
      <c:set var="runningBuilds" value="<%=buildTypeBranch.getRunningBuilds(currentUser)%>"/>
      <c:set var="queuedBuilds" value="${buildTypeBranch.queuedBuilds}"/>
    </c:when>
    <c:otherwise>
      <c:set var="runningBuilds" value="<%=buildType.getRunningBuilds(currentUser)%>"/>
      <c:set var="queuedBuilds" value="${buildType.queuedBuilds}"/>
    </c:otherwise>
  </c:choose>
  <div class="divsWithHeaders">
    <div class="first clearfix" id="pendingChangesDiv">
      <h2>Pending changes</h2>
      <c:if test="${not empty pendingChanges}">
        <bs:pendingChangesLink buildType="${buildType}"
                               pendingChanges="${pendingChanges}"
                               showForAllBranches="${branchBean.wildcardBranch}"
                               branch="${buildTypeBranch}">
          ${fn:length(pendingChanges)} change<bs:s val="${fn:length(pendingChanges)}"/>
        </bs:pendingChangesLink>
      </c:if>
      <c:if test="${empty pendingChanges}">No pending changes</c:if>
    </div>

    <div>
      <h2>Current status</h2>
      <bs:messages key="buildNotFound"/>

      <bs:buildTypeStatusText theRunningBuilds="${runningBuilds}"
                              queuedBuilds="${queuedBuilds}"
                              buildType="${buildType}"
                              branch="${buildTypeBranch}"
                              noPopupForRunning="true"/>
      <c:if test="${not hasCompatibleAgentsOrTypesToRun}">
        &nbsp;
        <a class="noCompatibleAgentsLink" href="viewType.html?tab=compatibilityList&buildTypeId=${buildTypeId}">No suitable agents</a>
      </c:if>
      <bs:systemProblemMarker buildTypeId="${buildTypeId}" branch="${buildTypeBranch}" maxWidth="100"/>
      <c:if test="${not empty runningBuilds}">
        <div class="runningTableContainer">
          <table cellspacing="0" class="overviewTypeTable running">
            <c:forEach items="${runningBuilds}" var="buildData">
              <tr>
                <bs:buildRow build="${buildData}"
                             maxBranchNameLength="28"
                             showBranchName="true"
                             showBuildNumber="true"
                             showStatus="true"
                             showArtifacts="true"
                             showCompactArtifacts="false"
                             showChanges="true"
                             showProgress="true"
                             showStop="true"/>
              </tr>
            </c:forEach>
          </table>
        </div>
      </c:if>
    </div>

    <c:if test="${(not buildType.status.successful && buildType.lastChangesFinished != null) or
                   responsible:isActive(buildType.responsibilityInfo) or responsible:isFixed(buildType.responsibilityInfo)}">
      <div>
        <h2>Investigation</h2>
        <resp:responsible buildData="${buildType.lastChangesFinished}" server="${serverTC}" currentUser="${currentUser}"/>
      </div>
    </c:if>

    <div>
      <bs:refreshable containerId="historyTable" pageUrl="${pageUrl}">
        <div>
          <c:if test="${not empty buildType.tags or not empty historyForm.tag}">
            <c:set var="tagsFilterBlock">
              <div id="filterByTags">
                Filter by tag: &nbsp;
                <t:showTags tags="${buildType.tags}"
                            buildTypeId="${buildTypeId}"
                            selectedTag="${historyForm.tag}"
                            onclick="$('historyFilter').elements['tag'].value = this.innerHTML; BS.HistoryTable.doSearch(); return false;"/>
                <c:if test="${not empty historyForm.tag}">
                  <forms:resetFilter resetHandler="$('historyFilter').elements['tag'].value = ''; BS.HistoryTable.doSearch();"/>
                </c:if>
              </div>
            </c:set>
            <c:set var="fullWidth" value="true"/>
          </c:if>

          <h2 style="${fullWidth ? "width: 100%;" : ''}">Recent history</h2>

          <c:if test="${not empty shortHistory or (not empty historyForm.tag and not branchBean.wildcardBranch)}">
            <div id="filterAboveTable">
              <form id="historyFilter" action="<c:url value='/viewType.html'/>" method="post" style="float: right;">
                <input type="hidden" name="buildTypeId" value="${buildTypeId}"/>
                <input type="hidden" name="tab" value="buildTypeStatusDiv"/>
                <input type="hidden" name="tag" value="${historyForm.tag}"/>
                <forms:checkbox name="showAllBuilds" checked="${historyForm.showAllBuilds}" onclick="BS.HistoryTable.doSearch()"/>
                <label for="showAllBuilds">Show canceled builds and builds that failed to start</label>
              </form>

              <div id="savingFilter">
                <forms:progressRing className="progressRingInline"/>
                Updating results...
              </div>
            </div>

            ${tagsFilterBlock}
          </c:if>
          <c:if test="${empty shortHistory}"><em>No builds available</em></c:if>
        </div>

        <c:if test="${not empty shortHistory}">
          <bs:historyTable historyRecords="${shortHistory}" buildType="${buildType}"/>
        </c:if>

        <c:if test="${showHistoryLink}">
          <c:url var="history_url" value="/viewType.html?buildTypeId=${buildTypeId}&tab=buildTypeHistoryList"/>
          <div id="showHistory">
            Showing ${fn:length(shortHistory)} build<bs:s val="${fn:length(shortHistory)}"/>, see <a href="${history_url}">entire history</a>
          </div>
        </c:if>
        <c:if test="${pinnedBuild.active}">
          <script type="text/javascript">
            BS.Pin.showSuccessMessage(${pinnedBuild.buildId}, ${pinnedBuild.pinned});
          </script>
        </c:if>
      </bs:refreshable>
    </div>

    <c:if test="${not empty shortHistory}">
      <div>
        <h2>Permalinks</h2>

        <span class="permalinks">
          <img src="img/buildStates/success_small.gif" alt="Successful build"/>
          <bs:buildLink buildId="lastSuccessful" buildTypeId="${buildTypeId}" title="Last successful build">Last successful build</bs:buildLink>
          <img src="img/pinned.png" alt="Last pinned build"/>
          <bs:buildLink buildId="lastPinned" buildTypeId="${buildTypeId}" title="Last pinned build">Last pinned build</bs:buildLink>
          <img src="img/star.gif" alt="Last finished build"/>
          <bs:buildLink buildId="lastFinished" buildTypeId="${buildTypeId}" title="Last finished build">Last finished build</bs:buildLink>
        </span>
      </div>
    </c:if>
  </div>
</div>

<script type="text/javascript">
  $j('.fading').each(function() {
    BS.Highlight(this);
  });
  BS.Branch.injectBranchParamToLinks($j("#pendingChangesDiv, #showHistory"), "${buildType.projectId}");
  $j(document).ready(function() {
    BS.SystemProblems.setOptions({btId:'${buildType.buildTypeId}'});
    BS.SystemProblems.startUpdates();
  });
</script>
