<%@ include file="include-internal.jsp" %>
<jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.BuildTypeEx" scope="request"/>
<jsp:useBean id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary" scope="request"/>
<c:set var="numberOfAgents" value="${serverSummary.authorizedAgentsCount}"/>
<p>There <bs:are_is val="${numberOfAgents}"/> ${numberOfAgents} agent<bs:s val="${numberOfAgents}"/> authorized.
<authz:authorize projectId="${buildType.projectId}" allPermissions="EDIT_PROJECT">
  <span id="editLink">To change number of compatible agents for this build configuration, please
    <admin:editBuildTypeLink buildTypeId="${buildType.buildTypeId}" step="requirements" title="Edit requirements">edit requirements</admin:editBuildTypeLink>.
  </span>
</authz:authorize>
</p>
<bs:buildTypeCompatibility compatibleAgents="${buildType}" project="${buildType.project}"/>
