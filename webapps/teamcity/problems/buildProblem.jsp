<%@ page import="jetbrains.buildServer.BuildProblemData" %>
<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="ext" tagdir="/WEB-INF/tags/ext" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>

<jsp:useBean id="buildProblem" type="jetbrains.buildServer.serverSide.problems.BuildProblem" scope="request"/>
<jsp:useBean id="buildData" type="jetbrains.buildServer.serverSide.SBuild" scope="request"/>

<%--<c:set var="buildProblemTitle">--%>
  <%--type: <c:out value="${buildProblem.buildProblemData.type}"/>, identity: <c:out value="${buildProblem.buildProblemData.identity}"/>--%>
<%--</c:set>--%>

<c:set var="buildProblemClass" value="buildProblem"/>

<c:if test="${buildProblem.mutedInBuild}">
  <c:set var="buildProblemClass" value="buildProblem ignoredBuildProblem"/>
</c:if>

<tr>
  <td><bs:icon icon="${buildProblem.mutedInBuild ? '../muted.gif' : 'error.gif'}"/></td>

  <td class="${buildProblemClass}">

    <c:if test="${empty requestScope['hideBuildProblemDescription']}">
      <c:set var="problemBody">

        <c:set var="typeDescr" value="${buildProblem.typeDescription}"/>
        <c:set var="descr" value="${buildProblem.buildProblemData.description}"/>

        <span title="${buildProblem.mutedInBuild ? 'Build problem is muted in this build' : ''}">
          <c:if test="${not empty typeDescr}"><c:out value="${typeDescr}"/></c:if><c:if test="${not empty typeDescr and not empty descr}">:&nbsp;</c:if><bs:out value="${descr}"/>
        </span>
      </c:set>
    </c:if>

    ${problemBody}

    <c:if test="${buildProblem.mutedInBuild}">
      <span class="ignoredInfo">
        &mdash; Muted in this build

        <c:set var="muteInfo" value="${buildProblem.muteInBuildInfo}"/>
        <c:if test="${not empty muteInfo}">
          on <bs:date value="${buildProblem.muteInBuildInfo.mutingTime}"/> by <c:out
            value="${buildProblem.muteInBuildInfo.mutingUser.descriptiveName}"/>
          <c:if test="${not empty buildProblem.muteInBuildInfo.mutingComment}">
            with comment: <i><bs:out value="${buildProblem.muteInBuildInfo.mutingComment}"/></i>
          </c:if>
        </c:if>

      </span>
    </c:if>
  </td>
</tr>

<tr>
  <td colspan="2">
    <ext:includeExtensions placeId="<%=PlaceId.BUILD_RESULTS_BUILD_PROBLEM%>"/>
  </td>
</tr>

<script type="text/javascript">
  BS.BuildProblemActions = {
    url: window['base_uri'] + "/problems/buildProblemAction.html",

    deleteBuildProblem: function(problemIdentity, buildId) {
      if (!confirm('Are you sure you want to delete this build problem?')) return false;

      BS.ajaxRequest(this.url, {
        parameters: {'buildId':  buildId,
                     'problemIdentity': problemIdentity},
        onSuccess: function() {
          window.location.reload();
        }
      });
    }
  };
</script>
