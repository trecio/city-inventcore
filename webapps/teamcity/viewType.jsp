<%@ page import="jetbrains.buildServer.controllers.BranchUtil" %><%@
    page import="jetbrains.buildServer.web.openapi.PlaceId" %><%@
    include file="include-internal.jsp" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %><%@
    taglib prefix="resp" tagdir="/WEB-INF/tags/responsible" %><%@
    taglib prefix="tags" tagdir="/WEB-INF/tags/tags"

%><jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.BuildTypeEx" scope="request"
/><jsp:useBean id="pinnedBuild" type="jetbrains.buildServer.controllers.buildType.BuildTypeController.PinnedBuildBean" scope="request"
/><jsp:useBean id="currentUser" type="jetbrains.buildServer.users.User" scope="request"
/><jsp:useBean id="branchBean" type="jetbrains.buildServer.controllers.BranchBean" scope="request"
/><jsp:useBean id="branchSelectorEnabled" type="java.lang.Boolean" scope="request"

/><c:set var="fullname" value="${buildType.fullName}"
/><ext:defineExtensionTab placeId="<%=PlaceId.BUILD_CONF_TAB%>"/>
<%--@elvariable id="extensionTab" type="jetbrains.buildServer.web.openapi.CustomTab"--%>
<c:set var='currentTabTitle' value=" > ${extensionTab.tabTitle}"
/><c:set var="pageTitle" value="${fullname}${currentTabTitle}" scope="request"
/><c:set var="pendingChanges" value="<%=BranchUtil.getPendingChangesInBranch(buildType, branchBean.getUserBranch())%>" scope="request"
/><c:set var="projectId" value="${buildType.projectId}"
/><bs:page>
  <jsp:attribute name="quickLinks_include">
    <authz:authorize projectId="${projectId}" allPermissions="RUN_BUILD">
      <div class="toolbarItem">
        <bs:runBuild buildTypeId="${buildType.buildTypeId}" redirectTo=""/>
      </div>
    </authz:authorize>
    <authz:authorize projectId="${projectId}"
                     anyPermission="PAUSE_ACTIVATE_BUILD_CONFIGURATION, CLEAN_BUILD_CONFIGURATION_SOURCES, RUN_BUILD, ASSIGN_INVESTIGATION">
      <div class="toolbarItem">
        <bs:simplePopup controlId="bcActions" linkOpensPopup="true"
                        popup_options="delay: 0, shift: {x: -150, y: 20}, className: 'quickLinksMenuPopup'">
          <jsp:attribute name="content">
            <div id="btDetails">
              <ul class="menuList">
                <authz:authorize projectId="${projectId}" allPermissions="PAUSE_ACTIVATE_BUILD_CONFIGURATION">
                  <c:if test="${buildType.paused and not buildType.project.archived}">
                    <l:li>
                      <a href="#" onclick="<bs:_pauseBuildTypeLinkOnClick buildType="${buildType}" pause="false"/>; return false">
                        Activate build configuration...
                      </a>
                    </l:li>
                  </c:if>
                  <c:if test="${not buildType.paused}">
                    <l:li>
                      <a href="#" onclick="<bs:_pauseBuildTypeLinkOnClick buildType="${buildType}" pause="true"/>; return false">
                        Pause build configuration...
                      </a>
                    </l:li>
                  </c:if>
                </authz:authorize>
                <authz:authorize projectId="${projectId}" allPermissions="RUN_BUILD">
                  <l:li>
                    <form class="pauseResume" action="<c:url value='/action.html'/>" method="post" id="checkForChangesBuildTypeForm">
                      <input type="hidden" name="checkForChangesBuildType" value="${buildType.buildTypeId}"/>
                    </form>
                    <a href="#" onclick="$('checkForChangesBuildTypeForm').submit(); return false;">Check for pending changes</a>
                  </l:li>
                </authz:authorize>
                <c:if test="${afn:permissionGrantedForBuildType(buildType, 'CLEAN_BUILD_CONFIGURATION_SOURCES') or
                              afn:permissionGrantedGlobally('CLEAN_AGENT_SOURCES')}">
                  <l:li>
                    <a href="#" onclick="BS.BuildTypeResetSources.showResetSourcesDialog(); return false">Enforce clean checkout...</a>
                  </l:li>
                </c:if>
                <authz:authorize projectId="${projectId}" allPermissions="ASSIGN_INVESTIGATION">
                  <l:li>
                    <c:set var="name"><bs:escapeForJs text="${buildType.name}" forHTMLAttribute="true"/></c:set>
                    <a href="#" onclick="return BS.ResponsibilityDialog.showDialog('${buildType.buildTypeId}', '${name}')">Investigate...</a>
                  </l:li>
                </authz:authorize>
              </ul>
            </div>
          </jsp:attribute>
          <jsp:body>Actions</jsp:body>
        </bs:simplePopup>
      </div>
    </authz:authorize>
    <authz:editBuildTypeGranted buildType="${buildType}">
      <admin:editBuildTypeMenu buildType="${buildType}">Edit Configuration Settings</admin:editBuildTypeMenu>
    </authz:editBuildTypeGranted>
  </jsp:attribute>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/pager.css
      /css/progress.css
      /css/modificationListTable.css
      /css/compatibilityList.css
      /css/buildTypeSettings.css
      /css/filePopup.css
      /css/viewType.css
      /css/overviewTable.css
      /css/buildQueue.css
      /css/historyTable.css
      /css/agentsInfoPopup.css
      /css/tags.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/bs/blocks.js
      /js/bs/blocksWithHeader.js
      /js/bs/blockWithHandle.js
      /js/bs/changesBlock.js
      /js/bs/collapseExpand.js
      /js/bs/pin.js
      /js/bs/buildComment.js

      /js/bs/runningBuilds.js

      /js/bs/systemProblemsMonitor.js
      /js/bs/tags.js
      /js/bs/overflower.js
      /js/bs/queueLikeSorter.js
      /js/bs/buildQueue.js
      /js/bs/historyTable.js
      /js/bs/buildType.js
      /js/bs/testGroup.js
    </bs:linkScript>
    <c:if test="${not empty buildType.description}">
      <c:set var="buildTypeDescription"><bs:out value="${buildType.description}" /></c:set>
      <c:set var="buildTypeDescription">(<bs:escapeForJs text="${buildTypeDescription}" forHTMLAttribute="false"/>)</c:set>
    </c:if>

    <script type="text/javascript">
      <bs:trimWhitespace>
        BS.Navigation.items = [
          {
            title: "<bs:escapeForJs text="${buildType.projectName}" forHTMLAttribute="true"/>",
            url: "project.html?projectId=${projectId}",
            selected: false,
            itemClass: "project"
          },
          {
            title: "<bs:escapeForJs text="${buildType.name}" forHTMLAttribute="true"/> <small>${buildTypeDescription}</small>",
            url: "viewType.html?buildTypeId=${buildType.buildTypeId}",
            selected: true,
            itemClass: "buildType ${buildType.status.failed ? 'failed' : ''}",
            buildTypeId: "${buildType.buildTypeId}",
            siblings: {
              type: "buildType",
              urlParamName: "buildTypeId",
              urlValuePrefix: "",
              buildTypes: [
                <c:forEach items="${buildType.project.buildTypes}" var="bt" varStatus="pos">
                  {
                    id: "${bt.buildTypeId}",
                    name: "<bs:escapeForJs text="${bt.name}"/>"
                    <c:if test="${buildType.buildTypeId == bt.buildTypeId}">,
                    selected: true
                    </c:if>
                  }
                  <c:if test="${not pos.last}">,</c:if>
                </c:forEach>
              ]
            }
          }
        ];

      $j(document).ready(function() {
        BS.RunningBuilds.startUpdates();
      });
      </bs:trimWhitespace>
    </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <et:subscribeOnProjectEvents projectId="${projectId}">
      <jsp:attribute name="eventNames">
        PROJECT_REMOVED
        PROJECT_RESTORED
        PROJECT_PERSISTED
        PROJECT_ARCHIVED
        PROJECT_DEARCHIVED
      </jsp:attribute>
      <jsp:attribute name="eventHandler">
        BS.reload();
      </jsp:attribute>
    </et:subscribeOnProjectEvents>
    <et:subscribeOnBuildTypeEvents buildTypeId="${buildType.buildTypeId}">
      <jsp:attribute name="eventNames">
        BUILD_TYPE_UNREGISTERED
      </jsp:attribute>
      <jsp:attribute name="eventHandler">
        BS.reload();
      </jsp:attribute>
    </et:subscribeOnBuildTypeEvents>
    <et:subscribeOnBuildTypeEvents buildTypeId="${buildType.buildTypeId}">
      <jsp:attribute name="eventNames">
        BUILD_STARTED
        BUILD_CHANGES_LOADED
        BUILD_FINISHED
        BUILD_INTERRUPTED
        BUILD_TYPE_ACTIVE_STATUS_CHANGED
        BUILD_TYPE_ADDED_TO_QUEUE
        BUILD_TYPE_REMOVED_FROM_QUEUE
        CHANGE_ADDED
      </jsp:attribute>
      <jsp:attribute name="eventHandler">
        BS.BuildType.updateView();
      </jsp:attribute>
    </et:subscribeOnBuildTypeEvents>
    <bs:refreshable containerId="buildConfigurationContainer" pageUrl="${pageUrl}">
      <script type="text/javascript">
        <bs:trimWhitespace>
          (function() {
            if (BS.viewTypeTabPane instanceof TabbedPane) {
              BS.viewTypeTabPane.clearTabs();
            } else {
              BS.viewTypeTabPane = new TabbedPane();
            }

            function initialize3dLevel(paneNav, myTabs) {
              paneNav.clearTabs();
              var baseUrl = "viewType.html?buildTypeId=${buildType.buildTypeId}&tab=";
              for(var tabCode in myTabs)
                paneNav.addTab(tabCode, {
                  caption: myTabs[tabCode].title,
                  url: baseUrl + tabCode
                });

              paneNav.setActiveCaption('${extensionTab.tabId}');

              <c:if test="${branchBean.hasBranches}">
                $j(document).on("bs.breadcrumbRendered", function(event) {
                  BS.Branch.installDropdownToBreadcrumb("<bs:escapeForJs text="${branchBean.userBranch}"/>", ${branchSelectorEnabled},
                                                        <bs:_branchesListJs branches="${branchBean.activeBranches}"/>,
                                                        <bs:_branchesListJs branches="${branchBean.otherBranches}"/>,
                                                        {projectId: "${projectId}", tab: "${extensionTab.tabId}"});
                  BS.Branch.injectBranchParamToLinks($j("#main_navigation"), "${projectId}");
                });
              </c:if>
            }

            var viewTypeTabs = {};
            var title;
            // Process plugins/tabs:
            <c:set var='selectedTabExtension' value="${null}"/>
            <ext:forEachTab placeId="<%=PlaceId.BUILD_CONF_TAB%>">
            // plugin ${extension.tabId}:
            title = '${extension.tabTitle}';
            <c:choose>
              <c:when test="${extension.tabId eq 'pendingChangesDiv'}">
                 title += ' (${fn:length(pendingChanges)})';
              </c:when>
              <c:when test="${extension.tabId eq 'compatibilityList'}">
                <c:set var="agentsCount"><bs:buildTypeCompatibility compatibleAgents="${buildType}" showActiveCompatibleCount="${true}" project="${buildType.project}"/></c:set>
                title += ' (${fn:trim(agentsCount)})';
              </c:when>
            </c:choose>
            viewTypeTabs['${extension.tabId}'] = { title: title };
            <c:if test="${extensionTab.tabId == extension.tabId}"><c:set var='selectedTabExtension' value="${extension}"/></c:if>
            </ext:forEachTab>

            initialize3dLevel(BS.viewTypeTabPane, viewTypeTabs);
            BS.viewTypeTabPane.showIn('tabsContainer3');
          })();
        </bs:trimWhitespace>
      </script>

      <div style="padding-top: 3px" id="messagesDiv">
         <bs:messages key="sourcesCleanedMessage"/>
      </div>

      <bs:buildTypePaused buildType="${buildType}" style="margin-top: 6px;"/>

      <ext:includeExtension extension="${selectedTabExtension}"/>

      <script type="text/javascript">
        BS.BuildType.updateStatus(${buildType.status.failed});
      </script>
    </bs:refreshable>

    <bs:viewTypeMenu buildType="${buildType}"/>

    <c:url value='/ajax.html' var="actionUrl"/>
    <bs:modalDialog formId="resetSources"
                    title="Choose agents"
                    action="${actionUrl}"
                    closeCommand="BS.BuildTypeResetSources.close()"
                    saveCommand="BS.BuildTypeResetSources.submitResetSources()">
      <div>Choose agents to enforce clean checkout<bs:help file="Clean+Checkout"/> for:</div>
      <bs:inplaceFilter containerId="agentId" activate="true" filterText="&lt;filter agents>" style="width: 98%;"/>
      <select name="agentId" multiple="multiple" size="10" style="width: 98%;">
        <option value="">&lt;All agents&gt;</option>
      <c:forEach items="${buildType.agentsWhereBuildConfigurationBuilt}" var="agent">
        <option class="inplaceFiltered" value="${agent.id}"><c:out value="${agent.name}"/></option>
      </c:forEach>
      </select>

      <input type="hidden" name="resetBuildConfigurationSources" value="${buildType.buildTypeId}"/>

      <div class="popupSaveButtonsBlock">
        <forms:cancel onclick="BS.BuildTypeResetSources.close()"/>
        <forms:submit label="Clean sources" id="resetSourcesSubmitButton"/>
        <forms:saving id="resetSourcesProgress"/>
      </div>
    </bs:modalDialog>

    <c:if test="${not empty buildType.project.buildTypes}">
      <bs:popupControl clazz="siblingBuildTypes" style="display: none"
                       showPopupCommand="" hidePopupCommand="" stopHidingPopupCommand=""
                       controlId="siblingBuildTypes${buildType.project.projectId}"></bs:popupControl>
    </c:if>

    <authz:authorize projectId="${projectId}" allPermissions="PAUSE_ACTIVATE_BUILD_CONFIGURATION">
      <jsp:attribute name="ifAccessGranted">
        <bs:pauseBuildTypeDialog/>
      </jsp:attribute>
    </authz:authorize>
  </jsp:attribute>
</bs:page>
