<%@ include file="/include-internal.jsp"%>
<c:set var="pageTitle" value="Server startup failed" scope="request"/>
<jsp:useBean id="startupFailure" scope="request" type="java.lang.Throwable"/>

<!DOCTYPE html>
<html>
<head>
  <title><c:if test="${not empty pageTitle}">${pageTitle} -- </c:if>TeamCity</title>
  <bs:linkCSS>
    /css/main.css
    /css/errorPage.css
  </bs:linkCSS>
  <bs:linkScript>
    /js/prototype.js
  </bs:linkScript>

  <link rel="Shortcut Icon" href="<c:url value="/favicon20.ico"/>" type="image/x-icon"/>

</head>

<body>
<div class="errorPage">

  <h1 class="errorTitle">Server startup failed</h1>

<p>
  Please examine the error below and make corrections in the server configuration.
</p>
<p>  
  If you believe this is not configuration problem, please ask for help in
   <a href="http://www.jetbrains.net/devnet/community/teamcity/teamcity">TeamCity support forum</a>.
</p>
  <% Throwable exception = startupFailure; %>
  <%@ include file="/errorDetails.jspf" %>
</div>

<iframe style="height:0;width:0;visibility:hidden" src="about:blank">
This frame should prevent back forward cache in Safari, see http://developer.apple.com/internet/safari/faq.html#anchor5
</iframe>
</body>
</html>