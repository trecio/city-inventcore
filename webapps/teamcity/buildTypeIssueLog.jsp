<%@ include file="include-internal.jsp" %>
<jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.SBuildType" scope="request"/>
<jsp:useBean id="bean" type="jetbrains.buildServer.controllers.buildType.tabs.IssueLogBean"
             scope="request"/>

<c:choose>
<c:when test="${bean.numberOfIssues > 0 || bean.hideRelated}">
<c:set var="btId" value="${buildType.buildTypeId}"/>
<c:set var="baseUrl" value="/viewType.html?buildTypeId=${btId}&tab=buildTypeIssueLog"/>
<%--@elvariable id="branchBean" type="jetbrains.buildServer.controllers.BranchBean"--%>
<c:if test="${not empty branchBean}">
  <c:set var="baseUrl" value="${baseUrl}&branch_${buildType.projectId}=${branchBean.userBranchEscaped}"/>
</c:if>
<c:url var="pagerUrlPattern" value="${baseUrl}&page=[page]"/>
<c:url value="${baseUrl}" var="url"/>

<div id="buildTypeIssueLog" class="logTable">
<form action="${url}" method="post" id="issueLogFilter" class="issueLogFilter"
      onsubmit="return BS.IssueLog.submitFilter();">

  <div class="actionBar">
    <span class="farRight">
      <bs:recordsPerPageSelect pager="${bean.pager}" onchange="BS.IssueLog.submitFilter();"/>
    </span>

    <span class="nowrap">
      <label for="from" class="firstLabel changeFilter">Show issues from: </label>
      <forms:textField id="from" className="actionInput" name="from" value="${bean.from}" defaultText="<build #>"/>
    </span>

    <span class="nowrap">
      <label for="to">to: </label>
      <forms:textField id="to" className="actionInput" name="to" value="${bean.to}" defaultText="<build #>"/>
    </span>

    <input class="btn btn_mini" type="submit" value="Filter"/>
    <forms:saving className="progressRingInline"/>
  </div>

  <bs:refreshable containerId="issueLogTable" pageUrl="${url}">
    <p class="resultsTitle">
      <c:url var="permalink" value='${baseUrl}'/>
      <c:set var="permalink" value='${permalink}&from=${util:urlEscape(bean.from)}&to=${util:urlEscape(bean.to)}'/>
      <c:set var="permalink" value='${permalink}&showBuilds=${bean.showBuilds}'/>
      <c:set var="permalink" value='${permalink}&hideRelated=${bean.hideRelated}'/>
      <span class="changeLogPermalink"><a href="${permalink}">Permalink</a></span>
      <span class="changeLogToggle">
        <forms:checkbox id="showBuilds" name="showBuilds" checked="${bean.showBuilds}"
                        onclick="BS.IssueLog.submitFilter();"/>
        <label for="showBuilds">Show builds</label>
      </span>
      <span class="changeLogToggle">
        <forms:checkbox id="hideRelated" name="hideRelated" checked="${bean.hideRelated}"
                        onclick="BS.IssueLog.submitFilter();"/>
        <label for="hideRelated">Show only resolved issues</label>
      </span>
    </p>

    <c:set var="visibleRows" value="${bean.visibleRows}"/>

    <c:if test="${bean.pager.totalRecords > 0}">
      <c:set var="pagerDesc">
        ${bean.numberOfIssues}${bean.pager.lastPage ? '' : '+'} issue<bs:s val="${bean.numberOfIssues}"/>
      </c:set>

      <bs:pager place="title" urlPattern="${pagerUrlPattern}" pager="${bean.pager}" pagerDesc="${pagerDesc}"/>
    </c:if>

    <c:if test="${bean.fetchingIssues}">
      <div class="attentionComment">
        Some of the issues are being retrieved from the issue-tracker system.
        Refresh the page to see the updates.
      </div>
    </c:if>
    <c:if test="${fn:length(visibleRows) > 0}">
      <table class="issueLogTable separatedWithLine overviewTypeTable">
        <c:forEach items="${visibleRows}" var="row" varStatus="pos">
          <c:choose>
            <c:when test="${row.build != null}">
              <tr>
                <td colspan="4">
                  <div class="captionRow">
                    <bs:changeLogBuildRow build="${row.build}"
                                          showBuildTypeInBuilds="${not empty showBuildTypeInBuilds}"
                                          showBranch="${bean.showBranch}"/>
                  </div>
                </td>
              </tr>
            </c:when>
            <c:otherwise>
              <c:set var="issue" value="${row.issue}"/>
              <c:set var="change" value="${issue.relatedModification}"/>
              <tr>
                <td class="id"><a href="${issue.url}">${issue.id}</a></td>
                <td class="summary">
                  <c:set var="fetchStatus" value="${issue.fetchStatus}"/>
                  <c:choose>
                    <c:when test="${fetchStatus.fetched}">
                      <c:out value="${issue.summary}"/>
                    </c:when>
                    <c:when test="${fetchStatus.failedToFetch}">
                      <span class="err"><c:out value="${issue.fetchError}"/></span>
                    </c:when>
                    <c:otherwise>
                      <span class="retrievingNote">Retrieving data...</span>
                    </c:otherwise>
                  </c:choose>
                </td>
                <td class="state"><c:out value="${issue.state}"/></td>
                <c:choose>
                  <c:when test="${change != null}">
                    <td class="changedFiles">
                      <c:set var="build" value="${change.firstBuilds[buildType]}"/>
                      <c:choose>
                        <c:when test="${not empty build}">
                          <bs:changedFilesLink modification="${change}" build="${build}"><bs:changeCommitters modification="${change}"/></bs:changedFilesLink>
                        </c:when>
                        <c:otherwise>
                          <bs:changedFilesLink modification="${change}" buildType="${buildType}"><bs:changeCommitters modification="${change}"/></bs:changedFilesLink>
                        </c:otherwise>
                      </c:choose>
                    </td>
                  </c:when>
                  <c:otherwise>
                    <td>
                      <c:set value="${row.relatedBuild}" var="build"/>
                      <bs:buildLink buildTypeId="${build.buildTypeId}" buildId="${build.buildId}">
                        Build comment
                      </bs:buildLink>
                    </td>
                  </c:otherwise>
                </c:choose>
              </tr>
            </c:otherwise>
          </c:choose>
        </c:forEach>
        </table>

      <c:if test="${bean.pager.totalRecords > 0}">
        <bs:pager place="bottom" urlPattern="${pagerUrlPattern}" pager="${bean.pager}"/>
      </c:if>
    </c:if>
  </bs:refreshable>
</form>
</div>
</c:when>
<c:otherwise>
<p class="resultsTitle">
  No issues are found related to the build configuration changes.<bs:help file="Integrating+TeamCity+with+Issue+Tracker"/>
  <authz:authorize allPermissions="CHANGE_SERVER_SETTINGS">
    <c:url var="url" value="/admin/admin.html?item=issueTracker"/>
    Configure <a href="${url}" title="Click to configure issue tracker settings">issue tracker settings</a>.
  </authz:authorize>
</p>
</c:otherwise>
</c:choose>
<script type="text/javascript">
  BS.Branch.baseUrl = "${url}";
</script>
