<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="showPopupCommand" required="true" %><%@
    attribute name="hidePopupCommand" required="true" %><%@
    attribute name="stopHidingPopupCommand" required="true" %><%@
    attribute name="controlId" required="true" %><%@
    attribute name="style" required="false" %><%@
    attribute name="clazz" required="false" %><%@
    attribute name="onGray" type="java.lang.Boolean" required="false" %><%@
    attribute name="imageSrc" required="false"
%><c:if test="${empty imageSrc}"><c:set var="imageSrc" value="/img/${onGray ? 'popupOnGray.gif' : 'popUpControl.gif'}"/></c:if
><span class="pc ${clazz}" <c:if test="${not empty style}">style="${style}"</c:if> onmouseover="${stopHidingPopupCommand}"><jsp:doBody
/><img id="${controlId}" src="<c:url value='${imageSrc}'/>" onmouseover="${showPopupCommand};_pc_over(this,<c:out value="\"${hidePopupCommand}\""/>)" onclick="_tc_es(event)" class="toggle"/></span>
