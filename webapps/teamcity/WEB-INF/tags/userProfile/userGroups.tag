<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz"%>
<%@attribute name="user" type="jetbrains.buildServer.users.SUser" required="true" %>

<c:set var="groups" value="${user.userGroups}"/>
<c:set var="groupsNum" value="${fn:length(groups)}"/>
<p class="note">
  <strong>${user.username}</strong>
  <c:choose>
    <c:when test="${groupsNum == 0}">is not assigned to any group.</c:when>
    <c:when test="${groupsNum == 1}">is assigned to <strong>1</strong> group.</c:when>
    <c:when test="${groupsNum > 1}">is assigned to <strong>${groupsNum}</strong> groups.</c:when>
</c:choose>
</p>

<div class="userGroupsContainer">
  <bs:messages key="userAssigned"/>

  <c:if test="${groupsNum > 0}">
  <table class="settings userGroupsTable">
    <tr>
      <th>Group name</th>
      <th>Description</th>
    </tr>
  <c:forEach items="${groups}" var="group">
    <tr>
      <td>
        <bs:editGroupLink group="${group}"/>
      </td>
      <td><c:out value="${group.description}"/></td>
    </tr>
  </c:forEach>
  </table>
  </c:if>

  <c:if test="${adminEditUserForm.canAddToAnyGroup}">
  <p>
    <forms:addButton onclick="BS.AttachToGroupsDialog.showAttachUserDialog(${user.id}); return false">Add/remove from groups</forms:addButton>
  </p>
  </c:if>
</div>

<jsp:include page="/admin/attachToGroups.html"/>
