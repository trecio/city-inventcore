<%@ tag import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="ext" tagdir="/WEB-INF/tags/ext" %>
<%@ taglib prefix="userProfile" tagdir="/WEB-INF/tags/userProfile" %>
<%@ attribute name="profileForm" rtexprvalue="true" type="jetbrains.buildServer.controllers.profile.ProfileForm" required="true" %>
<%@ attribute name="adminMode" rtexprvalue="true" type="java.lang.Boolean" %>

<a name="notifications"></a>
<l:settingsBlock title="Watched Builds and Notifications">

  <c:forEach items="${profileForm.notificatorPluginList}" var="pluginSection">
    <div class="notifierSection">
      <c:if test="${not empty profileForm.editee}">
        <c:set var="cameFromTitle" value="${adminMode ? profileForm.editee.descriptiveName : title}"/>
        <userProfile:editNotifierSettingsLink notifier="${pluginSection.pluginName}" user="${profileForm.editee}" adminMode="${adminMode}" cameFromUrl="${pageUrl}" cameFromTitle="${cameFromTitle}">Edit</userProfile:editNotifierSettingsLink>
      </c:if>
      <h3 class="title_underlined"><c:out value="${pluginSection.displayName}"/></h3>
      <c:if test="${not adminMode}">
        <ext:includeExtension extension="${profileForm.notifierExtensions[pluginSection.pluginName]}"/>
      </c:if>
    <c:forEach items="${pluginSection.propertyList}" var="property">
      <c:set var="propertyField" value="notificatorPlugins[${pluginSection.pluginName}].properties[${property.propertyName}].value"/>
      <p>
        <label class="tableLabel" for="${propertyField}"><c:out value="${property.displayName}"/>:</label>
        <input class="textfield" type="text" id="${propertyField}" name="${propertyField}" size="30"
               maxlength="256" value="<c:out value="${property.value}"/>"/>
      </p>
    </c:forEach>
      <div class="rulesDescription <c:if test="${not pluginSection.rulesConfigured}">noRules</c:if>">
        <c:out value="${pluginSection.rulesDescription}"/>
      </div>
    </div>
  </c:forEach>

</l:settingsBlock>
