<%@ tag import="jetbrains.buildServer.serverSide.crypt.RSACipher"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ attribute name="profileForm" rtexprvalue="true" type="jetbrains.buildServer.controllers.profile.ProfileForm" required="true" %>

<script type="text/javascript">
  $j(document).ready(function() {
    var form = document.forms[0];
    if ($('username1')) {
      $('username1').focus();
    }
    else {
      if (form.name && form.name.focus) {
        form.name.focus();
      }
    }
  });

  var makeEditable = function() {
    var input = document.getElementById('usernameInput');
    var link = document.getElementById('editUsernameLink');
    if (input) {
      input.innerHTML = '<input class="textfield" id="username1" type="text" name="username1" size="30" maxlength="50"' +
                       ' value="<c:out value="${profileForm.username1}"/>"/>' +
                       '<span class="error" id="errorUsername1" style="margin-left: 10.5em;"></span>';
    }
    if (link) {
      link.innerHTML = '';
    }
  }
</script>

<l:settingsBlock title="General">

  <p>
    <c:if test="${profileForm.showEditUsernameLink}">
      <span id="editUsernameLink" style="float: right;">
        <a href="#" onclick="makeEditable(); return false">Edit</a>
        <bs:help file="Managing+Users+and+User+Groups" anchor="Editinguseraccount"/>
      </span>
    </c:if>
    <label class="tableLabel" for="username1">Username:<c:if test="${profileForm.canChangeUsername}"> <l:star/></c:if></label>
    <c:if test="${profileForm.canChangeUsername}">
      <input class="textfield" id="username1" type="text" name="username1" size="30" maxlength="50"
             value="<c:out value="${profileForm.username1}"/>"/>
      <span class="error" id="errorUsername1" style="margin-left: 10.5em;"></span>
    </c:if>
    <c:if test="${!profileForm.canChangeUsername}">
      <span id="usernameInput"><span style="margin-left:0.5em;"><c:out value="${profileForm.username1}"/></span></span>
    </c:if>
  </p>

  <p>
    <label class="tableLabel" for="name">Name:</label>
    <input class="textfield" id="name" type="text" name="name" size="30" maxlength="128" value="<c:out value="${profileForm.name}"/>"/>
  </p>

  <p>
    <label class="tableLabel" for="email">Email address:</label>
    <input class="textfield" id="email" type="text" name="email" size="30" maxlength="128" value="<c:out value="${profileForm.email}"/>"/>
  </p>

  <c:if test="${profileForm.canChangePassword}">
  <p>
    <label class="tableLabel" for="password1">Password: <l:star/></label>
    <input class="textfield" id="password1" type="password" name="password1" size="30" maxlength="80"/>
    <span class="error" id="errorPassword1" style="margin-left: 10.5em;"></span>
  </p>

  <p>
    <label class="tableLabel" for="retypedPassword">Confirm password:&nbsp;<l:star/></label>
    <input class="textfield" id="retypedPassword" type="password" name="retypedPassword" size="30" maxlength="80"/>
    <input type="hidden" id="publicKey" name="publicKey" value="<c:out value='<%=RSACipher.getHexEncodedPublicKey()%>'/>"/>
  </p>
  </c:if>

</l:settingsBlock>
