<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="ext" tagdir="/WEB-INF/tags/ext" %><%@
    attribute name="build" required="true" type="jetbrains.buildServer.serverSide.SBuild" %><%@
    attribute name="showBuildTypeInBuilds" required="true" type="java.lang.Boolean" %><%@
    attribute name="showBranch" required="true" type="java.lang.Boolean"

%><table class="changeLogBuildRow">
  <tr>
    <c:if test="${showBuildTypeInBuilds}">
      <td class="bt"><bs:buildTypeLinkFull buildType="${build.buildType}" popupMode="true" dontShowProjectName="true"/></td>
    </c:if>
    <bs:buildRow build="${build}"
                 showBuildNumber="true"
                 showBranchName="${showBranch}"
                 showStatus="true"
                 outOfChangesSequence="${build.outOfChangesSequence}"/>
    <td class="startDate">
      <bs:date value="${build.startDate}"/>
    </td>
  </tr>
</table>