<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ attribute name="issues" required="true" type="java.util.Collection"%>
<%@ attribute name="build" required="true" type="jetbrains.buildServer.serverSide.SBuild"%>

<table class="dark issues borderBottom" cellspacing="0" id="buildIssuesTable">
  <thead>
    <tr>
      <th style="width: 10%;">Status</th>
      <th style="width: 10%;">ID</th>
      <th style="width: 45%;">Summary</th>
      <th style="width: 35%;">Description</th>
    </tr>
  </thead>
  <tbody>
    <c:forEach var="issue" items="${issues}">
      <bs:trimWhitespace>
        <%--@elvariable id="issue" type="jetbrains.buildServer.issueTracker.IssueEx"--%>
        <tr <c:if test="${issue.fixedByRelatedModification}">class="fixed"</c:if>>
          <td class="status">
            <span <c:if test="${issue.fixedByRelatedModification}">title="The issue is resolved in this build"</c:if>>${issue.state}</span>
          </td>
          <td><a href="${issue.url}" title="Open in ${issue.provider.name}">${issue.id}</a></td>
          <td>
            <c:set var="fetchStatus" value="${issue.fetchStatus}"/>
            <c:choose>
              <c:when test="${fetchStatus.failedToFetch}">
                <span class="err"><c:out value="${issue.fetchError}"/></span>
              </c:when>
              <c:otherwise>
                <c:out value="${issue.summary}"/>
              </c:otherwise>
            </c:choose>
          </td>
          <td>
            <c:choose>
              <c:when test="${not empty issue.relatedModification}">
                <c:set var="change" value="${issue.relatedModification}"/>
                Related change by
                <bs:popupControl
                    showPopupCommand="BS.FilesPopup.showPopup(event, {parameters: 'modId=${change.id}&personal=${change.personal}'});"
                    hidePopupCommand="BS.FilesPopup.hidePopup();"
                    stopHidingPopupCommand="BS.FilesPopup.stopHidingPopup();"
                    controlId="files:${change.id}">
                  <bs:modificationLink modification="${change}"><bs:changeCommitters modification="${change}"/></bs:modificationLink>
                </bs:popupControl>
              </c:when>
              <c:otherwise>
                <bs:buildLink buildTypeId="${build.buildTypeId}" buildId="${build.buildId}">
                  Build comment
                </bs:buildLink>
                by ${build.buildComment.user.name}
              </c:otherwise>
            </c:choose>
          </td>
        </tr>
      </bs:trimWhitespace>
    </c:forEach>
  </tbody>
</table>
