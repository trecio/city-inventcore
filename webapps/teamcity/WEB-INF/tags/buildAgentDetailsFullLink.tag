<%@ tag import="jetbrains.buildServer.web.util.WebUtil"%><%@
  taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
  taglib prefix="bs" tagdir="/WEB-INF/tags"%><%@
  attribute name="build" required="true" type="jetbrains.buildServer.serverSide.SBuild"%><%@
  attribute name="useDisplayName" required="false"%><%@
  attribute name="doNotShowOutdated" required="false" type="java.lang.Boolean"%><%@
  attribute name="doNotShowOSIcon" required="false" type="java.lang.Boolean"%><%@
  attribute name="doNotShowPoolInfo" required="false" type="java.lang.Boolean"%><%@
  attribute name="showRunningStatus" required="false" type="java.lang.Boolean"%><%@
  attribute name="doNotShowUnavailableStatus" required="false" type="java.lang.Boolean"
  %><c:set var="agent" value="${build.agent}"/><c:set var="agentType" value="${agent.agentType}"/><bs:agentDetailsFullLink agent="${agent}"
                            agentType="${agentType}"
                            useDisplayName="${useDisplayName}"
                            doNotShowOutdated="${doNotShowOutdated}"
                            doNotShowOSIcon="${doNotShowOSIcon}"
                            doNotShowPoolInfo="${doNotShowPoolInfo}"
                            showRunningStatus="${showRunningStatus}"
                            doNotShowUnavailableStatus="${doNotShowUnavailableStatus}"
  />