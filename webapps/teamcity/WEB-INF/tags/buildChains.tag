<%@ tag import="jetbrains.buildServer.controllers.graph.DependencyGraphsBean" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="et" tagdir="/WEB-INF/tags/eventTracker" %>
<%@ taglib prefix="up" tagdir="/WEB-INF/tags/userProfile" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@attribute name="dependencyGraphsBean" type="jetbrains.buildServer.controllers.graph.DependencyGraphsBean" required="true" %>
<%@attribute name="showFilter" type="java.lang.Boolean" required="false" %>
<jsp:useBean id="pageUrl" type="java.lang.String" scope="request"/>
<c:set var="graphsBean" value="${dependencyGraphsBean}"/>
<c:set var="numGraphs" value="${graphsBean.numberOfChains}"/>
<c:choose>
  <c:when test="${fn:contains(pageUrl, 'currentPage')}"><c:set var="pagerUrlPattern" value='<%=pageUrl.replaceAll("currentPage=[0-9]+", "currentPage=[page]")%>'/></c:when>
  <c:otherwise><c:set var="pagerUrlPattern" value="${pageUrl}&currentPage=[page]"/></c:otherwise>
</c:choose>

<bs:linkScript>
  /js/bs/blocksWithHeader.js
</bs:linkScript>

<bs:linkCSS dynamic="${true}">
  /css/buildChains.css
</bs:linkCSS>

<c:if test="${showFilter != false}">
<script type="text/javascript">
  BS.BuildChainsFilter = OO.extend(BS.AbstractWebForm, {
    formElement: function() {
      return $('buildChainsFilter');
    },

    savingIndicator: function() {
      return $('buildChainsFilterProgress');
    },

    submitFilter : function() {
      var that = this;
      this.setSaving(true);
      this.disable();
      BS.FormSaver.save(this, this.formElement().action, OO.extend(BS.SimpleListener, {
        onCompleteSave: function() {
          $('buildChains').refresh(that.savingIndicator(), null, function() {
            that.enable();
            that.setSaving(false);
          });
        }
      }));

      return false;
    }
  });
</script>
<div>
  <form action="<c:url value='/buildChainsFilter.html'/>" id="buildChainsFilter" method="post" onsubmit="return false;">
    <div class="actionBar">
      <span class="farRight">
        <bs:recordsPerPageSelect pager="${dependencyGraphsBean.pager}" onchange="return BS.BuildChainsFilter.submitFilter();"/>
      </span>

      <span class="nowrap">
        <label class="firstLabel" for="selectedTopBuildTypeId">Build chains:</label>
        <forms:select name="selectedTopBuildTypeId" onchange="BS.BuildChainsFilter.submitFilter();" enableFilter="true">
          <forms:option value="">&lt;All build chains&gt;</forms:option>
          <c:forEach items="${dependencyGraphsBean.availableTopBuildTypes}" var="bt">
            <forms:option value="${bt.buildTypeId}" selected="${dependencyGraphsBean.selectedTopBuildTypeId == bt.buildTypeId}"><c:out value="${bt.fullName}"/></forms:option>
          </c:forEach>
        </forms:select>
      </span>

      <forms:saving id="buildChainsFilterProgress" className="progressRingInline"/>

      <input type="hidden" name="graphsBeanId" value="${dependencyGraphsBean.id}"/>
    </div>
  </form>
</div>
</c:if>

<bs:refreshable containerId="buildChains" pageUrl="${pageUrl}">

<c:if test="${numGraphs > 0}">
  <c:set var="pagerDesc">${numGraphs} build chain<bs:s val="${numGraphs}"/><bs:help file="Build+Chain"/></c:set>
  <bs:pager place="title" urlPattern="${pagerUrlPattern}" pager="${graphsBean.pager}" pagerDesc="${pagerDesc}"/>

  <p>
    <bs:collapseExpand collapseAction="BS.CollapsableBlocks.collapseAll(true, 'chains'); return false" expandAction="BS.CollapsableBlocks.expandAll(true, 'chains'); return false"/>
  </p>
</c:if>

<c:forEach var="chainGraph" items="${graphsBean.currentPage}" varStatus="pos">
  <c:set var="buildType" value="${chainGraph.buildChain.topBuildType}"/>
  <c:set var="topPromotion" value="${chainGraph.buildChain.topBuildPromotion}"/>
  <c:set var="build" value="${not empty topPromotion ? topPromotion.associatedBuild : null}"/>
  <c:set var="queuedBuild" value="${not empty topPromotion ? topPromotion.queuedBuild : null}"/>
  <c:set var="lastStarted" value="${chainGraph.buildChain.lastStarted}"/>
  <c:set var="blockId" value="block_${chainGraph.buildChain.id}"/>
  <c:set var="graphId" value="graph_${chainGraph.buildChain.id}"/>
  <c:set var="chainInfo"><c:choose
    ><c:when test="${not empty queuedBuild}"><bs:queuedBuildIcon queuedBuild="${queuedBuild}"/> #??? waiting in the queue</c:when
    ><c:when test="${empty queuedBuild and empty build}"><em>not triggered</em></c:when></c:choose></c:set>
  <c:set var="title"><table class="buildChainHeader">
      <tr>
        <td class="buildChainInfo">
          <bs:buildTypeLinkFull buildType="${buildType}"/>
        </td>
        <td class="buildChainStatus">${chainInfo}&nbsp;</td>
        <td>
          <c:if test="${not empty lastStarted}">
          <table cellspacing="0" cellpadding="0" width="100%">
            <td style="width:20em;">
              Last build: <bs:buildTypeLinkFull buildType="${lastStarted.buildType}"/>
            </td>
            <bs:buildRow build="${lastStarted}" showBranchName="true" showBuildNumber="true" showStatus="true" showStartDate="true"/>
          </table>
          </c:if>
        </td>
      </tr>
    </table></c:set>
  <div class="buildChainBlock">
    <bs:_collapsibleBlock title="${title}" id="${blockId}" tag="div" collapsedByDefault="true" headerClass="no_title">
      <bs:buildChain buildChainGraph="${chainGraph}" graphId="${graphId}"/>
    </bs:_collapsibleBlock>
    <script type="text/javascript">
      (function() {
        var block = $('${blockId}')._block;
        BS.CollapsableBlocks.registerBlock(block, "chains");

        var func = block.onShowBlock;
        block.onShowBlock = function(contentEl, id) {
          func(contentEl, id);
          $('${graphId}').graphObject.drawWithSelected();
        };

        if (block.isExpanded(block.getId())) {
          $('${graphId}').graphObject.drawWithSelected();
        }
      })();
    </script>
  </div>
</c:forEach>

<bs:pager place="bottom" urlPattern="${pagerUrlPattern}" pager="${graphsBean.pager}"/>

</bs:refreshable>

<c:forEach items="${graphsBean.buildTypesToWatch}" var="bt">
  <et:subscribeOnBuildTypeEvents buildTypeId="${bt.buildTypeId}">
    <jsp:attribute name="eventNames">
      BUILD_STARTED
      BUILD_CHANGES_LOADED
      BUILD_FINISHED
      BUILD_INTERRUPTED
      BUILD_TYPE_ADDED_TO_QUEUE
    </jsp:attribute>
    <jsp:attribute name="eventHandler">
      BS.reload(false, function() {
        $('buildChains').refresh();
      });
    </jsp:attribute>
  </et:subscribeOnBuildTypeEvents>
</c:forEach>

