<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %><%@
    taglib prefix="resp" tagdir="/WEB-INF/tags/responsible" %><%@
    taglib prefix="responsible" uri="/WEB-INF/functions/resp" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@
    attribute name="responsibility" type="jetbrains.buildServer.responsibility.ResponsibilityEntry" required="true" %><%@
    attribute name="test" type="jetbrains.buildServer.serverSide.STest" required="false" %><%@
    attribute name="buildTypeRef" type="jetbrains.buildServer.serverSide.SBuildType" required="false" %><%@
    attribute name="noActions" type="java.lang.Boolean" required="false"

%><c:set var="state" value="${responsibility.state}"
/><div class="name-value"><table>
  <c:if test="${state.active}">
    <tr>
      <th>Investigator:</th>
      <td><c:out value="${responsibility.responsibleUser.descriptiveName}"/></td>
    </tr>
  </c:if>
  <c:if test="${state.fixed}">
    <tr>
      <th>Fixed by:</th>
      <td><c:out value="${responsibility.responsibleUser.descriptiveName}"/></td>
    </tr>
  </c:if>
  <c:if test="${(state.active or state.fixed) and
                responsibility.reporterUser != null and
                responsibility.responsibleUser.id != responsibility.reporterUser.id}">
    <tr>
      <th>${state.active ? 'Assigned' : 'Done'} by:</th>
      <td><c:out value="${responsibility.reporterUser.descriptiveName}"/></td>
    </tr>
  </c:if>
  <tr>
    <th>Since:</th>
    <td><bs:date value="${responsibility.timestamp}"/></td>
  </tr>
  <c:if test="${state.active}">
    <tr>
      <th>Resolve:</th>
      <td>${responsibility.removeMethod.whenFixed ? 'automatically when successful' : 'manually'}</td>
    </tr>
  </c:if>
  <c:if test="${not empty responsibility.comment}">
    <tr>
      <th>Comment:</th>
      <td class="resp-comment"><bs:out value="${responsibility.comment}"/></td>
    </tr>
  </c:if>
</table></div>
<c:if test="${not empty test and not noActions}"
  ><authz:authorize projectId="${test.projectId}" anyPermission="ASSIGN_INVESTIGATION,MANAGE_BUILD_PROBLEMS"
    ><jsp:attribute name="ifAccessGranted"
      ><div class="actions"><tt:testInvestigationLinks test="${test}" buildId="" withFix="${state.active}"/></div>
    </jsp:attribute
    ><jsp:attribute name="ifAccessDenied"
      ><div class="actions">No permissions to modify investigation</div>
    </jsp:attribute
  ></authz:authorize
></c:if><resp:btResponsibilityActions buildTypeRef="${buildTypeRef}" noActions="${noActions}" responsibility="${responsibility}"/>