<%@ taglib prefix="util" uri="/WEB-INF/functions/util" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="testRun" type="jetbrains.buildServer.serverSide.TestRunEx" required="true" %><%@
    attribute name="btScope" type="jetbrains.buildServer.serverSide.SBuildType" required="false" %><%@
    attribute name="ignoreMuteScope" type="java.lang.Boolean" required="false" %><%@
    attribute name="showMuteFromTestRun" type="java.lang.Boolean" required="false"

%><c:set var="test" value="${testRun.test}"
/><c:set var="currentMuteInfo" value="${test.currentMuteInfo}"
/><c:set var="showCurrentMuteInfo" value="${util:shouldShowCurrentMuteInfo(currentMuteInfo, ignoreMuteScope, btScope)}"
/><c:set var="showBuildMuteInfo" value="${showMuteFromTestRun and testRun.muted}"

/><c:if test="${showCurrentMuteInfo or showBuildMuteInfo}"
    ><c:url value="/img/muted.gif" var="url"
    /><c:set var="tooltip"><bs:muteInfoTooltip test="${test}"
                                               testRun="${testRun}"
                                               noCurrentMuteInfo="${not showCurrentMuteInfo}"
                                               noTestRunMuteInfo="${not showBuildMuteInfo}"/></c:set
    ><img src="${url}" class="mute-icon" <bs:tooltipAttrs text="${tooltip}" className="name-value-popup"/> alt=""/>
</c:if>