<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
    taglib prefix="agent" tagdir="/WEB-INF/tags/agent"%><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms"%><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags"%><%@
    attribute name="data" required="true" type="jetbrains.buildServer.controllers.agent.CompatibleConfigurationsBean"%><%@
    attribute name="selectedConfigurationsPolicy" required="true" type="java.lang.Boolean"%><%@
    attribute name="cssClass" required="true" type="java.lang.String"%><%@
    attribute name="active" required="true" type="java.lang.Boolean"

%><c:set var="nameSuffix" value=""/>
<c:if test="${not active}">
  <c:set var="nameSuffix" value="Inactive"/>
</c:if>
<table class="agentsCompatibilityTable">
  <tr>
    <th class="compatible ${cssClass}">
      <c:if test="${selectedConfigurationsPolicy}">
        <span class="toggleAll">
          <c:if test="${data.numCompatible > 0}">
            <forms:checkbox name="toggleAllCompatible${nameSuffix}"
                            onmouseover="BS.Tooltip.showMessage(this, {shift: {x: 10, y: 20}, delay: 600}, 'Click to select / unselect all compatible configurations')"
                            onmouseout="BS.Tooltip.hidePopup()"
                            onclick="BS.AgentRunPolicy.toggle(this.checked, $('runConfigurationForm'), 'canRunCompatible${nameSuffix}')"/>
          </c:if>
          <c:if test="${data.numCompatible == 0}">&nbsp;</c:if>
        </span>
      </c:if>
      <img src="<c:url value='/img/compatible.gif'/>" alt="List of compatible configurations"/>
      ${selectedConfigurationsPolicy ? 'Assigned compatible' : 'Compatible'} configurations (${data.numTotalCompatible})
    </th>
    <th class="empty">&nbsp;</th>
    <th class="incompatible ${cssClass}">
      <c:if test="${selectedConfigurationsPolicy}">
        <span class="toggleAll">
          <c:if test="${data.numIncompatible > 0}">
            <forms:checkbox name="toggleAllIncompatible${nameSuffix}"
                            onmouseover="BS.Tooltip.showMessage(this, {shift: {x: 10, y: 20}, delay: 600}, 'Click to select / unselect all incompatible configurations')"
                            onmouseout="BS.Tooltip.hidePopup()"
                            onclick="BS.AgentRunPolicy.toggle(this.checked, $('runConfigurationForm'), 'canRunIncompatible${nameSuffix}')"/>
          </c:if>
          <c:if test="${data.numIncompatible == 0}">&nbsp;</c:if>
        </span>
      </c:if>
      <img src="<c:url value='/img/noncompatible.gif'/>" alt="List of incompatible configurations"/>
      ${selectedConfigurationsPolicy ? 'Assigned incompatible' : 'Incompatible'} configurations (${data.numTotalIncompatible})
    </th>
  </tr>
  <tr>
    <td class="compatible ${cssClass}" id="compatible_td">
      <c:forEach items="${data.compatibleByProject}" var="entry">
        <agent:projectCompatibleEntries project="${entry.key}"
                                        compatibilities="${entry.value}"
                                        compatible="true"/>
      </c:forEach>
      <c:if test="${data.numTotalCompatible == 0}">
        <div class="nothing">None</div>
      </c:if>
      <c:if test="${data.numInaccessibleCompatible > 0}">
        <div class="attentionComment">
          There <bs:are_is val="${data.numInaccessibleCompatible}"/> ${data.numInaccessibleCompatible}
          compatible build configuration<bs:s val="${data.numInaccessibleCompatible}"/> you do not have access to.
        </div>
      </c:if>
    </td>
    <td class="empty">&nbsp;</td>
    <td class="incompatible ${cssClass}" id="incompatible_td">
      <c:forEach items="${data.incompatibleByProject}" var="entry">
        <agent:projectCompatibleEntries project="${entry.key}"
                                        compatibilities="${entry.value}"
                                        compatible="false"/>
      </c:forEach>
      <c:if test="${data.numTotalIncompatible == 0}">
        <div class="nothing">None</div>
      </c:if>
      <c:if test="${data.numInaccessibleIncompatible > 0}">
        <div class="attentionComment">
          There <bs:are_is val="${data.numInaccessibleIncompatible}"/> ${data.numInaccessibleIncompatible}
          incompatible build configuration<bs:s val="${data.numInaccessibleIncompatible}"/> you do not have access to.
        </div>
      </c:if>
    </td>
  </tr>
</table>
