<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="build" fragment="false" required="true" type="jetbrains.buildServer.serverSide.SBuild" %><%@
    attribute name="highlightIfCommitter" type="java.lang.Boolean" required="false"
%><c:set var="buildPromotion" value="${build.buildPromotion}"
/><c:set var="attrs" value="id='changes:${buildPromotion.id}'"
/><bs:changesPopup buildPromotion="${buildPromotion}"
                   highlightIfCommitter="${highlightIfCommitter}"
    ><bs:_viewLog build="${build}" title="" attrs="${attrs}" tab="buildChangesDiv"><jsp:doBody/></bs:_viewLog
></bs:changesPopup>