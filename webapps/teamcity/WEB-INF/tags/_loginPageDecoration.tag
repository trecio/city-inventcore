<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="title" required="true" type="java.lang.String" %>
<div id="${id}" class="initialPage">
  <span class="logo"><img src="<c:url value='/img/logoLogin.gif'/>" alt="TeamCity" width="61" height="102"/></span>
  <div id="pageContent">
    <h1 id="header">${title}</h1>
    <bs:version/>

    <jsp:doBody/>
  </div>
</div>
