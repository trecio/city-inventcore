<%@ tag import="jetbrains.buildServer.UserChangeInfo" %>
<%@ tag import="jetbrains.buildServer.util.StringUtil" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ attribute name="project" rtexprvalue="true" type="jetbrains.buildServer.serverSide.SProject" required="true"
  %><%@ attribute name="style" required="false"
  %><%@ attribute name="target" required="false"
  %><c:set var="text"><jsp:doBody/></c:set><a class="buildTypeName" style="${style}"
     href="${serverPath}<bs:projectUrl projectId="${project.projectId}"/>"
     <c:if test="${not empty target}">target="${target}"</c:if>
     title="Click to open &quot;${project.name}&quot; project home page"
     ><c:choose><c:when test="${not empty text}">${text}</c:when><c:otherwise><c:out value="${project.name}"/></c:otherwise></c:choose></a>