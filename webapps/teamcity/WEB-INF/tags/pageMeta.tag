<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@attribute name="title" required="true" %>
<link rel="Shortcut Icon" href="<c:url value="/favicon20.ico"/>" type="image/x-icon"/>
<%-- iOS - don't parse numbers as phone numbers --%>
<meta name="format-detection" content="telephone=no"/>
<%-- Google Chrome info and icons --%>
<meta name="application-name" content="TeamCity (<c:out value="${title}"/>)"/>
<meta name="description" content="Powerful Continuous Integration and Build Server"/>
<link rel="icon" href="<c:url value="/img/icons/TeamCity512.png"/>" sizes="512x512"/>
<link rel="icon" href="<c:url value="/img/icons/TeamCity128.png"/>" sizes="128x128"/>
<link rel="icon" href="<c:url value="/img/icons/TeamCity48.png"/>" sizes="48x48"/>
<link rel="icon" href="<c:url value="/img/icons/TeamCity32.png"/>" sizes="32x32"/>
<%-- FireFox uses the last icon as favicon, so the last one should be 16x16 to prevent scaling --%>
<link rel="icon" href="<c:url value='/img/icons/TeamCity16.png'/>" sizes="16x16"/>
