<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>

<%@ attribute name="buildProblems" type="java.util.List" required="true" %>

<bs:messages key="buildProblemRemoved"/>

<c:if test="${not empty buildProblems}">

  <c:set var="sectionIcon" value="../muted.gif"/>

  <c:forEach var="buildProblem" items="${buildProblems}">
    <c:if test="${not buildProblem.mutedInBuild}">
      <c:set var="sectionIcon" value="error.gif"/>
    </c:if>
  </c:forEach>

  <c:set var="sectionTitle"><bs:icon icon="${sectionIcon}"/> Build problems</c:set>

  <bs:_collapsibleBlock title="${sectionTitle}" id="buildProblems" collapsedByDefault="true">
    <table class="buildProblemsTable">
      <c:forEach var="buildProblem" items="${buildProblems}">
        <bs:changeRequest key="buildProblem" value="${buildProblem}">
          <jsp:include page="/problems/buildProblem.html"/>
        </bs:changeRequest>
      </c:forEach>
    </table>
  </bs:_collapsibleBlock>
</c:if>