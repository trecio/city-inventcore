<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz"%>
<%@attribute name="queuedBuild" type="jetbrains.buildServer.serverSide.SQueuedBuild" required="false"%>

<c:set var="buildEstimates" value="${queuedBuild.buildEstimates}"/>
<c:if test="${not empty buildEstimates}">
  <%--@elvariable id="buildAgent" type="jetbrains.buildServer.serverSide.SBuildAgent"--%>
  <c:set var="buildAgent" value="${buildEstimates.agent}"/>

  <c:set var="timeInterval" value="${buildEstimates.timeInterval}"/>

  <c:set var="secondsToStart" value="${not empty timeInterval ? timeInterval.startPoint.relativeSeconds : 'null'}"/>
  <c:set var="isNeverStarts" value="${(not empty timeInterval) and (timeInterval.startPoint.relativeSeconds > 10000 * 3600)}"/>

  BS.QueueEstimates._estimates['${queuedBuild.itemId}'] = {
    secondsToStart: ${isNeverStarts ? "'never'": secondsToStart},

    secondsToFinish: ${not empty timeInterval.endPoint ? timeInterval.endPoint.relativeSeconds : 'null'},

    isDelayed: ${buildEstimates.delayed},

    getDurationAsString: function() {
      <c:choose>
        <c:when test="${(not empty timeInterval) and (not empty timeInterval.endPoint)}">
          return '<bs:printTime time="${timeInterval.durationSeconds}"/>';
        </c:when>
        <c:otherwise>return '???';</c:otherwise>
      </c:choose>
    },

    getStartTime: function() {
      <c:choose>
      <c:when test="${empty timeInterval or isNeverStarts}">
        return '???';
      </c:when>
      <c:otherwise>
        return '<bs:date value="${buildEstimates.timeInterval.startPoint.absoluteTime}" pattern="dd MMM yy HH:mm:ss"/>';
      </c:otherwise>
      </c:choose>
    },

    getFinishTime: function() {
      <c:choose>
        <c:when test="${not isNeverStarts and (not empty timeInterval) and (not empty timeInterval.endPoint)}">
          return '<bs:date value="${timeInterval.endPoint.absoluteTime}" pattern="dd MMM yy HH:mm:ss"/>';
        </c:when>
        <c:otherwise>return '???';</c:otherwise>
      </c:choose>
    },

    getTimeFrame: function() {
    <c:choose>
    <c:when test="${empty timeInterval or isNeverStarts}">
      return 'unknown';
    </c:when>
    <c:when test="${empty timeInterval.endPoint}">
      return '<bs:date value="${buildEstimates.timeInterval.startPoint.absoluteTime}" pattern="dd MMM yy HH:mm"/> - ???';
    </c:when>
    <c:otherwise>
      return '<bs:interval from="${buildEstimates.timeInterval.startPoint.absoluteTime}" to="${buildEstimates.timeInterval.endPoint.absoluteTime}"/>';
    </c:otherwise>
    </c:choose>

    },

    getAgentLink: function() {
      var retStr = '';
      <c:if test="${(not empty buildAgent)}">
        <c:set var="str"><bs:agentDetailsFullLink agent="${buildAgent}"/></c:set>
        retStr = '<bs:escapeForJs text="${str}"/>';
      </c:if>
      return retStr;
    },

    getBuildLink: function() {
      var retStr = '';
      <c:if test="${(not empty timeInterval) and (buildEstimates.delayed) and (not empty buildAgent)}">
        <c:set var="runningBuild" value="${buildAgent.runningBuild}"/>
        <c:if test="${not empty runningBuild and afn:permissionGrantedForBuild(runningBuild, 'VIEW_PROJECT')}">
          <c:set var="runningBuildType" value="${runningBuild.buildType}"/>
          <c:if test="${not empty runningBuildType}">
            <c:set var="str"><bs:resultsLink build="${runningBuild}" noPopup="true"><c:out value="${runningBuildType.fullName}"/></bs:resultsLink></c:set>
            retStr = '<bs:escapeForJs text="${str}"/><c:if test="${runningBuild.probablyHanging}"> (probably hanging)</c:if>';
          </c:if>
        </c:if>
      </c:if>
      return retStr;
    },

    getEstimate: function() {
      <c:choose>
        <c:when test="${empty timeInterval or isNeverStarts}">
          return '???';
        </c:when>
        <c:when test="${timeInterval.startPoint.relativeSeconds >= 60}">
          var result = BS.Util.formatSeconds(this.secondsToStart);
          <c:if test="${buildEstimates.delayed}">result = 'More than ' + result;</c:if>
          return result;
        </c:when>
        <c:when test="${timeInterval.startPoint.relativeSeconds <= 0 || buildEstimates.delayed}">
          return 'Delayed';
        </c:when>
        <c:otherwise>
          return BS.Util.formatSeconds(this.secondsToStart);
        </c:otherwise>
      </c:choose>
    },

    getWaitReason: function() {
      <c:choose>
        <c:when test="${not empty buildEstimates and not empty buildEstimates.waitReason}">
          return '<bs:escapeForJs text="${buildEstimates.waitReason.description}"/>';
        </c:when>
        <c:otherwise>
          return null;
        </c:otherwise>
      </c:choose>
    }
  };
</c:if>
