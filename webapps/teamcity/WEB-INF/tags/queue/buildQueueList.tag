<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="afn" uri="/WEB-INF/functions/authz" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="queue" tagdir="/WEB-INF/tags/queue" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %><%@
    taglib prefix="l" tagdir="/WEB-INF/tags/layout"

%><jsp:useBean id="buildQueue" type="jetbrains.buildServer.controllers.queue.BuildQueueForm" scope="request"
/><c:set var="canReorderQueue" value="${afn:permissionGrantedForAnyProject('REORDER_BUILD_QUEUE')}"
/><c:set var="selectedBuildType" value="${param['buildTypeId']}"
/><bs:trimWhitespace>
  <%--@elvariable id="currentUser" type="jetbrains.buildServer.users.SUser"--%>
  <form action="#" id="buildQueueForm">
    <div id="buildQueue">
      <div class="queueWrapper">
        <c:if test="${buildQueue.numberOfItems == 0 && buildQueue.filteredByAgentPoolMode}">
          <div>No builds found for the selected pool in the build queue.</div>
        </c:if>
        <c:if test="${buildQueue.numberOfItems == 0 && !buildQueue.filteredByAgentPoolMode}">
          <div>Build queue is empty.</div>
        </c:if>
        <c:if test="${buildQueue.numberOfItems > 0 and fn:length(buildQueue.items) == 0}">
          <div class="attentionComment">You do not have permissions to see all of the items currently in the queue.</div>
        </c:if>
        <c:if test="${fn:length(buildQueue.items) > 0}">
          <c:if test="${buildQueue.numberOfItems > fn:length(buildQueue.items)}">
            <div class="attentionComment">You do not have permissions to see all of the <strong>${buildQueue.numberOfItems}</strong> items.</div>
          </c:if>

          <div class="queueActionMessages clearfix">
            <c:set var="canRemoveItems" value="${afn:permissionGrantedForAnyProject('CANCEL_BUILD') or
                                                 afn:permissionGrantedForAnyProject('CANCEL_ANY_PERSONAL_BUILD')}"/>
            <c:if test="${canReorderQueue}">
              <c:if test="${fn:length(buildQueue.items) > 1}">
                <span>Use drag-and-drop to reorder the queue.</span>
              </c:if>
            </c:if>

            <span class="messagesHolder">
              <span id="savingData">Saving...</span>
              <span id="dataSaved">New build queue order applied</span>
            </span>
          </div>
          <script type="text/javascript">
            BS.Queue.personalBuilds = [];
          </script>
          <c:if test="${canRemoveItems}">
            <div class="queueActions" id="queueActionsHolder">
              <bs:simplePopup controlId="queueActions" linkOpensPopup="true"
                              popup_options="delay: 0, hideDelay: 0, shift: {x: -80, y: 20}, className: 'quickLinksMenuPopup'">
                <jsp:attribute name="content">
                  <ul class="menuList">
                    <l:li onclick="BS.StopBuildDialog.showStopBuildDialog(BS.Util.getSelectedValues($('buildQueueForm'), 'removeItem'), '', true);">
                      <a href="#" onclick="return false">Remove selected builds from the queue</a>
                    </l:li>
                    <l:li onclick="if (BS.Queue.personalBuilds.length == 0) { alert('There are no personal builds of yours in the queue.'); } else BS.StopBuildDialog.showStopBuildDialog(BS.Queue.personalBuilds, '', true);">
                      <a href="#" onclick="return false">Remove your personal builds from the queue</a>
                    </l:li>
                  </ul>
                </jsp:attribute>
                <jsp:body>Remove</jsp:body>
              </bs:simplePopup>
            </div>
          </c:if>

          <div class="clr"></div>
          <queue:queueEstimates buildQueue="${buildQueue}"/>
          <table cellspacing="0" cellpadding="0" class="buildQueueTable" id="queueTable">
            <tr class="header">
              <td>
                <table class="buildQueueTableHeader">
                  <tr>
                    <td class="moveTop"><img src="<c:url value='/img/dragAndDrop/moveTopActive.gif'/>" alt="" style="visibility:hidden;"/></td>
                    <td class="orderNum"></td>
                    <td class="branch"></td>
                    <c:if test="${not buildQueue.filteredByBuildTypeMode}">
                      <td class="configurationName">Build configuration</td>
                    </c:if>
                    <td class="remaining">Time to start</td>
                    <td class="triggeredBy">Triggered by</td>
                    <td class="canRunOn">Can run on</td>
                    <c:if test="${canRemoveItems}">
                      <td class="remove" style="padding-right: 6px;">
                        <forms:checkbox name="removeAll" onclick="if (this.checked) BS.Util.selectAll($('buildQueueForm'), 'removeItem'); else BS.Util.unselectAll($('buildQueueForm'), 'removeItem')"/>
                      </td>
                    </c:if>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                <div id="queueTableRows">
                  <c:forEach items="${buildQueue.hiddenTopItems}" var="item">
                    <div style="display: none;" id="queue_${item.itemId}"></div>
                  </c:forEach>
                  <c:forEach var="queuedBuild" items="${buildQueue.items}" varStatus="pos">
                    <jsp:useBean id="queuedBuild" type="jetbrains.buildServer.serverSide.SQueuedBuild"/>
                    <c:set var="personalBuild" value="${queuedBuild.personal}"/>
                    <c:set var="parentBuildTypeId" value="${queuedBuild.buildTypeId}"/>
                    <c:if test="${personalBuild}"><c:set var="parentBuildTypeId" value="${queuedBuild.buildType.sourceBuildType.buildTypeId}"/></c:if>
                    <c:set var="myBuild" value="${personalBuild && currentUser == queuedBuild.buildType.user}"/>
                    <c:set var="foreignBuild" value="${personalBuild && not myBuild}"/>
                    <c:set var="draggable" value="${canReorderQueue}"/>
                    <c:set var="draggableClass" value="${draggable ? 'draggable' : 'notDraggable'}"/>
                    <c:set var="additionalClasses"><c:if test="${selectedBuildType == parentBuildTypeId}">selected</c:if> <c:if test="${foreignBuild}">foreignPersonalBuild</c:if></c:set>
                    <div style="padding-top:2px;"
                         class="${draggableClass} ${additionalClasses}" id="queue_${queuedBuild.itemId}"
                         <c:if test="${draggable}">onmouseover="this.className = 'draggable draggableHover ${additionalClasses}';" onmouseout="this.className = 'draggable ${additionalClasses}'"</c:if>
                        >
                      <a name="ref${parentBuildTypeId}"></a>
                      <table cellspacing="0" cellpadding="0">
                        <tr>
                          <td class="moveTop">
                            <c:if test="${draggable}">
                              <c:set var="moveTopIcon" value="moveTopInactive.gif"/>
                              <img src="<c:url value='/img/dragAndDrop/${moveTopIcon}'/>"
                                   class="moveTopIcon"
                                  <c:if test="${draggable}">
                                    onmouseover="this._oldSrc = this.src; this.src = '<c:url value='/img/dragAndDrop/moveTopActive.gif'/>';"
                                    onmouseout="this.src = this._oldSrc;"
                                    onclick="BS.Queue.moveToTop('queue_${queuedBuild.itemId}')"
                                  </c:if>
                                   <c:if test="${pos.first and empty buildQueue.hiddenTopItems}">style='visibility:hidden'</c:if>
                                  <c:if test="${not pos.first or not empty buildQueue.hiddenTopItems}"> title="Move to top"  alt="Move to top"</c:if>
                                  />
                            </c:if>
                          </td>
                          <c:set var="promo" value="${queuedBuild.buildPromotion}"/>
                          <td class="orderNum">#${buildQueue.orderNumber[queuedBuild]}<bs:promotionCommentIcon promotion="${promo}"/></td>
                          <c:set var="branch" value=""/>
                          <bs:_branchTd branch="${promo.branch}">
                            <jsp:attribute name="afterBranch">
                              <script type="text/javascript">
                                $j("#queueTable .buildQueueTableHeader td.branch").addClass("hasBranch").html("Branch");
                              </script>
                            </jsp:attribute>
                          </bs:_branchTd>
                          <c:if test="${not buildQueue.filteredByBuildTypeMode}">
                            <td class="configurationName">
                              <bs:buildTypeLinkFull buildType="${queuedBuild.buildType}"/>
                            </td>
                          </c:if>
                          <td id="estimate${queuedBuild.itemId}" class="remaining">
                            <span data-itemId='${queuedBuild.itemId}' class="remaining">
                              <span id="estimate${queuedBuild.itemId}:text"></span><img class="commentIcon" src="<c:url value='/img/commentIcon.gif'/>" alt="" border="0"
                                                                                        id="estimate${queuedBuild.itemId}:img" width="11" height="11"/>
                            </span>
                          </td>
                          <script type="text/javascript">
                            (function() {
                              var estimate = BS.QueueEstimates.getEstimate('${queuedBuild.itemId}');
                              var timeStr = estimate != null ? estimate.getEstimate() : 'N/A';

                              var textElement = $('estimate${queuedBuild.itemId}:text');
                              if (textElement) {
                                textElement.innerHTML = timeStr;
                              }

                              <c:if test="${myBuild}">
                              if (BS.Queue.personalBuilds) {
                                BS.Queue.personalBuilds.push(${queuedBuild.buildPromotion.id});
                              }
                              </c:if>
                            })();
                          </script>
                          <td class="triggeredBy">
                            <queue:triggeredBy queuedBuild="${queuedBuild}" currentUser="${currentUser}"/>
                          </td>
                          <td class="canRunOn">
                            <queue:queuedBuildAgent queuedBuild="${queuedBuild}"/>
                          </td>
                          <authz:authorize projectId="${queuedBuild.buildType.projectId}" allPermissions="CANCEL_BUILD">
                            <jsp:attribute name="ifAccessGranted">
                              <td class="remove">
                                <c:if test="${not personalBuild or myBuild or afn:permissionGrantedForBuildType(queuedBuild.buildType, 'CANCEL_ANY_PERSONAL_BUILD')}">
                                  <c:set var="itemId" value="remove_${queuedBuild.buildPromotion.id}"/>
                                  <a id="${itemId}" class="actionLink red" href="#"
                                     onclick="BS.StopBuildDialog.showStopBuildDialog([${queuedBuild.buildPromotion.id}], '', true); return false" title="Remove this build only">Remove</a>
                                  <forms:checkbox name="removeItem" value="${queuedBuild.buildPromotion.id}"/>
                                </c:if>
                              </td>
                            </jsp:attribute>
                            <jsp:attribute name="ifAccessDenied">
                              <c:if test="${afn:permissionGrantedForAnyProject('CANCEL_BUILD') or afn:permissionGrantedForAnyProject('CANCEL_ANY_PERSONAL_BUILD')}">
                                <td class="remove" title="You do not have enough permissions to remove this build">
                                  <forms:checkbox name="removeItem" value="${queuedBuild.buildPromotion.id}" disabled="true"/>
                                </td>
                              </c:if>
                            </jsp:attribute>
                          </authz:authorize>
                        </tr>
                      </table>
                    </div>
                  </c:forEach>
                  <script type="text/javascript">
                    (function() {
                      var queueTable = $j('#queueTable');

                      if (queueTable.find('td.hasBranch').length > 0) {
                        queueTable.find('td.branch').addClass('hasBranch');
                      }
                    })();
                  </script>
                </div>
              </td>
            </tr>
          </table>
        </c:if>
      </div>
    </div>
    <div id="agentsInfoDiv" class="popupDiv"> </div>
  </form>

  <c:if test="${canReorderQueue and fn:length(buildQueue.items) > 0}">
    <script type="text/javascript">
      (function() {
        Sortable.create(BS.Queue.containerId, {
          tag : "div",
          onUpdate: function(el) {
            BS.Queue.scheduleUpdate(el);
            $(el).addClassName('queueRowDragged');
          }
        });
      })();
    </script>
  </c:if>
</bs:trimWhitespace>
