<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags"%><%@
    taglib prefix="queue" tagdir="/WEB-INF/tags/queue"%><%@
    attribute name="itemId" type="java.lang.String" %><%@
    attribute name="branch" type="jetbrains.buildServer.serverSide.Branch" %><%@
    attribute name="noPopup" type="java.lang.Boolean"
%><c:set var="link"><a href="<c:url value='/queue.html?buildTypeId=${itemId}#ref${itemId}'/>" title="Click to see the Build Queue"><jsp:doBody/></a></c:set
><c:set var="branchName"
    ><c:if test="${not empty branch}">'<bs:escapeForJs text="${branch.name}" forHTMLAttribute="true"/>'</c:if
    ><c:if test="${empty branch}">null</c:if></c:set
><c:choose
  ><c:when test="${not noPopup}"
    ><bs:popupControl showPopupCommand="BS.QueuedBuildsPopup.showQueuedBuilds(this, '${itemId}', ${branchName})"
                      hidePopupCommand="BS.QueuedBuildsPopup.hidePopup()"
                      stopHidingPopupCommand="BS.QueuedBuildsPopup.stopHidingPopup()"
                      controlId="queuedBuilds:${itemId}"
      >${link}</bs:popupControl
  ></c:when
  ><c:otherwise>${link}</c:otherwise
></c:choose>