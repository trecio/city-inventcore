<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ attribute name="patchUrl" required="true"
  %><%@ attribute name="showMode" required="true"   
  %>
<c:choose>
  <c:when test="${showMode == 'compact'}">
    <a class="noUnderline activateFileLink" href="#" onclick="BS.Activator.doOpen('patch?file=${patchUrl}'); return false"
       title="Download patch to IDE"><jsp:doBody/>
       <img class="activateFileIcon" src="<c:url value='/img/openInIde.gif'/>">
    </a>  
  </c:when>
  <c:otherwise>
    <img style="vertical-align:middle;" src="<c:url value='/img/openInIde.gif'/>">  
    <a href="#" onclick="BS.Activator.doOpen('patch?file=${patchUrl}'); return false" title="Download patch to IDE">Download patch to IDE<jsp:doBody/></a>
  </c:otherwise>
</c:choose>


