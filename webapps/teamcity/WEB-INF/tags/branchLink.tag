<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="branch" required="true" rtexprvalue="true" type="jetbrains.buildServer.serverSide.Branch" %><%@
    attribute name="branchHolder" required="true" rtexprvalue="true" type="java.lang.Object" description="Any object with 'buildTypeId' and 'projectId' properties (normally build or build promotion)" %><%@
    attribute name="noLink" required="false" type="java.lang.Boolean"
%><c:set var="text"><jsp:doBody/></c:set><c:if test="${empty text}"><c:set var="text"><c:out value="${branch.displayName}"/></c:set></c:if
 ><c:choose
 ><c:when test="${noLink}"><span class="branchName">${text}</span></c:when
 ><c:otherwise
   ><a href="#" onmouseover="BS.Branch.setLink(this, '${branchHolder.buildTypeId}', '${branchHolder.projectId}', '<bs:escapeForJs forHTMLAttribute="true" text="${branch.name}"/>');"
                class="branchName">${text}</a
 ></c:otherwise
 ></c:choose>
