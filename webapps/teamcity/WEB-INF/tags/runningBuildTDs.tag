<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="buildData" required="true" rtexprvalue="true" type="jetbrains.buildServer.serverSide.SBuild" %><%@
    attribute name="rowClass" required="false"

%><bs:buildRow build="${buildData}" rowClass="${rowClass}"
               showBranchName="true"
               showBuildNumber="true"
               showStatus="true"
               showArtifacts="true"
               showCompactArtifacts="false"
               showChanges="true"
               showProgress="true"
               showStop="true"/>