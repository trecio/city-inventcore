<script type="text/javascript">
  (function() {
    var classNames = [];

    /*@cc_on
     classNames.push('ua-ie');
     @*/

    var ua = window.navigator.userAgent.toLowerCase();

    if (ua.indexOf('mac') > -1) {
      classNames.push('ua-mac');
    }

    document.documentElement.className = classNames.join(' ');
  })();
</script>