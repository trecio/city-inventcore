<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="build" required="true" type="jetbrains.buildServer.serverSide.SFinishedBuild" %><%@
    attribute name="pin" required="true" type="java.lang.Boolean" %><%@
    attribute name="onBuildPage" required="true" type="java.lang.Boolean" %><%@
    attribute name="style" required="false"%><%@
    attribute name="className" required="false"%>
<c:set var="tags"><bs:_printTags build="${build}"/></c:set
><c:set var="escapedTags"><bs:escapeForJs text="${tags}" forHTMLAttribute="true"/></c:set
><c:set var="pinComment"
    ><c:if test="${build.pinComment != null}">, '<bs:escapeForJs forHTMLAttribute="true" text="${build.pinComment.comment}"/>'</c:if
></c:set
><c:set var="onclick"
    >return BS.PinBuildDialog.showPinBuildDialog(${build.buildId}, ${pin}, '${escapedTags}', ${build.buildPromotion.numberOfDependencies}${pinComment});</c:set
><a href="#" id="pinLink${build.buildId}" class="pinLink ${className}" style="${style}" onclick="${onclick}"><jsp:doBody/></a>