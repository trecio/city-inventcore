<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="handleId" rtexprvalue="true" %><%@
    attribute name="expandedIcon" %><%@
    attribute name="imgTitle" %><%@
    attribute name="style" required="false" rtexprvalue="true"

%><c:if test="${empty expandedIcon}"
 ><c:set var="expandedIcon" value="blockExpanded.gif" scope="request"/></c:if
 ><c:if test="${empty imgTitle}"><c:set var="imgTitle" value="Click to collapse/expand details"/></c:if
 ><img src="<c:url value="/img/${expandedIcon}"/>" title="${imgTitle}" class="handle"
       <c:if test="${not empty style}">style="${style}" </c:if><c:if test="${not empty handleId}">id="blockHandle${handleId}"</c:if> />