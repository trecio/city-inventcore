<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="controlId" required="true" %><%@
    attribute name="content" fragment="true" required="true" %><%@
    attribute name="linkOpensPopup" fragment="false" required="false" type="java.lang.Boolean" %><%@
    attribute name="style" required="false" %><%@
    attribute name="controlClass" type="java.lang.String" required="false" %><%@
    attribute name="popup_options" required="false"
%><bs:trimWhitespace>
<c:if test="${linkOpensPopup}">
  <c:set var="body"><a href="#" class="popupLink" onclick="return false"><jsp:doBody/></a></c:set>
</c:if>
<c:if test="${not linkOpensPopup}">
  <c:set var="body"><jsp:doBody/></c:set>
</c:if>
<span class="pc ${controlClass}" id="sp_span_${controlId}"
    >${body}<img id="${controlId}" src="<c:url value='/img/popUpControl.gif'/>" class="toggle"/>
</span>
<div id="sp_span_${controlId}Content" class="popupDiv"><jsp:invoke fragment="content"/></div>
<script>
  BS.install_simple_popup('sp_span_${controlId}', {${popup_options}});
</script>
</bs:trimWhitespace>