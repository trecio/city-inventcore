<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %><%@

    attribute name="buildTypes" type="java.util.List" required="true" %><%@
    attribute name="selectedBuildTypeId" type="java.lang.String" required="true" %><%@
    attribute name="url" type="java.lang.String" required="true" %><%@
    attribute name="useShortName" type="java.lang.Boolean" required="false" %><%@
    attribute name="style" type="java.lang.String" required="false" %><%@
    attribute name="className" type="java.lang.String" required="false"

%><forms:select name="buildType" enableFilter="true" style="${style}" className="${className}"
  ><forms:option value="">&lt;All build configurations&gt;</forms:option
  ><c:forEach items="${buildTypes}" var="buildType"
    ><forms:option value="${buildType.buildTypeId}" selected="${buildType.buildTypeId == selectedBuildTypeId}"
      ><c:out value="${useShortName ? buildType.name : buildType.fullName}"
    /></forms:option
  ></c:forEach
></forms:select>
<script type="text/javascript">
  $j("#buildType").change(function(e) {
    var selected = $j(this).find("option:selected");
    var url = '${url}' + '&buildTypeId=' + selected.attr("value");
    BS.openUrl(e, url);
    return false;
  });
</script>