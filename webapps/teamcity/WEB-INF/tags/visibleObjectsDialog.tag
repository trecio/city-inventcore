<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms"%><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags"%><%@

    attribute name="actionUrl" type="java.lang.String" required="true"  %><%@
    attribute name="object" type="java.lang.String" required="true"  %><%@
    attribute name="objectHumanReadable" type="java.lang.String" required="true"  %><%@
    attribute name="jsDialog" type="java.lang.String" required="true"  %><%@
    attribute name="visibleObjectBean" type="jetbrains.buildServer.controllers.profile.VisibleObjectBean" required="true" %><%@
    attribute name="resetAction" type="java.lang.Boolean" required="false"

%><bs:modalDialog formId="${object}_visibleForm"
                  title="Configure visible ${objectHumanReadable}"
                  action="${actionUrl}"
                  closeCommand="${jsDialog}.close();"
                  saveCommand="${jsDialog}.save();">
  <bs:visibleObjectsDialogContent showReorderButtons="${true}"
                                  object="${object}"
                                  jsDialog="${jsDialog}"
                                  visibleTitle="Visible ${objectHumanReadable}"
                                  hiddenTitle="Hidden ${objectHumanReadable}"
                                  filterText="filter ${objectHumanReadable}">
    <jsp:attribute name="visibleOptions">
      <c:forEach items="${visibleObjectBean.orderedVisible}" var="objectBean">
        <option value="${objectBean.id}" <c:if test="${objectBean.strong}">class="strong"</c:if>>
          <c:out value="${objectBean.name}"/>
        </option>
      </c:forEach>
    </jsp:attribute>
    <jsp:attribute name="hiddenOptions">
      <c:forEach items="${visibleObjectBean.sortedHidden}" var="objectBean">
        <option class="inplaceFiltered <c:if test="${objectBean.strong}">strong</c:if>" value="${objectBean.id}">
          <c:out value="${objectBean.name}"/>
        </option>
      </c:forEach>
    </jsp:attribute>
  </bs:visibleObjectsDialogContent>
  <input type="hidden" id="${object}_order" name="${object}_order" value="${visibleObjectBean.objectsOrder}"/>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="${jsDialog}.close()"/>
    <forms:submit label="Save"/>
    <c:if test="${resetAction}">
      <forms:submit type="button" label="Reset to defaults" onclick="${jsDialog}.resetAction();"/>
    </c:if>
    <forms:saving id="${object}_savingVisible"/>
  </div>
</bs:modalDialog>