<%@ tag import="jetbrains.buildServer.controllers.BranchUtil" %><%@
    tag import="jetbrains.buildServer.vcs.SelectPrevBuildPolicy" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="util" uri="/WEB-INF/functions/util" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="l" tagdir="/WEB-INF/tags/layout" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %><%@
    taglib prefix="resp" tagdir="/WEB-INF/tags/responsible" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@

    attribute name="currentUser" required="true" type="jetbrains.buildServer.users.SUser" %><%@
    attribute name="buildType" type="jetbrains.buildServer.serverSide.BuildTypeEx" required="true"%><%@
    attribute name="branch" type="jetbrains.buildServer.serverSide.BranchEx" required="false"%><%@
    attribute name="theRunningBuilds" required="false" type="java.util.List" %><%@
    attribute name="problemsCounters" required="true" type="java.lang.Object" %><%@
    attribute name="shouldShowHideBuildTypeIcon" required="true" type="java.lang.Boolean"

%><bs:trimWhitespace>
  <%--@elvariable id="problemsCounters" type="jetbrains.buildServer.serverSide.ProblemsSummary.Counters"--%>
  <c:if test="${empty branch}">
    <c:set var="pendingChanges" value="<%=BranchUtil.getPendingChangesInAllBranches(buildType, null)%>"/>
    <c:set var="activeBuilds" value="<%=BranchUtil.getLastFinishedBuilds(buildType.getActiveBranches())%>"/>
    <c:set var="hasBuilds" value="${not empty buildType.lastChangesFinished or not empty theRunningBuilds or fn:length(activeBuilds) > 0}" scope="request"/>
    <c:set var="queuedBuilds" value="${buildType.queuedBuilds}"/>
    <c:set var="status" value="${buildType.status}"/>
  </c:if>
  <c:if test="${not empty branch}">
    <c:set var="pendingChanges" value="<%=branch.getDetectedChanges(SelectPrevBuildPolicy.SINCE_LAST_BUILD, null)%>"/>
    <c:set var="theRunningBuilds" value="<%=branch.getRunningBuilds(currentUser)%>"/>
    <c:set var="hasBuilds" value="${not empty branch.lastChangesFinished or not empty theRunningBuilds}" scope="request"/>
    <c:set var="queuedBuilds" value="${branch.queuedBuilds}"/>
    <c:set var="status" value="${branch.status}"/>
  </c:if>

  <div class="tableCaption" id="${buildType.buildTypeId}-div">
    <c:if test="${shouldShowHideBuildTypeIcon}">
      <authz:authorize allPermissions="CHANGE_OWN_PROFILE">
        <div class="relative">
          <a href="#" onclick="return BS.hideBuildType('${buildType.projectId}', '${buildType.buildTypeId}');"
             class="close-bt" title="Hide this configuration">&nbsp;</a>
        </div>
      </authz:authorize>
    </c:if>
    <table class="runTable">
      <tr>
        <c:if test="${not empty pendingChanges}">
          <td class="pendingMessage"><bs:pendingChangesLink buildType="${buildType}"
                                                            pendingChanges="${pendingChanges}"
                                                            branch="${branch}"
                                                            showForAllBranches="${empty branch}"/></td>
        </c:if>
        <td class="runningStatus">
            <span class="runningStatus">
              <c:choose
                ><c:when test="${empty theRunningBuilds}"><bs:icon icon="empty.gif"/></c:when
                ><c:otherwise><bs:runningMultipleBuildIcon currentStatuses="${theRunningBuilds}"/></c:otherwise
              ></c:choose>
            </span>
          <bs:buildTypeStatusText theRunningBuilds="${theRunningBuilds}"
                                  queuedBuilds="${queuedBuilds}"
                                  branch="${branch}"
                                  buildType="${buildType}"/>
        </td>
        <td class="runButton">
          <authz:authorize projectId="${buildType.projectId}" anyPermission="RUN_BUILD">
            <bs:runBuild buildTypeId="${buildType.buildTypeId}"
                         redirectTo=""
                         userBranch="${not empty branch ? branch.name : ''}"/>
          </authz:authorize>
        </td>
      </tr>
    </table>

    <c:set var="handle">
      <bs:handle handleId="${buildType.buildTypeId}"
                 expandedIcon="${status.failed ? 'eluruDesign/projectFailingSmallExpanded.png' :
                                   status.successful ? 'eluruDesign/projectSuccessfulSmallExpanded.png' :
                                   'eluruDesign/projectNeutralSmallExpanded.png'}"/>
    </c:set>
    <c:choose>
      <c:when test="${hasBuilds}">${handle}</c:when>
      <c:otherwise><span style="visibility:hidden;">${handle}</span></c:otherwise>
    </c:choose>

    <c:set var="title">Click to open &quot;<c:out value="${buildType.name}"/>&quot; home page</c:set>
    <c:if test="${buildType.paused}">
      <c:set var="title" value="Build configuration is paused. ${title}"/>
    </c:if>
    <c:if test="${not buildType.paused and status.failed}">
      <c:set var="title" value="Build configuration is failing. ${title}"/>
    </c:if>

    <bs:buildTypeLinkFull buildType="${buildType}" popupMode="no_self">
      <bs:buildTypeLink buildType="${buildType}" style="padding-left:0px;" title="${title}"/>
    </bs:buildTypeLinkFull>

    <span class="addMessage lastBuilt"><c:if test="${not hasBuilds}">No builds to display</c:if></span>

    <bs:systemProblemMarker buildTypeId="${buildType.buildTypeId}" branch="${branch}"/>

    <c:if test="${buildType.paused}">
      <c:set var="pausedLink">
        <bs:togglePopup linkText="Paused">
          <jsp:attribute name="content">
            <c:set var="pauseComment" value="${buildType.pauseComment}"/>
            <c:if test="${not empty pauseComment}">
              <div class="name-value"><table>
                <c:if test="${not empty pauseComment.user}">
                  <tr>
                    <th>Paused by:</th>
                    <td><c:out value="${pauseComment.user.descriptiveName}"/></td>
                  </tr>
                </c:if>
                <tr>
                  <th>Time:</th>
                  <td><bs:date value="${pauseComment.timestamp}"/></td>
                </tr>
                <c:if test="${not empty pauseComment.comment}">
                  <tr>
                    <th>Comment:</th>
                    <td class="resp-comment"><bs:out value="${pauseComment.comment}"/></td>
                  </tr>
                </c:if>
              </table></div>
            </c:if>
            <authz:authorize projectId="${buildType.projectId}" allPermissions="PAUSE_ACTIVATE_BUILD_CONFIGURATION">
              <div class="actions">
                <a href="#" onclick="<bs:_pauseBuildTypeLinkOnClick buildType="${buildType}" pause="false"/>; return false;">Activate...</a>
              </div>
            </authz:authorize>
          </jsp:attribute>
        </bs:togglePopup>
      </c:set>
      <span class="addMessage paused"><img src='<c:url value="/img/buildStates/paused.png"/>'/> ${pausedLink}</span>
    </c:if>

    <c:if test="${empty branch or branch.defaultBranch}">
      <c:set var="resp" value="${buildType.responsibilityInfo}"/>
      <c:if test="${not empty resp and (resp.state.active or resp.state.fixed)}">
        <c:set var="responsibilityIcon" value="/img/investigate.gif"/>
        <c:if test="${resp.state.active}">
          <c:set var="responsibleUser" value="${resp.responsibleUser}"/>
          <c:if test="${responsibleUser == currentUser}">
            <c:set var="responsibilityNote"><span class="${currentUser.highlightRelatedDataInUI ? 'highlightChanges' : ''}">You are investigating the build configuration</span></c:set>
          </c:if>
          <c:if test="${responsibleUser != currentUser}">
            <c:set var="responsibilityNote"><c:out value="${responsibleUser.descriptiveName}"/> is investigating the build configuration</c:set>
          </c:if>
        </c:if>
        <c:if test="${resp.state.fixed}">
          <c:set var="responsibilityNote">Fixed by <c:out value="${resp.responsibleUser == currentUser ? 'you' : resp.responsibleUser.descriptiveName}"/></c:set>
          <c:set var="responsibilityIcon" value="/img/buildStates/fixedTestResp.gif"/>
        </c:if>
        <c:set var="responsibilityNote">
          <img class="responsibilityIcon" src='<c:url value="${responsibilityIcon}"/>'/><span class="text">${responsibilityNote}</span>
        </c:set>

          <span class="addMessage responsibility" title="Click to view details or update investigation">
            <bs:togglePopup linkText="${responsibilityNote}">
              <jsp:attribute name="content">
                <bs:responsibleTooltip responsibility="${resp}" buildTypeRef="${buildType}"/>
              </jsp:attribute>
            </bs:togglePopup>
          </span>
      </c:if>
    </c:if>

    <c:if test="${not empty problemsCounters and problemsCounters.hasData}">
      <c:set var="failedNum" value="${problemsCounters.failedTests}"/>
      <c:set var="investNum" value="${problemsCounters.investigatedTests}"/>
      <c:set var="fixedNum" value="${problemsCounters.markedAsFixedTests}"/>
      <c:set var="mutedNum" value="${problemsCounters.mutedTests}"/>
      <span class="problemsSummary" title="View problems summary for this build configuration">
        <a class="summaryLink" href="<c:url value="project.html?projectId=${buildType.projectId}&tab=problems&buildTypeId=${buildType.buildTypeId}"/>">
          <span>Test<span class="problemDetails"> failure</span>s:</span>
          <c:if test="${failedNum > 0}">
            <span>
              <img class="problemIcon" src="<c:url value="/img/buildStates/buildFailed.gif"/>" alt=""/>${failedNum}
              <span class="problemDetails">not investigated<c:if test="${investNum + fixedNum + mutedNum > 0}">,</c:if></span>
            </span>
          </c:if>
          <c:if test="${investNum > 0}">
            <span>
              <img class="problemIcon" src="<c:url value="/img/investigate.gif"/>" alt=""/>${investNum}
              <span class="problemDetails">under investigation<c:if test="${fixedNum + mutedNum > 0}">,</c:if></span>
            </span>
          </c:if>
          <c:if test="${fixedNum > 0}">
            <span>
              <img class="problemIcon" src="<c:url value="/img/buildStates/fixedTestResp.gif"/>" alt=""/>${fixedNum}
              <span class="problemDetails">marked as fixed<c:if test="${mutedNum > 0}">,</c:if></span>
            </span>
          </c:if>
          <c:if test="${mutedNum > 0}">
            <span>
              <img class="problemIcon" src="<c:url value="/img/muted-red.gif"/>" alt=""/>${mutedNum}
              <span class="problemDetails">muted</span>
            </span>
          </c:if>
        </a>
      </span>
    </c:if>
  </div>

  <c:set var="blockTypeId" value="bt${buildType.buildTypeId}"/>
  <div class="overviewTypeTableContainer" id="btb${buildType.buildTypeId}" style="${util:blockHiddenCss(pageContext.request, blockTypeId, false)}">
    <c:if test="${hasBuilds}">
      <div class="bt-separator"></div>

      <table cellspacing="0" class="overviewTypeTable">
        <c:forEach items="${theRunningBuilds}" var="buildData">
          <tr><bs:runningBuildTDs buildData="${buildData}"/></tr>
        </c:forEach>
        <c:if test="${empty branch}">
          <c:forEach var="build" items="${activeBuilds}">
            <tr class="lastFinished"><bs:runningBuildTDs buildData="${build}"/></tr>
          </c:forEach>
        </c:if>
        <c:if test="${not empty branch}">
          <c:set var="build" value="${branch.lastChangesFinished}"/>
          <c:if test="${build != null}">
            <tr class="lastFinished"><bs:runningBuildTDs buildData="${build}"/></tr>
          </c:if>
        </c:if>
      </table>

      <script type="text/javascript">
        <l:blockState blocksType="${blockTypeId}" />
        <c:set var="imgName" value="${status.failed ? 'projectFailingSmall.png' :
                                      status.successful ? 'projectSuccessfulSmall.png' : 'projectNeutralSmall.png'}"/>
        BS.CollapsableBlocks.registerBlock(new BS.BuildTypeBlock("bt", '${buildType.buildTypeId}', 'eluruDesign/${imgName}'));
      </script>
    </c:if>
  </div>
</bs:trimWhitespace>