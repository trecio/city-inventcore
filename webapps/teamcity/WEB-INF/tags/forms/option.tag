<%-- data-title attribute is used for in-place search. OPTION elements don't need an actual title, really --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
    %><%@ attribute name="value" required="true"
    %><%@ attribute name="className" required="false"
    %><%@ attribute name="selected" required="false" type="java.lang.Boolean"
    %><%@ attribute name="disabled" required="false" type="java.lang.Boolean"
    %><%@ attribute name="title" required="false" type="java.lang.String"

%><option <c:if test="${not empty className}"
    >class="${className}" </c:if
    >value="${value}"<c:if test="${selected}"> selected="selected"</c:if><c:if test="${disabled}"> disabled="disabled" </c:if
    > data-title="<c:out value="${title}"></c:out>"><jsp:doBody/></option>