<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
    %><%@attribute name="resetUrl" required="false"
    %><%@attribute name="resetHandler" required="false"
    %><span class="separator">|</span><c:choose
    ><c:when test="${not empty resetUrl}"><a href="${resetUrl}">reset</a></c:when
    ><c:otherwise><a href="#" onclick="${resetHandler}; return false">reset</a></c:otherwise
    ></c:choose> 