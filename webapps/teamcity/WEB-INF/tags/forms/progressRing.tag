<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    attribute name="id" required="false" %><%@
    attribute name="style" required="false" %><%@
    attribute name="className" required="false" %><%@
    attribute name="progressTitle" required="false"

%><c:set var="id" value="${empty id ? 'inProgress' : id}"
/><c:set var="title" value="${empty progressTitle ? 'Please wait...' : progressTitle}"
/><c:set var="className">
  <c:choose>
    <c:when test="${empty className}"><c:out value="progressRing progressRingDefault"/></c:when>
    <c:otherwise><c:out value="progressRing ${className}"/></c:otherwise>
  </c:choose>
  </c:set
><img id="${id}" style="${style}" class="${className}" src="<c:url value='/img/ajax-loader.gif'/>" width="16" height="16" alt="${title}" title="${title}"/>