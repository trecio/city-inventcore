<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="rule" type="java.lang.Object" required="true" %>
<c:choose>
  <c:when test="${rule.watchType == 'ALL_PROJECTS'}">
    <em>All projects</em>
  </c:when>
  <c:when test="${rule.watchType == 'BUILDS_WITH_USER_CHANGES'}">
    <em>Builds with my changes</em>
  </c:when>
  <c:when test="${rule.watchType == 'SPECIFIC_PROJECT'}">
    <c:out value="${rule.project.name}"/> <em>All configurations</em>
  </c:when>
  <c:when test="${rule.watchType == 'SPECIFIC_PROJECT_BUILD_TYPES'}">
    <c:forEach items="${rule.selectedBuildTypes}" var="buildType">
      <c:out value="${buildType.fullName}"/><br/>
    </c:forEach>
  </c:when>
  <c:when test="${rule.watchType == 'SYSTEM_WIDE'}">
    <em>System wide events</em>
  </c:when>
</c:choose>
