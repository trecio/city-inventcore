<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="resp" tagdir="/WEB-INF/tags/responsible" %><%@
    taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %><%@
    attribute name="buildTypes" type="java.util.List" required="true"  %>

<table class="responsibilitiesTable">
  <c:forEach items="${buildTypes}" var="row" varStatus="pos">
    <tr>
      <td class="first">
          <%--@elvariable id="row" type="jetbrains.buildServer.controllers.investigate.BuildTypeInvestigationRow"--%>
        <c:set var="entry" value="${row.entry}"/>
        <c:set var="buildType" value="${entry.buildType}"/>

        <bs:responsibleIcon responsibility="${buildType.responsibilityInfo}" buildTypeRef="${buildType}"/>
        <c:choose>
          <c:when test="${empty param['projectId']}">
            <bs:buildTypeLinkFull buildType="${buildType}"/>
          </c:when>
          <c:otherwise>
            <bs:buildTypeLink buildType="${buildType}"/>
          </c:otherwise>
        </c:choose>
      </td>
      <td class="details">
        <resp:investigationDetails entry="${row.entry}" />
      </td>
      <td class="actions">
        <resp:btResponsibilityActions buildTypeRef="${buildType}" responsibility="${row.entry}"/>
      </td>
    </tr>
  </c:forEach>
</table>
