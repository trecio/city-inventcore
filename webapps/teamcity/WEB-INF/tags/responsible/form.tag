<%@ tag import="jetbrains.buildServer.web.util.SessionUser" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="resp" tagdir="/WEB-INF/tags/responsible" %><%@
    attribute name="buildType" required="true" rtexprvalue="true" type="jetbrains.buildServer.serverSide.SBuildType" %><%@
    attribute name="currentUser" required="true" type="jetbrains.buildServer.users.User"
%><bs:executeOnce id="investigationForm"
    ><bs:modalDialog formId="investigationForm"
                     title="Responsibility"
                     action=""
                     closeCommand="BS.ResponsibilityDialog.close();"
                     saveCommand="BS.ResponsibilityDialog.submit()"
      ><c:if test="${not empty buildType}"
        ><resp:formContent buildType="${buildType}" currentUser="${currentUser}"><jsp:doBody/></resp:formContent
      ></c:if
    ></bs:modalDialog
></bs:executeOnce>