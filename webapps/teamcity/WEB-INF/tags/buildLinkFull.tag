<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz"
  %><%@ attribute name="build" rtexprvalue="true" type="jetbrains.buildServer.serverSide.SBuild" required="true"
  %><c:set var="titleStart" value="Click to open build #"/><c:set var="buildType" value="${build.buildType}"
  /><c:set var="title" value="${titleStart}${build.buildNumber} home page"/><c:if test="${buildType != null}"><bs:buildTypeLinkFull buildType="${build.buildType}"/>&nbsp;</c:if
  ><c:if test="${buildType == null}"><c:set var="title" value="Click to open build home page"/></c:if
  ><bs:buildLink buildId="${build.buildId}" buildTypeId="${build.buildTypeId}" title="${title}"><c:if test="${buildType != null}">#</c:if><c:out value="${build.buildNumber}"/></bs:buildLink>