<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@attribute name="queuedBuild" type="jetbrains.buildServer.serverSide.SQueuedBuild" required="true" %>
<c:set var="promotion" value="${queuedBuild.buildPromotion}"/>
<c:choose>
  <c:when test="${not promotion.personal}">
    <bs:icon icon="pending.gif" alt="Waiting in the queue"/>
  </c:when>
  <c:when test="${promotion.personal}">
    <c:set var="my" value="${promotion.owner == currentUser ? 'personal/my_' : 'personal/'}"/>
    <c:set var="text">
      <bs:personalBuildPrefix buildPromotion="${promotion}"/> waiting in the queue
      <br/>Personal change: <bs:personalBuildComment buildPromotion="${promotion}"/>
    </c:set>
    <span <bs:tooltipAttrs text="${text}"/> ><bs:icon icon="${my}personalPending.gif"/></span>
  </c:when>
</c:choose>
