<%@ tag import="java.util.Collections"
  %><%@ tag import="java.util.List"
  %><%@ tag import="jetbrains.buildServer.users.SUser"
  %><%@ tag import="jetbrains.buildServer.vcs.SelectPrevBuildPolicy"
  %><%@ tag import="jetbrains.buildServer.web.util.SessionUser"
  %><%@ tag import="jetbrains.buildServer.web.util.WebUtil"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ attribute name="buildPromotion" fragment="false" required="true" type="jetbrains.buildServer.serverSide.BuildPromotionEx"
  %><%@ attribute name="highlightIfCommitter" required="false" type="java.lang.Boolean"
  %><%
  boolean highlight = false;
  if (highlightIfCommitter == null || highlightIfCommitter) {
    SUser sessionUser = SessionUser.getUser(request);
    highlight = buildPromotion.getCommitters(SelectPrevBuildPolicy.SINCE_LAST_BUILD).getUsers().contains(sessionUser);
  }
  jspContext.setAttribute("highlightChanges", highlight);
%><bs:popupControl 
    clazz="${highlightChanges ? 'highlightChanges': ''}"
    showPopupCommand="BS.ChangesPopup.showBuildChangesPopup(this, ${buildPromotion.id});"
    hidePopupCommand="BS.ChangesPopup.hidePopup();"
    stopHidingPopupCommand="BS.ChangesPopup.stopHidingPopup();"
    controlId="changes:${buildPromotion.id}"><jsp:doBody/></bs:popupControl>