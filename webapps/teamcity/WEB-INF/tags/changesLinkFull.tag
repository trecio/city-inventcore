<%@ tag import="java.util.List" %><%@
    tag import="jetbrains.buildServer.UserChanges" %><%@
    tag import="jetbrains.buildServer.controllers.changes.ChangesBean" %><%@
    tag import="jetbrains.buildServer.users.SUser" %><%@
    tag import="jetbrains.buildServer.web.util.SessionUser" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@

    attribute name="buildPromotion" fragment="false" required="true" type="jetbrains.buildServer.serverSide.BuildPromotion" %><%@
    attribute name="noPopup" type="java.lang.Boolean" required="false" %><%@
    attribute name="noLink" type="java.lang.Boolean" required="false" %><%@
    attribute name="attrs" required="false" %><%@
    attribute name="noUsername" type="java.lang.Boolean" required="false" %><%@
    attribute name="highlightIfCommitter" type="java.lang.Boolean" required="false"

%><%
  SUser currentUser = SessionUser.getUser(request);
  ChangesBean changesBean = new ChangesBean(buildPromotion, 0, currentUser);
  boolean highlight = currentUser.isHighlightRelatedDataInUI();
  if (highlightIfCommitter != null && !highlightIfCommitter) {
    highlight = false;
  }
  List<UserChanges> changes = changesBean.getChanges();
  jspContext.setAttribute("changes", changes);
  jspContext.setAttribute("totalNum", changesBean.getTotal());
  jspContext.setAttribute("highlight", highlight);

%><c:set var="build" value="${buildPromotion.associatedBuild}"

/><c:if test="${buildPromotion.outOfChangesSequence}"
    ><c:set var="attrs" value="id='changes:${buildPromotion.id}'"
   /><c:set var="sequenceBuild" value="${buildPromotion.sequenceBuild}"
   /><c:if test="${not noPopup}"><bs:changesPopup buildPromotion="${buildPromotion}" highlightIfCommitter="false">History</bs:changesPopup
 ></c:if><c:if test="${noPopup}"><span class="commentText">History</span></c:if></c:if

><c:if test="${not buildPromotion.outOfChangesSequence}"
><c:set var="text"
    ><c:choose
      ><c:when test="${totalNum == 0}">No changes</c:when
      ><c:when test="${noUsername or fn:length(changes) > 1}">Changes (${totalNum})</c:when
      ><c:otherwise><c:set var="username" value="<%=changes.get(0).getCommitters().get(0).getName()%>"/><bs:trim maxlength="14">${username}</bs:trim> (${totalNum})</c:otherwise
    ></c:choose
  ></c:set
  ><c:if test="${not empty changes}"
    ><c:choose
    ><c:when test="${noPopup}"
      ><c:choose
        ><c:when test="${build != null}"><bs:_viewLog build="${build}" title="Click to see changes" attrs="${attrs}" tab="buildChangesDiv">${text}</bs:_viewLog></c:when
        ><c:otherwise>${text}</c:otherwise
      ></c:choose
    ></c:when
    ><c:otherwise
      ><c:choose
        ><c:when test="${build != null and not noLink}"><bs:changesLink build="${build}" highlightIfCommitter="${highlight}">${text}</bs:changesLink></c:when
        ><c:otherwise
          ><bs:changesPopup buildPromotion="${buildPromotion}" highlightIfCommitter="${highlight}">${text}</bs:changesPopup
        ></c:otherwise
      ></c:choose
    ></c:otherwise
    ></c:choose
  ></c:if
><c:if test="${empty changes and not noPopup}"
  ><bs:changesPopup buildPromotion="${buildPromotion}" highlightIfCommitter="${highlight}"><span class="commentText">No changes</span></bs:changesPopup
></c:if
><c:if test="${empty changes and noPopup}"><span class="commentText">No changes</span></c:if
></c:if>