<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ attribute name="id" required="true"
  %><%@ attribute name="title" required="true"
  %>
<div id="${id}" class="popupDiv popupWithTitle">
  <c:set var="closeIcon"><c:url value="/img/close.gif"/></c:set>
  <h3 class="popupWithTitleHeader">
    <div class="closeWindow"><a showdiscardchangesmessage="false" href="#" onclick="BS.Hider.hideDiv('${id}'); return false"><img src="${closeIcon}"/></a></div>
    <c:out value="${title}"/>
  </h3>

  <div class="contentWrapper"><jsp:doBody/></div>
</div>
