<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@attribute name="className" required="false" %>
<%@attribute name="style" required="false" %>
<%@attribute name="mouseovertitle" required="false" %>
<%@attribute name="highlightingDisabled" required="false" %>
<%@attribute name="id" required="false" %>
<!-- whether to enable table highlighting immediately after showing the table or on page load -->
<%@attribute name="highlightImmediately" required="false" type="java.lang.Boolean" %>
<table <c:if test="${not empty id}">id="${id}"</c:if> class="${className}" style="${style}" cellpadding="0" cellspacing="0">
  <jsp:doBody/>
</table>
<c:if test="${not highlightingDisabled}">
<script type="text/javascript">
  (function() {
    var highlightableElements = $j("<c:if test="${not empty id}">#${id} </c:if>td.highlight");
    highlightableElements.each(function(i, element) {
      BS.TableHighlighting.createInitElementFunction.call(this, element, '<bs:escapeForJs text="${mouseovertitle}"/>');
    });

    <c:if test="${highlightImmediately}">
    highlightableElements.mouseover();
    </c:if>
  })();
</script>
</c:if>