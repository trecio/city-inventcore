<%@ attribute name="title"
  %><%@ attribute name="style" %>
<div class="settingsBlock" style="${style}">
  <p class="sbTitle">${title}</p>
  <div style="background-color:#fff; padding: 10px;"><jsp:doBody/></div>
</div>
