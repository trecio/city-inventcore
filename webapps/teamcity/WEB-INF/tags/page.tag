<%--    @elvariable id="serverTC" type="jetbrains.buildServer.serverSide.SBuildServer"
--%><%--@elvariable id="currentUser" type="jetbrains.buildServer.users.SUser"
--%><%@ tag import="jetbrains.buildServer.controllers.PageBeforeContentPagePlaceController"
  %>
<%@ tag import="jetbrains.buildServer.web.openapi.PlaceId" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="afn" uri="/WEB-INF/functions/authz"
  %><%@ taglib prefix="ufn" uri="/WEB-INF/functions/user"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz"
  %><%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin"
  %><%@ taglib prefix="ext" tagdir="/WEB-INF/tags/ext"
  %><%@ taglib prefix="et" tagdir="/WEB-INF/tags/eventTracker"
  %><%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"
  %><%@ taglib prefix="resp" tagdir="/WEB-INF/tags/responsible"
  %><%@ attribute name="page_title" fragment="true"
  %><%@ attribute name="head_include" fragment="true"
  %><%@ attribute name="body_include" fragment="true"
  %><%@ attribute name="sidebar_include" fragment="true"
  %><%@ attribute name="toolbar_include" fragment="true"
  %><%@ attribute name="quickLinks_include" fragment="true"
  %><%@ attribute name="beforeTabs_include" fragment="true"
  %><%@ attribute name="besideTabs_include" fragment="true"
  %><%@ attribute name="disableScrollingRestore" fragment="false"
  %><%@ tag body-content="empty"
  %><!DOCTYPE html><jsp:useBean id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary" scope="request"
    />
<c:set var="pageTitle">
  <c:choose>
    <c:when test="${not empty pageTitle}"><c:out value="${pageTitle}"/> -- TeamCity</c:when>
    <c:otherwise>
      <c:set var="pageTitleFragment">
        <jsp:invoke fragment="page_title"/>
      </c:set>
      <c:if test="${not empty pageTitleFragment}">${pageTitleFragment} -- </c:if>TeamCity
    </c:otherwise>
  </c:choose>
</c:set>
<html>
  <head>
    <title>${pageTitle}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9, chrome=1"/>
    <bs:pageMeta title="${pageTitle}"/>
    <bs:linkCSS>
      /css/main.css
      /css/footer.css
      /css/tabs.css
      /css/buildLog/buildResultsDiv.css
      /css/testGroups.css
      /css/testList.css
      /css/investigation.css
      /css/statusChangeLink.css
      /css/tree/oldTree.css
      /css/tree/tree.css

      /css/quickLinksPopUp.css
      /css/allProjects.css
      /css/forms.css
      /css/runCustomBuild.css
      /css/issues.css
      /css/ellipsis.css

      /css/jquery/jquery-ui-ufd/ufd-base.css
      /css/jquery/jquery-ui-ufd/default/default.css
      /css/jquery/jquery-ui-ufd/popup/popup.css
      /css/jquery/jquery-ui/jquery-ui.custom.css

      /css/jquery/jquery-autocomplete/jquery.ui.autocomplete.css
      /css/autocompletion.css
    </bs:linkCSS>

<!--[if lte IE 7]>
    <style type="text/css">
      @import <c:url value='/css/ie7_fix.css'/>;
    </style>
<![endif]-->

    <bs:ua/>
    <bs:baseUri/>
    <c:if test="${ufn:booleanPropertyValue(currentUser, 'autodetectTimeZone') and empty sessionScope['userTimezoneKey']}">
      <script type="text/javascript" src="<c:url value='/js/bs/timezone.js'/>"></script>
    </c:if>

    <bs:linkScript>
      /js/jquery/jquery-1.7.2.min.js
    </bs:linkScript>

    <script type="text/javascript">
      window.$j = jQuery.noConflict();
    </script>

    <bs:linkScript>
      <%-- jQuery, jQuery UI, and plugins --%>
      /js/jquery/jquery.mousewheel.js
      /js/jquery/jquery-ui-1.8.16.custom.min.js
      /js/jquery/jquery.ui.ufd.js
      /js/jquery/jquery.ui.textarea.js

      /js/bs/teamcity.ui.autocomplete.js
      /js/bs/teamcity.ui.placeholder.js
      /js/bs/teamcity.ui.ellipsis.js

      <%-- Prototype.js/Scriptaculous --%>
      /js/prototype.js
      /js/aculo/effects.js
      /js/aculo/dragdrop.js

      <%-- Other frameworks --%>
      /js/behaviour.js
      /js/underscore-min.js

      <%-- BS - utility components --%>
      /js/bs/bs.js
      /js/bs/cookie.js
      /js/bs/resize.js
      /js/bs/position.js
      /js/bs/refresh.js

      <%-- Crypto stuff --%>
      /js/crypt/md5.js
      /js/crypt/rsa.js
      /js/crypt/jsbn.js
      /js/crypt/prng4.js
      /js/crypt/rng.js
      /js/bs/encrypt.js

      <%-- BS - common components --%>
      /js/bs/tabs.js
      /js/bs/forms.js
      /js/bs/basePopup.js
      /js/bs/menuList.js
      /js/bs/modalDialog.js
      /js/bs/investigation.js
      /js/bs/changeBuildStatus.js
      /js/bs/tree.js
      /js/bs/issues.js
      /js/bs/pluginProperties.js
      /js/bs/serverLink.js
      /js/bs/activation.js

      <%-- BS - business logic --%>
      /js/bs/allProjects.js
      /js/bs/adminActions.js
      /js/bs/runBuild.js
      /js/bs/stopBuild.js
      /js/bs/editParameters.js
      /js/bs/siblingsPopup.js
      /js/bs/branch.js
    </bs:linkScript>

    <script type="text/javascript">
      <c:if test="${not empty currentUser}">
      BS.topNavPane = new TabbedPane('top');

      BS.topNavPane.addTab("overview", {
        caption: "Projects",
        url: "<c:url value="/overview.html"/>"
      });
      <authz:authorize allPermissions="CHANGE_OWN_PROFILE">
      BS.topNavPane.addTab("changes", {
        caption: "My Changes",
        url: "<c:url value="/changes.html"/>"
      });
      </authz:authorize>

      BS.topNavPane.addTab("agents", {
        caption: "Agents (${serverSummary.registeredAgentsCount})",
        url: "<c:url value="/agents.html"/>"
      });

      <c:set var="buildQueue" value="${serverTC.queue}"/>
      BS.topNavPane.addTab("queue", {
        caption: "Build Queue (${buildQueue.numberOfItems})",
        url: "<c:url value="/queue.html"/>"
      });
      </c:if>

      (function() {
        function initPage() {
        <c:if test="${empty disableScrollingRestore or not disableScrollingRestore}">
          if (document.location.href.indexOf('#') == -1) {
            restoreScrolling();
          }
        </c:if>

          if (typeof BS == "undefined") return;
          BS.initReloadBlocker();
        }

        $j(document).ready(initPage);
      })();

      $j(document).ready(function() {
        if (typeof BS == "undefined") return;

        BS.StatisticsMonitor.start("<c:url value='/serverStatistics.html'/>");
        BS.EventTracker.startTracking("<c:url value='/eventTracker.html'/>");
      });

      Event.observe(window, "unload", function() {
        rememberScrolling();
        if (typeof BS == "undefined") return;
        BS.blockRefreshPermanently();
        BS.EventTracker.dispose();
      });
    </script>
  <jsp:invoke fragment="head_include"/>

  <ext:includeExtensions placeId="<%=PlaceId.ALL_PAGES_HEADER%>"/>
  </head>

  <body class="pageBG">

  <div id="bodyWrapper">
    <c:if test="${not loadingWarningDisabled}">
      <div id="loadingWarning">
        LOADING...
      </div>

      <script type="text/javascript">
        (function() {
          function getLoadingDiv() {
            return document.getElementById('loadingWarning');
          }
          var loadingTimeout = setTimeout(function(){
            var div = getLoadingDiv();
            if (div != null) {
              div.style.display = 'block';
            }
          }, 500);

          $j(document).ready(function() {
            if (loadingTimeout) clearTimeout(loadingTimeout);
            var div = getLoadingDiv();
            if (div != null) {
              BS.Util.hide(div);
            }
          });
        })();
      </script>
    </c:if>

    <div id="topWrapper">
      <div class="headBG clearfix">
        <div class="fixedWidth">

          <a href="<c:url value="/"/>" class="headerLogo"><i class="headerLogoImg"></i></a>

          <div id="tabsContainer"></div>
          <div id="userPanel">
            <c:if test="${not empty currentUser}">
            <div class="info"><p><span id="beforeUserId"></span>
              <c:url value="/profile.html" var="settingsUrl"/>
              <bs:simplePopup controlId="usernamePopup" popup_options="shift: {x: -120, y: 15}, className: 'quickLinksMenuPopup'"
                ><jsp:attribute name="content">
              <ul class="menuList">
                  <authz:authorize allPermissions="CHANGE_OWN_PROFILE">
                    <li>
                      <a href='<c:url value="/investigations.html?init=1"/>' showdiscardchangesmessage='false'>My Investigations</a>
                    </li>
                    <li>
                      <a href='${settingsUrl}' showdiscardchangesmessage='false'>My Settings & Tools</a>
                    </li>
                  </authz:authorize>
                  <c:url value="/ajax.html?logout=1" var="logoutUrl"/>
                    <li>
                      <a class="logout" href="#" onclick="BS.Logout('${logoutUrl}'); return false" showdiscardchangesmessage="false">Logout</a>
                    </li>
                </ul>

                </jsp:attribute
                ><jsp:body
                  ><authz:authorize allPermissions="CHANGE_OWN_PROFILE"
                    ><jsp:attribute name="ifAccessDenied"><c:out value='${currentUser.descriptiveName}'/></jsp:attribute
                    ><jsp:attribute name="ifAccessGranted"><a href='${settingsUrl}'><c:out value='${currentUser.descriptiveName}'/></a></jsp:attribute
                  ></authz:authorize
                ></jsp:body
              ></bs:simplePopup></div>
            <c:if test="${afn:adminSpaceAvailable()}">
              <div class="info admin"><p><a href='<c:url value="/admin/admin.html"/>'>Administration</a></div>
            </c:if>
            </c:if>
          </div>
        </div>
        <div id="header-ready"></div>
      </div>
      <script type="text/javascript">
        if (BS.topNavPane) {
          BS.topNavPane.showIn('tabsContainer');
          jQuery("#tabsContainer li").each(function() {
            var self = jQuery(this);
            if (!self.hasClass("first")) {
              self.addClass("leftBorder")
            }
          });
          BS.AllProjectsPopup.install();
        }
      </script>

      <div class="fixedWidth clearfix">
        <jsp:invoke fragment="toolbar_include"/>
        <ul id="main_navigation"><li/></ul>

        <div class="quickLinks">
          <jsp:invoke fragment="quickLinks_include"/>
        </div>

        <script type="text/javascript">
            BS.Navigation.writeBreadcrumbs();
        </script>
      </div>
    </div>
    <div class="fixedWidth">

      <jsp:include page="<%=PageBeforeContentPagePlaceController.PATH%>"/>

      <jsp:invoke fragment="beforeTabs_include"/>
      <table class="tabsTable">
        <tr>
          <td>
            <div id="tabsContainer3" class="simpleTabs"></div>
          </td>
          <td class="besideContent">
            <jsp:invoke fragment="besideTabs_include"/>
          </td>
        </tr>
      </table>
    </div>

    <div id="archivedProjectsContent" class="popupDiv" style="display:none;">
    </div>
    <div id="buildResultsSummaryTemplate" style="display:none">
      <div class="summaryContainer">
        <span id="detailedSummary:##BUILD_ID##">
          <table cellspacing="0">
          <tr>
            <td>
              <div class="header">Build shortcuts</div>
              <ul class="bsLinks">
                <li>
                  <a href="<c:url value='/viewLog.html?tab=buildLog&buildTypeId=##BUILD_TYPE_ID##&buildId=##BUILD_ID##'/>"
                         title="View log messages">Build log</a>
                  <span class="separator">|</span>
                  <a href="<c:url value='downloadBuildLog.html?buildId=##BUILD_ID##&archived=true'/>" target="_blank"
                          title="Download archived build log">.zip</a>
                </li>
                <li><a href="<c:url value='/viewLog.html?tab=buildParameters&buildTypeId=##BUILD_TYPE_ID##&buildId=##BUILD_ID##'/>"
                       title="View build parameters">Parameters</a></li>
                <li><span id="summaryProgress:##BUILD_ID##" style="font-weight: normal; color:#888;"></span></li>
              </ul>
            </td>
          </tr>
          </table>
        </span>
      </div>
    </div>
    <div id="buildTypeMenuTemplate" style="display:none">
      <div class="summaryContainer">
      <ul class="menuList menuListGrouped">
        <c:url value='/project.html?projectId=##PROJECT_ID####BRANCH##' var="projectUrl"/>
        <li data-self="true">
          <a href="${projectUrl}" title="Project home page">Project Home</a>
        </li>
        <c:url value='/viewType.html?buildTypeId=##BUILD_TYPE_ID####BRANCH##' var="buildTypeUrl"/>
        <li data-self="true">
          <a href="${buildTypeUrl}" title="Build configuration home page">Build Configuration Home</a>
        </li>
        <li data-self="true" class="menuListSeparator"></li>
        <c:url value='/viewType.html?buildTypeId=##BUILD_TYPE_ID##&tab=buildTypeHistoryList##BRANCH##' var="historyUrl"/>
        <li>
          <a href="${historyUrl}" title="Build history for the configuration">History</a>
        </li>
        <c:url value='/viewType.html?buildTypeId=##BUILD_TYPE_ID##&tab=buildTypeChangeLog##BRANCH##' var="changeLogUrl"/>
        <li>
          <a href="${changeLogUrl}" title="View user changes">Change Log</a>
        </li>
        <li class="menuListSeparator"></li>
        <li data-responsibility="true">
          <a href="#" onclick="BS.ResponsibilityDialog.showDialog('##BUILD_TYPE_ID##', '##BUILD_TYPE_NAME##'); return false;"
             title="Assign a user to investigate problems in the configuration">Investigate...</a>
        </li>
        <li data-responsibility="true" class="menuListSeparator"></li>
        <li data-admin="true">
          <admin:editBuildTypeLink buildTypeId="##BUILD_TYPE_ID##" cameFromUrl="${cameFromUrl}"
                                   title="Edit build configuration settings">Edit Settings</admin:editBuildTypeLink>
        </li>
      </ul>
      </div>
    </div>
    <div id="issueDetailsTemplate" style="display:none">
      <span id="detailedSummary:##ISSUE_ID##">
        <forms:progressRing className="progressRingInline"/>
      </span>
    </div>

    <div id="mainContent" class="fixedWidth clearfix">
      <div id="content">
        <bs:messages key="accessDenied"/>
        <jsp:invoke fragment="body_include"/>
      </div>
    </div>

    <jsp:include page="/footer.jsp"/>

  </div>

  <et:subscribeOnEvents>
    <jsp:attribute name="eventNames">
      SERVER_SHUTDOWN
    </jsp:attribute>
    <jsp:attribute name="eventHandler">
      BS.ServerLink.onShutdown();
    </jsp:attribute>
  </et:subscribeOnEvents>

  <bs:modalDialog formId="muteTestsForm"
                  title="Mute test"
                  action="#"
                  saveCommand="BS.BulkInvestigateDialog.submit();"
                  closeCommand="BS.BulkInvestigateDialog.close();">
    <div id="mute-dialog-container"></div>
    <div><forms:saving id="mute-dialog-progress" className="progressRingInline"/></div>
  </bs:modalDialog>

  <bs:modalDialog formId="errorForm"
                  title=""
                  action="#"
                  saveCommand="BS.ErrorDialog.close();"
                  closeCommand="BS.ErrorDialog.close();">
  </bs:modalDialog>

  <bs:dialog dialogId="shutdownDialog"
             title="Server communication failure"
             titleId="shutdownDialogTitle"
             closeCommand="BS.ServerLink.close()">
    <strong id="shutdownDetails">Server is unavailable</strong>
    <p id="onCommunicationFailure">Server stopped or communication with the server is not possible due to network failure.</p>
    <p id="onServerShutdown">Server shutdown started.</p>
  </bs:dialog>

  <c:url var="actionUrl" value="/runCustomBuild.html"/>
  <bs:modalDialog formId="runBuild"
                  title="Run Custom Build"
                  action="${actionUrl}"
                  closeCommand="BS.RunBuildDialog.close()"
                  saveCommand="BS.RunBuildDialog.submit()">
  </bs:modalDialog>

  <bs:stopBuildDialog/>

  <resp:form buildType="${null}" currentUser="${currentUser}" />
  </body>
</html>
