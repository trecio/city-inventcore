<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz" %>
<%@ taglib prefix="ufn" uri="/WEB-INF/functions/user"%>
<%@attribute name="buildChainGraph" type="jetbrains.buildServer.controllers.graph.BuildChainGraph" required="true" %>
<%@attribute name="graphId" type="java.lang.String" required="false" %>
<c:set var="canStartMap" value="${buildChainGraph.canStart}"/>
<c:set var="modificationIdOpt" value="modificationId: ''"/>
<c:if test="${not empty buildChainGraph.buildChain.chainModificationId and buildChainGraph.buildChain.chainModificationId > 0}">
  <c:set var="modificationIdOpt" value="modificationId: ${buildChainGraph.buildChain.chainModificationId}"/>
</c:if>
<bs:layeredGraph layers="${buildChainGraph.layers}" id="${graphId}">
  <jsp:attribute name="nodeContent">
    <c:set var="reified" value="${not empty node.userData.buildPromotion}"/>
    <c:if test="${canStartMap[node.userData.buildType.buildTypeId] != null and node.userData.queuedBuild == null}">
    <c:set var="depOnBranchName" value=""/>
    <c:set var="dependOn" value=""/>
    <c:forEach items="${canStartMap[node.userData.buildType.buildTypeId]}" var="p">
      <c:set var="dependOn" value="${dependOn},${p.id}"/>
      <c:set var="branch" value="${p.branch}"/>
      <c:if test="${fn:length(depOnBranchName) == 0 and not empty branch}"><c:set var="depOnBranchName" value="${branch.name}"/></c:if>
    </c:forEach>
    <c:choose>
      <c:when test="${param['tab'] != 'projectBuildChains'}"><c:url value="/viewType.html?buildTypeId=${node.userData.buildType.buildTypeId}&tab=buildTypeChains" var="redirectTo"/></c:when>
      <c:otherwise><c:set var="redirectTo" value=""/></c:otherwise>
    </c:choose>
      <c:set var="startOpt" value="${modificationIdOpt}, dependOnPromotionIds: '${dependOn}', afterTrigger: function() { $('buildChains').refresh(); }, init: true, stateKey: 'promote' "/>
      <authz:authorize projectId="${node.userData.buildType.projectId}" anyPermission="RUN_BUILD">
      <span style="float:right;" ${not reified ? 'title="Click to continue build chain"' : ''} >
        <c:choose>
          <c:when test="${reified}">
          <c:set var="branch" value="${node.userData.build.branch}"/>
          <c:set var="startOpt">${startOpt}<c:if test="${not empty branch}">, branchName: '<bs:escapeForJs text="${branch.name}"/>'</c:if></c:set>
          <a class="noUnderline" href="#" onclick="Event.stop(event); BS.RunBuild.runCustomBuild('${node.userData.buildType.buildTypeId}', { ${startOpt} }); return false" title="Run build with the same revisions...">
            <img style="vertical-align:middle;" src="<c:url value='/img/runBuildChanges.png'/>">
          </a>
          </c:when>
          <c:otherwise>
            <c:set var="startOpt">${startOpt}<c:if test="${not empty depOnBranchName}">, branchName: '${depOnBranchName}'</c:if></c:set>
            <bs:runBuild buildTypeId="${node.userData.buildType.buildTypeId}" options="${startOpt}" redirectTo="${redirectTo}">Run</bs:runBuild>
          </c:otherwise>
        </c:choose><div class="clr"></div>
      </span>
      </authz:authorize>
    </c:if>
    <c:choose>
      <c:when test="${reified}">
        <c:set var="build" value="${node.userData.build}"/>
        <bs:dependencyState dependency="${node.userData.buildPromotion}"/>
      </c:when>
      <c:otherwise>
        <c:set var="bt" value="${node.userData.buildType}"/>
        <div class="buildConfRun">
          <bs:buildTypeLinkFull buildType="${bt}" popupMode="true"/>
        </div>
        <div>
          <em>not triggered</em>
        </div>
      </c:otherwise>
    </c:choose>
  </jsp:attribute>
</bs:layeredGraph>
<script type="text/javascript">
$('${graphId}').graphObject.drawWithSelected = function() {
  var onGraphDrawn = function() {
    if (this._shown) {
    <c:forEach items="${buildChainGraph.selectedNodes}" var="id">
      var node = $('${graphId}').graphObject._graph.nodes['${graphId}_${id}'];
      if (node.setCurrent) {
        node.setCurrent();
      }
    </c:forEach>
    }
  }.bind(this);

  this.drawGraph(onGraphDrawn);
};
</script>
