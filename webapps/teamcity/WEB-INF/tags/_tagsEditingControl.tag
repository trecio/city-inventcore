<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"
  %><%@ attribute name="allTags" required="true" type="java.util.List"
  %><%@ attribute name="object" required="true" type="java.lang.String"
  %><textarea name="buildTagsInfo" id="buildTagsInfo" rows="3" cols="46" class="commentTextArea"></textarea><br/>
  <c:forEach var="tagName" items="${allTags}">
    <c:set var="escapedTag"><bs:escapeForJs text="${tagName}" forHTMLAttribute="true"/></c:set>
    <a href="#" class="unselectedTag" onclick="${object}.appendTag('${escapedTag}'); return false"><c:out value="${tagName}"/></a>
  </c:forEach>