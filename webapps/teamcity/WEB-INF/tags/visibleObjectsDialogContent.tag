<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms"%><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags"%><%@

    attribute name="showReorderButtons" type="java.lang.Boolean" required="true"  %><%@
    attribute name="object" type="java.lang.String" required="true"  %><%@
    attribute name="jsDialog" type="java.lang.String" required="true"  %><%@
    attribute name="visibleTitle" type="java.lang.String" required="true"  %><%@
    attribute name="hiddenTitle" type="java.lang.String" required="true"  %><%@
    attribute name="filterText" type="java.lang.String" required="true"  %><%@
    attribute name="visibleOptions" fragment="true" required="true"  %><%@
    attribute name="hiddenOptions" fragment="true" required="true"

%><table style="width: 49em">
  <tr>
    <c:if test="${showReorderButtons}">
      <td class="arrowButtons">
        <button type="button" class="orderButton" id="${object}_moveVisibleUp" onclick="${jsDialog}.moveUp();">
          <img class="arrowImg" src="<c:url value='/img/visibleProjects/up.gif'/>" alt="move up">
        </button>
        <br>
        <button type="button" class="orderButton" id="${object}_moveVisibleDown" onclick="${jsDialog}.moveDown();">
          <img class="arrowImg" src="<c:url value='/img/visibleProjects/down.gif'/>" alt="move down">
        </button>
      </td>
    </c:if>
    <td class="visibleProjects">
      ${visibleTitle}:
      <br>
      <select name="${object}_visible" id="${object}_visible" multiple="multiple" onchange="${jsDialog}.updateButtons();" style="width:265px; height: 190px;" ondblclick="${jsDialog}.moveToHidden();">
        <jsp:invoke fragment="visibleOptions"/>
      </select>
    </td>
    <td class="arrowButtons">
      <button type="button" id="${object}_moveToHidden" onclick="${jsDialog}.moveToHidden();">
        <img class="arrowImg" src="<c:url value='/img/visibleProjects/right.gif'/>" alt="move to hidden">
      </button>
      <br>
      <button type="button" id="${object}_moveToVisible" onclick="${jsDialog}.moveToVisible();">
        <img class="arrowImg" src="<c:url value='/img/visibleProjects/left.gif'/>" alt="move to visible">
      </button>
    </td>
    <td class="hiddenProjects">
      ${hiddenTitle}:
      <br>
      <bs:inplaceFilter containerId="${object}_hidden" activate="true" filterText="&lt;${filterText}&gt;" style="width:265px;"/>
      <select id="${object}_hidden" multiple="multiple" style="width:265px; height: 163px;" onchange="${jsDialog}.updateButtons();" ondblclick="${jsDialog}.moveToVisible();">
        <jsp:invoke fragment="hiddenOptions"/>
      </select>
    </td>
  </tr>
</table>