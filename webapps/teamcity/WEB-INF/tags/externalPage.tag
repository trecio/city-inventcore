<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ attribute name="head_include" fragment="true"
  %><%@ attribute name="body_include" fragment="true"
  %><%@ attribute name="page_title" fragment="true"
  %><%@ tag body-content="empty"
  %><!DOCTYPE html>
<html>
<c:set var="pageTitle"><jsp:invoke fragment="page_title"/> -- TeamCity</c:set>
<head>
  <title>${pageTitle}</title>
  <meta http-equiv="X-UA-Compatible" content="IE=9, chrome=1" >
  <bs:pageMeta title="${pageTitle}"/>

  <bs:linkCSS>
    /css/main.css
  </bs:linkCSS>

  <bs:ua/>
  <bs:baseUri/>

  <bs:linkScript>
    /js/jquery/jquery-1.7.2.min.js
  </bs:linkScript>

  <script type="text/javascript">
    window.$j = jQuery.noConflict();
  </script>

  <bs:linkScript>
    /js/prototype.js
    /js/aculo/effects.js

    /js/behaviour.js
    /js/underscore-min.js

    /js/bs/bs.js
    /js/bs/cookie.js
    /js/bs/position.js
    /js/bs/basePopup.js
    /js/bs/menuList.js
  </bs:linkScript>

  <jsp:invoke fragment="head_include"/>

</head>

<body>

<jsp:invoke fragment="body_include"/>

</body>
</html>