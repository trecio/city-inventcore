<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ attribute name="myChanges" required="true" type="java.lang.Boolean"
    %><%@ attribute name="noTitle" required="false" type="java.lang.Boolean"
    %><c:set var="iconTitle"><c:if test="${not noTitle}">Remote Run</c:if></c:set><c:choose
  ><c:when test="${myChanges}"><bs:icon icon="personal/my_personal.gif" alt="${iconTitle}"/></c:when
  ><c:otherwise><bs:icon icon="personal/personal.gif" alt="${iconTitle}"/></c:otherwise
  ></c:choose>