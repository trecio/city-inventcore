<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="util" uri="/WEB-INF/functions/util" %><%@
    attribute name="containerId" required="true" %><%@
    attribute name="pageUrl" required="true" %><%@
    attribute name="useJsp" required="false" %><%@
    attribute name="deferred" required="false" type="java.lang.Boolean"

%><div id="${containerId}" class="refreshable">
  <div id="${containerId}Inner" class="refreshableInner"><jsp:doBody/></div>
</div>
<c:choose>
  <c:when test="${deferred}">
    <script type="text/javascript">
      BS.Util.runWithElement('${containerId}', function() {
        $('${containerId}').refresh = BS.Refreshable.createRefreshFunction('${containerId}', '${util:escapeUrlForQuotes(pageUrl)}', '${useJsp}');
      });
    </script>
  </c:when>
  <c:otherwise>
    <script type="text/javascript">
      $('${containerId}').refresh = BS.Refreshable.createRefreshFunction('${containerId}', '${util:escapeUrlForQuotes(pageUrl)}', '${useJsp}');
    </script>
  </c:otherwise>
</c:choose>
