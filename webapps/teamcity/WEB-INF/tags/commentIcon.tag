<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ attribute name="text"
  %><%@ attribute name="style"
  %><c:if test="${not empty text}"
  ><c:url var="iconUrl" value="/img/commentIcon.gif"
  /><% String id = "commentHover_" + text.hashCode();
  %><img class="commentIcon" src="${serverPath}${iconUrl}" <c:if test="${not empty style}">style="${style}" </c:if>width="11" height="11" <bs:tooltipAttrs containerId="<%=id%>" withOnClick="true"/> /> <div id="<%=id%>" class="hidden">${text}</div></c:if>