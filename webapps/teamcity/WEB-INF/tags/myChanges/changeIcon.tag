<%@ tag import="jetbrains.buildServer.web.util.SessionUser" %>
<%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags"%><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    attribute name="changeStatus" required="true" type="jetbrains.buildServer.vcs.ChangeStatus"
    %><c:set var="myBuild" value="<%=changeStatus.getChange().isPersonal() && changeStatus.getChange().getCommitters().contains(SessionUser.getUser(request))%>"
    /><c:set var="my" value="${changeStatus.change.personal && myBuild ? 'personal/my_' : 'personal/'}"/><c:choose
  ><c:when test="${changeStatus.change.personal}"
    ><c:choose
    ><c:when test="${changeStatus.canceledPersonal}"
    ><bs:icon icon="${my}personalCancelled.gif" alt="Personal build canceled"
    /></c:when
    ><c:when test="${changeStatus.failedCount > 0 && changeStatus.runningBuildsNumber == 0}"
    ><bs:icon icon="${my}personalFinishedFailed.gif" alt="Personal build failed"
    /></c:when
    ><c:when test="${changeStatus.failedCount > 0 && changeStatus.runningBuildsNumber > 0}"
    ><bs:icon icon="${my}personalRunningFailing.gif" alt="Personal build is failing"
    /></c:when
    ><c:when test="${changeStatus.successful && changeStatus.runningBuildsNumber == 0}"
    ><bs:icon icon="${my}personalFinished.gif" alt="Personal build was successful"
    /></c:when
    ><c:when test="${changeStatus.runningBuildsNumber > 0}"
    ><bs:icon icon="${my}personalRunning.gif" alt="Personal build is running"
    /></c:when
    ><c:otherwise
    ><bs:icon icon="${my}personalPending.gif" alt="Personal build is waiting in the queue"
    /></c:otherwise
    ></c:choose
    ></c:when
  ><c:otherwise
    ><c:choose
      ><c:when test="${changeStatus.failedCount > 0 && changeStatus.runningBuildsNumber == 0}"
        ><bs:icon icon="error_small.gif" alt="Build with this change failed"
       /></c:when
      ><c:when test="${changeStatus.failedCount > 0 && changeStatus.runningBuildsNumber > 0}"
        ><bs:icon icon="running_red_transparent.gif" alt="Build with this change is failing"
       /></c:when
      ><c:when test="${changeStatus.successful && changeStatus.runningBuildsNumber == 0}"
        ><bs:icon icon="success_small.gif" alt="All builds are successful"
       /></c:when
      ><c:when test="${changeStatus.failedCount == 0 && changeStatus.runningBuildsNumber > 0}"
        ><bs:icon icon="running_green_transparent.gif" alt="Build with this change is running"
       /></c:when
      ><c:when test="${changeStatus.pendingBuildsTypesNumber > 0}"
        ><bs:icon icon="pending.gif" alt="Waiting in the queue"
       /></c:when
    ></c:choose
  ></c:otherwise
></c:choose>