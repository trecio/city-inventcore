<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="afn" uri="/WEB-INF/functions/authz"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz"
  %><%@ attribute name="build" fragment="false" required="true" type="jetbrains.buildServer.serverSide.SRunningBuild"
  %><%@ attribute name="ifAccessGranted" fragment="true"
  %><%@ attribute name="ifAccessDenied" fragment="true" %>
<%--@elvariable id="currentUser" type="jetbrains.buildServer.users.User"--%>
<authz:authorize projectId="${build.projectId}" allPermissions="CANCEL_BUILD">
  <jsp:attribute name="ifAccessGranted">
    <c:choose>
      <c:when test="${afn:permissionGrantedForBuild(build, 'CANCEL_ANY_PERSONAL_BUILD') or not build.personal or (build.owner != null && build.owner.id == currentUser.id)}"><jsp:invoke fragment="ifAccessGranted"/></c:when>
      <c:otherwise><jsp:invoke fragment="ifAccessDenied"/></c:otherwise>
    </c:choose>
  </jsp:attribute>
  <jsp:attribute name="ifAccessDenied"><jsp:invoke fragment="ifAccessDenied"/></jsp:attribute>
</authz:authorize>