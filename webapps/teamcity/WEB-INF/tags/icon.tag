<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ attribute name="icon" fragment="false"
  %><%@ attribute name="alt" fragment="false"
  %><%@ attribute name="imgId" fragment="false"
  %><%@ attribute name="addClass" fragment="false"
  %><c:url var="iconUrl" value="/img/buildStates/${icon}"
  /><c:if test="${not empty imgId}" ><c:set var="idAttr" value="id='${imgId}'"/></c:if><img
  src="${serverPath}${iconUrl}" class="icon ${addClass}" <c:if test="${not empty alt}">alt="${alt}" title="${alt}" </c:if>${idAttr}/>