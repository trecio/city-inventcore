<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %><%@
    taglib prefix="string" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    attribute name="id" required="true" type="java.lang.String" %><%@
    attribute name="dialogId" required="true" type="java.lang.String" %><%@
    attribute name="dialogTitle" required="true" type="java.lang.String" %><%@
    attribute name="pageUrl" required="true" type="java.lang.String" %><%@
    attribute name="bean" required="true" type="java.lang.Object" %><%@
    attribute name="actionPath" required="true" type="java.lang.String" %><%@
    attribute name="homePath" required="true" type="java.lang.String" %><%@
    attribute name="jsBase" required="true" type="java.lang.String" %><%@
    attribute name="belowFileName" fragment="true" required="false" %><%@
    attribute name="headMessage" fragment="true" required="false" %><%@
    attribute name="headMessageNoFiles" fragment="true" required="false" %>

<c:url var="actionUrl" value="${actionPath}"/>

<bs:refreshable containerId="${id}" pageUrl="${pageUrl}">
<div class="fileBrowse">
  <c:choose>
    <c:when test="${bean.showFile}">
      <c:url var="homeUrl" value="${homePath}"/>
      <div class="headMessage">
        <span class="fileName">
          <c:out value="${bean.fileName}"/> <span class="fileSize">(${bean.fileSize})</span>
        </span>
        <span class="fileOperations">
          <a href="${homeUrl}">&laquo; All files</a>
          <span class="separator">|</span>
          <c:if test="${bean.deleteSupported}">
            <a href="#" onclick="return ${jsBase}.deleteFile('${actionUrl}', '${homeUrl}', '${bean.messageOnDelete}', '<c:out value="${bean.fileName}"/>');"
               class="delete" title='Remove <c:out value="${bean.fileName}"/>'>Delete file</a>
            <span class="separator">|</span>
          </c:if>
          <c:url var="linkForFile" value="${bean.linkForFile}"/>
          <c:set var="ch" value="${string:contains(linkForFile, '?') ? '&' : '?'}"/>
          <a href="${linkForFile}${ch}forceInline=true">View in browser</a>
          <span class="separator">|</span>
          <a class="downloadLink" href="${linkForFile}${ch}forceAttachment=true">Download file</a>
        </span>
        <div><jsp:invoke fragment="belowFileName"/></div>
      </div>
      <div>
        <pre class="fileContent"><c:out value="${bean.fileContent}"/></pre>
      </div>
    </c:when>
    <c:when test="${bean.hasFiles}">
      <div class="headMessage">
        <jsp:invoke fragment="headMessage"/>
        <forms:saving id="refreshing"/>
        <div class="downloadLink">
          <a class="downloadLink" href="<c:url value="${bean.linkForZip}" />">Download all (.zip)</a>
        </div>
      </div>
      <div id="tree"></div>
      <c:if test="${bean.showTotalSize}">
        <div class="filesize" style="margin-top: 1em;">
          Total size: ${bean.totalSize}
          <c:if test="${not bean.showEmptyFiles}">
            &nbsp;&nbsp;
            <em style="color: #888;">(zero-length files aren't shown)</em>
          </c:if>
        </div>
      </c:if>
      <script type="text/javascript">
        BS.LazyTree.treeUrl = "${actionUrl}";
        BS.LazyTree.loadTree("tree");
      </script>
    </c:when>
    <c:otherwise>
      <div class="headMessage">
        <jsp:invoke fragment="headMessageNoFiles"/>
        <forms:saving id="refreshing"/>
      </div>
    </c:otherwise>
  </c:choose>

  <c:if test="${not bean.showFile and bean.uploadSupported}">
    <div class="upload">
      <forms:addButton onclick="return ${jsBase}.show();">Upload new file</forms:addButton>
    </div>

    <bs:dialog dialogId="${dialogId}"
               dialogClass="uploadDialog"
               title="${dialogTitle}"
               titleId="${dialogId}Title"
               closeCommand="${jsBase}.close();">
      <forms:multipartForm id="${dialogId}Form" action="${actionUrl}"
                           onsubmit="return ${jsBase}.validate();"
                           targetIframe="hidden-iframe">
        <div>
          <table>
            <tr>
              <th>Name</th>
              <td><input type="text" id="fileName" name="fileName" value="${bean.initUploadFileName}"/></td>
            </tr>
            <tr>
              <th>File</th>
              <td><forms:file name="fileToUpload" size="28"/></td>
            </tr>
          </table>
          <div id="uploadError" style="display: none;"></div>
        </div>
        <div class="popupSaveButtonsBlock">
          <forms:cancel onclick="${jsBase}.close()" showdiscardchangesmessage="false"/>
          <forms:submit label="Save"/>
          <forms:saving id="saving"/>
        </div>
      </forms:multipartForm>
    </bs:dialog>

    <script type="text/javascript">
      ${jsBase}.setFiles([<c:forEach var="file" items="${bean.files}">'${file}',</c:forEach>]);
      ${jsBase}.prepareFileUpload();
    </script>
  </c:if>
</div>
</bs:refreshable>
