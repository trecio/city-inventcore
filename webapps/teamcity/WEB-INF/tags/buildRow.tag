<%@ tag import="jetbrains.buildServer.serverSide.SBuild" %><%@
    tag import="jetbrains.buildServer.serverSide.SRunningBuild" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %><%@
    taglib prefix="t" tagdir="/WEB-INF/tags/tags" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    attribute name="build" required="true" rtexprvalue="true" type="jetbrains.buildServer.serverSide.SBuild" %><%@
    attribute name="rowClass" required="false" %><%@
    attribute name="showBranchName" type="java.lang.Boolean" required="false" %><%@
    attribute name="noLinkBranchForBranchName" type="java.lang.Boolean" required="false" %><%@
    attribute name="maxBranchNameLength" type="java.lang.Integer" required="false" %><%@
    attribute name="showBuildNumber" type="java.lang.Boolean" required="false" %><%@
    attribute name="showStatus" type="java.lang.Boolean" required="false" %><%@
    attribute name="showArtifacts" type="java.lang.Boolean" required="false" %><%@
    attribute name="showCompactArtifacts" type="java.lang.Boolean" required="false" %><%@
    attribute name="showChanges" type="java.lang.Boolean" required="false" %><%@
    attribute name="showStartDate" type="java.lang.Boolean" required="false" %><%@
    attribute name="showProgress" type="java.lang.Boolean" required="false" %><%@
    attribute name="showDuration" type="java.lang.Boolean" required="false" %><%@
    attribute name="showStop" type="java.lang.Boolean" required="false" %><%@
    attribute name="showAgent" type="java.lang.Boolean" required="false" %><%@
    attribute name="showTags" type="java.lang.Boolean" required="false" %><%@
    attribute name="showUsedByOtherBuildsIcon" type="java.lang.Boolean" required="false" %><%@
    attribute name="showPin" type="java.lang.Boolean" required="false" %><%@
    attribute name="outOfChangesSequence" type="java.lang.Boolean" required="false"
%><%
  boolean outdated = build instanceof SRunningBuild && ((SRunningBuild)build).isOutdated();
  if (outdated) {
    SBuild prevBuild = build.getPreviousFinished();
    if (prevBuild != null) {
      request.setAttribute("prevBuild", prevBuild);
    }
  }
  request.setAttribute("outdated", outdated);
%><bs:trimWhitespace

  ><c:set var="buildId" value="${build.buildId}"
  /><c:set var="promo" value="${build.buildPromotion}"
  /><c:set var="stopMessage" value=""
  /><c:set var="tooltipMessage" value=""
  /><c:set var="buildHangs" value=""
  /><c:set var="tooltipBuild" value="${build}"

  /><c:if test="${not build.finished}"
    ><c:set var="running" value="${build}"/><%--@elvariable id="running" type="jetbrains.buildServer.serverSide.SRunningBuild"--%>
    <c:choose
      ><c:when test="${not build.agent.registered}"
        ><c:set var="tooltipMessage">Build agent ${build.agentName} was disconnected while running a build.<br/>
          Please check the agent and network connectivity. Either ensure the agent is connected or stop the build.<br/>
          Last message was received on:
          <bs:date value="${running.lastBuildActivityTimestamp}"/> (<bs:printTime time="${running.timeSpentSinceLastBuildActivity}"/> ago)
        </c:set
        ><c:set var="stopMessage" value="Agent does not respond."
      /></c:when
      ><c:when test="${running.probablyHanging}"
        ><c:set var="buildHangs" value="true"
       /><c:set var="tooltipMessage">This build is probably hanging. <br/>
          Last message was received on: <bs:date value="${running.lastBuildActivityTimestamp}"/>
          (<bs:printTime time="${running.timeSpentSinceLastBuildActivity}"/> ago)
        </c:set
        ><c:set var="stopMessage" value="This build hung."
      /></c:when><c:when test="${outdated}"
        ><c:set var="tooltipBuild" value="${prevBuild}"
       /><c:set var="tooltipMessage">This build is outdated (more recent build is already finished).
          Click the icon to check the newer results.<c:if test="${prevBuild != null}"> (<c:out value="${tooltipBuild.buildNumber}"/>)</c:if>
        </c:set
        ><c:set var="stopMessage" value="This build is outdated."
      /></c:when
    ></c:choose
  ></c:if

  ><c:if test="${not empty tooltipMessage}"
      ><c:set var="rowClass" value="${rowClass} obsoleteRunningBuild"
  /></c:if
  ><c:if test="${build.outOfChangesSequence or promo.changesDetached}"
      ><c:set var="rowClass" value="${rowClass} detachedChanges"
  /></c:if

  ><c:if test="${showBranchName}"
    ><c:set var="branch" value="${build.branch}"
    /><c:set var="length" value="${empty maxBranchNameLength ? 12 : maxBranchNameLength}"
    /><c:set var="doShowBranch" value="${not empty branch}"
    /><c:set var="clazz" value="${branch.defaultBranch ? 'default' : ''}${doShowBranch ? ' hasBranch' : ''}"/>
    <td class="${rowClass} ${clazz} branch">
      <c:if test="${doShowBranch}"
        ><c:set var="trimmedBranch"><bs:trimBranch branch="${branch}" defaultMaxLength="${length}"/></c:set
        ><bs:branchLink branch="${branch}" branchHolder="${build}" noLink="${noLinkBranchForBranchName}">${trimmedBranch}</bs:branchLink
      ></c:if>
    </td>
  </c:if

  ><c:if test="${showBuildNumber}">
    <td class="${rowClass} buildNumber">
      <span class="buildNumberInner">
        <span><nobr><bs:buildNumber buildData="${build}"/> <bs:buildCommentIcon build="${build}"/></nobr></span>
        <c:if test="${not empty tooltipMessage}"
          ><bs:resultsLink build="${tooltipBuild}" noPopup="true" noTitle="true"
          ><c:set var="image" value="/img/attentionComment.gif"
          /><c:if test="${not empty buildHangs}"
            ><c:set var="image" value="/img/attentionCommentRed.gif"
          /></c:if
          ><img class="icon" src="<c:url value='${image}'/>" alt="" <bs:tooltipAttrs text="${tooltipMessage}"/> /></bs:resultsLink
        ></c:if>
      </span>
    </td>
  </c:if

  ><c:if test="${showStatus}">
    <td class="${rowClass} status">
      <bs:buildDataIcon buildData="${build}"
                        imgId="build:${buildId}:img"/>
      <bs:resultsLink build="${build}"
                      skipChangesArtifacts="true"
          ><span id="build:${buildId}:text"><bs:trim maxlength="100">${build.statusDescriptor.text}</bs:trim></span
          ></bs:resultsLink>
      <c:if test="${outOfChangesSequence}">
        <span class="outofseq">(History build)</span>
      </c:if>
    </td>
  </c:if

  ><c:if test="${showArtifacts}"
    ><c:set var="_id" value="build:${buildId}"
    /><c:set var="artifactsLink"
        ><bs:artifactsLink build="${build}"
                          showIcon="${showCompactArtifacts}"
                          compactView="${showCompactArtifacts}"/></c:set
    ><c:set var="noArtifacts"><span id="${_id}:noArtifactsText" class="commentText">${artifactsLink}</span></c:set>
    <td class="${rowClass} link artifactsLink">
      <c:choose
        ><c:when test="${build.artifactsExists}"><span id="${_id}:artifactsLink">${artifactsLink}</span></c:when
        ><c:when test="${build.finished and not build.artifactsExists}">${noArtifacts}</c:when
        ><c:otherwise><span id="${_id}:artifactsLink" style="display:none">${artifactsLink}</span>${noArtifacts}</c:otherwise
      ></c:choose
    ></td>
  </c:if

  ><c:if test="${showChanges}">
    <td class="${rowClass} link changesLink"><bs:changesLinkFull buildPromotion="${promo}" noUsername="false"/></td>
  </c:if

  ><c:if test="${showStartDate}">
    <td class="${rowClass} startDate"><bs:date value="${build.startDate}"/></td>
  </c:if

  ><c:if test="${showDuration}">
    <td class="${rowClass} duration"><bs:printTime time="${build.duration}" showIfNotPositiveTime="&lt; 1s"/></td>
  </c:if

  ><c:if test="${showProgress}">
    <td class="${rowClass} duration"><bs:buildProgress buildData="${build}"/></td>
  </c:if

  ><c:if test="${showStop}">
    <td class="${rowClass} stopBuild">
      <c:if test="${not build.finished}"><bs:stopBuild build="${build}" message="${stopMessage}"/></c:if>
    </td>
  </c:if

  ><c:if test="${showAgent}">
    <td class="${rowClass} nowrap"><bs:agentDetailsLink agent="${build.agent}"/></td>
  </c:if

  ><c:if test="${showTags}">
    <td class="${rowClass}">
      <t:tagsInfo build="${build}"/>
    </td>
  </c:if

  ><c:if test="${showUsedByOtherBuildsIcon}">
    <td class="${rowClass}">
      <c:if test="${build.usedByOtherBuilds}"
        ><bs:_viewLog build="${build}"
                      tab="dependencies"
                      title="This build is used by other builds"
            ><img src="<c:url value="/img/link.png"/>" alt="" class="pinImg" title="This build is used by other builds"
        /></bs:_viewLog>
      </c:if>&nbsp;
    </td>
  </c:if

  ><c:if test="${showPin}">
    <td class="${rowClass} pin nowrap" id="pinTd${buildId}" <bs:_pinTdAttrs buildId="${buildId}" pin="${build.pinned}"/> >
      <authz:authorize projectId="${build.projectId}" allPermissions="PIN_UNPIN_BUILD">
        <jsp:attribute name="ifAccessGranted">
          <forms:saving id="progressIcon${buildId}"/>
          <c:if test="${build.pinned}">
            <bs:pinImg build="${build}"/>&nbsp;<bs:pinLink build="${build}"
                                                           pin="false"
                                                           onBuildPage="false"
                                                           className="unpinLink">Unpin</bs:pinLink>
          </c:if>
          <c:if test="${not build.pinned}">
            <bs:pinImg build="${build}" style="display:none;"
            /><bs:pinLink build="${build}"
                          pin="true"
                          onBuildPage="false"><img class="pinLinkImage" src="<c:url value="/img/pin.png"/>" alt=""/></bs:pinLink>
          </c:if>
        </jsp:attribute>
        <jsp:attribute name="ifAccessDenied">
          <%-- The fix for incorrect rendering in IE: if "td" is empty, no border is displayed. --%>
          <span class="hidden">.</span>
          <c:if test="${build.pinned}">
            <bs:pinImg build="${build}"/>
          </c:if>
        </jsp:attribute>
      </authz:authorize>
    </td>
  </c:if
></bs:trimWhitespace>
