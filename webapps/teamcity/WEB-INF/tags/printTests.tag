<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"
  %><%@ taglib prefix="tt" tagdir="/WEB-INF/tags/tests"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"
  %><%@ attribute name="tests" fragment="false" required="true" type="java.util.List"
  %><%@ attribute name="maxTests2Show" fragment="false" required="true"
  %><%@ attribute name="buildData" fragment="false" required="true" type="jetbrains.buildServer.serverSide.SBuild"
  %>
<div id="failedTestsDl">

  <tt:_testGroupForBuild tests="${tests}" maxTests2Show="${maxTests2Show}"
                         buildData="${buildData}" showMuteFromTestRun="false" id="build_fail"/>

</div>

<script type="text/javascript">
  // We don't want this function to run on each AJAX refresh call, and we don't want to expand stacktrace
  // on each refresh for a running build.
  //
  // That's why we use syntax $(document).bind("ready", handler);
  //
  $j(document).bind("ready", function() {
    var index = document.location.href.indexOf('#testNameId');
    if (index > 0) {
      var id = document.location.href.substring(index + 11);
      var element = $('testNameId' + id);
      if (element) {

        $j(element.down('a.testWithDetails')).click();

        var parent_table = element.up("table");
        if (parent_table) {
          var parent_group = parent_table.previous("div.group-name");
          if (parent_group && parent_group._simple_block) {
            if (!parent_group._simple_block.isExpanded()) {
              parent_group._simple_block.changeState('', true, true);
            }
          }
        }
      }
      else {
        if ( $$("div.attentionComment").length > 0 ) {
          var fullLink = document.location.href.replace("maxFailed", "maxFailedOld");
          fullLink = fullLink.replace("#testNameId", "&maxFailed=-1#testNameId");
          $$("div.attentionComment")[0].update("Cannot find the test failure, <a href='" + fullLink + "'>try loading this page</a> with all failed tests shown");
        }
      }
    }
  });
</script>
