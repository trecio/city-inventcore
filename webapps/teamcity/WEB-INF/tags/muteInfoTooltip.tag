<%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@
    attribute name="test" type="jetbrains.buildServer.serverSide.TestEx" required="false" %><%@
    attribute name="testRun" type="jetbrains.buildServer.serverSide.TestRunEx" required="false" %><%@
    attribute name="noCurrentMuteInfo" type="java.lang.Boolean" required="false" %><%@
    attribute name="noTestRunMuteInfo" type="java.lang.Boolean" required="false" %><%@
    attribute name="noActions" type="java.lang.Boolean" required="false" %><%@
    attribute name="onlyTestRunMuteInfo" type="java.lang.Boolean" required="false"

%><div class="name-value"><c:if test="${not noTestRunMuteInfo}"
  ><c:choose
    ><c:when test="${noCurrentMuteInfo}"
      ><bs:_muteInfoTable description="" muteInfo="${testRun.muteInfo}"
                          showCurrentMuteInfo="false" onlyTestRunMuteInfo="${onlyTestRunMuteInfo}"
    /></c:when
  ><c:otherwise
    ><table class="mutePopup"><tr><td>This test failure was muted</td></tr></table></c:otherwise
  ></c:choose
></c:if
><c:if test="${not noCurrentMuteInfo}"
  ><c:set var="currentMuteInfo" value="${test.currentMuteInfo}"
  /><c:set var="projectMuteInfo" value="${currentMuteInfo.projectMuteInfo}"
  /><c:if test="${not empty projectMuteInfo}"
      ><c:set var="description"><bs:projectLink project="${test.project}"/></c:set
      ><bs:_muteInfoTable description="${description}" muteInfo="${projectMuteInfo}" showCurrentMuteInfo="true"
  /></c:if
  ><c:if test="${not empty currentMuteInfo.buildTypeMuteInfo}"
    ><c:forEach items="${currentMuteInfo.muteInfoGroups}" var="entry"
      ><c:set var="btMuteInfo" value="${entry.key}"
      /><c:set var="description"></c:set
      ><c:forEach items="${entry.value}" var="bt" varStatus="status"
        ><c:set var="description"
          >${description} <bs:buildTypeLink buildType="${bt}"/><c:if test="${not status.last}">, </c:if></c:set
      ></c:forEach
      ><bs:_muteInfoTable description="${description}" muteInfo="${btMuteInfo}" showCurrentMuteInfo="true"
    /></c:forEach
  ></c:if
></c:if
></div><c:if test="${not noActions}"
  ><authz:authorize projectId="${test.projectId}" anyPermission="ASSIGN_INVESTIGATION,MANAGE_BUILD_PROBLEMS"
    ><jsp:attribute name="ifAccessGranted">
      <div class="actions">
        <tt:testInvestigationLinks test="${test}"
                                   buildId="${not empty testRun.buildOrNull ? testRun.build.buildId : ''}"/>
      </div>
    </jsp:attribute
    ><jsp:attribute name="ifAccessDenied"
      ><div class="actions">No permissions to modify mute state</div>
    </jsp:attribute
  ></authz:authorize
></c:if>