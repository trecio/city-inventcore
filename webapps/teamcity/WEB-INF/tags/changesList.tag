<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"%>
<%@ attribute name="changeLog" required="true" type="jetbrains.buildServer.controllers.buildType.tabs.ChangeLogBean" %>
<%@ attribute name="url" required="true" type="java.lang.String" %>
<%@ attribute name="filterUpdateUrl" required="true" type="java.lang.String" %>
<%@ attribute name="projectId" required="true" type="java.lang.String" %>
<%@ attribute name="hideBuildSelectors" required="false"%>
<%@ attribute name="hideShowBuilds" required="false"%>
<%@ attribute name="enableCollapsibleChanges" required="false"%>
<%@ attribute name="showBuildTypeLink" required="false"%>

<%--@elvariable id="branchBean" type="jetbrains.buildServer.controllers.BranchBean"--%>
<c:if test="${not empty branchBean}">
  <c:set var="url" value="${url}&branch_${projectId}=${branchBean.userBranchEscaped}"/>
</c:if>
<c:url var="urlInternal" value="${url}"/>
<c:url var="filterUpdateUrlInternal" value="${filterUpdateUrl}"/>

<bs:linkCSS dynamic="${true}">/css/changeLog.css</bs:linkCSS>

<c:if test="${hideShowBuilds}">
  <c:set target="${changeLog.filter}" property="showBuilds" value="false"/>
</c:if>

<div id="changeLog" class="logTable">
  <c:set var="changeLogPager" value="${changeLog.pager}"/>
  <c:set var="pagerUrlPattern" value="${urlInternal}&page=[page]"/>

  <form action="${filterUpdateUrlInternal}" method="post" id="changeLogFilter" class="changeLogFilter" onsubmit="return BS.ChangeLog.submitFilter();">
    <c:choose>
      <c:when test="${changeLog.hasChanges}">
        <div class="actionBar">
          <span class="farRight">
            <bs:recordsPerPageSelect pager="${changeLog.pager}" onchange="BS.ChangeLog.submitFilter();"/>
          </span>

          <c:if test="${not hideBuildSelectors}">
            <span class="nowrap">
              <label class="firstLabel" for="from">Show changes from:</label>
              <forms:textField style="width: 7.5em;" name="from" className="actionInput" value="${changeLog.from}" defaultText="<build #>"/>
            </span>

            <span class="nowrap">
              <label for="to">to:</label>
              <forms:textField style="width: 7.5em;" name="to" className="actionInput" value="${changeLog.to}" defaultText="<build #>"/>
            </span>
          </c:if>

          <c:choose>
            <c:when test="${hideBuildSelectors}">
              <c:set var="userFilterLabel">Show changes by</c:set>
              <c:set var="userFilterLabelClass">firstLabel</c:set>
            </c:when>
            <c:otherwise>
              <c:set var="userFilterLabel">by</c:set>
            </c:otherwise>
          </c:choose>
          <span class="nowrap">
            <label for="userDropDown" class="${userFilterLabelClass}"><c:out value="${userFilterLabel}"/>:</label>
            <bs:changeLogUserFilter changeLogBean="${changeLog}"/>
          </span>

          <wbr/>

          <span class="nowrap">
            <label for="path">containing path:</label>
            <forms:textField style="width: 10em;" name="path" className="actionInput"
                             value="${changeLog.path}" defaultText=""/>
          </span>

          <input class="btn btn_mini" type="submit" value="Filter"/>
          <forms:saving className="progressRingInline"/>
        </div>
      </c:when>
      <c:otherwise>
        <div class="logTableEmpty">No changes found</div>
      </c:otherwise>
    </c:choose>

    <bs:refreshable containerId="changeLogTable" pageUrl="${urlInternal}">
      <c:if test="${not changeLog.fromPositionFound}"><div class="searchDescription">Build with build number <strong><c:out value='${changeLog.from}'/></strong>
        does not exist, showing changes from the first detected change in the changes history.</div>
      </c:if>
      <c:if test="${not changeLog.toPositionFound}">
        <div class="searchDescription">Build with build number <strong><c:out value='${changeLog.to}'/></strong>
          does not exist, showing changes till the most recent detected change.</div>
      </c:if>
      <p class="resultsTitle">
        <c:if test="${changeLog.hasChanges}">
          <span class="changeLogPermalink">
          <bs:changeLogLink baseUrl="${urlInternal}" from="${changeLog.from}" to="${changeLog.to}"
                            userId="${changeLog.userId}" path="${changeLog.path}" changesLimit="${changeLog.filter.changesLimit}"
                            showBuilds="${changeLog.showBuilds}">Permalink</bs:changeLogLink>
          </span>
          <span class="changeLogToggle">
            <forms:checkbox name="showGraph" checked="${changeLog.showGraph}" onclick="BS.ChangeLog.toggleGraph()" disabled="${not empty changeLog.path}"/>
            <label for="showGraph">Show graph</label>
          </span>
          <c:if test="${not hideShowBuilds}">
            <span class="changeLogToggle">
              <bs:changeLogShowBuildsCheckBox changeLogBean="${changeLog}"/>
            </span>
          </c:if>
          <span class="changeLogToggle">
            <forms:checkbox name="showFiles" checked="${changeLog.showFiles}" onclick="BS.ChangeLog.submitFilter(this)"/>
            <label for="showFiles">Show files</label>
          </span>
        </c:if>
      </p>

      <c:if test="${fn:length(changeLog.visibleRows) > 0 and changeLogPager.totalRecords > 0}">
        <c:set var="pagerDesc">
          <span id="changes-num-found">${changeLog.numberOfChanges}</span> change<bs:s val="${changeLog.numberOfChanges}"
            /><c:if test="${changeLog.notAllResultsShown}">
          in first <span class="changes-total">${(changeLog.filter.changesLimit) * changeLog.bigPageSize}</span>
        </c:if>
        </c:set>

        <bs:pager place="title" urlPattern="${pagerUrlPattern}" pager="${changeLogPager}" pagerDesc="${pagerDesc}"/>
      </c:if>

      <c:if test="${fn:length(changeLog.visibleRows) > 0}">
        <bs:changeLogTable changeLogBean="${changeLog}" showBranch="${changeLog.showBranch}" showBuildTypeInBuilds="${showBuildTypeLink}"/>
      </c:if>

      <c:if test="${changeLog.notAllResultsShown and changeLogPager.lastPage}">
        <p class="resultsTitle">
          Searched through <strong class="changes-total">${(changeLog.filter.changesLimit) * changeLog.bigPageSize}</strong> changes.
          <script type="text/javascript">BS.ChangeLog.currentPage = 1;</script>
          <a id="search-more" href="#" onclick="BS.ChangeLog.showNextBigPage('${urlInternal}'); return false;">Search in next ${changeLog.bigPageSize}</a>
          <forms:saving id="progress" className="progressRingInline"/>
        </p>
      </c:if>

      <c:if test="${fn:length(changeLog.visibleRows) > 0 and changeLogPager.totalRecords > 0}">
        <bs:pager place="bottom" urlPattern="${pagerUrlPattern}" pager="${changeLogPager}"/>
      </c:if>

      <script type="text/javascript">
        BS.ChangeLog.initChangeLogTable();

        <c:if test="${changeLog.showGraph}">
        BS.ChangeLog.redrawGraph();
        </c:if>
      </script>
    </bs:refreshable>
  </form>
  <br/>
</div>
<script type="text/javascript">
  BS.Branch.baseUrl = "${urlInternal}";
</script>
