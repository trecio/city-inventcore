<%--
Branch spec should be placed closer to the branch field.
Branch field location is vcs-plugin specific. This tag
is needed to not edit markup in each plugin.
--%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<tr>
  <th><label>Branch Specification:</label></th>
  <td>
    <props:multilineProperty name="teamcity:branchSpec" value="${vcsPropertiesBean.branchSpec}" rows="3" cols="60"
                             linkTitle="Edit branch specification" expanded="${true}" className="longField"/>
    <span class="smallNote">Branches to monitor in addition to default one. Newline-delimited set of rules in the form of <b>+|-:branch name</b> (with optional <b>*</b> placeholder)<bs:help file="Working+with+Feature+Branches#WorkingwithFeatureBranches-branchSpec"/><br/></span>
    <span class="error" id="error_teamcity:branchSpec"></span>
  </td>
</tr>
