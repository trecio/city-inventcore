<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="l" tagdir="/WEB-INF/tags/layout" %><%@

    attribute name="title" type="java.lang.String" required="true" %><%@
    attribute name="id" type="java.lang.String" required="true" %><%@
    attribute name="builds" type="java.util.List" required="true"

%><bs:_collapsibleBlock title="${title}" id="${id}">
  <table class="modificationBuilds">
    <c:forEach var="build" items="${builds}" varStatus="pos">
      <c:set var="buildType" value="${build.buildType}"/>
      <c:if test="${not empty buildType}">
        <tr class="buildTypeProblem">
          <td class="name">
            <bs:responsibleIcon responsibility="${buildType.responsibilityInfo}" buildTypeRef="${buildType}"/>
            <bs:buildTypeLinkFull buildType="${buildType}" dontShowProjectName="true" popupMode="no_self">
              <bs:buildTypeLinkFull buildType="${buildType}"/>
            </bs:buildTypeLinkFull>
          </td>
          <td><%@ include file="/changeBuild.jspf" %></td>
        </tr>
      </c:if>
    </c:forEach>
  </table>
</bs:_collapsibleBlock>