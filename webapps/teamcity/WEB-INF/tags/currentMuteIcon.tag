<%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="test" type="jetbrains.buildServer.serverSide.STest" required="true" %><%@
    attribute name="noActions" type="java.lang.Boolean" required="false"

%><c:url value="/img/muted.gif" var="url"
/><c:if test="${test.muted}"
  ><c:set var="tooltip"><bs:muteInfoTooltip test="${test}" noTestRunMuteInfo="true" noActions="${noActions}"/></c:set
  ><img src="${url}" class="mute-icon" <bs:tooltipAttrs text="${tooltip}" className="name-value-popup"/> alt=""/>
</c:if>