<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ attribute name="fileName" required="true" %>
<%@ attribute name="projectName" required="false" %>
<a class="activateFileLink" href="#" onclick="BS.Activator.doOpen('file?file=${fn:replace(fileName,'\\','/')}&project=${projectName}'); return false"
   title="Click to open in active IDE">
  <img class="activateFileIcon" src="<c:url value='/img/openInIde.gif'/>"><jsp:doBody/>
</a>