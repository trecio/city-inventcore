<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="tags" tagdir="/WEB-INF/tags/tags"
  %><%@ attribute name="build" fragment="false" required="true" type="jetbrains.buildServer.serverSide.SBuild"
  %>
<script type="text/javascript">
  BS['_ViewTagsPopup${build.buildId}'] = new BS.Popup('viewTagsPopup-${build.buildId}', {
    shift: {x: -20, y: 20}}
  ); 
</script>
<c:set var="attrs" value="id='tags:${build.buildId}'"/>
<bs:popupControl showPopupCommand="BS['_ViewTagsPopup${build.buildId}'].showPopupNearElement(this, ${build.buildId});"
                 hidePopupCommand="BS['_ViewTagsPopup${build.buildId}'].hidePopup();"
                 stopHidingPopupCommand="BS['_ViewTagsPopup${build.buildId}'].stopHidingPopup();"
                 controlId="tags:${build.buildId}"><jsp:doBody/></bs:popupControl>
<div id="viewTagsPopup-${build.buildId}" class="popupDiv" style="width: 15em;">
  <tags:showTags tags="${build.tags}" buildTypeId="${build.buildTypeId}"/> &nbsp;<tags:editTagsLink className="btn btn_mini" build="${build}">Edit</tags:editTagsLink>
</div>