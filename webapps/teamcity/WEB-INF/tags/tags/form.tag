<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"
  %><%@ attribute name="allTags" required="true" type="java.util.List"
  %>
<c:url value='/ajax.html' var="action"/>
<bs:modalDialog formId="editTagsForm" 
                title="Edit tags" 
                action="${action}" 
                closeCommand="BS.Tags.close()" 
                saveCommand="BS.Tags.submitTags()">
  <bs:_tagsEditingControl allTags="${allTags}" object="BS.Tags"/>
  
  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.Tags.close()"/>
    <forms:submit label="Save"/>
    <input type="hidden" value="" name="editTagsForBuild"/>    
    <forms:saving id="savingTags"/>
  </div>
</bs:modalDialog>
