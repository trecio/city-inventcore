<%@ tag import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ attribute name="buildTypeId" required="true" type="java.lang.String"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ attribute name="tag" required="true" type="java.lang.String"
  %><%@ attribute name="onclick" type="java.lang.String"
  %><%@ attribute name="selected" required="true" type="java.lang.Boolean"
  %><c:set var="escapedTag" value="<%=WebUtil.encode(tag)%>"/><c:url var="url" value="/viewType.html?buildTypeId=${buildTypeId}&tab=buildTypeHistoryList&tag=${escapedTag}"/>
<c:if test="${selected}"><span class="selectedTag"><c:out value="${tag}"/></span></c:if> 
<c:if test="${not selected}"><a class="unselectedTag" href="${url}" onclick="${onclick}"><c:out value="${tag}"/></a></c:if>