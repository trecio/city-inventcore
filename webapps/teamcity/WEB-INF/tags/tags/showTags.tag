<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/tags" %>
<%@ attribute name="tags" required="true" type="java.util.List"%>
<%@ attribute name="selectedTag" required="false"%>
<%@ attribute name="onclick" %>
<%@ attribute name="buildTypeId" required="true" type="java.lang.String"%>
<c:if test="${fn:length(tags) == 0}">There are no tags</c:if>
<c:if test="${not empty tags}">
  <t:tagLink buildTypeId="${buildTypeId}" tag="${tags[0]}" selected="${tags[0] eq selectedTag}" onclick="${onclick}"/>
  <c:forEach var="tag" items="${tags}" begin="1"> <t:tagLink buildTypeId="${buildTypeId}" tag="${tag}" selected="${tag eq selectedTag}" onclick="${onclick}"/></c:forEach>
</c:if>
