<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="t" tagdir="/WEB-INF/tags/tags"
  %><%@ attribute name="build" required="true" type="jetbrains.buildServer.serverSide.SBuild"
  %><%@ attribute name="nopopup"
  %>
<c:set var="linkText" value="None" scope="request"/>
<c:choose>
  <c:when test="${fn:length(build.tags) == 1}"><c:set var="linkText" scope="request"><t:tagLink selected="${false}" buildTypeId="${build.buildTypeId}" tag="${build.tags[0]}"/></c:set></c:when>
  <c:when test="${fn:length(build.tags) > 1}"><c:set var="linkText" value="Tags (${fn:length(build.tags)})" scope="request"/></c:when>
</c:choose>
<c:if test="${fn:length(build.tags) > 1 or empty nopopup}">
  <t:tagsLinkPopup build="${build}">${linkText}</t:tagsLinkPopup>
</c:if>
