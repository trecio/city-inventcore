<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags" 
  %><%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz"
  %><%@ taglib prefix="tags" tagdir="/WEB-INF/tags/tags" %><%@
  attribute name="build" required="true" type="jetbrains.buildServer.serverSide.SBuild"%><%@
  attribute name="className" required="false" type="java.lang.String"%>
<authz:authorize allPermissions="TAG_BUILD" projectId="${build.projectId}"
    ><c:set var="tags"><bs:_printTags build="${build}"/></c:set
  ><c:set var="escapedTags"><bs:escapeForJs text="${tags}" forHTMLAttribute="true"/></c:set>
<a href="#" class="${className}" onclick="BS.Tags.showEditDialog(${build.buildId}, '${escapedTags}'); return false"><jsp:doBody/></a>
</authz:authorize>