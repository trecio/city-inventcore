<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
    %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"
    %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
    %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
    %><%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"
    %><%@ attribute name="profile" type="jetbrains.buildServer.clouds.server.web.beans.CloudTabFormProfileInfo"
    %><%@ attribute name="showRunning" type="java.lang.Boolean" required="false"
    %><%@ taglib prefix="clouds" tagdir="/WEB-INF/tags/clouds"  %>
<c:if test="${not profile.loading and not profile.hasErrors and (empty showRunning or showRunning)}">
  <span class="profileRunning">
    <strong>${profile.runningInstancesCount}</strong> running
  </span>
</c:if>
<c:out value="${profile.name}"/>
<c:if test="${not empty profile.description}">
  <span style="font-weight:normal;">(<c:out value="${profile.description}"/>)</span>
</c:if>
<clouds:cloudProblemsLink controlId="error_${profile.id}" problems="${profile.problems}">Profile Error</clouds:cloudProblemsLink>
