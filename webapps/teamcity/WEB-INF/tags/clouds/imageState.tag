<%@ attribute name="status" type="jetbrains.buildServer.clouds.InstanceStatus" rtexprvalue="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<c:set var="st">${status.name}</c:set>

<img
<c:choose>
  <c:when test="${st eq 'SCHEDULED_TO_START'}">src="<c:url value="/clouds/img/starting.gif"/>" </c:when>
  <c:when test="${st eq 'STARTING'}">src="<c:url value="/clouds/img/starting.gif"/>" </c:when>
  <c:when test="${st eq 'RUNNING'}">src="<c:url value="/clouds/img/running.gif"/>" </c:when>
  <c:when test="${st eq 'RESTARTING'}">src="<c:url value="/clouds/img/restarting.gif"/>" </c:when>
  <c:when test="${st eq 'STOPPING'}">src="<c:url value="/clouds/img/stopping.gif"/>" </c:when>
  <c:when test="${st eq 'SCHEDULED_TO_STOP'}">src="<c:url value="/clouds/img/stopping.gif"/>" </c:when>
  <c:when test="${st eq 'STOPPED'}">src="<c:url value="/clouds/img/stopped.gif"/>" </c:when>
  <c:when test="${st eq 'ERROR'}">src="<c:url value="/clouds/img/error.gif"/>" </c:when>
  <c:when test="${st eq 'ERROR_CANNOT_STOP'}">src="<c:url value="/clouds/img/error.gif"/>" </c:when>
  <c:when test="${st eq 'UNKNOWN'}">src="<c:url value="/clouds/img/unknown.gif"/>" </c:when>
  <c:otherwise>src="<c:url value="/clouds/img/unknown.gif"/>" </c:otherwise>
</c:choose>
alt="${status.text}"
<bs:tooltipAttrs text="${status.text}" deltaX="20" />
/>