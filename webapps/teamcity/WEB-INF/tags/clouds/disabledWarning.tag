<%@ attribute name="disabled" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${disabled}">
  <div class="attentionComment">Cloud integration is disabled.</div>
</c:if>
