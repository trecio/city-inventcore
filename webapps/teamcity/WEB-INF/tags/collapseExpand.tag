<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="collapseAction" required="true" %>
<%@ attribute name="expandAction" required="true" %>
<%@ attribute name="type" required="false" %>
<c:choose>
  <c:when test="${type eq 'text'}">
    <a href="#" title="Collapse All" onclick="${collapseAction}">Collapse All</a>
    <span class="separator">|</span>
    <a href="#" title="Expand All" onclick="${expandAction}">Expand All</a>
  </c:when>
  <c:otherwise>
    <a class="btn btn_mini btn_icon" href="#" title="Collapse All" onclick="${collapseAction}" style="margin-right: 4px;"><img class="btn_icon_inner" src="<c:url value="/img/collapse2.png"/>" alt="" width="16" height="16"/></a>
    <a class="btn btn_mini btn_icon" href="#" title="Expand All" onclick="${expandAction}"><img class="btn_icon_inner" src="<c:url value="/img/expand2.png"/>" alt="" width="16" height="16"/></a>
  </c:otherwise>
</c:choose>
