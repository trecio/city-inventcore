<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@attribute name="iconTitle" required="false"
  %><img src="<c:url value='/img/help.gif'/>" alt="${iconTitle}" border="0" width="10" height="10"
    <c:if test="${not empty iconTitle}"><bs:tooltipAttrs text="${iconTitle}" /></c:if>
  />