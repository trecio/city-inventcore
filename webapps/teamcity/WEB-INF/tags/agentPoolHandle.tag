<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%><%@
  taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
  attribute name="agentPoolId" required="true" type="java.lang.Integer"%><%@
  attribute name="kind" required="false" type="java.lang.String"%><%@
  attribute name="forceState" required="false" type="java.lang.Boolean"%><%@
  attribute name="expanded" required="false" type="java.lang.Boolean"
  %><bs:chooseAgentPoolBlockState agentPoolId="${agentPoolId}" kind="${kind}" forceState="${forceState}" expanded="${expanded}"
  ><jsp:attribute name="ifExpanded"><c:url var="imgUrl" value="/img/blockExpanded.gif"/></jsp:attribute
  ><jsp:attribute name="ifCollapsed"><c:url var="imgUrl" value="/img/blockCollapsed.gif"/></jsp:attribute
  ></bs:chooseAgentPoolBlockState
  ><c:if test="${empty kind}"><c:set var="kind" value=""/></c:if
    ><img src="${imgUrl}" class="icon agentBlockHandle agentBlockHandle-${kind}${agentPoolId}" id="agentBlockHandle:${kind}${agentPoolId}" onclick="BS.AgentBlocks.toggleBlock('${kind}${agentPoolId}', true);"/>