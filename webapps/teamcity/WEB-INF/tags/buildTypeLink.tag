<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    attribute name="buildType" rtexprvalue="true" type="jetbrains.buildServer.serverSide.SBuildType" required="true" %><%@
    attribute name="style" required="false" %><%@
    attribute name="title" required="false" %><%@
    attribute name="target" required="false"

%><c:set var="buildTypeId" value="${buildType.buildTypeId}"
/><c:if test="${buildType.personal}"
    ><c:set var="buildTypeId" value="${buildType.sourceBuildType.buildTypeId}"
/></c:if
><c:url value="/viewType.html?buildTypeId=${buildTypeId}" var="url"
/><c:set var="text"><jsp:doBody/></c:set
><c:if test="${empty title}"
    ><c:set var="title">Click to open &quot;<bs:escapeForJs text="${buildType.name}" forHTMLAttribute="true"/>&quot; build configuration home page</c:set
></c:if
><c:if test="${buildType.project == null}"
    ><c:set var="title">Click to open build configuration home page</c:set
></c:if

><a class="buildTypeName" style="${style}" href="${serverPath}${url}" <c:if test="${not empty target}">target="${target}"</c:if>
     title="${title}"><c:choose><c:when test="${not empty text}">${text}</c:when><c:otherwise><c:out value="${buildType.name}"/></c:otherwise></c:choose></a>