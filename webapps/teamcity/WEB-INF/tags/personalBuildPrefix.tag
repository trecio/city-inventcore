<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@attribute name="buildPromotion" type="jetbrains.buildServer.serverSide.BuildPromotion" required="true"
  %><c:set var="myBuild" value="${buildPromotion.personal and currentUser == buildPromotion.owner}"
  /><c:choose
  ><c:when test="${buildPromotion.personal and myBuild}">Your personal build</c:when
  ><c:when test="${buildPromotion.personal and not myBuild and not empty buildPromotion.owner}">Personal build by <c:out value="${buildPromotion.owner.descriptiveName}"/></c:when
  ><c:when test="${buildPromotion.personal and not myBuild and empty buildPromotion.owner}">Personal build by unknown user</c:when
  ></c:choose>