<%@ tag import="jetbrains.buildServer.web.util.WebUtil"%><%@
  tag import="jetbrains.buildServer.controllers.agent.OSKind"%><%@
  taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
  taglib prefix="bs" tagdir="/WEB-INF/tags"%><%@
  attribute name="osName" required="true" type="java.lang.String"%><%@
  attribute name="small" required="false" type="java.lang.Boolean"
  %><c:set var="osKind" value="<%=OSKind.guessByName(osName)%>"
  /><c:set var="codeSuffix"><c:if test="${small}">-small</c:if></c:set
  ><c:if test="${not empty osKind}"><img src="<c:url value='/img/os/${osKind.code}${codeSuffix}-bw.png'/>"
                                         class="osIcon"
                                         title="<c:out value='${osName}'/>"
                                         alt="<bs:_trimFromComma value='${osName}'/>"
  /></c:if>