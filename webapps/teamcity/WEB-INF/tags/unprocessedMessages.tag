<%@ tag import="jetbrains.buildServer.controllers.ActionMessages" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>

<c:set var="actionMessages" value="<%=ActionMessages.getMessages(request)%>"/>

<c:forEach var="msg" items="${actionMessages.messages}">
<div class="successMessage ${msg.isWarning ? 'attentionComment' :''}" id="unprocessed_${msg.key}" style="display: none;">${msg.message}</div>
</c:forEach>
<script type="text/javascript">
  $j(document).ready(function() {
    if (!window._shownMessages) window._shownMessages = {};
    <c:forEach var="msg" items="${actionMessages.messages}">
    if (!window._shownMessages['message_${msg.key}']) {
      $('unprocessed_${msg.key}').show();
      window._shownMessages['message_${msg.key}'] = "${msg.isWarning ? 'warn' : 'info'}";
    }
    </c:forEach>

    BS.MultilineProperties.updateVisible();
  });
</script>
