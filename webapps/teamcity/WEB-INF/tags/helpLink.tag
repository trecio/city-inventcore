<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="util" uri="/WEB-INF/functions/util"
  %><%@ attribute name="file" required="true"
  %><%@ attribute name="anchor" required="false"
  %><%@ attribute name="style" required="false"
  %><%@ attribute name="id" required="false"
  %><%@ attribute name="width" required="false"
  %><%@ attribute name="height" required="false"
  %><c:choose>
  <c:when test="${empty anchor}"><c:set var="url"><bs:helpUrlPrefix/>/${file}</c:set></c:when>
  <c:otherwise><c:set var="url"><bs:helpUrlPrefix/>/${file}#${util:urlEscape(fn:replace(file, '+', ''))}-${anchor}</c:set></c:otherwise>
  </c:choose><a id="${id}" class="helpIcon" onclick="BS.Util.showHelp(event, '${url}', {width: ${empty width ? 0 : width}, height: ${empty height ? 0 : height}}); return false" style="${style}" href="${url}"
    title="View help" showdiscardchangesmessage="false"><jsp:doBody/></a>