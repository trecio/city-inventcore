<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
    %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
    %><%@ taglib prefix="queue" tagdir="/WEB-INF/tags/queue"%>
<%@attribute name="dependency" type="jetbrains.buildServer.serverSide.BuildPromotion" required="true" %>
<%@attribute name="dontShowProjectName" type="java.lang.Boolean" required="false" %>
<c:set var="startedBuild" value="${dependency.associatedBuild}"/>
<c:set var="queuedBuild" value="${dependency.queuedBuild}"/>

<div class="buildConf">
  <bs:buildTypeLinkFull buildType="${dependency.parentBuildType}" popupMode="true" dontShowProjectName="${dontShowProjectName}"/>
</div>
<c:set var="branch" value="${not empty startedBuild ? startedBuild.branch : queuedBuild.buildPromotion.branch}"/>
<c:set var="branchContent">
  <c:if test="${not empty branch}">
    <span class="branch hasBranch ${branch.defaultBranch ? 'default' : ''}"><span class="branchName"><bs:trimBranch branch="${branch}" defaultMaxLength="15"/></span></span>
  </c:if>
</c:set>
<c:choose>
  <c:when test="${not empty startedBuild}">
    <div>
      ${branchContent}
      <bs:buildCommentIcon build="${startedBuild}"/>
      <bs:buildDataIcon buildData="${startedBuild}"/>

      <bs:resultsLink build="${startedBuild}"><bs:buildNumber buildData="${startedBuild}"/> <c:out value="${startedBuild.statusDescriptor.text}"/></bs:resultsLink>
    </div>
  </c:when>
  <c:when test="${not empty queuedBuild}">
    <div>
      ${branchContent}
      <bs:queuedBuildIcon queuedBuild="${queuedBuild}"/> <queue:triggeredBy queuedBuild="${queuedBuild}" currentUser="${currentUser}">in queue</queue:triggeredBy>
    </div>
  </c:when>
</c:choose>
