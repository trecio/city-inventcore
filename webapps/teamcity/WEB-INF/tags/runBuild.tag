<%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="buildTypeId" fragment="false" required="true" %><%@
    attribute name="redirectTo" fragment="false" required="false" %><%@
    attribute name="userBranch" fragment="false" required="false" %><%@
    attribute name="options" fragment="false" required="false"
%><c:set var="caption"><jsp:doBody/></c:set
><c:if test="${fn:length(caption) == 0}"><c:set var="caption" value="Run"/></c:if
><c:set var="opts">redirectTo: '${redirectTo}'</c:set
><c:choose
    ><c:when test="${not empty userBranch}"><c:set var="opts">${opts}, branchName: '<bs:escapeForJs text="${userBranch}" forHTMLAttribute="true"/>'</c:set></c:when
    ><c:when test="${not empty branchBean and not branchBean.wildcardBranch}"
      ><c:set var="opts">${opts}, branchName: '<bs:escapeForJs text="${branchBean.userBranch}" forHTMLAttribute="true"/>'</c:set
    ></c:when
></c:choose
><c:if test="${not empty options}"><c:set var="opts">${opts}, ${options}</c:set></c:if>
<span class="buttonWrapper">
  <button class="btn btn_mini btn_main"
          onclick="Event.stop(event); BS.RunBuild.runOnAgent(this, '${buildTypeId}', { ${opts} });">${caption}</button>
  <button class="btn btn_mini btn_append"
          onclick="Event.stop(event); BS.RunBuild.runCustomBuild('${buildTypeId}', { ${opts} });"
          title="Run custom build">...</button>
</span>