<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="l" tagdir="/WEB-INF/tags/layout" %><%@

    attribute name="collapsedByDefault" type="java.lang.Boolean" %><%@
    attribute name="title" type="java.lang.String" required="true" %><%@
    attribute name="id" type="java.lang.String" required="true" %><%@
    attribute name="headerStyle" type="java.lang.String" required="false" %><%@
    attribute name="headerClass" type="java.lang.String" required="false" %><%@
    attribute name="tag" type="java.lang.String" required="false"


%><l:blockStateCss blocksType="Block_${id}" collapsedByDefault="${collapsedByDefault}" id="${id}Dl"
/><c:set var="tag" value="${not empty tag ? tag : 'p'}"/>
<${tag} class="blockHeader ${collapsedByDefault ? 'collapsed' : 'expanded'} ${headerClass}" style="${headerStyle}" id="${id}">${title}</${tag}>
<div id="${id}Dl">
  <jsp:doBody/>
</div>
<script type="text/javascript">
  <l:blockState blocksType="Block_${id}"/>
  (function() {
    var el = $('${id}');
    if (!el._block) {
      el._block = new BS.BlocksWithHeader('${id}');
    } else {
      el._block.restoreSavedBlocks();
    }
  })();
</script>