<%--@elvariable id="buildGraphBean" type="jetbrains.buildServer.web.statistics.graph.BuildGraphBean"--%>
<%@ tag import="java.util.Enumeration" %>
<%@ tag import="jetbrains.buildServer.web.statistics.graph.BuildGraphBean" %>
<%@ tag import="jetbrains.buildServer.web.statistics.graph.BuildGraphHelper" %>
<%@ tag import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="stats" tagdir="/WEB-INF/tags/graph" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="id" required="true" fragment="false" %>
<%@ attribute name="valueType" required="true" fragment="false" %>
<%@ attribute name="height" fragment="false" type="java.lang.Integer" %>
<%@ attribute name="width" fragment="false" type="java.lang.Integer" %>
<%@ attribute name="controllerUrl" fragment="false" required="false" %>
<%@ attribute name="hideFilters" required="false" type="java.lang.String" %>
<%@ attribute name="hideTitle" required="false" type="java.lang.Boolean" %>
<%@ attribute name="defaultFilter" required="false" type="java.lang.String" %>
<%@ attribute name="additionalFilter" required="false" type="java.lang.String" %>
<%@ attribute name="hints" required="false" type="java.lang.String" %>
<%@ tag body-content="empty" %>
<c:if test="${empty param['_graphKey'] || param['_graphKey']==id}">
<%
  BuildGraphBean buildGraphBean = ((BuildGraphHelper)request.getAttribute("buildGraphHelper")).createGraphBean(id, valueType, jspContext);
  request.setAttribute("buildGraphBean", buildGraphBean);
%>
<c:if test="${not empty buildGraphBean && buildGraphBean.available}">

<script type="text/javascript">
  BS.BuildGraph = BS.BuildGraph || {};

  BS.BuildGraph.showTip${id} = function(event, element) {
    BS.Tooltip.hidePopup();
    var tip = element.getAttribute("tip");
    var href = element.getAttribute("href");
    if (href) {
      tip = tip.replace('$LINK', href);
    }
    BS.Tooltip.showMessageAtCursor(event, {shift:{x:0}}, tip);
  };

  BS.BuildGraph.hideTip${id} = function() {
    BS.Tooltip.hidePopup();
  };

  BS.BuildGraph.attachHandlers${id} = function(k) {
    if (! document.getElementById("Map" + k)) return false;
    $("Map" + k).on("mouseover", "area", BS.BuildGraph.showTip${id});
    $("Map" + k).on("mouseout", "area", BS.BuildGraph.hideTip${id});
    return true;
  };

  BS.BuildGraph.applyFilter = function(graphKey) {
    var container = graphKey + 'Container';
    Form.disable(graphKey + 'Form');
    var req = "${controllerUrl}?" + BS.Util.serializeForm($(graphKey + 'Form'));
    BS.ajaxUpdater($(container), req, {
      evalScripts: true,
      method: 'GET'
    });
  }
</script>

<div id="${id}Container" class="GraphContainer">
  <div id="${id}Container_i">
    <c:set var="guid" value="${buildGraphBean.guid}"/>
    <form id="${id}Form" onsubmit="BS.BuildGraph.applyFilter('${id}');">
      <input type="hidden" name="_graphKey" value="${id}"/>
      <input type="hidden" name="_guid" value="${guid}"/>
      <input type="hidden" name="__fragmentId" value="${id}Container_i"/>
      <%
        Enumeration names = request.getParameterNames();
        while (names.hasMoreElements()) {
          String name = (String)names.nextElement();
          if (name.startsWith("_") || name.startsWith("@")) continue;
          String[] values = request.getParameterValues(name);
          for (String value : values) {
      %><input type="hidden" name="<%=name%>" value="<%=WebUtil.escapeXml(value)%>"/><%
        }
      }
    %>
      <c:set var="descriptor" value="${buildGraphBean.graphDescriptor}"/>

      <table>
        <tr>
          <td>
            <div id="${id}TT" style="width: ${descriptor.width}px; overflow: hidden">
              <div style="text-align: center; white-space: nowrap; height: 1.5em">
                <c:if test="${not hideTitle}"><strong>${buildGraphBean.title}</strong></c:if>
              </div>
            </div>
              ${buildGraphBean.graphInfo.imageMap}
            <img id="${id}Chart" usemap="#Map${guid}" src="chart.png?key=${buildGraphBean.graphInfo.imageName}"
                 alt="${descriptor.title}" width="${descriptor.width}" height="${descriptor.height}"/>
          </td>
          <c:if test="${not fn:contains(hideFilters, 'all') }">
            <td class="agentsList">
              <div class="posRel">
                <div class="agentsListHelp"><bs:help file="Statistic Charts" anchor="${valueType}"/></div>
                <c:if test="${not fn:contains(hideFilters, 'series') }">
                  <stats:agentsFilter graphKey="${id}"/>
                </c:if>
                <c:if test="${not fn:contains(hideFilters, 'averaged') }">
                  <stats:averageFilter graphKey="${id}"/>
                </c:if>
                <c:if test="${not fn:contains(hideFilters, 'range') }">
                  <stats:rangeFilter graphKey="${id}"/>
                </c:if>
                <c:if test="${not fn:contains(hideFilters, 'showFailed') }">
                  <stats:statusFilter graphKey="${id}"/>
                </c:if>
                <c:if test="${not empty additionalFilter}">
                  <jsp:include page="${additionalFilter}">
                    <jsp:param name="graphKey" value="${id}"/>
                  </jsp:include>
                </c:if>
              </div>
            </td>
          </c:if>
        </tr>
      </table>
    </form>

    <script type="text/javascript">
      new BS.Overflower($("${id}TT")).installTooltip();
      BS.BuildGraph.tryAttach${id} = setInterval(function() {
        if (BS.BuildGraph.attachHandlers${id}("${guid}")) clearInterval(BS.BuildGraph.tryAttach${id});
      }, 100);
    </script>
  </div>
</div>

</c:if>
</c:if>