<%@ tag import="java.util.Enumeration" %>
<%@ tag import="jetbrains.buildServer.web.util.WebUtil" %>
<%--@elvariable id="buildGraphBean" type="jetbrains.buildServer.web.statistics.graph.BuildGraphBean"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ attribute name="graphKey" required="true" %>
<c:set var="filter" value="${buildGraphBean.settings}"/>

<div class="averageFilter">
  <forms:checkbox
    id="@filter.average${graphKey}" name="@filter.average" value="true"
    checked="${not empty param['@filter.average'] or filter.average}"
    onclick="this.form.onsubmit();"
  /><label class="graphFilterAverage" for="@filter.average${graphKey}">Average</label>
</div>
