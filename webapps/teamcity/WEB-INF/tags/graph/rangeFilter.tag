<%--@elvariable id="buildGraphBean" type="jetbrains.buildServer.web.statistics.graph.BuildGraphBean"--%>
<%@ tag import="java.util.Enumeration" %>
<%@ tag import="jetbrains.buildServer.serverSide.statistics.build.BuildChartSettings" %>
<%@ tag import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<c:set var="RPN"><%=BuildChartSettings.FILTER_RANGE%>
</c:set>
<c:set var="rpv" value="${param[RPN]}"/>
<%@ attribute name="graphKey" required="true" %>
<span id="rangeFilter${graphKey}" class="rangeFilter">
  Range
  <select name="${RPN}" onchange="this.form.onsubmit();">
    <option value="1" <c:if test="${rpv=='1'}">selected</c:if>>Today</option>
    <option value="2" <c:if test="${rpv=='2'}">selected</c:if>>Week</option>
    <option value="3" <c:if test="${empty rpv || rpv=='3'}">selected</c:if>>Month</option>
    <option value="5" <c:if test="${rpv=='5'}">selected</c:if>>Quarter</option>
    <option value="4" <c:if test="${rpv=='4'}">selected</c:if>>Year</option>
    <option value="0" <c:if test="${rpv=='0'}">selected</c:if>>All</option>
  </select>
</span>
