<%@ tag import="java.util.Enumeration" %>
<%@ tag import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ attribute name="graphKey" required="true" %>
<%--<c:set var="buildGraphBean" value="${sessionScope[graphKey]}"/>--%>
<%--@elvariable id="buildGraphBean" type="jetbrains.buildServer.web.statistics.graph.BuildGraphBean"--%>
<c:set var="agents" value="${buildGraphBean.availableAgents}"/>
<c:set var="cols" value="1"/>
<%--<c:if test="${not empty agents and fn:length(agents)>1 }">--%>
<script type="text/javascript">
  BS.BuildGraph = BS.BuildGraph || {};
  BS.BuildGraph.setBoxes = function(key, val) {
    var boxes = document.forms[key+"Form"]['@filter.s'];
    if (boxes) {
      if (boxes.length) {
        for (var i = 0; i < boxes.length; i++) boxes[i].checked = val;
      }
      else boxes.checked = val;
    }
    return boxes;
  }
</script>
<div class="agentsFilter" id="agentsFilter${graphKey}">

    <div>
      <strong>${buildGraphBean.valueType.seriesGenericName}s</strong>
      <a href="#" onclick="BS.BuildGraph.setBoxes('${graphKey}', true); BS.BuildGraph.applyFilter('${graphKey}'); return false" class="all_none">All</a>
      <a href="#" onclick="var boxes = BS.BuildGraph.setBoxes('${graphKey}', false); if (boxes && typeof boxes.checked != 'undefined') { BS.BuildGraph.applyFilter('${graphKey}'); }; return false" class="all_none">None</a>
    </div>
    <c:forEach items="${agents}" var="agentName">
      <input type="hidden" name="@filter.pas" value="${agentName}"/>
    </c:forEach>
    <div class="agentsFilterInner" style="height: ${buildGraphBean.graphDescriptor.height-30}px;">
    <table class="agentsFilterTable">
      <c:forEach items="${agents}" var="agentName" varStatus="pos">
        <c:if test="${pos.index % cols == 0}"><tr></c:if>
        <td>
          <c:if test="${pos.index % cols != 0}">&nbsp;&nbsp;</c:if>
          <c:set var="id" value="${graphKey}agentName${pos.index}"/><label for="${id}" class="name">
          <forms:checkbox
            id="${id}"
            name="@filter.s"
            value="${agentName}"
            checked="${not empty buildGraphBean.settings.selectedSeries[agentName]}"
            style="vertical-align:middle; margin:0; padding:0;"
            onclick="this.form.onsubmit();"/>
          <img src="<c:url value="/img/gp.gif"/>"
               style="margin:0 2px; background-color: ${buildGraphBean.graphInfo.colorMap[agentName]} !important;"><c:out value="${agentName}"/></label>
        </td>
      </c:forEach>
    </table>
    </div>

</div>
<%--</c:if>--%>
