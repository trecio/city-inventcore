<%@ tag import="java.util.List" %>
<%@ tag import="jetbrains.buildServer.serverSide.SFinishedBuild" %>
<%@ tag import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout"%>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin"%>
<%@ attribute name="buildType" required="true" type="jetbrains.buildServer.serverSide.SBuildType"%>
<div id="valuePopup" class="valuePopup modalDialog">
  <div id="valuePopupHeader" class="dialogHeader">
    <div class="closeWindow">
      <a title="Close dialog window" href="#" onclick="BS.BuildTypeSettingsPopup.closeMultilineValuePopup(); return false">
        <img src='<c:url value="/img/close.gif"/>'/>
      </a>
    </div>
    <div class="dialogHandle">
      <h3 class="dialogTitle" id="valuePopupTitle"></h3>
    </div>
  </div>

  <div class="modalDialogBody">
    <textarea id="valueContainer" rows="5" cols="40" readonly="true" wrap="off"></textarea>
  </div>
</div>
