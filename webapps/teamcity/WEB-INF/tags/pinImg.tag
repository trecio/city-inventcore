<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@attribute name="build" required="true" type="jetbrains.buildServer.serverSide.SFinishedBuild"
  %><%@attribute name="style"
  %><c:set var="buildId" value="${build.buildId}"
  /><img src="<c:url value="/img/pinned.png"/>" alt="" id="pinImg${buildId}" class='pinImg' style="${style}"
    <bs:_pinLinkAttrs build="${build}" unpinAndNotOnBuildPage="false"/> />