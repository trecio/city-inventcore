<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    attribute name="buildData" rtexprvalue="true" type="jetbrains.buildServer.serverSide.SRunningBuild"

%><c:if test="${buildData.buildStatus.successful}"
    ><div class="progressInner" style="background-color:green; width: ${buildData.completedPercent}%;"><jsp:doBody/></div></c:if
 ><c:if test="${not buildData.buildStatus.successful}"
    ><div class="progressInner" style="background-color:#c90a00; width: ${buildData.completedPercent}%;"><jsp:doBody/></div></c:if>