<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="name" required="true" type="java.lang.String"%>
<%@ attribute name="checked" required="false" type="java.lang.Boolean"%>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean"%>
<%@ attribute name="onclick" required="false" type="java.lang.String"%>
<%@ attribute name="style" required="false" type="java.lang.String"%>
<%@ attribute name="className" required="false" type="java.lang.String"%>
<%@ attribute name="value" required="false" type="java.lang.String"%>

<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>

<c:set var="value" value="${empty value ? 'true' : value}"/>
<c:set var="checked" value="${checked or not empty propertiesBean.properties[name] ? 'checked=true' : ''}"/>
<c:set var="disabled" value="${disabled ? 'disabled=true' : ''}"/>

<input type="checkbox" name="prop:${name}" id="${name}" onclick="${onclick}" ${checked} ${disabled} value="${value}"
        <c:if test="${not empty className}">class="${className}"</c:if> style="vertical-align:middle; margin:0; padding:0; ${style}"/>

