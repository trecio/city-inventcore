<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>

<tr>
  <th>
    <label for="jvmArgs">JVM command line parameters:</label>
  </th>
  <td>
    <props:textProperty name="jvmArgs" className="longField" expandable="true"/>
  </td>
</tr>
