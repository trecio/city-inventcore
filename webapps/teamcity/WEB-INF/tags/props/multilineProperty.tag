<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ attribute name="name" required="true" %>
<%@ attribute name="linkTitle" required="true" %>
<%@ attribute name="cols" required="true" type="java.lang.Integer" %>
<%@ attribute name="rows" required="true" type="java.lang.Integer" %>
<%@ attribute name="onkeydown" required="false"%>
<%@ attribute name="style" required="false"%>
<%@ attribute name="expanded" required="false" type="java.lang.Boolean" rtexprvalue="true" %>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean"%>
<%@ attribute name="className" required="false"%>
<%@ attribute name="value" required="false" type="java.lang.String"%>
<jsp:useBean id="propertiesBean" type="jetbrains.buildServer.controllers.BasePropertiesBean" scope="request"/>
<c:set var="actualValue" value="${empty value ? propertiesBean.properties[name] : value}" />

<props:textarea name="${name}" textAreaName="prop:${name}" value="${actualValue}"
                linkTitle="${linkTitle}" cols="${cols}" rows="${rows}" disabled="${disabled}"
                expanded="${expanded}" onkeydown="${onkeydown}" style="${style}" className="${className}"/>
