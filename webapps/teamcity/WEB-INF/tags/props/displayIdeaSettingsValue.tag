<%@ tag import="jetbrains.buildServer.ideaSettings.IdeaSettings" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>

<props:viewWorkingDirectory/>

<%
jetbrains.buildServer.ideaSettings.IdeaSettings settings = new jetbrains.buildServer.ideaSettings.IdeaSettings();
settings.readFrom(propertiesBean.getProperties().get("iprInfoRunParam"));
jspContext.setAttribute("ideaSettings", settings);
jspContext.setAttribute("pathVarsNames", settings.getPathVariables().keySet());
%>

<%--@elvariable id="ideaSettings" type="jetbrains.buildServer.ideaSettings.IdeaSettings"--%>
<%--@elvariable id="pathVarsNames" type="java.util.Collection"--%>
<div class="parameter">
  Path to the project: <strong><c:out value="${ideaSettings.ipr}"/></strong>
</div>

<div class="parameter">
  Path variables: <c:if test="${fn:length(pathVarsNames) == 0}"><strong>none defined</strong></c:if>
</div>
<c:if test="${fn:length(pathVarsNames) > 0}">
<div class="nestedParameter"><table>
<c:forEach items="${pathVarsNames}" var="varName">
<tr>
  <td style="vertical-align:top;"><c:out value="${varName}"/>:</td>
  <td style="vertical-align:top;"><c:out value="${ideaSettings.pathVariables[varName].value}"/></td>
</tr>
</c:forEach>
</table></div>
</c:if>

<div class="parameter">
  Project JDKs: <c:if test="${fn:length(ideaSettings.jdks) == 0}"><strong>none defined</strong></c:if>
</div>

<c:if test="${fn:length(ideaSettings.jdks) > 0}">
<div class="nestedParameter"><table>
<c:forEach items="${ideaSettings.jdks}" var="jdk">
<tr>
  <td style="vertical-align:top;"><c:out value="${jdk.name}"/>:</td>
  <td style="vertical-align:top;"><c:out value="${jdk.pathToJdk}"/><br/><c:out value="${jdk.patternsText}"/>
</td>
</tr>
</c:forEach>
</table></div>
</c:if>

<div class="parameter">
  Global libraries: <c:if test="${fn:length(ideaSettings.globalLibraries) == 0}"><strong>none defined</strong></c:if>
</div>

<c:if test="${fn:length(ideaSettings.globalLibraries) > 0}">
<div class="nestedParameter"><table>
<c:forEach items="${ideaSettings.globalLibraries}" var="lib">
<tr>
  <td style="vertical-align:top;"><c:out value="${lib.name}"/>:</td>
  <td style="vertical-align:top;"><c:out value="${lib.pathToLibrary}"/><br/><c:out value="${lib.patternsText}"/>
</td>
</tr>
</c:forEach>
</table></div>
</c:if>
