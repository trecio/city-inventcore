<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="name" required="true" type="java.lang.String"%>
<%@ attribute name="value" required="false" type="java.lang.String"%>

<jsp:useBean id="propertiesBean" scope="request" type="jetbrains.buildServer.controllers.BasePropertiesBean"/>
<c:choose>
  <c:when test="${empty value}">
    <c:set var="actualValue" value="${propertiesBean.properties[name]}"/>
  </c:when>
  <c:otherwise>
    <c:set var="actualValue" value="${value}"/>
  </c:otherwise>
</c:choose>
<input type="hidden"
       name="prop:${name}"
       id="${name}"
       value="${actualValue}"/>
