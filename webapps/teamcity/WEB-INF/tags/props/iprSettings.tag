<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"
  %><%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@attribute name="showWorkingDir" type="java.lang.Boolean" %>
<jsp:useBean id="iprInfo" type="jetbrains.buildServer.controllers.admin.ipr.IprInfo" scope="request"/>
<jsp:useBean id="supportedProjectTypes" type="java.util.Map" scope="request"/>

<script type="text/javascript">
  BS.reparseIpr = function(button) {
    if (button.form.cancel.value != 'true') {

      // Start reparse

      BS.Util.show('iprParseProgress');
      BS.Util.hide('iprWarning');

      BS.ajaxRequest("iprReparse.html", {
        method: 'post',
        parameters: BS.Util.serializeForm(button.form) + "&id=${param['id']}&template=${buildForm.template}&projectId=${buildForm.project.projectId}&runnerId=${buildForm.buildRunnerBean.id}",
        onSuccess: function() {
          BS.refreshReparse();
        }
      });

      button.form.cancel.value = 'true';
      button.form.reparse.value = 'Cancel';
    }
    else {

      // Cancel reparse

      BS.Util.hide('iprParseProgress');
      BS.Util.hide('iprRefreshProgress');

      BS.ajaxRequest("iprReparse.html", {
        method: 'post',
        parameters: BS.Util.serializeForm(button.form) + "&id=${param['id']}&template=${buildForm.template}&projectId=${buildForm.project.projectId}&runnerId=${buildForm.buildRunnerBean.id}"
      });

      button.form.cancel.value = '';
      button.form.reparse.value = 'Check/Reparse Project';
    }

    BS.Util.hide('error_projectReparseRequired');
    BS.MultilineProperties.updateVisible();
  };

  BS.reloadIprRunner = function() {
    try {
      var select = $('runnerType');
      $('runnerParams').updateContainer(select.options[select.selectedIndex].value);
    } catch (e) {
      //
    }
  };

  BS.refreshReparse = function() {

    BS.Util.show('iprParseProgress');
    $('iprRefreshProgress').style.display = 'inline';
    BS.MultilineProperties.updateVisible();

    BS.ajaxRequest("iprReparse.html", {
      method: 'get',
      parameters: "id=${param['id']}&projectId=${buildForm.project.projectId}&runnerId=${buildForm.buildRunnerBean.id}",
      onSuccess: function (transport) {
        if (transport.responseXML) {
          var element = transport.responseXML.documentElement;

          // expected mandatory attributes are 'done', 'progressText', 'percent'

          if (element.getAttribute('done') == 'true') {
            BS.reloadIprRunner();
          }
          else {
            // still in progress
            $('iprRefreshProgressText').innerHTML = element.getAttribute('progressText');
            var percent = parseInt(element.getAttribute('percent'));
            $('iprRefreshPercent').innerHTML = percent > 0 ? "(" + percent + "%)" : "";

            BS.refreshReparse.delay(5);
          }
        }
        else {
          BS.reloadIprRunner();
        }
      }
    });
  };
  <c:set var="cancelValue" value=""/>
  <c:set var="reparseButtonText" value="Check/Reparse Project"/>
  <c:if test="${not empty iprInfo.analyzeTask and not iprInfo.analyzeTask.done}">
    BS.refreshReparse.delay(1);
    <c:set var="cancelValue" value="true"/>
    <c:set var="reparseButtonText" value="Cancel"/>
  </c:if>

</script>
<l:settingsGroup title="IntelliJ IDEA Project Settings">

<c:if test="${supportedProjectTypes['maven'] != null}">
  <c:if test="${'maven' == iprInfo.projectType}">
    <c:set var="hideCheckReparse" value="style='display: none'"/>
  </c:if>
<tr>
  <th><label for="iprInfo.ipr">Project file type:</label></th>
  <td>
    <c:set var="onclick">
      BS.Util.show('checkReparseSection');
      BS.Util.show('projectSdksAndLibs');
      BS.Util.show('iprNote');
      BS.Util.hide('pomNote');
      BS.MultilineProperties.updateVisible();
    </c:set>
    <forms:radioButton name="iprInfo.projectType" id="projectTypeIpr" value="ipr" checked="${'ipr' == iprInfo.projectType}" onclick="${onclick}" title=""/>
    <label for="projectTypeIpr">IntelliJ IDEA</label>

    <span style="padding-left: 5em">
      <c:set var="onclick">
        $('parseAllProjectFiles').selected = false;
        BS.Util.hide('checkReparseSection');
        BS.Util.hide('projectSdksAndLibs');
        BS.Util.hide('iprNote');
        BS.Util.show('pomNote');
        BS.MultilineProperties.updateVisible();
      </c:set>
      <forms:radioButton name="iprInfo.projectType" id="projectTypeMaven" value="maven" checked="${'maven' == iprInfo.projectType}" onclick="${onclick}" title=""/>
      <label for="projectTypeMaven">Maven</label>
    </span>
  </td>
</tr>
</c:if>
<tr>
  <th><label for="iprInfo.ipr">Path to the project: </label><bs:help file="IntelliJ+IDEA+Project" anchor="projectPath"/></th>
  <td>
    <input type="text" name="iprInfo.ipr" id="iprInfo.ipr" value="${iprInfo.ipr}"
            class="textProperty longField"/><bs:vcsTree fieldId="iprInfo.ipr"/>
    <span class="error" id="error_iprInfo.ipr"></span>
    <c:if test="${not empty iprInfo.iprWarning}">
      <div class="attentionComment" id="iprWarning">${iprInfo.iprWarning}</div>
    </c:if>
    <c:if test="${'maven' == iprInfo.projectType}">
      <c:set var="hideIprNote" value="style='display: none'"/>
    </c:if>
    <c:if test="${'maven' != iprInfo.projectType}">
      <c:set var="hidePomNote" value="style='display: none'"/>
    </c:if>

    <span class="smallNote" id="iprNote" ${hideIprNote}>Should reference path to project file (<strong>.ipr</strong>) or project directory for directory-based (<strong>.idea</strong>) projects. The specified path should be relative to the checkout directory. Leave empty if <strong>.idea</strong> directory is right under the checkout directory.</span>
    <span class="smallNote" id="pomNote" ${hidePomNote}>Should reference path to Maven <strong>pom.xml</strong> file. The specified path should be relative to the checkout directory.</span>

    <div id="checkReparseSection" ${hideCheckReparse}>
      <input type="hidden" name="_iprInfo.parseAllProjectFiles" value=""/>
      <forms:checkbox name="iprInfo.parseAllProjectFiles" id="parseAllProjectFiles" checked="${iprInfo.parseAllProjectFiles}"/>
      <label for="parseAllProjectFiles">Detect global libraries and module-based JDK in the *.iml files</label>
      <span class="smallNote">This process can take time, because it involves loading and parsing all project module (*.iml) files.</span>

      <div style="margin-top: 1em;" id="reparseBlock">
        <input type="button" name="reparse" value="${reparseButtonText}" onclick="BS.reparseIpr(this);" class="btn btn_mini"/>
        <input type="hidden" name="cancel" value="${cancelValue}"/>
        <forms:saving id="iprParseProgress" savingTitle="Loading and analyzing project files..." className="progressRingInline" />
        <span id="iprRefreshProgress" style="display: none;">
          <span id="iprRefreshProgressText"></span>
          <span id="iprRefreshPercent"></span>
        </span>

      </div>

      <span class="error" id="error_projectReparseRequired"></span>
    </div>

  </td>
</tr>

</l:settingsGroup>

<tr>
  <td colspan="2" style="margin:0; padding: 0;" class="noBorder">
  <table id="projectSdksAndLibs" ${hideCheckReparse} style="margin: 0; padding: 0" width="100%" class="runnerFormTable">

  <c:if test="${not empty iprInfo.unresolvedModules or not empty iprInfo.pathVariables}">
  <l:settingsGroup title="Unresolved Project Modules and Path Variables">

  <c:if test="${not empty iprInfo.unresolvedModules}">
    <tr>
      <td colspan="2" class="unresolvedModules">
        <bs:icon icon="../attentionComment.gif"/> Unresolved Project Modules:
        <c:forEach items="${iprInfo.unresolvedModules}" var="module">
          <div><c:out value="${module}"/></div>
        </c:forEach>

      </td>
    </tr>
  </c:if>

  <c:if test="${not empty iprInfo.pathVariables}">
    <c:forEach var="pathVar" items="${iprInfo.pathVariables}">
      <tr>
        <td class="pathVariableName">${pathVar.key}</td>
        <td class="pathVariableValue">
          <c:set var="var" value="${pathVar.value}"/>
          <jsp:useBean id="var" type="jetbrains.buildServer.ideaSettings.PathVariableInfo"/>

          <c:if test="${not empty var.unresolvedText}">
            <label>Used in path:</label>
            <div class="cannotResolveMessage" >${var.unresolvedText}</div>
          </c:if>

          <label for="${pathVar.key}_pathvar_value">
            Set value to:
          </label>
          <input type="text" name="iprInfo.pathVariables[${pathVar.key}].value"
                 id="${pathVar.key}_pathvar_value" value="${var.value}" class="textProperty longField"/>
        </td>
      </tr>
    </c:forEach>
  </c:if>
  </l:settingsGroup>
  </c:if>

  <l:settingsGroup title="Project JDKs">
    <c:if test="${empty iprInfo.jdk and empty iprInfo.ideaJdk}">
      <tr>
        <td colspan="2"><div class="attentionComment">Project JDKs are not configured, please click "Check/Reparse Project" button.</div></td>
      </tr>
    </c:if>
    <c:forEach var="jdk" items="${iprInfo.jdk}">
      <c:set var="varJdk" value="${jdk.value}"/>
      <jsp:useBean id="varJdk" type="jetbrains.buildServer.ideaSettings.Jdk"/>
      <tr>
        <th class="libraryName">${varJdk.name}</th>
        <td class="libraryValue">
          <props:library libraryKey="${jdk.key}_jdk" pathName="iprInfo.jdk[${jdk.key}].pathToJdk"
                         pathTitle="JDK Home" pathValue="${varJdk.pathToJdk}"
                         patternName="iprInfo.jdk[${jdk.key}].patternsText"
                         patternValue="${varJdk.patternsText}" patternTitle="JDK Jar Files Patterns"
                         helpAnchor="projectJdk"
            />
          <props:iprRemoveJdk key="jdk_${jdk.key}" typeName="JDK" shouldShow="${not varJdk.used}"/>
        </td>
      </tr>
    </c:forEach>

    <c:forEach var="jdk" items="${iprInfo.ideaJdk}">
      <c:set var="varIdeaJdk" value="${jdk.value}"/>
      <jsp:useBean id="varIdeaJdk" type="jetbrains.buildServer.ideaSettings.IdeaJdk"/>
      <tr>
        <th class="libraryName">${varIdeaJdk.name}</th>
        <td class="libraryValue">
          <props:library libraryKey="${jdk.key}_ideaJdk" pathName="iprInfo.ideaJdk[${jdk.key}].pathToIdea"
                         pathTitle="IDEA Home" pathValue="${varIdeaJdk.pathToIdea}"
                         patternName="iprInfo.ideaJdk[${jdk.key}].ideaPatternsText"
                         helpAnchor="projectJdk"
                         patternValue="${varIdeaJdk.ideaPatternsText}" patternTitle="IDEA Jar Files Patterns"/>
          <props:library libraryKey="${jdk.key}_jdk1" pathName="iprInfo.ideaJdk[${jdk.key}].pathToJdk"
                         pathTitle="JDK Home" pathValue="${varIdeaJdk.pathToJdk}"
                         patternName="iprInfo.ideaJdk[${jdk.key}].patternsText"
                         helpAnchor="projectJdk"
                         patternValue="${varIdeaJdk.patternsText}" patternTitle="JDK Jar Files Patterns"/>
          <props:iprRemoveJdk key="ideajdk_${jdk.key}" typeName="IDEA JDK" shouldShow="${not varIdeaJdk.used}"/>
        </td>
      </tr>
    </c:forEach>

  </l:settingsGroup>

  <c:if test="${not empty iprInfo.libraries}">
    <l:settingsGroup title="Project Global Libraries">
      <c:forEach var="lib" items="${iprInfo.libraries}">
        <c:set var="varLib" value="${lib.value}"/>
        <jsp:useBean id="varLib" type="jetbrains.buildServer.ideaSettings.Library"/>
        <tr>
          <th class="libraryName">${varLib.name}</th>
          <td class="libraryValue">
            <props:library libraryKey="${lib.key}_lib" pathName="iprInfo.libraries[${lib.key}].pathToLibrary"
                           pathTitle="Path to Library" pathValue="${varLib.pathToLibrary}"
                           patternName="iprInfo.libraries[${lib.key}].patternsText"
                           helpAnchor="projectLibrary"
                           patternValue="${varLib.patternsText}" patternTitle="Library Jar Files Patterns"/>
            <props:iprRemoveJdk key="library_${lib.key}" typeName="global library" shouldShow="${not varLib.used}"/>
          </td>
        </tr>
      </c:forEach>
    </l:settingsGroup>
  </c:if>
  </table>
  </td>
</tr>

<l:settingsGroup title="Java Parameters">
  <props:editJavaHome/>

  <props:editJvmArgs/>

  <c:if test="${showWorkingDir}">
    <forms:workingDirectory />
  </c:if>
</l:settingsGroup>
