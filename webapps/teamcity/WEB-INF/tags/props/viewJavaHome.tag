<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>

<div class="parameter">
  JDK home path: <props:displayValue name="target.jdk.home" emptyValue="not specified"/>
</div>
