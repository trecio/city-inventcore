<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>

<tr>
  <th><label for="target.jdk.home">JDK home path: <bs:help file="Ant" anchor="antJdkHomePathOptionDescription"/></label></th>
  <td>
    <props:textProperty name="target.jdk.home" className="longField"/>
    <span class="smallNote">If left blank, the path will be read from JAVA_HOME environment variable or agent's own Java.</span>
  </td>
</tr>
