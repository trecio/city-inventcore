<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@attribute name="containerId" required="true"
  %><%@attribute name="filterText" required="false"
  %><%@attribute name="style" required="false" 
  %><%@attribute name="afterApplyFunc" required="false"
  %><%@attribute name="activate" required="false" type="java.lang.Boolean"
  %><%@attribute name="className" required="false" %>
<div class="inplaceFilterDiv ${className}" style="${style}">
  <input type="text" id="${containerId}_filter" value="" autocomplete="off"/>
</div>
<script type="text/javascript">
  (function($) {
    var input = $('#${containerId}_filter');
    input.placeholder({text: '${empty filterText ? "<filter>" : filterText}'});
    input.on('keyup', _.throttle(function() {
      BS.InPlaceFilter.applyFilter('${containerId}', this<c:if test="${not empty afterApplyFunc}">, ${afterApplyFunc}</c:if>);
    }, 100));
    $(document).ready(function() {
      BS.InPlaceFilter.prepareFilter('${containerId}');
    });
    <c:if test="${activate}">
    input.trigger('focus');
    </c:if>
  })(jQuery);
</script>
