<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin"%>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"%>
<%@attribute name="userParametersBean" type="jetbrains.buildServer.controllers.buildType.ParametersBean" required="true" %>
<%@attribute name="parametersActionUrl" type="java.lang.String" required="true" %>
<%@attribute name="parametersAutocompletionUrl" type="java.lang.String" required="true" %>

<bs:refreshable containerId="userParams" pageUrl="${pageUrl}">

<bs:messages key="parameterRemoved"/>
<bs:messages key="parameterUpdated"/>

<p>
  <forms:addButton showdiscardchangesmessage="false" onclick="BS.EditParameterDialog.showDialog('', '', 'conf', false, false, ''); return false"><c:out value="Add new parameter"/></forms:addButton>
</p>

<h2 class="noBorder">Configuration Parameters <bs:help file="Configuring+Build+Parameters" anchor="ConfigurationParameters"
      shortHelp="Configuration parameters are not passed into build, can be used in references only."/></h2>
<admin:editableParametersList params="${userParametersBean.configurationParameters}" paramType="conf"/>

<br/>

<h2 class="noBorder">System Properties (system.) <bs:help file="Configuring+Build+Parameters" anchor="BuildParameters"
      shortHelp="System properties will be passed into the build, they are only supported by the build runners that understand the property notion."/></h2>
<admin:editableParametersList params="${userParametersBean.systemProperties}" paramType="system"/>

<br/>

<h2 class="noBorder">Environment Variables (env.) <bs:help file="Configuring+Build+Parameters" anchor="BuildParameters"
      shortHelp="Environment variables will be added to the environment of the processes launched by the build runner."/></h2>
<admin:editableParametersList params="${userParametersBean.environmentVariables}" paramType="env"/>

</bs:refreshable>

<bs:modalDialog formId="editParamForm"
                title="Parameters"
                action="${parametersActionUrl}"
                closeCommand="BS.EditParameterDialog.cancelDialog()"
                saveCommand="BS.EditParameterForm.saveParameter()">
  <table class="runnerFormTable">
    <tr>
      <th>
        <label class="editParameterLabel" for="parameterName">Name:<l:star/></label>
      </th>
      <td>
        <forms:textField name="parameterName" value="" style="width:100%;" maxlength="512" noAutoComplete="true"/>
        <span class="error" id="error_parameterName"></span>
        <span class="smallNote" id="inheritedParamName" style="display: none; margin-left: 0;">Name of the inherited parameter cannot be changed</span>
      </td>
    </tr>
    <tr>
      <th>
        <label class="editParameterLabel" for="paramType">Kind: </label>
      </th>
      <td>
        <select id="paramType" name="paramType">
          <option value="conf">Configuration parameter</option>
          <option value="system">System property (system.)</option>
          <option value="env">Environment variable (env.)</option>
        </select>        
      </td>
    </tr>
    <tr>
      <th>
        <label class="editParameterLabel" for="parameterValue">Value:</label>
      </th>
      <td>
        <forms:textField expandable="true" name="parameterValue" style="width: 100%;" className="buildTypeParams"/>
        <span class="error" id="error_parameterValue"></span>
        <span class="smallNote">Type '%' for reference completion</span>
      </td>
    </tr>
    <tr>
      <th>
        <label class="editParameterLabel" for="parameterSpec">Spec:</label>
      </th>
      <td>
        <div style="margin-bottom: 5px">
          <forms:saving id="parameterSpecEditFormContentLoading" className="progressRingInline"/>
          <input id="editParameterSpec" class="btn btn_mini" type="button"
                 onclick="return BS.EditParametersSpecDialog.showDialog($('parameterSpec').value);" value="Edit..."/>
        </div>
        <div id="parameterSpecHolderEdit">
          <forms:textField expandable="true" name="parameterSpec" style="width:100%;" className="buildTypeParams" noAutoComplete="${true}"/>

          <div class="clr spacing"></div>
          <span class="error" id="error_parameterSpec"></span>
          <span class="smallNote">
            Defines parameter control presentation and validation. Parameter specification format is <em>typeName[ key='value'[ key2='value2']]</em>, ServiceMessage syntax is used.
          </span>
        </div>

        <div id="parameterSpecHolderExpand">
          <a href="#" style="vertical-align: middle;" onclick="return BS.EditParameterDialog.showSpecEditFields(true, true);">Show raw value</a>
          <span class="smallNote">
            Defines parameter control presentation and validation.
          </span>
        </div>
      </td>
    </tr>
  </table>

        <script type="text/javascript">
          (function($) {
            $(document).ready(function() {
              $("#parameterName").on("keyup", BS.EditParameterDialog.createParamNameChangeHandler($("#parameterName"), $("#paramType")));

              $("#paramType")
                  .on("change", BS.EditParameterDialog.paramTypeChangedHandler)
                  .on("keyup", function(event) {
                     if (BS.EditParameterDialog.canChangeSelectValue(event)) {
                       BS.EditParameterDialog.paramTypeChangedHandler();
                     }
                   });
            });
          }(jQuery));
        </script>

        <div class="popupSaveButtonsBlock">
          <forms:cancel showdiscardchangesmessage="false" onclick="BS.EditParameterDialog.cancelDialog()"/>
          <forms:submit label="Save"/>
          <forms:saving id='userParamsSaving'/>
        </div>

        <input type="hidden" id="currentNameWithPrefix" name="currentNameWithPrefix" value=""/>
        <input type="hidden" id="currentName" name="currentName" value=""/>
        <input type="hidden" id="submitAction" name="submitAction" value=""/>
        <input type="hidden" id="submitBuildType1" name="submitBuildType" value="1"/>
</bs:modalDialog>

<jsp:useBean id="parametersCongig" class="jetbrains.buildServer.controllers.parameters.ParameterConstants"/>
<c:url var="parametersEditFormUrl" value="${parametersCongig.editTypeParameterUrl}"/>
<bs:modalDialog formId="parameterSpecEditForm"
                title="Edit parameter specification"
                action="${parametersEditFormUrl}"
                closeCommand="BS.EditParametersSpecDialog.closeDialog()"
                saveCommand="BS.EditParametersSpecDialog.submitDialog()">
  <div id="parameterSpecEditFormContent">

  </div>

  <div class="popupSaveButtonsBlock">
    <forms:cancel showdiscardchangesmessage="false" onclick="BS.EditParametersSpecDialog.closeDialog()"/>
    <forms:submit id="parameterSpecEditFormSubmit" label="Save"/>
    <forms:saving id='parameterSpecEditFormSaving'/>
  </div>
</bs:modalDialog>

<script type="text/javascript">
  (function($) {
    var alt = false;
    $(document).keydown(function(event) {
      if (event.keyCode === $.ui.keyCode.ALT) {
        alt = true;
      } else if (alt && event.keyCode === $.ui.keyCode.INSERT) {
        if (!BS.EditParameterDialog.isVisible()) {
          BS.EditParameterDialog.showDialog('', '', 'conf');
          return false;
        }
      }
    }).keyup(function(event) {
      if (event.keyCode === $.ui.keyCode.ALT) {
        alt = false;
      }
    });
  } (jQuery));
</script>

