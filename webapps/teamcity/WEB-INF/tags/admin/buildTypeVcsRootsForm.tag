<%@ tag import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="admfn" uri="/WEB-INF/functions/admin" %>
<%@ attribute name="vcsRootsBean" required="true" type="jetbrains.buildServer.controllers.admin.projects.VcsSettingsBean"%>
<%@ attribute name="mode" required="true" %>
<%@ attribute name="pageUrl" type="java.lang.String" required="true" %>
<c:set var="encodedCameFromUrl" value="<%=WebUtil.encode(pageUrl)%>"/>
<c:set var="createMode" value="${fn:startsWith(mode, 'create')}"/>

<input type="hidden" name="submitBuildType" id="submitBuildType" value=""/>
<input type="hidden" name="doAttach" id="doAttach" value="false">
<input type="hidden" name="doApplyVcsSettings" id="doApplyVcsSettings" value="false">

<script type="text/javascript">
  document.observe("dom:loaded", function() {
    $('checkoutDir').on('keypress', function() {
      setTimeout(BS.EditVcsRootsForm.updateCheckoutDirectoryWarning, 10);
    });
    BS.EditVcsRootsForm.updateCheckoutDirectoryWarning();
  });
</script>

<h2 class="noBorder">Version Control Settings</h2>
<c:set var="pageTitle">
  <c:choose>
    <c:when test="${mode == 'createTemplate'}">Create Build Configuration Template</c:when>
    <c:when test="${mode == 'createBuildType'}">Create Build Configuration</c:when>
    <c:when test="${mode == 'editTemplate'}">Edit Build Configuration Template</c:when>
    <c:when test="${mode == 'editBuildType'}">Edit Build Configuration</c:when>
  </c:choose>
</c:set>

<div id="existingVcsRoots">
  <admin:vcsRootsTable vcsRootsBean="${vcsRootsBean}"
                       pageUrl="${pageUrl}"
                       pageTitle="${pageTitle}"
                       buildForm="${buildForm}" />

  <c:if test="${empty vcsRootsBean.vcsRoots}">
    There are no VCS roots attached to this build configuration.
  </c:if>
</div>

<c:set var="canAttachLocalRoots" value="${not buildForm.template or mode == 'createTemplate' or buildForm.canAttachLocalVcsRoots}"/>
<c:set var="attachableLocalRoots" value="${vcsRootsBean.attachableLocalVcsRoots}"/>
<c:set var="attachableSharedRoots" value="${vcsRootsBean.attachableSharedVcsRoots}"/>
<table class="actionTable" cellpadding="0" cellspacing="0">
  <c:if test="${canAttachLocalRoots or afn:permissionGrantedGlobally('CHANGE_GLOBAL_VCS_ROOT')}">
    <tr>
      <td colspan="2">
        <admin:vcsRootEditScope vcsRootsBean="${vcsRootsBean}" vcsRoot="${null}" buildForm="${buildForm}"/>
        <c:set var="createLink"><admin:createVcsRootLink editingScope="${editingScope}"
                                                         cameFromTitle="${pageTitle}"
                                                         cameFromUrl="${pageUrl}"
                                                         withoutLink="true"/></c:set>
        <forms:addButton href="${createLink}" showdiscardchangesmessage="false">Create and attach new VCS root</forms:addButton>
      </td>
    </tr>
  </c:if>
  <c:if test="${(not empty attachableLocalRoots and canAttachLocalRoots) or not empty attachableSharedRoots}">
  <tr>
  <td class="option" style="width: 24%; padding-top: 1em">
    <label for="vcsRootId">or attach existing VCS root:</label>
  </td>
  <td class="selector" style="padding-top: 1em">
    <forms:select name="vcsRootId" style="width: 340px;" onchange="BS.EditVcsRootsForm.attachExistingVcsRoot()" enableFilter="true">
      <forms:option value="">-- Choose VCS root to attach --</forms:option>
      <c:if test="${canAttachLocalRoots}">
        <c:forEach items="${attachableLocalRoots}" var="vcsRoot">
          <forms:option value="${vcsRoot.id}"><c:out value="${vcsRoot.name}"/></forms:option>
        </c:forEach>
      </c:if>
      <forms:option value="">-- Shared VCS roots --</forms:option>
      <c:forEach items="${attachableSharedRoots}" var="vcsRoot">
        <forms:option value="${vcsRoot.id}"><c:out value="${vcsRoot.name}"/></forms:option>
      </c:forEach>
    </forms:select>
    <forms:saving id="attachProgress" className="progressRingInline" style="padding-top: 0.3em;"/>
    <div class="error" id="errorVcsRootId" style="margin-left: 0;"></div>
    <c:if test="${not canAttachLocalRoots and not empty attachableLocalRoots}">
    <div class="smallNoteAttention">
      This template is used from build configurations of another project.
      <br/>Only shared VCS roots can be attached to this template.
    </div>
    </c:if>
  </td>
  </tr>
  </c:if>
</table>

<br/>

<c:set var="vcsRoots" value="${vcsRootsBean.vcsRoots}"/>

<bs:messages key="vcsSettingsChanged"/>
<table class="runnerFormTable">
<l:settingsGroup title="Checkout Settings">
  <tr>
   <th class="option"><label for="checkoutType">VCS checkout mode: <bs:help file="VCS+Checkout+Mode" /></label></th>
   <td class="selector"><forms:select id="checkoutType" name="checkoutType" style="width: 340px;" onchange="BS.EditVcsRootsForm.updateCheckoutDirectoryWarning()" enableFilter="${true}">
     <c:forEach items="${vcsRootsBean.availableCheckoutTypes}" var="typeName">
       <forms:option selected="${vcsRootsBean.checkoutType == typeName}" value="${typeName}"><c:out value="${vcsRootsBean.checkoutTypeDescription[typeName]}"/></forms:option>
     </c:forEach>
    </forms:select>
     <c:if test="${vcsRootsBean.showInefficientCheckoutRulesWarning}">
       <div class="attentionComment">
         Exclude checkout rules are specified while "checkout on agent" mode is used. Applying these checkout rules requires additional overhead and is inefficient.
       </div>
     </c:if>
     <c:if test="${vcsRootsBean.showIncludeRulesIntersectWarning}">
       <div class="attentionComment">
         The paths specified in the checkout rules of one VCS root conflict (intersect) with checkout rules of another VCS root. This may cause problems in "checkout on agent" mode.
       </div>
     </c:if>
   </td>
  </tr>
  <tr>
   <th class="option"><label for="checkoutDir">Checkout directory: <bs:help file="Build+Checkout+Directory"/></label></th>
   <td class="selector">
     <div>
       <forms:select name="checkoutDirectorySharing" style="width: 340px;" onchange="BS.EditVcsRootsForm.updateCheckoutSharing()" enableFilter="${true}">
         <forms:option value="shared">Auto (recommended)</forms:option>
         <forms:option value="custom" selected="${not empty vcsRootsBean.checkoutDir}">Custom path</forms:option>
       </forms:select>
     </div>
     <div id="autoCheckout" class="smallNote" style='${not empty vcsRootsBean.checkoutDir ? 'display: none' : ''}'>
       With this selection all build configurations with the same VCS settings will use the same checkout directory.
     </div>
     <div id="customCheckout" style='${empty vcsRootsBean.checkoutDir ? 'display: none' : ''}'>
       <forms:textField name="checkoutDir" style="width:340px;" maxlength="512" value="${vcsRootsBean.checkoutDir}" className="buildTypeParams"/>
       <div class="smallNote" style="margin-left: 0;">Relative path to agent work directory or absolute path. Restrictions apply
                                                      <bs:help file="Build+Checkout+Directory#BuildCheckoutDirectory-Customcheckoutdirectory"/></div>
       <div id="deletionWarning" class="smallNoteAttention" style="display:none;">This directory might be cleaned by TeamCity before the build</div>
     </div>
     <c:if test="${not createMode and not buildForm.template}">
       <c:set var="sameDirBuildTypes" value="${buildForm.vcsRootsBean.checkoutDirBean.buildTypesWithSameCheckoutDirAndDifferentVcsSettings}"/>
       <c:if test="${not empty sameDirBuildTypes}">
         <div><bs:icon icon="../attentionComment.gif"/> The following build configurations have the same checkout directory but different VCS settings,
           this may lead to unnecessary clean checkout on agent:</div>
         <c:forEach var="bt" items="${sameDirBuildTypes}">
           <div style="padding-left: 20px">
           <authz:authorize projectId="${bt.projectId}" allPermissions="EDIT_PROJECT">
             <jsp:attribute name="ifAccessGranted">
               <admin:editBuildTypeLink step="vcsRoots" buildTypeId="${bt.buildTypeId}" title=""><c:out value="${bt.fullName}"/></admin:editBuildTypeLink>
             </jsp:attribute>
             <jsp:attribute name="ifAccessDenied">
               <bs:buildTypeLink buildType="${bt}"><c:out value="${bt.fullName}"/></bs:buildTypeLink>
             </jsp:attribute>
           </authz:authorize>
           </div>
         </c:forEach>
       </c:if>
     </c:if>
   </td>
  </tr>
  <tr>
   <th class="option"><label for="cleanBuild">Clean all files before build: </label></th>
   <td class="selector">
     <forms:checkbox name="cleanBuild" checked="${vcsRootsBean.cleanBuild}" onclick="BS.EditVcsRootsForm.updateCheckoutDirectoryWarning()"/>
   </td>
  </tr>
</l:settingsGroup>

<c:if test="${not empty vcsRootsBean.vcsRoots}">
<l:settingsGroup title="VCS Labeling">
  <tr>
    <th class="option ${empty vcsRoots ? 'noBorder' : ''}"><label for="checkoutType">VCS labeling mode: <bs:help file="VCS+Labeling"/></label></th>

   <td class="selector ${empty vcsRoots ? 'noBorder' : ''}">
     <c:set var="noneMode" value="${vcsRootsBean.labelingTypeStr eq 'NONE'}"/>
     <c:set var="allMode" value="${vcsRootsBean.labelingTypeStr eq 'ALWAYS'}"/>
     <c:set var="successfulMode" value="${vcsRootsBean.labelingTypeStr eq 'SUCCESSFUL_ONLY'}"/>

     <c:set var="onclick">
       $('labelingPattern').disabled = true;
       <c:forEach items="${vcsRootsBean.vcsRoots}" var="root">
         $('labelingRoots[${root.vcsRoot.id}]').disabled = true;
       </c:forEach>
       BS.VisibilityHandlers.updateVisibility('labelingPattern');
     </c:set>
     <input type="radio" name="labelingTypeStr" value="NONE"
                    id="labeling-type-none"
                    <c:if test="${noneMode}">checked="true"</c:if> onclick="${onclick}"/> <label for="labeling-type-none">Do not label</label>
      <br />
     <c:set var="onclick">
       $('labelingPattern').disabled = false;
       $('labelingPattern').focus();
       <c:forEach items="${vcsRootsBean.vcsRoots}" var="root">
         <c:if test="${vcsRootsBean.supportingLabelingVcsRoots[root.vcsRoot.id]}">
           $('labelingRoots[${root.vcsRoot.id}]').disabled = false;
         </c:if>
       </c:forEach>
       BS.VisibilityHandlers.updateVisibility('labelingPattern');
     </c:set>
     <input type="radio" name="labelingTypeStr" value="SUCCESSFUL_ONLY"
                    id="labeling-type-successful"
                    <c:if test="${successfulMode}">checked="true"</c:if> onclick="${onclick}"/> <label for="labeling-type-successful">Successful only</label>
     <br />
     <c:set var="onclick">
       $('labelingPattern').disabled = false;
       $('labelingPattern').focus();
       <c:forEach items="${vcsRootsBean.vcsRoots}" var="root">
         <c:if test="${vcsRootsBean.supportingLabelingVcsRoots[root.vcsRoot.id]}">
           $('labelingRoots[${root.vcsRoot.id}]').disabled = false;
         </c:if>
       </c:forEach>
       BS.VisibilityHandlers.updateVisibility('labelingPattern');
     </c:set>
      <input type="radio" name="labelingTypeStr" value="ALWAYS"
                    id="labeling-type-always"
                    <c:if test="${allMode}">checked="true"</c:if> onclick="${onclick}"/> <label for="labeling-type-always">Always</label>
      <br/>
      <label for="labelingPattern">Labeling pattern: </label>
      <forms:textField name="labelingPattern" value="${vcsRootsBean.labelingPattern}" disabled="${noneMode}" className="buildTypeParams" style="width: 18em;"/>
   </td>
  </tr>

  <tr>
    <th class="option noBorder">
      <label>Choose VCS roots to label:</label>
    </th>
    <td class="noBorder">
      <c:forEach items="${vcsRootsBean.vcsRoots}" var="root">
        <c:set var="propName" value="labelingRoots[${root.vcsRoot.id}]"/>
        <c:set var="inherited" value="${not buildForm.template and not buildForm.createMode and not vcsRootsBean.detacheableVcsRoots[root.vcsRoot.id]}"/>
        <forms:checkbox checked="${vcsRootsBean.labelingRoots[root.vcsRoot.id]}" name="${propName}"
          id="${propName}" disabled="${not vcsRootsBean.supportingLabelingVcsRoots[root.vcsRoot.id] or noneMode or inherited}" className="labelCheck"/>
        <c:if test="${not vcsRootsBean.supportingLabelingVcsRoots[root.vcsRoot.id]}">
          <label class="smallNote" style="margin-left:0"> (labeling not supported)</label>
        </c:if>
          <label for="${propName}"><c:out value="${root.vcsRoot.name}"/> <c:if test="${inherited}"><span class="inheritedParam">(inherited)</span></c:if></label>
        <br/>
      </c:forEach>
    </td>
  </tr>
</l:settingsGroup>
</c:if>

<l:settingsGroup title="Display settings">
  <tr>
    <th>
      Display options:
    </th>
    <td>
      <forms:checkbox name="showDependenciesChanges" checked="${vcsRootsBean.showDependenciesChanges}"/>
      <label for="showDependenciesChanges">Show changes from snapshot dependencies</label>
    </td>
  </tr>
</l:settingsGroup>
  
</table>

<c:if test="${not createMode}">
  <c:if test="${not buildForm.templateBased or vcsRootsBean.labelingPossible}">
  <div class="saveButtonsBlock">
    <forms:cancel cameFromSupport="${buildForm.cameFromSupport}"/>
    <forms:submit label="Save"/>
    <forms:saving id="applyVcsSettingsProgress"/>
    <input type="hidden" value="${buildForm.numberOfSettingsChangesEvents}" name="numberOfSettingsChangesEvents"/>
  </div>
  </c:if>
</c:if>
