<%--@elvariable id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@attribute name="sourceBuildType" required="true" type="jetbrains.buildServer.serverSide.SBuildType" %>
<%@attribute name="newName" required="true" %>
<%@attribute name="targetProjectId" required="false" %>
<bs:professionalLimited shouldBePositive="${serverSummary.buildConfigurationsLeft}">
  <jsp:attribute name="disabled">
    <span class="commentText" title="Cannot copy due to limit for number of build configurations">copy</span>
  </jsp:attribute>
  <jsp:body>
    <a href="#" title="Copy build configuration"
       onclick="BS.CopyBuildTypeForm.showDialog('${sourceBuildType.buildTypeId}', '${sourceBuildType.projectId}', '<bs:escapeForJs text="${newName}" forHTMLAttribute="true"/>', '${targetProjectId}'); return false"><jsp:doBody/></a>
  </jsp:body>
</bs:professionalLimited>
