<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz"%>
<authz:authorize allPermissions="CHANGE_SERVER_SETTINGS">
  <div class="smallNote" style="margin-left: 0; padding-bottom: 0.5em;">Navigate to <a href="<c:url value='/admin/admin.html?item=serverConfigGeneral'/>">Server Configuration page</a> to enable per-project permissions.</div>
</authz:authorize>
