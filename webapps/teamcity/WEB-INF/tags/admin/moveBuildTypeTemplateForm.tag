<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ attribute name="editableProjects" required="true" type="java.util.Collection" %>

<c:url value='/admin/moveTemplate.html' var="moveAction"/>
<bs:modalDialog formId="moveTemplateForm"
                title="Move Build Configuration Template"
                action="${moveAction}"
                closeCommand="BS.MoveTemplateForm.cancelDialog()"
                saveCommand="BS.MoveTemplateForm.submitMove()">

  <label for="moveTemplateToProjectId" class="tableLabel">Move to project:</label>
  <select id="moveTemplateToProjectId" name="projectId" style="width: 17em;"></select>
  <span class="error" id="error_moveTemplateForm_projectId"></span>
  <script type="text/javascript">
    BS.MoveTemplateForm._projects = new Array();
    <c:forEach items="${editableProjects}" var="project" varStatus="pos">
    BS.MoveTemplateForm._projects[${pos.index}] = { id: '${project.projectId}', name: '<bs:escapeForJs text="${project.extendedName}"/>' };
    </c:forEach>
  </script>
  <!--</p>-->

  <div id="moveTemplate_vcsRootsSection" class="vcsRootsSection" style="display: none;">
  <authz:authorize allPermissions="CHANGE_GLOBAL_VCS_ROOT">
    <jsp:attribute name="ifAccessGranted">
      <div class="attentionComment">There is at least one non-shared VCS root attached to this template. This VCS root will be shared after the move.</div>
      <script type="text/javascript">
        $('moveTemplate_vcsRootsSection').onshow = function() {
          Form.Element.enable(BS.MoveTemplateForm.formElement().moveTemplate);
        };
      </script>
    </jsp:attribute>
    <jsp:attribute name="ifAccessDenied">
      <div class="attentionComment">There is at least one non-shared VCS root attached to this template. To move this template attached VCS roots need to be shared but you do not have enough permissions to share them. Move operation is not possible.</div>
      <script type="text/javascript">
        $('moveTemplate_vcsRootsSection').onshow = function() {
          Form.Element.disable(BS.MoveTemplateForm.formElement().moveTemplate);
        };
      </script>
    </jsp:attribute>
  </authz:authorize>
  </div>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.MoveTemplateForm.cancelDialog()" showdiscardchangesmessage="false"/>
    <forms:submit name="moveTemplate" label="Move"/>
    <forms:saving id="moveTemplateProgress"/>
  </div>

  <input type="hidden" name="templateId" id="templateId" value=""/>
  <input type="hidden" name="sourceProjectId" id="sourceProjectId" value=""/>

</bs:modalDialog>
