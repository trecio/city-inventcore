<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ attribute name="formAction" required="true" %>

<bs:modalDialog formId="editCheckoutRulesForm"
                title=""
                action="${formAction}"
                closeCommand="BS.EditCheckoutRulesForm.cancelDialog()"
                saveCommand="BS.EditCheckoutRulesForm.submit()">

  <h2 id="vcsRootName" class="noBorder"></h2>
  <textarea rows="6" cols="58" name="checkoutRules" id="checkoutRules" class="buildTypeParams" wrap="off" style="width: 37em;"></textarea>
  <span class="error" id="errorCheckoutRules" style="margin-left: 0;"></span>

  <div class="smallNote" style="margin-left: 0;">
    Newline-delimited set or rules in the form of +|-:VCSPath[=>AgentPath]<bs:help file="VCS+Checkout+Rules"/><br/>
    <!--By default, all is included<br/>-->
    e.g. use <strong>-:.</strong> to exclude all, or <strong>-:repository/path</strong> to exclude only the path from checkout<br/>
    or <strong>+:repository/path => another/path</strong> to map to different path.<br/>
    Note: checkout rules can only be set to directories, files are <strong>not</strong> supported.
  </div>

  <div class="popupSaveButtonsBlock" id="editCheckoutRulesFormSaveBlock">
    <forms:cancel onclick="BS.EditCheckoutRulesForm.cancelDialog()" showdiscardchangesmessage="false"/>
    <forms:submit name="saveCheckoutRules" label="Save"/>
    <forms:saving id="saveRulesProgress"/>
  </div>

  <input type="hidden" name="vcsRootId" value=""/>

</bs:modalDialog>
