<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="l" tagdir="/WEB-INF/tags/layout"
%><%@ attribute name="projectForm" required="true" rtexprvalue="true" type="jetbrains.buildServer.controllers.admin.projects.BaseProjectForm"

%><bs:messages key="projectUpdated"
/><bs:messages key="projectCreated"
/><table class="runnerFormTable">
  <tr>
    <th><label for="name">Name: <l:star/></label></th>
    <td>
      <forms:textField name="name" className="longField" maxlength="80" value="${projectForm.name}"/>
      <span class="error" id="errorName"></span>
    </td>
  </tr>
  <tr>
    <th class="noBorder"><label for="description">Description:</label></th>
    <td class="noBorder"><forms:textField name="description" className="longField" maxlength="256" value="${projectForm.description}"/></td>
  </tr>
  <input type="hidden" id="submitProject" name="submitProject" value="store"/>
</table>
