<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<c:url value='/admin/copyProject.html' var="copyAction"/>

<bs:modalDialog formId="copyProjectForm"
                title="Copy Project"
                action="${copyAction}"
                closeCommand="BS.CopyProjectForm.cancelDialog()"
                saveCommand="BS.CopyProjectForm.submitCopy()">
  <label for="newProjectName" class="tableLabel">New project name: <l:star/></label>
  <forms:textField id="newProjectName" name="newName" value="" style="width: 22em;"/>
  <span class="error" id="error_newProjectName" style="margin-left: 11em;"></span>

  <authz:authorize allPermissions="CHANGE_GLOBAL_VCS_ROOT">
    <jsp:attribute name="ifAccessGranted">
      <div id="projectVcsRootsSection" class="vcsRootsSection" style="display: none;">
      <div class="attentionComment">There is at least one non-shared VCS root in this project. What do you want to do with these VCS roots?</div>
      <ul>
        <li>
          <forms:radioButton name="copyVcsRoots" id="copyProjectVcsRoots1" value="true" checked="true"/>
          <label for="copyProjectVcsRoots1">Use copies of these VCS roots in new build configurations</label>
        </li>
        <li>
          <forms:radioButton name="copyVcsRoots" id="copyProjectVcsRoots2" value="false"/>
          <label for="copyProjectVcsRoots2">Share these VCS roots</label>
        </li>
      </ul>
      </div>
    </jsp:attribute>
  </authz:authorize>

  <div id="projectTemplatesSection" class="templatesSection" style="display: none;">
    <div class="attentionComment">This project contains templates. What do you want to do with the templates?</div>
    <ul>
      <li>
        <forms:radioButton name="copyTemplates" id="copyProjectTemplates1" value="true" checked="true"/>
        <label for="copyProjectTemplates1">Create copies of the templates in the new project</label>
      </li>
      <li>
        <forms:radioButton name="copyTemplates" id="copyProjectTemplates2" value="false"/>
        <label for="copyProjectTemplates2">Re-use templates from the original project</label>
      </li>
    </ul>
  </div>

  <div style="padding-top: 0.5em;">
    <forms:checkbox name="copyAssociatedSettings" id="projectCopyAssociatedSettings" checked="true"/>
    <label for="projectCopyAssociatedSettings">Copy project-associated user, agent and other settings</label>
  </div>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.CopyProjectForm.cancelDialog()"/>
    <forms:submit label="Copy"/>
    <forms:saving id="copyProjectProgress"/>
  </div>

  <input type="hidden" name="projectId" value=""/>
</bs:modalDialog>