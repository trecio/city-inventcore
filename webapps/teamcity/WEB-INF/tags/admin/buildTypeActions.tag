<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout"
  %><%@attribute name="buildType" required="true" type="jetbrains.buildServer.serverSide.SBuildType"
  %><%@attribute name="editableProjects" required="true" type="java.util.Collection"
  %>
<bs:simplePopup controlId="btActions${buildType.buildTypeId}" linkOpensPopup="true"
                popup_options="shift: {x: -150, y: 20}, className: 'quickLinksMenuPopup'">
  <jsp:attribute name="content">
    <div>
      <ul class="menuList">
        <bs:professionalLimited shouldBePositive="${serverSummary.buildConfigurationsLeft}">
          <jsp:body>
            <l:li>
              <admin:copyBuildTypeLink sourceBuildType="${buildType}" newName="${buildType.name}" targetProjectId="${buildType.projectId}">Copy build configuration</admin:copyBuildTypeLink>
            </l:li>
          </jsp:body>
        </bs:professionalLimited>
        <c:if test="${fn:length(editableProjects) > 1}">
        <l:li>
          <admin:moveBuildTypeLink sourceBuildType="${buildType}">Move build configuration</admin:moveBuildTypeLink>
        </l:li>
        </c:if>
        <l:li>
          <admin:deleteBuildTypeLink buildTypeId="${buildType.buildTypeId}">Delete build configuration</admin:deleteBuildTypeLink>
        </l:li>
      </ul>
    </div>

  </jsp:attribute>
  <jsp:body>more</jsp:body>
</bs:simplePopup>
