<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ attribute name="editableProjects" required="true" type="java.util.Collection" %>
<c:url value='/admin/copyBuildType.html' var="copyAction"/>

<bs:modalDialog formId="copyBuildTypeForm"
                title="Copy Build Configuration"
                action="${copyAction}"
                closeCommand="BS.CopyBuildTypeForm.cancelDialog()"
                saveCommand="BS.CopyBuildTypeForm.submitCopy()">

  <label for="copyProjectId" class="tableLabel">Copy to project:</label>
  <forms:select id="copyProjectId" name="projectId" style="width: 22em;" enableFilter="true">
    <c:forEach items="${editableProjects}" var="project">
      <forms:option value="${project.projectId}"><c:out value="${project.extendedName}"/></forms:option>
    </c:forEach>
  </forms:select>
  <span class="error" id="error_projectId"></span>
  <script type="text/javascript">
    $('copyBuildTypeForm').projectId.selectProject = function(projectId) {
      for (var i = 0; i < this.options.length; i++) {
        if (this.options[i].value == projectId) {
          this.selectedIndex = i;
          break;
        }
      }
      BS.jQueryDropdown('#' + this.id).ufd("changeOptions");
    };

    $('copyBuildTypeForm').setShowVcsRootsSection = function(show) {
      $('copyBuildTypeForm')._showVcsRootsSection = show;
    };

    $('copyBuildTypeForm').projectId.onchange = function() {
      var selectedIdx = this.selectedIndex;
      if (selectedIdx == -1) {
        selectedIdx = 0;
      }
      var selectedProjectId = this.options[selectedIdx].value;
      if (selectedProjectId != this.form.sourceProjectId.value && $('copyBuildTypeForm')._showVcsRootsSection) {
        BS.Util.show('vcsRootsSection');
        <c:if test="${not afn:permissionGrantedGlobally('CHANGE_GLOBAL_VCS_ROOT')}">
        $('copyBuildTypeButton').disable();
        </c:if>
      } else {
        BS.Util.hide('vcsRootsSection');
        $('copyBuildTypeButton').enable();
      }
    }
  </script>

  <p>
    <label for="newBuildTypeName" class="tableLabel">New configuration name: <l:star/></label>
    <forms:textField id="newBuildTypeName" name="newName" value="" style="width: 22em;"/>
    <span class="error" id="error_newName"></span>
  </p>

  <div style="padding-top: 0.5em;">
    <forms:checkbox name="copyAssociatedSettings" id="btCopyAssociatedSettings" checked="true"/> <label for="btCopyAssociatedSettings">Copy build configuration-associated user, agent and other settings</label>
  </div>

  <admin:vcsRootsSection id="vcsRootsSection"
                         message="You have choosen to copy to different project, while there is
                                  at least one non-shared VCS root in this build configuration.
                                  What do you want to do with these VCS roots?"
                         messageIfSharingNotPossible="There is at least one non-shared VCS root in this build configuration.
                                                      To copy this build configuration attached VCS roots need to be shared but you do not have enough permissions to share them.
                                                      Move operation is not possible."
      />


  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.CopyBuildTypeForm.cancelDialog()" showdiscardchangesmessage="false"/>
    <forms:submit name="copyBuildType" id="copyBuildTypeButton" label="Copy"/>
    <forms:saving id="copyBuildTypeProgress"/>
  </div>

  <input type="hidden" name="buildTypeId" id="buildTypeId" value=""/>
  <input type="hidden" name="sourceProjectId" id="sourceProjectId" value=""/>

</bs:modalDialog>
