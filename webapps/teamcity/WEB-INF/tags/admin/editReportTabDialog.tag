<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="afn" uri="/WEB-INF/functions/authz" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %><%@
    taglib prefix="l" tagdir="/WEB-INF/tags/layout" %><%@
    attribute name="title" type="java.lang.String" required="true" %><%@
    attribute name="isProjectTab" type="java.lang.Boolean" required="false" %><%@
    attribute name="project" type="jetbrains.buildServer.serverSide.SProject" required="false"

%><c:url var="action" value="/admin/action.html"
/><bs:modalDialog formId="editReportTab" title="${title}" action="${action}"
                  saveCommand="BS.ReportTabSettingsDialog.save();"
                  closeCommand="BS.ReportTabSettingsDialog.close();">
  <table class="editTabSettingsTable">
    <tr>
     <th><label for="tabTitle">Tab Title: <l:star/></label></th>
     <td>
       <forms:textField className="longField" name="tabTitle"/>
       <span class="error" id="error_tabTitle"></span>
     </td>
    </tr>
    <c:if test="${isProjectTab}">
      <tr>
        <th><label for="buildTypeId">Get artifacts from: <l:star/></label></th>
        <td>
          <forms:select name="buildTypeId" id="buildTypeId" className="longField" enableFilter="true">
            <c:forEach items="${project.buildTypes}" var="buildType">
              <forms:option value="${buildType.buildTypeId}"><c:out value="${buildType.name}"/></forms:option>
            </c:forEach>
          </forms:select>
        </td>
      </tr>
      <tr>
        <th></th>
          <td>
            <select id="revisionRules" class="longField"
                    onchange="BS.ReportTabSettingsDialog.updateFieldVisibility(); BS.VisibilityHandlers.updateVisibility('editReportTab');">
              <forms:buildAnchorOptions/>
            </select>
            <div id="buildNumberField" class="buildNumberPattern">
              <label for="buildNumberPattern">Build number:</label>
              <forms:textField name="buildNumberPattern" size="12" maxlength="100" style="width: 18em"/><bs:help file="Build+Number"/>
              <span class="error" id="error_buildNumberPattern" style="margin-left: 10em;"></span>
            </div>
            <div id="buildTagField" class="buildNumberPattern">
              <label for="buildTag">Build tag:</label>
              <forms:textField name="buildTag" size="12" maxlength="100" style="width: 20em" />
              <div id="buildTagList">Available tags: <span id="buildTagListSpan"></span></div>
              <span class="error" id="error_buildTag" style="margin-left: 10em;"></span>
            </div>
          </td>
      </tr>
    </c:if>
    <tr>
       <th><label for="tabStartPage">Start page:</label></th>
       <td>
         <forms:textField className="longField" name="tabStartPage"/>
         <div class="smallNote" style="margin-left: 0;">Relative path from the root of build's artifacts to a start page of the generated report.</div>
         <div class="smallNote" style="margin-left: 0;">To reference a file from an archive, use the <b>[path-to-archive]![relative-path]</b> syntax.</div>
       </td>
    </tr>
  </table>
  <div class="saveButtonsBlock">
    <forms:cancel onclick="BS.ReportTabSettingsDialog.close()"/>
    <c:if test="${isProjectTab}">
        <input type="hidden" name="projectId" value="${project.projectId}"/>
    </c:if>
    <forms:submit name="editReportTab" label="Save"/>
    <forms:saving id="saving_reportTab"/>
  </div>
</bs:modalDialog>
<c:if test="${isProjectTab}">
  <script type="text/javascript">
    BS.ReportTabSettingsDialog.BuildFinder.shouldAttach();
  </script>
</c:if>