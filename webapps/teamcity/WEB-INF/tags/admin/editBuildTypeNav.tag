<%--@elvariable id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>

<%@ attribute name="buildTypeForm" required="true" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm"%>
<%@ attribute name="selectedLink" required="false" %>
<%@ attribute name="style" required="false" %>

<div class="configurationSection" style="${style}">
  <h2>Configuration Steps</h2>

  <admin:editBuildTypeNavSteps settings="${buildTypeForm.settings}"/>
  <c:forEach var="configStep" items="${buildConfigSteps}">
    <c:choose>
      <c:when test="${selectedLink == configStep.stepId}">
        <c:set var="stepContent">
          <tr>
            <td class="number">${configStep.number}</td>
            <td class="text">${configStep.title}</td>
          </tr>
        </c:set>
        <table class="selectedStep">${stepContent}</table>
      </c:when>

      <c:when test="${buildTypeForm.template}">
        <c:set var="url"><admin:editTemplateLink step="${configStep.stepId}" style="text-decoration: none;" templateId="${buildTypeForm.settings.id}" title="${configStep.title}" withoutLink="true"/></c:set>
        <c:set var="stepContent">
          <tr>
            <td class="number">${configStep.number}</td>
            <td class="text"><a class="stepLink" href="${url}">${configStep.title}</a></td>
          </tr>
        </c:set>
        <table class="step" onclick="BS.openUrl(event, '${url}')">${stepContent}</table>
      </c:when>

      <c:when test="${not buildTypeForm.template}">
        <c:set var="url"><admin:editBuildTypeLink step="${configStep.stepId}" style="text-decoration: none;" buildTypeId="${buildTypeForm.settingsBuildType.buildTypeId}" title="${configStep.title}" withoutLink="true"/></c:set>
        <c:set var="stepContent">
          <tr>
            <td class="number">${configStep.number}</td>
            <td class="text"><a class="stepLink" href="${url}">${configStep.title}</a></td>
          </tr>
        </c:set>
        <table class="step" onclick="BS.openUrl(event, '${url}')">${stepContent}</table>
      </c:when>
    </c:choose>
  </c:forEach>
</div>
<script type="text/javascript">
  $j('table.step').live("mouseover", function() {
    $j(this).addClass('highlightedStep');
  });

  $j('table.step').live("mouseout", function() {
    $j(this).removeClass('highlightedStep');
  });
</script>

<c:if test="${buildTypeForm.template}">
  <c:set var="tpl" value="${buildTypeForm.settings}"/>
  <table class="usefulLinks" style="width: 100%;">
    <tr>
      <td>
      Used in <strong>${tpl.numberOfUsages}</strong> build <admin:templateUsagesPopup templateId="${tpl.id}" selectedStep="${selectedLink}">configuration<bs:s val="${tpl.numberOfUsages}"/></admin:templateUsagesPopup>
      </td>
    </tr>
  </table>
</c:if>

<form action="#">
<table class="usefulLinks">
  <c:if test="${not buildTypeForm.template and not buildTypeForm.project.archived}">
    <authz:authorize projectId="${buildTypeForm.project.projectId}" allPermissions="PAUSE_ACTIVATE_BUILD_CONFIGURATION">
      <tr>
        <td class="button">
          <c:choose>
            <c:when test="${buildTypeForm.settingsBuildType.paused}">
              <input type="button" class="btn btn_mini action" value="Activate" onclick="<bs:_pauseBuildTypeLinkOnClick buildType="${buildTypeForm.settingsBuildType}" pause="false"/>"/>
            </c:when>
            <c:otherwise>
              <input type="button" class="btn btn_mini action" value="Pause" onclick="<bs:_pauseBuildTypeLinkOnClick buildType="${buildTypeForm.settingsBuildType}" pause="true"/>"/>
            </c:otherwise>
          </c:choose>
        </td>
        <td>
          <c:choose>
            <c:when test="${buildTypeForm.settingsBuildType.paused}">
              Activate this configuration
            </c:when>
            <c:otherwise>
              Pause this configuration
            </c:otherwise>
          </c:choose>
        </td>
      </tr>
    </authz:authorize>
  </c:if>
  <c:if test="${not buildTypeForm.template}">
  <bs:professionalLimited shouldBePositive="${serverSummary.buildConfigurationsLeft}">
    <tr>
      <td class="button">
        <input type="button" class="btn btn_mini action" value="Copy" onclick="BS.CopyBuildTypeForm.showDialog('${buildTypeForm.settingsBuildType.buildTypeId}', '${buildTypeForm.project.projectId}', '<bs:escapeForJs text="${buildTypeForm.name}" forHTMLAttribute="true"/>', '${buildTypeForm.project.projectId}')"/>
      </td>
      <td>Copy this configuration</td>
    </tr>
  </bs:professionalLimited>
  <c:if test="${fn:length(buildTypeForm.editableProjects) > 1}">
  <tr>
    <td class="button">
      <input type="button" class="btn btn_mini action" value="Move" onclick="BS.MoveBuildTypeForm.showDialog('${buildTypeForm.settingsBuildType.buildTypeId}', '${buildTypeForm.project.projectId}')"/>
    </td>
    <td>Move this configuration</td>
  </tr>
  </c:if>
  <tr>
    <td class="button">
      <input type="button" class="btn btn_mini action" value="Delete" onclick="BS.AdminActions.deleteBuildType('${buildTypeForm.settingsBuildType.buildTypeId}')"/>
    </td>
    <td>
      Delete this configuration and all related data
    </td>
  </tr>
  </c:if>
  <c:if test="${buildTypeForm.template}">
    <bs:professionalLimited shouldBePositive="${serverSummary.buildConfigurationsLeft}">
    <tr>
      <td class="button" colspan="2">
        <input type="button" class="btn btn_mini action" value="Create Build Configuration" onclick="BS.CreateFromTemplateAction.showDialog('${buildTypeForm.settings.id}')"/>
      </td>
    </tr>
    </bs:professionalLimited>
    <c:if test="${fn:length(buildTypeForm.editableProjects) > 1}">
    <tr>
      <td class="button">
        <input type="button" class="btn btn_mini action" value="Move" onclick="BS.MoveTemplateForm.showDialog('${buildTypeForm.settings.id}', '${buildTypeForm.project.projectId}')"/>
      </td>
      <td>Move this template</td>
    </tr>
    </c:if>
    <tr>
      <td class="button">
        <input type="button" class="btn btn_mini action" value="Delete" onclick="BS.AdminActions.deleteBuildTypeTemplate('${buildTypeForm.settings.id}')"/>
      </td>
      <td>
        Delete this template
      </td>
    </tr>
  </c:if>
  <c:if test="${not buildTypeForm.template}">
    <c:if test="${not buildTypeForm.templateBased and (fn:length(buildTypeForm.currentProjectTemplates) + fn:length(buildTypeForm.otherProjectsTemplates)) > 0}">
      <tr>
        <td class="button" colspan="2">
          <input type="button" class="btn btn_mini action" value="Associate with Template" onclick="BS.AttachDetachTemplateAction.showDialog('${buildTypeForm.settingsBuildType.buildTypeId}')"/>
        </td>
      </tr>
    </c:if>
    <c:if test="${not buildTypeForm.templateBased}">
      <tr>
        <td class="button" colspan="2">
          <input type="button" class="btn btn_mini action" value="Extract Template" onclick="BS.ExtractTemplateAction.showDialog('${buildTypeForm.settingsBuildType.buildTypeId}')"/>
        </td>
      </tr>
    </c:if>
    <c:if test="${buildTypeForm.templateBased}">
      <tr>
        <td class="button" colspan="2">
          <input type="button" class="btn btn_mini action" value="Detach from Template" onclick="BS.AttachDetachTemplateAction.detachFromTemplate('${buildTypeForm.settingsBuildType.buildTypeId}')"/>
        </td>
      </tr>
    </c:if>
  </c:if>
</table>

<admin:configModificationInfo auditLogAction="${buildTypeForm.settings.lastConfigModificationAction}"/>
</form>
<c:if test="${not buildTypeForm.template}">
  <bs:pauseBuildTypeDialog/>
  <admin:copyBuildTypeForm editableProjects="${buildTypeForm.editableProjects}"/>
  <admin:moveBuildTypeForm editableProjects="${buildTypeForm.editableProjects}"/>
  <admin:attachTemplateDialog buildType="${buildTypeForm.settingsBuildType}" currentProjectTemplates="${buildTypeForm.currentProjectTemplates}" otherProjectsTemplates="${buildTypeForm.otherProjectsTemplates}"/>
  <admin:extractTemplate project="${buildTypeForm.project}"/>
</c:if>
<c:if test="${buildTypeForm.template}">
  <admin:moveBuildTypeTemplateForm editableProjects="${buildTypeForm.editableProjects}"/>
  <admin:createBuildTypeFromTemplate project="${buildTypeForm.project}" showTemplatesChooser="false"/>
</c:if>
