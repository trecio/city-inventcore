<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
%><%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ attribute name="template" type="jetbrains.buildServer.serverSide.BuildTypeTemplate" required="true"
%><c:set var="title">Click to edit &quot;<c:out value="${template.name}"/>&quot; build configuration template</c:set
><bs:projectLink project="${template.parentProject}"><c:out value="${template.parentProject.name}"
/></bs:projectLink> :: <admin:editTemplateLink templateId="${template.id}" title="${title}"
><c:out value="${template.name}"/></admin:editTemplateLink>