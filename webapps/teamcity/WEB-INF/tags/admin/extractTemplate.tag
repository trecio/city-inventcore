<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ attribute name="project" required="true" type="jetbrains.buildServer.serverSide.SProject" %>

<c:url value='/admin/action.html' var="actionUrl"/>
<bs:modalDialog formId="extractTemplateForm"
                title="Extract Template"
                action="${actionUrl}"
                closeCommand="BS.ExtractTemplateAction.cancelDialog()"
                saveCommand="BS.ExtractTemplateAction.submit()">
  <span class="error" id="error_extractFailed" style="margin-left: 0;"></span>

  <table class="runnerFormTable">
    <tr>
      <td class="name noBorder"><label for="templateName"><strong>Template name:</strong></label></td>
      <td class="noBorder">
        <forms:textField name="templateName" style="width: 100%"/>
        <span class="error" id="error_templateName" style="margin-left: 0;"></span>

        <div class="smallNote" style="margin-left: 0; border: none;">Note that this build configuration will be attached to the extracted template.</div>
      </td>
    </tr>
  </table>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.ExtractTemplateAction.cancelDialog()" showdiscardchangesmessage="false"/>
    <forms:submit name="extractTemplateButton" label="Extract"/>
    <forms:saving id="extractTemplateProgress"/>
  </div>

  <input type="hidden" name="buildTypeId" value=""/>
  <input type="hidden" name="extractTemplate" value="1"/>

</bs:modalDialog>
