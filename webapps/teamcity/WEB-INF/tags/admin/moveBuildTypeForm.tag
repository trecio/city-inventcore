<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ attribute name="editableProjects" required="true" type="java.util.Collection" %>

<c:url value='/admin/moveBuildType.html' var="moveAction"/>
<bs:modalDialog formId="moveBuildTypeForm"
                title="Move Build Configuration"
                action="${moveAction}"
                closeCommand="BS.MoveBuildTypeForm.cancelDialog()"
                saveCommand="BS.MoveBuildTypeForm.submitMove()">

  <label for="moveToProjectId" class="tableLabel">Move to project:</label>
  <forms:select id="moveToProjectId" name="projectId" style="width: 17em;" enableFilter="true">
    <c:forEach items="${editableProjects}" var="project">
      <forms:option value="${project.projectId}"><c:out value="${project.extendedName}"/></forms:option>
    </c:forEach>
  </forms:select>
  <span class="error" id="error_moveBuildTypeForm_projectId"></span>
  <script type="text/javascript">
    BS.MoveBuildTypeForm._projects = new Array();
    <c:forEach items="${editableProjects}" var="project" varStatus="pos">
    BS.MoveBuildTypeForm._projects[${pos.index}] = { id: '${project.projectId}', name: '<bs:escapeForJs text="${project.extendedName}"/>' };
    </c:forEach>

    $('moveBuildTypeForm').setShowVcsRootsSection = function(show) {
      $('moveBuildTypeForm')._showVcsRootsSection = show;

      if (show) {
        BS.Util.show('mvVcsRootsSection');
        <c:if test="${not afn:permissionGrantedGlobally('CHANGE_GLOBAL_VCS_ROOT')}">
        $('moveBuildTypeButton').disable();
        </c:if>
      } else {
        BS.Util.hide('mvVcsRootsSection');
        $('moveBuildTypeButton').enable();
      }
    };

    $('moveBuildTypeForm').projectId.onchange = function() {
      var selectedIdx = this.selectedIndex;
      if (selectedIdx == -1) {
        selectedIdx = 0;
      }
    }
  </script>

  <admin:vcsRootsSection id="mvVcsRootsSection"
                         message="There is at least one non-shared VCS root in this build configuration.
                                  What do you want to do with these VCS roots?"
                         messageIfSharingNotPossible="There is at least one non-shared VCS root in this build configuration.
                                                      To move this build configuration attached VCS roots need to be shared but you do not have enough permissions to share them.
                                                      Move operation is not possible."
      />

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.MoveBuildTypeForm.cancelDialog()" showdiscardchangesmessage="false"/>
    <forms:submit name="moveBuildType" id="moveBuildTypeButton" label="Move"/>
    <forms:saving id="moveBuildTypeProgress"/>
  </div>

  <input type="hidden" name="buildTypeId" id="buildTypeId" value=""/>
  <input type="hidden" name="sourceProjectId" id="sourceProjectId" value=""/>

</bs:modalDialog>
