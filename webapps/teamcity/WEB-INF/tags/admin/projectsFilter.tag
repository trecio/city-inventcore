<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"
  %><%@attribute name="adminOverviewForm" required="true" type="jetbrains.buildServer.controllers.admin.AdminOverviewForm"
  %><%@attribute name="tab" required="true" %>
<form action="<c:url value='/admin/admin.html'/>" method="get">
  <div class="actionBar">
    <span class="nowrap">
      <label class="firstLabel" for="keyword">Filter: </label>
      <forms:textField name="keyword" value="${adminOverviewForm.keyword}" size="20"/>
    </span>

    <input class="btn btn_mini" type="submit" name="submitFilter" value="Filter"/>
    <c:if test="${not empty adminOverviewForm.keyword}"><c:url value='/admin/admin.html?tab=${tab}' var="resetUrl"/><forms:resetFilter resetUrl="${resetUrl}"/></c:if>
    <input type="hidden" name="tab" value="${tab}"/>
  </div>
</form>
