<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="afn" uri="/WEB-INF/functions/authz" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %><%@
    taglib prefix="l" tagdir="/WEB-INF/tags/layout" %><%@
    attribute name="pageUrl" type="java.lang.String" required="true" %><%@
    attribute name="reportTabsForm" type="jetbrains.buildServer.controllers.admin.reportTabs.EditReportTabsForm" required="true" %><%@
    attribute name="project" type="jetbrains.buildServer.serverSide.SProject" required="false" %><%@
    attribute name="isProjectTabs" type="java.lang.Boolean" required="false"

%><bs:refreshable containerId="reportTabsList" pageUrl="${pageUrl}">
  <bs:messages key="reportTabsUpdated"/>
  <c:set var="reportTabs" value="${reportTabsForm.reportTabs}"/>   
  <c:if test="${not empty reportTabs}">
   <l:tableWithHighlighting className="settings reportTabsTable" mouseovertitle="Click to edit report tab settings" highlightImmediately="true">
    <tr class="reportTabsHead">
      <th>Tab Title</th>
      <c:if test="${isProjectTabs}">
        <th>Get Artifacts From</th>
      </c:if>
      <th colspan="3">Start Page</th>
    </tr>

    <c:forEach var="reportTab" items="${reportTabs}">
      <tr>
         <c:set var="editTab">var target = Event.element(event); if (target.tagName == 'A' && target.innerHTML != 'edit') return true; BS.ReportTabSettingsDialog.show('${reportTab.id}', '<bs:escapeForJs text="${reportTab.title}" forHTMLAttribute="true"/>', '<bs:escapeForJs text="${reportTab.startPage}" forHTMLAttribute="true"/>'
             <c:if test="${isProjectTabs}">, '<bs:escapeForJs text="${reportTab.buildTypeId}"/>',
                 '<bs:escapeForJs text="${reportTab.revisionRule.name}"/>',
                 '<bs:escapeForJs text="${reportTab.revisionRule.name eq 'buildTag'  ?
                                           reportTab.revisionRule.buildTag : reportTab.revisionRule.revision }"/>'</c:if>);</c:set>
         <td class="highlight" onclick="${editTab}">
           <c:choose>
             <c:when test="${isProjectTabs}"><a href="<bs:projectUrl projectId='${reportTab.project.projectId}' tab="${reportTab.tabId}"/>"><c:out value="${reportTab.title}"/></a></c:when>
             <c:otherwise><c:out value="${reportTab.title}"/></c:otherwise>
           </c:choose>
         </td>
      <c:if test="${isProjectTabs}">
          <td class="highlight"  onclick="${editTab}"><bs:buildTypeLink buildType="${reportTab.buildType}"/> (<bs:revisionOfBuildLink revisionRule="${reportTab.revisionRule}" buildTypeId="${reportTab.buildTypeId}"/>)</td>
      </c:if>
         <td class="highlight" onclick="${editTab}"><c:out value="${reportTab.startPage}"/></td>
         <td class="edit highlight" onclick="${editTab}">
             <a href="#" onclick="${editTab}; Event.stop(event)">edit</a>
         </td>
         <td class="edit">
             <a href="#" onclick="BS.ReportTabsForm.deleteTab('${reportTab.id}'<c:if test="${isProjectTabs}">, '<bs:escapeForJs text="${project.projectId}"/>'</c:if>); return false">delete</a>
         </td>
      </tr>
    </c:forEach>
   </l:tableWithHighlighting>
  </c:if>

  <p><forms:addButton onclick="BS.ReportTabSettingsDialog.show(null); return false">Create new report tab</forms:addButton></p>
</bs:refreshable>
