<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>

<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="message" required="true" type="java.lang.String" %>
<%@ attribute name="messageIfSharingNotPossible" required="true" type="java.lang.String" %>

<div id="${id}" class="vcsRootsSection" style="display: none;">
<authz:authorize allPermissions="CHANGE_GLOBAL_VCS_ROOT">
  <jsp:attribute name="ifAccessGranted">
  <div class="attentionComment">${message}</div>
  <ul>
    <li>
      <forms:radioButton id="${id}_copyVcsRoots1" name="copyVcsRoots" value="true" checked="true"/>
      <label for="${id}_copyVcsRoots1" style="width: 16em;">Use copies of these VCS roots</label>
    </li>
    <li>
      <forms:radioButton id="${id}_copyVcsRoots2" name="copyVcsRoots" value="false"/>
      <label for="${id}_copyVcsRoots2" style="width: 16em;">Share these VCS roots</label>
    </li>
  </ul>
  </jsp:attribute>
  <jsp:attribute name="ifAccessDenied">
    <div class="attentionComment">${messageIfSharingNotPossible}</div>
  </jsp:attribute>
</authz:authorize>
</div>
