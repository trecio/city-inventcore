<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %><%@
    taglib prefix="afn" uri="/WEB-INF/functions/authz" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %><%@
    attribute name="project" required="true" type="jetbrains.buildServer.serverSide.SProject" %><%@
    attribute name="showTemplatesChooser" required="true" type="java.lang.Boolean"

%><c:url value='/admin/action.html' var="actionUrl"/>
<bs:modalDialog formId="createBuildTypeFromTemplate"
                title="Create Build Configuration From Template"
                action="${actionUrl}"
                closeCommand="BS.CreateFromTemplateAction.cancelDialog()"
                saveCommand="BS.CreateFromTemplateAction.submit()">
  <table class="runnerFormTable">
    <c:if test="${showTemplatesChooser}">
      <tr>
        <td class="name">
          <label for="template"><strong>Template:</strong></label>
        </td>
        <td>
          <forms:select name="template" enableFilter="false">
            <c:forEach items="${project.buildTypeTemplates}" var="template">
              <forms:option value="${template.id}"><c:out value="${template.name}"/></forms:option>
            </c:forEach>
          </forms:select>
        </td>
      </tr>
    </c:if>
    <tr>
      <td class="name">
        <label for="buildTypeName"><strong>Configuration name:</strong></label>
      </td>
      <td>
        <forms:textField name="buildTypeName"/>
        <span class="error" id="error_buildTypeName"></span>
        <span class="smallNote">Please provide build configuration name.</span>
        <span class="error" id="error_createFailed"></span>
      </td>
    </tr>
  </table>

  <forms:saving id="createFromTemplateParamsProgress"/>
  <div id="createFromTemplateParams"></div>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.CreateFromTemplateAction.cancelDialog()" showdiscardchangesmessage="false"/>
    <forms:submit name="createFromTemplate" label="Create"/>
    <forms:saving id="createFromTemplateProgress"/>
  </div>
  <c:if test="${not showTemplatesChooser}">
    <input type="hidden" name="template" value=""/>
  </c:if>
</bs:modalDialog>
