<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>

<%@attribute name="head_include" fragment="true" %>
<%@attribute name="body_include" fragment="true" %>

<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.CreateBuildTypeForm" scope="request"/>
<c:choose>
  <c:when test="${buildForm.template}">
    <c:set var="pageTitle" value="Create Build Configuration Template" scope="request"/>
  </c:when>
  <c:otherwise>
    <c:set var="pageTitle" value="Create Build Configuration" scope="request"/>
  </c:otherwise>
</c:choose>

<bs:page disableScrollingRestore="true">
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/admin/buildTypeForm.css
      /css/admin/templates.css
      /css/admin/runParams.css
      /css/admin/adminMain.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/bs/editBuildType.js
    </bs:linkScript>
    <script type="text/javascript">
      BS.Navigation.items = [
        {title: "Administration", url: '<c:url value="/admin/admin.html"/>'},
        {title: "<bs:escapeForJs text="${buildForm.project.name}" forHTMLAttribute="true"/> Project", url: '<admin:editProjectLink projectId="${buildForm.project.projectId}" withoutLink="true"/>'},
        {title: "${pageTitle}", selected:true}
      ];
      $j(document).ready(function() {
        BS.AvailableParams.attachPopups('projectId=${buildForm.project.projectId}', 'buildTypeParams', 'textProperty', 'multilineProperty');
        BS.VisibilityHandlers.updateVisibility('mainContent');
      });
    </script>
    <jsp:invoke fragment="head_include"/>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <jsp:invoke fragment="body_include"/>
  </jsp:attribute>
</bs:page>
