<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@attribute name="runner" type="jetbrains.buildServer.controllers.admin.projects.BuildRunnerBean" required="true" %>
<div style="${not runner.enabled ? 'color: #888': ''}">
  <div class="stepName">
    <strong><c:choose
      ><c:when test="${fn:length(runner.buildStepName) == 0}"><c:out value="${runner.runType.displayName}"/></c:when
      ><c:otherwise><bs:makeBreakable text="${runner.buildStepName}" regex=".{60}"/></c:otherwise></c:choose></strong>
    <c:choose>
      <c:when test="${runner.inherited and not runner.enabled}">&nbsp;<span class="inheritedParam">(inherited, disabled)</span></c:when>
      <c:when test="${runner.inherited}">&nbsp;<span class="inheritedParam">(inherited)</span></c:when>
      <c:when test="${not runner.enabled}">&nbsp;<span class="inheritedParam">(disabled)</span></c:when>
    </c:choose>
    <br/>
  </div>
  <div class="stepDescription">
    <c:if test="${fn:length(runner.buildStepName) > 0}"><c:out value="${runner.runType.displayName}"/><br/></c:if>
    <bs:makeMultiline><bs:makeBreakable text="${runner.shortDescription}" regex=".{60}"/></bs:makeMultiline>
  </div>
  <div class="stepDescription">
    Execute: <c:out value="${runner.selectedExecutionPolicy.description}"/>
  </div>
</div>