<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ attribute name="buildForm" required="true" type="jetbrains.buildServer.controllers.admin.projects.BuildTypeForm"%>
<%@ attribute name="showResetCounter" required="true" type="java.lang.Boolean"%>
<tr>
  <th><label for="buildNumberFormat">Build number format:</label><bs:help file="Configuring+General+Settings" anchor="BuildNumberFormat" /><l:star/></th>
  <td><forms:textField name="buildNumberFormat" value="${buildForm.buildNumberFormat}" className="buildTypeParams longField"/>
    <span class="error" id="errorBuildNumberFormat"></span>
    <span class="smallNote">Format may include '{0}' as a placeholder for build counter value, for example 1.{0}.
    It may also contain a reference to any available parameter, for example, VCS revision number: <strong>%build.vcs.number.*%</strong>.
    </span>
    <span class="smallNote">Note: maximum length of a build number after all substitutions is <strong>256</strong> characters.</span>
  </td>
</tr>
<c:if test="${not buildForm.template}">
<tr>
  <th><label for="buildCounter">Build counter: <l:star/></label></th>
  <td><forms:textField name="buildCounter" style="width:10em;" maxlength="80" value="${buildForm.buildCounter}"/>&nbsp;<c:if test="${showResetCounter}"><a title="Next build number will be set to 1" showdiscardchangesmessage="false" href="#" onclick="$('buildCounter').value = '1'; return false">Reset counter</a></c:if>
    <span class="error" id="errorBuildCounter"></span></td>

</tr>
</c:if>