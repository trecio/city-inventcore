<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@attribute name="feature" type="jetbrains.buildServer.controllers.admin.projects.BuildFeatureBean" required="true" %>
<%@attribute name="showName" type="java.lang.Boolean" %>
<%@attribute name="showDescription" type="java.lang.Boolean" %>
<div style="${not feature.enabled ? 'color: #888': ''}">
  <c:if test="${empty showName or showName}">
    <div class="featureName">
      <c:out value="${feature.descriptor.buildFeature.displayName}"/>
      <c:choose>
        <c:when test="${feature.inherited and not feature.enabled}">&nbsp;<span class="inheritedParam">(inherited, disabled)</span></c:when>
        <c:when test="${feature.inherited}">&nbsp;<span class="inheritedParam">(inherited)</span></c:when>
        <c:when test="${not feature.enabled}">&nbsp;<span class="inheritedParam">(disabled)</span></c:when>
      </c:choose>
    </div>
  </c:if>
  <c:if test="${empty showDescription or showDescription}">
  <div class="featureDescription">
    ${feature.shortDescription}
  </div>
  </c:if>
</div>