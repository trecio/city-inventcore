<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz"
  %><%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout"
  %><%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"
  %><%@ taglib prefix="ext" tagdir="/WEB-INF/tags/ext"
  %><%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ attribute name="body_include" fragment="true" %>
<%@ attribute name="head_include" fragment="true" %>
<%@ attribute name="selectedStep" required="true" %>
<%@ tag body-content="empty" %>

<jsp:useBean id="buildForm" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" scope="request"/>
<bs:page disableScrollingRestore="true">
  <jsp:attribute name="page_title">
    <c:set var="pageTitle">${buildForm.name} <c:choose>
      <c:when test="${buildForm.template}">Template</c:when>
      <c:otherwise>Configuration</c:otherwise>
    </c:choose></c:set><c:out value="${pageTitle}"/>
  </jsp:attribute>

  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/admin/buildTypeForm.css
      /css/admin/templates.css
      /css/admin/adminMain.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/bs/copyProject.js
      /js/bs/moveBuildType.js
      /js/bs/editBuildType.js
      /js/bs/buildType.js
      /js/jquery/jquery.ui.position.js
    </bs:linkScript>
    <script type="text/javascript">
      BS.Navigation.items = [
          {title: "Administration", url: '<c:url value="/admin/admin.html"/>'},
          {title: "<bs:escapeForJs text="${buildForm.project.name}" forHTMLAttribute="true"/> Project", url: '<admin:editProjectLink projectId="${buildForm.project.projectId}" withoutLink="true"/>'},
          {
            title: "<bs:escapeForJs text="${pageTitle}" forHTMLAttribute="true"/>",
            selected:true,
            siblings: {
              type: "buildType",
              urlParamName: "id",
              urlValuePrefix: "buildType:",
              buildTypes: [<c:forEach items="${buildForm.project.buildTypes}" var="bt" varStatus="pos">{id: "${bt.buildTypeId}", name: "<bs:escapeForJs text="${bt.name}"/>"<c:if test="${buildForm.settingsBuildType.buildTypeId == bt.buildTypeId}">, selected: true</c:if>}<c:if test="${not pos.last}">,</c:if></c:forEach>]
            }
          }
      ];

      $j(document).ready(function() {
        BS.AvailableParams.attachPopups('settingsId=${buildForm.settingsId}', 'buildTypeParams');
        BS.VisibilityHandlers.updateVisibility('mainContent');
      });
    </script>
    <jsp:invoke fragment="head_include"/>
  </jsp:attribute>

  <jsp:attribute name="quickLinks_include">
    <c:if test="${not buildForm.template}">
      <authz:authorize projectId="${buildForm.project.projectId}" allPermissions="RUN_BUILD">
        <div class="toolbarItem">
          <c:url var="buildTypeHome" value="/viewType.html?buildTypeId=${buildForm.settingsBuildType.buildTypeId}"/>
          <bs:runBuild buildTypeId="${buildForm.settingsBuildType.buildTypeId}" redirectTo=""/>
        </div>
        <div class="separator toolbarItem"></div>
      </authz:authorize>
      <bs:buildTypeLink buildType="${buildForm.settingsBuildType}" style="float:left">Build Configuration Home</bs:buildTypeLink>
    </c:if>
  </jsp:attribute>

  <jsp:attribute name="body_include">
    <c:if test="${not buildForm.template}">
      <bs:buildTypePaused buildType="${buildForm.settingsBuildType}"/>
    </c:if>

    <div id="container" class="clearfix">
      <div class="editBuildPageGeneral">
        <c:if test="${buildForm.templateBased}">
          <div class="smallNote templateNote">
            <c:set var="templateId">template:${buildForm.settingsBuildType.template.id}</c:set>
            <c:set var="templateStateUrl">?${fn:replace(pageContext.request.queryString, param["id"], templateId)}</c:set>
            This build configuration is based on template
            <bs:trimWhitespace>
              <authz:authorize projectId="${buildForm.settingsBuildType.template.parentProject.projectId}" allPermissions="EDIT_PROJECT">
                <jsp:attribute name="ifAccessGranted">
                    <a href="${templateStateUrl}"><c:out value="${buildForm.settingsBuildType.template.fullName}"/></a>
                </jsp:attribute>
                <jsp:attribute name="ifAccessDenied">
                    <c:out value="${buildForm.settingsBuildType.template.fullName}"/>
                </jsp:attribute>
              </authz:authorize>
            </bs:trimWhitespace>.

            Most of the settings of this build configuration cannot be edited directly and should be
            <bs:trimWhitespace>
              <authz:authorize projectId="${buildForm.settingsBuildType.template.parentProject.projectId}" allPermissions="EDIT_PROJECT">
                <jsp:attribute name="ifAccessGranted"><a href="${templateStateUrl}">modified in the template</a></jsp:attribute>
                <jsp:attribute name="ifAccessDenied">modified in the template</jsp:attribute>
              </authz:authorize>
            </bs:trimWhitespace>.

            Those settings where parametrization was allowed can be edited on the
            <admin:editBuildTypeLink step="buildParams" style="text-decoration: none;" buildTypeId="${buildForm.settingsBuildType.buildTypeId}" title="Build Parameters">Build Parameters</admin:editBuildTypeLink> section.
          </div>
        </c:if>
        <bs:unprocessedMessages/>
        <jsp:invoke fragment="body_include"/>
      </div>

      <bs:refreshable containerId="sidebarAdmin" pageUrl="${pageUrl}">
        <admin:editBuildTypeNav buildTypeForm="${buildForm}" selectedLink="${selectedStep}"/>
      </bs:refreshable>

    </div>

    <c:if test="${not empty buildForm.project.buildTypes}">
      <bs:popupControl clazz="siblingBuildTypes" style="display: none"
                       showPopupCommand="" hidePopupCommand="" stopHidingPopupCommand=""
                       controlId="siblingBuildTypes${buildForm.project.projectId}"></bs:popupControl>
    </c:if>
  </jsp:attribute>
</bs:page>

