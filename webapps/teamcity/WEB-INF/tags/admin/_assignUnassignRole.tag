<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@attribute name="availableRolesBean" type="jetbrains.buildServer.controllers.user.AvailableRolesBean" required="true"
  %><%@attribute name="formId" required="true"
  %><%@attribute name="dialogObject" required="true"
  %><%@attribute name="assignMode" required="true" %>
<script type="text/javascript">
  ${dialogObject}._projectBasedRoles = {};
  ${dialogObject}._roles = {};
  <c:forEach items="${availableRolesBean.availableRoles}" var="roleBean">
    <c:if test="${roleBean.projectAssociationSupported}">${dialogObject}._projectBasedRoles['${roleBean.role.id}'] = true;</c:if>
    ${dialogObject}._roles['${roleBean.role.id}'] = {
      name: '<bs:escapeForJs text="${roleBean.role.name}"/>',
      permissions: new Array(),
      projects: new Array()
    };
    <c:forEach items="${roleBean.rolePermissions}" var="permission">
    ${dialogObject}._roles['${roleBean.role.id}'].permissions.push('<bs:escapeForJs text="${permission.description}"/>');
    </c:forEach>
    <c:forEach items="${availableRolesBean.projectsMap[roleBean]}" var="project">
      <c:choose>
        <c:when test="${null == project}">
          ${dialogObject}._roles['${roleBean.role.id}'].projects.push({id: '', name: ''});
          <c:set var="globalScopeAvailable" value="true"/>
        </c:when>
        <c:otherwise>
          ${dialogObject}._roles['${roleBean.role.id}'].projects.push({id: '${project.projectId}', name: '<bs:escapeForJs text="${project.extendedName}"/>'});
        </c:otherwise>
      </c:choose>
    </c:forEach>
  </c:forEach>
</script>
<table width="100%" cellspacing="0" cellpadding="0" class="assignUnassignRoleDialogTable">
  <tr>
    <td valign="top" class="header"><label for="${formId}_role">Role: </label></td>
    <td valign="top">
      <forms:select name="role" id="${formId}_role" onchange="${dialogObject}.roleChanged()" style="width: 320px;" enableFilter="true">
        <c:forEach items="${availableRolesBean.availableRoles}" var="roleBean">
          <forms:option value="${roleBean.role.id}"><c:out value="${roleBean.role.name}"/></forms:option>
        </c:forEach>
      </forms:select>
    </td>
  </tr>
  <tr>
    <td></td>
    <td>
      <span class="smallNote" style="margin-left: 0; margin-right: 2em; font-size: 85%; float: right;">View "<span id="${formId}_roleName"></span>" role <a href="#" onclick="${dialogObject}.showRolesDescription(); return false" title="View role permissions">permissions</a></span>
    </td>
  </tr>
  <c:if test="${globalScopeAvailable}">
    <tr>
      <td valign="top"><label for="${formId}_scope">Scope: </label></td>
      <td>
        <forms:radioButton name="roleScope" id="${formId}_globalScope" value="global" onclick="this.form.projectId.selectedIndex = -1; Form.Element.disable(this.form.projectId)"/> <label for="${formId}_globalScope">${assignMode ? 'Grant role globally' : 'Role granted globally'}</label>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        <forms:radioButton name="roleScope" id="${formId}_perProjectScope" value="perProject" onclick="Form.Element.enable(this.form.projectId)"/> <label for="${formId}_perProjectScope">${assignMode ? 'Grant role in selected projects:' : 'Role granted in selected projects:'}</label>
      </td>
    </tr>
  </c:if>
  <tr id="projectsRow">
    <td></td>
    <td valign="top">
      <bs:inplaceFilter containerId="${formId}_projectId" activate="true" style="width: 320px;" filterText="&lt;filter projects>"/>
      <select name="projectId" id="${formId}_projectId" style="width: 320px;" size="7" multiple="multiple" onchange="if (${dialogObject}.scopeChooserAvailable()) {this.form.roleScope[1].click();}">
      </select>
    </td>
  </tr>
</table>
<c:if test="${not globalScopeAvailable}">
  <input type="hidden" name="roleScope" value="perProject"/>
</c:if>