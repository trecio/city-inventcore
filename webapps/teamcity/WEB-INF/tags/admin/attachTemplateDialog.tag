<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ attribute name="currentProjectTemplates" required="true" type="java.util.List" %>
<%@ attribute name="otherProjectsTemplates" required="true" type="java.util.List" %>
<%@ attribute name="buildType" required="true" type="jetbrains.buildServer.serverSide.SBuildType" %>

<c:url value='/admin/action.html' var="actionUrl"/>
<bs:modalDialog formId="useTemplateForm"
                title="Associate with Template"
                action="${actionUrl}"
                closeCommand="BS.AttachDetachTemplateAction.cancelDialog()"
                saveCommand="BS.AttachDetachTemplateAction.submit()">
  <div class="attentionComment">You are going to associate this build configuration with a template.
    Note that this build configuration settings (except dependencies, parameters and requirements) will be completely superseded by template settings.</div>

  <span class="error" id="error_attachFailed" style="margin-left: 0;"></span>
  <br/>

  <table class="runnerFormTable">
    <tr>
      <td class="name"><label for="templateId"><strong>Choose template:</strong></label></td>
      <td class="value">
        <forms:select id="templateId" name="templateId" style="width: 21em;" enableFilter="true"
                onchange="BS.AttachDetachTemplateAction.templateChanged('${buildType.buildTypeId}', this.options[this.selectedIndex].value, this.selectedIndex < ${1+fn:length(currentProjectTemplates)})">
          <option value="">-- Please choose template --</option>
          <c:forEach items="${currentProjectTemplates}" var="template">
            <forms:option value="${template.id}"><c:out value="${template.name}"/></forms:option>
          </c:forEach>
          <c:if test="${not empty otherProjectsTemplates}">
            <option value="">-- Templates from other projects --</option>
            <c:forEach items="${otherProjectsTemplates}" var="template">
              <forms:option value="${template.id}"><c:out value="${template.fullName}"/></forms:option>
            </c:forEach>
          </c:if>
        </forms:select>
        <forms:saving id="templateParamsUpdateProgress"/>
      </td>
    </tr>
  </table>

  <div id="attachTemplate_vcsRootsSection" class="vcsRootsSection" style="display: none;">
  <authz:authorize allPermissions="CHANGE_GLOBAL_VCS_ROOT">
    <jsp:attribute name="ifAccessGranted">
      <div class="attentionComment">There is at least one non-shared VCS root in the selected template. This VCS root will be shared if you continue.</div>
      <script type="text/javascript">
        $('attachTemplate_vcsRootsSection').onshow = function() {
          BS.AttachDetachTemplateAction.submitEnabled(true);
        };
      </script>
    </jsp:attribute>
    <jsp:attribute name="ifAccessDenied">
      <div class="attentionComment">There is at least one non-shared VCS root in the selected template. To attach to this template VCS roots need to be shared but you do not have enough permissions to share them. Operation is not possible.</div>
      <script type="text/javascript">
        $('attachTemplate_vcsRootsSection').onshow = function() {
          BS.AttachDetachTemplateAction.submitEnabled(false);
        };
      </script>
    </jsp:attribute>
  </authz:authorize>
  </div>
  

  <div id="templateParameters"></div>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.AttachDetachTemplateAction.cancelDialog()" showdiscardchangesmessage="false"/>
    <forms:submit name="associateWithTemplate" label="Associate"/>
    <forms:saving id="associateWithTemplateProgress"/>
  </div>

  <input type="hidden" name="buildTypeId" value=""/>
  <input type="hidden" name="attachToTemplate" value="1"/>

</bs:modalDialog>
