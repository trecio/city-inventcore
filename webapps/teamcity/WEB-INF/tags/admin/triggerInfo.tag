<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@attribute name="trigger" type="jetbrains.buildServer.controllers.admin.projects.BuildTriggerInfo" required="true" %>
<%@attribute name="showName" type="java.lang.Boolean" %>
<%@attribute name="showDescription" type="java.lang.Boolean" %>
<div style="${not trigger.enabled ? 'color: #888': ''}">
  <c:if test="${empty showName or showName}">
    <div class="triggerName">
      <c:out value="${trigger.buildTriggerService.displayName}"/>
      <c:choose>
        <c:when test="${trigger.inherited and not trigger.enabled}">&nbsp;<span class="inheritedParam">(inherited, disabled)</span></c:when>
        <c:when test="${trigger.inherited}">&nbsp;<span class="inheritedParam">(inherited)</span></c:when>
        <c:when test="${not trigger.enabled}">&nbsp;<span class="inheritedParam">(disabled)</span></c:when>
      </c:choose>
    </div>
  </c:if>
  <c:if test="${empty showDescription or showDescription}">
  <div class="triggerDescription">
    <bs:out value="${trigger.description}"/>
  </div>
  </c:if>
</div>