<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@attribute name="buildType" required="true" type="jetbrains.buildServer.serverSide.SBuildType"
  %><%@attribute name="cameFromUrl" required="false" %>
<script type="text/javascript">
  <c:set var="cameFrom"><c:if test="${not empty cameFromUrl}">&cameFromUrl=${cameFromUrl}</c:if></c:set>
  BS['_EditSettings${buildType.buildTypeId}']  = new BS.Popup('editBuildConfigurationSettingsPopup', {
    url: window['base_uri'] + '/showJsp.html?jspPath=/editBuildTypePopup.jsp?buildTypeId=${buildType.buildTypeId}${cameFrom}',
    shift: {x: -241, y: 20},
    className: 'quickLinksMenuPopup'
  });
</script>
<bs:popupControl showPopupCommand="BS['_EditSettings${buildType.buildTypeId}'].showPopupNearElement(this)"
                 hidePopupCommand="BS['_EditSettings${buildType.buildTypeId}'].hidePopup();"
                 stopHidingPopupCommand="BS['_EditSettings${buildType.buildTypeId}'].stopHidingPopup();"
                 controlId="editControl${buildType.buildTypeId}">
  <admin:editBuildTypeLink buildTypeId="${buildType.buildTypeId}" cameFromUrl="${cameFromUrl}"
                           title="Edit build configuration settings"><jsp:doBody/></admin:editBuildTypeLink></bs:popupControl>
