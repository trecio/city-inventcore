<%@ attribute name="rolesForm" type="jetbrains.buildServer.controllers.admin.roles.EditRolesForm" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="afn" uri="/WEB-INF/functions/authz"
  %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz"
  %><%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"
  %>

<c:url var="action" value="/admin/action.html"/>
<bs:modalDialog formId="addPermissions" title="Add Permissions" action="${action}"
                closeCommand="BS.AddPermissionsDialog.close();" saveCommand="BS.AddPermissionsDialog.save();">
  <script type="text/javascript">
     window.rolesWithPermissions = {};
     <c:forEach items="${rolesForm.roles}" var="role">
        var perms = new Array();
        <c:forEach items="${role.availablePermissions}" var="permission">
            perms.push({
                name: '${permission.name}',
                description: '<bs:escapeForJs text="${permission.description}"/>'
            });
        </c:forEach>
        rolesWithPermissions['${role.id}'] = {
          availablePermissions: perms
        };
     </c:forEach>

      function fillPermissionsList(roleId) {
          var selector = $('permissionId');
          while (selector.options.length > 0) {
              selector.options[0] = null;
          }

          var availablePermissions = rolesWithPermissions[roleId].availablePermissions;
          for (var i = 0; i < availablePermissions.length; i++) {
              var option = new Option(availablePermissions[i].description, availablePermissions[i].name, false, false);
              option.className = "inplaceFiltered";
              selector.options[i] = option;
          }
      }

      BS.AddPermissionsDialog.fillPermissionsList = fillPermissionsList;
  </script>
  Select permissions:
  <br>
  <bs:inplaceFilter containerId="permissionId" activate="false" filterText="&lt;filter permissions&gt;"/>
  <select name="permissionId" id="permissionId" style="width: 100%" size="7" multiple="multiple">
  </select>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.AddPermissionsDialog.close()"/>
    <forms:submit name="addRolePermissions" label="Add"/>
    <forms:saving id="addingPermissions"/>
  </div>
</bs:modalDialog>
