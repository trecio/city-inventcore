<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="l" tagdir="/WEB-INF/tags/layout" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@
    taglib prefix="afn" uri="/WEB-INF/functions/authz" %><%@
    attribute name="project" required="true" type="jetbrains.buildServer.serverSide.SProject"

%><c:set var="projId" value="${project.projectId}"
/><c:choose>
  <%--@elvariable id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary"--%>
  <c:when test="${serverSummary.buildConfigurationsLeft - fn:length(project.buildTypes) + 1 > 0 or
                  afn:permissionGrantedForProject(project, 'ARCHIVE_PROJECT') or
                  afn:permissionGrantedForProject(project, 'DELETE_PROJECT')}">
    <bs:simplePopup controlId="prjActions${projId}" linkOpensPopup="true"
                    popup_options="shift: {x: -150, y: 20}, className: 'quickLinksMenuPopup'">
      <jsp:attribute name="content">
        <div>
          <ul class="menuList">
            <bs:professionalLimited shouldBePositive="${serverSummary.buildConfigurationsLeft - fn:length(project.buildTypes) + 1}">
              <jsp:body>
                <l:li>
                  <admin:copyProjectLink sourceProject="${project}" newName="${project.name}">Copy project</admin:copyProjectLink>
                </l:li>
              </jsp:body>
            </bs:professionalLimited>
            <authz:authorize allPermissions="ARCHIVE_PROJECT" projectId="${projId}">
              <c:choose>
                <c:when test="${project.archived}">
                  <l:li>
                    <a href="#" title="Unarchive project" onclick="return BS.AdminActions.dearchiveProject('${projId}', 1)">Unarchive project</a>
                  </l:li>
                </c:when>
                <c:otherwise>
                  <l:li>
                    <a href="#" title="Archive project" onclick="return BS.AdminActions.archiveProject('${projId}', 1)">Archive project</a>
                  </l:li>
                </c:otherwise>
              </c:choose>
            </authz:authorize>
            <authz:authorize allPermissions="DELETE_PROJECT">
              <l:li>
                <a href="#" title="Delete project" onclick="return BS.AdminActions.deleteProject('${projId}')">Delete project</a>
              </l:li>
            </authz:authorize>
          </ul>
        </div>
      </jsp:attribute>
      <jsp:body>more</jsp:body>
    </bs:simplePopup>
  </c:when>
  <c:otherwise>
    <span class="commentText" title="No actions available">more</span>
  </c:otherwise>
</c:choose>
