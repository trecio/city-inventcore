<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ attribute name="buildForm" required="true" type="jetbrains.buildServer.controllers.admin.projects.BuildTypeForm"%>
<%@ attribute name="title" required="true"%>

<h2 class="noBorder">General Settings</h2>
<table class="runnerFormTable">
<tr>
  <th><label for="name">Name: <l:star/></label></th>
  <td><forms:textField name="name" className="longField" maxlength="80" value="${buildForm.name}"/>
  <span class="error" id="errorName"></span></td>
</tr>

<c:if test="${not buildForm.template}">
<tr>
  <th><label for="description">Description:</label></th>
  <td><forms:textField name="description" className="longField" maxlength="256" value="${buildForm.description}"/></td>
</tr>
</c:if>

<admin:buildTypeBuildNumbers buildForm="${buildForm}" showResetCounter="true"/>

<tr>
  <th><label for="artifactPaths">Artifact paths: <bs:help file="Configuring+General+Settings" anchor="ArtifactPaths"/></label></th>
  <td><props:textarea name="artifactPaths" textAreaName="artifactPaths" value="${buildForm.artifactPaths}"
                linkTitle="Edit artifact paths" cols="49" rows="3" expanded="true" className="buildTypeParams longField"/>
      <span class="smallNote">New line or comma separated paths to build artifacts. Ant-style wildcards like <kbd>dir/**/*.zip</kbd> and target directories
    like <kbd>*.zip => winFiles,unix/distro.tgz => linuxFiles</kbd>, where <kbd>winFiles</kbd> and <kbd>linuxFiles</kbd> are target directories are supported.</span>
  </td>
</tr>

  <tr>
    <th class="noBorder"><label>Build options:</label></th>
    <td class="noBorder">
      <forms:checkbox name="hangingBuildsDetectionEnabled" checked="${buildForm.hangingBuildsDetectionEnabled}"/>
      <label for="hangingBuildsDetectionEnabled">enable hanging builds detection</label>
      <br/>
      <forms:checkbox name="allowExternalStatus" checked="${buildForm.allowExternalStatus}"/>
      <label for="allowExternalStatus">enable status widget</label>
      <bs:help file="Configuring+General+Settings" anchor="EnableStatusWidget"/>
      <br/>
      <label for="maxBuilds">Limit the number of simultaneously running builds (0 &mdash; unlimited)</label>
      <forms:textField name="maxBuilds" value="${buildForm.maxBuilds}" style="width: 4em; font-size:90%;" maxlength="8"/>
      <span class="error" id="errorMaxBuilds"></span>
    </td></tr>

</table>
<script type="text/javascript">
  BS.MultilineProperties.updateVisible();
</script>
