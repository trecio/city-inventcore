<%@ tag import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="admfn" uri="/WEB-INF/functions/admin" %>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz" %>
<%@ attribute name="vcsRootsBean" required="true" type="jetbrains.buildServer.controllers.admin.projects.VcsSettingsBean"%>
<%@ attribute name="pageUrl" required="true" type="java.lang.String"%>
<%@ attribute name="buildForm" required="true" type="jetbrains.buildServer.controllers.admin.projects.BuildTypeForm"%>
<%@ attribute name="pageTitle" required="false" type="java.lang.String"%>

<bs:messages key="vcsRootsUpdated"/>
<bs:messages key="checkoutRulesUpdated"/>
<bs:messages key="vcsRootUpdateFailure"/>
<c:if test="${not empty vcsRootsBean.vcsRoots}">
  <l:tableWithHighlighting className="parametersTable" mouseovertitle="Click to edit VCS root" id="vcsRoots" highlightImmediately="true">
    <tr>
      <th colspan="3">VCS root</th>
      <th style="width: 20%;">Checkout rules</th>
    </tr>
    <c:forEach items="${vcsRootsBean.vcsRoots}" var="vcsRootEntry" varStatus="status">
      <c:set var="vcsRoot" value="${vcsRootEntry.vcsRoot}"/>
      <c:set var="canEditVcsRoot" value="${afn:canEditVcsRoot(vcsRoot)}"/>
      <c:set var="highlight" value="${canEditVcsRoot ? 'highlight' : ''}"/>
      <admin:vcsRootEditScope vcsRootsBean="${vcsRootsBean}" vcsRoot="${vcsRoot}" buildForm="${buildForm}"/>
      <c:set var="cameFromUrl">${pageUrl}<c:if test="${not buildForm.createMode}">&init=1</c:if></c:set>
      <c:set var="editVcsRootLink"><admin:editVcsRootLink vcsRoot="${vcsRoot}" editingScope="${editingScope}" cameFromUrl="${pageUrl}" cameFromTitle="${pageTitle}" withoutLink="true"/></c:set>
      <c:set var="onclick"><c:if test="${canEditVcsRoot}">BS.openUrl(event, '${editVcsRootLink}');</c:if></c:set>
      <c:set var="inherited" value="${not buildForm.template and not buildForm.createMode and not vcsRootsBean.detacheableVcsRoots[vcsRoot.id]}"/>
      <tr>
        <td class="${highlight}" onclick="${onclick}">
          <em>(${vcsRoot.vcsName}) </em> <c:out value="${vcsRoot.name}"/> <c:if test="${inherited}"><span class="inheritedParam">(inherited)</span></c:if>
        </td>
        <td class="edit ${highlight}" onclick="${onclick}">
          <c:choose>
            <c:when test="${canEditVcsRoot}">
              <a href="${editVcsRootLink}" title="Click to edit this VCS root" showdiscardchangesmessage="false">edit</a>
            </c:when>
            <c:otherwise>
              <span style="white-space:nowrap" title="You do not have enough permissions to edit this VCS root">cannot be edited</span>
            </c:otherwise>
          </c:choose>
        </td>
        <td class="edit" style="white-space: nowrap;">
          <c:choose>
            <c:when test="${not inherited}">
              <a href="#" onclick="BS.EditVcsRootsForm.detachVcsRoot(${vcsRoot.id}); return false" showdiscardchangesmessage="false">detach</a>
            </c:when>
            <c:otherwise>
              <span title="Inherited VCS roots cannot be detached">can't detach</span>
            </c:otherwise>
          </c:choose>
        </td>
        <!-- TODO: remove inline styles from here! -->
        <td style="white-space: nowrap;">
          <a href="#" showdiscardchangesmessage="false" onclick="BS.EditCheckoutRulesForm.showDialog(${vcsRoot.id}, '<bs:escapeForJs text="${vcsRoot.name}" forHTMLAttribute="true"/>', '<bs:escapeForJs text="${vcsRootEntry.checkoutRules.asString}" forHTMLAttribute="true"/>', ${inherited}); return false">${not inherited ? 'edit' : 'view'} checkout rules (${fn:length(vcsRootEntry.checkoutRules.body)})</a>
        </td>
      </tr>
    </c:forEach>
  </l:tableWithHighlighting>
</c:if>
