<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ attribute name="requirementsBean" required="true" type="jetbrains.buildServer.controllers.buildType.RequirementsBean"%>
<%@ attribute name="requirements" required="true" type="java.util.Collection"%>
<%@ attribute name="editable" required="true" type="java.lang.Boolean"%>

<c:if test="${not empty requirements}">
<c:choose>
  <c:when test="${editable}">
    <l:tableWithHighlighting className="parametersTable" mouseovertitle="Click to edit requirement">
      <tr style="background-color: #f5f5f5;">
        <th>Parameter Name</th>
        <th colspan="3">Condition</th>
      </tr>
      <c:forEach var="requirement" items="${requirements}">
      <c:set var="onclick">BS.EditRequirementDialog.showDialog($('name_${requirement.id}').value, $('value_${requirement.id}').value, '${requirement.type.name}', ${requirement.inherited});</c:set>
      <tr>
        <td class="name highlight" onclick="${onclick}">
          <c:out value="${requirement.parameterNameWithPrefix}"/>
          <c:if test="${requirement.inherited}"><span class="inheritedParam">(inherited)</span></c:if>
        </td>
        <td class="value highlight" onclick="${onclick}">
          <admin:requirementValue requirementType="${requirement.type}" parameterValue="${requirement.parameterValue}"/>
        </td>
        <td class="edit highlight" onclick="${onclick}"><a href="#" onclick="${onclick}; Event.stop(event)">edit</a>
        <input type="hidden" id="name_${requirement.id}" value="<c:out value='${requirement.parameterNameWithPrefix}'/>"/>
        <input type="hidden" id="value_${requirement.id}" value="<c:out value='${requirement.parameterValue}'/>"/>
        </td>
        <td class="edit">
          <c:choose>
            <c:when test="${requirement.redefined}">
              <a href="#" onclick="BS.RequirementsForm.resetRequirement(${requirement.id}); return false">reset</a>
            </c:when>
            <c:when test="${requirement.inherited}">
              <span title="Inherited requirements cannot be deleted">undeletable</span>
            </c:when>
            <c:otherwise>
              <a href="#" onclick="BS.RequirementsForm.removeRequirement(${requirement.id}); return false">delete</a>
            </c:otherwise>
          </c:choose>
        </td>
      </tr>
      </c:forEach>
    </l:tableWithHighlighting>
  </c:when>
  <c:otherwise>
    <table class="parametersTable" cellpadding="0" cellspacing="0">
      <tr style="background-color: #f5f5f5;">
        <th>Parameter Name</th>
        <th>Condition</th>
      </tr>
      <c:forEach var="requirement" items="${requirements}">
      <tr>
        <td class="name" ><c:out value="${requirement.parameterNameWithPrefix}"/></td>
        <td class="value">
          <admin:requirementValue requirementType="${requirement.type}" parameterValue="${requirement.parameterValue}"/>
        </td>
      </tr>
      </c:forEach>
    </table>
  </c:otherwise>
</c:choose>
</c:if>


