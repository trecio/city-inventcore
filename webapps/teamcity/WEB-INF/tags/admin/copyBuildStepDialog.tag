<%@ tag import="jetbrains.buildServer.controllers.admin.projects.EditBuildTypeForm" %>
<%@ tag import="jetbrains.buildServer.controllers.admin.projects.EditBuildTypeFormFactory" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %>
<%@ taglib prefix="afn" uri="/WEB-INF/functions/authz" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ attribute name="buildTypeForm" required="true" type="jetbrains.buildServer.controllers.admin.projects.EditableBuildTypeSettingsForm" %>
<c:set var="editableProjects" value="${buildTypeForm.editableProjects}"/>
<c:set var="btPrefix" value="<%=EditBuildTypeFormFactory.BT_PREFIX%>"/>
<c:set var="tplPrefix" value="<%=EditBuildTypeFormFactory.TEMPLATE_PREFIX%>"/>
<c:url value="/admin/copyBuildStep.html" var="copyAction"/>

<bs:modalDialog formId="copyBuildStepForm"
                title="Copy Build Step"
                action="${copyAction}"
                closeCommand="BS.CopyBuildStepForm.cancelDialog()"
                saveCommand="BS.CopyBuildStepForm.submitCopy()">

  <label for="targetSettingsId" class="tableLabel">Copy Build Step to:</label>
  <forms:select id="targetSettingsId" name="targetId" style="width: 22em;" enableFilter="true">
    <c:forEach items="${editableProjects}" var="project">
      <c:if test="${not empty project.buildTypes or not empty project.buildTypeTemplates}">
        <c:forEach items="${project.buildTypes}" var="bt">
          <forms:option value="${btPrefix}${bt.buildTypeId}" selected="${buildTypeForm.settings == bt}"><c:out value="${bt.fullName}"/></forms:option>
        </c:forEach>
        <c:forEach items="${project.buildTypeTemplates}" var="tpl">
          <forms:option value="${tplPrefix}${tpl.id}" selected="${buildTypeForm.settings == tpl}"><c:out value="${tpl.fullName}"/> (template)</forms:option>
        </c:forEach>
      </c:if>
    </c:forEach>
  </forms:select>

  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.CopyBuildStepForm.cancelDialog()" showdiscardchangesmessage="false"/>
    <forms:submit name="copyBuildStep" label="Copy"/>
    <forms:saving id="copyBuildStepProgress"/>
  </div>

  <input type="hidden" name="id" id="sourceSettingsId" value="${buildTypeForm.settingsId}"/>
  <input type="hidden" name="runnerId" id="sourceRunnerId" value=""/>

</bs:modalDialog>

