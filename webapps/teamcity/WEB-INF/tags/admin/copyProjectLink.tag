<%--@elvariable id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@attribute name="sourceProject" required="true" type="jetbrains.buildServer.serverSide.SProject" %>
<%@attribute name="newName" required="true" %>
<bs:professionalLimited shouldBePositive="${serverSummary.buildConfigurationsLeft - fn:length(sourceProject.buildTypes) + 1}">
  <jsp:attribute name="disabled">
    <span class="commentText" title="Cannot copy project due to limit for number of build configurations">copy</span>
  </jsp:attribute>
  <jsp:body>
    <a href="#" title="Copy project" onclick="BS.CopyProjectForm.showDialog('${sourceProject.projectId}', '<c:out value="${newName}"/>', ${fn:length(sourceProject.buildTypeTemplates) > 0}); return false"><jsp:doBody/></a>
  </jsp:body>
</bs:professionalLimited>

