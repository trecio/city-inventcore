<%@ tag import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
  attribute name="vcsRoot" required="true" type="jetbrains.buildServer.vcs.SVcsRoot" %><%@
  attribute name="withoutLink" required="false" type="java.lang.Boolean" %><%@
  attribute name="editingScope" required="true" type="java.lang.String" %><%@
  attribute name="cameFromUrl" required="true" type="java.lang.String" %><%@
  attribute name="cameFromTitle" required="false" type="java.lang.String"
%><c:set var="escapedCameFromUrl" value="<%=WebUtil.encode(cameFromUrl)%>"
/><c:set var="escapedTitle" value="<%=WebUtil.encode(cameFromTitle)%>"
/><c:url value="/admin/editVcsRoot.html?init=1&action=editVcsRoot&vcsRootId=${vcsRoot.id}&editingScope=${editingScope}&cameFromUrl=${escapedCameFromUrl}&cameFromTitle=${escapedTitle}" var="editVcsLink"
/><c:choose><c:when test="${withoutLink}">${editVcsLink}</c:when><c:otherwise><a href="${editVcsLink}" title="Click to edit this VCS root"><jsp:doBody/></a></c:otherwise></c:choose>