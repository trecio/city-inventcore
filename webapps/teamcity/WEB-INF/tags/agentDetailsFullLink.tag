<%@ tag import="jetbrains.buildServer.web.util.WebUtil"%><%@
  taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
  taglib prefix="bs" tagdir="/WEB-INF/tags"%><%@
  taglib prefix="authz" tagdir="/WEB-INF/tags/authz"%><%@
  attribute name="agent" required="false" type="jetbrains.buildServer.serverSide.BuildAgentEx"%><%@
  attribute name="agentType" required="false" type="jetbrains.buildServer.serverSide.agentTypes.SAgentType"%><%@
  attribute name="useDisplayName" required="false"%><%@
  attribute name="doNotShowOutdated" required="false" type="java.lang.Boolean"%><%@
  attribute name="doNotShowOSIcon" required="false" type="java.lang.Boolean"%><%@
  attribute name="doNotShowPoolInfo" required="false" type="java.lang.Boolean"%><%@
  attribute name="showPoolInfoAsText" required="false" type="java.lang.Boolean"%><%@
  attribute name="showRunningStatus" required="false" type="java.lang.Boolean"%><%@
  attribute name="showCloudIcon" required="false" type="java.lang.Boolean"%><%@
  attribute name="cloudStartingInstances" required="false" type="java.lang.Integer"%><%@
  attribute name="doNotShowUnavailableStatus" required="false" type="java.lang.Boolean"
  %><jsp:useBean id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary" scope="request"
  /><c:set var="nonEmptyAgentType" value="${empty agentType ? agent.agentType : agentType}"
  /><c:set var="realAgent" value="${empty agent ? nonEmptyAgentType.realAgent : agent}"
  /><c:if test="${not empty realAgent and not doNotShowOutdated}"><bs:agentOutdated agent="${realAgent}"/></c:if
  ><authz:authorize allPermissions="VIEW_AGENT_DETAILS"
  ><c:if test="${not doNotShowOSIcon}"
  ><bs:osIcon osName="${nonEmptyAgentType.operatingSystemName}" small="${true}"/></c:if
  ><c:if test="${not empty showCloudIcon and showCloudIcon and nonEmptyAgentType.details.cloudAgent}"
  ><img src="<c:url value='/img/cloud.png'/>" title="Cloud" alt="Cloud" class="osIcon" style="width: 16px;"/></c:if></authz:authorize
  ><bs:agentDetailsLink agent="${realAgent}" agentType="${agentType}" useDisplayName="${useDisplayName}"><jsp:doBody/></bs:agentDetailsLink
  ><c:if test="${not doNotShowPoolInfo and serverSummary.hasSeveralAgentPools and nonEmptyAgentType.agentTypeId > 0}">&nbsp;(<bs:agentPoolLink agentPoolId="${nonEmptyAgentType.agentPool.agentPoolId}" agentPoolName="${nonEmptyAgentType.agentPool.name}" noLink="${showPoolInfoAsText}"/>)</c:if
  ><c:if test="${not empty realAgent and realAgent.id > 0 and (showRunningStatus or not doNotShowUnavailableStatus)}"><bs:agentShortStatus agent="${realAgent}" showRunningStatus="${showRunningStatus}" showUnavailable="${not doNotShowUnavailableStatus}"/></c:if
  ><c:if test="${not empty cloudStartingInstances and cloudStartingInstances > 0}"> (${cloudStartingInstances} cloud agent<bs:s val="${cloudStartingInstances}"/> <bs:are_is val="${cloudStartingInstances}"/> starting)</c:if>