<%@ tag import="java.util.Date"%><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%><%@
    attribute name="buildData" required="true" rtexprvalue="true" type="jetbrains.buildServer.serverSide.SBuild"

%><p class="hidden" id="aLink:${buildData.buildId}"><bs:buildAgentDetailsFullLink build="${buildData}"/></p>
<c:if test="${buildData.finished}"
    ><bs:date value="${buildData.finishDate}" smart="true" no_smart_title="true"
   /> <bs:duration buildData="${buildData}"
/></c:if
 ><c:if test="${not buildData.finished}"
    ><c:if test="${buildData.estimationForTimeLeft < 0}"
      ><div id="build:${buildData.buildId}:progress">
        <bs:date value="${buildData.startDate}"
      /> <bs:duration buildData="${buildData}"
      /></div></c:if
    ><c:if test="${buildData.estimationForTimeLeft >= 0}"
      ><%-- The following logic is repeated in runningBuilds.js:
   --%><c:set var="remaining"><bs:printTime time="${buildData.estimationForTimeLeft}"/></c:set
      ><c:set var="overtime"><bs:printTime time="${buildData.durationOvertime}"/></c:set
      ><c:set var="progressText">&nbsp;&nbsp;<c:choose><c:when test="${not empty remaining and remaining != '< 1s'}"><c:out value="${fn:replace(remaining, ' ', '&nbsp;')}"
          />&nbsp;left</c:when><c:when test="${not empty overtime and overtime != '< 1s'}">overtime:&nbsp;<c:out value="${fn:replace(overtime, ' ', '&nbsp;')}"
          /></c:when></c:choose></c:set

      ><div class="progress" id="build:${buildData.buildId}:progress">${progressText}<bs:progress buildData="${buildData}">${progressText}</bs:progress
      ></div><div class="hidden"><bs:date value="${buildData.startDate}"/></div></c:if
></c:if>