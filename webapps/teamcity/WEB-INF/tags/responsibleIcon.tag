<%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="responsibility" type="jetbrains.buildServer.responsibility.ResponsibilityEntry" required="false" %><%@
    attribute name="title" type="java.lang.String" required="false" %><%@
    attribute name="style" type="java.lang.String" required="false" %><%@
    attribute name="test" type="jetbrains.buildServer.serverSide.STest" required="false" %><%@
    attribute name="buildTypeRef" type="jetbrains.buildServer.serverSide.SBuildType" required="false" %><%@
    attribute name="noActions" type="java.lang.Boolean" required="false"

%><c:choose
  ><c:when test="${not empty responsibility and responsibility.state.fixed}"
    ><c:url value="/img/buildStates/fixedTestResp.gif" var="url"
/></c:when><c:when test="${not empty responsibility and responsibility.state.active}"
    ><c:url value="/img/investigate.gif" var="url"
/></c:when></c:choose
    
><c:if test="${not empty responsibility}"
    ><c:set var="title"><bs:responsibleTooltip responsibility="${responsibility}" test="${test}" buildTypeRef="${buildTypeRef}"
                                               noActions="${noActions}"/></c:set>
 </c:if

 ><c:if test="${not empty style}"
    ><c:set var="style_attr">style="${style}"</c:set
 ></c:if
><c:if test="${not empty url}"><img src="${url}" class="respIcon" ${style_attr} <bs:tooltipAttrs text="${title}" className="name-value-popup"/> alt=""/></c:if>