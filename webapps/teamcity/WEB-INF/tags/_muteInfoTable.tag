<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    attribute name="description" type="java.lang.String" required="true" %><%@
    attribute name="muteInfo" type="jetbrains.buildServer.serverSide.mute.MuteInfo" required="true"  %><%@
    attribute name="showCurrentMuteInfo" type="java.lang.Boolean" required="true" %><%@
    attribute name="onlyTestRunMuteInfo" type="java.lang.Boolean" required="false"

%><table>
  <c:if test="${showCurrentMuteInfo}">
    <tr>
      <th>Currently muted in:</th>
      <td>${description}</td>
    </tr>
  </c:if>
  <c:if test="${not showCurrentMuteInfo}">
    <tr>
      <td colspan="2">
        This test failure was muted<c:if test="${empty onlyTestRunMuteInfo or not onlyTestRunMuteInfo}">, but currently test is not muted</c:if>
      </td>
    </tr>
  </c:if>
  <c:if test="${not empty muteInfo.mutingUser}">
    <tr>
      <th>Muted by:</th><td><c:out value="${muteInfo.mutingUser.descriptiveName}"/></td>
    </tr>
  </c:if>
  <tr>
    <th>Since:</th>
    <td><bs:date value="${muteInfo.mutingTime}"/></td>
  </tr>
  <c:set var="autoUnmute" value="${muteInfo.autoUnmuteOptions}"/>
  <c:if test="${showCurrentMuteInfo}">
    <tr>
      <th>Unmute:</th>
      <c:choose>
        <c:when test="${autoUnmute.unmuteManually}">
          <td>manually</td>
        </c:when>
        <c:when test="${autoUnmute.unmuteWhenFixed}">
          <td>automatically when test is fixed</td>
        </c:when>
        <c:when test="${not empty autoUnmute.unmuteByTime}">
          <td>automatically on <bs:date value="${autoUnmute.unmuteByTime}"/></td>
        </c:when>
      </c:choose>
    </tr>
  </c:if>
  <c:if test="${not empty muteInfo.mutingComment}">
    <tr>
      <th>Comment:</th><td class="mute-comment"><bs:out value="${muteInfo.mutingComment}"/></td>
    </tr>
  </c:if>
</table>