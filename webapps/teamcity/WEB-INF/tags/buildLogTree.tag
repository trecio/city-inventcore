<%@ tag import="jetbrains.buildServer.web.util.SessionUser" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="intprop" uri="/WEB-INF/functions/intprop"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>

<%@ attribute name="buildData" fragment="false" required="true" type="jetbrains.buildServer.serverSide.SBuild"%>
<%@ attribute name="messagesIterator" fragment="false" required="true" type="jetbrains.buildServer.serverSide.buildLog.LowLevelEventsAwareMessageIterator"%>
<%@ attribute name="expand" fragment="false" required="true" type="java.lang.String"%>
<%@ attribute name="groupFlows" fragment="false" required="true" type="java.lang.Boolean"%>
<%@ attribute name="enabledFilter" fragment="false" required="true" type="java.lang.String"%>
<%@ attribute name="hideBlocks" fragment="false" required="true" type="java.lang.String"%>

<jsp:useBean id="server" type="jetbrains.buildServer.serverSide.SBuildServer" scope="request"/>
<jsp:useBean id="filters" type="java.util.Collection<jetbrains.buildServer.controllers.viewLog.tree.filters.TreeViewMessageFilter>" scope="request"/>

<div class="actionBar" style="margin-top: 5px;">
  <span class="farRight">
    <span title="Show parent block for each message"><label><forms:checkbox name="" id="hideBlockNames"/>Repeat block names</label></span>
  </span>

  <span class="nowrap">
    <bs:collapseExpand collapseAction="BS.BuildLogTree.reloadExpand('none'); return false;" expandAction="BS.BuildLogTree.reloadExpand('all'); return false;"/>
  </span>

  <span class="nowrap">
    <label for="messageFilters">View: </label>
    <select id="messageFilters" onchange="BS.BuildLogTree.reloadFilter(this.options[this.selectedIndex].value);">
        <c:forEach items="${filters}" var="filter" >
          <props:option value="${filter.type}" selected="${filter.type eq enabledFilter}">${filter.displayName}</props:option>
        </c:forEach>
      <props:option value="debug" selected="${'debug' eq enabledFilter}">Verbose</props:option>
    </select>
  </span>

  <c:if test="${intprop:getBoolean('teamcity.buildLog.highlightParallelActions.enabled')}">
  <span class="nowrap">
    <label>Parallel actions:</label>
    <forms:checkbox name="highlightFlows" id="highlightFlows"/><label for="highlightFlows" class="rightLabel">Highlight</label>
    <%--<forms:checkbox name="groupFlows" id="groupFlows" disabled="true"/><label for="groupFlows" class="rightLabel">Group</label>--%>
  </span>
  </c:if>
</div>

<div class="log rTree" id="buildLog">
  <script type="text/javascript">
    BS.BuildLogTree.init('${expand}', '${enabledFilter}', ${hideBlocks});
  </script>

  <bs:changeRequest key="messagesIterator" value="${messagesIterator}">
    <jsp:include page="buildLogTreePrinter.html">
      <jsp:param name="enabledFilter" value="${enabledFilter}"/>
      <jsp:param name="incremental" value="false"/>
      <jsp:param name="expand" value="${expand}"/>
      <jsp:param name="hideBlocks" value="${hideBlocks}"/>
    </jsp:include>
  </bs:changeRequest>
</div>