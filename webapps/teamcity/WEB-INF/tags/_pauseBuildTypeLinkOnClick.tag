<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
%><%@ attribute name="buildType" required="true" type="jetbrains.buildServer.serverSide.SBuildType"
%><%@ attribute name="pause" required="true" type="java.lang.Boolean"
%>BS.PauseBuildTypeDialog.showPauseBuildTypeDialog('${buildType.buildTypeId}', ${pause}<c:if test='${buildType.pauseComment != null && not empty buildType.pauseComment.comment}'>, '<bs:escapeForJs text="${buildType.pauseComment.comment}" forHTMLAttribute="true"/>'</c:if>)
