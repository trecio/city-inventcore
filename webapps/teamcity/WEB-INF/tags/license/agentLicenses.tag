<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms"%>
<%@attribute name="licenseData" type="jetbrains.buildServer.controllers.license.AgentLicenseData" required="true" rtexprvalue="true" %>

<div class="licensesPage clearfix">
  <div id="pageContent">
    <jsp:include page="/license/availableAgentLicenses.jsp"/>

    <c:set var="displayLinks" value="${not licenseData.enterKeysMode || licenseData.allEnteredKeysValid ? 'block' : 'none'}"/>
    <div class="newLicense" style="display: ${displayLinks}">
      <c:url value='/admin/admin.html?item=license&enterKeys=true' var="licenseUrl"/>
      <forms:addButton href="${licenseUrl}">Enter new license(s)</forms:addButton>
    </div>

    <c:set var="displayForm" value="${licenseData.enterKeysMode && !licenseData.allEnteredKeysValid ? 'block' : 'none'}"/>
    <form id="licensesForm" action="<c:url value='/admin/licenses.html'/>" style="display: ${displayForm}" method="post">

      <h2>Enter TeamCity License Keys</h2>

      <p class="licenseMessage">
        Please enter license keys below (place each license key on a separate line):
      </p>

      <textarea id="licenseKeys" name="licenseKeys" rows="5" cols="75">${licenseData.licenseKeys}</textarea>

      <div style="margin-top: 1em">
        <a class="btn btn_primary" href="#" id="saveKeys" name="saveKeys" onclick="BS.LicensesForm.submitLicenses(); return false">Save</a>
        <a class="btn" href="<c:url value='/admin/admin.html?item=license'/>">Cancel</a>
      </div>

      <input type="hidden" id="enterKeys" name="enterKeys" value="true"/>
      <input type="hidden" id="submitKeys" name="submitKeys" value="Save"/>
    </form>

    <div id="verificationStatus"></div>
  </div>
</div>
