<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ attribute name="url" fragment="false" required="true"
  %>
<iframe id="iframe" src="${url}" width="100%" height="600" frameborder="0"
onload="{
function resizeFrame() {
  var window = BS.Util.windowSize(window);
  var header = $('topWrapper').getDimensions();
  var height = window[1] - header.height - 200;
  $('iframe').style.height = height + 'px';
}

resizeFrame();
this.onload='javascript://';
Event.observe(window, 'resize', resizeFrame);
}"></iframe>
