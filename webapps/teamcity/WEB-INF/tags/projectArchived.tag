<%@ tag import="jetbrains.buildServer.util.StringUtil" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    attribute name="project" required="true" type="jetbrains.buildServer.serverSide.SProject"%><%@
    attribute name="style" required="false"
%><c:if test="${project.archived}">
  <div id="projectDetails" <c:if test="${style != null}">style="${style}"</c:if>>
    <img src="<c:url value='/img/buildStates/paused.png'/>" title="Project is archived" alt="||"/>
    <c:set var="archivingUser"><c:if test="${not empty project.archivingUser.name}">by <c:out value="${project.archivingUser.descriptiveName}"/></c:if></c:set>
    <span>Project was archived ${archivingUser} <%=StringUtil.elapsedTimeToString(project.getArchivingTime())%></span>
  </div>
</c:if>