<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@ 
    attribute name="hideIcon" type="java.lang.Boolean" required="false" 
    %><%@ attribute name="icon" required="true" %><c:if test="${not hideIcon}">class="iconLink" style="background-image: url(<c:url value='${icon}'/>);"</c:if>