<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    attribute name="build" required="true" rtexprvalue="true" type="jetbrains.buildServer.serverSide.SBuild" %><%@
    attribute name="showIcon" required="true" type="java.lang.Boolean" %><%@
    attribute name="compactView" required="true" type="java.lang.Boolean"

%><c:choose
    ><c:when test="${build.artifactsExists}"
        ><c:if test="${showIcon}"><bs:_artifactsIcon/></c:if
        > <bs:_artifactsLink build="${build}">${compactView ? 'View' : 'Artifacts'}</bs:_artifactsLink
    ></c:when
    ><c:when test="${build.hasInternalArtifactsOnly}"
        ><span class="none"><bs:_artifactsLink build="${build}"
                                               hiddenOnly="true">${compactView ? 'None' : 'No artifacts'}</bs:_artifactsLink></span>
    </c:when
    ><c:otherwise><span class="none">${compactView ? 'None' : 'No artifacts'}</span></c:otherwise
></c:choose>