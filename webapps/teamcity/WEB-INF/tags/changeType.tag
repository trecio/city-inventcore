<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
    %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
    %><%@ attribute name="modification" required="true" type="jetbrains.buildServer.vcs.SVcsModification"
    %>
<c:choose>
  <c:when test="${not modification.personal}">change</c:when>
  <c:otherwise>
    <c:if test="${modification.personalChangeInfo.commitType.commit}">pre-tested commit</c:if>
    <c:if test="${not modification.personalChangeInfo.commitType.commit}">remote run</c:if>
  </c:otherwise>
</c:choose>
