<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>
<%@attribute name="buildId" required="true" rtexprvalue="true"%>
<%@attribute name="pin" required="true" type="java.lang.Boolean"%>
<c:if test="${pin}"> 
onmouseover="BS.Util.show('pinLink${buildId}');"
onmouseout="BS.Util.hide('pinLink${buildId}');"
</c:if>