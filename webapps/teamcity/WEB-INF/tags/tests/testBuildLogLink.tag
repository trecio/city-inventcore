<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"%>

<%@attribute name="build" type="jetbrains.buildServer.serverSide.SBuild" required="true" %>
<%@attribute name="testRun" type="jetbrains.buildServer.serverSide.TestRunEx" required="true" %>

<c:url var="testBuildLogUrl"
       value="/viewLog.html?tab=buildLog&logTab=tree&filter=debug&expand=all&buildId=${build.buildId}#_focus=${testRun.testRunId}"/>
<a href="${testBuildLogUrl}" title="Click to show test in Build Log" <bs:iconLinkStyle icon="/img/showInBuildLog.png"/>>
  <jsp:doBody/>
</a>
