<%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %><%@
    attribute name="testBean" type="jetbrains.buildServer.serverSide.STest" required="true" %><%@
    attribute name="showPackage" type="java.lang.Boolean" required="false" %><%@
    attribute name="noClass" type="java.lang.Boolean" required="false" %><%@
    attribute name="project" type="jetbrains.buildServer.serverSide.SProject" required="false" %><%@
    attribute name="buildType" type="jetbrains.buildServer.serverSide.SBuildType" required="false"
    %><tt:testName testBean="${testBean}" showPackage="${showPackage}" noClass="${noClass}">
  <c:choose>
    <c:when test="${not empty buildType}"><tt:testDetailsLink testBean="${testBean}" buildType="${buildType}"/></c:when>
    <c:when test="${not empty project}"><tt:testDetailsLink testBean="${testBean}" project="${project}"/></c:when>
  </c:choose>
  <bs:currentMuteIcon test="${testBean}"/>
  <jsp:doBody/>
</tt:testName>
