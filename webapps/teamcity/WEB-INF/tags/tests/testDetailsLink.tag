<%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    attribute name="testBean" type="jetbrains.buildServer.serverSide.STest" required="true" %><%@
    attribute name="buildType" type="jetbrains.buildServer.serverSide.SBuildType" required="false" %><%@
    attribute name="project" type="jetbrains.buildServer.serverSide.SProject" required="false" %><%@
    attribute name="hideIcon" type="java.lang.Boolean" required="false" %><%@
    attribute name="showResponsible" type="java.lang.Boolean" required="false" %>
<%-- either project or buildType should be specified --%>
<c:url var="testDetailUrl" value="/project.html?tab=testDetails">
  <c:choose>
    <c:when test="${not empty project}">
      <c:param name="projectId" value="${project.projectId}"/>
    </c:when>
    <c:when test="${not empty buildType}">
      <c:param name="projectId" value="${buildType.projectId}"/>
      <c:param name="buildTypeId" value="${buildType.buildTypeId}"/>
    </c:when>
    <c:otherwise>
      <c:param name="projectId" value="${testBean.projectId}"/>
    </c:otherwise>
  </c:choose>
  <c:choose>
    <c:when test="${not empty testBean.testId}">
      <c:param name="testNameId" value="${testBean.testId}"/>
    </c:when>
    <c:otherwise>
      <c:param name="testName" value="${testBean.testName}"/>
    </c:otherwise>
  </c:choose>
</c:url>
<a href="${testDetailUrl}" title="View test details"
   <bs:iconLinkStyle icon="/img/testDetails.gif" hideIcon="${hideIcon}"/>
    ><jsp:doBody/></a>
