<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="l" tagdir="/WEB-INF/tags/layout" %><%@
    taglib prefix="util" uri="/WEB-INF/functions/util" %><%@
    taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@

    attribute name="groupId" type="java.lang.String" required="false" %><%@
    attribute name="withoutActions" required="true" %><%@
    attribute name="testBean" type="jetbrains.buildServer.web.problems.STestBean" %><%@

    attribute name="testMoreData" fragment="true" required="false" %><%@
    attribute name="testAfterName" fragment="true" required="false" %><%@

    attribute name="testLinkAttrs" required="false" %><%@

    attribute name="ignoreMuteScope" type="java.lang.Boolean" required="false" %><%@
    attribute name="showMuteFromTestRun" type="java.lang.Boolean" required="false" %><%@

    attribute name="showPackage" required="false" type="java.lang.Boolean" %><%@
    attribute name="checkboxRequired" required="false" type="java.lang.Boolean" %><%@
    attribute name="singleBuildTypeContext" required="false" type="java.lang.Boolean"

%><c:set var="buildsNum" value="${singleBuildTypeContext ? 1 : testBean.buildsCount}"
/><c:set var="test" value="${testBean.run}"
/><c:set var="build" value="${test.buildOrNull}"
/><c:set var="testDetailsUrlParams" value="testNameId=${testBean.run.test.testId}&builds="
/><c:forEach var="bld" items="${testBean.relatedBuilds}"
  ><c:set var="testDetailsUrlParams" value="${testDetailsUrlParams}${bld.buildId}."
/></c:forEach
><c:set var="testDetailsUrlParams" value="${testDetailsUrlParams}&projectId=${test.test.projectId}"
/><c:set var="link2stacktrace" value="${false}"
/><%--@elvariable id="linkToTestRequired" type="java.lang.Boolean"--%>
<%--@elvariable id="doNotHighlightMyInvestigation" type="java.lang.Boolean"--%>

<tr>
  <td class="testNamePart">
    <c:if test="${checkboxRequired}">
      <authz:authorize projectId="${test.test.projectId}" anyPermission="ASSIGN_INVESTIGATION,MANAGE_BUILD_PROBLEMS">
        <c:set var="test_uuid"><bs:id/></c:set>
        <forms:checkbox custom="true" name="tst-${test_uuid}" attrs="data-testId=\"${test.test.testId}\"" className="checkbox"/>
      </authz:authorize>
    </c:if>
    <c:if test="${linkToTestRequired}">
      <c:set var="link2stacktrace" value="${true}"/>
      <c:set var="testDetailsUrlParams" value="${null}"/>
    </c:if>
    <!-- This span is for #testNameId navigation to the test ONLY -->
    <span id="testNameId${test.test.testId}">
      <tt:testNameWithPopup testRun="${test}"
                            trimTestName="${true}"
                            testDetailsUrlParams="${testDetailsUrlParams}" link2Stacktrace="${link2stacktrace}"
                            showPackage="${showPackage}"
                            showProjectTestsInDialog="true"
                            testLinkAttrs="${testLinkAttrs}"
                            ignoreMuteScope="${ignoreMuteScope}" showMuteFromTestRun="${showMuteFromTestRun}"
                            groupIdForBulkMode="${withoutActions ? '' : groupId}" doNotShowBuildLogLink="${false}"
                            doNotHighlightMyInvestigation="${doNotHighlightMyInvestigation}"/>
      </span>
      <jsp:invoke fragment="testAfterName"/>
  </td>
  <td class="testFailedInPart">
    <c:set var="testRunCount" value="${fn:length(testBean.runs)}"/>
    <c:if test="${testRunCount > 1}">
      <c:choose>
        <c:when test="${testRunCount == testBean.buildTypeCount}"
            >${testBean.buildTypeCount} build configurations<%@include file="_buildTypesTooltip.jspf"
        %></c:when
        ><c:when test="${testBean.buildsCount > 1 and testBean.buildTypeCount > 1 and testBean.buildTypeCount == testBean.buildsCount}"
            >${fn:length(testBean.runs)} test failures in ${testBean.buildTypeCount} build configurations<%@include file="_buildTypesTooltip.jspf"
        %></c:when
        ><c:when test="${testRunCount == testBean.buildsCount and testBean.buildTypeCount > 1 and testBean.buildTypeCount < testBean.buildsCount}"
            >${testBean.buildsCount} builds of ${testBean.buildTypeCount} build configurations<%@include file="_buildTypesTooltip.jspf"
        %></c:when
        ><c:when test="${testBean.buildsCount > 1 and testBean.buildTypeCount > 1 and testBean.buildTypeCount < testBean.buildsCount}"
            >${fn:length(testBean.runs)} test failures in ${testBean.buildsCount} builds of ${testBean.buildTypeCount} build configurations<%@include file="_buildTypesTooltip.jspf"
        %></c:when
        ><c:when test="${testRunCount == testBean.buildsCount and testBean.buildTypeCount == 1}"
            >${testBean.buildsCount} builds</c:when
        ><c:when test="${testBean.buildsCount > 1 and testBean.buildTypeCount == 1}">${fn:length(testBean.runs)} test failures in ${testBean.buildsCount} builds</c:when
        ><c:when test="${testBean.buildsCount == 1}">${fn:length(testBean.runs)} test failures in one build</c:when
      ></c:choose>
    </c:if>
    <c:if test="${testRunCount == 1 and fn:length(testBean.buildTypes) > 0 and not singleBuildTypeContext}">
      <c:set var="_buildType" value="${testBean.buildTypes[0]}"/>
      Failed in <%@include file="_buildTypeLink.jspf" %>
    </c:if>
  </td>
</tr>
<jsp:invoke fragment="testMoreData"/>
<c:if test="${testBean.buildTypeCount > 1}">
  <c:set var="_lastBuild" value="${null}" scope="request"/>
</c:if>
<c:if test="${buildsNum == 1}">
  <c:set var="_lastBuild" value="${build}" scope="request"/>
</c:if>