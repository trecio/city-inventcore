<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@
    attribute name="test" type="jetbrains.buildServer.serverSide.STest" required="true" %><%@
    attribute name="buildId" type="java.lang.String" required="false" %><%@
    attribute name="withFix" type="java.lang.Boolean" required="false"

%><c:if test="${withFix}">
<authz:authorize projectId="${test.projectId}" anyPermission="ASSIGN_INVESTIGATION">
  <jsp:attribute name="ifAccessGranted">
    <a href="#" title="Fix and unmute test"
       onclick="return BS.BulkInvestigateDialog.showForTest('${test.testId}', '${buildId}', '${test.projectId}', true);">Fix...</a> <span class="separator">|</span>
  </jsp:attribute>
</authz:authorize></c:if>
<tt:testInvestigationLink
    onclick="return BS.BulkInvestigateDialog.showForTest('${test.testId}', '${buildId}', '${test.projectId}');"
    projectId="${test.projectId}"/>