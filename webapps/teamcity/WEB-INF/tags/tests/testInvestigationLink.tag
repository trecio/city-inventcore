<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@
    attribute name="onclick" required="true" %><%@
    attribute name="projectId" required="true"
%><authz:authorize projectId="${projectId}" allPermissions="ASSIGN_INVESTIGATION,MANAGE_BUILD_PROBLEMS">
  <jsp:attribute name="ifAccessGranted">
    <a class="bulk-operation-link" href="#" title="Assign a user to investigate tests or mute them"
       onclick="${onclick}">Investigate / Mute...</a>
  </jsp:attribute>
  <jsp:attribute name="ifAccessDenied">
    <authz:authorize projectId="${projectId}" allPermissions="MANAGE_BUILD_PROBLEMS">
      <jsp:attribute name="ifAccessGranted">
        <a class="bulk-operation-link" href="#" title="Mute test failures" onclick="${onclick}">Mute...</a>
      </jsp:attribute>
      <jsp:attribute name="ifAccessDenied">
        <a class="bulk-operation-link" href="#" title="Assign a user to investigate tests" onclick="${onclick}">Investigate...</a>
      </jsp:attribute>
    </authz:authorize>
  </jsp:attribute>
</authz:authorize>