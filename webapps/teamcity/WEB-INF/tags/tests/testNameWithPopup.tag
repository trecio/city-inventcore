<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags" %><%@
    taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %><%@
    taglib prefix="authz" tagdir="/WEB-INF/tags/authz" %><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%@
    attribute name="testRun" type="jetbrains.buildServer.serverSide.TestRunEx" required="true" %><%@
    attribute name="showPackage" type="java.lang.Boolean" required="false" %><%@
    attribute name="trimTestName" type="java.lang.Boolean" required="false" %><%@
    attribute name="maxTestNameLength" type="java.lang.Integer" required="false" %><%@
    attribute name="noClass" type="java.lang.Boolean" required="false" %><%@
    attribute name="hideResponsibileIcon" type="java.lang.Boolean" required="false" %><%@
    attribute name="hideMuteIcon" type="java.lang.Boolean" required="false" %><%@
    attribute name="link2Stacktrace" type="java.lang.Boolean" required="false" %><%@
    attribute name="testDetailsUrlParams" type="java.lang.String" required="false" %><%@
    attribute name="testLinkAttrs" type="java.lang.String" required="false" %><%@
    attribute name="ignoreMuteScope" type="java.lang.Boolean" required="false" %><%@
    attribute name="showMuteFromTestRun" type="java.lang.Boolean" required="false" %><%@
    attribute name="showProjectTestsInDialog" type="java.lang.Boolean" required="false" %><%@
    attribute name="groupIdForBulkMode" type="java.lang.String" required="false" %><%@
    attribute name="doNotHighlightMyInvestigation" type="java.lang.String" required="false"%><%@
    attribute name="doNotShowBuildLogLink" type="java.lang.Boolean" required="false"

%><c:set var="popupId">testActionsPopup<%= (long)Math.abs(Math.random() * Long.MAX_VALUE) %></c:set
><c:set var="build" value="${testRun.buildOrNull}"
/><c:set var="test" value="${testRun.test}"
/><c:set var="resp" value="${test.responsibility}"
/><c:if test="${empty showPackage}"><c:set var="showPackage" value="${true}"/></c:if
><c:set var="testNameClass" value="${testRun.newFailure ? 'newTestFailure' : ''}"
/><c:if test="${not doNotHighlightMyInvestigation and currentUser.highlightRelatedDataInUI and
                not empty resp and resp.responsibleUser == currentUser and resp.state.active}"
    ><c:set var="testNameClass" value="${testNameClass} highlightChanges"
/></c:if

><bs:simplePopup controlId="${popupId}"
                 controlClass="testNamePopup ${testNameClass}"
                 popup_options="shift: {x: -30, y: 15}">
  <jsp:attribute name="content">
    <table class="testActionsPopup">
      <tr><td>
        <tt:testDetailsLink testBean="${test}">Test Details</tt:testDetailsLink>
      </td></tr>
      <authz:authorize projectId="${test.projectId}" anyPermission="ASSIGN_INVESTIGATION,MANAGE_BUILD_PROBLEMS">
        <jsp:attribute name="ifAccessGranted">
          <tr><td>
            <img src="<c:url value="/img/investigate.gif"/>" class="actionPopupIcon" alt=""/>
            <tt:testInvestigationLinks test="${test}"
                                       buildId="${build.buildId}"
                                       withFix="${not empty resp && resp.state.active}"/>
          </td></tr>
        </jsp:attribute>
      </authz:authorize>
      <c:if test="${not empty build and testRun.status.failed}">
        <tr><td>
          <tt:activateTestLink build="${build}" testId="${testRun.testRunId}">Open in IDE</tt:activateTestLink>
        </td></tr>
      </c:if>
      <c:if test="${not empty build and not doNotShowBuildLogLink eq 'true'}">
        <tr><td>
          <tt:testBuildLogLink build="${build}" testRun="${testRun}">Show in Build Log</tt:testBuildLogLink>
        </td></tr>
      </c:if>
    </table>
  </jsp:attribute>

  <jsp:body>
    <c:set var="body">
      <tt:testName testBean="${test}" trimTestName="${trimTestName}"
                   maxTestNameLength="${maxTestNameLength}"
                   noClass="${noClass}" showPackage="${showPackage}"/>
    </c:set>

    <c:if test="${testRun.status.failed}">
      <c:choose>
        <c:when test="${link2Stacktrace}">
          <c:set var="body">
            <tt:testStatusDetailsLink testNameId="${test.testId}"
                                      buildData="${build}"
                                      attrs="${testLinkAttrs}">${body}</tt:testStatusDetailsLink>
          </c:set>
        </c:when>
        <c:when test="${not empty testDetailsUrlParams}">
          <c:set var="body">
            <c:set var="onclick"
                   value="BS.TestDetails.toggleDetails(this, '/change/testDetails.html?${testDetailsUrlParams}'); return false;"/>
            <a class="testWithDetails" onclick="${onclick}" title="Show related test details" href="#testNameId${testRun.test.testId}">${body}</a>
          </c:set>
        </c:when>
      </c:choose>
    </c:if>

    <c:if test="${testRun.fixed && !build.personal}">
      <c:set var="body"><s class="fixed" title="Test is already fixed">${body}</s></c:set>
    </c:if>

    <c:if test="${not hideResponsibileIcon}"
        ><bs:responsibleIcon responsibility="${resp}"
                             test="${test}"
                             style="cursor: auto;"
    /></c:if><c:if test="${not hideMuteIcon}"
      ><bs:testRunMuteIcon testRun="${testRun}"
                           btScope="${not empty build ? build.buildType : null}"
                           ignoreMuteScope="${ignoreMuteScope}"
                           showMuteFromTestRun="false"
      /></c:if>

    ${body}
  </jsp:body>
</bs:simplePopup
    ><c:if test="${showMuteFromTestRun and testRun.muted}">
  <c:set var="muteInfo" value="${testRun.muteInfo}"/>
  &mdash; Muted on <bs:date value="${muteInfo.mutingTime}"/> by <c:out value="${muteInfo.mutingUser.descriptiveName}"/>
  <c:if test="${not empty muteInfo.mutingComment}">
    with comment: <i><bs:out value="${muteInfo.mutingComment}"/></i>
  </c:if>
</c:if>
