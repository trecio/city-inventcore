<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %><%@
    taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%@
    taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%><%@
    attribute name="levelToSet" required="true" %><%@
    attribute name="testItems" required="true" type="java.util.List" %><%@
    attribute name="buildData" required="true" type="jetbrains.buildServer.serverSide.SBuild" %>
<c:forEach items="${testItems}" var="testRun">
  <%--@elvariable id="testRun" type="jetbrains.buildServer.serverSide.STestRun"--%>
  <c:set var="rowClass"><c:if test="${testRun.newFailure}">class="failedNewTst" title="This is a new failed test"</c:if></c:set>
  <tr ${rowClass}>
    <td class="test-status"><tt:testStatus testRun="${testRun}"/></td>
    <td class="nameT">
      <bs:responsibleIcon responsibility="${testRun.test.responsibility}"
                          test="${testRun.test}"
                          style="cursor: auto;"/>
      <bs:testRunMuteIcon testRun="${testRun}"
                          btScope="${testRun.build.buildType}"
                          ignoreMuteScope="false"
                          showMuteFromTestRun="false"/>
      <c:set var="group" value="${testRun.test.name.groupName}"/>
      <tt:classLink group="${group}">.</tt:classLink>
      <tt:testNameWithPopup testRun="${testRun}" showPackage="false" noClass="true"
                            hideResponsibileIcon="true" hideMuteIcon="true"
                            link2Stacktrace="${testRun.status.failed}"
                            doNotHighlightMyInvestigation="true"/>
      <c:if test="${group.packageSet or group.suiteSet}">
        <span class="package">
          (<tt:suiteLink group="${group}" levelToSet="${levelToSet}"><c:if test="${group.packageSet}">:</c:if></tt:suiteLink><tt:packageLink group="${group}" levelToSet="${levelToSet}"/>)
        </span>
      </c:if>
    </td>
    <td class="duration"><tt:duration duration='${testRun.duration}'/>
      <a href="#" onclick="return false;" title="View test duration graph" id="trends${testRun.testRunId}"><bs:icon icon="../testDetails/testDuration.png" alt="View trend"/></a>
    </td>
    <td class="orderNum"><c:out value="${testRun.orderId}"/></td>
  </tr>
</c:forEach>
