<%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
  %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
  %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
  %><%@ taglib prefix="afn" uri="/WEB-INF/functions/authz"
  %><%@ taglib prefix="authz" tagdir="/WEB-INF/tags/authz"
  %><%@ taglib prefix="util" uri="/WEB-INF/functions/util"
  %><%@ attribute name="buildType" rtexprvalue="true" type="jetbrains.buildServer.serverSide.SBuildType" required="true"
  %><%@ attribute name="popupMode" required="false"
  %><%@ attribute name="style" required="false"
  %><%@ attribute name="target" required="false"
  %><%@ attribute name="dontShowProjectName" required="false" type="java.lang.Boolean"
  %><%@ attribute name="projectText" required="false"

%><c:if test="${empty popupMode}"><c:url value="/project.html?projectId=${buildType.projectId}" var="projectUrl"
  /><c:if test="${dontShowProjectName != true}"><bs:projectLink project="${buildType.project}" style="${style}" target="${target}"><c:out value="${projectText}"></c:out></bs:projectLink> :: </c:if
  ><bs:buildTypeLink buildType="${buildType}" style="${style}" target="${target}"
/></c:if

><c:if test="${not empty popupMode}"
  ><c:set var="withAdmin" value="false"
    /><authz:editBuildTypeGranted buildType="${buildType}"
      ><c:set var="withAdmin" value="true"
    /></authz:editBuildTypeGranted
  ><c:set var="linkContent"><jsp:doBody/></c:set
  ><c:if test="${empty linkContent}"
    ><c:set var="linkContent"><c:if test="${dontShowProjectName != true}"><c:out value="${buildType.projectName}"/> :: </c:if><c:out value="${buildType.name}"/></c:set
  ></c:if
  ><c:set var="showResponsibility" value="${afn:permissionGrantedForBuildType(buildType, 'ASSIGN_INVESTIGATION')}"
 /><bs:popupControl
  showPopupCommand="BS.BuildTypeSummary.showSummaryPopup(this, '${buildType.buildTypeId}', '${buildType.projectId}', ${popupMode != 'no_self'}, ${withAdmin}, ${showResponsibility}, '${util:forJS(buildType.name, true, true) }');"
  hidePopupCommand="BS.BuildTypeSummary.hidePopup();"
  stopHidingPopupCommand="BS.BuildTypeSummary.stopHidingPopup();"
  controlId="btpp${buildType.buildTypeId}"
  clazz="btPopup"
  >${linkContent}</bs:popupControl>
</c:if>