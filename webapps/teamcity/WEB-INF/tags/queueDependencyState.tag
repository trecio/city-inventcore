<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
    %><%@ taglib prefix="bs" tagdir="/WEB-INF/tags"
    %><%@ taglib prefix="queue" tagdir="/WEB-INF/tags/queue"%>
<%@attribute name="dependency" type="jetbrains.buildServer.serverSide.BuildPromotion" required="true" %>
<c:set var="buildData" value="${dependency.associatedBuild}"/>
<c:set var="queuedBuild" value="${dependency.queuedBuild}"/>
<c:if test="${empty buildData}">
  <c:choose>
    <c:when test="${dependency.personal}"><bs:icon icon="personal/personalPending.gif"/></c:when>
    <c:otherwise><bs:icon icon="pending.gif"/></c:otherwise>
  </c:choose>
</c:if>
<c:if test="${not empty buildData}">
  <bs:buildDataIcon buildData="${buildData}"/>
</c:if>

<label for="bi${dependency.id}"><c:out value="${dependency.buildType.fullName}"/></label>

<c:if test="${not empty buildData}">
  <span class="separator">|</span>
  <c:set var="branch" value="${buildData.branch}"/>
  <c:if test="${not empty branch}">
    <span class="branch hasBranch ${branch.defaultBranch ? 'default' : ''}">
      <span class="branchName"><bs:trimBranch branch="${branch}" defaultMaxLength="15"/></span>
    </span>
  </c:if>
  <bs:resultsLink build="${buildData}" noTitle="true" noPopup="true"><bs:buildNumber buildData="${buildData}"/></bs:resultsLink>
</c:if>

<div class="estimateDetails">
  <c:if test="${not empty buildData and not buildData.finished}">
    Completed: ${buildData.completedPercent}%
  </c:if>

  <c:if test="${not empty queuedBuild}">
    <c:set var="est" value="${queuedBuild.buildEstimates.timeInterval}"/>
    <c:if test="${not empty est and not empty est.durationSeconds}">
      Time to start: <bs:printTime time="${est.durationSeconds}"/>
    </c:if>
  </c:if>
</div>
