<%@ include file="/include-internal.jsp"%>
<jsp:useBean id="params" type="jetbrains.buildServer.controllers.RunBuildParameters" scope="request"/>

<div id="properties-tab" style="display: none;" class="tabContent">
  <table class="runnerFormTable wideLabel buildParameters">
    <tbody style="${params.customRunDialogForRunButton ? 'display:none' : ''}">
    <tr>
      <td colspan="3" class="noBorder">
        <select id="customBuild-paramType" name="paramType">
          <option value="conf">Configuration parameter</option>
          <option value="system">System property</option>
          <option value="env">Environment variable</option>
        </select>
      </td>
    </tr>
    <tr class="addRow">
      <td class="paramName noBorder">
        <div class="completionIconWrapper">
          <forms:textField name="customBuild-parameterName" value="" style="width: 100%;"/>
        </div>
      </td>
      <td class="paramValue noBorder">
        <div class="completionIconWrapper">
          <forms:textField name="customBuild-parameterValue" style="width: 100%" value="" noAutoComplete="true" className="buildTypeParams" expandable="true"/>
        </div>
      </td>
      <td class="edit noBorder">
        <input type="button" class="btn btn_mini" value="add" onclick="BS.RunBuildDialog.addParameter()"/>
      </td>
    </tr>
    </tbody>
    <c:if test="${params.customRunDialogForRunButton}">
      <tr>
        <td colspan="3">
          The below parameters are marked as necessary for review
        </td>
      </tr>
    </c:if>
    <c:forEach var="group" items="${params.parameterGroups}">
      <bs:changeRequest key="group" value="${group}">
        <jsp:include page="runCustomBuildParametersContent.jsp"/>
      </bs:changeRequest>
    </c:forEach>
  </table>
</div>


<div id="newParamTemplate"><!--
  <tr class="modifiedParam">
      <td class="paramName"><forms:textField name="[prefix]name" value="" style="width: 100%" defaultText="<Name>"/></td>
      <td class="paramValue"><div class="completionIconWrapper"><textarea rows="5" cols="30" style="margin:0; padding:0; resize: vertical; width: 100%;" class="one_line buildTypeParams" name="[prefix]value" id="[prefix]value[id]" wrap="soft" autocomplete="off"></textarea></div></td>
      <td class="edit"><a href="#" onclick="BS.RunBuildDialog.removeParameter(this.parentNode.parentNode); return false">delete</a></td>
  </tr>
  --></div>

<c:if test="${params.containsRequiredParameters}">
  <script type="text/javascript">
    BS.Util.show('customRunPropertiesRequired');
  </script>
</c:if>