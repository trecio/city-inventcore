<%@ page import="jetbrains.buildServer.serverSide.TimePoint" %>
<%@
    include file="include-internal.jsp" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %><%@
    taglib prefix="tags" tagdir="/WEB-INF/tags/tags" %><%@
    taglib prefix="q" tagdir="/WEB-INF/tags/queue"

%><jsp:useBean id="modification" type="jetbrains.buildServer.vcs.SVcsModification" scope="request"
/><jsp:useBean id="changeStatus" type="jetbrains.buildServer.vcs.ChangeStatus" scope="request"
/><jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.SBuildType" scope="request"
/><jsp:useBean id="currentUser" type="jetbrains.buildServer.users.User" scope="request"


/><c:set var="changeBtStatus" value="${changeStatus.buildTypesStatus[buildType]}" scope="request"
/><c:set var="firstBuild" value="${changeBtStatus.firstBuild}" scope="request"
/><c:set var="queuedBuild" value="${changeBtStatus.queuedBuild}"
/><c:set var="currentBuild" value="${changeBtStatus.currentBuild}" scope="request"
/><c:set var="neverInterval" value="<%= TimePoint.NEVER%>"

/><!-- To enable hide successful option: -->
<%--@elvariable id="hideSuccessfulProperty" type="java.lang.Boolean"--%>
<c:if test="${not empty param.show_all_builds}"><c:set var="disableHidingBuilds" value="true"/></c:if>
<c:if test="${empty hideSuccessfulProperty}"
  ><c:set var="hideSuccessfulProperty" value="${ufn:booleanPropertyValue(currentUser, 'changePage_hideSuccessful') and not disableHidingBuilds}"
/></c:if
><c:set var="no_display"><c:if test="${hideSuccessfulProperty and not changeBtStatus.failed}">style='display: none;'</c:if></c:set
><tr id="viewModificationBuildType_${buildType.buildTypeId}_<bs:_csId changeStatus="${changeStatus}"/>" ${no_display} style="vertical-align:top;"
    class="buildTypeProblem ${firstBuild == null ? 'pendingBuild' : ''}">
  <td class="name">
    <bs:responsibleIcon responsibility="${buildType.responsibilityInfo}"/>
    <bs:buildTypeLinkFull buildType="${buildType}" projectText="${buildType.project.extendedName}"/>
  </td>
  <td>
    <c:choose>
      <c:when test="${not empty firstBuild}">
        <c:if test="${firstBuild != currentBuild}">
          <c:set var="build" value="${currentBuild}"/>
          <div class="buildPrefix">Current:</div>
          <%@ include file="changeBuild.jspf" %>
        </c:if>
        <c:set var="build" value="${firstBuild}"/>
        <div class="buildPrefix">First run:</div>
        <%@ include file="changeBuild.jspf" %>
      </c:when>
      <c:when test="${not empty queuedBuild}">
        <div class="buildPrefix">Queued:</div>
        <bs:icon icon="pending.gif" alt="Build is in queue"/>

        <c:set var="estimates" value="${queuedBuild.buildEstimates}"/>
        <c:set var="queueBuildTypeId" value="${buildType.personal ? buildType.sourceBuildType.buildTypeId : buildType.buildTypeId}"/>
        <c:choose>
          <c:when test="${not empty estimates}">
            <c:set var="estimatedStart"><c:if test="${not empty estimates.timeInterval && estimates.timeInterval.startPoint != neverInterval}">
              ${estimates.timeInterval.startPoint.relativeSeconds > 0 ? "Will start" : "Should have started"}

              <q:queueLink itemId="${queueBuildTypeId}"><bs:date value="${estimates.timeInterval.startPoint.absoluteTime}" smart="true"/></q:queueLink>

              ${estimates.delayed ? "<em>(delayed)</em>" : ""}
            </c:if></c:set>

            ${estimatedStart}
            <c:if test="${empty estimatedStart}">
              <q:queueLink itemId="${queueBuildTypeId}">Start time unknown</q:queueLink>
            </c:if>

            <em>${not empty estimates.waitReason ? estimates.waitReason.description : ""}</em>
          </c:when>
          <c:otherwise>
            <q:queueLink itemId="${queueBuildTypeId}">Cannot start</q:queueLink>
          </c:otherwise>
        </c:choose>
      </c:when>
      <c:otherwise>
        <div class="buildPrefix">&nbsp;</div>
        <bs:icon icon="pending.gif" alt="Build is not triggered yet"/>
        <em>Not triggered</em>
      </c:otherwise>
    </c:choose>
  </td>
</tr>