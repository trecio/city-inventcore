<%@ page import="static jetbrains.buildServer.notification.NotificationRule.Event.*" %>
<%@ page import="jetbrains.buildServer.notification.WatchType" %>
<%@ include file="include-internal.jsp"%>
<%@ taglib prefix="profile" tagdir="/WEB-INF/tags/userProfile"%>
<jsp:useBean id="notificationRulesForm" type="jetbrains.buildServer.controllers.profile.notifications.NotificationRulesForm" scope="request"/>
<jsp:useBean id="ruleBean" type="jetbrains.buildServer.controllers.profile.notifications.NotificationRulesForm.EditableNotificationRule" scope="request"/>

<script type="text/javascript">
  _.extend(BS.NotificationRuleForm, {
    updateWatchType: function() {
      $('watchType').value = '';
      if ($('watchTypeCommitter').checked) {
        $('watchType').value = '<%=WatchType.BUILDS_WITH_USER_CHANGES.name()%>';
        return;
      }

      var projectSelector = $('project');
      if ($('watchTypeProject').checked && projectSelector.selectedIndex == 0) {
        $('watchType').value = '<%=WatchType.ALL_PROJECTS.name()%>';
        return;
      }

      if ($('watchTypeProject').checked && projectSelector.selectedIndex > 0) {
        $('watchType').value = '<%=WatchType.SPECIFIC_PROJECT.name()%>';
        return;
      }

      if ($('watchTypeBuildConfigurations').checked) {
        $('watchType').value = '<%=WatchType.SPECIFIC_PROJECT_BUILD_TYPES.name()%>';
        return;
      }

      if ($('watchTypeSystemWide').checked) {
        $('watchType').value = '<%=WatchType.SYSTEM_WIDE.name()%>';
      }
    }
  });

  $j(document).ready(function() {
    $('watchTypeCommitter').scrollTo();

    if ($('watchTypeSystemWide').checked) {
      BS.NotificationRuleForm.switchRightPanel(true);
    }
  });
</script>
<h2>
  <c:choose>
    <c:when test="${ruleBean.newRule}">Add New Rule</c:when>
    <c:otherwise>Edit Rule</c:otherwise>
  </c:choose>
</h2>
<form id="notificationRuleForm" style="margin-top:0;" action="<c:url value='/notificationRules.html'/>" method="post" onsubmit="return BS.NotificationRuleForm.submitRule();">
<div class="panelsContainer">
    <div class="leftPanel">
    <h3>Watch:</h3>
    <div class="watchTypeCommitterContainer">
      <input type="radio" name="watchType1" id="watchTypeCommitter" <c:if test="${ruleBean.committerRule}">checked</c:if> onclick="{
        BS.NotificationRuleForm.enableEvent('BUILD_FINISHED_NEW_FAILURE');
        BS.NotificationRuleForm.disableEvent('MUTE_UPDATED');
        BS.NotificationRuleForm.uncheckEvent('MUTE_UPDATED');
        BS.NotificationRuleForm.switchRightPanel(false);
        BS.NotificationRuleForm.updateWatchType();
      }"><label for="watchTypeCommitter">Builds affected by my changes</label>
    </div>
    <div class="watchTypeProject">
      <input type="radio" name="watchType1" id="watchTypeProject" <c:if test="${ruleBean.projectBasedRule}">checked</c:if> onclick="{
        // Replace this line with `$('project').focus();` if UFD is not used here.
        jQuery('#project').ufd('inputFocus');
        BS.NotificationRuleForm.disableEvent('BUILD_FINISHED_NEW_FAILURE');
        BS.NotificationRuleForm.uncheckEvent('BUILD_FINISHED_NEW_FAILURE');
        BS.NotificationRuleForm.enableEvent('MUTE_UPDATED');
        BS.NotificationRuleForm.switchRightPanel(false);
        BS.NotificationRuleForm.updateWatchType();
      }"><label for="watchTypeProject">Builds from the project:</label>
    </div>
    <div class="projectSelector">
      <forms:select id="project" name="projectId" onchange="setTimeout(function() { $('watchTypeProject').click(); BS.NotificationRuleForm.updateWatchType(); }, 10)" enableFilter="true">
        <option value="" <c:if test="${ruleBean.watchType == 'ALL_PROJECTS'}">selected="selected"</c:if>>&lt;All projects></option>
        <c:forEach items="${notificationRulesForm.projects}" var="project">
        <option value="${project.projectId}" <c:if test="${project.projectId == ruleBean.projectId}">selected="selected"</c:if>><c:out value="${project.extendedName}"/></option>
        </c:forEach>
      </forms:select>
      <span class="error" style="margin-left: 8.5em;" id="errorProject"></span>
    </div>
    <div class="watchTypeBuildConfigurations">
      <input type="radio" name="watchType1" id="watchTypeBuildConfigurations" <c:if test="${not ruleBean.committerRule and not ruleBean.projectBasedRule}">checked</c:if> onclick="{
        $('configurations').focus();
        BS.NotificationRuleForm.disableEvent('BUILD_FINISHED_NEW_FAILURE');
        BS.NotificationRuleForm.uncheckEvent('BUILD_FINISHED_NEW_FAILURE');
        BS.NotificationRuleForm.enableEvent('MUTE_UPDATED');
        BS.NotificationRuleForm.switchRightPanel(false);
        BS.NotificationRuleForm.updateWatchType();
      }"><label for="watchTypeBuildConfigurations">Builds from the selected build configurations:</label>
    </div>
    <div class="configurationSelector">
      <bs:inplaceFilter containerId="configurations" activate="true" filterText="&lt;filter build configurations>"/>
      <select id="configurations" name="buildTypeId" size="8" multiple="multiple" onchange="$('watchTypeBuildConfigurations').click(); BS.NotificationRuleForm.updateWatchType();">
        <c:forEach items="${notificationRulesForm.allBuildTypes}" var="entry">
          <optgroup class="inplaceFiltered" label="${entry.key.name}">
            <c:forEach items="${entry.value}" var="buildType">
              <option class="inplaceFiltered" value="${buildType.buildTypeId}" data-title="${entry.key.name} :: ${buildType.name}"
                <c:if test="${ruleBean.selectedBuildTypesMap[buildType.buildTypeId]}">selected="selected"</c:if>>
                <c:out value="${buildType.name}"/>
              </option>
            </c:forEach>
          </optgroup>
        </c:forEach>
      </select>
      <input type="hidden" name="_buildTypeId" value=""/>
      <input type="hidden" id="watchType" name="watchType" value="${ruleBean.watchType}"/>
      <span class="error" style="margin-left: 8.5em;" id="errorBuildType"></span>
    </div>

    <div class="watchTypeSystemWide">
      <input type="radio" name="watchType1" id="watchTypeSystemWide" <c:if test="${ruleBean.systemWideRule}">checked</c:if> onclick="{
        BS.NotificationRuleForm.switchRightPanel(true);
        BS.NotificationRuleForm.updateWatchType();
      }"><label for="watchTypeSystemWide">System wide events</label>
    </div>
  </div>

  <div class="rightPanel">
    <h3>Send notification when:</h3>

    <table class="eventsTable" id="non-system-events">
      <tr>
        <td class="buildFailed"><forms:checkbox name="editingEvents['BUILD_FINISHED_FAILURE']" checked="${ruleBean.editingEvents['BUILD_FINISHED_FAILURE']}"
                                                onclick="if (!this.checked) { BS.NotificationRuleForm.uncheckEvent('BUILD_FINISHED_NEW_FAILURE'); BS.NotificationRuleForm.uncheckEvent('FIRST_FAILURE_AFTER_SUCCESS'); }"/><label for="editingEvents['BUILD_FINISHED_FAILURE']">Build fails</label></td>
      </tr>
      <tr>
        <td class="eventOption"><forms:checkbox name="editingEvents['BUILD_FINISHED_NEW_FAILURE']"
                                                checked="${ruleBean.editingEvents['BUILD_FINISHED_NEW_FAILURE']}"
                                                disabled="${not ruleBean.committerRule}"
                                                onclick="if (this.checked) BS.NotificationRuleForm.checkEvent('BUILD_FINISHED_FAILURE');"/><label for="editingEvents['BUILD_FINISHED_NEW_FAILURE']">Ignore failures not caused by my changes</label></td>
      </tr>
      <tr>
        <td class="eventOption"><forms:checkbox name="editingEvents['FIRST_FAILURE_AFTER_SUCCESS']"
                                                checked="${ruleBean.editingEvents['FIRST_FAILURE_AFTER_SUCCESS']}"
                                                onclick="if (this.checked) BS.NotificationRuleForm.checkEvent('BUILD_FINISHED_FAILURE');"/><label for="editingEvents['FIRST_FAILURE_AFTER_SUCCESS']">Only notify on the first failed build after successful</label></td>
      </tr>
      <tr>
        <td><forms:checkbox name="editingEvents['BUILD_FINISHED_SUCCESS']" checked="${ruleBean.editingEvents['BUILD_FINISHED_SUCCESS']}"
                            onclick="if (!this.checked) BS.NotificationRuleForm.uncheckEvent('FIRST_SUCCESS_AFTER_FAILURE');"/><label for="editingEvents['BUILD_FINISHED_SUCCESS']">Build is successful</label></td>
      </tr>
      <tr>
        <td class="eventOption"><forms:checkbox name="editingEvents['FIRST_SUCCESS_AFTER_FAILURE']"
                                                checked="${ruleBean.editingEvents['FIRST_SUCCESS_AFTER_FAILURE']}"
                                                onclick="if (this.checked) BS.NotificationRuleForm.checkEvent('BUILD_FINISHED_SUCCESS');"/><label for="editingEvents['FIRST_SUCCESS_AFTER_FAILURE']">Only notify on the first successful build after failed</label></td>
      </tr>
      <tr>
        <td class="startOfGroup"><forms:checkbox name="editingEvents['BUILD_FAILING']" checked="${ruleBean.editingEvents['BUILD_FAILING']}"/><label for="editingEvents['BUILD_FAILING']">The first build error occurs</label></td>
      </tr>
      <tr>
        <td><forms:checkbox name="editingEvents['BUILD_STARTED']" checked="${ruleBean.editingEvents['BUILD_STARTED']}"/><label for="editingEvents['BUILD_STARTED']">Build starts</label></td>
      </tr>
      <tr>
        <td><forms:checkbox name="editingEvents['BUILD_FAILED_TO_START']" checked="${ruleBean.editingEvents['BUILD_FAILED_TO_START']}"
          /><label for="editingEvents['BUILD_FAILED_TO_START']">Build fails to start</label></td>
      </tr>
      <tr>
        <td><forms:checkbox name="editingEvents['BUILD_PROBABLY_HANGING']" checked="${ruleBean.editingEvents['BUILD_PROBABLY_HANGING']}"/><label for="editingEvents['BUILD_PROBABLY_HANGING']">Build is probably hanging</label></td>
      </tr>
      <tr>
        <td class="startOfGroup"><forms:checkbox name="editingEvents['RESPONSIBILITY_CHANGES']" checked="${ruleBean.editingEvents['RESPONSIBILITY_CHANGES']}"/><label for="editingEvents['RESPONSIBILITY_CHANGES']">Investigation is updated</label></td>
      </tr>
      <tr>
        <td class="startOfGroup"><forms:checkbox name="editingEvents['MUTE_UPDATED']" disabled="${ruleBean.committerRule}" checked="${ruleBean.editingEvents['MUTE_UPDATED']}"/><label for="editingEvents['MUTE_UPDATED']">Tests are muted or unmuted</label></td>
      </tr>
    </table>

    <table class="eventsTable" id="system-wide-events" style="display: none;">
      <tr>
        <td><forms:checkbox name="editingEvents['RESPONSIBILITY_ASSIGNED']" checked="${ruleBean.editingEvents['RESPONSIBILITY_ASSIGNED']}"/><label for="editingEvents['RESPONSIBILITY_ASSIGNED']">Investigation assigned to me</label></td>
      </tr>
    </table>
  </div>
  </div>

  <div class="saveButtonsBlock">
    <forms:cancel onclick="BS.NotificationRuleForm.cancelEditing()"/>
    <forms:submit label="Save"/>
    <forms:saving/>
  </div>

  <input type="hidden" name="id" value="${ruleBean.id}"/>
  <input type="hidden" name="notificatorType" value="${notificationRulesForm.notificatorType}"/>
  <input type="hidden" name="holderId" value="${notificationRulesForm.editeeId}"/>
  <input type="hidden" name="submitRule" value="save"/>
</form>
