<%@ include file="/include-internal.jsp"
%><jsp:useBean id="currentUser" type="jetbrains.buildServer.users.SUser"  scope="request"
/><jsp:useBean id="project" type="jetbrains.buildServer.serverSide.SProject"  scope="request"
/><jsp:useBean id="projectBuildTypes" type="java.util.List<jetbrains.buildServer.serverSide.SBuildType>" scope="request"
/><c:set var="projectId" value="${project.projectId}"/>
<et:subscribeOnProjectEvents projectId="${projectId}">
  <jsp:attribute name="eventNames">
    BUILD_STARTED
    BUILD_CHANGES_LOADED
    BUILD_FINISHED
    BUILD_INTERRUPTED
    BUILD_TYPE_ACTIVE_STATUS_CHANGED
    BUILD_TYPE_ADDED_TO_QUEUE
    BUILD_TYPE_REMOVED_FROM_QUEUE
    BUILD_TYPE_REGISTERED
    BUILD_TYPE_UNREGISTERED
    CHANGE_ADDED
    PROJECT_PERSISTED
    PROJECT_RESTORED
  </jsp:attribute>
  <jsp:attribute name="eventHandler">
    <c:set var="firstProjectInList" value="${!!firstProjectInList}"/>
    BS.Projects.updateProjectView('${projectId}', ${firstProjectInList});
  </jsp:attribute>
</et:subscribeOnProjectEvents>
<et:subscribeOnProjectEvents projectId="${projectId}">
  <jsp:attribute name="eventNames">
    PROJECT_REMOVED
    PROJECT_ARCHIVED
    PROJECT_DEARCHIVED
  </jsp:attribute>
  <jsp:attribute name="eventHandler">
    BS.reload();
  </jsp:attribute>
</et:subscribeOnProjectEvents

><div class="build ${empty overviewPage ? 'project-page' : 'overview-page'}" id="p_${projectId}">
  <bs:trimWhitespace>
    <span style="display:none;"><!-- ================ Project block header: --><c:out value="${project.name}"/></span>

    <%--@elvariable id="overviewPage" type="java.lang.Boolean"--%>
    <%--@elvariable id="hasFilteredConfigurations" type="java.lang.Boolean"--%>
    <%--@elvariable id="branchBean" type="jetbrains.buildServer.controllers.BranchBean"--%>
    <%--@elvariable id="projectStatusDetails" type="jetbrains.buildServer.controllers.overview.ProjectStatusDetails"--%>
    <%--@elvariable id="hiddenBuildTypes" type="java.util.List"--%>
    <%--@elvariable id="runningBuilds" type="java.util.Map"--%>
    <%--@elvariable id="problemsSummary" type="java.util.Map"--%>

    <c:if test="${not empty branchBean and not branchBean.wildcardBranch}">
      <c:set var="userBranch" value="${branchBean.userBranch}" scope="request"/>
    </c:if>

    <c:set var="id" value="ovr_${projectId}"
    /><c:set var="blockId" value="t_${id}"/><c:set var="project_collapsed" value='${util:isBlockHidden(pageContext.request, blockId, not firstProjectInList)}'/>
    <div class="projectHeader ${project_collapsed ? "" : "exp"}">
     <table class="projectHeaderTable">
       <tr>
         <td class="projectName">
           <span class="branchContainer" id="branch_container_${id}"></span>
           <c:if test="${overviewPage and (not empty branchBean.activeBranches or not empty branchBean.otherBranches)}">
             <script type="text/javascript">
               BS.Branch.installDropDownToProjectPane("#branch_container_${id}",
                                                      "<bs:escapeForJs text="${branchBean.userBranch}"/>",
                                                       <bs:_branchesListJs branches="${branchBean.activeBranches}"/>,
                                                       <bs:_branchesListJs branches="${branchBean.otherBranches}"/>,
                                                      {projectId: "${project.projectId}", isFirst: ${firstProjectInList}});
             </script>
           </c:if>

           <bs:handle handleId="${id}"
                      expandedIcon="${project.status.failed ? 'eluruDesign/projectFailingSmallExpanded.png' :
                                      project.status.successful ? 'eluruDesign/projectSuccessfulSmallExpanded.png' :
                                                              'eluruDesign/projectNeutralSmallExpanded.png'}"
                      imgTitle="${project.status.failed ? 'Project has failing build configurations. Click to expand / collapse details' :
                                  project.status.successful ? 'Project is successful. Click to expand / collapse details' :
                                                              'Project does not have builds. Click to expand / collapse details'}"/>
           <a href="<bs:projectUrl projectId='${projectId}'/>" title="Go to the project page"><c:out value="${project.name}"/></a>
           <c:if test="${not empty project.description}"><span class="projectDescription"><bs:out value="${project.description}"/></span></c:if>
         </td>
         <td class="projectStatus err">
           <c:set var="errorsNum" value="${projectStatusDetails.errorsNumber}"/>
           <c:if test="${errorsNum > 0}">
             <c:set var="tooltip">${errorsNum} build configuration<bs:s val="${errorsNum}"/> with configuration problems or failed to start builds</c:set>
             <span <bs:tooltipAttrs text="${tooltip}"/> ><img src="<c:url value="img/buildStates/redSign.gif"/>" /> ${errorsNum} error<bs:s val="${errorsNum}"/></span>
           </c:if>
         </td>
         <td class="projectStatus failing">
           <c:set var="failingNum" value="${projectStatusDetails.failingNumber}"/>
           <c:if test="${failingNum > 0}">
             <c:set var="tooltip">${failingNum} failing build configuration<bs:s val="${failingNum}"/></c:set>
             <span <bs:tooltipAttrs text="${tooltip}"/> >${failingNum} failing</span>
           </c:if>
         </td>
         <td class="projectStatus successful">
           <c:set var="successfulNum" value="${projectStatusDetails.successfulNumber}"/>
           <c:if test="${successfulNum > 0}">
             <c:set var="tooltip">${successfulNum} successful build configuration<bs:s val="${successfulNum}"/></c:set>
             <span <bs:tooltipAttrs text="${tooltip}"/> >${successfulNum} successful</span>
           </c:if>
         </td>
         <td class="hideProject">
           <authz:authorize allPermissions="CHANGE_OWN_PROFILE"
             ><jsp:attribute name="ifAccessGranted"
               ><c:set var="hiddenBuildTypesNumber" value="${not empty hiddenBuildTypes ? fn:length(hiddenBuildTypes) : 0}"
               /><bs:showBuildTypesPopup projectId="${projectId}"><c:choose
                 ><c:when test="${hiddenBuildTypesNumber > 0}">${hiddenBuildTypesNumber} hidden</c:when
                 ><c:otherwise>no hidden</c:otherwise
               ></c:choose></bs:showBuildTypesPopup
              ><a class="hideProject" href="#" onclick="return BS.Projects.hideProject('${projectId}')" title="Hide this project"/></jsp:attribute
             ><jsp:attribute name="ifAccessDenied">&nbsp;</jsp:attribute
           ></authz:authorize>
         </td>
       </tr>
     </table>
    </div>
  </bs:trimWhitespace>

  <div id="btb${id}">
    <c:set var="showAllSetting" value="shows_all_build_types_${project.projectId}"
    /><c:set var="shouldShowHideBuildTypeIcon" value="${not hasFilteredConfigurations or not ufn:booleanPropertyValue(currentUser, showAllSetting)}"
    /><l:displayCollapsibleBlock blocksType="${blockId}"
                                 id="btb${id}"
                                 collapsedByDefault="${not firstProjectInList}"
                                 alwaysShowBlock="${empty overviewPage or param['force_show_build_types']}"
      ><jsp:attribute name="content"
        ><c:if test="${not empty overviewPage and empty projectBuildTypes and not empty project.buildTypes and ufn:booleanPropertyValue(currentUser, 'overview.hideSuccessful')}"
          ><c:set var="num" value="${fn:length(project.buildTypes)}"/>
          There are <b>${num}</b> successful build configuration<bs:s val="${num}"/> which are hidden.
          <a href="#" onclick="BS.User.setBooleanProperty('overview.hideSuccessful', false, {afterComplete:function(){BS.reload(true);}}); return false">Show</a>
        </c:if

        ><c:forEach var="buildType" items="${projectBuildTypes}"
          ><c:if test="${empty userBranch}"
            ><bs:_buildTypeTable currentUser="${currentUser}"
                                 buildType="${buildType}"
                                 branch="${null}"
                                 theRunningBuilds="${runningBuilds[buildType]}"
                                 problemsCounters="${problemsSummary[buildType]}"
                                 shouldShowHideBuildTypeIcon="${shouldShowHideBuildTypeIcon}"
          /></c:if
          ><c:if test="${not empty userBranch}"
            ><jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.BuildTypeEx"
            /><jsp:useBean id="userBranch" type="java.lang.String" scope="request"
            /><c:set var="branch" value="<%=buildType.getBranch(userBranch)%>"
            /><bs:_buildTypeTable currentUser="${currentUser}"
                                  buildType="${buildType}"
                                  branch="${branch}"
                                  problemsCounters="${problemsSummary[buildType]}"
                                  shouldShowHideBuildTypeIcon="${shouldShowHideBuildTypeIcon}"
          /></c:if
        ></c:forEach
        ><c:if test="${not empty userBranch}">
          <script type="text/javascript">
            BS.Branch.injectBranchParamToLinks($j("#p_${projectId}"), "${projectId}");
          </script>
        </c:if
      ></jsp:attribute
    ></l:displayCollapsibleBlock>
  </div>

  <c:if test="${not empty overviewPage}"
    ><script type="text/javascript">
      <l:blockState blocksType="${blockId}"/>
      <c:set var="imgName" value="${project.status.failed ? 'projectFailingSmall.png' :
                                    project.status.successful ? 'projectSuccessfulSmall.png' : 'projectNeutralSmall.png'}"/>
      BS.CollapsableBlocks.registerBlock(new BS.ProjectBlock("t_", '${id}', "eluruDesign/${imgName}", ${not firstProjectInList}));
    </script>
  </c:if

  ><span style="display:none;"><!-- ================ END Project build types for: --><c:out value="${project.name}"/></span>
</div>
<script type="text/javascript">
  (function($) {
    $("#p_${projectId} .tableCaption").first().addClass("firstRow");
    <c:if test="${not empty userBranch}">
      BS.Branch.injectBranchParamToLinks($j("#p_${projectId} .projectHeader"), "${projectId}");
    </c:if>
  })(jQuery);
</script>
<bs:executeOnce id="pauseDialog"><bs:pauseBuildTypeDialog/></bs:executeOnce>