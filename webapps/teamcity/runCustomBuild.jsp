<%@ include file="/include-internal.jsp"%>
<jsp:useBean id="runBuildBean" type="jetbrains.buildServer.controllers.RunBuildBean" scope="request"/>
<c:set var="changes" value="${runBuildBean.visibleChanges}" scope="request"/>
<c:set var="branches" value="${runBuildBean.branches}" scope="request"/>
<c:set var="hasBranches" value="${not empty branches}" scope="request"/>

<div id="runCustomBuildDiv">
  <div class="simpleTabs clearfix" style="margin-bottom: 0.5em;">
    <ul class="tabs">
      <li class="first selected" id="tab-0" onclick="return BS.RunBuildDialog.showTab('general-tab');"><p><a href="#">General</a></p></li>
      <c:if test="${runBuildBean.dependenciesExist}">
        <li id="tab-1" onclick="return BS.RunBuildDialog.showTab('dependencies-tab');"><p><a href="#">Dependencies</a></p></li>
      </c:if>
      <authz:authorize projectId="${runBuildBean.buildType.projectId}" allPermissions="CUSTOMIZE_BUILD_REVISIONS">
        <c:if test="${not empty changes or hasBranches}">
          <li id="tab-2" onclick="return BS.RunBuildDialog.showTab('changes-tab');"><p><a href="#">Changes</a></p></li>
        </c:if>
      </authz:authorize>
      <authz:authorize projectId="${runBuildBean.buildType.projectId}" allPermissions="CUSTOMIZE_BUILD_PARAMETERS">
        <li id="tab-3" onclick="return BS.RunBuildDialog.showTab('properties-tab');"><p><a href="#">Build Parameters</a> <span id="customRunPropertiesRequired" style="display: none;"><l:star/></span></p></li>
      </authz:authorize>
      <li class="last" id="tab-4" onclick="return BS.RunBuildDialog.showTab('comment-tab');"><p><a href="#">Comment</a></p></li>
    </ul>
  </div>

  <div id="runCustomBuildContentDiv" class="custom-scroll">
    <jsp:include page="runCustomBuildGeneral.jsp"/>
    <bs:changeRequest key="dependencies" value="${runBuildBean.dependencies}">
      <jsp:include page="runCustomBuildDependencies.jsp"/>
    </bs:changeRequest>
    <jsp:include page="runCustomBuildRevisions.jsp"/>
    <authz:authorize projectId="${runBuildBean.buildType.projectId}" allPermissions="CUSTOMIZE_BUILD_PARAMETERS">
      <bs:changeRequest key="params" value="${runBuildBean.buildParameters}">
        <jsp:include page="runCustomBuildParameters.jsp"/>
      </bs:changeRequest>
    </authz:authorize>
    <jsp:include page="runCustomBuildComment.jsp"/>
  </div>
</div>

<span id="errorNoAgents" class="error" style="margin-left: 0;"></span>

<div class="popupSaveButtonsBlock">
  <forms:cancel onclick="BS.RunBuildDialog.close()"/>
  <forms:submit label="Run Build" id="runCustomBuildButton"/>
  <forms:saving id="runBuildProgress"/>
</div>

<script type="text/javascript">
  BS.RunBuildDialog.highlightTabOnShow();
  BS.RunBuild.setTitleSuffix(' <c:out value="${runBuildBean.dialogTitleSuffix}"/>');
</script>
