<%@ include file="include-internal.jsp" %>
<jsp:useBean id="createAdminForm" type="jetbrains.buildServer.controllers.user.NewUserForm" scope="request"/>
<c:set var="title" value="Create Administrator Account"/>
<bs:externalPage>
  <jsp:attribute name="page_title">${title}</jsp:attribute>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/forms.css
      /css/initialPages.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/crypt/rsa.js
      /js/crypt/jsbn.js
      /js/crypt/prng4.js
      /js/crypt/rng.js
      /js/bs/forms.js
      /js/bs/encrypt.js
      /js/bs/createUser.js
    </bs:linkScript>
    <script type="text/javascript">
      $j(document).ready(function($) {
        $("#username1").focus();
      });
    </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <div id="createAdminPage" class="initialPage">
      <span class="logo"><img src="img/logoLogin.gif" alt="TeamCity" width="61" height="102"/></span>

      <div id="pageContent">
        <h1 id="header">${title}</h1>
        <bs:version/>
        <form action="<c:url value='/createAdminSubmit.html'/>" onsubmit="return BS.CreateUserForm.submitCreateUser();" method="post">

          <div id="errorMessage"></div>

          <table>
            <tr class="formField">
              <th><label for="username1">* Username:</label></th>
              <td><input class="text" id="username1" type="text" name="username1" size="25" maxlength="256"
                 value="<c:out value='${createAdminForm.username1}'/>">
                <span class="error" id="errorUsername1" style="margin-left: 0;"></span>
                 </td>
            </tr>
            <tr class="formField">
              <th><label for="name">Name:</label></th>
              <td><input class="text" id="name" type="text" name="name" size="25" maxlength="128"
                 value="<c:out value='${createAdminForm.name}'/>"></td>
            </tr>
            <tr class="formField">
              <th><label for="email">Email address:</label></th>
              <td><input class="text" id="email" type="text" name="email" size="25" maxlength="128"
                 value="<c:out value='${createAdminForm.email}'/>"></td>
            </tr>
            <tr class="formField">
              <th><label for="password1">* Password:</label></th>
              <td><input class="text" id="password1" type="password" name="password1" size="25" maxlength="80">
                <span class="error" id="errorPassword1" style="margin-left: 0;"></span>
              </td>
            </tr>
            <tr class="formField">
              <th><label for="retypedPassword">* Once again:</label></th>
              <td><input class="text" id="retypedPassword" type="password" name="retypedPassword" size="25" maxlength="80"></td>
            </tr>
            <tr>
              <th><forms:saving style="padding-top: 0.6em"/></th>
              <td><input class="btn" type="submit" value="Create Account"/></td>
            </tr>
          </table>

          <input type="hidden" id="submitCreateUser" name="submitCreateUser"/>
          <input type="hidden" id="publicKey" name="publicKey" value="${createAdminForm.hexEncodedPublicKey}"/>
        </form>
      </div>
    </div>
  </jsp:attribute>
</bs:externalPage>

