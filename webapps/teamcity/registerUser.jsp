<%--@elvariable id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary"--%>
<%@ include file="include-internal.jsp" %>
<c:set var="title" value="Register a New User Account"/>
<jsp:useBean id="registerUserForm" type="jetbrains.buildServer.controllers.user.NewUserForm" scope="request"/>
<bs:externalPage>
  <jsp:attribute name="page_title">${title}</jsp:attribute>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/forms.css
      /css/initialPages.css
    </bs:linkCSS>
    <script>
      document.documentElement.className = 'ua-js';
    </script>
    <bs:linkScript>
      /js/crypt/rsa.js
      /js/crypt/jsbn.js
      /js/crypt/prng4.js
      /js/crypt/rng.js
      /js/bs/forms.js
      /js/bs/encrypt.js
      /js/bs/createUser.js
    </bs:linkScript>
    <script type="text/javascript">
      $j(document).ready(function($) {
        var loginForm = $('.loginForm');

        $("#username").focus();

        loginForm.attr('action', '<c:url value='/registerUserSubmit.html'/>');
        loginForm.submit(function() {
          return BS.CreateUserForm.submitCreateUser();
        });
      });
    </script>
  </jsp:attribute>
  <jsp:attribute name="body_include">
    <div id="registerUserPage" class="initialPage">
      <span class="logo"><img src="img/logoLogin.gif" alt="TeamCity" width="61" height="102"/></span>

      <div id="pageContent">
        <h1 id="header">${title}</h1>
        <bs:version/>

        <form class="loginForm" method="post">

          <div id="errorMessage"></div>
          <table>
          <tr class="formField">
            <th><label for="username1">* Username:</label></th>
            <td><input class="text" id="username1" type="text" name="username1" style="width:15em;"/>
            <span class="error" id="errorUsername1" style="margin-left: 0;"></span>
            </td>
          </tr>
          <tr class="formField">
            <th><label for="name">Name:</label></th>
            <td><input class="text" id="name" type="text" name="name" style="width:15em;" maxlength="128"
                 value="<c:out value='${registerUserForm.name}'/>"></td>
          </tr>
          <tr class="formField">
            <th><label for="email">Email address:</label></th>
            <td><input class="text" id="email" type="text" name="email" style="width:15em;" maxlength="128"
                 value="<c:out value='${registerUserForm.email}'/>"></td>
          </tr>
          <tr class="formField">
            <th><label for="password1">* Password:</label></th>
            <td><input class="text" id="password1" type="password" name="password1" style="width:15em;" maxlength="80">
              <span class="error" id="errorPassword1" style="margin-left: 0;"></span>
            </td>
          </tr>
          <tr class="formField">
            <th><label for="retypedPassword">* Confirm:</label></th>
            <td><input class="text" id="retypedPassword" type="password" name="retypedPassword" style="width:15em;" maxlength="80"></td>
          </tr>
          <tr>
            <th><forms:saving style="padding-top: 0.6em"/></th>
            <td>
              <noscript>
                <div class="noJavaScriptEnabledMessage">
                  Please enable JavaScript in your browser to proceed with registration.
                </div>
              </noscript>

              <input class="btn loginButton" type="submit" value="Register and Login"/>
              <a class="loginButton" style="margin-left: 1em;" href="<c:url value='/login.html'/>">Cancel</a>
            </td>
          </tr>
          </table>

          <input type="hidden" id="submitCreateUser" name="submitCreateUser"/>
          <input type="hidden" id="publicKey" name="publicKey" value="${registerUserForm.hexEncodedPublicKey}"/>

          <br/>
          <span class="versionTag">Fields marked with * are required</span>
        </form>

      </div>
    </div>
  </jsp:attribute>
</bs:externalPage>
