<%--@elvariable id="serverTC" type="jetbrains.buildServer.serverSide.BuildServerEx"--%>
<%--@elvariable id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary"--%>
<%@ page import="jetbrains.buildServer.serverSide.VersionChecker"
  %><%@ page import="jetbrains.buildServer.web.openapi.PlaceId"
  %><%@ include file="include-internal.jsp"
  %><c:set var="mode" value="${serverSummary.enterpriseMode ? 'ent' : 'prof'}"
  /><c:url value="http://www.jetbrains.com/teamcity/feedback?source=footer&version=${serverTC.fullServerVersion}&build=${serverTC.buildNumber}&mode=${mode}" var="feedbackUrl"/>
<script type="text/javascript">
  var addr = "teamcity" + "-" + "feedback" + "@" + "jetbrains.com";
  var version = "${serverTC.fullServerVersion}";
</script>

<div id="footer">
  <div class="footerExtensions fixedWidth"><ext:includeExtensions placeId="<%=PlaceId.ALL_PAGES_FOOTER%>"/></div>

  <div class="footerMainContainer">
    <div class="footerMain fixedWidth clearfix">
      <div class="column1">
        <div class="columnContent">
          <a class="helpLink" target="_blank" href="<bs:helpUrlPrefix/>"> Help</a>
          <a class="feedbackLink" target="_blank" href="${feedbackUrl}">Feedback</a>
        </div>
      </div>

      <div class="column2">
        <div class="columnContent">
          <a target="_blank" href="http://www.jetbrains.com/teamcity/?fromServer">TeamCity <c:choose
          ><c:when test="${serverSummary.enterpriseMode}">Enterprise</c:when><c:otherwise>Professional</c:otherwise></c:choose></a> <bs:version
          /><c:if test="<%=VersionChecker.isNewVersionAvailable()%>">
            <a class="newVersionLink" target="_blank" title="Download TeamCity <%=VersionChecker.getNewVersion()%>"
               href="http://www.jetbrains.com/teamcity/download/index.html?fromServer">Get new version!</a></c:if>
        </div>
      </div>

      <div class="column3">
        <div class="columnContent">
          <a onclick="BS.Util.popupWindow('<c:url value='/showAgreement.html'/>', 'agreement'); return false" href="#">License agreement</a>
          <c:if test="${serverTC.daysToLicenseExpiration <= 10 && serverTC.daysToLicenseExpiration > 0}">
          <div class="expirationWarning">
              Temporary license will expire in ${serverTC.daysToLicenseExpiration} day(s)
          </div>
          </c:if>
        </div>
      </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  (function($) {
    var $window = $(window),
        mainContent = $('#mainContent'),
        footer = $('#footer');

    mainContent.css('padding-bottom', footer.height() + 'px');
    footer.css('height', footer.height() + 'px');
    footer.css('visibility', 'visible');
  })(jQuery);
</script>
