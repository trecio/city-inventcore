<%@taglib prefix="stats" tagdir="/WEB-INF/tags/graph"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="graph" type="java.util.List<jetbrains.buildServer.web.statistics.graph.DeclarativeCompositeValueType>"--%>
<c:forEach items="${customGraphs}" var="graph">
<stats:buildGraph id="${graph.key}" valueType="${graph.key}"
                  hideFilters="${graph.hideFilters}" defaultFilter="${graph.defaultFilter}"/>
   <%--height="${graph.height}"--%>
</c:forEach>

