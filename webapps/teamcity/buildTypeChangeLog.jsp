<%@ include file="include-internal.jsp" %>
<jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.SBuildType" scope="request"/>
<jsp:useBean id="changeLogBean" type="jetbrains.buildServer.controllers.buildType.tabs.ChangeLogBean" scope="request"/>

<bs:changesList
     changeLog="${changeLogBean}"
     url="viewType.html?buildTypeId=${buildType.buildTypeId}&tab=buildTypeChangeLog"
     filterUpdateUrl="buildTypeChangeLogTab.html?buildTypeId=${buildType.buildTypeId}"
     projectId="${buildType.projectId}"
/>