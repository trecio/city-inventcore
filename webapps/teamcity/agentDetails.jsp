<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %>
<jsp:useBean id="agentDetailInfo" scope="request" type="jetbrains.buildServer.controllers.agent.AgentDetailInfo"/>
<jsp:useBean id="serverSummary" scope="request" type="jetbrains.buildServer.web.openapi.ServerSummary"/>

<%@ include file="include-internal.jsp" %>
<l:defineCurrentTab defaultTab=""/>

<ext:forEachTab placeId="<%=PlaceId.AGENT_DETAILS_TAB%>">
  <c:if test="${empty currentTab and extension.tabId == 'agentSummary'}">
    <c:set var="currentTab" value="agentSummary"/>
  </c:if>
  <c:if test="${empty currentTab and extension.tabId == 'agentTypeSummary'}">
    <c:set var="currentTab" value="agentTypeSummary"/>
  </c:if>
  <c:if test="${currentTab == extension.tabId}">
    <c:set var='currentTabTitle' value="${extension.tabTitle}"/>
  </c:if>
</ext:forEachTab>
<c:set var="pageTitle" value="${currentTabTitle}" scope="request"/>
<bs:page>
  <jsp:attribute name="head_include">
    <bs:linkCSS>
      /css/progress.css
      /css/viewType.css
      /css/compatibilityList.css
      /css/overviewTable.css
      /css/agents.css
    </bs:linkCSS>
    <bs:linkScript>
      /js/bs/runningBuilds.js
      /js/bs/testGroup.js

      /js/bs/blocks.js
      /js/bs/blockWithHandle.js

      /js/bs/agents.js
    </bs:linkScript>
    <c:if test="${serverSummary.hasSeveralAgentPools}"><c:set var="poolName">${agentDetailInfo.poolName} pool</c:set></c:if>
    <c:set var="agentName">${agentDetailInfo.caption}</c:set>
    <script type="text/javascript">
      BS.Navigation.items = [
      <c:choose>
        <c:when test="${agentDetailInfo.agentType}">
          <jsp:useBean id="agentDetails" type="jetbrains.buildServer.controllers.agent.AgentTypeDetailsForm" scope="request"/>
          <c:choose>
            <c:when test="${agentDetails.agent.details.cloudAgent}">
        {title: "Cloud Agents", url:"<c:url value='/agents.html?tab=clouds'/>"},
            </c:when>
            <c:otherwise>
        {title: "Agents", url:"<c:url value='/agents.html'/>"},
            </c:otherwise>
          </c:choose>
        </c:when>
        <c:when test="${not agentDetailInfo.authorized}">
        {title: "Unauthorized agents", url:"<c:url value='/agents.html?unauthorized=true'/>"},
        </c:when>
        <c:when test="${not agentDetailInfo.registered}">
        {title: "Disconnected agents", url:"<c:url value='/agents.html?unregistered=true'/>"},
        </c:when>
        <c:otherwise>
        {title: "Connected agents", url:"<c:url value='/agents.html'/>"},
        </c:otherwise>
      </c:choose>
        <c:if test="${serverSummary.hasSeveralAgentPools}">
        {title: "<bs:escapeForJs text="${poolName}" forHTMLAttribute="true"/>", url:"<c:url value='/agents.html?tab=agentPools#${agentDetails.agentType.agentPool.agentPoolId}'/>"},
        </c:if>
        {title: "<bs:escapeForJs text="${agentName}" forHTMLAttribute="true"/>", url:"<c:url value='/agentDetails.html?${agentDetailInfo.urlParameters}'/>", selected:true}
      ];

      BS.topNavPane.setActiveCaption('agents');

      $j(document).ready(function() {
        BS.RunningBuilds.startUpdates();
      });
    </script>

  </jsp:attribute>
<jsp:attribute name="body_include">
  <et:subscribeOnEvents>
    <jsp:attribute name="eventNames">
      AGENT_REGISTERED
      AGENT_PARAMETERS_UPDATED
      AGENT_UNREGISTERED
      AGENT_REMOVED
      AGENT_STATUS_CHANGED
    </jsp:attribute>
    <jsp:attribute name="eventHandler">
      if ($('agentTabContent')) {
        BS.reload(false, function() {
          $('agentTabContent').refresh();
        });
      }
    </jsp:attribute>
  </et:subscribeOnEvents>

  <div class="agentDetails" id="agentDetails">
    <script type="text/javascript">
      (function() {
        var paneNav = new TabbedPane();

        function initialize3dLevel(paneNav, myTabs) {
          paneNav.clearTabs();
          var baseUrl = "<c:url value='/agentDetails.html?${agentDetailInfo.urlParameters}&tab='/>";
          for(var tabCode in myTabs) {
            paneNav.addTab(tabCode, {
              caption: myTabs[tabCode].title,
              url: baseUrl + tabCode
            });
          }

          paneNav.setActiveCaption('${currentTab}');
        }

        var agentDetailsTabs = {};

        // Process plugins/tabs:
        <c:set var="selectedTabExtension" value="${null}"/>

        <c:set var="hasTabs" value="${false}"/>
        <ext:forEachTab placeId="<%=PlaceId.AGENT_DETAILS_TAB%>">
          <c:set var="hasTabs" value="${true}"/>
          // plugin ${extension.tabId}:
          agentDetailsTabs['${extension.tabId}'] = { title: '<bs:escapeForJs text="${extension.tabTitle}"/>' };
          <c:if test="${currentTab == extension.tabId}">
            <c:set var='selectedTabExtension' value="${extension}"/>
          </c:if>
        </ext:forEachTab>

        initialize3dLevel(paneNav, agentDetailsTabs);
        if (paneNav.getNumberOfTabs() > 1) {
          paneNav.showIn('tabsContainer3');
        }
      })();
    </script>
    <div class="agentsContent">
    <bs:refreshable containerId="agentTabContent" pageUrl="${pageUrl}">
      <c:choose>
        <c:when test="${hasTabs and not empty selectedTabExtension}">
          <ext:includeExtension extension="${selectedTabExtension}"/>
        </c:when>
        <c:otherwise>No tab selected</c:otherwise>
      </c:choose>
    </bs:refreshable>
    </div>
  </div>
</jsp:attribute>
</bs:page>
