<%@ include file="include-internal.jsp" %><%@
    taglib prefix="resp" tagdir="/WEB-INF/tags/responsible"%><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin"%><%@
    taglib prefix="user" tagdir="/WEB-INF/tags/userProfile"%><%@
    taglib prefix="t" tagdir="/WEB-INF/tags/tags"
%><jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.SBuildType" scope="request"
/><jsp:useBean id="currentUser" type="jetbrains.buildServer.users.SUser" scope="request"
/><jsp:useBean id="buildTypeBranches" type="java.util.List" scope="request"

/><div class="buildTypeBranchesHeader">
  <c:set var="activeNum" value="${fn:length(buildTypeBranches)}"/>
  <c:set var="otherNum" value="${fn:length(branchBean.otherBranches)}"/>
  There ${activeNum > 1 ? 'are' : 'is'} <b>${activeNum}</b> active branch${activeNum > 1 ? 'es' : ''} in this configuration.
  <c:if test="${otherNum > 0}"><b>${otherNum}</b> inactive branch${otherNum > 1 ? 'es' : ''} ${otherNum > 1 ? 'are' : 'is'} not displayed.</c:if>
</div>
<div class="clearfix buildTypeBranches">
  <bs:_branchesTable currentUser="${currentUser}"
                     buildType="${buildType}"
                     buildTypeBranches="${buildTypeBranches}"/>
</div>
