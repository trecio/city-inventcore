<%@include file="/include-internal.jsp" %>
<%@ taglib prefix="clouds" tagdir="/WEB-INF/tags/clouds" %>
<jsp:useBean id="form" type="jetbrains.buildServer.clouds.server.web.beans.CloudTabForm" scope="request"/>
<jsp:useBean id="pageUrl" scope="request" type="java.lang.String"/>
<jsp:useBean id="profile" scope="request" type="jetbrains.buildServer.clouds.server.web.beans.CloudTabFormProfileInfo"/>
<jsp:useBean id="image" scope="request" type="jetbrains.buildServer.clouds.server.web.beans.CloudTabFormImageInfo"/>
<jsp:useBean id="instance" scope="request" type="jetbrains.buildServer.clouds.server.web.beans.CloudTabFormInstanceInfo"/>

<tr class="instance">
  <td class="noRightBorder instanceName <c:if test="${not instance.canTerminate}">instanceNameGrayed</c:if>">
    <span style="float:right;">Launched <bs:date value="${instance.instance.startedTime}"/></span>
    <c:if test="${not instance.hasError}">
      <clouds:imageState status="${instance.instance.status}"/>
    </c:if>
    <c:out value="${instance.name}"/>
    <clouds:cloudProblemsLink controlId="error_${instance.id}" problems="${instance.problems}">
      Instance Error
    </clouds:cloudProblemsLink>
    <clouds:cloudProblemContent controlId="error_${instance.id}" problems="${instance.problems}"/>
  </td>
  <td class="buttons">
    <c:set var="showInstanceStatus" value="${true}"/>
    <authz:authorize allPermissions="START_STOP_CLOUD_AGENT">
      <c:if test="${instance.canTerminate}">
        <c:set var="showInstanceStatus" value="${false}"/>
        <div id="stopInstanceDiv_${instance.uniqueId}">
          <a href="#"
             onclick="return BS.Clouds.stopInstance('<bs:forJs>${instance.uniqueId}</bs:forJs>','<bs:forJs>${profile.id}</bs:forJs>', '<bs:forJs>${image.id}</bs:forJs>', '<bs:forJs>${instance.id}</bs:forJs>'); return false">Stop</a>
        </div>
        <div id="stoppingInstaceDiv_${instance.uniqueId}" style="display:none;">
          <forms:saving className="progressRingInline"/> Stopping...
        </div>
      </c:if>
    </authz:authorize>
    <c:if test="${showInstanceStatus}">
      <c:out value="${instance.statusText}"/>
    </c:if>
  </td>
</tr>