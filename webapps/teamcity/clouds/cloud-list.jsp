<%@include file="/include-internal.jsp"%>
<%@ taglib prefix="clouds" tagdir="/WEB-INF/tags/clouds"  %>
<jsp:useBean id="form" type="jetbrains.buildServer.clouds.server.web.beans.CloudTabForm" scope="request"/>
<jsp:useBean id="pageUrl" scope="request" type="java.lang.String"/>

<script type="text/javascript">
  BS.Clouds.Problems = {};
</script>

  <p>You have <strong>${fn:length(form.profiles)}</strong> profile<bs:s val="${fn:length(form.profiles)}"/> configured.</p>
  <c:forEach items="${form.profiles}" var="profile">
    <l:blockStateCss blocksType="Block_profile${profile.id}" collapsedByDefault="false" id="cp${profile.id}"/>

    <clouds:cloudProblemContent controlId="error_${profile.id}" problems="${profile.problems}" />

    <p class="blockHeader profileHeader expanded" id="profile${profile.id}">
      <clouds:profile profile="${profile}"/>
    </p>

    <div class="cloudProfile" id="cloudProfile:${profile.id}">
      <c:choose>
        <c:when test="${profile.loading}">
          <forms:saving className="progressRingInline"/> Loading...
        </c:when>
        <c:otherwise>
          <div class="cloudProfileContent">
            <c:choose>
              <c:when test="${profile.hasErrors}">
                <p style="padding-left:1em;">No images were found due to a error. <clouds:editProfilesNote/></p>
              </c:when>
              <c:when test="${empty profile.images}">
                <p style="padding-left:1em;">No images were found. <clouds:editProfilesNote/></p>
              </c:when>
              <c:otherwise>
                <c:forEach items="${profile.images}" var="image">
                  <bs:changeRequest key="profile" value="${profile}">
                  <bs:changeRequest key="image" value="${image}">
                    <jsp:include page="cloud-list-image.jsp"/>
                  </bs:changeRequest>
                  </bs:changeRequest>
                </c:forEach>
              </c:otherwise>
            </c:choose>
          </div>
        </c:otherwise>
      </c:choose>
    </div>
    <script type="text/javascript">
      <l:blockState blocksType="Block_profile${profile.id}"/>
      new BS.BlocksWithHeader('profile${profile.id}');
    </script>
  </c:forEach>
  <c:if test="${empty form.profiles}">
    <div>
      <clouds:editProfilesNote/>
    </div>
  </c:if>

