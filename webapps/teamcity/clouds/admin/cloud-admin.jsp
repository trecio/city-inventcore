<%@include file="/include-internal.jsp" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="clouds" tagdir="/WEB-INF/tags/clouds" %>

<jsp:useBean id="form" type="jetbrains.buildServer.clouds.server.web.beans.CloudTabForm" scope="request"/>
<jsp:useBean id="pageUrl" scope="request" type="java.lang.String"/>

<c:set var="profileAjaxUrl"><c:url value="/clouds/admin/cloudAdminProfile.html"/></c:set>
<c:set var="ajaxUrl"><c:url value="/clouds/admin/cloudAdmin.html"/></c:set>
<c:set var="numProfiles" value="${fn:length(form.profiles)}"/>
<script type="text/javascript">
  BS.Clouds.Admin.registerRefresh();
</script>

<c:if test="${not form.disabled}">
  <div class="right_info_pane">
    Images and running instances of configured cloud profiles are shown on the <a href="<c:url value="/agents.html?tab=clouds"/>">Clouds tab</a>.
  </div>
</c:if>

<div class="right_info_pane">
  <c:choose>
    <c:when test="${form.disabled}">
      <forms:saving id="enable_integration_loader" className="progressRingInline"/>
      <input class="btn btn_mini" type="submit" value="Enable cloud integration" onclick="BS.Clouds.Admin.enabledClouds(this, '<bs:forJs>${ajaxUrl}</bs:forJs>')">
    </c:when>
    <c:otherwise>
      <input class="btn btn_mini" type="submit" value="Disable cloud integration" onclick="BS.Clouds.Admin.ConfirmShutdownDialog.showConfirmDisableDialog()">
    </c:otherwise>
  </c:choose>
</div>

<div class="cloudProfile cloudAdminProfile">
<bs:refreshable containerId="cloudRefreshable" pageUrl="${ajaxUrl}">
  <clouds:disabledWarning disabled="${form.disabled}"/>
  <script type="text/javascript">
    BS.Clouds.Admin.setRunningInstancesCount(${form.runningInstancesCount});
    BS.Clouds.Problems = {};
  </script>
  
<c:choose>
  <c:when test="${numProfiles == 0}">
    <div>There are no profiles configured.</div>
  </c:when>
  <c:otherwise>
    <div>You have ${numProfiles} profile<bs:s val="${numProfiles}"/> configured.</div>

    <c:if test="${numProfiles > 0}">
      <l:tableWithHighlighting className="settings cloudSettings" highlightImmediately="true">
        <tr class="header">
          <th colspan="3">Available Agent Cloud Profiles</th>
        </tr>
        <c:forEach var="profile" items="${form.profiles}">
          <clouds:cloudProblemContent controlId="error_${profile.id}" problems="${profile.problems}" />
          <c:set var="editUrl"><c:url value="/admin/admin.html?item=clouds&action=edit&profileId=${profile.profile.profileId}&showEditor=true"/></c:set>
          <c:set var="onclick">document.location.href='<bs:forJs>${editUrl}</bs:forJs>'; return false;</c:set>
          <tr>
            <td class="highlight" onclick="${onclick}"><clouds:profile profile="${profile}" showRunning="${not form.disabled}"/></td>
            <td class="highlight edit" onclick="${onclick}"><a href="#" onclick="${onclick}; Event.stop(event)">edit</a></td>
            <td class="edit"><a href="#" onclick="return BS.Clouds.Admin.ConfirmDeleteProfileDialog.showDeleteProfileDialog('<bs:forJs>${profile.profile.profileId}</bs:forJs>', '<bs:forJs>${profile.profile.profileName}</bs:forJs>', ${profile.runningInstancesCount}); return false">delete</a></td>
          </tr>
        </c:forEach>
      </l:tableWithHighlighting>
    </c:if>
  </c:otherwise>
</c:choose>
</bs:refreshable>
</div>

<c:set var="addUrl"><c:url value="/admin/admin.html?item=clouds&action=new&showEditor=true"/></c:set>
<p><forms:addButton href="${addUrl}">Create new profile</forms:addButton></p>

<bs:modalDialog formId="newProfileForm"
                title="Create Cloud Profile"
                action="${profileAjaxUrl}"
                closeCommand="BS.Clouds.Admin.CreateProfileDialog.close()"
                saveCommand="BS.Clouds.Admin.CreateProfileDialog.submit()">
  <bs:refreshable containerId="newProfileFormMainRefresh" pageUrl="${profileAjaxUrl}"/>
</bs:modalDialog>


<bs:modalDialog formId="confirmShutdown"
                title="Disable cloud integration"
                action="${ajaxUrl}"
                closeCommand="BS.Clouds.Admin.ConfirmDialog.close();"
                saveCommand="BS.Clouds.Admin.ConfirmDialog.submit();">
  Are you sure you want to <span id="confirmShutdown_action"></span>?
  <div id="terminateInstancesCheckbox" style="margin-left:1em;">
    <forms:checkbox name="terminateInstances" checked="${false}"/>
    <label for="terminateInstances" id="confirmShutdown_instances_message">
      Terminate <span id="confirmShutdown_instanceCount"></span> running instance<bs:s val="${form.runningInstancesCount}"/>
    </label>
  </div>
  <div class="popupSaveButtonsBlock">
    <forms:cancel onclick="BS.Clouds.Admin.ConfirmShutdownDialog.close()"/>
    <forms:submit label="Agree" id="confirmShutdown_submit"/>
    <forms:saving id="confirmShutdownDialog_loader"/>
  </div>
</bs:modalDialog>
