<%@include file="/include-internal.jsp" %>
<%@ taglib prefix="props" tagdir="/WEB-INF/tags/props" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags/layout" %>
<%@ taglib prefix="forms" tagdir="/WEB-INF/tags/forms" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="publicKey" scope="request" type="java.lang.String"/>
<jsp:useBean id="action" scope="request" type="java.lang.String"/>
<jsp:useBean id="form" scope="request" type="jetbrains.buildServer.clouds.server.web.admin.CloudAdminProfileForm"/>
<jsp:useBean id="extensions" scope="request" type="java.util.Collection<jetbrains.buildServer.clouds.CloudTypeExtension>"/>
<jsp:useBean id="postUrl" scope="request" type="java.lang.String"/>
<jsp:useBean id="backUrl" scope="request" type="java.lang.String"/>
<jsp:useBean id="serverUrl" scope="request" type="java.lang.String"/>

<script type="text/javascript">
   BS.Clouds.Admin.CreateProfileForm.baseParams = function() {
      return "&action=${action}<c:if test="${not empty form.selectedProfile}">&profileId=${form.selectedProfile.profileId}</c:if>";
   };
</script>

<c:set var="ajaxUrl"><c:url value="${postUrl}"/></c:set>
<c:set var="cancelUrl"><c:url value="${backUrl}"/></c:set>

<div id="newProfileFormDialog" class="cloudProfile">

  <c:if test="${empty form.selectedProfile}">
  <h2 class="noBorder">Create new cloud profile</h2>
  </c:if>
  <c:if test="${not empty form.selectedProfile}">
  <h2 class="noBorder">Edit cloud profile <c:out value="${form.selectedProfile.profileName}"/></h2>
  </c:if>

  <form id="newProfileForm" action="${ajaxUrl}" method="post" onsubmit="return BS.Clouds.Admin.CreateProfileForm.submit();" autocomplete="off">


<table class="runnerFormTable">
  <tr>
    <th><label for="profileName">Profile Name:<l:star/></label></th>
    <td>
      <props:textProperty name="profileName" className="longField"/>
      <span id="error_profileName" class="error"></span>
    </td>
  </tr>
  <tr>
    <th><label for="profileDescription">Description:</label></th>
    <td>
      <props:textProperty name="profileDescription" className="longField"/>
      <span id="error_profileDescription"></span>
    </td>
  </tr>
  <tr>
    <th><label for="terminateTimeOut">Terminate Instance Idle Time:</label></th>
    <td>
      <props:textProperty name="terminateTimeOut" className="longField"/>
      <span id="error_terminateTimeOut" class="error"></span>
      <span class="smallNote">Minutes to wait before stopping idle build agent</span>
    </td>
  </tr>
  <tr>
    <th><label for="terminateAfterFirstBuild">Terminate Instance:</label></th>
    <td>
      <props:checkboxProperty name="terminateAfterFirstBuild"/>
      <label for="terminateAfterFirstBuild">After first build finished</label>
    </td>
  </tr>
  <tr>
    <th><label for="cloudType">Cloud Type:<l:star/></label></th>
    <td>
      <select name="cloudType" id="cloudType" onchange="BS.Clouds.Admin.CreateProfileForm.refreshSelectedCloudType();" class="longField">
        <forms:option value="" selected="${empty form.selectedTypeCode}">--- Choose cloud type ---</forms:option>
        <c:forEach items="${form.cloudTypes}" var="type">
          <forms:option value="${type.cloudCode}" selected="${not empty form.selectedTypeCode and type.cloudCode eq form.selectedTypeCode}"><c:out value="${type.displayName}"/></forms:option>
        </c:forEach>
      </select>
      <forms:saving id="newProviderSaving" className="progressRingInline"/>
      <span class="error" id="error_cloudType"></span>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <div class="attentionComment">
        Server URL<bs:help file="Configuring+Server+URL"/> is <strong>${serverUrl}</strong>.
        It will be used by build agent to connect. Make sure this URL is available from build agent machine.
        To change it use <a href="<c:url value='/admin/admin.html?item=serverConfigGeneral'/>" target="_blank">Server Configuration page</a>.
      </div>
    </td>
  </tr>
</table>


<bs:refreshable containerId="newProfilesContainer" pageUrl="${ajaxUrl}">
  <c:if test="${not empty form.selectedType}">
    <table class="runnerFormTable">
    <c:choose>
      <c:when test="${not empty form.selectedType.editProfileUrl}">
          <jsp:include page="${form.selectedType.editProfileUrl}"/>
      </c:when>
      <c:otherwise>
        <tr>
          <td colspan="2">
            <div class="smallNote" style="margin-left: 0;">There are no additional options.</div>
          </td>
        </tr>
      </c:otherwise>
    </c:choose>
    <c:forEach var="ext" items="${extensions}">
      <jsp:useBean id="ext" type="jetbrains.buildServer.clouds.CloudTypeExtension"/>
      <!-- Cloud profile form extension: ${ext} -->
      <jsp:include page="${ext.includeUrl}"/>
      <!-- End of: Cloud profile form extension: ${ext} -->
    </c:forEach>
    </table>
  </c:if>

  <input type="hidden" id="publicKey" name="publicKey" value="${publicKey}"/>
  <input type="hidden" id="action" name="action" value="${action}"/>
  <c:if test="${not empty form.selectedProfile}">
    <input type="hidden" id="profileId" name="profileId" value="${form.selectedProfile.profileId}"/>
  </c:if>
</bs:refreshable>

<div class="popupSaveButtonsBlock">
  <forms:cancel href="${cancelUrl}"/>
  <forms:submit label="${form.submitButtonCaption}" id="createButton"/>
  <forms:saving id="newProfileProviderProgress"/>
</div>

 </form>
</div>

<script type="text/javascript">
  BS.Clouds.Admin.CreateProfileForm.beforeShow();
</script>