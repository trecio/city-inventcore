if (!BS.Clouds) {
  BS.Clouds = {};
}

BS.Clouds.Admin = {
  _runningInstancesCount : 0,

  setRunningInstancesCount: function(c) {
    this._runningInstancesCount = c;
  },

  getRunningInstancesCount: function() {
    return this._runningInstancesCount;
  },

  refresh: function() {
    $('cloudRefreshable').refresh();
  },

  registerRefresh: function() {
    document.observe("dom:loaded", function() {
      BS.Clouds.registerRefreshable(BS.Clouds.Admin.refresh.bind(BS.Clouds.Admin));
    });
  },

  CreateProfileForm : OO.extend(BS.PluginPropertiesForm, {
    formElement: function() {
      return $('newProfileForm');
    },

    savingIndicator: function() {
      return $('newProfileProviderProgress');
    },

    getSelectedType: function() {
      var el = this.formElement().cloudType;
      var idx = el.selectedIndex;
      return el.options[idx].value;
    },

    beforeSaveForm: function() {},

    saveForm: function() {
      this.beforeSaveForm();
      var that = this;
      var url = this.formElement().getAttributeNode('action').value;
      BS.PasswordFormSaver.save(that, url, OO.extend(BS.ErrorsAwareListener, {
        onCompleteSave: function(form, responseXML, err) {
          var wereErrors = BS.XMLResponse.processErrors(responseXML, {}, that.propertiesErrorsHandler);

          BS.ErrorsAwareListener.onCompleteSave(form, responseXML, err);

          if (wereErrors) {
            BS.Util.reenableForm(that.formElement());
            return;
          }

          document.location.href = window['base_uri'] + "/admin/admin.html?item=clouds";
        }
      }));
    },

    baseParams: function() {
      return "";
    },

    beforeShow: function() {
      var providerType = this.getSelectedType();
      if (providerType == '') {
        this.disableButton(['createButton'], true);
      }
    },

    disableButton: function(ids, value) {
      if (value) {
        ids.each(function(item) {
          $(item).disabled = 'disabled';
        });
      } else {
        ids.each(function(item) {
          $(item).disabled = '';
        });
      }
      var that = this;
      return function() {
        that.disableButton(ids, !value);
      };
    },

    refreshSelectedCloudType: function() {
      var enable;
      var providerType = this.getSelectedType();
      if (providerType != '') {
        enable = this.disableButton(["createButton", "cloudType"], true);
      } else {
        this.disableButton(["createButton"], true);
        enable = null;
      }
      var url = 'cloudType=' + providerType + "&" + this.baseParams();
      $('newProfilesContainer').refresh('newProviderSaving',  url, enable);
    },

    submit: function() {
       var providerType = this.formElement();
       if (providerType == '') {
         alert('Please select Cloud Type');
         return false;
       }
       this.saveForm();
       return false;
     }
  }),

  enabledClouds: function(b, uri) {
    var btn = $(b);
    btn.disable();

    $('enable_integration_loader').show();
    BS.ajaxRequest(uri, {
      parameters : {
        action :  'enable'
      },
      method: 'post',
      onComplete: function() {
        BS.reload(true);
      }
    });
  },

  ConfirmDialog : OO.extend(BS.AbstractModalDialog, {
    aThis : null,

    getContainer: function() {
      return $('confirmShutdownDialog');
    },

    showDialog: function(caption, submitCaption, runningCount) {
      BS.Clouds.Admin.ConfirmDialog.aThis = this;
      $('confirmShutdown_action').innerHTML = caption;
      $('confirmShutdown_submit').value = submitCaption;
      $('confirmShutdown_instanceCount').innerHTML = runningCount;
      if (runningCount == 0) {
        $('terminateInstancesCheckbox').hide();
      }
      this.showCentered();
      this.bindCtrlEnterHandler(this.submit.bind(this));
    },

    shouldKillInstances: function() {
      return $('terminateInstances').checked ? 'true' : 'false';
    },

    //Override this function to handle confirm
    onConfirm: function() {
    },

    submit: function() {
      $('confirmShutdown').disable();
      $('confirmShutdownDialog_loader').show();

      (this.aThis||this).onConfirm();

      return false;
    },

    postSubmit: function() {
      $('confirmShutdownDialog_loader').hide();
      $('confirmShutdown').enable();
      this.close();
    },

    processKillError: function(transport) {
      return BS.XMLResponse.processErrors(transport.responseXML, {
        onKillFailedError: function(elem) {
          alert(elem.firstChild.nodeValue + "\nAction is canceled due to the error");
        }
      });
    }
  })
};

BS.Clouds.Admin.ConfirmDeleteProfileDialog = OO.extend(BS.Clouds.Admin.ConfirmDialog, {
  profileId : "",

  showDeleteProfileDialog: function(profileId, profileName, runningCount) {
    this.profileId = profileId;
    $("confirmShutdownTitle").innerHTML = "Remove profile confirmation";
    return this.showDialog('remove profile "' + profileName, 'Remove', runningCount);
  },

  onConfirm: function() {
    var that = this;
    BS.ajaxRequest(window['base_uri'] + "/clouds/admin/cloudAdminProfile.html", {
      parameters : {
        action : 'delete',
        profileId : that.profileId,
        killInstances : that.shouldKillInstances()
      },
      method : 'post',
      onComplete: function(transport) {
        var handled = that.processKillError(transport);
        if (!handled) {
          BS.Clouds.Admin.refresh();
        }
        that.postSubmit();
      }
    });
  }
});

BS.Clouds.Admin.ConfirmShutdownDialog = OO.extend(BS.Clouds.Admin.ConfirmDialog, {
  showConfirmDisableDialog: function() {
    var running = BS.Clouds.Admin.getRunningInstancesCount();
    $("confirmShutdownTitle").innerHTML = "Disable cloud integration";
    return this.showDialog('disable cloud integration', 'Disable', running);
  },

  onConfirm: function() {
    var form = 'confirmShutdown';
    var url = $(form).getAttribute('action');

    var that = this;
    BS.ajaxRequest(url, {
      parameters : {
        action : 'disable',
        kill : that.shouldKillInstances()
      },
      method: 'post',
      onComplete: function(transport) {
        var handled = that.processKillError(transport);
        if (!handled) {
          BS.reload(true);
        }
        that.postSubmit();
      }
    });
  }
});


