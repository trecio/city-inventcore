BS.Clouds = {
  Problems : {},

  registerProblem: function(id) {
    BS.Clouds.Problems[id] = OO.extend(
        new BS.Popup(id, {  delay: 0,  hideDelay: -1}),
        {
          showMe: function(el) {
            this.showPopupNearElement(el);
        }
       });
  },

  _showSimpleAjax: function(beforeAjax, confirmText, params) {
    BS.Clouds.unregisterRefresh();

    var r = confirm(confirmText);
    if (r) {
      beforeAjax();
      BS.ajaxRequest(window['base_uri'] + "/clouds/tab.html",
      {
        parameters: params,
        method : "post",
        onComplete: function() {
          BS.Clouds.refresh();
        }
      });
    } else {
      BS.Clouds.registerRefresh();
    }
    return false;
  },

  registerRefreshable: function(fun) {
    var isPending = false;
    BS.PeriodicalRefresh.start(15, function() {
      if (!isPending) {
        isPending = true;
        try {
          BS.Clouds.Problems = {};
          fun();
        } finally {
          isPending = false;
        }
      }
    });
  },
  
  registerRefresh: function() {
    document.observe("dom:loaded", function() {
      BS.Clouds.registerRefreshable(BS.Clouds.refresh.bind(BS.Clouds));
    });
  },

  unregisterRefresh: function() {
    BS.PeriodicalRefresh.stop();
  },

  refresh: function() {
    $('cloudRefreshable').refresh();
  },

  startInstance: function(uniqueId, profileId, imageId) {
    return BS.Clouds._showSimpleAjax(
        function() {
          $('startImageButton_' + uniqueId).disabled = 'disabled';
          $('startImageLoader_' + uniqueId).show();
        },
        "Are you sure you want to start new cloud image instance?",
        {
          action : "startInstance",
          profileId : profileId,
          imageId : imageId
        }
    );
  },

  stopInstance: function(uniqueId, profileId, imageId, instanceId) {
    return BS.Clouds._showSimpleAjax(
        function() {
          $('stopInstanceDiv_' + uniqueId).hide();
          $('stoppingInstaceDiv_' + uniqueId).show();
        },
        "Are you sure you want to stop or terminate cloud instance?",
        {
          action : "stopInstance",
          profileId : profileId,
          imageId : imageId,
          instanceId : instanceId
        });
  }
};