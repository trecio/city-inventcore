<%@ include file="include-internal.jsp" %>
<%@ taglib prefix="agent" tagdir="/WEB-INF/tags/agent" %>
<jsp:useBean id="serverSummary" type="jetbrains.buildServer.web.openapi.ServerSummary" scope="request"/>
<jsp:useBean id="compatibilityModel" scope="request" type="jetbrains.buildServer.controllers.compatibility.CompatibilityModel"/>

<bs:linkCSS dynamic="${true}">
  /css/agentBlocks.css
</bs:linkCSS>
<bs:linkScript>
  /js/bs/agentBlocks.js
</bs:linkScript>
<script type="text/javascript">
  BS.AgentBlocks.mustPersistState = false;
</script>

<c:if test="${compatibilityModel.hasSeveralPools}">
  <agent:poolCollapseExpandAll grouped="${true}"/>
</c:if>

<bs:_compatibilityTable tableModel="${compatibilityModel.activeTable}" active="${true}"  />

<c:if test="${compatibilityModel.inactiveTable.notEmpty}">
  <br/>
  <div class="attentionComment">Following agents belong to the agent pools which are not associated with "<c:out value="${compatibilityModel.projectName}"/>" project</div>
  <br/>
  <bs:_compatibilityTable tableModel="${compatibilityModel.inactiveTable}" active="${false}" />
</c:if>
