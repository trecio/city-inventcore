<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %><%@
    include file="include-internal.jsp"

%><jsp:useBean id="modification" type="jetbrains.buildServer.vcs.VcsModification" scope="request"
/><jsp:useBean id="changedFile" type="jetbrains.buildServer.vcs.VcsChangeInfo" scope="request"
/><c:set var="title" value="${fn:split(changedFile.relativeFileName,'\\\/')}"
/><c:set var="title" value="${title[fn:length(title)-1]} diff"
/><bs:externalPage
  ><jsp:attribute name="page_title">${title}</jsp:attribute
    ><jsp:attribute name="head_include">
    <%--@elvariable id="changes" type="String"--%>
    <%--@elvariable id="styles" type="String"--%>
    <bs:linkCSS>
      /css/main.css
      /css/diffView.css
      /css/filePopup.css
      /css/forms.css
      /css/issues.css
      /css/highlight-idea.css
    </bs:linkCSS>
    <style type="text/css">
      ${styles}
    </style>

    <!--[if lt IE 9]>
    <style type="text/css">
      .source .list {
        counter-reset: none;
        list-style: decimal;
      }

      .source li:before {
        content: none;
        counter-increment: none;
      }
    </style>
    <![endif]-->

    <bs:linkScript>
      /js/aculo/dragdrop.js

      /js/swfobject.js
      /js/jquery/jquery.zclip.min.js

      /js/bs/forms.js
      /js/bs/modalDialog.js
      /js/bs/activation.js
      /js/bs/issues.js
    </bs:linkScript>
  </jsp:attribute>
  <jsp:attribute name="body_include">
  <%--@elvariable id="before" type="String"--%>
  <%--@elvariable id="after" type="String"--%>
<c:choose>
  <c:when test="${changedFile.type == 'CHANGED'}"><c:set var="chg" value="edited"/></c:when>
  <c:when test="${changedFile.type == 'ADDED'}"><c:set var="chg" value="added"/></c:when>
  <c:when test="${changedFile.type == 'REMOVED'}"><c:set var="chg" value="deleted"/></c:when>
  <c:when test="${changedFile.type == 'NOT_CHANGED'}"><c:set var="chg" value="unchanged"/></c:when>
</c:choose>

<c:set var="isActualModification" value="${modification.id >= 0}"/>
<c:if test="${isActualModification}">
  <c:set var="queryParams" value="modId=${modification.id}&personal=${modification.personal}"/>
  <c:set var="queryParams" value="${queryParams}&openFileLinksInSameTab=true&highlightChange=${changedFile.fileName}"/>
  <c:url value='/diffView.html?id=${modification.id}&personal=${modification.personal}' var="diffUrl"/>
</c:if>

<div id="mainContent">
  <div id="diffView">
    <div id="fileName">
      <a class="closeWindow" href="#" onclick="window.close(); return false" title="Close window"><img src="<c:url value="/img/closeWindow2.gif"/>"></a>
        <%--@elvariable id="prevChange" type="String"--%>
        <%--@elvariable id="nextChange" type="String"--%>
      <c:if test="${isActualModification}">
      <span class="prevNextContainer">
        <c:choose>
          <c:when test="${empty prevChange}">
            <a class="prevNext prevNextDisabled" href="#">
              &larr;
            </a>
          </c:when>
          <c:otherwise>
            <a class="prevNext" href="${diffUrl}&vcsFileName=${prevChange}" title="Show previous file diff">
              &larr;
            </a>
          </c:otherwise>
        </c:choose>

        <c:choose>
          <c:when test="${empty nextChange}">
            <a class="prevNext prevNextDisabled" href="#">
              &rarr;
            </a>
          </c:when>
          <c:otherwise>
            <a class="prevNext" href="${diffUrl}&vcsFileName=${nextChange}" title="Show next file diff">
              &rarr;
            </a>
          </c:otherwise>
        </c:choose>
      </span>
      </c:if>

      <c:set var="chgAndFileName">${chg} <bs:trimWithTooltip trimCenter="true" maxlength="120">${changedFile.relativeFileName}</bs:trimWithTooltip></c:set
          ><c:choose><c:when test="${isActualModification}"><bs:popupControl
        showPopupCommand="BS.FilesPopup.showPopup(event, {parameters: '${queryParams}'});"
        hidePopupCommand="BS.FilesPopup.hidePopup();"
        stopHidingPopupCommand="BS.FilesPopup.stopHidingPopup();"
        controlId="modfiles:${modification.id}"
        clazz="fileNamePopup">${chgAndFileName}</bs:popupControl
        ><ext:includeExtensions placeId="<%=PlaceId.CHANGED_FILE_LINK%>"/></c:when
        ><c:otherwise><span style="color: white;">${chgAndFileName}</span></c:otherwise></c:choose>
    </div>

    <div id="desc">
      <div class="buttonbar">
        <jsp:useBean id="ignoreSpaces" type="java.lang.Boolean" scope="request"/>
        <forms:checkbox id="ignoreSpaces" name="ignoreSpaces" checked="${ignoreSpaces}" onclick="BS.DiffView.ignoreSpaces(${not ignoreSpaces});"/>
        <label for="ignoreSpaces">Ignore whitespaces</label>

        <c:if test="${isActualModification}">
        <span style="margin-left: 1em">
          <a href="#" onclick="BS.Activator.doOpen('file?file=${changedFile.relativeFileName}'); return false"
             title="Open file in IDE" <bs:iconLinkStyle icon="/img/openInIde.gif"/>>Open in IDE</a>
        </span>
        </c:if>
      </div>

      <div class="description">
        <c:choose>
          <c:when test="${isActualModification}">
            <strong><bs:changeCommitters modification="${modification}"/></strong>: <bs:out value="${modification.description}"/>
          </c:when>
          <c:otherwise>
            <strong><c:out value="${modification.userName}"/></strong>: <bs:out value="${modification.description}"/>
          </c:otherwise>
        </c:choose>
      </div>
    </div>

  </div>

  <div id="toolbar">
  <span class="column-before">
    <c:if test="${param['personal']=='true'}"><span class="personalBefore">NOTE: Displaying LATEST repository revision</span></c:if>
    <a href="#" class="clipboard" data-for="tbefore">Copy to clipboard</a>
  </span>
  <span class="column-tools">
    <button class="btn btn_mini tool" onclick="BS.DiffView.cc = (BS.DiffView.cc - 1 + BS.DiffView.changes.length) % BS.DiffView.changes.length; BS.DiffView.scrollToChange(); this.blur();" title="Previous change">
      <img src="<c:url value="/img/up+.gif"/>" alt="">
    </button>
    <button class="btn btn_mini tool" onclick="BS.DiffView.cc = (BS.DiffView.cc + 1) % BS.DiffView.changes.length; BS.DiffView.scrollToChange(); this.blur();" title="Next change">
      <img src="<c:url value="/img/down+.gif"/>" alt="">
    </button>
  </span>
  <span class="column-after">
    <a href="#" class="clipboard" data-for="tafter">Copy to clipboard</a>
  </span>
  </div>

  <div style="text-align:center;" id="status"></div>

  <c:if test="${empty styles}">
    <c:set var="highlightClass">needsHighlight</c:set>
  </c:if>

  <div id="panels" style="display:none;">
    <div id="dbefore" class="custom-scroll">
      <pre>
        <div id="tbefore" class="source ${highlightClass}">${before}<span id=bottom></span></div>
      </pre>
    </div>
    <div id="dmap" class="dmap">
      <div id="mapWindow"></div>
    </div>
    <div id="dafter" class="custom-scroll">
      <pre>
        <div id="tafter" class="source ${highlightClass}">${after}</div>
      </pre>
    </div>
  </div>

  <div id="contentPlain" style="display: none">
    <textarea id="tbeforePlain"><c:out value="${beforePlain}"/></textarea>
    <textarea id="tafterPlain"><c:out value="${afterPlain}"/></textarea>
  </div>
</div>

<bs:linkScript>
  /js/bs/diffView.js
  /js/highlight.pack.js
</bs:linkScript>

<script type="text/javascript">
  //[*[0-lCount, 1-lStart, 2-rCount, 3-rStart, 4-addedEmptyLines, 5-lastLine]]
  BS.DiffView.changes = [${changes}];
  BS.DiffView.fileName = "<bs:escapeForJs text="${changedFile.relativeFileName}"/>";
  BS.DiffView.beforeLines = ${beforeLines};
  BS.DiffView.loadingMessage = '<img src="<c:url value="/img/ajax-loader.gif"/>" alt="" class="progressRing">Calculating... (This may take long time to complete).';
  BS.DiffView.pageUrl = '${pageUrl}';

  <%--@elvariable id="contentInlined" type="boolean"--%>
  <c:if test="${contentInlined}">
  BS.DiffView.init();
  </c:if>
</script>

<%--@elvariable id="contentIsGraphics" type="boolean"--%>
<c:if test="${isActualModification && contentIsGraphics}">
<script type="text/javascript">
  // Process images
  <c:url value="/rawVcsModification.html?modId=${modification.id}&personal=${modification.personal}&vcsFileName=${changedFile.fileName}"
         var="url"
  /><c:if test="${not changedFile.type.added}">$('dbefore').innerHTML = "<iframe src='${url}&show=before'/>";</c:if
   ><c:if test="${not changedFile.type.removed}">$('dafter').innerHTML = "<iframe src='${url}&show=after'/>";</c:if
   >
</script>
</c:if>

<bs:modalDialog formId="errorForm"
                title=""
                action="#"
                saveCommand="BS.ErrorDialog.close();"
                closeCommand="BS.ErrorDialog.close();">
</bs:modalDialog>

<div id="issueDetailsTemplate" style="display:none">
  <span id="detailedSummary:##ISSUE_ID##">
    <forms:progressRing className="progressRingInline"/>
  </span>
</div>

  </jsp:attribute>
</bs:externalPage>