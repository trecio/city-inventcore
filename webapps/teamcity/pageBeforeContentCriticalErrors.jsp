<%@ include file="include-internal.jsp"%>
<jsp:useBean id="criticalErrors" type="jetbrains.buildServer.serverSide.ErrorsCollection" scope="request"/>

<authz:authorize allPermissions="VIEW_SERVER_ERRORS">
  <c:forEach items="${criticalErrors.errors}" var="error">
    <div class="attentionComment"><c:out value="${error}" escapeXml="false"/></div>
  </c:forEach>
</authz:authorize>

  