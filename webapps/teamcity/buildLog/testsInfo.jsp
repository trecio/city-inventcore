<%@ page import="jetbrains.buildServer.web.util.WebUtil" %>
<%@ include file="../include-internal.jsp"
  %><%@ taglib prefix="props" tagdir="/WEB-INF/tags/props"
  %><%@ taglib prefix="tt" tagdir="/WEB-INF/tags/tests"
  %>

<!-- SEE css/buildLog/testsInfo.css for styles -->

<div id="testsInfo">
  <tt:setSelfLink buildData="${buildData}"/>
  <bs:refreshable containerId="testInfoRefreshable" pageUrl="${selfLink}" useJsp="buildLog/testInfoRefreshableInner.jsp">
    <%@ include file="testInfoRefreshableInner.jsp" %>
  </bs:refreshable>
</div>

<script type="text/javascript">
    BS.GraphPopup = new BS.Popup(
      'testsDurationGraph', {
      delay: 0,
      hideDelay: -1,
      url: '<c:url value="/buildGraph.html?jsp=buildLog/testDuration.jsp&buildTypeId=${buildData.buildTypeId}"/>',
      shift: {
        x: -100,
        y: 15
      },
      loadingText: 'Loading graph...'
    });
    document.observe("dom:loaded", function() {
      $('filterText').focus();
    });
</script>
