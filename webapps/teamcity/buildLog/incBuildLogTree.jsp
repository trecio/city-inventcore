<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="server" type="jetbrains.buildServer.serverSide.SBuildServer" scope="request"/>
<jsp:useBean id="buildData" type="jetbrains.buildServer.serverSide.SBuild" scope="request"/>
<jsp:useBean id="tailMessages" type="jetbrains.buildServer.serverSide.buildLog.LowLevelEventsAwareMessageIterator" scope="request"/>
<jsp:useBean id="enabledFilter" type="java.lang.String" scope="request"/>

<bs:changeRequest key="messagesIterator" value="${tailMessages}">
  <jsp:include page="buildLogTreePrinter.html">
    <jsp:param name="enabledFilter" value="${enabledFilter}"/>
    <jsp:param name="incremental" value="true"/>
  </jsp:include>
</bs:changeRequest>

<div class='hidden updateInfo'>${tailMessages.currentIndex}</div>

<c:if test="${buildData.finished}">
  <div style="display: none;">Build finished</div>
</c:if>

<div class="updateInfo" style="display: none;">
  <bs:buildDataIcon buildData="${buildData}" imgId="buildDataIcon" imgOnly="true"/>
  <script type="text/javascript">
    BS.BuildLog.minorUpdate("${buildData.buildLog.sizeEstimate}");
  </script>
</div>
