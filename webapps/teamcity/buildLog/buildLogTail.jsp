<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>

<jsp:useBean id="buildData" type="jetbrains.buildServer.serverSide.SBuild" scope="request"/>

<jsp:useBean id="tailIterator" type="java.util.Iterator" scope="request"/>
<bs:buildLog buildData="${buildData}" messagesIterator="${tailIterator}" renderRunningTime="true" mergeTestOutput="false"/>

<c:if test="${not buildData.finished}">
  <script type="text/javascript">
    $j(document).ready(function () {
      BS.BuildLog.enableRefresh();
    });
  </script>
</c:if>

