<%@ include file="../include-internal.jsp" %><%@
    taglib prefix="tags" tagdir="/WEB-INF/tags/tags" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %><%@
    taglib prefix="labels" tagdir="/WEB-INF/tags/labels" %><%@
    taglib prefix="bs" tagdir="/WEB-INF/tags"%><%@
    taglib prefix="tt" tagdir="/WEB-INF/tags/tests"

%><jsp:useBean id="buildResultsBean" type="jetbrains.buildServer.controllers.viewLog.BuildResultsBean" scope="request"
/><c:set var="buildStatistics" value="${buildResultsBean.buildStatistics}"
/><c:set var="dependenciesBean" value="${buildResultsBean.dependenciesBean}"
/><jsp:useBean id="buildData" type="jetbrains.buildServer.serverSide.SBuild" scope="request"

/><script type="text/javascript">
  enablePeriodicalRefresh();
  BS.BuildResults._expanded_count = 0;
</script>

<c:if test="${not buildData.finished}">
  <div id="noRefreshWarning" class="attentionComment" style="display:none;">
    This page will not refresh until you hide all open stacktraces on it
    (<a href="#" onclick="BS.unblockRefresh('stacktrace'); $('buildResults').refresh(null, 'runningBuildRefresh=1'); return false;">force refresh</a>)
  </div>
</c:if>

<%@ include file="userBuildDetails.jspf" %>

<bs:buildDataPlugins/>

<c:set var="failedTestCount" value="${fn:length(buildStatistics.failedTests)}"/>
<c:set var="mutedTestCount" value="${fn:length(buildStatistics.mutedTests)}"/>
<c:set var="ignoredTestCount" value="${fn:length(buildStatistics.ignoredTests)}"/>

<c:set var="maxTests2Show" value="${param['maxFailed']}"/>
<c:if test="${empty maxTests2Show}">
  <c:set var="maxTests2Show" value="1000"/>
</c:if>

<!--Compilation errors info-->

<c:set var="compilationErrorMessages" value="${buildStatistics.compilationErrorBlocks}"/>

<c:if test="${not empty compilationErrorMessages}">
  <c:set var="state" value="expanded"/>
  <l:blockStateCss blocksType="Block_compileErrors" collapsedByDefault="false" id="compileErrorsData"/>
  <p class="blockHeader ${state}" id="compileErrors">
    <bs:icon icon="error.gif"/> Compilation errors found
  </p>
  <ul id="compileErrorsData" class="compiler-errors">
    <c:forEach items="${compilationErrorMessages}" var="block" varStatus="status">
      <c:set var="id" value="message-${status.index}"/>
      <li class="compiler-error">
        ${block.compilerMessageForWeb}
      <br/>
      </li>
    </c:forEach>
  </ul>

  <script type="text/javascript">
    <l:blockState blocksType="Block_compileErrors"/>
    new BS.BlocksWithHeader('compileErrors');
  </script>

</c:if>

<!--Failed tests info-->

<c:if test="${failedTestCount > 0}">

  <c:set var="state" value="expanded"/>
  <c:set var="failedTitle">
      <bs:icon icon="error.gif"/>
      <span class="failCount">${failedTestCount} test<bs:s val="${failedTestCount}"/> failed (<bs:new count="${buildStatistics.newFailedCount}"/>)</span>
  </c:set>

  <bs:_collapsibleBlock title="${failedTitle}" id="idfailed">
      <bs:showAllTests buildData="${buildData}" maxTests2Show="${maxTests2Show}" testCount="${failedTestCount}" testType="failed"/>
      <bs:printTests tests="${buildStatistics.failedTests}" buildData="${buildData}" maxTests2Show="${maxTests2Show}"/>
  </bs:_collapsibleBlock>

</c:if>

<!--Muted tests info-->

<c:if test="${mutedTestCount > 0}">
  <c:set var="state" value="expanded"/>

  <c:set var="mutedTitle">
    <bs:icon icon="../muted-red.gif"/> <span class="mutedCount">${mutedTestCount} test failure<bs:s val="${mutedTestCount}"/> muted</span>
  </c:set>

  <bs:_collapsibleBlock title="${mutedTitle}" id="idmuted">
    <bs:showAllTests buildData="${buildData}" maxTests2Show="${maxTests2Show}" testCount="${mutedTestCount}" testType="muted"/>
    <tt:_testGroupForBuild tests="${buildStatistics.mutedTests}" maxTests2Show="${maxTests2Show}"
                           buildData="${buildData}" showMuteFromTestRun="true" id="build_mute"/>
  </bs:_collapsibleBlock>
</c:if>

<!--Tests ignored info-->

<c:if test="${ignoredTestCount > 0}">
  <c:set var="ignoredTitle">
    <bs:icon icon="ignored.gif"/> <span class="failCount">${ignoredTestCount} test<bs:s val="${ignoredTestCount}"/> ignored</span>
  </c:set>

  <bs:_collapsibleBlock title="${ignoredTitle}" id="idignored" collapsedByDefault="true">
    <bs:showAllTests buildData="${buildData}" maxTests2Show="${maxTests2Show}" testCount="${ignoredTestCount}" testType="ignored"/>
    <tt:testGroupWithActions groupedTestsBean="${buildResultsBean.ignoredGroup}"
                             groupSelector="noBuildType" defaultOption="package" singleBuildTypeContext="true"
                             maxTests="${maxTests2Show > 0 ? maxTests2Show : 100000}"
                             maxTestsPerGroup="${maxTests2Show > 0 ? maxTests2Show/2 : 100000}" id="build_ignore"
                             withoutFixAction="${true}">
      <jsp:attribute name="testLinkAttrs">onclick='return false;'</jsp:attribute>
      <jsp:attribute name="testAfterName">
          <c:set var='test' value="${testBean.run}"/>
          <c:if test="${not empty test.ignoreComment}">
            <span class="commentText">&mdash; <bs:out value="${test.ignoreComment}"/></span>
          </c:if>
      </jsp:attribute>
      <jsp:attribute name="viewAllUrl">
      </jsp:attribute>
        </tt:testGroupWithActions>
  </bs:_collapsibleBlock>
</c:if>

<!--Tests passed info-->

<c:if test="${buildStatistics.passedTestCount > 0}">
  <p class="passedTestsBlock">
    <bs:icon icon="success.gif"/> <span class="passCount">${buildStatistics.passedTestCount} tests passed</span>
  </p>
</c:if>

<!--Dependencies-->

<c:if test="${not empty dependenciesBean.failedDependencies or not empty dependenciesBean.compilationErrors}">
  <c:if test="${not empty dependenciesBean.compilationErrors}">
    <c:set var="title"><bs:icon icon="error.gif"/> Dependencies failed with compilation error: ${fn:length(dependenciesBean.compilationErrors)}</c:set>
    <bs:_collapsibleBuilds title="${title}" id="failedDepsCompile" builds="${dependenciesBean.compilationErrors}"/>
  </c:if>

  <c:if test="${dependenciesBean.groupedFailedTests.testsNumber > 0}">
    <c:set var="title2"><bs:icon icon="error.gif"/> Failed tests from dependent builds: ${dependenciesBean.groupedFailedTests.testsNumber}</c:set>
    <bs:_collapsibleBlock title="${title2}" id="failedDepsTest">
      <tt:testGroupWithActions groupedTestsBean="${dependenciesBean.groupedFailedTests}" defaultOption="bt" groupSelector="true"
                               id="build_dep"/>
    </bs:_collapsibleBlock>
  </c:if>

  <c:if test="${not empty dependenciesBean.failedDependencies}">
  <c:set var="title3"><bs:icon icon="error.gif"/> Failed dependencies: ${fn:length(dependenciesBean.failedDependencies)}</c:set>
  <bs:_collapsibleBuilds title="${title3}" id="failedDepsBuild" builds="${dependenciesBean.failedDependencies}"/>
  </c:if>
</c:if>

<!--Build problems info-->

<bs:buildProblemsList buildProblems="${buildResultsBean.knownBuildProblems}"/>

<!--Build log errors-->

<c:set var="buildProblemsCount" value="${fn:length(buildResultsBean.buildProblems)}"/>
<c:set var="has_TC_ERROR_MESSAGE_problem" value='<%=buildData.hasBuildProblemOfType("TC_ERROR_MESSAGE")%>'/>
<c:set var="has_TC_EXIT_CODE_problem" value='<%=buildData.hasBuildProblemOfType("TC_EXIT_CODE")%>'/>
<c:set var="has_TC_UNKNOWN_problem" value='<%=buildData.hasBuildProblemOfType("TC_UNKNOWN")%>'/>
<c:set var="TC_EXIT_CODE_is_the_only_problem" value="${buildProblemsCount == 1 && has_TC_EXIT_CODE_problem}"/>

<c:if test="${failedTestCount == 0 &&
              buildStatistics.compilationErrorsCount == 0 &&
              !buildData.buildStatus.successful &&
              empty dependenciesBean.failedDependencies &&
              empty dependenciesBean.compilationErrors &&
              (buildProblemsCount == 0 || has_TC_ERROR_MESSAGE_problem || TC_EXIT_CODE_is_the_only_problem || has_TC_UNKNOWN_problem)}">
<c:catch var="error">
  <c:set var="errorMessages" value="${buildData.buildLog.errorMessages}"/>
  <c:if test="${not empty errorMessages}">
    <div style="margin-top: 0.5em">
      <h2>Build errors</h2>
    </div>
    <bs:buildLog buildData="${buildData}" messagesList="${errorMessages}" renderRunningTime="true"/>
  </c:if>
  </c:catch>
  <c:if test="${not empty error}">
    <div class="error" style="margin: 1em 0 0 0;">${error.cause == null ? error.message : error.cause.message}</div>
  </c:if>
</c:if>
