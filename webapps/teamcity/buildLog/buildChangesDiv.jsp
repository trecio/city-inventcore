<%@ include file="../include-internal.jsp" %>
<%@ taglib prefix="labels" tagdir="/WEB-INF/tags/labels" %>

<!--=========================== VCS ROOTS INFORMATION BLOCK =======-->
<jsp:useBean id="revisionsBean" type="jetbrains.buildServer.controllers.RevisionsBean" scope="request"/>
<c:set var="defaultBranch" value="${buildData.branch.defaultBranch}"/>

<c:if test="${not empty revisionsBean.revisions or (not empty depsRevisionsBean and not empty depsRevisionsBean.revisions)}">
  <div id="vcsInformation">
    <bs:refreshable containerId="buildLabels" pageUrl="${pageUrl}">
    <c:set var="buildType"  value="${buildData.buildType}"/>

    <div class="clr"></div>
    <table cellspacing="0" class="plain changeRevisionsTable">
      <tr>
        <th class="vcsRoot">VCS Root</th>
        <th class="revision">Revision</th>
        <th class="label">Label</th>
      </tr>
      <c:forEach var="revision" items="${revisionsBean.revisions}">
        <tr>
          <td class="vcsRoot"><em>(<c:out value="${revision.root.vcsName}"/>)</em> <c:out value="${revision.root.name}"/>
            <span class="noteOnVcsRootInBuild">
              <c:set var="vcsRoot" value="${revision.root}" scope="request"/>
              <ext:includeJsp jspPath="/buildLog/buildChangesVcsDetails.jsp"/>
            </span>
          </td>
          <c:set var="vcsBranch" value="${revision.repositoryVersion.vcsBranch}"/>
          <td class='revision <c:if test="${vcsBranch != null}">branch <c:if test="${defaultBranch}">default</c:if><c:if test="${not defaultBranch}">hasBranch</c:if></c:if>'>
            <c:if test="${vcsBranch != null}">
              <span class="branchName"><c:out value="${vcsBranch}"/></span>
            </c:if>
            <span class="revisionNum"><c:out value="${revision.revisionDisplayName}"/></span>
          </td>
          <td class="label">
            <c:set var="label" value="${revisionsBean.labels[revision.root.id]}"/>
            <c:choose>
              <c:when test="${label eq null}"><span class="commentText">none</span></c:when>
              <c:when test="${label.successful}"><c:out value="${label.labelText}"/></c:when>
              <c:when test="${label.failed}"><div class="error" style="margin-left: 0">Failed: <c:out value="${label.failureReason}"/></div></c:when>
              <c:otherwise><span class="commentText">none (<c:out value="${label.statusText}"/>)</span></c:otherwise>
            </c:choose>
          </td>
        </tr>
      </c:forEach>
      <c:if test="${(not empty depsRevisionsBean) && (not empty depsRevisionsBean.revisions)}">
        <jsp:useBean id="depsRevisionsBean" type="jetbrains.buildServer.controllers.RevisionsBean" scope="request"/>
        <c:set var="buildType"  value="${buildData.buildType}"/>
          <c:forEach var="revision" items="${depsRevisionsBean.revisions}">
            <tr>
              <td class="vcsRoot">
                <img class="dependencyRelationIcon"  src="<c:url value='/img/dependencyChange.gif'/>" alt="" title="This root is attached to snapshot dependency"/>
                <em>(<c:out value="${revision.root.vcsName}"/>)</em> <c:out value="${revision.root.name}"/>
              </td>
              <c:set var="vcsBranch" value="${revision.repositoryVersion.vcsBranch}"/>
              <td class='revision <c:if test="${vcsBranch != null}">branch <c:if test="${defaultBranch}">default</c:if><c:if test="${not defaultBranch}">hasBranch</c:if></c:if>'>
                <c:if test="${vcsBranch != null}">
                  <span class="branchName"><c:out value="${vcsBranch}"/></span>
                </c:if>
                <span class="revisionNum"><c:out value="${revision.revisionDisplayName}"/></span>
              </td>
              <td class="label"></td>
            </tr>
          </c:forEach>
      </c:if>
    </table>
      <div class="changeRevisionsActions clearfix">
        <div class="right">
        <authz:authorize projectId="${buildType.projectId}" allPermissions="LABEL_BUILD">
        <c:if test="${revisionsBean.buildFinished and not empty revisionsBean.availableRootsToLabel}">
          <a class="setLabelLink" href="#" onclick="BS.Label.showEditDialog(${buildData.buildId}); return false">Label this build sources</a>
        </c:if>
        </authz:authorize>
        </div>

        <authz:authorize projectId="${buildType.projectId}" allPermissions="EDIT_PROJECT">
          <admin:editBuildTypeLink step="vcsRoots" buildTypeId="${buildData.buildTypeId}" title="Configure VCS settings and labels">Configure VCS settings and labels</admin:editBuildTypeLink>
        </authz:authorize>
      </div>
      <div class="messagesHolder">
        <bs:messages key="vcsLabelingSuccessful"/>
        <bs:messages key="vcsLabelingFailed" className="error" style="margin-left: 0"/>
      </div>

    <labels:form vcsRootsBean="${revisionsBean}" buildId="${buildData.buildId}"/>
    </bs:refreshable>
  </div>
</c:if>

<!--=========================== CHANGES BLOCK =======-->
<jsp:useBean id="changeLogBean" type="jetbrains.buildServer.controllers.buildType.tabs.ChangeLogBean" scope="request"/>
<jsp:useBean id="build" type="jetbrains.buildServer.serverSide.SBuild" scope="request"/>
<bs:changesList changeLog="${changeLogBean}"
                url="viewLog.html?buildId=${build.buildId}&buildTypeId=${build.buildTypeId}&tab=buildChangesDiv"
                filterUpdateUrl="buildChangeLogTab.html?buildId=${build.buildId}&buildTypeId=${build.buildTypeId}"
                projectId="${build.projectId}"
                hideBuildSelectors="true"
                hideShowBuilds="true"
                enableCollapsibleChanges="true"/>