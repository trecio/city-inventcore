<%@ include file="../include-internal.jsp" %><%@
    taglib prefix="admin" tagdir="/WEB-INF/tags/admin" %><%@
    taglib prefix="tags" tagdir="/WEB-INF/tags/tags" %><%@
    page import="jetbrains.buildServer.serverSide.BuildPromotionEx" %><%@
    page import="jetbrains.buildServer.vcs.SelectPrevBuildPolicy" %><%@
    page import="jetbrains.buildServer.web.openapi.PlaceId"
%><jsp:useBean id="buildData" type="jetbrains.buildServer.serverSide.SBuild" scope="request"
/><c:set var="no_refresh" scope="request" value="true"
/><c:set var="buildType" value="${buildData.buildType}" scope="request"
/><jsp:useBean id="buildType" type="jetbrains.buildServer.serverSide.SBuildType" scope="request"

/><ext:defineExtensionTab placeId="<%=PlaceId.BUILD_RESULTS_TAB%>"/>
<%--@elvariable id="extensionTab" type="jetbrains.buildServer.web.openapi.CustomTab"--%>
<c:set var="isBuildOverview" value="${extensionTab.tabId == 'buildResultsDiv'}"
/><c:set var="currentTabCaption" value=" > ${extensionTab.tabTitle}"
/><c:set var="startDate"><bs:date value="${buildData.startDate}" pattern="dd MMM yy HH:mm" no_span="true"/></c:set
 ><c:set var="pageTitle" scope="request">${buildType.fullName} > #${buildData.buildNumber} (${startDate})${currentTabCaption}</c:set

><%
  final BuildPromotionEx buildPromotion = (BuildPromotionEx)buildData.getBuildPromotion();
  final int numDetectedChanges = buildPromotion.getDetectedChanges(SelectPrevBuildPolicy.SINCE_LAST_BUILD).size();
  pageContext.setAttribute("numDetectedChanges", numDetectedChanges);
%>
<bs:page>
<jsp:attribute name="quickLinks_include">
  <authz:authorize projectId="${buildType.projectId}" allPermissions="RUN_BUILD">
    <div class="toolbarItem">
      <bs:runBuild buildTypeId="${buildType.buildTypeId}"
                   redirectTo="viewType.html?buildTypeId=${buildType.buildTypeId}"
                   userBranch="${buildData.branch}"/>
    </div>
  </authz:authorize>
  <authz:authorize projectId="${buildType.projectId}" anyPermission="COMMENT_BUILD,PIN_UNPIN_BUILD,TAG_BUILD">
    <div class="toolbarItem">
      <bs:simplePopup controlId="bdActions" linkOpensPopup="true"
                      popup_options="delay: 0, shift: {x: -100, y: 20}, width: '10em', className: 'quickLinksMenuPopup'">
        <jsp:attribute name="content">
          <div id="bdDetails">
            <ul class="menuList">
              <authz:authorize projectId="${buildType.projectId}" allPermissions="COMMENT_BUILD">
                <c:set var="commentLinkText">Comment...</c:set>
                <l:li><bs:buildCommentLink buildId="${buildData.buildId}"
                                           oldComment="${buildData.buildComment.comment}">${commentLinkText}</bs:buildCommentLink></l:li>
              </authz:authorize>
              <c:if test="${buildData.finished}">
              <authz:authorize projectId="${buildType.projectId}" allPermissions="PIN_UNPIN_BUILD">
                  <c:set var="pinLinkText">Pin...</c:set>
                  <c:if test="${buildData.pinned}">
                    <c:set var="pinLinkText">Unpin...</c:set>
                  </c:if>
                  <l:li><bs:pinLink build="${buildData}" pin="${not buildData.pinned}" onBuildPage="true">${pinLinkText}</bs:pinLink></l:li>
              </authz:authorize>
              </c:if>
              <authz:authorize projectId="${buildType.projectId}" allPermissions="TAG_BUILD">
                <c:set var="tagLinkText">Tag...</c:set>
                <l:li><tags:editTagsLink build="${buildData}">${tagLinkText}</tags:editTagsLink></l:li>
              </authz:authorize>
              <c:if test="${not buildData.personal &&
                            (buildData.buildType.numberOfArtifactReferences > 0 || buildData.buildType.numberOfDependencyReferences > 0)}">
                <l:li>
                  <a onclick="BS.PromoteBuildDialog.showDialog(${buildData.buildId}); return false" href="#">Promote...</a>
                </l:li>
              </c:if>

              <c:if test="${not buildData.internalError}">
                <authz:authorize projectId="${buildData.projectId}" allPermissions="MANAGE_BUILD_PROBLEM_INSTANCES">
                  <l:li>
                    <bs:changeBuildStatusLink build="${buildData}"/>
                  </l:li>
                </authz:authorize>
              </c:if>

            </ul>
          </div>
        </jsp:attribute>
        <jsp:body>Build Actions</jsp:body>
      </bs:simplePopup>
    </div>
  </authz:authorize>
  <authz:editBuildTypeGranted buildType="${buildType}">
    <admin:editBuildTypeMenu buildType="${buildType}">Edit Configuration Settings</admin:editBuildTypeMenu>
  </authz:editBuildTypeGranted>
</jsp:attribute>

<jsp:attribute name="head_include">
  <bs:linkCSS>
    /css/modificationListTable.css
    /css/statusTable.css
    /css/buildDataStatus.css

    /css/buildLog/runningLogStatus.css
    /css/buildLog/buildLog.css
    /css/buildLog/buildLogTree.css
    /css/buildLog/buildLogColors.css
    /css/buildLog/viewLog.css
    /css/filePopup.css
    /css/buildTypeSettings.css
    /css/labels.css
    /css/tags.css
    /css/viewModification.css
    /css/pager.css
  </bs:linkCSS>
  <bs:linkScript>
    /js/swfobject.js
    /js/jquery/jquery.zclip.min.js

    /js/bs/buildResultsDiv.js
    /js/bs/blocks.js
    /js/bs/blocksWithHeader.js
    /js/bs/blockWithHandle.js
    /js/bs/changesBlock.js
    /js/bs/collapseExpand.js

    /js/bs/runningBuilds.js

    /js/bs/pin.js
    /js/bs/buildComment.js
    /js/bs/tags.js
    /js/bs/labels.js
    /js/bs/overflower.js
    /js/bs/testDetails.js
    /js/bs/testGroup.js

    /js/bs/locationHash.js
    /js/bs/buildLogTree.js
  </bs:linkScript>

  <!-- ===== JS files, provided by plugins: ==== -->
  <c:set var="historyBuildText"><c:if test="${buildData.outOfChangesSequence}">History build&nbsp;</c:if></c:set>
  <c:set var="title">#${buildData.buildNumber} (${startDate})</c:set>
  <c:if test="${'N/A' == buildData.buildNumber}">
    <c:set var="title" value="${startDate}"/>
  </c:if>
  <script type="text/javascript">
    <bs:trimWhitespace>
      BS.Pin.noHover = true;

      BS.Navigation.items = [
        {
          title: "<bs:escapeForJs text="${buildType.projectName}" forHTMLAttribute="true"/>",
          url: "project.html?projectId=${buildType.projectId}",
          selected: false,
          itemClass: "project"
        },
        {
          title: "<bs:escapeForJs text="${buildType.name}" forHTMLAttribute="true"/>",
          url: "viewType.html?buildTypeId=${buildType.buildTypeId}",
          selected: false,
          itemClass: "buildType ${buildType.status.failed ? 'failed' : ''}",
          buildTypeId: "${buildType.buildTypeId}",
          siblings: {
            type: "buildType",
            url: "viewType.html?buildTypeId=",
            buildTypes: [
              <c:forEach items="${buildType.project.buildTypes}" var="bt" varStatus="pos">
              {
                id: "${bt.buildTypeId}",
                name: "<bs:escapeForJs text="${bt.name}"/>"
                <c:if test="${buildType.buildTypeId == bt.buildTypeId}">,
                selected: true
                </c:if>
              }
              <c:if test="${not pos.last}">,</c:if>
              </c:forEach>
            ]
          }
        },
        {
          title: "<bs:forJs><bs:buildDataIcon buildData="${buildData}"
                                              simpleTitle="${true}"
                                              style="margin-right: 5px; margin-bottom: 2px"/></bs:forJs>${historyBuildText}<bs:escapeForJs text="${title}" forHTMLAttribute="true"/>",
          url: "viewLog.html?buildTypeId=${buildType.buildTypeId}&buildId=${buildData.buildId}",
          selected: true
        }
      ];

      var enablePeriodicalRefresh = _.once(function() {
        <c:if test="${not buildData.finished}">
          $j(document).ready(function() {
            BS.PeriodicalRefresh.start(5, function() {
              $('buildResults').refresh(null, "runningBuildRefresh=1");
            });
          });
        </c:if>
      });

      <c:if test="${not isBuildOverview}">
        $j(document).on("bs.navigationRendered", function() {
          BS.BuildResults.installBuildDetailsPopup(${buildData.buildId});
        });
      </c:if>
    </bs:trimWhitespace>
  </script>
</jsp:attribute>

<jsp:attribute name="besideTabs_include">
  <c:if test="${not empty buildData.finishDate}">
    <span class="shortHistory"><jsp:include page="nextPreviousBlock.jsp"/></span>
  </c:if>
</jsp:attribute>

<jsp:attribute name="body_include">
  <c:set var="buildData" value="${buildData}" scope="request"/>
  <div id="container" class="clearfix">
    <bs:refreshable containerId="buildResults" pageUrl="${pageUrl}">
    <c:if test="${buildData.finished and not empty param['runningBuildRefresh']}">
    <script type="text/javascript">
      BS.reload(true);
    </script>
    </c:if>
    <c:if test="${not buildData.finished or empty param['runningBuildRefresh']}">
      <script type="text/javascript">
        (function() {
          var paneNav = new TabbedPane();

          function initialize3dLevel() {
            var baseUrl = "viewLog.html?buildId=${buildData.buildId}&buildTypeId=${buildData.buildTypeId}&tab=";
            var caption;

          <ext:forEachTab placeId="<%=PlaceId.BUILD_RESULTS_TAB%>">
          <c:set var="caption"><bs:forJs>${extension.tabTitle}</bs:forJs></c:set>
            <c:choose>
            <c:when test="${extension.tabId  == 'buildChangesDiv' and not buildData.outOfChangesSequence}">
            caption = "Changes (${numDetectedChanges})";
            </c:when>
            <c:otherwise>
            caption = "${caption}";
            </c:otherwise>
            </c:choose>

            paneNav.addTab("${extension.tabId}", {
              caption: caption,
              titleText: "${caption}",
              url: baseUrl + "${extension.tabId}"
            });
          </ext:forEachTab>

            paneNav.setActiveCaption('${extensionTab.tabId}');
          }
          initialize3dLevel();

          paneNav.showIn('tabsContainer3');
        })();
      </script>

      <c:if test="${isBuildOverview}"><bs:buildDataStatus buildData="${buildData}" showAlsoRunning="true"/></c:if>
      <ext:includeExtension extension="${extensionTab}"/>
    </c:if>
    <c:if test="${not buildData.finished and not empty param['runningBuildRefresh']}">
      <div style="display: none;">
        <bs:buildDataIcon buildData="${buildData}" imgId="buildDataIcon"/>
        <script type="text/javascript">
          BS.BuildResults.minorUpdate();
        </script>
      </div>
    </c:if>
    </bs:refreshable>
  </div>

  <bs:viewTypeMenu buildType="${buildType}"/>

  <tags:form allTags="${buildData.buildType.tags}"/>
  <bs:pinBuildDialog onBuildPage="${true}" allTags="${buildData.buildType.tags}"/>
  <bs:buildCommentDialog />
  <bs:promoteBuildDialog />

  <c:if test="${not empty buildType.project.buildTypes}">
    <bs:popupControl clazz="siblingBuildTypes" style="display: none"
                     showPopupCommand="" hidePopupCommand="" stopHidingPopupCommand=""
                     controlId="siblingBuildTypes${buildType.project.projectId}"></bs:popupControl>
  </c:if>

</jsp:attribute>
</bs:page>
