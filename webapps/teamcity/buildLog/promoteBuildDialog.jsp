<%@ include file="/include-internal.jsp" %>
<jsp:useBean id="build" type="jetbrains.buildServer.serverSide.SBuild" scope="request"/>
<jsp:useBean id="dependencyRefs" type="java.util.Collection" scope="request"/>
<jsp:useBean id="modificationIdsMap" type="java.util.Map" scope="request"/>
<jsp:useBean id="totalNumberOfRefs" type="java.lang.Integer" scope="request"/>

<c:set var="buildType" value="${build.buildType}"/>

<c:set value="${build.buildId}" var="id"/>
<c:set value="${build.branch}" var="branch"/>
<c:set var="branchOpts"><c:if test="${not empty branch}"> , branchName: '<bs:escapeForJs text="${branch.name}"/>'</c:if></c:set>
<c:if test="${fn:length(dependencyRefs) > 0}">
<p>Promote build <strong><bs:buildNumber buildData="${build}"/></strong> to:</p>
<table class="promoteBuild borderBottom">
  <c:forEach items="${dependencyRefs}" var="bt" varStatus="pos">
    <c:set value="${bt.buildTypeId}" var="btId"/>
    <c:url value="/viewType.html?buildTypeId=${btId}" var="redirectTo"/>

    <tr>
      <c:if test="${pos.last}">
        <c:set var="noBorder" value="noBorder"/>
      </c:if>
      <td class="header ${noBorder}"><c:out value="${bt.fullName}"/></td>
      <td class="runButton ${noBorder}">
        <span class="buttonWrapper">
        <button class="btn btn_mini btn_main action" id="runButton:${btId}"
                onclick="BS.RunBuild.runOnAgent(this, '${btId}',
                    { redirectTo: '${redirectTo}', dependOnPromotionIds: ${build.buildPromotion.id}<c:if test="${not empty modificationIdsMap[bt]}">, modificationId: ${modificationIdsMap[bt]}</c:if>${branchOpts} }); return false;"
                title="Click to run build on any agent">Run</button>
        <button class="btn btn_mini btn_append" id="run_${btId}"
                onclick="BS.RunBuild.runCustomBuild('${btId}',
                    { redirectTo: '${redirectTo}', dependOnPromotionIds: ${build.buildPromotion.id}<c:if test="${not empty modificationIdsMap[bt]}">, modificationId: ${modificationIdsMap[bt]}</c:if>${branchOpts}, init: true, stateKey: 'promote' }); return false"
                title="Run custom build">...</button>
        </span>
      </td>
    </tr>
  </c:forEach>
</table>
</c:if>
<c:if test="${fn:length(dependencyRefs) == 0 and totalNumberOfRefs == 0}">
  <div>Cannot find build configurations depending on <strong><c:out value="${buildType.fullName}"/></strong> by snapshot or artifact dependency.</div>
</c:if>
<c:if test="${fn:length(dependencyRefs) == 0 and totalNumberOfRefs > 0}">
  <div>You do not have enough permissions to run builds in build configurations depending on <strong><c:out value="${buildType.fullName}"/></strong>.</div>
</c:if>
