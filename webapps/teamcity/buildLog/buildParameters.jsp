<%@include file="/include-internal.jsp"%>
<jsp:useBean id="buildParameters" type="jetbrains.buildServer.controllers.viewLog.ActualBuildParametersBean" scope="request"/>
<jsp:useBean id="buildFinishParameters" type="jetbrains.buildServer.controllers.viewLog.ActualBuildParametersBean" scope="request"/>

<c:set var="overriddenConfigParams" value="${buildParameters.overridenConfigParams}"/>
<c:set var="overriddenSysProps" value="${buildParameters.overriddenSystemProperties}"/>
<c:set var="overriddenEnvVars" value="${buildParameters.overriddenEnvVariables}"/>

<div class="buildParameters divsWithHeaders">
  <h2>User Defined Parameters</h2>
  <c:choose>
    <c:when test="${buildParameters.allParameters.parametersAvailable}">
      <c:set var="sectionNumber" value="${buildParameters.sectionNumber}"/>
      <c:set var="counter" value="0"/>
      <c:if test="${not (empty buildParameters.allParameters.configurationParameters and empty overriddenConfigParams)}">
        <c:set var="counter" value="${counter + 1}"/>
        <c:set var="divClass"><c:out value="${counter eq 1 ? 'first' : ''}"/> <c:out value="${counter eq sectionNumber ? ' last' : ''}"/></c:set>
        <div class="${divClass}" style="${(counter eq sectionNumber ? 'border-bottom: none;' : '')}">
          <h3>Configuration Parameters</h3>
          <bs:_buildParamsTable valueColumnTitle="Value passed to build" parameters="${buildParameters.allParameters.configurationParameters}" overriddenParameters="${overriddenConfigParams}"/>
        </div>
      </c:if>

      <c:if test="${not (empty buildParameters.allParameters.systemProperties and empty overriddenSysProps)}">
        <c:set var="counter" value="${counter + 1}"/>
        <c:set var="divClass"><c:out value="${counter eq 1 ? 'first' : ''}"/> <c:out value="${counter eq sectionNumber ? ' last' : ''}"/></c:set>
        <div class="${divClass}" style="${(counter eq sectionNumber ? 'border-bottom: none;' : '')}">
          <h3>System properties</h3>
          <bs:_buildParamsTable valueColumnTitle="Value passed to build" parameters="${buildParameters.allParameters.systemProperties}" overriddenParameters="${overriddenSysProps}"/>
        </div>
      </c:if>

      <c:if test="${not (empty buildParameters.allParameters.environmentVariables and empty overriddenEnvVars)}">
        <c:set var="counter" value="${counter + 1}"/>
        <c:set var="divClass"><c:out value="${counter eq 1 ? 'first' : ''}"/> <c:out value="${counter eq sectionNumber ? ' last' : ''}"/></c:set>
        <div class="${divClass}" style="${(counter eq sectionNumber ? 'border-bottom: none;' : '')}">
          <h3>Environment variables</h3>
          <bs:_buildParamsTable valueColumnTitle="Value passed to build" parameters="${buildParameters.allParameters.environmentVariables}" overriddenParameters="${overriddenEnvVars}"/>
        </div>
      </c:if>
    </c:when>
    <c:otherwise>
      <div class="last" style="border-bottom: none;">no parameters defined</div>
    </c:otherwise>
  </c:choose>
</div>


<c:if test="${buildFinishParameters.allParameters.parametersAvailable}">
  <c:set var="sectionNumber" value="${buildFinishParameters.sectionNumber}"/>
  <c:set var="counter" value="0"/>
  <div class="buildParameters divsWithHeaders">
    <h2>Actual Parameters on Agent</h2>

    <c:if test="${not empty buildFinishParameters.allParameters.configurationParameters}">
      <c:set var="counter" value="${counter + 1}"/>
      <c:set var="divClass"><c:out value="${counter eq 1 ? 'first' : ''}"/> <c:out value="${counter eq sectionNumber ? ' last' : ''}"/></c:set>
      <div class="${divClass}" style="${(counter eq sectionNumber ? 'border-bottom: none;' : '')}">
        <h3>Configuration Parameters</h3>
        <bs:_buildParamsTable valueColumnTitle="Value used by the build" parameters="${buildFinishParameters.allParameters.configurationParameters}" overriddenParameters="${buildFinishParameters.overridenConfigParams}"/>
      </div>
    </c:if>

    <c:if test="${not empty buildFinishParameters.allParameters.systemProperties}">
      <c:set var="counter" value="${counter + 1}"/>
      <c:set var="divClass"><c:out value="${counter eq 1 ? 'first' : ''}"/> <c:out value="${counter eq sectionNumber ? ' last' : ''}"/></c:set>
      <div class="${divClass}" style="${(counter eq sectionNumber ? 'border-bottom: none;' : '')}">
        <h3>System properties</h3>
        <bs:_buildParamsTable valueColumnTitle="Value used by the build" parameters="${buildFinishParameters.allParameters.systemProperties}" overriddenParameters="${buildFinishParameters.overriddenSystemProperties}"/>
      </div>
    </c:if>

    <c:if test="${not empty buildFinishParameters.allParameters.environmentVariables}">
      <c:set var="counter" value="${counter + 1}"/>
      <c:set var="divClass"><c:out value="${counter eq 1 ? 'first' : ''}"/> <c:out value="${counter eq sectionNumber ? ' last' : ''}"/></c:set>
      <div class="${divClass}" style="${(counter eq sectionNumber ? 'border-bottom: none;' : '')}">
        <h3>Environment variables</h3>
        <bs:_buildParamsTable valueColumnTitle="Value used by the build" parameters="${buildFinishParameters.allParameters.environmentVariables}" overriddenParameters="${buildFinishParameters.overriddenEnvVariables}"/>
      </div>
    </c:if>
  </div>
</c:if>
