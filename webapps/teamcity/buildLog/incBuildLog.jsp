<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="fromIndex" type="java.lang.Integer" scope="request"/>
<jsp:useBean id="buildData" type="jetbrains.buildServer.serverSide.SBuild" scope="request"/>
<jsp:useBean id="tailMessages" type="jetbrains.buildServer.controllers.viewLog.CountingIterator" scope="request"/>

<bs:changeRequest key="messagesIterator" value="${tailMessages}">
  <jsp:include page="buildLogPrinter.html">
    <jsp:param name="renderRunningTime" value="false"/>
    <jsp:param name="mergeTestOutput" value="true"/>
  </jsp:include>
</bs:changeRequest>

<div class='hidden updateInfo'>${tailMessages.counter - fromIndex}</div>

<c:if test="${buildData.finished}">
  <div style="display: none;">Build finished</div>
</c:if>

<div class="updateInfo" style="display: none;">
  <bs:buildDataIcon buildData="${buildData}" imgId="buildDataIcon" imgOnly="true"/>
  <script type="text/javascript">BS.BuildLog.minorUpdate("${buildData.buildLog.sizeEstimate}");</script>
</div>
