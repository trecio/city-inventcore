<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>

<jsp:useBean id="buildData" type="jetbrains.buildServer.serverSide.SBuild" scope="request"/>
<jsp:useBean id="treeIterator" type="jetbrains.buildServer.serverSide.buildLog.LowLevelEventsAwareMessageIterator" scope="request"/>
<jsp:useBean id="expand" type="java.lang.String" scope="request"/>
<jsp:useBean id="hideBlocks" type="java.lang.String" scope="request"/>

<c:set var="enabledFilter"><%=request.getParameter("filter") == null ? "all" : request.getParameter("filter")%></c:set>
<c:set var="filterParam" value="${'all' eq enabledFilter ? '' : '&filter='}${'all' eq enabledFilter ? '' : enabledFilter}"/>
<c:set var="groupFlows"><%=Boolean.parseBoolean(request.getParameter("groupFlows"))%></c:set>

<bs:buildLogTree messagesIterator="${treeIterator}" buildData="${buildData}" expand="${expand}" groupFlows="${groupFlows}" enabledFilter="${enabledFilter}" hideBlocks="${hideBlocks}"/>

<c:if test="${not buildData.finished}">
  <c:set var="highlightFlows"><%=Boolean.parseBoolean(request.getParameter("highlightFlows"))%></c:set>

  <script type="text/javascript">
    $j(document).ready(function () {
      <c:set var="additionalParams"
             value="logTab=tree&filter=${enabledFilter}&expand=${expand}&groupFlows=${groupFlows}&hideBlocks=${hideBlocks}&highlightFlows=${highlightFlows}"/>
      /* The second argument: the index of last message. */
        BS.BuildLog.startUpdates(${buildData.buildId}, ${treeIterator.currentIndex}, true, 'incBuildLogTree.html', '${additionalParams}');
    });
  </script>
</c:if>
