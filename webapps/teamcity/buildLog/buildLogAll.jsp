<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>

<jsp:useBean id="buildData" type="jetbrains.buildServer.serverSide.SBuild" scope="request"/>
<jsp:useBean id="allIterator" type="java.util.Iterator" scope="request"/>

<c:catch var="error">
  <bs:buildLog buildData="${buildData}" messagesIterator="${allIterator}" renderRunningTime="false"/>
</c:catch>
<c:if test="${not empty error}">
  <div class="logReadingError">${error.cause == null ? error.message : error.cause.message}</div>
</c:if>

<c:if test="${not buildData.finished}">
  <script type="text/javascript">
    $j(document).ready(function () {
        /* The second argument: the index of last message. */
        BS.BuildLog.startUpdates(${buildData.buildId}, ${allIterator.counter}, false, 'incBuildLog.html');
    });
  </script>
</c:if>



