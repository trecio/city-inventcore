<%@ page import="jetbrains.buildServer.serverSide.buildLog.LogMessageFilter" %>
<%@ include file="../include-internal.jsp" %>

<jsp:useBean id="logReadingError" type="java.lang.String" scope="request"/>

<c:choose>
  <c:when test='${logReadingError eq ""}'>
    <jsp:useBean id="messagesAvailable" type="java.lang.Boolean" scope="request"/>

    <c:choose>
      <c:when test="${messagesAvailable}">
        <jsp:useBean id="buildData" type="jetbrains.buildServer.serverSide.SBuild" scope="request"/>
        <jsp:useBean id="logTab" type="java.lang.String" scope="request"/>
        <jsp:useBean id="canShowTree" type="java.lang.Boolean" scope="request"/>

        <c:set var="enabledFilter"><%=request.getParameter("filter") == null ? "all" : request.getParameter("filter")%>
        </c:set>
        <c:set var="filterParam" value="${'all' eq enabledFilter ? '' : '&filter='}${'all' eq enabledFilter ? '' : enabledFilter}"/>

        <bs:_viewLog build="${buildData}" noLink="true" urlAddOn="&logTab=important${filterParam}"/>
        <c:set var="important" value="${url}"/>

        <bs:_viewLog build="${buildData}" noLink="true" urlAddOn="&logTab=all${filterParam}"/>
        <c:set var="all" value="${url}"/>

        <bs:_viewLog build="${buildData}" noLink="true" urlAddOn="&logTab=tail${filterParam}"/>
        <c:set var="tail" value="${url}"/>

        <bs:_viewLog build="${buildData}" noLink="true" urlAddOn="&logTab=tree${filterParam}"/>
        <c:set var="tree" value="${url}"/>

        <bs:linkScript>
          /js/bs/buildLog.js
        </bs:linkScript>

        <c:set var="showTail" value="${logTab == 'tail'}"/>
        <c:set var="showTree" value="${logTab == 'tree'}"/>
        <c:set var="showAll" value="${logTab == 'all'}"/>

        <div class="subTabs">
          <table class="farRight">
            <tr>
              <td><a class="downloadLink" href="downloadBuildLog.html?buildId=${buildData.buildId}" style="white-space:nowrap;">
                Download full build log (~<span id="buildLogSizeEstimate">${buildData.buildLog.sizeEstimate}</span>)
              </a></td>
              <td><span class="separator">|</span></td>
              <td><a href="downloadBuildLog.html?buildId=${buildData.buildId}&archived=true">.zip</a></td>
              <td class="rawMessageFileOption" style="display:none;"><span class="separator">|</span></td>
              <td class="rawMessageFileOption" style="display:none;"><a href="downloadRawMessageFile.html?buildId=${buildData.buildId}">raw
                message file</a></td>
            </tr>
          </table>

          <c:set var="showTree" value="${showTree and canShowTree}"/>
          <c:set var="showImportant" value="${not showTail and not showTree and not showAll}"/>

          <c:choose>
            <c:when test="${showAll}">
              <a href="${important}">Important messages</a>
              <span class="separator">|</span>
              <strong>All messages</strong>
              <c:if test="${canShowTree}">
                <span class="separator">|</span>
                <a href="${tree}">Tree view</a>
              </c:if>
              <span class="separator">|</span>
              <a href="${tail}">Tail</a>
            </c:when>
            <c:when test="${showTail}">
              <a href="${important}">Important messages</a>
              <span class="separator">|</span>
              <a href="${all}">All messages</a>
              <c:if test="${canShowTree}">
                <span class="separator">|</span>
                <a href="${tree}">Tree view</a>
              </c:if>
              <span class="separator">|</span>
              <strong>Tail</strong>
            </c:when>
            <c:when test="${showTree}">
              <a href="${important}">Important messages</a>
              <span class="separator">|</span>
              <a href="${all}">All messages</a>
              <span class="separator">|</span>
              <strong>Tree view</strong>
              <span class="separator">|</span>
              <a href="${tail}">Tail</a>
            </c:when>
            <c:otherwise>
              <strong>Important messages</strong>
              <span class="separator">|</span>
              <a href="${all}">All messages</a>
              <c:if test="${canShowTree}">
                <span class="separator">|</span>
                <a href="${tree}">Tree view</a>
              </c:if>
              <span class="separator">|</span>
              <a href="${tail}">Tail</a>
            </c:otherwise>
          </c:choose>
            <%--<span style="margin-lefcont:2em"><label><input type="checkbox" id="wrapLines"/>Wrap lines</label></span>--%>
        </div>

        <c:choose>
          <%--all messages--%>
          <c:when test="${showAll}">
            <jsp:include page="buildLogAll.html"/>
          </c:when>

          <%--tree view--%>
          <c:when test="${showTree}">
            <jsp:include page="buildLogTree.html"/>
          </c:when>

          <%--tail--%>
          <c:when test="${showTail}">
            <jsp:include page="buildLogTail.html"/>
          </c:when>

          <%--important messages--%>
          <c:otherwise>
            <c:set var="filter" value="<%=new LogMessageFilter.DefaultFilter()%>" scope="request"/>
            <bs:buildLog buildData="${buildData}"
                         messagesList='<%=buildData.getBuildLog().getFilteredMessages((LogMessageFilter)request.getAttribute("filter"))%>'
                         renderRunningTime="true"/>
            <c:set var="overflowMessage" value="${filter.overflowMessage}"/>
            <c:if test="${overflowMessage != null}">
              <br/>

              <div style="color:${overflowMessage.status.htmlColor}"><c:out value="${overflowMessage.text}"/></div>
            </c:if>

            <c:if test="${not buildData.finished}">
              <script type="text/javascript">
                $j(document).ready(function () {
                  BS.BuildLog.enableRefresh();
                });
              </script>
            </c:if>
          </c:otherwise>
        </c:choose>

        <c:if test="${not buildData.finished}">
          <div id="buildLogProgress" style="float:left; vertical-align: middle; margin-top: 20px;">
            <img src="<c:url value='/img/buildStates/running_${buildData.buildStatus.successful ? "green" : "red"}_transparent.gif'/>"
                 class="icon"/>
            Running...
          </div>
        </c:if>

      </c:when>

      <c:otherwise>
        <i style="color:#888">Build log is empty</i>
      </c:otherwise>

    </c:choose>
  </c:when>

  <c:otherwise>
    <div class="logReadingError">${logReadingError}</div>
  </c:otherwise>

</c:choose>