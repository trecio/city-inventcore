<%@ include file="/include.jsp" %>
<%@ taglib prefix="bs" tagdir="/WEB-INF/tags" %>
<jsp:useBean id="dependencyGraphs" type="jetbrains.buildServer.controllers.graph.DependencyGraphsBean" scope="request"/>

<div style="margin-top: 1em;">
  <c:set var="numGraphs" value="${dependencyGraphs.numberOfChains}"/>
  <p>This change affected ${numGraphs} build chain<bs:s val="${numGraphs}"/>. <bs:help file="Build+Chain" /></p>
  <bs:buildChains dependencyGraphsBean="${dependencyGraphs}"/>
</div>