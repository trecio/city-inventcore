<%@ include file="/include-internal.jsp" %>
<%@ taglib prefix="tt" tagdir="/WEB-INF/tags/tests" %>

<jsp:useBean id="test" scope="request" type="jetbrains.buildServer.serverSide.STest"/>
<jsp:useBean id="testRuns" scope="request" type="java.util.List<jetbrains.buildServer.serverSide.STestRun>"/>
<jsp:useBean id="extraRuns" scope="request" type="java.util.Map<jetbrains.buildServer.serverSide.SBuild,java.lang.Integer>"/>
<c:set var="testDetailsId">tdi_${util:uniqueId()}</c:set>
<div id="${testDetailsId}" class="testDetailsInline">
  <div class="simpleTabs clearfix"></div>
  <script>
    (function() {
      var tabs = new TabbedPane();
      var parentDiv = $("${testDetailsId}");
      <c:forEach items="${testRuns}" var="testRun">
        <c:set var="build" value="${testRun.build}"/>
        <c:set var="buildBranch" value="${build.branch}"/>
        <c:set var="branchName" value="${not empty buildBranch ? buildBranch.displayName : ''}"/>
        tabs.addTab('tab_${testRun.testRunId}_${build.buildId}', {
          caption: "${util:forJS(build.buildType.name, true, true)}",   <%--Include build number? (<bs:buildNumber buildData="${build}"/>)--%>
          _branch: "${util:forJS(branchName, true, true)}",
          onselect: function() {
            $j('#${testDetailsId} div.testBlock').hide();
            var myBlock = $('${testDetailsId}').down("[data-buildId=${build.buildId}][data-testId=${testRun.testRunId}]");
            myBlock.show();
            var table = myBlock.down("table.testRelatedBuilds");
            if (table) {
              BS.TestDetails.loadFFIInformation(${build.buildId}, ${testRun.testRunId}, table);
            }
          }
        });
      </c:forEach>

      var allTabs = tabs.getTabs();
      for (var i = 0; i < allTabs.length; ++i) {
        var options = allTabs[i].myOptions;
        var branch = options._branch;
        if (branch) {
          if (branch.length > 16) {
            branch = branch.substr(0, 15) + "...";
          }
          options.captionAddin = "<span class='branch hasBranch'><span class='branchName' title='Build branch: " + branch + "'>" + branch + "</span></span>";
        }
      }

      var tabsContainer = parentDiv.down("div.simpleTabs");
      tabs.showIn(tabsContainer);

      tabs.setFirstActive();
      if (${fn:length(testRuns) == 1}) {
        tabsContainer.hide();
      }
    })();
  </script>
  <c:forEach items="${testRuns}" var="testRun">
    <bs:trimWhitespace>
      <c:set var="build" value="${testRun.build}"/>
      <c:set var="testId" value="${testRun.testRunId}"/>
      <div class="testBlock" data-buildId="${build.buildId}" data-testId="${testId}" style="display: none;">
        <c:choose>
          <c:when test="${testRun.status.failed}">
            <div class="rightBlock expandedDetails">
              <div class="collapser" onclick="BS.TestDetails.toggleBuildDetails(this);" title="Click to hide the details block">&nbsp;</div>
              <div class="relatedBuildsWrapper">
                <table class="testRelatedBuilds" cellspacing="0">
                  <%@ include file="testRelatedBuilds.jsp" %>
                </table>
              </div>
            </div>
            <c:set var="extra" value="${extraRuns[build]}"/>
            <c:if test="${not empty extra and extra > 1}">
              <div class="testRunsNote">The test was run <b>${extra}</b> times in a build</div>
            </c:if>
            <div class="rightBlock collapsedDetails" style="display: none;" onclick="BS.TestDetails.toggleBuildDetails(this);">
              <div class="collapser" title="Click to show the details block">
                <a href="#" onclick="return false;">show details</a>
              </div>
            </div>
            <div class="fullStacktrace"></div>
            <a href="#" onclick="BS.TestDetails.closeDetails(this); return false;" class="hideStacktrace">&laquo; Hide stacktrace</a>
          </c:when>
          <c:when test="${testRun.status.successful}">
            Successful in <bs:buildLinkFull build="${build}"/>
          </c:when>
          <c:when test="${testRun.muted}">
            Muted in <bs:buildLinkFull build="${build}"/>
          </c:when>
          <c:when test="${testRun.status.ignored}">
            Ignored in <bs:buildLinkFull build="${build}"/>
          </c:when>
        </c:choose>
      </div>
    </bs:trimWhitespace>
  </c:forEach>
</div>
