<%@ include file="/include-internal.jsp" %>
<jsp:useBean id="modification" scope="request" type="jetbrains.buildServer.vcs.SVcsModification"/>
<%--@elvariable id="showMode" type=""--%>
<c:url value="/downloadPatch.html" var="patchUrl">
  <c:param name="buildTypeId" value="${buildType.buildTypeId}"/>
  <c:param name="modId" value="${modification.id}"/>
  <c:param name="personal" value="${modification.personal}"/>
</c:url>
<c:if test="${modification.changeCount > 0}">
  <c:choose>
    <c:when test="${showMode == 'compact'}">
      <a class="noUnderline activateFileLink" href="${patchUrl}" title="Download patch"><img class="activateFileIcon" src="<c:url value='/img/artifacts.gif'/>"></a><bs:openPatchIde patchUrl="${patchUrl}" showMode="${showMode}"/>
    </c:when>
    <c:otherwise>
      <dt><img style="vertical-align:middle;" src="<c:url value='/img/artifacts.gif'/>"> <a href="${patchUrl}" title="Download patch">Download patch</a></dt>
      <dt><bs:openPatchIde patchUrl="${patchUrl}" showMode="${showMode}"/></dt>
    </c:otherwise>
  </c:choose>
</c:if>
