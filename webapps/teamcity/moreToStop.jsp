<%@ include file="/include-internal.jsp" %>
<jsp:useBean id="sequenceBuildInfo" type="jetbrains.buildServer.controllers.promotionGraph.SequenceBuildInfo" scope="request"/>

<c:if test="${empty buildPromotion}">
  Cannot find build data, it seems that build is already deleted.
  <script language="text/javascript">
    BS.StopBuildDialog.handleBuildNotFound();
  </script>
</c:if>
<c:if test="${not empty buildPromotion}">
<jsp:useBean id="buildPromotion" type="jetbrains.buildServer.serverSide.BuildPromotion" scope="request"/>

<c:if test="${not buildPromotion.partOfBuildChain or buildPromotion.numberOfDependedOnMe == 0}">
<c:if test="${not buildIsInQueue or buildIsInQueue == false}">
  <forms:checkbox name="readd" id="stopBuildReadd"/><label for="stopBuildReadd">Re-add build to the queue</label>
  <br/>
</c:if>
</c:if>
<c:if test="${buildPromotion.partOfBuildChain}">

<br/>
This build is part of a build chain. <bs:help file="Build+Chain" />

<c:choose>
  <c:when test="${not empty sequenceBuildInfo.buildsToStop and not sequenceBuildInfo.hasUnavailable}">
    <br/>
    Stop other parts:
    <br/>
    <label for="killAll"><forms:checkbox name="killAll" id="killAll"/><strong>All builds</strong></label>
    <div class="clr"></div>
  </c:when>
  <c:when test="${not empty sequenceBuildInfo.buildsToStop and sequenceBuildInfo.hasUnavailable}">
    <!--You may also want to stop the following builds from the sequence (you don't have access rights for all the builds):-->
    <div class="attentionComment">You don't have access rights to see some of its parts.</div> 
    Select builds to stop:
    <br/>
  </c:when>
  <c:when test="${empty sequenceBuildInfo.buildsToStop and sequenceBuildInfo.hasUnavailable}">
    <div class="attentionComment">You don't have access rights to see its other parts.</div>
  </c:when>
</c:choose>

<c:forEach items="${sequenceBuildInfo.buildsToStop}" var="buildInfo">
  <c:set var="buildData" value="${buildInfo.promotion.associatedBuild}"/>

  <c:if test="${buildInfo.canStop}">
    <forms:checkbox id="bi${buildInfo.id}" name="kill" value="${buildInfo.promotion.id}" checked="${buildInfo.checked}"/>
  </c:if>
  <c:if test="${not buildInfo.canStop}">
    <forms:checkbox name="" className="checkboxPlaceholder"/>
  </c:if>

  <bs:queueDependencyState dependency="${buildInfo.promotion}"/>

</c:forEach>
</c:if>
</c:if>