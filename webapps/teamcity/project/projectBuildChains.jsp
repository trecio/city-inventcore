<%@ include file="../include-internal.jsp" %>
<jsp:useBean id="dependencyGraphs" type="jetbrains.buildServer.controllers.graph.DependencyGraphsBean" scope="request"/>

<c:set var="numGraphs" value="${dependencyGraphs.numberOfChains}"/>

<bs:buildChains dependencyGraphsBean="${dependencyGraphs}"/>
