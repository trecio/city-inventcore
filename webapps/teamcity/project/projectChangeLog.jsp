<%@ include file="../include-internal.jsp" %>
<jsp:useBean id="project" type="jetbrains.buildServer.serverSide.SProject" scope="request" />
<jsp:useBean id="changeLogBean" type="jetbrains.buildServer.controllers.buildType.tabs.ChangeLogBean" scope="request" />

<bs:changesList
     changeLog="${changeLogBean}"
     url="/project.html?projectId=${project.projectId}&tab=projectChangeLog"
     filterUpdateUrl="/projectChangeLogTab.html?projectId=${project.projectId}"
     projectId="${project.projectId}"
     hideBuildSelectors="true"
     showBuildTypeLink="true"/>