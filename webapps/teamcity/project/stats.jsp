<%@ page import="jetbrains.buildServer.web.openapi.PlaceId" %><%@
  taglib prefix="ext" tagdir="/WEB-INF/tags/ext"%><%@
  taglib prefix="bs" tagdir="/WEB-INF/tags"%>

<ext:includeExtensions placeId="<%=PlaceId.PROJECT_STATS_FRAGMENT%>"/>

<div id="nothingMessage" style="display: none">
  There are no available charts for this project. Please read the <bs:helpLink file="Custom+Chart">documentation</bs:helpLink> on adding custom project charts.
</div>

<script type="text/javascript">
  document.observe("dom:loaded", function() {
    if ($$("div.GraphContainer").length == 0) {
      $('nothingMessage').style.display = "block";
    }
  })
</script>