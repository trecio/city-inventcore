<%@ page import="jetbrains.buildServer.web.openapi.PlaceId"
  %><%@include file="/include-internal.jsp"
  %><%@taglib prefix="stats" tagdir="/WEB-INF/tags/graph"
  %>

<style type="text/css">
  #agentsFilterTimeToFixStatistics {
    display: none;
  }
  #showFailedTimeToFixStatistics {
    display: none;
  }
</style>

<br />
<stats:buildGraph id="SuccessRate" valueType="SuccessRate" height="110" defaultFilter="showFailed,averaged" hideFilters="showFailed,averaged"
                  hints="rendererB,xtemLabels"/>
<stats:buildGraph id="BuildDurationNetTimeStatistics" valueType="BuildDurationNetTime"/>
<stats:buildGraph id="TimeInQueueStatistics" valueType="TimeSpentInQueue" defaultFilter="showFailed" hideFilters="series, markers, showFailed"/>
<%--<stats:buildGraph id="TotalBuildDuration" valueType="BuildDuration"/>--%>
<stats:buildGraph id="TestCount" valueType="TestCount" defaultFilter="showFailed" hideFilters="averaged"/>

<%--@elvariable id="buildType" type="jetbrains.buildServer.serverSide.SBuildType"--%>

<stats:buildGraph id="TimeToFixStatistics" valueType="MaxTimeToFixTestGraph" defaultFilter="showFailed" />

<ext:includeExtensions placeId="<%=PlaceId.BUILD_CONF_STATISTICS_FRAGMENT%>"/>