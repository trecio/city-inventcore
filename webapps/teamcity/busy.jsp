<%@ include file="/include-internal.jsp"%>
<c:set var="pageTitle" value="Cleanup in progress" scope="request"/>
<jsp:useBean id="adminAccess" scope="request" type="java.lang.Boolean"/>
<!DOCTYPE html>
<html>
<head>
  <title><c:if test="${not empty pageTitle}">${pageTitle} -- </c:if>TeamCity</title>
  <bs:linkCSS>
    /css/main.css
  </bs:linkCSS>

  <script type="text/javascript">
    var base_uri=window.location.protocol+"//"+window.location.host+"<%=request.getContextPath() %>";
  </script>

  <bs:linkScript>
    /js/jquery/jquery-1.7.2.min.js
  </bs:linkScript>

  <script type="text/javascript">
    window.$j = jQuery.noConflict();
  </script>

  <bs:linkScript>
    /js/prototype.js
    /js/aculo/effects.js

    /js/behaviour.js
    /js/underscore-min.js

    /js/bs/bs.js
    /js/bs/cookie.js
    /js/bs/position.js
    /js/bs/basePopup.js
    /js/bs/menuList.js
    /js/bs/forms.js
    /js/bs/jvmStatusForm.js
  </bs:linkScript>

  <style type="text/css">
    .maintenanceMessage {
      font-size: 140%;
      margin-top: 10em;
      margin-left: auto;
      margin-right: auto;
      width: 80%;
    }

    .cleanupDiagnostics {
      font-size: 100%;
      margin-top: 10em;
      margin-left: auto;
      margin-right: auto;
      width: 80%;
    }
  </style>
  <script type="text/javascript">
    $j(document).ready(function() {
      window.setTimeout(function() {
        var href = document.location.href;
        if (href.indexOf('?') == -1) {
          if (href.indexOf('#') != -1) {
            document.location.href = href.replace('#', '?_#');
          } else {
            document.location.href = href + '?_';
          }
        } else {
          BS.reload(true);
        }
      }, 15000);
    })
  </script>
  <link rel="Shortcut Icon" href="<c:url value="/favicon20.ico"/>" type="image/x-icon"/>
</head>

<body>

<div class="maintenanceMessage">
  Server is performing cleanup at the moment. It may take from several minutes to several hours depending on the size of the database.
  <br/>
  <br/>
  Elapsed time: <c:out value="${elapsedTime}"/>
  <br/>
  <br/>
  This page will be reloaded automatically when the cleanup process is completed.
</div>

<c:if test="${adminAccess}">
<form action="<c:url value='/admin/diagnostic.html'/>" method="post" id="jvmStatusForm" style="padding:0; margin: 0.5em 0">
  <div class="cleanupDiagnostics">
    <h2>Cleanup Diagnostics</h2>

    <div style="width: 100%; margin-top: 0.5em;">
      <bs:messages key="threadDumpSucceeded"/>
    </div>

    <div>
      If cleanup process appears slow or is hanging, please try taking several thread dumps with some interval and send them to
      <bs:helpLink file='Reporting+Issues#ReportingIssues-SendingInformationtotheDevelopers'>TeamCity developers</bs:helpLink>.
      <br/>
      <br/>
      <input class="btn" type="button" name="threadDump" value="Save Thread Dump" onclick="BS.JvmStatusForm.takeThreadDump()"/>
      <input type="hidden" name="actionName" value=""/>
    </div>
  </div>
</form>
</c:if>

<iframe style="height:0;width:0;visibility:hidden" src="about:blank">
  This frame should prevent back forward cache in Safari, see http://developer.apple.com/internet/safari/faq.html#anchor5
</iframe>
<div id="__tc_cleanupInProgressMarker"></div><!-- do not remove this div, it is used as marker to identify this page -->
</body>
</html>
